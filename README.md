#CMS WEBTEK

##RELEASES
###v.0.9.28
- Riscritta gestione dei log (ora avviene sui files)
- Aggiunto trait Loggable a tutte le entity da loggare    
- Modifica css per mostrare le tabelle al 100% in admin 
- Aggiunto file php_cs.dist per utilizzare https://github.com/squizlabs/PHP_CodeSniffer

###v.0.9.27
- Aggiunto ordinamento dei prodotti nell'ecommerce    
- Aggiunto comandi ecommerce 
    * ecommerce:dummy per generare un ecommerce con alcuni prodotti di test
    * ecommerce:fixsort per impostare l'ordinamento dei prodotti nel caso l'ecommerce fosse stato generato prima di questo tag
    
###v.0.9.26
- Aggiunto il modulo per la gestione dell'ecommerce
- Aggiunte diverse fixtures per poter ricaricare un layout spartano per l'ecommerce.

###v.0.9.25
- Tag cumulativo dei bug riscontrati dalla versione precedente, verificare messaggi di commit intermedi

###v.0.9.24
- Bugfix problema caricamento allegati e immagini nelle rows per news e pagine in fase di prima creazione

###v.0.9.23
- Introdotto il bundle Azienda che permette di salvare sedi di lavoro, con reparti e orari di apertura e chiusura e dipendenti in relazione alle sedi.
- Eseguito il merge del lato pubblico dell'ecommerce, oneri e onori a Gabriele Colombera :-)

###v.0.9.22
- Introdotta la funzionalità di gestione dei gruppi e dei ruoli direttamente dal pannello amministrativo.

###v.0.9.21
- Introdotto il bundle delle offerte e rivisto secondo i nuovi standard
- Pulizia di codice inutilizzato nel bundle news. 

###v.0.9.20
- Introdotto l'utilizzo di npm per installare le librerie css/js, dopo l'installazione è necessario un npm install nella web dir del progetto.
- Integrato il bundle degli appartamenti, riviste alcune dinamiche, aggiunta la compressione delle immagini
- Aggiunta categorizzazione moduli 

###v.0.9.12
- Fix filemanager, le immagini non venivano compresse all'atto dell'upload
- Fix Query di estrazione news da categoria, baco nel controllo di start e limit. 

###v.0.9.11
- Rimosso font montserrat nell'header di default #31 
- Chiamata a metodo errato nel blog del metamanager , il servizio poi era erroneamente nell'ecommerceBundle, spostato #23 #29
- Problemi con il caricamento immagini nel blog #17 
- Sistemati i problemi di path in windows per il file manager #15
- Il file head.html.twig veniva importato 2 volte #14
- Sistemato lo script per la creazione delle fixtures #18
- Salvataggio degli alt degli allegati in caso di nuova lingua #13
- Commentato nel securty.yml il firewall dell'ecommerce da attivare on demand #27
 
###v.0.9.10
- Issue #13, risoluzione del baco relativo al salvataggio del testo alternativo in multilingua (vedi issue per maggiori dettagli)

###v.0.9.9
- Bugfix per sitemap che genera url pagine anche quando queste sono disattive
- Aggiunto nuovo ruolo che permette di rendere la visualizzazione pagine più semplice da utilizzare (vengono disattivate tutte le parti di modifica della struttura)  

###v.0.9.8
- Modificata la gestione vetrina per utilizzare il nuovo metodo di ridimensionamento immagini.  

###v.0.9.7
- Aggiunto modulo per la gestione delle gallery fotografiche con categoria. 

###v.0.9.6
- issue #11 (attenzione, tutti i moduli che usano i filtri sono stati aggiornati in modo massivo, potrebbero esserne sfuggiti alcuni)
- issue #10
- Aggiunta la possibilità in admin di indicare di caricare le risorse minificate. (controllare head.html.twig in layout default) 
- Piccoli bugfix sulla gestione degli allegati e delle immagini in linea. 
- Rimosso bundle iperauto

###v.0.9.5

- Aggiunto il modulo per la gestione dei dipendenti 
- Modificati i brand dell'ecommerce per utilizzare il nuovo metodo di gestione delle immagini in linea 
- Fix minori relativi ai percorsi di file comuni utilizzati da più bundles

###v.0.9.4

- issue #4 - Modificati i percorsi nel file manager per evitare incongruenze con i sistemi windows 
- Aggiunta la possibilità di aggiungere campi a numero variabile per i tipi: image, link, file, simpleText SOLO in campi NON MULTILINGUA

###v.0.9.3

- issue #3 - Sitemap 
- Aggiunti file mancanti nel master per la gestione delle row della versione precedente.

###v.0.9.2

- issue #2. 
  Aggiunto in pagine e news il bottone per creare una riga a 2 colonne, tipica dei box doppi

###v.0.9.1

- Implementato ridimensionamento e compressione delle immagini automatico per news, pagine e prodotti.

- Bugfix estranzione categoria news nel blogcontroller. 

- Aggiunta versione nel footer

###v.0.9.0 

- Versione beta