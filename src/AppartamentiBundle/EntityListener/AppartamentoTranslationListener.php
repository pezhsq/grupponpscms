<?php
// 05/01/17, 16.31
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\EntityListener;


use AppartamentiBundle\Entity\AppartamentoTranslation;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AppartamentoTranslationListener {

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    private $slugger;


    public function __construct(Slugify $slugger, AuthorizationCheckerInterface $authorizationChecker) {

        $this->authorizationChecker = $authorizationChecker;
        $this->slugger              = $slugger;
    }


    /**
     * @ORM\PrePersist()
     * @param $page AppartamentoTranslation
     * @param $event LifecycleEventArgs
     */
    public function prePersist(AppartamentoTranslation $AppartamentoTranslation, LifecycleEventArgs $event) {

        $this->generaSlug($AppartamentoTranslation);
        $this->generaMetaTitle($AppartamentoTranslation);
        $this->generaMetaDescription($AppartamentoTranslation);

    }

    /**
     * @ORM\PreUpdate()
     * @param AppartamentoTranslation $page
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(AppartamentoTranslation $AppartamentoTranslation, PreUpdateEventArgs $event) {

        $changed = $event->getEntityChangeSet();
        
        if($changed && isset($changed['titolo'])) {
            $this->generaSlug($AppartamentoTranslation);
            $this->generaMetaTitle($AppartamentoTranslation);
            $this->generaMetaDescription($AppartamentoTranslation);
        }

    }

    private function generaSlug(AppartamentoTranslation $AppartamentoTranslation) {

        if(!$this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') || ($this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') && !$AppartamentoTranslation->getSlug())) {

            $AppartamentoTranslation->setSlug($this->slugger->slugify($AppartamentoTranslation->getTitolo()));


        }


    }

    private function generaMetaTitle(AppartamentoTranslation $AppartamentoTranslation) {

        if(!$this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') || ($this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') && !$AppartamentoTranslation->getMetaTitle())) {
            $AppartamentoTranslation->setMetaTitle($AppartamentoTranslation->getTitolo());
        }

    }

    private function generaMetaDescription(AppartamentoTranslation $appartamentoTranslation) {


    }


}