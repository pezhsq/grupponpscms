<?php
// 13/03/17, 13.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\EntityListener;


use AppartamentiBundle\Entity\Appartamento;
use AppBundle\Service\AttachmentFSManager;
use AppBundle\Service\Base64Image;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Filesystem;

class AppartamentoListener {

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var Base64Image
     */
    private $base64Image;
    /**
     * @var AttachmentFSManager
     */
    private $attachmentFSManager;

    public function __construct(Slugify $slugify, Base64Image $base64Image, AttachmentFSManager $attachmentFSManager) {

        $this->slugify             = $slugify;
        $this->base64Image         = $base64Image;
        $this->attachmentFSManager = $attachmentFSManager;
    }

    /**
     * @ORM\PrePersist()
     * @param $appartamento Appartamento
     * @param $event LifecycleEventArgs
     */
    public function prePersist(Appartamento $appartamento, LifecycleEventArgs $event) {

        if($appartamento->getHeaderImgFileName()) {
            $appartamento->setHeaderImgFileName($this->base64Image->cleanFileName($appartamento->getHeaderImgFileName()));
        }

        if($appartamento->getListImgFileName()) {
            $appartamento->setListImgFileName($this->base64Image->cleanFileName($appartamento->getListImgFileName()));
        }


    }

    /**
     * @ORM\PreUpdate()
     * @param $appartamento $appartamento
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(Appartamento $appartamento, PreUpdateEventArgs $event) {

        if($event->hasChangedField('headerImgFileName')) {
            $this->base64Image->delete($appartamento, $event->getOldValue('headerImgFileName'));
            if($event->getNewValue('headerImgFileName')) {
                $appartamento->setHeaderImgFileName($this->base64Image->cleanFileName($appartamento->getHeaderImgFileName()));
            }
        }

        if($event->hasChangedField('listImgFileName')) {
            $this->base64Image->delete($appartamento, $event->getOldValue('listImgFileName'));
            if($event->getNewValue('listImgFileName')) {
                $appartamento->setListImgFileName($this->base64Image->cleanFileName($appartamento->getListImgFileName()));
            }
        }

    }


    /**
     * @ORM\PreRemove()
     */
    public function clean(Appartamento $appartamento, LifecycleEventArgs $event) {

        if($appartamento->getDeletedAt()) {
            $dir = $this->base64Image->getRootDir().$appartamento->getUploadDir();

            $fs = new Filesystem();
            $fs->remove($dir);
        }

    }

    /**
     * @ORM\PostPersist()
     * @param Appartamento $appartamento
     * @param LifecycleEventArgs $args
     */
    public function postPersist(Appartamento $appartamento, LifecycleEventArgs $args) {

        $this->attachmentFSManager->moveFiles($appartamento);

    }

}