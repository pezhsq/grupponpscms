<?php
// 13/03/17, 11.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\Repository;


use Doctrine\ORM\EntityRepository;

class AppartamentoRepository extends EntityRepository {

    function findAllNotDeleted() {

        return $this->createQueryBuilder('page')
            ->andWhere('page.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

}