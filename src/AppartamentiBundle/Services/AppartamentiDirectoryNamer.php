<?php
// 14/03/17, 12.02
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\Services;


use AppartamentiBundle\Entity\AppartamentoRow;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class AppartamentiDirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping)
    {

        $dir = $mapping->getUriPrefix();

        $Appartamento = $object;

        if ($Appartamento instanceof AppartamentoRow) {

            $Appartamento = $object->getAppartamento();

        }

        $dir = $Appartamento->getId();
        

        return $dir;

    }


}
