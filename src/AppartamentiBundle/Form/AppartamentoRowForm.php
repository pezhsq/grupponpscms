<?php
// 13/03/17, 14.46
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\Form;


use AppartamentiBundle\Entity\AppartamentoRow;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AppartamentoRowForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('slot1', TextareaType::class, ['label' => false]);
        $builder->add('slot2', TextareaType::class, ['label' => false]);
        $builder->add('slot3', TextareaType::class, ['label' => false]);
        $builder->add('locale', HiddenType::class);
        $builder->add('slot1ImageFile', FileType::class, ['label' => false, 'attr' => ['class' => 'uploadImgPage']]);
        $builder->add('slot2ImageFile', FileType::class, ['label' => false, 'attr' => ['class' => 'uploadImgPage']]);
        $builder->add('slot3ImageFile', FileType::class, ['label' => false, 'attr' => ['class' => 'uploadImgPage']]);
        $builder->add('slot1ImageAlt', TextType::class, ['label' => 'default.labels.altimage', 'required' => false]);
        $builder->add('slot2ImageAlt', TextType::class, ['label' => 'default.labels.altimage', 'required' => false]);
        $builder->add('slot3ImageAlt', TextType::class, ['label' => 'default.labels.altimage', 'required' => false]);
        $builder->add('structure', HiddenType::class);
        $builder->add('sort', HiddenType::class, ['attr' => ['class' => 'sortField']]);
    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class' => AppartamentoRow::class,
                                'label'      => false]);
    }

}