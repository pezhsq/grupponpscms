<?php
// 13/03/17, 14.24
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppartamentiBundle\Entity\Appartamento;
use AppBundle\Form\TypeExtension\SeoDescription;
use AppBundle\Form\TypeExtension\SeoTitle;
use Doctrine\ORM\EntityManager;
use Faker\Provider\Text;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use TagBundle\Entity\Tag;

class AppartamentoForm extends AbstractType
{

    /**
     * @var AuthorizationChecker
     */
    private $auth;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * PageForm constructor.
     */
    public function __construct(AuthorizationChecker $auth, EntityManager $em)
    {

        $this->auth = $auth;
        $this->em = $em;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('uuid', HiddenType::class);

        $builder->add('listImg', FileType::class);
        $builder->add('listImgData', HiddenType::class);
        $builder->add('listImgAlt', TextType::class, []);
        $builder->add('listImgDelete', HiddenType::class, []);

        $builder->add('headerImg', FileType::class);
        $builder->add('headerImgData', HiddenType::class);
        $builder->add('headerImgAlt', TextType::class, []);
        $builder->add('headerImgDelete', HiddenType::class, []);


        $builder->add(
            'servizi',
            EntityType::class,
            [
                'class' => Tag::class,
                'choice_label' => function ($Tag) {

                    $Categoria = $Tag->getCategoria()->translate()->getNome();

                    return $Categoria.': '.$Tag->translate()->getNome();

                },
                'multiple' => true,
                'expanded' => true,
                'required' => false,
            ]
        );

        $builder->add('prezzo', TextType::class, ['label' => 'appartamenti.labels.prezzo', 'required' => false]);
        $builder->add(
            'superficie',
            TextType::class,
            ['label' => 'appartamenti.labels.superficie', 'required' => false]
        );

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'appartamenti.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );


        $fields = [
            'titolo' => [
                'label' => 'pages.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            'sottotitolo' => [
                'label' => 'pages.labels.sottotitolo',
                'required' => false,
            ],
        ];

        $excluded_fields = [];

        if (!$this->auth->isGranted('ROLE_EXTRA_SEO')) {
            $excluded_fields = ['metaTitle', 'metaDescription', 'slug'];
        } else {

            $fields = array_merge(
                $fields,
                [
                    'metaTitle' => [
                        'label' => 'default.labels.meta_title',
                        'required' => false,
                        'field_type' => SeoTitle::class,
                    ],
                    'metaDescription' => [
                        'label' => 'default.labels.meta_description',
                        'required' => false,
                        'field_type' => SeoDescription::class,
                    ],
                    'slug' => [
                        'label' => 'default.labels.slug',
                        'required' => false,
                    ],
                ]
            );

        }

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'exclude_fields' => $excluded_fields,
            ]
        );


        $builder->add(
            'appartamentiRows',
            CollectionType::class,
            [
                'entry_type' => AppartamentoRowForm::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_extra_fields' => true,
                'label' => false,
                'attr' => [],
            ]
        );


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Appartamento::class,
                'error_bubbling' => true,
                'layout' => 'webtek',
                'langs' => [
                    'it' => 'Italiano',
                    'allow_extra_fields' => true,
                ],
            ]
        );
    }

    public function getName()
    {

        return 'appartamento_form';
    }


}