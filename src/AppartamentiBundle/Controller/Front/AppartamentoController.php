<?php
// 17/03/17, 12.15
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppartamentiBundle\Controller\Front;

use AppartamentiBundle\Entity\Appartamento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AppartamentoController extends Controller
{

    /**
     * @Route("/camera/{slug}", defaults={"_locale"="it"}, name="appartamento_it")
     * @Route("/{_locale}/camera/{slug}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl"}, defaults={"_locale"="it"}, name="appartamento")
     */
    public function readAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $AppartamentoTranslation = $em->getRepository('AppartamentiBundle:AppartamentoTranslation')->findOneBy(
            ['slug' => $request->get('slug')]
        );
        $pathManager = $this->get('app.path_manager');
        if ($AppartamentoTranslation) {

            /**
             * @var $Appartamento Appartamento
             */
            $Appartamento = $AppartamentoTranslation->getTranslatable();
            if ($Appartamento && $Appartamento->getIsEnabled()) {

                $TemplateLoader = $this->get('app.template_loader');
                $Languages = $this->get('app.languages');
                $AdditionalData = [];
                $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
                $AdditionalData['Entity'] = $Appartamento;
                $META = [];
                $META['title'] = $Appartamento->translate($request->getLocale())->getMetaTitle();
                $META['description'] = $Appartamento->translate($request->getLocale())->getMetaDescription();
                $META = array_merge($META, $this->get('app.open_graph')->generateOpenGraphData($Appartamento));
                foreach ($AdditionalData['langs'] as $sigla => $estesa) {

                    $params = ['slug' => $Appartamento->translate($sigla)->getSlug()];
                    $META['alternate'][$sigla] = $pathManager->generate('appartamento', $params, $sigla, true);

                }
                $AdditionalData['META'] = $META;
                $twigs = $TemplateLoader->getTwigs(
                    'appartamento',
                    $Languages->getActivePublicLanguages(),
                    $AdditionalData
                );

                return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

            }

        }
        throw new NotFoundHttpException("Pagina non trovata");

    }

}