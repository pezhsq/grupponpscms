<?php
// 17/03/17, 10.54
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\Controller\Front;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactUsController extends Controller {

    /**
     * @Route("/contact-appartamenti", defaults={"_locale"="it"}, name="contact-us-appartamenti_it")
     * @Route("/{_locale}/contact-us", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl"}, defaults={"_locale"="it"}, name="contact-us-appartamenti")
     */
    public function indexController(Request $request) {

        $trans = $this->get('translator');

        $errori = [];

        $data = [];

        if(!$request->request->has('fullname') || !$request->request->get('fullname')) {
            $errori[] = $trans->trans('booking.errors.fullname_empty', [], 'public');
        } else {
            $data['fullname'] = strip_tags($request->request->get('fullname'));
        }
        if(!$request->request->has('adulti') || !$request->request->get('adulti') || !is_numeric($request->request->get('adulti'))) {
            $errori[] = $trans->trans('booking.errors.adulti_empty', [], 'public');
        } else {
            $data['adulti'] = intval($request->request->get('adulti'));
        }

        $data['bambini'] = intval($request->request->get('bambini'));


        if(!$request->request->has('phone') || !$request->request->get('phone')) {
            $errori[] = $trans->trans('booking.errors.phone_empty', [], 'public');
        } else {
            $data['phone'] = strip_tags($request->request->get('phone'));
        }
        if(!$request->request->has('email') || !$request->request->get('email') || !filter_var($request->request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $errori[] = $trans->trans('booking.errors.bad_email', [], 'public');
        } else {
            $data['email'] = $request->request->get('email');
        }
        if(!$request->request->has('data-arrivo') || !$request->request->get('data-arrivo')) {
            $errori[] = $trans->trans('booking.errors.data_arrivo_empty', [], 'public');
        } else {
            $data['data_arrivo'] = strip_tags($request->request->get('data-arrivo'));
        }

        if(!$request->request->has('data-partenza') || !$request->request->get('data-partenza')) {
            $errori[] = $trans->trans('booking.errors.data_partenza_empty', [], 'public');
        } else {
            $data['data_partenza'] = strip_tags($request->request->get('data-partenza'));
        }

        if(!$request->request->has('messaggio')) {
            $errori[] = $trans->trans('booking.errors.messaggio_non_settato');
        } else {
            $data['messaggio'] = strip_tags($request->request->get('messaggio'));
        }

        if(!$request->request->has('privacy')) {
            $errori[] = $trans->trans('booking.errors.privacy_empty', [], 'public');
        }

        $return           = [];
        $return['result'] = true;

        if($errori) {
            $return['result'] = false;
            $return['errors'] = $errori;
        } else {

            $destinatari = explode(";", $this->getParameter('emails')['destinatario_contatti']);
            $destinatari = array_map('trim', $destinatari);
            $destinatari = array_filter($destinatari);


            $message = \Swift_Message::newInstance()
                ->setSubject('Richiesta informazioni')
                ->setFrom($data['email'], $data['fullname'])
                ->setTo($destinatari)
                ->setBody(
                    $this->renderView('modules/selettoreDate/SD.2.0.0/selettoreDate.email.twig',
                        ['data' => $data]
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);

        }

        return new JsonResponse($return);

    }

}