<?php
// 10/03/17, 16.51
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\Controller\Admin;

use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\AppartamentoAttachment;
use AppartamentiBundle\Entity\AppartamentoRow;
use AppartamentiBundle\Form\AppartamentoForm;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_APPARTAMENTI')")
 */
class AppartamentiController extends Controller
{

    /**
     * @Route("/appartamenti", name="appartamenti")
     */
    public function listAction()
    {

        return $this->render('AppartamentiBundle:Appartamenti:list.html.twig');

    }

    /**
     * @Route("/appartamenti/json", name="appartamenti_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $Appartamenti = $em->getRepository('AppartamentiBundle:Appartamento')->findAll();
        } else {
            $Appartamenti = $em->getRepository('AppartamentiBundle:Appartamento')->findAllNotDeleted();
        }

        $retData = [];

        $pages = [];

        foreach ($Appartamenti as $appartamento) {
            /**
             * @var $appartamento Appartamento;
             */
            $record = [];
            $record['id'] = $appartamento->getId();
            $record['titolo'] = $appartamento->translate($request->getLocale())->getTitolo();
            $record['deleted'] = $appartamento->isDeleted();
            $record['createdAt'] = $appartamento->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $appartamento->getUpdatedAt()->format('d/m/Y H:i:s');

            $pages[] = $record;
        }

        $retData['data'] = $pages;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/appartamenti/new", name="appartamenti_new")
     * @Route("/appartamenti/edit/{id}",  name="appartamenti_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $Appartamento = new Appartamento();

        $id = null;

        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;

        $uuid = $Appartamento->getUuid();

        if ($request->get('id')) {

            $id = $request->get('id');

            $Appartamento = $em->getRepository('AppartamentiBundle:Appartamento')->findOneBy(['id' => $id]);

            if (!$Appartamento) {

                return $this->redirectToRoute('appartamenti_new');
            }

            if ($Appartamento->getListImgFileName()) {
                $supportData['listImgUrl'] = '/'.$Appartamento->getUploadDir().$Appartamento->getListImgFileName();
            }
            if ($Appartamento->getHeaderImgFileName()) {
                $supportData['headerImgUrl'] = '/'.$Appartamento->getUploadDir().$Appartamento->getHeaderImgFileName();
            }


            $uuid = $id;

        }

        $originalApartmentRows = new ArrayCollection();

        // Crea an ArrayCollection delle attuali pagerows nel db
        foreach ($Appartamento->getAppartamentiRows() as $appartamentiRow) {
            $originalApartmentRows->add($appartamentiRow);
        }

        $form = $this->createForm(
            AppartamentoForm::class,
            $Appartamento,
            ['langs' => $langs, 'attr' => ['data-id' => $uuid]]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Appartamento Appartamento
             */

            $Appartamento = $form->getData();

            $new = false;

            if (!$Appartamento->getId()) {
                $new = true;
            }

            $em->persist($Appartamento);
            $em->flush();

            if ($new) {

                $rootDir = $this->get('app.web_dir')->get().'/';

                foreach ($Appartamento->getAppartamentiRows() as $appartamentiRow) {
                    /**
                     * @var $appartamentiRow AppartamentoRow
                     */
                    if ($appartamentiRow->getSlot1Image()) {
                        $file = $rootDir.'files/appartamenti/'.$appartamentiRow->getSlot1Image();
                        if (file_exists($file)) {
                            rename($file, $rootDir.$Appartamento->getUploadDir().$appartamentiRow->getSlot1Image());
                        }
                    }
                    if ($appartamentiRow->getSlot2Image()) {
                        $file = $rootDir.'files/appartamenti/'.$appartamentiRow->getSlot2Image();
                        if (file_exists($file)) {
                            rename($file, $rootDir.$Appartamento->getUploadDir().$appartamentiRow->getSlot2Image());
                        }
                    }
                    if ($appartamentiRow->getSlot3Image()) {
                        $file = $rootDir.'files/appartamenti/'.$appartamentiRow->getSlot3Image();
                        if (file_exists($file)) {
                            rename($file, $rootDir.$Appartamento->getUploadDir().$appartamentiRow->getSlot3Image());
                        }
                    }
                }

            }

            $elemento = $Appartamento->translate($request->getLocale())->getTitolo();

            foreach ($originalApartmentRows as $appartamentiRow) {
                if (false === $Appartamento->getAppartamentiRows()->contains($appartamentiRow)) {
                    $em->remove($appartamentiRow);
                }
            }

            $cancellaHeaderPrecedente = $request->get('appartamento_form')['headerImgDelete'];
            if ($cancellaHeaderPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($Appartamento, 'headerImg');
                $Appartamento->setHeaderImg(null);
                $Appartamento->setHeaderImgAlt('');
            }

            $cancellaListPrecedente = $request->get('appartamento_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($Appartamento, 'listImg');
                $Appartamento->setListImg(null);
                $Appartamento->setListImgAlt('');
            }

            $em->persist($Appartamento);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Appartamento "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Appartamento "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('appartamenti');

        }

        $view = 'AppartamentiBundle:Appartamenti:new.html.twig';

        if ($Appartamento->getId()) {
            $view = 'AppartamentiBundle:Appartamenti:edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView(), 'supportData' => $supportData]);

    }
    
    /**
     * @Route("/appartamenti/delete/{id}/{force}", name="appartamenti_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Appartamento $appartamento)
    {

        if ($appartamento) {

            $elemento = $appartamento->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($appartamento->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($appartamento);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Appartamento "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Appartamento "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$appartamento->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Appartamento "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($appartamento);
                $em->flush();


            }

        }

        return $this->redirectToRoute('appartamenti');

    }

    /**
     * @Route("/appartamenti/restore/{id}", name="appartamento_restore")
     */
    public function restoreAction(Request $request, Appartamento $appartamento)
    {

        if ($appartamento->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $appartamento->translate($request->getLocale())->getTitolo().' ('.$appartamento->getId().')';

            $appartamento->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Appartamento "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('appartamenti');

    }


}