<?php
// 17/03/17, 15.28
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\DataFormatter;


use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\AppartamentoRow;
use AppBundle\DataFormatter\DataFormatter;

class AppartamentiRowsFormatter extends DataFormatter {

    public function getData() {

        /**
         * @var $Appartamento Appartamento
         */
        $Appartamento = $this->AdditionalData['Entity'];

        $primaRiga = false;
        if($this->data[$this->locale]['da_riga']['val']) {
            $primaRiga = intval($this->data[$this->locale]['da_riga']['val'] - 1);
        }
        $length = null;

        if(is_numeric($this->data[$this->locale]['a_riga']['val']) && $this->data[$this->locale]['a_riga']['val'] > $primaRiga) {
            $length = $this->data[$this->locale]['a_riga']['val'] - $primaRiga;
        }

        $rows = [];

        $out = '';

        foreach($Appartamento->getAppartamentiRows() as $AppartamentoRow) {
            /**
             * @var $AppartamentoRow AppartamentoRow
             */
            if($AppartamentoRow->getLocale() == $this->locale) {
                $rows[] = $AppartamentoRow;
            }
        }

        if($primaRiga !== false) {
            $rows = array_slice($rows, $primaRiga, $length);
        }

        $data         = [];
        $data['rows'] = $rows;

        return $data;

    }

    protected function extractData() {
        // TODO: Implement extractData() method.
    }


}