<?php
// 15/03/17, 17.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppartamentiBundle\DataFormatter;

use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\CategoriaTag;
use AppartamentiBundle\Entity\Tag;
use AppBundle\DataFormatter\DataFormatter;

class AppartamentiServiziFormatter extends DataFormatter
{

    private $menu;

    private $Servizi;

    public function getData()
    {

        $data         = [];
        $data['menu'] = $this->menu;

        if ($this->Servizi) {

            $menu = [];
            foreach ($this->Servizi as $servizio) {
                /**
                 * @var $servizio Tag
                 */

                $menuItem           = [];
                $menuItem['nome']   = $servizio->translate()->getNome();
                $menuItem['childs'] = [];

                $appartamenti = $servizio->getAppartamenti();

                foreach ($appartamenti as $Appartamento) {

                    /**
                     * @var $Appartamento Appartamento
                     */
                    $child          = [];
                    $child['label'] = $Appartamento->translate()->getTitolo();
                    $child['url']   = $this->container->get('app.path_manager')->generateUrl(
                        'appartamento',
                        ['slug' => $Appartamento->translate()->getSlug()]
                    );

                    $menuItem['childs'][] = $child;
                }

                $menu[] = $menuItem;

            }

            $data['menu'] = $menu;
        }


        return $data;
    }

    public function getVetrina()
    {

        $liipManager = $this->container->get('liip_imagine.controller');
        $liipCache   = $this->container->get('liip_imagine.cache.manager');

        $this->extractData();

        $data = [];

        $data['appartamenti'] = [];
        $data['label']        = false;

        if ($this->Servizi) {


            $appartamenti = [];

            foreach ($this->Servizi as $servizio) {
                /**
                 * @var $servizio Tag
                 */

                if (!$data['label']) {
                    $data['label'] = $servizio->translate()->getNome();
                }

                $Appartamenti = $servizio->getAppartamenti();

                foreach ($Appartamenti as $Appartamento) {

                    /**
                     * @var $Appartamento Appartamento
                     */
                    $appartamento               = [];
                    $appartamento['label']      = $Appartamento->translate()->getTitolo();
                    $appartamento['img']        = [];
                    $appartamento['img']['src'] = 'http://fpoimg.com/630x410?text=WebtekCMS';
                    $appartamento['img']['alt'] = $appartamento['label'];

                    if ($Appartamento->getListImgFileName()) {

                        $liipManager->filterAction(
                            $this->request,
                            $Appartamento->getUploadDir().$Appartamento->getListImgFileName(),
                            'appartamenti_list'
                        );

                        $appartamento['img']['src'] = $liipCache->getBrowserPath(
                            $Appartamento->getUploadDir().$Appartamento->getListImgFileName(),
                            'appartamenti_list'
                        );

                        $appartamento['img']['alt'] = $Appartamento->getListImgAlt();
                    }

                    $appartamento['url'] = $this->container->get('app.path_manager')->generateUrl(
                        'appartamento',
                        ['slug' => $Appartamento->translate()->getSlug()]
                    );

                    $appartamenti[] = $appartamento;

                }


            }

            $data['appartamenti'] = $appartamenti;

        }

        return $data;


    }

    public function extractData()
    {

        if ($this->data['it']['servizi']['val']) {

            $re = '/(([^,]+)\,?)+/';

            if (preg_match($re, $this->data['it']['servizi']['val'], $m)) {

                $servizi = explode(',', trim($this->data['it']['servizi']['val']));

                $servizi = array_map('trim', $servizi);

                if (is_numeric($servizi[0])) {
                    $Servizi = $this->em->getRepository('TagBundle:Tag')->findBy(['id' => $servizi]);
                } else {
                    $Servizi = $this->em->getRepository('TagBundle:Tag')->findByName($servizi, $this->locale);
                }

                $this->Servizi = $Servizi;

            }

        }


    }
}