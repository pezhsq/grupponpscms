<?php

namespace AppartamentiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @Gedmo\Loggable
 * @ORM\EntityListeners({"AppartamentiBundle\EntityListener\AppartamentoTranslationListener"})
 * @ORM\Table(name="appartamenti_translations")
 */
class AppartamentoTranslation {

    use ORMBehaviours\Translatable\Translation;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $titolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $metaTitle;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $metaDescription;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $slug;
    
    /**
     * @return mixed
     */
    public function getTitolo() {

        return $this->titolo ? $this->titolo : '-';
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo) {

        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo() {

        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo) {

        $this->sottotitolo = $sottotitolo;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle() {

        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle) {

        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription() {

        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription) {

        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getSlug() {

        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug) {

        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getTranslatable() {

        return $this->translatable;
    }


}