<?php

namespace AppartamentiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Ramsey\Uuid\Uuid;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppartamentiBundle\Repository\AppartamentoRepository")
 * @ORM\EntityListeners({"AppartamentiBundle\EntityListener\AppartamentoListener"})
 * @ORM\Table(name="appartamenti")
 * @Vich\Uploadable()
 */
class Appartamento
{

    use ORMBehaviours\Loggable\Loggable, ORMBehaviours\Sortable\Sortable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, ORMBehaviours\SoftDeletable\SoftDeletable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppartamentiBundle\Entity\AppartamentoRow", mappedBy="appartamento", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $appartamentiRows;

    /**
     * @ORM\OneToMany(targetEntity="AppartamentiBundle\Entity\AppartamentoAttachment", mappedBy="appartamento", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $attachments;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\ManyToMany(targetEntity="TagBundle\Entity\Tag")
     * @ORM\JoinTable(name="appartamenti_has_servizi",
     *      joinColumns={@ORM\JoinColumn(name="appartamenti_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="servizi_id", referencedColumnName="id")})
     */
    private $servizi;
    private $uuid;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    /**
     * @Vich\UploadableField(mapping="appartamenti", fileNameProperty="listImgFileName")
     */
    private $listImg;

    public $listImgData;
    private $listImgDelete;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgAlt;

    /**
     * @Vich\UploadableField(mapping="appartamenti", fileNameProperty="headerImgFileName")
     */
    private $headerImg;

    public $headerImgData;
    private $headerImgDelete;

    /**
     * @ORM\Column(type="string")
     */
    private $prezzo;

    /**
     * @ORM\Column(type="string")
     */
    private $superficie;

    public function __construct()
    {

        $this->appartamentiRows = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->servizi = new ArrayCollection();
        $this->setUuid(Uuid::uuid1());

    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }


    /**
     * @return ArrayCollection
     */
    public function getAppartamentiRows()
    {

        return $this->appartamentiRows;
    }

    /**
     * Add appartamentoRow
     *
     * @param \AppartamentiBundle\Entity\AppartamentoRow $appartamentoRow
     *
     * @return Appartamento
     */

    public function addAppartamentiRow(\AppartamentiBundle\Entity\AppartamentoRow $appartamentoRow)
    {

        $appartamentoRow->setAppartamento($this);

        $this->appartamentiRows[] = $appartamentoRow;

        return $this;
    }

    /**
     * Remove appartamentoRow
     *
     * @param \AppartamentiBundle\Entity\AppartamentoRow $appartamentoRow
     */
    public function removeAppartamentiRow(\AppartamentiBundle\Entity\AppartamentoRow $appartamentoRow)
    {

        $this->appartamentiRows->removeElement($appartamentoRow);
    }


    public function __toString()
    {

        return (string)$this->translate('it')->getTitolo();
    }

    function getUploadDir()
    {

        return 'files/appartamenti/'.$this->getId().'/';

    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {

        return $this->attachments;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {

        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {

        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getServizi()
    {

        return $this->servizi;
    }

    /**
     * @param mixed $servizi
     */
    public function setServizi($servizi)
    {

        $this->servizi = $servizi;
    }

    /**
     * @return File
     */
    public function getListImg()
    {

        return $this->listImg;
    }

    public function setListImg($listImg = null)
    {

        $this->listImg = $listImg;

        if ($listImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;

    }

    /**
     * @return mixed
     */
    public function getListImgFileName()
    {

        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {

        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {

        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {

        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {

        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {

        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {

        return $this->listImgData;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgFileName()
    {

        return $this->headerImgFileName;
    }

    /**
     * @param mixed $headerImgFileName
     */
    public function setHeaderImgFileName($headerImgFileName)
    {

        $this->headerImgFileName = $headerImgFileName;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgAlt()
    {

        return $this->headerImgAlt;
    }

    /**
     * @param mixed $headerImgAlt
     */
    public function setHeaderImgAlt($headerImgAlt)
    {

        $this->headerImgAlt = $headerImgAlt;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgDelete()
    {

        return $this->headerImgDelete;
    }

    /**
     * @param mixed $headerImgDelete
     */
    public function setHeaderImgDelete($headerImgDelete)
    {

        $this->headerImgDelete = $headerImgDelete;
    }


    /**
     * @return mixed
     */
    public function getHeaderImg()
    {

        return $this->headerImg;
    }

    /**
     * @param mixed $headerImg
     *
     * @return Appartamento
     */
    public function setHeaderImg($headerImg)
    {

        $this->headerImg = $headerImg;

        if ($headerImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgData()
    {

        return $this->headerImgData;
    }

    /**
     * @return mixed
     */
    public function getPrezzo()
    {

        return $this->prezzo;
    }

    /**
     * @param mixed $prezzo
     */
    public function setPrezzo($prezzo)
    {

        $this->prezzo = $prezzo;
    }

    /**
     * @return mixed
     */
    public function getSuperficie()
    {

        return $this->superficie;
    }

    /**
     * @param mixed $superficie
     */
    public function setSuperficie($superficie)
    {

        $this->superficie = $superficie;
    }

}
