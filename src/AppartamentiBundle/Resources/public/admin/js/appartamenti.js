var modulo = 'appartamenti';

$(function() {

    sezione = window.location.getPositionalUrl(2);

    switch (sezione) {

        case 'new':
        case 'edit':
            $(document).on('click', '.btn-remove-service', function(e) {
                e.preventDefault();
                $('#appartamento_form_servizi_' + $(this).data('id')).removeAttr('checked');
                $(this).remove();
            });

            if ($('#servizio').length) {

                $("#servizio").autocomplete({
                    source: function(request, response) {
                        var excluded = [];
                        $('.btn-remove-service').each(function() {
                            excluded.push($(this).data('id'));
                        });

                        $.ajax({
                            url: "/admin/tag/autocomplete-json",
                            dataType: "json",
                            data: {
                                term: request.term,
                                excluded: excluded
                            },
                            success: function(data) {
                                response(data);
                            }
                        });
                    },
                    appendTo: $('#servizio').closest('.form-group'),
                    select: function(event, ui) {
                        var markup = '<a href="#" data-id="' + ui.item.id + '" class="btn-remove-service btn btn-default"><i class="fa fa-trash"></i> ' + ui.item.label + '</a>';
                        $(markup).appendTo('#appartamento_form_servizi');
                        $('#appartamento_form_servizi_' + ui.item.id).prop('checked', true);
                        $('#servizio').val('');
                        return false;

                    }
                });

                $("#servizio").data("ui-autocomplete")._renderMenu = function(ul, items) {
                    var that = this;
                    ul.attr("class", "nav nav-pills nav-stacked dropdown-menu");
                    $.each(items, function(index, item) {
                        that._renderItemData(ul, item);
                    });
                };

                $("#servizio").data("ui-autocomplete")._renderItem = function(ul, item) {
                    return $("<li>")
                    .attr("data-value", item.value)
                    .append('<a>' + item.label + '</a>')
                    .appendTo(ul);
                };


            }

            break;
        default:


            /** Datatable della pagina di elenco */
            if ($('#datatable').length) {

                var cols = [];

                var col = {
                    'title': 'Modifica',
                    'className': 'dt0 dt-body-center',
                    'searchable': false
                };
                cols.push(col);

                col = {
                    'title': 'Titolo',
                    'className': 'dt2',
                    searchable: true,
                    data: 'titolo'
                };

                cols.push(col);

                col = {
                    'title': 'Creato',
                    'className': 'dt6',
                    searchable: true,
                    data: 'createdAt'
                };

                cols.push(col);

                col = {
                    'title': 'Ultima modifica',
                    'className': 'dt7',
                    searchable: true,
                    data: 'updatedAt'
                };

                cols.push(col);

                col = { 'className': 'dt-body-center' };

                // placeholder cancellazione
                cols.push(col);

                var columnDefs = [];

                var columnDef = {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                    }
                };

                columnDefs.push(columnDef);


                columnDef = {
                    targets: cols.length - 1,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        if (full.deleted) {
                            var title = _('Recupera:');
                            var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                            var title = _('Elimina definitivamente: ');
                            ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        } else {
                            var title = 'Elimina: ';
                            var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        }
                        return ret;
                    }
                };

                columnDefs.push(columnDef);


                $('#datatable').dataTable({
                    ajax: {
                        "url": "/admin/" + modulo + "/json"
                    },
                    aaSorting: [[2, 'asc']],
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs,
                    createdRow: function(row, data, index) {
                        if (data.deleted) {
                            $(row).addClass("danger");
                        }
                    }
                });

            }

            break;

    }


});

/** FINE document.ready **/