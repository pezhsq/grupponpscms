var markUpPulsantieraImgOrTxt = false;
var formPrototype = false;
var markUpRows = false;
var warnings = {};
var $form;
var $tree;
$(function() {

    $form = $('form');
    $form.data('warningchecked', false);

    // markup della pulsantiera per la scelta tra immagine e area testuale.
    markUpPulsantieraImgOrTxt = $('#pulsantieraImgOrTxt').html();
    //
    markUpRows = $('#rowsContainer').html();
    formPrototype = $('#formPrototype').html();

    $('div[data-locale]').each(function() {
        $(markUpRows).appendTo(this);
    });

    /** Dopo la scelta del file da caricare viene iniettata l'immagine nel container */
    $(document).on('change', '.uploadImgEntity', function() {
        var $uploadField = $(this);

        readFile(this, function(e) {
            // recupero del container
            var $container = $uploadField.closest('.containerRows');
            // la riga che contiene il container
            var $entityRow = $uploadField.closest('.entityRow');
            // indice del blocco che sto modificando
            var blockIndex = $container.parent().data('index');


            var opts = {
                'id': 'modalImageContainer',
                'onShow': function($modal) {
                    // src dell'immagine appena caricata;
                    var src = e.target.result;
                    $modal.find('img').attr('src', src);

                    // nome del campo hidden che conserverà il valore dell'alt
                    var altName = 'slot' + (blockIndex + 1) + 'ImageAlt';
                    // il campo input che deve conservare l'alt per l'immagine
                    var $inputAlt = $entityRow.find('[id*="' + altName + '"]');
                    // Cerco di comporre un alt automatico

                    // tab in cui mi trovo
                    var $tabPane = $entityRow.closest('.tab-pane');
                    // campo titolo del tabpane
                    var titolo = $tabPane.find('[name*=titolo]').val();
                    if (titolo) {
                        titolo += ': ';
                    }

                    // cambio il type a text
                    $inputAlt.attr('type', 'text');
                    $inputAlt.addClass('form-control');
                    $inputAlt.val(titolo + _('Immagine ' + ($tabPane.find('.img-responsive').length + 1)));
                    // e lo posiziono sotto l'immagine
                    $inputAlt.appendTo($modal.find('.inputContainer'));

                    $modal.find('.btn-confirm').on('click', function(e) {
                        e.preventDefault();
                        if ($.trim($modal.find('input').val()) == '') {
                            HandleBarHelper.alert({ 'content': _('Il campo alt non può essere lasciato vuoto') });
                        } else {
                            // nascondo l'upload field
                            $uploadField.hide();
                            // recupero del campo structure per aggiornarne il contenuto
                            var $structure = $entityRow.find('[id$="structure"]');
                            // json decode del valore
                            var structureVal = JSON.parse($structure.val());
                            // aggiornamento del type
                            structureVal[blockIndex]['type'] = 'img';
                            // re-encode del value
                            structureVal = JSON.stringify(structureVal);
                            // ri-assegnazione del valore che verrà spedito in post
                            $structure.val(structureVal);
                            // il campo upload field viene spostato in una parte del DOM che non subisce modifiche
                            $uploadField.appendTo($entityRow);
                            // svuotamento del container per fare spazio all'immagine
                            $container.empty();
                            // l'immagine viene messa nel container svuotato precedentemente
                            $('<img class="img-responsive" src="' + src + '" />').appendTo($container);
                            // il blocco del form group al campo container
                            var $formGroup = $inputAlt.closest('.form-group');
                            // viene tolto l'help che non serve
                            $formGroup.find('.help').remove();
                            // viene appeso il gruppo al container
                            $formGroup.appendTo($container);

                            $modal.modal('hide');
                            // aggiornamento dell'altezza della riga in modo che i container siano tutti uguali
                            aggiornaAltezzaPageRow($entityRow);
                        }
                    });
                },

            };

            HandleBarHelper.modal(opts);

        });

    });

    $('.titolo').on('change', function(e) {
        if (this.defaultValue != '') {
            var $tabpane = $(this).closest('.tab-pane');
            console.log($tabpane);
            var longLocale = $tabpane.data('longlocale');
            warnings[longLocale] = _('Il cambio del titolo di una pagina determina il cambio dell\'indirizzo per raggiungerla. <br />Il vecchio titolo era <strong>%s</strong> per la lingua %s<br /><strong>Attenzione all\'indicizzazione su Google!</strong>', 'pages', this.defaultValue, longLocale);
        }
    });


    /** Click sul bottone che permette di aggiungere una riga di contenuti **/
    $('.addRow').on('click', function(e) {
        e.preventDefault();
        /** il tab di lingua è stato cliccato il bottone **/
        var $tabPane = $(this).closest('.tab-pane');
        /** la lingua associata **/
        var locale = $tabPane.data('locale');
        /** id del bottone che è stato cliccato **/
        var id = $(this).attr('id');
        /** composizione dell'id contenente il template di griglia **/
        var griglia = id.replace('add', '');
        var htmlBlockId = 'row' + griglia;
        /** markup della griglia **/
        var markUpPageRow = $('#' + htmlBlockId).html();
        /** La nuova riga da aggiungere **/
        var $entityRow = $(markUpPageRow);
        /** Il nuovo index associato, prelevato dal numero di righe già presenti **/
        var indexes = [];

        var ii = 0;
        $('.entityRow').each(function() {
            if (typeof($(this).attr('data-index')) == 'undefined') {
                $(this).attr('data-index', ii);
                ii++;
            }
            indexes.push(parseInt($(this).attr('data-index'), 10));
        });
        var index = Math.max.apply(null, indexes) + 1;
        /** Il form di partenza che poi verrà modificato nel numero di indice utilizzato **/
        var newForm = formPrototype.replace(/__name__/g, index);
        /** Il nuovo form viene appeso alla entityRow **/
        $(newForm).appendTo($entityRow);
        /** id che verrà assegnato alla page row **/
        var idPageRow = 'entityRow_' + locale + '_' + index;
        $entityRow.attr('id', idPageRow);
        $entityRow.attr('data-index', index);
        /** viene aggiunta la pulsantiera per lo spostamento/eliminazione della riga **/
        $('<div class="form-group"></div>' + $('#pulsantiRow').html()).appendTo($entityRow);
        /** La entityRow viene appesa alla tab di lingua, prima della pulsantiera **/
        $entityRow.insertBefore($tabPane.find('.pulsantiera'));
        /** Aggiornamento dello stato delle singole righe **/
        disableInusableButtons();

        var indexBtn;

        /** PageRow Nel dom **/
        var $entityRowDOM = $('#' + idPageRow);


        /** I campi di upload vengono "nascosti" sotto l'icona dell'immagine per ogni blocco disponibile **/
        $entityRowDOM.find('.containerRows').each(function(indexBottone) {
            var $markUpPulsantieraImgOrTxt = $(markUpPulsantieraImgOrTxt);
            indexBtn = indexBottone + 1;
            $('#entityRow_' + index + '_slot' + indexBtn + 'ImageFile').appendTo($markUpPulsantieraImgOrTxt.find('.btn-img'));
            $markUpPulsantieraImgOrTxt.appendTo($(this));
        });

        $($('#pulsantiContainer').html()).insertAfter($entityRowDOM.find('.containerRows'));

        /** I campi upload non necessari vengono rimossi dal DOM **/
        for(var i = indexBtn + 1; i <= 3; i++) {
            // console.log('Rimuovo ' + 'entityRow' + index + '_slot' + i + 'ImageFile');
            $('#entityRow_' + index + '_slot' + i + 'ImageFile').remove();
        }

        /** modifica della struttura per rispecchiare il template scelto **/
        var structure = [];

        for(var i = 0, len = griglia.length; i < len; i++) {
            var column = {
                size: parseInt(griglia[i]),
                'type': '-'
            };
            if (parseInt(griglia[i]) == 0) {
                column['type'] = null;
            }
            structure.push(column);
        }

        /** Assegnazione del valore della structure */
        structure = JSON.stringify(structure);
        $entityRowDOM.find('#entityRow_' + index + '_structure').val(structure);
        /** Assegnazione del locale nel form di riga **/
        $entityRowDOM.find('#entityRow_' + index + '_locale').val(locale);
        /** Assegnazione del campo sort per indicare l'ordine di visualizzazione **/
        $entityRowDOM.find('#entityRow_' + index + '_sort').val(index);

    });


    $('.btn-main-submit').on('click', function(e) {
        e.preventDefault();

        checkForm(function() {
            if (checkWarnings()) {
                $form.submit();
            }
        });

    });

    $(document).on('click', '.btn-ck', function(e) {
        e.preventDefault();
        /** La entityRow in cui è presente il bottone che è stato cliccato **/
        var $entityRow = $(this).closest('.entityRow');
        /** Il blocco tratteggiato che contiene il bottone appena cliccato **/
        var $container = $(this).closest('.containerRows');
        /** L'indice di posizione rispetto agli altri blocchi per il blocco contenitore **/
        var index = $container.parent().data('index');
        // console.log('Index: ' + index);

        // svuoto il contenitore per poterci mettere la textarea
        $container.empty();

        // Viene prelevata la textarea da spostare
        var $itemToMove = $entityRow.find('[id$="slot' + (index + 1) + '"]');
        // viene aggiunta la classe ck (da ckeditor)
        $itemToMove.addClass('ck').removeClass('hide');

        // viene prelevato il valore del campo in json che contiene la struttura della riga
        var $structure = $entityRow.find('[id$="structure"]');
        // ne viene fatto il parsing
        var structureVal = JSON.parse($structure.val());
        // viene modificato il tipo ck
        structureVal[index]['type'] = 'ck';
        // viene encodato nuovamente
        structureVal = JSON.stringify(structureVal);
        // e riassegnato al campo
        $structure.val(structureVal);

        // l'item viene aggiunto al container
        $itemToMove.appendTo($container);

        aggiornaAltezzaPageRow($entityRow);

        CKEDITOR.replace($itemToMove.get(0), {
            customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
        });

        CKEDITOR.instances[$itemToMove.attr('id')].on('instanceReady', function(e) {
            h = $('#' + $itemToMove.attr('id')).closest('.containerRows').height();
            e.editor.resize('100%', h);
        });


    });


    /** disabilitazione del bottone UP per il primo elemento delle entityRows e del bottone DOWN per l'ultima riga */
    disableInusableButtons();

    /** click sul bottone che permette di spostare alla riga precedente a quella selezionata */
    $(document).on('click', '.btn-row-up', function(e) {
        e.preventDefault();
        var $thisPageRow = $(this).closest('.entityRow');
        var $elementoPrecedente = $thisPageRow.prev();
        $thisPageRow.find('.ck').each(function() {
            CKEDITOR.instances[this.id].destroy();
        });
        $thisPageRow.insertBefore($elementoPrecedente);
        $thisPageRow.find('.ck').each(function() {
            CKEDITOR.replace(this, {
                customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
            });
            CKEDITOR.instances[this.id].on('instanceReady', function(e) {
                h = $('#' + e.editor.name).closest('.containerRows').height();
                e.editor.resize('100%', h);
            });
        });
        disableInusableButtons();
    });

    /** click sul bottone che permette di spostare alla riga successiva a quella selezionata */
    $(document).on('click', '.btn-row-down', function(e) {
        e.preventDefault();
        var $thisPageRow = $(this).closest('.entityRow');
        var $elementoPrecedente = $thisPageRow.next();
        $thisPageRow.find('.ck').each(function() {
            CKEDITOR.instances[this.id].destroy();
        });
        $thisPageRow.insertAfter($elementoPrecedente);
        $thisPageRow.find('.ck').each(function() {
            CKEDITOR.replace(this, {
                customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
            });
            CKEDITOR.instances[this.id].on('instanceReady', function(e) {
                h = $('#' + e.editor.name).closest('.containerRows').height();
                e.editor.resize('100%', h);
            });
        });
        disableInusableButtons();
    });

    /** Viene creato un editor per ogni textarea che ha la classe .ck **/

    /** Per ogni istanza dell'editor viene aumentata l'altezza in base all'altezza del contenitore */
    if (typeof(CKEDITOR) !== 'undefined') {
        for(var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].on('instanceReady', function(e) {
                console.log(i);
                h = $('#' + i).closest('.containerRows').height();
                e.editor.resize('100%', h);
            });
        }
    }

    /** Click sul bottone che permette di eliminare il contenuto del container e ritornare alla situazione iniziale **/
    $(document).on('click', '.btn-reset-container', function(e) {

        $this = $(this);

        e.preventDefault();

        var opts = {
            'type': 'danger',
            'titolo': _('Attenzione'),
            'content': _('Vuoi eliminare il contenuto di questo blocco?'),
            'OK': _('Ok'),
            'CANCEL': _('Annulla'),
            'onOK': function($modal) {
                // questo è il box tratteggiato che potrà contenere un'immagine o un testo
                var $container = $this.parent().find('.containerRows');
                // questo è l'indice del container (praticamente un indice da 1 a 3 numerato da sinistra a destra
                var indexContainer = $container.parent().data('index') + 1;
                // questo è il contenitore della riga (l'elemento tratteggiato sottile con sfondo colorato
                var $entityRow = $this.closest('.entityRow');

                var $textarea = $container.find('textarea');
                // se al click del cestino il contenuto era una textarea
                if ($container.find('textarea').lenght) {
                    if (CKEDITOR.instances[$textarea.attr('id')]) {
                        CKEDITOR.instances[$textarea.attr('id')].destroy();
                    }
                } else {

                    $container.empty();
                }
                $textarea.remove();

                $container.height('auto');

                // recupero dell'indice partendo da un input che è già presente in riga
                var re = /\[([\d])\]/;
                var indexRiga = re.exec($entityRow.find('input:first').attr('name'))[1];
                var $formPrototype = $('<div>' + $('#formPrototype').html() + '</div>');

                // viene prelevato il campo input file da aggiungere all'icona dal prototype, cambiando nome e id
                var $inputImmagine = $formPrototype.find('[name*="slot' + indexContainer + 'ImageFile"]');
                $inputImmagine.attr('name', $inputImmagine.attr('name').replace('__name__', indexRiga));
                $inputImmagine.attr('id', $inputImmagine.attr('id').replace('__name__', indexRiga));

                // box con le 2 icone per scegleire testo o immagine
                $pulsantieraImgOrTxt = $($('#pulsantieraImgOrTxt').html());
                $inputImmagine.appendTo($pulsantieraImgOrTxt.find('.btn-img'));
                $pulsantieraImgOrTxt.appendTo($container);

                // textarea da posizionare nella pagerow in modo da essere pronta al click dell'icona testuale
                var $inputTextarea = $formPrototype.find('textarea[name*="slot' + indexContainer + '"]');
                $inputTextarea.attr('name', $inputTextarea.attr('name').replace('__name__', indexRiga));
                $inputTextarea.attr('id', $inputTextarea.attr('id').replace('__name__', indexRiga));
                // siccome potrei aver cancellato diverse volte, la textarea potrebbe già esserci, controllo prima di inserirla.
                if (!$entityRow.find('#' + $inputTextarea.attr('id')).length) {
                    $inputTextarea.appendTo($entityRow);
                }

                // blocco testuale per il testo alternativo in modo da esser pronto in caso di upload
                var $inputAlt = $formPrototype.find('input[name*="slot' + indexContainer + 'ImageAlt"]');
                $inputAlt.attr('name', $inputAlt.attr('name').replace('__name__', indexRiga));
                $inputAlt.attr('id', $inputAlt.attr('id').replace('__name__', indexRiga));
                if (!$entityRow.find('#' + $inputAlt.attr('id')).length) {
                    $inputAlt.appendTo($entityRow.find('div.hide'));
                }

                $modal.modal('hide');

                aggiornaAltezzaPageRow($entityRow);
            }
        };
        HandleBarHelper.confirm(opts);

    });

    /** Click sul bottone che elimina la riga **/
    $(document).on('click', '.btn-delete-pagerow', function(e) {
        $this = $(this);

        $entityRow = $(this).closest('.entityRow');

        e.preventDefault();

        var opts = {
            'type': 'danger',
            'titolo': _('Attenzione'),
            'content': _('Vuoi eliminare il contenuto di questa riga?', 'pages'),
            'OK': _('Ok'),
            'CANCEL': _('Annulla'),
            'onOK': function($modal) {
                var $tabPane = $entityRow.closest('.tab-pane');

                $entityRow.remove();
                if ($tabPane.find('.entityRow').length == 1) {
                    $tabPane.find('.pulsantiera-row .btn').addClass('disabled');
                }
                $modal.modal('hide');
            }
        };

        HandleBarHelper.confirm(opts);

    });

    /**
     * Dopo l'attivazione di una tab viene istanziato il ck editor per le aree testuali già disponibili,
     * nel caso non ci sia nulla aggiunge una riga vuota.
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var $tabPane = $($(e.target).data("target"));
        if (!$tabPane.find('.entityRow').length) {
            addEmptyRow($tabPane);
        } else {
            $tabPane.find('.ck').each(function() {
                attivaCkEditor(this);
            });
        }
    });

    /** Aggiunta di una riga vuota nel caso il tabpane attivo non abbia righe associate **/
    if ($('.tab-pane.active').length && !$('.tab-pane.active').find('.entityRow').length) {
        addEmptyRow($('.tab-pane.active'));
    }

    /** Aggiunge le istanze di ckeditor per la tab attiva al caricamento **/
    if ($('.tab-pane.active').length) {
        var $tabPane = $('.tab-pane.active');
        $tabPane.find('.ck').each(function() {
            attivaCkEditor(this);
        });
    }

    /** UPLOAD HEADER **/

    $('#upload_header').on('change', function() {
        var dimensioni = $(this).closest('.panelImg').data('dimensions');
        readFileB64(this, dimensioni);
    });

    $('#upload_list').on('change', function() {
        var dimensioni = $(this).closest('.panelImg').data('dimensions');
        readFileB64(this, dimensioni, 'elenchi');
    });

    $('.btn-delete-header-img').on('click', function(e) {
        e.preventDefault();
        var opts = {
            'type': 'danger',
            'titolo': _('Cancellazione immagine header', 'pages'),
            'content': _('Vuoi eliminare l\'immagine di header?<br /><strong>N.B.<br />L\'operazione sarà irreversibile</strong>'),
            'onOK': function($modal) {
                $('#headerImg').find('img').remove();
                $('#headerImg').find('.form-group').parent().addClass('hide');
                $('#appartamento_form_headerImg').val('');
                $('#appartamento_form_headerImgAlt').val('');
                $('#appartamento_form_headerImgFileName').val('');
                $('#page_header_img').find('.missingImage').removeClass('hide');
                $('#appartamento_form_deleteHeaderImg').val(1);
                $modal.modal('hide');
            }
        };
        HandleBarHelper.confirm(opts);
    });

    $('.btn-delete-list-img').on('click', function(e) {
        e.preventDefault();
        var opts = {
            'type': 'danger',
            'titolo': _('Cancellazione immagine elenchi', 'pages'),
            'content': _('Vuoi eliminare l\'immagine per gli elenchi?<br /><strong>N.B.<br />L\'operazione sarà irreversibile</strong>'),
            'onOK': function($modal) {
                $('#listImg').find('img').remove();
                $('#listImg').find('.form-group').remove();
                $('#appartamento_form_listImg').val('');
                $('#appartamento_form_listImgFileName').val('');
                $('#page_list_img').find('.missingImage').removeClass('hide');
                $('#appartamento_form_deleteListImg').val(1);
                $modal.modal('hide');
            }
        };
        HandleBarHelper.confirm(opts);
    });


});
/** FINE document.ready **/

/**----------**/
/** FUNZIONI **/
/**----------**/
function readFile(input, cb) {
    if (input.files && input.files[0]) {

        if (input.files[0].size > $body.data('max_upload_file_size')) {

            var opts = {
                'type': 'danger',
                'content': _('Non posso permetterti di caricare un file cosi grande (%s)', 'default', humanFileSize(input.files[0].size))
            };
            HandleBarHelper.alert(opts);

        } else {

            var reader = new FileReader();

            reader.onload = function(e) {
                cb(e);
            };

            reader.readAsDataURL(input.files[0]);

        }
    } else {
        var opts = {
            'type': 'danger',
            'content': _('Il tuo browser non supporta le funzionalità')
        };
        HandlerbarHelper.alert(opts);
    }
}

$(window).load(function() {
    /**
     * Dopo il caricamento il contenitore potrebbe variare d'altezza in base al contenuto,
     * con questo codice viene uniformata l'altezza di ogni container al valore più alto
     */
    $('.entityRow').each(function() {
        aggiornaAltezzaPageRow($(this));
    });

});


/**
 * Per ogni tabpane toglie la possibilità di cliccare il bottone Su per la prima riga, e il bottone
 * giu per l'ultima riga.
 * A termine operazioni viene aggiornato il valore del sorting (questa funzione viene richiamata dopo
 * ogni spostamento di riga).
 */
function disableInusableButtons() {
    $('.tab-pane').each(function() {
        var $tabPane = $(this);
        if ($tabPane.find('.entityRow').length > 1) {
            $tabPane.find('.entityRow').find('.pulsantiera-row .btn').removeClass('disabled');

            $tabPane.find('.entityRow:first').find('.pulsantiera-row .btn-row-up').addClass('disabled');
            $tabPane.find('.entityRow:last').find('.pulsantiera-row .btn-row-down').addClass('disabled');
        } else {
            $tabPane.find('.entityRow .pulsantiera-row .btn').attr('disabled');
        }
    });


    refreshSort();
}

/**
 * Cicla le tab, e per ogni riga aggiorna il valore del campo sort in base alla posizione attuale
 * della riga.
 */
function refreshSort() {
    $('.tab-pane').each(function() {
        $(this).find('.entityRow .sortField').each(function(index) {
            $(this).val(index);
        });
    });
}

/**
 * Passato un oggetto jquery $('.entityRow'), calcola l'altezza massima del container
 * per poi decidere il valore da applicare a tutti i container della riga
 * @param $entityRow
 */
function aggiornaAltezzaPageRow($entityRow) {

    $entityRow.find('.containerRows').each(function() {
        $(this).css('height', 'auto');
    });

    var heights = [];

    $entityRow.find('.containerRows').each(function() {
        heights.push($(this).height());
    });

    var h = Math.max.apply(null, heights);

    $entityRow.find('.containerRows').each(function() {
        $(this).height(h);
    });

}

/**
 * Passato un tabpane aggiunge una riga vuota a tutta larghezza con il ckeditor già disponibile
 * @param $tabPane
 */
function addEmptyRow($tabPane) {
    /** markup della griglia **/
    var markUpPageRow = $('#row300').html();
    /** la lingua associata **/
    var locale = $tabPane.data('locale');
    /** La nuova riga da aggiungere **/
    var $entityRow = $(markUpPageRow);
    /** Il nuovo index associato, prelevato dal numero di righe già presenti **/
    var index = $('.entityRow').length;
    /** Il form di partenza che poi verrà modificato nel numero di indice utilizzato **/
    var newForm = formPrototype.replace(/__name__/g, index);
    /** Il nuovo form viene appeso alla entityRow **/
    $(newForm).appendTo($entityRow);
    /** id che verrà assegnato alla page row **/
    var idPageRow = 'entityRow_' + locale + '_' + index;
    $entityRow.attr('id', idPageRow);
    /** La entityRow viene appesa alla tab di lingua, prima della pulsantiera **/
    $entityRow.insertBefore($tabPane.find('.pulsantiera'));
    /** I campi upload non necessari vengono rimossi dal DOM **/

    /** PageRow Nel dom **/
    var $entityRowDOM = $('#' + idPageRow);


    for(var i = 1; i <= 3; i++) {
        console.log('Rimuovo ' + 'entityRow' + index + '_slot' + i + 'ImageFile');
        $('#entityRow_' + index + '_slot' + i + 'ImageFile').remove();
    }
    $($('#pulsantiContainer').html()).insertAfter($entityRowDOM.find('.containerRows'));

    /** modifica della struttura per rispecchiare il template scelto **/
    var structure = [{ size: 3, type: 'ck' }, { size: 0, type: null }, { size: 0, type: null }];

    $('<div class="form-group"></div>' + $('#pulsantiRow').html()).appendTo($entityRowDOM);

    $entityRowDOM.find('.pulsantiera-row').find('.btn').addClass('disabled');

    /** Assegnazione del valore della structure */
    structure = JSON.stringify(structure);
    $entityRowDOM.find('#entityRow_' + index + '_structure').val(structure);
    /** Assegnazione del locale nel form di riga **/
    $entityRowDOM.find('#entityRow_' + index + '_locale').val(locale);
    /** Assegnazione del campo sort per indicare l'ordine di visualizzazione **/
    $entityRowDOM.find('#entityRow_' + index + '_sort').val(index);

    // Viene prelevata la textarea da spostare
    var $itemToMove = $entityRowDOM.find('[id$="slot' + (index + 1) + '"]');
    $itemToMove.removeClass('hide');

    $container = $entityRowDOM.find('.containerRows');

    // l'item viene aggiunto al container
    $itemToMove.appendTo($container);

    CKEDITOR.replace($itemToMove.get(0), {
        customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
    });

    CKEDITOR.instances[$itemToMove.attr('id')].on('instanceReady', function(e) {
        h = $container.height();
        e.editor.resize('100%', h);
    });

}

/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {

    var $textarea = $(textarea);
    var $container = $textarea.parent();

    CKEDITOR.replace(textarea, {
        customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
    });

    var h = $container.height();

    CKEDITOR.instances[textarea.id].on('instanceReady', function(e) {
        e.editor.resize('100%', h);
    });

}

/**
 * Riceve in ingresso il file selezionato per l'upload
 */
function readFileB64(input, dimensioni, type) {

    if (typeof type == 'undefined') {
        type = 'header';
    }

    console.log(dimensioni);

    var d = dimensioni.split('x');

    if (input.files && input.files[0]) {

        if (input.files[0].size > $body.data('max_upload_file_size')) {

            var opts = {
                'type': 'danger',
                'titolo': _('Dimensioni file non consentite'),
                'content': _('Non posso permetterti di caricare un file cosi grande (%s), <strong>il massimo consentito è %s</strong>', 'default', humanFileSize(input.files[0].size, true), humanFileSize(1000000, true))
            };
            HandleBarHelper.alert(opts);

        } else {

            var modalTitle;

            if (type == 'header') {

                modalTitle = _('Caricamento immagine di testata', 'pages');
                defaultAlt = 'Immagine Header';

            } else {

                modalTitle = _('Caricamento immagine per gli elenchi', 'pages');
                defaultAlt = 'Immagine Elenchi';

            }

            var reader = new FileReader();

            reader.onload = function(e) {

                var opts = {
                    'id': 'modalCroppie',
                    'data': { 'title': modalTitle },
                    'onShow': function($modal) {

                        if (type == 'header') {
                            $modal.find('#appartamento_form_listImgAlt').closest('.form-group').remove();
                        } else {
                            $modal.find('#appartamento_form_headerImgAlt').closest('.form-group').remove();
                        }

                        var w = d[0] / 2;
                        var h = d[1] / 2;

                        var alt = '';

                        $('.titolo').each(function() {
                            if ($.trim($(this).val()) != '') {
                                alt += $.trim($(this).val());
                                alt += ': '
                                return false;
                            }
                        });

                        alt += defaultAlt;

                        $modal.find('input[type="text"]').val(alt);

                        $('.croppie').height(h + h * (20 / 100));

                        var croppieOpts = {
                            viewport: {
                                width: w,
                                height: h,
                                type: 'square'
                            },
                            boundary: {
                                width: w,
                                height: h
                            },
                            enableExif: true
                        };

                        console.log(croppieOpts);

                        var $croppie = $modal.find('.croppie').croppie(croppieOpts);

                        $croppie.croppie('bind', {
                            url: e.target.result
                        });

                        $modal.find('.btn-cancel').on('click', function(e) {
                            $(input).val('');
                            $modal.modal('hide');
                        });

                        $modal.find('.btn-confirm').on('click', function(e) {
                            e.preventDefault();
                            $croppie.croppie('result', {
                                type: 'base64',
                                size: { width: d[0], height: d[1] },
                                format: 'jpeg'
                            }).then(function(resp) {

                                var AltImpostato = $modal.find('#alt').val();

                                if (type == 'header') {
                                    $('#appartamento_form_headerImg').val(resp);
                                    var $headerImg = $('#headerImg');
                                    $headerImg.find('.missingImage').addClass('hide');
                                    $headerImg.find('.img-responsive').remove();
                                    $('#appartamento_form_deleteHeaderImg').val('0');
                                    $('.btn-delete-header-img').removeClass('disabled');
                                    $('<img class="img-responsive" src="' + resp + '" />').prependTo($('#headerImg'));
                                    $('#appartamento_form_headerImgAlt').closest('.form-group').parent().removeClass('hide');
                                    $('#appartamento_form_headerImgAlt').val(AltImpostato);
                                    $('#appartamento_form_headerImgFileName').val(input.files[0].name);
                                } else {
                                    $('#appartamento_form_listImg').val(resp);
                                    var $listImg = $('#listImg');
                                    $listImg.find('.missingImage').addClass('hide');
                                    $listImg.find('.img-responsive').remove();
                                    $('#appartamento_form_deleteListImg').val('0');
                                    $('.btn-delete-list-img').removeClass('disabled');
                                    $('<img class="img-responsive" src="' + resp + '" />').prependTo($('#listImg'));
                                    $('#appartamento_form_listImgAlt').closest('.form-group').parent().removeClass('hide');
                                    $('#appartamento_form_listImgAlt').val(AltImpostato);
                                    $('#appartamento_form_listImgFileName').val(input.files[0].name);
                                }

                                $croppie.croppie('destroy');
                                $modal.modal('hide');

                            });
                        });
                    }
                };
                HandleBarHelper.modal(opts);
            }

            reader.readAsDataURL(input.files[0]);

        }
    }
    else {
        HandleBarHelper.alert({ 'content': _('Il tuo browser non supporta le funzionalità richieste da questo componente.') });
    }
}


/**
 * Riceve in ingresso il file selezionato per l'upload
 */
function readListFile(input, dimensioni) {

    var d = dimensioni.split('x');

    if (input.files && input.files[0]) {

        if (input.files[0].size > $body.data('max_upload_file_size')) {

            var opts = {
                'type': 'danger',
                'titolo': _('Dimensioni file non consentite'),
                'content': _('Non posso permetterti di caricare un file cosi grande (%s), <strong>il massimo consentito è %s</strong>', 'default', humanFileSize(input.files[0].size, true), humanFileSize(1000000, true))
            };
            HandleBarHelper.alert(opts);

        } else {


            var reader = new FileReader();

            reader.onload = function(e) {

                var opts = {
                    'id': 'modalCroppie',
                    'data': { 'title': _('Caricamento immagine per gli elenchi', 'pages') },
                    'onShow': function($modal) {

                        var w = d[0] / 2;
                        var h = d[1] / 2;

                        var alt = '';

                        $('.titolo').each(function() {
                            if ($.trim($(this).val()) != '') {
                                alt += $.trim($(this).val());
                                return false;
                            }
                        });

                        alt += ': Immagine Elenchi';

                        $modal.find('input[type="text"]').val(alt);

                        $('.croppie').height(h + h * (20 / 100));

                        var croppieOpts = {
                            viewport: {
                                width: w,
                                height: h,
                                type: 'square'
                            },
                            boundary: {
                                width: w,
                                height: h
                            },
                            enableExif: true
                        };

                        console.log(croppieOpts);

                        var $croppie = $modal.find('.croppie').croppie(croppieOpts);

                        $croppie.croppie('bind', {
                            url: e.target.result
                        });

                        $modal.find('.btn-confirm').on('click', function(e) {
                            e.preventDefault();
                            $croppie.croppie('result', {
                                type: 'base64',
                                size: { width: d[0], height: d[1] },
                                format: 'jpeg'
                            }).then(function(resp) {
                                $croppie.croppie('destroy');
                                $modal.modal('hide');
                            });
                        });
                    }
                };
                HandleBarHelper.modal(opts);
            }

            reader.readAsDataURL(input.files[0]);

        }
    } else {
        HandleBarHelper.alert({ 'content': _('Il tuo browser non supporta le funzionalità richieste da questo componente.') });
    }
}

/**
 * La funzione fa il submit solo dopo aver verificato che non ci siano warning da mostrare
 * Questa funzione deve essere richiamata SOLO quando si è sicuri che il form non contiene errori;
 */
function checkWarnings() {
    var warn = [];

    for(i in warnings) {
        warn.push(warnings[i]);
    }

    if ($form.data('warningchecked')) {
        return true;
    }

    if (!$form.data('warningchecked')) {


        if (warn.length) {

            var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': warn.join('<br />'),
                'OK': _('Sono consapevole, procedi lo stesso'),
                'CANCEL': _('Annulla'),
                'onOK': function($modal) {
                    $modal.modal('hide');
                    $form.submit();
                },
                'onCANCEL': function($modal) {
                    $modal.modal('hide');
                    $form.data('warningchecked', false);

                }
            };
            HandleBarHelper.confirm(opts);

            return false;
        }

    }

    return true;

}


function checkForm(callback) {

    var errors = [];

    /** Verifico quali entityRows sono state lasciate incomplete **/
    $('.a2lix_translationsFields').children('div').each(function() {
        var longlocale = $(this).data('longlocale');
        if ($(this).find('.btn-ck').length) {
            var singolare = ($(this).find('.btn-ck').length == 1);
            var errore = false;
            if (singolare) {
                errore = _('1 blocco nella scheda "%s" non è stato compilato', 'pagine', longlocale);
            } else {
                errore = _('%d blocchi nella scheda "%s" non sono stati compilati', 'pagine', $(this).find('.btn-ck').length, longlocale);
            }
            errors.push(errore);
        }
    });

    /** Controllo se nelle page rows esiste qualche immagine il cui campo alt è stato lasciato vuoto **/
    $('.altTXT:visible').each(function() {
        if ($.trim($(this).val()) == '') {
            var fieldLabel = $(this).closest('.form-group').find('label').text();
            re = /\[([^\]]+)\]/;
            var fieldName = re.exec($(this).attr('name'))[1];
            errors.push(_('Il campo %s (%s) non può essere lasciato vuoto', 'pages', fieldLabel, fieldName));
        }
    });

    var AltVuoti = $('.input-alt').filter(function() {
        return $(this).val() == "";
    }).length;

    if (AltVuoti > 0) {
        errors.push(_('%d testi alternativi mancanti per gli allegati', 'pages', AltVuoti));
    }

    /** Se non ci sono errori vado avanti, altrimenti mi fermo e mostro i warning **/
    if (!errors.length) {

        /** Se ci sono degli allegati non salvati provo a fare il submit automatico via ajax prima di fare
         * il submit vero e proprio del form principale.
         * Il tutto funziona grazie ad una funzioen che accetta una callback, in cui decremento il numero
         * degli allegati non salvati fino ad arrivare a zero .
         */
        var contaAllegatiNonSalvati = $('.unsaved').length;

        if (contaAllegatiNonSalvati) {

            $('.unsaved').each(function() {

                /** Riga contenitore **/
                var $tr = $(this).closest('tr');
                /** Determina quale indirizzo verrà chiamato per la cancellazione **/
                var controller = $tr.data('controller');
                /** Id dell'elemento da modificare **/
                var id = $tr.data('id');
                /** Viene reperito il nome file direttamente dal DOM **/
                var nomefile = $tr.find('.filename').text();

                sendAltData(id, controller, $tr, nomefile, function() {

                    contaAllegatiNonSalvati--;

                    if (!contaAllegatiNonSalvati) {
                        if ($('#submitFiles:visible').length) {
                            sendFilesToServer(function() {
                                callback();
                            });
                        } else {
                            callback();
                        }
                    }
                });

            });

        }

        if ($('#submitFiles:visible').length) {
            sendFilesToServer(function() {
                callback();
            });
        } else {
            callback();
        }

    } else {

        if (errors.length) {
            var alertOptions = {
                'titolo': _('Attenzione'),
                'content': errors.join('<br />'),
                'OK': _('Ok'),
                'callback': null
            };
            HandleBarHelper.alert(alertOptions);
        }

    }

}
