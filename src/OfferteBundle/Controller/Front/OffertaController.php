<?php
// 17/03/17, 12.15
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Controller\Front;


use OfferteBundle\Entity\Offerta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class OffertaController extends Controller
{

    /**
     * @Route("/offerte/{slug}", defaults={"_locale"="it"}, name="offerta_it")
     * @Route("/{_locale}/offerte/{slug}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl"}, defaults={"_locale"="it"}, name="offerta")
     */
    public function readAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $OffertaTranslation = $em->getRepository('OfferteBundle:OffertaTranslation')->findOneBy(
            ['slug' => $request->get('slug')]
        );

        $pathManager = $this->get('app.path_manager');

        if ($OffertaTranslation) {

            /**
             * @var $Offerta Offerta
             */
            $Offerta = $OffertaTranslation->getTranslatable();

            if ($Offerta && $Offerta->getIsEnabled()) {

                $TemplateLoader = $this->get('app.template_loader');
                $Languages = $this->get('app.languages');

                $AdditionalData = [];
                $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
                $AdditionalData['Entity'] = $Offerta;


                $META = [];
                $META['title'] = $Offerta->translate($request->getLocale())->getMetaTitle();
                $META['description'] = $Offerta->translate($request->getLocale())->getMetaDescription();

                $META = array_merge($META, $this->get('app.open_graph')->generateOpenGraphData($Offerta));


                foreach ($AdditionalData['langs'] as $sigla => $estesa) {

                    if ($Offerta->getIsEnabled()) {
                        $params = ['slug' => $Offerta->translate($sigla)->getSlug()];
                        $META['alternate'][$sigla] = $pathManager->generate('offerta', $params, $sigla, true);
                    }

                }

                $AdditionalData['META'] = $META;

                $twigs = $TemplateLoader->getTwigs('offerta', $Languages->getActivePublicLanguages(), $AdditionalData);


                return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

            }

        }

        throw new NotFoundHttpException("Pagina non trovata");

    }

}
