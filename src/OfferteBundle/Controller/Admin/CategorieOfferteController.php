<?php
// 09/01/17, 16.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Controller\Admin;

use OfferteBundle\Entity\OffertaCategory;
use OfferteBundle\Form\CategorieOffertaForm;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_OFFERTE')")
 */
class CategorieOfferteController extends Controller
{

    /**
     * @Route("/offertecat", name="offertecat")
     */
    public function listAction()
    {

        return $this->render('OfferteBundle:categorie:list.html.twig');

    }

    /**
     * @Route("/offertecat/new", name="offertecat_new")
     */
    public function newAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(
            CategorieOffertaForm::class,
            null,
            ['langs' => $langs, 'layout' => $this->getParameter('generali')['layout']]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $OffertaCategory OffertaCategory
             */

            $OffertaCategory = $form->getData();


            $em = $this->getDoctrine()->getManager();
            $em->persist($OffertaCategory);
            $em->flush();

            $elemento = '"'.$OffertaCategory->translate('it')->getTitolo().'" ('.$OffertaCategory->getId().')';

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.creata'));

            return $this->redirectToRoute('offertecat');

        }

        return $this->render('OfferteBundle:categorie:new.html.twig', ['OffertaCatForm' => $form->createView()]);

    }

    /**
     * @Route("/offertecat/toggle-enabled/{id}", name="offertecat_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, OffertaCategory $offertaCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $translator = $this->get('translator');

        $elemento = '"'.$offertaCategory->translate($request->getLocale())->getTitolo().'" ('.$offertaCategory->getId(
            ).')';

        if (!$offertaCategory->getIsEnabled() && $em->getRepository('OfferteBundle:OffertaCategory')->countAllActive(
            ) >= $this->getParameter('blog') ['max_active_category']) {

            $this->addFlash(
                'error',
                $translator->trans(
                    'offertacat.messages.cant_enable_more',
                    ['howmany' => $this->getParameter('max_active_category')]
                )
            );

            return $this->redirectToRoute('offertacat');

        }

        $flash = 'Categoria '.$elemento.' ';

        if ($offertaCategory->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $offertaCategory->setIsEnabled(!$offertaCategory->getIsEnabled());

        $em->persist($offertaCategory);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('offertecat');


    }


    /**
     * @Route("/offertecat/edit/{id}", name="offertecat_edit")
     */
    public function editAction(Request $request, OffertaCategory $offertaCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(
            CategorieOffertaForm::class,
            $offertaCategory,
            ['langs' => $langs, 'layout' => $this->getParameter('generali')['layout']]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($offertaCategory);
            $em->flush();

            $elemento = '"'.$offertaCategory->translate($request->getLocale())->getTitolo(
                ).'" ('.$offertaCategory->getId().')';

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria '.$elemento.' '.$translator->trans('default.labels.modificata'));

            return $this->redirectToRoute('offertecat');

        }

        return $this->render('OfferteBundle:categorie:new.html.twig', ['OffertaCatForm' => $form->createView()]);

    }


    /**
     * @Route("/offertecat/json", name="offertecat_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $CategorieOfferteList = $em->getRepository('OfferteBundle:OffertaCategory')->findAll();
        } else {
            $CategorieOfferteList = $em->getRepository('OfferteBundle:OffertaCategory')->findAllNotDeleted();
        }

        $retData = [];

        $vetrine = [];

        foreach ($CategorieOfferteList as $OffertaCategory) {
            /**
             * @var $OfferteCategory OfferteCategory
             */
            $record = [];
            $record['id'] = $OffertaCategory->getId();
            $record['titolo'] = $OffertaCategory->translate($request->getLocale())->getTitolo();
            $record['template'] = $OffertaCategory->getTemplate();
            $record['deleted'] = $OffertaCategory->isDeleted();
            $record['isEnabled'] = $OffertaCategory->getIsEnabled();
            $record['createdAt'] = $OffertaCategory->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $OffertaCategory->getUpdatedAt()->format('d/m/Y H:i:s');

            $vetrine[] = $record;
        }

        $retData['data'] = $vetrine;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/offertecat/restore/{id}", name="offertecat_restore")
     */
    public function restoreAction(Request $request, OffertaCategory $offertaCategory)
    {

        if ($offertaCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = '"'.$offertaCategory->translate($request->getLocale())->getTitolo(
                ).'" ('.$offertaCategory->getId().')';

            $offertaCategory->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria '.$elemento.' '.$translator->trans('default.labels.ripristinata'));

        }

        return $this->redirectToRoute('offertecat');

    }


    /**
     * @Route("/offertecat/delete/{id}/{force}", name="offertecat_delete",  requirements={"id" = "\d+"},
     *     defaults={"force" = false}))
     */
    public function deleteAction(Request $request, OffertaCategory $offertaCategory)
    {

        $elemento = '"'.$offertaCategory->translate($request->getLocale())->getTitolo().'" ('.$offertaCategory->getId(
            ).')';

        $translator = $this->get('translator');


        $em = $this->getDoctrine()->getManager();
        if ($offertaCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }

            }

            // remove the entity
            $em->remove($offertaCategory);

            try {

                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));


            } catch (ForeignKeyConstraintViolationException $e) {

                $this->addFlash(
                    'error',
                    'Categoria "'.$elemento.'" '.$translator->trans('offerta.errors.non_cancellabile')
                );

            }


        } elseif (!$offertaCategory->isDeleted()) {

            $offertaCategory->setIsEnabled(0);
            $em->flush();

            $em->persist($offertaCategory);

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));

            $em->remove($offertaCategory);
            $em->flush();


        }


        return $this->redirectToRoute('offertecat');

    }

    /**
     * @Route("/offertecat/sort", name="offertecat_sort")
     */
    public function sortAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $trans = $this->get('translator');

        $categorie = [];

        $Categories = $em->getRepository('OfferteBundle:OffertaCategory')->findAllNotDeleted();

        foreach ($Categories as $CategoriaOfferta) {

            /**
             * @var $CategoriaOfferta OffertaCategory
             */
            $categorie[$CategoriaOfferta->getId()] = $CategoriaOfferta->translate($request->getLocale())->getTitolo();
        }

        return $this->render('@Offerte/categorie/sort.html.twig', ['categorie' => $categorie]);

    }

    /**
     * @Route("/offertecat/save-sort", name="offertecat_save_sort")
     */
    public function saveSortAction(Request $request)
    {

        $trans = $this->get('translator');

        $return = [];

        if ($request->get('sorted')) {

            $em = $this->getDoctrine()->getManager();

            $res = $em->getRepository('OfferteBundle:OffertaCategory')->getSortedByIds($request->get('sorted'));

            foreach ($res as $k => $Category) {
                /**
                 * @var $Category OfferteCategory
                 */

                $Category->setSort($k);
                $em->persist($Category);
            }

            $return['result'] = true;

            $em->flush();

            return new JsonResponse($return);

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);

    }

}
