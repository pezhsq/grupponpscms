<?php
// 13/01/17, 13.50
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Controller\Admin;

use OfferteBundle\Entity\Offerta;
use OfferteBundle\Entity\OffertaAttachment;
use OfferteBundle\Form\OffertaForm;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_OFFERTE')")
 */
class OfferteController extends Controller
{

    /**
     * @Route("/offerte", name="offerte")
     */
    public function listAction()
    {

        return $this->render('OfferteBundle:Offerte:list.html.twig');

    }

    /**
     * @Route("/offerte/new", name="offerte_new")
     */
    public function newAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(OffertaForm::class, null, ['langs' => $langs]);
        $form->handleRequest($request);

        $Offerta = new Offerta();

        $CheckCategories = $em->getRepository('OfferteBundle:OffertaCategory')->countAllActive();

        if (!$CheckCategories) {

            $translator = $this->get('translator');

            $this->addFlash('error', $translator->trans('offerta.messages.not_enough_cat'));

            return $this->redirectToRoute('offerte');

        }

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Offerta Offerta
             */
            $Offerta = $form->getData();

            $em->persist($Offerta);
            $em->flush();

            $translator = $this->get('translator');


            $elemento = '"'.$Offerta->translate($request->getLocale())->getTitolo().'" ('.$Offerta->getId().') ';

            $this->addFlash('success', 'Offerta '.$elemento.$translator->trans('default.labels.creata'));

            return $this->redirectToRoute('offerte');

        }

        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;


        return $this->render(
            'OfferteBundle:Offerte:new.html.twig',
            ['form' => $form->createView(), 'supportData' => $supportData]
        );

    }

    /**
     * @Route("/offerte/edit/{id}", name="offerte_edit")
     */
    public function editAction(Request $request, Offerta $Offerta)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();

        $em = $this->getDoctrine()->getManager();

        $originalOffertaRows = new ArrayCollection();
        // Crea an ArrayCollection delle attuali offertarows nel db
        foreach ($Offerta->getOffertaRows() as $offertaRow) {
            $originalOffertaRows->add($offertaRow);
        }

        $originaleOffertaHasCat = new ArrayCollection();
        foreach ($Offerta->getOffertaCategoriesAssociation() as $association) {
            $originaleOffertaHasCat->add($association);
        }

        $form = $this->createForm(OffertaForm::class, $Offerta, ['langs' => $langs]);
        $form->handleRequest($request);

        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;
        if ($Offerta->getListImgFileName()) {
            $supportData['listImgUrl'] = '/'.$Offerta->getUploadDir().$Offerta->getListImgFileName();
        }
        if ($Offerta->getHeaderImgFileName()) {
            $supportData['headerImgUrl'] = '/'.$Offerta->getUploadDir().$Offerta->getHeaderImgFileName();
        }


        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($originaleOffertaHasCat as $OffertaHasCat) {
                if (false === $Offerta->getOffertaCategoriesAssociation()->contains($OffertaHasCat)) {
                    $em->remove($OffertaHasCat);
                }
            }

            foreach ($originalOffertaRows as $offertaRow) {
                if (false === $Offerta->getOffertaRows()->contains($offertaRow)) {
                    $em->remove($offertaRow);
                }
            }

            $cancellaHeaderPrecedente = $request->get('offerta_form')['headerImgDelete'];
            if ($cancellaHeaderPrecedente) {
                $this->container->get('vich_uploader.upload_handler')->remove($Offerta, 'headerImg');
                $Offerta->setHeaderImg(null);
                $Offerta->setHeaderImgAlt('');
            }

            $cancellaListPrecedente = $request->get('offerta_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->container->get('vich_uploader.upload_handler')->remove($Offerta, 'listImg');
                $Offerta->setListImg(null);
                $Offerta->setListImgAlt('');
            }

            $em->persist($Offerta);
            $em->flush();

            $elemento = '"'.$Offerta->translate($request->getLocale())->getTitolo().'" ('.$Offerta->getId().') ';

            $translator = $this->get('translator');

            $this->addFlash('success', 'Offerta '.$elemento.$translator->trans('default.labels.modificata'));

            return $this->redirectToRoute('offerte');

        }

        return $this->render(
            'OfferteBundle:Offerte:edit.html.twig',
            ['form' => $form->createView(), 'Offerte' => $Offerta, 'supportData' => $supportData]
        );

    }

    /**
     * @Route("/offerte/json", name="offerte_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();


        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $OfferteList = $em->getRepository('OfferteBundle:Offerta')->findAll();
        } else {
            $OfferteList = $em->getRepository('OfferteBundle:Offerta')->findAllNotDeleted();
        }

        $retData = [];

        $offerta = [];

        // $commentRepository = $em->getRepository('OfferteBundle:Commento');

        foreach ($OfferteList as $Offerta) {

            $categories = [];

            $cats = $Offerta->getCategorieAggiuntive();

            foreach ($cats as $cat) {
                $categories[] = $cat->translate($request->getLocale())->getTitolo();
            }
            /**
             * @var $Offerta Offerta;
             */
            $record = [];
            $record['id'] = $Offerta->getId();
            $record['titolo'] = $Offerta->translate($request->getLocale())->getTitolo();
            $record['isEnabled'] = $Offerta->getIsEnabled();
            $record['category'] = $Offerta->getPrimaryCategory()->translate($request->getLocale())->getTitolo();
            if ($categories) {
                $record['category'] .= ' ('.implode(', ', $categories).')';
            }

            // $record['commenti_attivi'] = $commentRepository->getCommentiCountForOfferta($Offerta);
            // $record['commenti']        = $commentRepository->getCommentiCountForOfferta($Offerta, 0);
            $record['deleted'] = $Offerta->isDeleted();
            $record['createdAt'] = $Offerta->getCreatedAt()->format('d/m/Y H:i:s');
            $record['publishAt'] = $Offerta->getPublishAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Offerta->getUpdatedAt()->format('d/m/Y H:i:s');

            $offerta[] = $record;
        }

        $retData['data'] = $offerta;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/offerte/delete/{id}/{force}", name="offerte_delete",  requirements={"id" = "\d+"}, defaults={"force" =
     *     false}))
     */
    public function deleteAction(Request $request, Offerta $Offerta)
    {

        $elemento = '"'.$Offerta->translate('it')->getTitolo().'" ('.$Offerta->getId().')';

        $em = $this->getDoctrine()->getManager();

        if ($Offerta->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }
            }
            // remove the entity
            $em->remove($Offerta);
            $em->flush();

        } elseif (!$Offerta->isDeleted()) {
            $em->remove($Offerta);
            $em->flush();
        }

        $this->addFlash('success', 'Offerta '.$elemento.' eliminata');

        return $this->redirectToRoute('offerte');

    }

    /**
     * @Route("/offerte/restore/{id}", name="offerte_restore")
     */
    public function restoreAction(Request $request, Offerta $Offerta)
    {

        if ($Offerta->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = '"'.$Offerta->translate('it')->getTitolo().'" ('.$Offerta->getId().')';

            $Offerta->restore();

            $offertaSlugger = $this->get('app.offerte.services.offerta_slugger');

            foreach ($Offerta->getTranslations() as $OffertaTranslation) {

                /**
                 * @var $OffertaTranslation OffertaTranslation
                 */
                if ($OffertaTranslation->getIsEnabled()) {

                    $newSlug = $offertaSlugger->slugify($OffertaTranslation, $OffertaTranslation->getSlug());

                    if ($OffertaTranslation->getSlug() != $newSlug) {
                        $OffertaTranslation->setSlug($newSlug);
                    }

                }

            }


            $em->flush();

            $this->addFlash('success', 'Offerta '.$elemento.' ripristinata');

        }

        return $this->redirectToRoute('offerte');

    }

    /**
     * @Route("/offerte/toggle-enabled/{id}", name="offerte_toggle_enabled")
     */
    public function toggleIsEnabledOfferteAction(Request $request, Offerta $Offerta)
    {

        $elemento = '"'.$Offerta->translate()->getTitolo().'" ('.$Offerta->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Offerta '.$elemento.' ';

        if ($Offerta->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Offerta->setIsEnabled(!$Offerta->getIsEnabled());

        $em->persist($Offerta);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('offerte');


    }

}
