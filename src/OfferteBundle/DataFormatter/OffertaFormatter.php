<?php
// 17/03/17, 17.14
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\DataFormatter;

use OfferteBundle\Entity\Offerta;
use OfferteBundle\Entity\OffertaCategory;
use AppBundle\DataFormatter\DataFormatter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;

class OffertaFormatter extends DataFormatter
{

    public function extractData()
    {
        // TODO: Implement getData() method.
    }

    public function getOfferta()
    {

        if ($this->AdditionalData && isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Offerta) {

            /**
             * @var $Entity Offerta
             */
            $Entity = false;

            if (isset($this->AdditionalData['Entity'])) {
                $Entity = $this->AdditionalData['Entity'];
            }

            $rows = $Entity->getOffertaRows();

            $offerta = [];

            $offerta['titolo'] = $Entity->translate()->getTitolo();
            $offerta['prezzo'] = $Entity->getPrezzo();
            $offerta['inizio'] = $Entity->getDataInizio();
            $offerta['fine'] = $Entity->getDataFine();
            $offerta['rows'] = $rows;
            $offerta['categoria'] = $Entity->getPrimaryCategory()->translate()->getTitolo();

            $data = [];
            $data['offerta'] = $offerta;

            return $data;

        }

    }

    public function getData()
    {

        $data = [];

        if ($this->request->get('slug')) {

            $OffertaCategoryTranslation = $this->em->getRepository(
                'OfferteBundle:OffertaCategoryTranslation'
            )->findOneBy(
                ['slug' => $this->request->get('slug'), 'locale' => $this->locale]
            );

            if ($OffertaCategoryTranslation) {
                $data['categoria'] = $OffertaCategoryTranslation->getTitolo();


                // if (!$OffertaCategoryTranslation) {
                // 	throw new NotFoundHttpException("Pagina non trovata");
                // }

                $OffertaCategory = $OffertaCategoryTranslation->getTranslatable();

                // if (!$OffertaCategory->getIsEnabled()) {
                // 	throw new NotFoundHttpException("Pagina non trovata");
                // }

                $query = $this->em->getRepository('OfferteBundle:OffertaHasCategory')->getOfferte(
                    $OffertaCategory->getId(),
                    $this->locale,
                    $start = 0,
                    $limit = 4
                );

                $offerte = [];

                foreach ($query as $q) {
                    $offerta = [];
                    $offerta['titolo'] = $q->getOfferta()->translate(
                        $this->request->getLocale()
                    )->getTitolo();
                    $offerta['sottotitolo'] = $q->getOfferta()->translate(
                        $this->request->getLocale()
                    )->getSottotitolo();
                    $offerta['inizio'] = $q->getOfferta()->getDataInizio()->format('d/m/Y');
                    $offerta['fine'] = $q->getOfferta()->getDataFine()->format('d/m/Y');
                    $offerta['prezzo'] = $q->getOfferta()->getPrezzo();
                    $offerta['list_img_src'] = '/'.$q->getOfferta()->getUploadDir().$q->getOfferta(
                        )->getListImgFileName();
                    $offerta['list_img_alt'] = $q->getOfferta()->getListImgAlt();
                    $offerta['slug'] = $this->container->get('app.path_manager')->generateUrl(
                        'offerta',
                        ['slug' => $q->getOfferta()->translate()->getSlug()]
                    );
                    if ($q->getOfferta()->getOffertaRows() != null) {
                        $offertaRows = $q->getOfferta()->getOffertaRows();
                    }
                    $offerta['rows'] = $offertaRows;
                    $offerte[] = $offerta;
                }

                $data['offerte'] = $offerte;

            }

            return $data;

        }

    }

    public function getLastUpdatedOfCategories()
    {

        $data = [];

        $categorie = $this->em->getRepository('OfferteBundle:OffertaCategory')->findAll();

        foreach ($categorie as $categoria) {
            # code...
            $offerta_cat = $this->em->getRepository('OfferteBundle:Offerta')->getOneByCategory(
                $categoria->getId(),
                $this->locale
            );
            $data['offerte'][$categoria->translate()->getTitolo()] = $offerta_cat;
        }

        return $data;
    }

    public function getAllFromCategory()
    {

        $data = [];

        $categorie = $this->em->getRepository('OfferteBundle:OffertaCategory')->findAll();

        $categorie_t = [];
        foreach ($categorie as $c) {
            $categorie_t[] = $c->translate($this->request->getLocale())->getTitolo();
        }

        $offerte = $this->em->getRepository('OfferteBundle:Offerta')->findBy(["isEnabled" => true]);
        $offerte = new ArrayCollection($offerte);

        $gruppo = [];
        $lista_off = [];

        foreach ($categorie as $c) {
            $gruppo['titolo'] = $c->translate($this->request->getLocale())->getTitolo();
            $parametro = $c->getId();
            $lista_off = $offerte->filter(
                function ($entry) use ($parametro) {

                    if ($entry->getPrimaryCategory()->getId() == $parametro) {
                        return true;
                    }
                }
            );
            $gruppo['lista_offerte'] = $lista_off;
            $data['gruppi'][] = $gruppo;
        }

        return $data;

    }

}
