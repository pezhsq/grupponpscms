<?php
// 09/01/17, 10.09
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Service;


use AppBundle\Entity\Page;
use OfferteBundle\Entity\OffertaTranslation;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Tree\Fixture\Closure\News;

class OffertaSlugger {

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var EntityManager
     */
    private $em;

    private $offerta;

    public function __construct(Slugify $slugify, EntityManager $em) {

        $this->slugify = $slugify;

        $this->slugify->addRule('à', 'a');
        $this->slugify->addRule('è', 'e');
        $this->slugify->addRule('é', 'e');
        $this->slugify->addRule('ì', 'i');
        $this->slugify->addRule('ò', 'o');
        $this->slugify->addRule('ù', 'u');

        $this->em = $em;
    }

    public function slugify(OffertaTranslation $offertaTranslation, $slug = null) {

        $this->offerta = $offertaTranslation->getTranslatable();

        if(!$slug) {
            $slug = $this->slugify->slugify($offertaTranslation->getTitolo());
        }

        $newSlug = $slug;

        $i = 1;

        while(!$this->checkSlug($newSlug, $offertaTranslation->getLocale())) {

            $newSlug = $slug.'-'.$i;

            $i++;

        }

        return $newSlug;


    }

    private function checkSlug($slug, $locale) {

        $data = $this->em->getRepository('OfferteBundle:Offerta')->findBySlugButNotId($slug, $locale, $this->offerta->getId());

        return !(bool)count($data);


    }


}
