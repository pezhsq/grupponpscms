<?php
// 09/01/17, 16.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use AppBundle\Validator\Constraints as WebTekAssert;

/**
 * @WebTekAssert\MaxCategories
 * @ORM\Entity(repositoryClass="OfferteBundle\Repository\CategorieOffertaRepository")
 * @ORM\Table(name="offertacategory")
 */
class OffertaCategory
{

	use ORMBehaviours\SoftDeletable\SoftDeletable,
		ORMBehaviours\Timestampable\Timestampable,
		ORMBehaviours\Sortable\Sortable,
		ORMBehaviours\Translatable\Translatable;
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $isEnabled;

	/**
	 * @ORM\OneToMany(targetEntity="OfferteBundle\Entity\OffertaHasCategory", mappedBy="category_id")
	 */
	private $offerta_categories_association;

	/**
	 * @ORM\Column(type="string", length=150)
	 */
	private $template = 'blog';

	public function __construct()
	{

		$this->offerta_categories_association = new ArrayCollection();

	}

	public function addOffertaCategoriesAssociation(OffertaHasCategory $offertaHasCategory)
	{

		$this->offerta_categories_association[] = $offertaHasCategory;
	}

	public function removeOffertaCategoriesAssociation(OffertaHasCategory $offertaHasCategory)
	{

		$this->offerta_categories_association->removeElement($offertaHasCategory);

	}

	/**
	 * @return mixed
	 */
	public function getIsEnabled()
	{

		return $this->isEnabled;
	}

	/**
	 * @param mixed $isEnabled
	 */
	public function setIsEnabled($isEnabled)
	{

		$this->isEnabled = $isEnabled;
	}

	/**
	 * @return mixed
	 */
	public function getTemplate()
	{
		return $this->template;
	}

	/**
	 * @param mixed $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}

	/**
	 * @Assert\Callback()
	 * @param ExecutionContextInterface $context
	 * @param $payload
	 */
	public function validate(ExecutionContextInterface $context, $payload)
	{

		/**
		 * @var $object OffertaCategory
		 */
		$object = $context->getObject();

		$translations = $object->getTranslations();

		foreach ($translations as $translation) {
			if (false == $translation->getTitolo()) {
				$context->buildViolation('offertacat.titolo_empty_no_lang')
					->atPath('translations['.$translation->getLocale().'].titolo')
					->addViolation();
				$context->buildViolation('offertacat.titolo_empty', ['lang' => $translation->getLocale()])
					->atPath('translations_'.$translation->getLocale().'_titolo')
					->addViolation();
			}
		}

	}

	public function __toString()
	{

		return (string)$this->getId();
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{

		return $this->id;
	}


}
