<?php
// 16/01/17, 14.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="OfferteBundle\Repository\OffertaHasCategoriesRepository")
 * @ORM\Table(name="offerta_has_categories")
 */
class OffertaHasCategory {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OfferteBundle\Entity\Offerta", inversedBy="offerta_categories_association")
     * @ORM\JoinColumn(name="offerta_id", referencedColumnName="id")
     */
    private $offerta;

    /**
     * @ORM\ManyToOne(targetEntity="OfferteBundle\Entity\OffertaCategory", inversedBy="offerta_categories_association")
     * @ORM\JoinColumn(name="offerta_category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isMain;

    public function setOfferta(Offerta $offerta = null) {

        $this->offerta = $offerta;

        return $this;

    }

    public function getOfferta() {

        return $this->offerta;

    }

    public function setCategory(OffertaCategory $offertaCategory) {

        $this->category = $offertaCategory;

        return $this;

    }

    public function getCategory() {

        return $this->category;

    }

    /**
     * @return mixed
     */
    public function getIsMain() {

        return $this->isMain;
    }

    /**
     * @param mixed $isMain
     */
    public function setIsMain($isMain) {

        $this->isMain = $isMain;
    }

    /**
     * @return mixed
     */
    public function getId() {

        return $this->id;
    }

    public function __toString() {

        return $this->getId().': Offerta('.$this->getOfferta()->getId().'), Category('.$this->getCategory()->getId().'), isMain('.$this->getIsMain().')';

    }


}
