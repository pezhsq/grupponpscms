<?php
// 02/01/17, 15.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="OfferteBundle\Repository\OffertaRepository")
 * @ORM\Table(name="offerta")
 * @Vich\Uploadable()
 */
class Offerta
{

    use ORMBehaviours\Loggable\Loggable,
        ORMBehaviours\Timestampable\Timestampable,
        ORMBehaviours\Translatable\Translatable,
        ORMBehaviours\SoftDeletable\SoftDeletable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="OfferteBundle\Entity\OffertaRow", mappedBy="offerta", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $offertaRows;

    /**
     * @ORM\OneToMany(targetEntity="OfferteBundle\Entity\OffertaAttachment", mappedBy="offerta", cascade={"persist",
     *     "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     *
     */
    private $attachments;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;


    /**
     * @ORM\Column(type="datetime")
     */
    private $publishAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dataInizio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dataFine;

    /**
     * @ORM\Column(type="string")
     */
    private $prezzo;

    /**
     * @ORM\OneToMany(targetEntity="OfferteBundle\Entity\OffertaHasCategory", mappedBy="offerta", cascade={"all"})
     */
    private $offerta_categories_association;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    /**
     * @Vich\UploadableField(mapping="offerte", fileNameProperty="listImgFileName")
     */
    private $listImg;

    public $listImgData;
    private $listImgDelete;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgAlt;

    /**
     * @Vich\UploadableField(mapping="offerte", fileNameProperty="headerImgFileName")
     */
    private $headerImg;

    public $headerImgData;
    private $headerImgDelete;

    private $uuid;

    private $categorieAggiuntive;

    public function __construct()
    {

        $this->offertaRows = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->offerta_categories_association = new ArrayCollection();

    }

    public function addOffertaCategoriesAssociation(OffertaHasCategory $offertaHasCategory)
    {

        $this->offerta_categories_association[] = $offertaHasCategory;
    }

    public function removeOffertaCategoriesAssociation(OffertaCategory $offertaCategory)
    {

        $this->offerta_categories_association = $this->offerta_categories_association->filter(
            function ($entry) use ($offertaCategory) {

                return $entry->getCategory() != $offertaCategory || $entry->getIsMain() == 1;
            }
        );

    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOffertaRows()
    {

        return $this->offertaRows;
    }


    /**
     * @return mixed
     */
    public function getAttachments()
    {

        return $this->attachments;
    }

    /**
     * @param mixed $attachments
     */
    public function setAttachments($attachments)
    {

        $this->attachments = $attachments;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {

        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {

        $this->uuid = $uuid;
    }


    /**
     * Add offertaRow
     *
     * @param $offertaRows
     *
     * @return Offerta
     */

    public function addOffertaRow(OffertaRow $offertaRows)
    {

        $offertaRows->setOfferta($this);

        $this->offertaRows[] = $offertaRows;

        return $this;
    }

    /**
     * Remove offertaRow
     *
     * @param \OfferteBundle\Entity\OffertaRow $offertaRows
     */
    public function removeOffertaRow(OffertaRow $offertaRows)
    {

        $this->offertaRows->removeElement($offertaRows);
    }

    /**
     * @return mixed
     */
    public function getTranslatable()
    {

        return $this->translatable;
    }


    function getUploadDir()
    {

        return 'files/offerte/'.$this->getId().'/';

    }

    /**
     * @return mixed
     */
    public function getPublishAt()
    {

        return $this->publishAt;
    }

    /**
     * @param mixed $publishAt
     */
    public function setPublishAt($publishAt)
    {

        $this->publishAt = $publishAt;
    }

    public function getPrimaryCategory()
    {

        $data = $this->offerta_categories_association->filter(
            function ($entry) {

                return $entry->getIsMain() == 1;

            }
        );


        if (!$data->isEmpty()) {

            return $data->last()->getCategory();

        }

        return null;

    }

    public function getCategorieAggiuntive()
    {

        //dump($this->offerta_categories_association);

        $data = $this->offerta_categories_association->filter(
            function ($entry) {

                return $entry->getIsMain() == 0;

            }
        );

        $return = [];

        foreach ($data as $entry) {
            /**
             * @var $entry OffertaCategory
             */
            $return[] = $entry->getCategory();
        }

        return $return;

    }

    public function setPrimaryCategory(OffertaCategory $offertaCategory)
    {

        $offertaCategoryAssociations = new OffertaHasCategory();

        $offertaCategoryAssociations->setCategory($offertaCategory);
        $offertaCategoryAssociations->setIsMain(true);
        $offertaCategoryAssociations->setOfferta($this);

        if (!$this->offerta_categories_association->exists(
            function ($key, $entry) use ($offertaCategory) {

                return $entry->getIsMain() == 1 && $entry->getCategory() == $offertaCategory;

            }
        )
        ) {

            $this->offerta_categories_association = $this->offerta_categories_association->filter(
                function ($entry) {

                    return $entry->getIsMain() != 1;

                }
            );

            $this->addOffertaCategoriesAssociation($offertaCategoryAssociations);

        }

        $this->addOffertaCategoriesAssociation($offertaCategoryAssociations);

    }

    public function getOffertaCategoriesAssociation()
    {

        return $this->offerta_categories_association;
    }

    public function addCategorieAggiuntive(OffertaCategory $offertaCategory)
    {

        $offertaCategoryAssociations = new OffertaHasCategory();

        $offertaCategoryAssociations->setCategory($offertaCategory);
        $offertaCategoryAssociations->setIsMain(false);
        $offertaCategoryAssociations->setOfferta($this);

        $this->offerta_categories_association->add($offertaCategoryAssociations);

    }

    public function setCategorieAggiuntive($offertaCategories)
    {

        foreach (array_diff($this->getCategorieAggiuntive(), $offertaCategories) as $category) {
            $this->removeOffertaCategoriesAssociation($category);
        }

        foreach ($offertaCategories as $offertaCategory) {
            if (!$this->offerta_categories_association->exists(
                function ($key, $entry) use ($offertaCategory) {

                    return $entry->getCategory() == $offertaCategory;
                }
            )
            ) {
                $this->addCategorieAggiuntive($offertaCategory);
            }
        }

    }

    function __toString()
    {

        return (string)$this->getId();
    }


    /**
     * Set dataInizio
     *
     * @param \DateTime $dataInizio
     *
     * @return Offerta
     */
    public function setDataInizio($dataInizio)
    {
        $this->dataInizio = $dataInizio;

        return $this;
    }

    /**
     * Get dataInizio
     *
     * @return \DateTime
     */
    public function getDataInizio()
    {
        return $this->dataInizio;
    }

    /**
     * Set dataFine
     *
     * @param \DateTime $dataFine
     *
     * @return Offerta
     */
    public function setDataFine($dataFine)
    {
        $this->dataFine = $dataFine;

        return $this;
    }

    /**
     * Get dataFine
     *
     * @return \DateTime
     */
    public function getDataFine()
    {
        return $this->dataFine;
    }

    /**
     * Set prezzo
     *
     * @param string $prezzo
     *
     * @return Offerta
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return string
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }

    /**
     * Add attachment
     *
     * @param \OfferteBundle\Entity\OffertaAttachment $attachment
     *
     * @return Offerta
     */
    public function addAttachment(\OfferteBundle\Entity\OffertaAttachment $attachment)
    {
        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \OfferteBundle\Entity\OffertaAttachment $attachment
     */
    public function removeAttachment(\OfferteBundle\Entity\OffertaAttachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /** IMMAGINI **/

    /**
     * @return File
     */
    public function getListImg()
    {

        return $this->listImg;
    }

    public function setListImg($listImg = null)
    {

        $this->listImg = $listImg;

        if ($listImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;

    }

    /**
     * @return mixed
     */
    public function getListImgFileName()
    {

        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {

        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {

        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {

        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {

        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {

        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {

        return $this->listImgData;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgFileName()
    {

        return $this->headerImgFileName;
    }

    /**
     * @param mixed $headerImgFileName
     */
    public function setHeaderImgFileName($headerImgFileName)
    {

        $this->headerImgFileName = $headerImgFileName;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgAlt()
    {

        return $this->headerImgAlt;
    }

    /**
     * @param mixed $headerImgAlt
     */
    public function setHeaderImgAlt($headerImgAlt)
    {

        $this->headerImgAlt = $headerImgAlt;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgDelete()
    {

        return $this->headerImgDelete;
    }

    /**
     * @param mixed $headerImgDelete
     */
    public function setHeaderImgDelete($headerImgDelete)
    {

        $this->headerImgDelete = $headerImgDelete;
    }


    /**
     * @return mixed
     */
    public function getHeaderImg()
    {

        return $this->headerImg;
    }

    /**
     * @param mixed $headerImg
     */
    public function setHeaderImg($headerImg)
    {

        $this->headerImg = $headerImg;

        if ($headerImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgData()
    {

        return $this->headerImgData;
    }
}
