<?php
// 17/01/17, 10.20
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Repository;


use OfferteBundle\Entity\OffertaCategory;
use Doctrine\ORM\EntityRepository;

class OffertaHasCategoriesRepository extends EntityRepository
{

    public function associationExists($offertaId, $categoryId, $isMain)
    {

        return $this->createQueryBuilder('nhc')
            ->select('count(nhc.id)')
            ->andWhere('nhc.offerta = :offerta')
            ->andWhere('nhc.category = :category')
            ->andWhere('nhc.isMain = :isMain')
            ->setParameter('offerta', $offertaId)
            ->setParameter('category', $categoryId)
            ->setParameter('isMain', $isMain)
            ->getQuery()
            ->getSingleScalarResult();

    }

    public function getOfferteQuery($category_id, $locale)
    {

        $qb = $this->createQueryBuilder('nhc');

        $qb->leftJoin(
            'OfferteBundle\Entity\Offerta',
            'n',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'n = nhc.offerta'
        )->leftJoin(
            'OfferteBundle\Entity\OffertaTranslation',
            'nt',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nt.translatable = nhc.offerta AND nt.locale = :locale'
        )->andWhere('nhc.category = :category_id')->setParameter('category_id', $category_id)
            ->andWhere('n.isEnabled = 1')
            ->andWhere('n.publishAt < :publishAt')
            ->andWhere('nt.isEnabled = 1')
            ->setParameter('publishAt', new \DateTime())
            ->setParameter('locale', $locale)
            ->orderBy('n.publishAt', 'DESC');

        $query = $qb->getQuery();


        return $query;

    }

    public function getOfferte($category_id, $locale, $start = 0, $limit = 10, $excluded = [])
    {

        $qb = $this->createQueryBuilder('nhc');

        $qb->leftJoin(
            'OfferteBundle\Entity\Offerta',
            'n',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'n.id = nhc.offerta'
        )
            ->leftJoin(
                'OfferteBundle\Entity\OffertaTranslation',
                'nt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nt.translatable = nhc.offerta AND nt.locale = :locale'
            )
            ->leftJoin(
                'OfferteBundle\Entity\OffertaCategory',
                'nc',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nc.id = nhc.category'
            )
            ->setParameter('locale', $locale)
            ->andWhere('nc.id = :category_id')
            ->setParameter('category_id', $category_id)
            ->andWhere('n.isEnabled = 1')
            ->andWhere('nt.isEnabled = 1');
        if ($excluded) {
            $qb->andWhere('n NOT IN (:offerta)')
                ->setParameter('offerta', $excluded);
        }
        $qb->setMaxResults($limit);
        if ($start && $limit) {
            $qb->setFirstResult($start);
        }

        $qb->orderBy('n.publishAt', 'DESC');

        $query = $qb->getQuery();
        return $query->getResult();

    }

    public function getOfferteCount(OffertaCategory $category, $locale)
    {

        $qb = $this->createQueryBuilder('nhc');
        $qb->select('count(nhc.offerta)');
        $qb->leftJoin(
            'OfferteBundle\Entity\Offerta',
            'n',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'n = nhc.offerta'
        )->leftJoin(
            'OfferteBundle\Entity\OffertaTranslation',
            'nt',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nt.translatable = nhc.offerta AND nt.locale = :locale'
        )->andWhere('nhc.category = :category')->setParameter('category', $category)
            ->andWhere('n.isEnabled = 1')
            ->andWhere('n.publishAt < :publishAt')
            ->andWhere('nt.isEnabled = 1')
            ->setParameter('publishAt', new \DateTime())
            ->setParameter('locale', $locale);

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();


    }



}
