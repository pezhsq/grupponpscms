<?php
// 13/01/17, 8.57
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace OfferteBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CategorieOffertaTranslationRepository extends EntityRepository {


    function findBySlugButNotId($slug, $locale, $id) {

        $query = $this->createQueryBuilder('translation')
            ->leftJoin('OfferteBundle\Entity\OffertaCategory',
                'nc',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nc.id = translation.translatable'
            )
            ->andWhere('translation.slug = :slug')
            ->setParameter('slug', $slug)
            ->andWhere('translation.locale = :locale')
            ->setParameter('locale', $locale)
            ->andWhere('translation.id != :id')
            ->setParameter('id', $id)
            ->andWhere('nc.deletedAt IS NULL')
            ->getQuery();

        return $query->execute();
    }

}
