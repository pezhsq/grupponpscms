<?php
// 29/06/17, 11.53
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AziendaBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity()
 * @ORM\Table(name="dipartimenti")
 * @Vich\Uploadable()
 */
class Department
{

    use ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $telefono;

    /**
     * @ORM\OneToMany(targetEntity="AziendaBundle\Entity\Orario", mappedBy="department", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $orari;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @Vich\UploadableField(mapping="dipartimenti", fileNameProperty="foto")
     */
    private $fotoFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $foto;

    /**
     * @ORM\ManyToOne(targetEntity="AziendaBundle\Entity\Sede", inversedBy="departments")
     */
    private $sede;

    public function __construct()
    {

        $this->orari = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;

    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {

        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {

        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getOrari()
    {

        return $this->orari;
    }

    public function addOrarus(Orario $orario)
    {

        $orario->setDepartment($this);
        $this->orari[] = $orario;

        return $this;

    }

    public function removeOrarus(Orario $orario)
    {

        $this->orari->removeElement($orario);
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {

        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {

        return $this->foto;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto)
    {

        $this->foto = $foto;
    }

    /**
     * @return mixed
     */
    public function getFotoFile()
    {

        return $this->fotoFile;
    }

    /**
     * @param mixed $fotoFile
     * @return Department
     */
    public function setFotoFile($fotoFile)
    {

        $this->fotoFile = $fotoFile;
        if ($fotoFile) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSede()
    {

        return $this->sede;
    }

    /**
     * @param mixed $sede
     */
    public function setSede(
        Sede $sede = null
    ) {

        $this->sede = $sede;
    }

    public function getUploadDir()
    {

        return 'files/sedi/'.$this->getId().'/';
    }


}