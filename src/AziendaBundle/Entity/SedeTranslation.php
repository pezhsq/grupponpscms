<?php
// 28/06/17, 15.55
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AziendaBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\Seo;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\EntityListeners({"AziendaBundle\EntityListener\SedeTranslationListener"})
 * @ORM\Table(name="sedi_translation")
 */
class SedeTranslation
{

    use ORMBehaviours\Translatable\Translation;
    use Seo, Loggable;

}