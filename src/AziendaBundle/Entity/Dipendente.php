<?php

namespace AziendaBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AziendaBundle\Repository\DipendenteRepository")
 * @ORM\EntityListeners({"AziendaBundle\EntityListener\DipendenteListener"})
 * @ORM\Table(name="dipendenti")
 * @Vich\Uploadable()
 */
class Dipendente
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string")
     */
    private $nome;

    /**
     * @ORM\Column(type="string")
     */
    private $cognome;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebook;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $twitter;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $googleplus;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $linkedin;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    /**
     * @Vich\UploadableField(mapping="dipendenti", fileNameProperty="listImgFileName")
     */
    private $listImg;

    public $listImgData;
    private $listImgDelete;


    /**
     * @ORM\ManyToMany(targetEntity="Sede", inversedBy="dipendenti")
     * @ORM\JoinTable(name="dipendenti_has_sedi",
     *      joinColumns={@ORM\JoinColumn(name="dipendenti_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="sede_id", referencedColumnName="id")})
     */
    private $sedi;

    /**
     * @ORM\OneToMany(targetEntity="AziendaBundle\Entity\DipendenteAttachment", mappedBy="dipendente", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC", "id" = "ASC"})
     */
    private $attachments;

    private $uuid;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCognome()
    {

        return $this->cognome;
    }

    /**
     * @param mixed $cognome
     */
    public function setCognome($cognome)
    {

        $this->cognome = $cognome;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {

        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {

        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {

        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {

        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     */
    public function setFacebook($facebook)
    {

        $this->facebook = $facebook;
    }

    /**
     * @return mixed
     */
    public function getTwitter()
    {

        return $this->twitter;
    }

    /**
     * @param mixed $twitter
     */
    public function setTwitter($twitter)
    {

        $this->twitter = $twitter;
    }

    /**
     * @return mixed
     */
    public function getGoogleplus()
    {

        return $this->googleplus;
    }

    /**
     * @param mixed $googleplus
     */
    public function setGoogleplus($googleplus)
    {

        $this->googleplus = $googleplus;
    }

    /**
     * @return mixed
     */
    public function getLinkedin()
    {

        return $this->linkedin;
    }

    /**
     * @param mixed $linkedin
     */
    public function setLinkedin($linkedin)
    {

        $this->linkedin = $linkedin;
    }

    /**
     * @return File
     */
    public function getListImg()
    {

        return $this->listImg;
    }

    public function setListImg($listImg = null)
    {

        $this->listImg = $listImg;
        if ($listImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;

    }

    /**
     * @return mixed
     */
    public function getListImgFileName()
    {

        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {

        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {

        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {

        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {

        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {

        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {

        return $this->listImgData;
    }

    function getUploadDir()
    {

        return 'files/dipendenti/'.$this->getId().'/';

    }

    public function __toString()
    {

        return $this->getNome().' '.$this->getCognome();
    }


    /**
     * Add sedi
     *
     * @param Sede $sedi
     *
     * @return Dipendente
     */
    public function addSedus(Sede $sede)
    {

        $this->sedi[] = $sede;

        return $this;
    }

    /**
     * Remove sedi
     *
     * @param \Sede $sede
     */
    public function removeSedus(Sede $sede)
    {


        $this->sedi->removeElement($sede);
    }

    /**
     * @return mixed
     */
    public function getSedi()
    {

        return $this->sedi;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {

        return $this->attachments;
    }

    /**
     * @param mixed $attachments
     */
    public function setAttachments($attachments)
    {

        $this->attachments = $attachments;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {

        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {

        $this->uuid = $uuid;
    }


}
