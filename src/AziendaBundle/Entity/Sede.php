<?php
// 28/06/17, 15.14
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AziendaBundle\Entity;

use AppBundle\Traits\Loggable;
use AziendaBundle\Entity\Dipendente;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AziendaBundle\Repository\SedeRepository")
 * @ORM\EntityListeners({"AziendaBundle\EntityListener\SedeListener"})
 * @ORM\Table(name="sedi")
 * @Vich\Uploadable()
 */
class Sede
{

    use ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, ORMBehaviours\SoftDeletable\SoftDeletable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $codice;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $nome;
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $indirizzo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $indirizzo2;

    /**
     * @ORM\Column(type="string")
     */
    private $coordinate;

    /**
     * @ORM\Column(type="integer")
     */
    private $zoom;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $telefono;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgAlt;

    /**
     * @ORM\OneToMany(targetEntity="AziendaBundle\Entity\Department", mappedBy="sede", cascade={"persist", "remove"})
     */
    private $departments;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $sort;

    /**
     * @Vich\UploadableField(mapping="sedi", fileNameProperty="headerImgFileName")
     */
    private $headerImg;

    public $headerImgData;
    private $headerImgDelete;

    /**
     * @ORM\ManyToMany(targetEntity="AziendaBundle\Entity\Dipendente", mappedBy="sedi")
     * @ORM\JoinTable(name="dipendenti_has_sedi",
     *      joinColumns={@ORM\JoinColumn(name="dipendenti_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="sede_id", referencedColumnName="id")})
     */
    private $dipendenti;

    public function __construct()
    {

        $this->coordinate = '46.1712597,9.8693336';
        $this->zoom = 13;
        $this->departments = new ArrayCollection();
        $this->dipendenti = new ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getIndirizzo()
    {

        return $this->indirizzo;
    }

    /**
     * @param mixed $indirizzo
     */
    public function setIndirizzo($indirizzo)
    {

        $this->indirizzo = $indirizzo;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {

        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {

        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {

        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {

        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax)
    {

        $this->fax = $fax;
    }


    /**
     * @return mixed
     */
    public function getHeaderImgFileName()
    {

        return $this->headerImgFileName;
    }

    /**
     * @param mixed $headerImgFileName
     */
    public function setHeaderImgFileName($headerImgFileName)
    {

        $this->headerImgFileName = $headerImgFileName;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgAlt()
    {

        return $this->headerImgAlt;
    }

    /**
     * @param mixed $headerImgAlt
     */
    public function setHeaderImgAlt($headerImgAlt)
    {

        $this->headerImgAlt = $headerImgAlt;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgDelete()
    {

        return $this->headerImgDelete;
    }

    /**
     * @param mixed $headerImgDelete
     */
    public function setHeaderImgDelete($headerImgDelete)
    {

        $this->headerImgDelete = $headerImgDelete;
    }


    /**
     * @return mixed
     */
    public function getHeaderImg()
    {

        return $this->headerImg;
    }

    /**
     * @param mixed $headerImg
     */
    public function setHeaderImg($headerImg)
    {

        $this->headerImg = $headerImg;
        if ($headerImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgData()
    {

        return $this->headerImgData;
    }

    /**
     * @return mixed
     */
    public function getisEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getIndirizzo2()
    {

        return $this->indirizzo2;
    }

    /**
     * @param mixed $indirizzo2
     */
    public function setIndirizzo2($indirizzo2)
    {

        $this->indirizzo2 = $indirizzo2;
    }

    /**
     * @return mixed
     */
    public function getCoordinate()
    {

        return $this->coordinate;
    }

    /**
     * @param mixed $coordinate
     */
    public function setCoordinate($coordinate)
    {

        $this->coordinate = $coordinate;
    }

    /**
     * @return mixed
     */
    public function getZoom()
    {

        return $this->zoom;
    }

    /**
     * @param mixed $zoom
     */
    public function setZoom($zoom)
    {

        $this->zoom = $zoom;
    }

    /**
     * @return mixed
     */
    public function getDepartments()
    {

        return $this->departments;
    }

    /**
     * Add Department
     * @param Department $Department
     * @return $this
     */
    public function addDepartment(Department $Department)
    {

        $Department->setSede($this);
        $this->departments[] = $Department;

        return $this;
    }

    /**
     * @param Department $Department
     */
    public function removeDepartment(Department $Department)
    {

        $this->departments->removeElement($Department);
    }

    public function getUploadDir()
    {

        return 'files/sedi/'.$this->getId().'/';

    }

    public function __toString()
    {

        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getCodice()
    {

        return $this->codice;
    }

    /**
     * @param mixed $codice
     */
    public function setCodice($codice)
    {

        $this->codice = $codice;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {

        $this->sort = $sort;
    }

    /**
     * Add dipendenti
     *
     * @param Dipendente $dipendenti
     *
     * @return Sede
     */
    public function addDipendentus(Dipendente $dipendenti)
    {

        $dipendenti->addSedus($this);
        $this->dipendenti[] = $dipendenti;

        return $this;
    }

    /**
     * Remove dipendenti
     *
     * @param Dipendente $dipendenti
     *
     */
    public function removeDipendentus(Dipendente $dipendenti)
    {

        $dipendenti->removeSedus($this);
        $this->dipendenti->removeElement($dipendenti);
    }

    /**
     * Get dipendenti
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDipendenti()
    {

        return $this->dipendenti;
    }
}
