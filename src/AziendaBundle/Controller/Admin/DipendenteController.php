<?php

namespace AziendaBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AziendaBundle\Entity\Dipendente;
use AziendaBundle\Form\DipendenteForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_DIPENDENTI')")
 */
class DipendenteController extends Controller
{

    /**
     * @Route("/dipendenti", name="dipendenti")
     */
    public function listAction()
    {

        return $this->render('@Azienda/admin/dipendenti/list.html.twig');

    }

    /**
     * @Route("/dipendenti/json", name="dipendenti_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.azienda.services.dipendente_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/dipendenti/new", name="dipendenti_new")
     * @Route("/dipendenti/edit/{id}",  name="dipendenti_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');

        $em = $this->getDoctrine()->getManager();

        $Dipendente = new Dipendente();

        $id = null;

        $supportData = [];
        $supportData['listImgUrl'] = false;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Dipendente = $em->getRepository('AziendaBundle:Dipendente')->findOneBy(['id' => $id]);

            if (!$Dipendente) {
                return $this->redirectToRoute('dipendenti_new');
            }

            if ($Dipendente->getListImgFileName()) {
                $supportData['listImgUrl'] = '/'.$Dipendente->getUploadDir().$Dipendente->getListImgFileName();
            }

        }

        $form = $this->createForm(
            DipendenteForm::class,
            $Dipendente,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Dipendente Dipendente
             */

            $Dipendente = $form->getData();

            $new = false;

            if (!$Dipendente->getId()) {
                $new = true;
            }

            $cancellaListPrecedente = $request->get('dipendente_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($Dipendente, 'listImg');
                $Dipendente->setListImg(null);
                $Dipendente->setListImgAlt('');
            }

            $em->persist($Dipendente);
            $em->flush();

            $elemento = (string)$Dipendente;


            if (!$new) {
                $this->addFlash(
                    'success',
                    'Dipendente "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Dipendente "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('dipendenti');

        }

        $view = '@Azienda/admin/dipendenti/new.html.twig';

        if ($Dipendente->getId()) {
            $view = '@Azienda/admin/dipendenti/edit.html.twig';
        }

        return $this->render(
            $view,
            ['form' => $form->createView(), 'supportData' => $supportData]
        );


    }

    /**
     * @Route("/dipendenti/toggle-enabled/{id}", name="dipendenti_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Dipendente $Dipendente)
    {

        $elemento = (string)$Dipendente;

        $em = $this->getDoctrine()->getManager();

        $flash = 'Dipendente '.$elemento.' ';

        if ($Dipendente->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Dipendente->setIsEnabled(!$Dipendente->getIsEnabled());

        $em->persist($Dipendente);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('dipendenti');


    }

    /**
     * @Route("/dipendenti/delete/{id}/{force}", name="dipendenti_delete",  requirements={"id" = "\d+"},
     *     defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Dipendente $Dipendente)
    {

        if ($Dipendente) {

            $elemento = (string)$Dipendente;

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Dipendente->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Dipendente);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Dipendente "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Dipendente "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Dipendente->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Dipendente "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($Dipendente);
                $em->flush();


            }

        }

        return $this->redirectToRoute('dipendenti');

    }

    /**
     * @Route("/dipendenti/restore/{id}", name="dipendenti_restore")
     */
    public function restoreAction(Request $request, Dipendente $Dipendente)
    {

        if ($Dipendente->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = (string)$Dipendente;

            $Dipendente->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Dipendente "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('dipendenti');

    }

}