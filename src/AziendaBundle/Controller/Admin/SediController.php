<?php

namespace AziendaBundle\Controller\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use AziendaBundle\Entity\Department;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AziendaBundle\Entity\Sede;
use AziendaBundle\Form\SedeForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_SEDI')")
 */
class SediController extends Controller
{

    /**
     * @Route("/sedi", name="sedi")
     */
    public function listAction()
    {

        return $this->render('@Azienda/admin/sedi/list.html.twig');

    }

    /**
     * @Route("/sedi/json", name="sedi_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.azienda.services.sede_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/sedi/new", name="sedi_new")
     * @Route("/sedi/edit/{id}",  name="sedi_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');

        $em = $this->getDoctrine()->getManager();

        $Sede = new Sede();

        $id = null;

        $errors = false;
        $supportData = [];
        $supportData['headerImgUrl'] = false;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Sede = $em->getRepository('AziendaBundle:Sede')->findOneBy(['id' => $id]);

            if (!$Sede) {
                return $this->redirectToRoute('sedi_new');
            }

            if ($Sede->getHeaderImgFileName()) {
                $supportData['headerImgUrl'] = '/'.$Sede->getUploadDir().$Sede->getHeaderImgFileName();
            }

        }

        $dipartimentiOriginali = new ArrayCollection();

        // Crea an ArrayCollection dei dipartimenti attuali
        foreach ($Sede->getDepartments() as $Department) {
            $dipartimentiOriginali->add($Department);
        }


        $form = $this->createForm(
            SedeForm::class,
            $Sede,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            /**
             * @var $Sede Sede
             */

            foreach ($dipartimentiOriginali as $Department) {
                if (false === $Sede->getDepartments()->contains($Department)) {
                    /**
                     * @var $Department Department
                     */
                    $em->remove($Department);
                }
            }

            $Sede = $form->getData();

            $cancellaHeaderPrecedente = $request->get('sede_form')['headerImgDelete'];
            if ($cancellaHeaderPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($Sede, 'headerImg');
                $Sede->setHeaderImg(null);
                $Sede->setHeaderImgAlt('');
            }


            $new = false;

            if (!$Sede->getId()) {
                $new = true;
            }

            $em->persist($Sede);
            $em->flush();

            $elemento = $Sede->getNome();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Sedi "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Sedi "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('sedi');

        } else {
            $errors = [];
            $trans = $this->get('translator');
            foreach ($form->getErrors(true) as $error) {
                /**
                 * @var $error FormError
                 */

                /**
                 * @var $Config FormConfigInterface
                 */
                $Config = $error->getOrigin()->getConfig();

                $lbl = $Config->getOptions()['label'];
                if ($lbl) {
                    $lbl = $trans->trans($lbl);
                } else {
                    $lbl = ucfirst($Config->getName());
                }
                $errors[] = $lbl.': '.$error->getMessage();
            }

            $errors = array_unique($errors);
            $errors = implode('<br />', $errors);

        }

        $view = '@Azienda/admin/sedi/new.html.twig';

        if ($Sede->getId()) {
            $view = '@Azienda/admin/sedi/edit.html.twig';
        }

        $formView = $form->createView();

        return $this->render($view, ['form' => $formView, 'supportData' => $supportData, 'errors' => $errors]);


    }

    /**
     * @Route("/sedi/toggle-enabled/{id}", name="sedi_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Sede $Sede)
    {

        $elemento = '"'.$Sede->getNome().'" ('.$Sede->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Sedi '.$elemento.' ';

        if ($Sede->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Sede->setIsEnabled(!$Sede->getIsEnabled());

        $em->persist($Sede);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('sedi');


    }

    /**
     * @Route("/sedi/delete/{id}/{force}", name="sedi_delete",  requirements={"id" = "\d+"}, defaults={"force" =
     *     false}))
     */
    public function deleteAction(Request $request, Sede $Sede)
    {

        if ($Sede) {

            $elemento = $Sede->getNome();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Sede->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Sede);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Sedi "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Sedi "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Sede->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Sedi "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($Sede);
                $em->flush();


            }

        }

        return $this->redirectToRoute('sedi');

    }

    /**
     * @Route("/sedi/restore/{id}", name="sedi_restore")
     */
    public function restoreAction(Request $request, Sede $Sede)
    {

        if ($Sede->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Sede->getNome().' ('.$Sede->getId().')';

            $Sede->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Sedi "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('sedi');

    }

}