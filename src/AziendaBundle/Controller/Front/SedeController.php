<?php
// 21/07/17, 6.25
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AziendaBundle\Controller\Front;

use AziendaBundle\Entity\Sede;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SedeController extends Controller
{
    /**
     * @Route("/sede/{slug}", defaults={"_locale"="it"}, name="sede_it")
     * @Route("/{_locale}/sede/{slug}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"},
     *     defaults={"_locale"="it"}, name="sede")
     */
    public function readAction(Request $request, $slug, $_locale)
    {

        $em = $this->getDoctrine()->getManager();
        $Languages = $this->get('app.languages');
        $pathManager = $this->get('app.path_manager');
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');


        if ($slug) {

            /**
             * @var $Sede Sede
             */
            $Sede = $em->getRepository('AziendaBundle:Sede')->findBySlug(
                $slug,
                $_locale
            );

            if ($Sede) {

                if ($Sede->getIsEnabled()) {

                    $AdditionalData = [];
                    $AdditionalData['Entity'] = $Sede;
                    $AdditionalData['langs'] = $Languages->getActivePublicLanguages();

                    $META = [];
                    $META['title'] = $Sede->translate($request->getLocale())->getMetaTitle();
                    $META['description'] = $Sede->translate($request->getLocale())->getMetaDescription();
                    $META['alternate'] = [];

                    $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
                    $linguePreferite = explode(',', $linguePreferite);

                    foreach ($AdditionalData['langs'] as $sigla => $estesa) {
                        $key = $sigla;
                        if ($key == $linguePreferite[0]) {
                            $key = 'x-default';
                        }

                        $META['alternate'][$key] = $pathManager->generate(
                            'sede',
                            ['slug' => $Sede->translate($sigla)->getSlug()],
                            $sigla
                        );
                    }

                    $AdditionalData['META'] = $META;

                    $twigs = $TemplateLoader->getTwigs(
                        'sede',
                        $Languages->getActivePublicLanguages(),
                        $AdditionalData
                    );

                    $META = $MetaManager->merge($twigs, $META);

                    if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

                        $metaVars = [
                            '{NOME}' => $Sede->getNome(),
                            '{INDIRIZZO}' => $Sede->getIndirizzo().' - '.$Sede->getIndirizzo2(),
                        ];

                        if (!isset($META['description']) || !$META['description']) {
                            $META = $MetaManager->manageDefaults(
                                'description',
                                'sedi',
                                $META,
                                $metaVars
                            );
                        }
                        if (!isset($META['title']) || !$META['title']) {
                            $META = $MetaManager->manageDefaults('title', 'sedi', $META, $metaVars);
                        }
                    }

                    return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

                }
            }

        }

        throw new NotFoundHttpException("Pagina non trovata");


    }

}