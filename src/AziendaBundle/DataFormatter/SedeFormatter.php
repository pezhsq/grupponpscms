<?php
// 20/07/17, 13.56
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AziendaBundle\DataFormatter;


use AppBundle\DataFormatter\DataFormatter;
use AppBundle\Entity\Dipendente;
use AziendaBundle\Entity\Department;
use AziendaBundle\Entity\Orario;
use AziendaBundle\Entity\Sede;
use Webtek\EcommerceBundle\Entity\Brand;

class SedeFormatter extends DataFormatter
{

    public function getData()
    {
        // TODO: Implement getData() method.
    }

    public function extractData()
    {
        // TODO: Implement extractData() method.
    }

    public function getSedi()
    {

        $data = [];

        $Sedi = $this->em->getRepository('AziendaBundle\Entity\Sede')->findBy(['isEnabled' => 1], ['sort' => 'ASC']);

        $sedi = [];

        foreach ($Sedi as $Sede) {

            /**
             * @var $Sede Sede
             */

            $sede = [];
            $sede['id'] = $Sede->getId();
            $sede['nome'] = $Sede->getNome();
            $sede['indirizzo1'] = $Sede->getIndirizzo();
            $sede['indirizzo2'] = $Sede->getIndirizzo2();
            $sede['telefono'] = $Sede->getTelefono();
            $sede['fax'] = $Sede->getFax();
            $sede['email'] = $Sede->getEmail();
            $sede['img'] = false;
            if ($Sede->getHeaderImgFileName()) {
                $sede['img'] = $Sede->getUploadDir().$Sede->getHeaderImgFileName();
                $sede['alt'] = $Sede->getHeaderImgAlt();
            } else {
                $sede['alt'] = $sede['nome'];
            }
            $sede['url'] = $this->container->get('app.path_manager')->generate(
                'sede',
                ['slug' => $Sede->translate()->getSlug()]
            );

            $sedi[] = $sede;


        }

        $data['sedi'] = $sedi;

        return $data;

    }

    public function getHeader()
    {

        $data = [];
        $data['img'] = false;
        $data['alt'] = '';
        $data['nomeSede'] = 'Sede non trovata';
        $data['indirizzo1'] = '';
        $data['indirizzo2'] = '';
        $data['telefono'] = '';
        $data['telefonoProtocol'] = '';
        $data['fax'] = '';
        $data['email'] = '';
        $data['coordinate'] = '46.1712597,9.8693336';
        $data['zoom'] = 13;

        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Sede) {

            /**
             * @var $Sede Sede
             */
            $Sede = $this->AdditionalData['Entity'];

            if ($Sede->getHeaderImgFileName()) {
                $data['img'] = $Sede->getUploadDir().$Sede->getHeaderImgFileName();
                $data['alt'] = $Sede->getHeaderImgAlt();
            }

            $data['nome'] = $Sede->getNome();
            $data['indirizzo1'] = $Sede->getIndirizzo();
            $data['indirizzo2'] = $Sede->getIndirizzo2();
            $data['telefono'] = $Sede->getTelefono();
            $data['telefonoProtocol'] = preg_replace('/\D/', '', $data['telefono']);
            $data['fax'] = $Sede->getFax();
            $data['email'] = $Sede->getEmail();
            $data['coordinate'] = $Sede->getCoordinate();
            $data['zoom'] = $Sede->getZoom();

        }

        return $data;

    }

    public function getDipartimenti()
    {

        $data = [];
        $dipartimenti = [];

        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Sede) {
            /**
             * @var $Sede Sede
             */
            $Sede = $this->AdditionalData['Entity'];

            $Departments = $Sede->getDepartments();

            foreach ($Departments as $department) {

                /**
                 * @var $department Department
                 */
                $dipartimento = [];
                $dipartimento['nome'] = $department->translate()->getTitolo();
                $dipartimento['email'] = $department->getEmail();
                $dipartimento['link'] = $department->translate()->getLink();
                $dipartimento['img'] = false;
                $dipartimento['alt'] = $dipartimento['nome'];

                if ($department->getFoto()) {
                    $dipartimento['img'] = $department->getUploadDir().$department->getFoto();
                }

                $Orari = $department->getOrari();

                $orari = [];

                foreach ($Orari as $Orario) {

                    /**
                     * @var $Orario Orario
                     */

                    if (!isset($orari[$Orario->getTipo()])) {
                        $orari[$Orario->getTipo()] = [];
                    }
                    if (!isset($orari[$Orario->getTipo()][$Orario->getEtichetta()])) {
                        $orari[$Orario->getTipo()][$Orario->getEtichetta()] = [];
                    }

                    if ($Orario->getDalle() || $Orario->getAlle()) {
                        $record = [];
                        $record['dalle'] = $Orario->getDalle();
                        $record['alle'] = $Orario->getAlle();
                        $orari[$Orario->getTipo()][$Orario->getEtichetta()][] = $record;
                    }


                }

                $dipartimento['orari'] = $orari;

                $dipartimenti[] = $dipartimento;

            }

        }

        $data['dipartimenti'] = $dipartimenti;

        return $data;

    }

    public function getDipendenti()
    {

        $data = [];

        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Sede) {

            $dipendenti = [];

            /**
             * @var $Sede Sede
             */
            $Sede = $this->AdditionalData['Entity'];

            $Dipendenti = $Sede->getDipendenti();

            foreach ($Dipendenti as $Dipendente) {

                /**
                 * @var $Dipendente Dipendente
                 */
                $dipendente = [];
                $dipendente['nome'] = $Dipendente->getNome();
                $dipendente['cognome'] = $Dipendente->getCognome();
                $dipendente['telefono'] = $Dipendente->getTelefono();
                $dipendente['email'] = $Dipendente->getEmail();
                $dipendente['img'] = false;
                $dipendente['alt'] = $Dipendente->getNome().' '.$Dipendente->getCognome();
                if ($Dipendente->getListImgFileName()) {
                    $dipendente['img'] = $Dipendente->getUploadDir().$Dipendente->getListImgFileName();
                    $dipendente['alt'] = $Dipendente->getListImgAlt();
                }

                $dipendente['marchi'] = [];

                $marchi = $Dipendente->getBrands();

                foreach ($marchi as $marchio) {

                    /**
                     * @var $marchio Brand
                     */

                    if ($marchio->getListImgFileName()) {

                        $brand = [];
                        $brand['nome'] = $marchio->translate()->getTitolo();
                        $brand['img'] = $marchio->getUploadDir().$marchio->getListImgFileName();
                        $brand['alt'] = $marchio->getListImgAlt();

                        $dipendente['marchi'][] = $brand;

                    }

                }

                $dipendenti[] = $dipendente;

            }

            $data['dipendenti'] = $dipendenti;

        }

        return $data;


    }

    public function getSediMappa()
    {

        $data = [];

        $Sedi = $this->em->getRepository('AziendaBundle:Sede')->findAllNotDeleted(true);

        $sedi = [];

        foreach ($Sedi as $Sede) {

            /**
             * @var $Sede Sede
             */

            $info = $Sede->getNome();
            $info .= "<br />";
            $info .= $Sede->getIndirizzo();
            $info .= "<br />";
            $info .= $Sede->getIndirizzo2();

            $sede = array_merge([$info], explode(',', $Sede->getCoordinate()));

            $sedi[] = $sede;

        }

        $data['sedi'] = json_encode($sedi);

        return $data;

    }


}