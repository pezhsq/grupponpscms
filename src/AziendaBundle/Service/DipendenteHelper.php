<?php
// 27/06/17, 18.06
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AziendaBundle\Service;

use AziendaBundle\Entity\Dipendente;
use Doctrine\ORM\EntityManager;

class DipendenteHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BrandsHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Dipendenti = $this->entityManager->getRepository('AziendaBundle:Dipendente')->findAll();
        } else {
            $Dipendenti = $this->entityManager->getRepository('AziendaBundle:Dipendente')->findAllNotDeleted();
        }

        $records = [];

        foreach ($Dipendenti as $Dipendente) {

            /**
             * @var $Dipendente Dipendente;
             */
            $record = [];
            $record['id'] = $Dipendente->getId();
            $record['nome'] = $Dipendente->getNome().' '.$Dipendente->getCognome();
            $record['mansione'] = $Dipendente->translate()->getMansione();
            $record['telefono'] = $Dipendente->getTelefono();
            $record['email'] = $Dipendente->getEmail();
            $record['deleted'] = $Dipendente->isDeleted();
            $record['isEnabled'] = $Dipendente->getIsEnabled();
            $record['createdAt'] = $Dipendente->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Dipendente->getUpdatedAt()->format('d/m/Y H:i:s');

            $records[] = $record;
        }

        return $records;

    }

}