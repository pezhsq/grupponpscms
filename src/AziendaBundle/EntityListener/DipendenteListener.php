<?php
// 22/06/17, 12.12
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AziendaBundle\EntityListener;

use AppBundle\Service\AttachmentFSManager;
use AppBundle\Service\WebDir;
use AziendaBundle\Entity\Dipendente;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Doctrine\ORM\Mapping as ORM;

class DipendenteListener
{

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;
    /**
     * @var TokenStorage
     */
    private $token;
    /**
     * @var Slugify
     */
    private $slugify;
    private $apiKey;
    /**
     * @var WebDir
     */
    private $webDir;
    /**
     * @var AttachmentFSManager
     */
    private $attachmentFSManager;

    public function __construct(
        WebDir $webDir,
        AttachmentFSManager $attachmentFSManager
    ) {

        $this->webDir = $webDir;
        $this->attachmentFSManager = $attachmentFSManager;
    }

    /**
     * @ORM\PostPersist()
     * @param Dipendente $Dipendente
     * @param LifecycleEventArgs $event
     */
    public function postPersist(Dipendente $Dipendente, LifecycleEventArgs $event)
    {

        $em = $event->getEntityManager();
        if ($Dipendente->getListImgFileName()) {
            if (!is_dir($this->webDir->get().'/'.$Dipendente->getUploadDir())) {
                mkdir($this->webDir->get().'/'.$Dipendente->getUploadDir(), 0755, true);
            }
            rename(
                $this->webDir->get().'/'.$Dipendente->getUploadDir().'../'.$Dipendente->getListImgFileName(),
                $this->webDir->get().'/'.$Dipendente->getUploadDir().$Dipendente->getListImgFileName()
            );
        }
        $this->attachmentFSManager->moveFiles($Dipendente);


    }

}