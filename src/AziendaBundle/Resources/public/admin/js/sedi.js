var modulo = 'sedi';
var map;
var marker;
var head = document.getElementsByTagName('head')[0];

// Save the original method
var insertBefore = head.insertBefore;

// Replace it!
head.insertBefore = function(newElement, referenceElement) {

    if (newElement.href && newElement.href.indexOf('https://fonts.googleapis.com/css?family=Roboto') === 0) {

        console.info('Prevented Roboto from loading!');
        return;
    }

    insertBefore.call(head, newElement, referenceElement);
};

$(function() {

    sezione = window.location.getPositionalUrl(2);

    switch (sezione) {

        case 'new':
        case 'edit':

            $('.btn-geocode').on('click', function(e) {
                e.preventDefault();
                geocode();
            });

            $(document).on('click', '.btn-remove-departement', function(e) {
                e.preventDefault();
                var titolo = $(this).closest('.x_content').find('.titolo').val();
                var $x_panel = $(this).closest('.x_panel');
                var opts = {
                    'type': 'danger',
                    'titolo': _('Rimozione dipartimento'),
                    'content': _('Vuoi rimuovere il dipartimento "%s"?', 'sedi', titolo),
                    'OK': 'Ok',
                    'CANCEL': 'Annulla',
                    'onOK': function($dialog) {
                        $x_panel.remove();
                        $dialog.modal('hide');
                    },
                    'onCANCEL': null
                };
                HandleBarHelper.confirm(opts);
            });

            $(document).on('click', '.btn-add-departement', function(e) {
                e.preventDefault();
                var html = $('#sede_formTemplate').html();
                index = $('.department-panel').length;

                html = html.replace(/__name__/g, index);
                var $html = $(html);
                $html.find('label:first').remove();
                $html.insertBefore($('.btn-add-departement'));
            });

            $(document).on('click', '.btn-add-orario', function(e) {
                e.preventDefault();
                var $orario = $(this).closest('.orari');
                var markup = $orario.data('prototype');

                index = $orario.find('.orario').length;

                $markup = $('<div class="form-group">' + markup.replace(/__name__/g, index) + '</div>');
                $markup.find('.sort').val(index);

                $markup.insertBefore(this);
                $orari = $(this).closest('.orari');
                setSort($orari);
            });

            $(document).on('click', '.btn-move-up', function(e) {
                e.preventDefault();
                var $thisBlock = $(this).closest('.orario').closest('.form-group');
                var $prevBlock = $thisBlock.prev();
                $thisBlock.insertBefore($prevBlock);
                $orari = $(this).closest('.orari');
                setSort($orari);
            });

            $(document).on('click', '.btn-move-down', function(e) {
                e.preventDefault();
                var $thisBlock = $(this).closest('.orario').closest('.form-group');
                var $nextBlock = $thisBlock.next();
                $thisBlock.insertAfter($nextBlock);
                $orari = $(this).closest('.orari');
                setSort($orari);
            });


            $(document).on('click', '.btn-remove-orario', function(e) {
                e.preventDefault();

                var $orario = $(this).closest('.orario');

                var opts = {
                    'type': 'danger',
                    'titolo': _('Rimozione dipartimento'),
                    'content': _('Vuoi rimuovere questa fascia oraria?', 'sedi'),
                    'OK': 'Ok',
                    'CANCEL': 'Annulla',
                    'onOK': function($dialog) {
                        $orario.remove();
                        $dialog.modal('hide');
                    },
                    'onCANCEL': null
                };
                HandleBarHelper.confirm(opts);

            });

            $('.orari').each(function() {
                $(this).find('.orario:first').find('.btn-move-up').addClass('disabled');
                $(this).find('.orario:last').find('.btn-move-down').addClass('disabled');
            });


            break;

        case null:

            /** Datatable della pagina di elenco */
            var cols = [];

            var col = {
                'title': 'Modifica',
                'className': 'dt0 dt-body-center',
                'searchable': false
            };
            cols.push(col);

            col = {
                'title': 'Nome',
                'className': 'dt2',
                searchable: true,
                data: 'nome'
            };

            cols.push(col);

            col = {
                'title': 'Slug',
                'className': 'dt2',
                searchable: true,
                data: 'slug'
            };

            cols.push(col);

            col = {
                'title': 'Creato',
                'className': 'dt6',
                searchable: true,
                data: 'createdAt'
            };

            cols.push(col);

            col = {
                'title': 'Ultima modifica',
                'className': 'dt7',
                searchable: true,
                data: 'updatedAt'
            };

            cols.push(col);


            col = { 'className': 'dt-body-center' };

            // placeholder attivazione
            cols.push(col);

            // placeholder cancellazione
            cols.push(col);


            var columnDefs = [];

            var columnDef = {
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'dt-body-center',
                render: function(data, type, full, meta) {
                    var toString = full.nome;
                    return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                }
            };

            columnDefs.push(columnDef);

            columnDef = {
                targets: cols.length - 2,
                searchable: false,
                className: 'dt-body-center',
                orderable: false,
                render: function(data, type, full, meta) {
                    var toString = full.nome;
                    var icon = 'check-square-o';
                    var title = 'Disabilita: ';
                    if (!full.isEnabled) {
                        var icon = 'square-o';
                        var title = 'Abilita: ';
                    }
                    var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                    ret += '<i class="fa fa-' + icon + '"></i>';
                    ret += '</a>';
                    return ret;
                }
            };

            columnDefs.push(columnDef);

            columnDef = {
                targets: cols.length - 1,
                searchable: false,
                className: 'dt-body-center',
                orderable: false,
                render: function(data, type, full, meta) {
                    var toString = full.nome;
                    console.log(full);
                    if (full.deleted) {
                        var title = _('Recupera:');
                        var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                        ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                        var title = _('Elimina definitivamente: ');
                        ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                        if (parseInt(full.cancellabile, 10) == 0) {
                            ret += ' disabled';
                        }
                        ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                    } else {
                        var title = 'Elimina: ';
                        var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                        if (parseInt(full.cancellabile, 10) == 0) {
                            ret += ' disabled';
                        }
                        ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                    }
                    return ret;
                }
            };

            columnDefs.push(columnDef);


            $('#datatable').dataTable({
                ajax: {
                    "url": "/admin/" + modulo + "/json"
                },
                aaSorting: [[2, 'asc']],
                stateSave: true,
                iDisplayLength: 15,
                responsive: true,
                columns: cols,
                columnDefs: columnDefs,
                createdRow: function(row, data, index) {
                    if (data.deleted) {
                        $(row).addClass("danger");
                    }
                }
            });

            break;
    }


});
/** FINE document.ready **/



function initMap() {
    var coordinate = $('.coordinate').val().split(',');
    var zoom = parseInt($('.zoom').val(), 10);

    var point = { 'lat': parseFloat(coordinate[0]), 'lng': parseFloat(coordinate[1]) };

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoom,
        center: point
    });
    marker = new google.maps.Marker({
        position: point,
        map: map,
        draggable: true
    });

    map.addListener('zoom_changed', function() {
        $('.zoom').val(map.getZoom());
    });

    google.maps.event.addListener(marker, "dragend", function(event) {
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();

        $('.coordinate').val(lat + ',' + lng);
    });
}

function geocode() {
    var addr = [];

    $('.mapContainer').find('input[type="text"]').each(function() {
        addr.push($(this).val());
    });

    var address = '';
    if (addr.length) {
        address = addr.join(',');
    }


    geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, function(results, status) {
        if (status === 'OK') {
            map.setCenter(results[0].geometry.location);
            $('.coordinate').val(results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng());
            marker.setPosition(results[0].geometry.location);
        } else {
            var msg = _('Non sono riuscito a reperire la posizione: ');
            switch (status) {
                case 'ZERO_RESULTS':
                    msg += _('Nessun risultato', 'maps');
                    break;
                case 'INVALID_REQUEST':
                    msg += _('Formato indirizzo errato', 'maps');
                    break;
                default:
                    msg += status;
            }
            var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': msg,
                'OK': 'Ok',
                'callback': null
            };
            HandleBarHelper.alert(opts);

        }
    });
}

function setSort($orari) {
    $orari.find('.btn').removeClass('disabled');
    $orari.find('.orario:first').find('.btn-move-up').addClass('disabled');
    $orari.find('.orario:last').find('.btn-move-down').addClass('disabled');

    $orari.find('.sort').each(function(index) {
        $(this).val(index);
    });

}