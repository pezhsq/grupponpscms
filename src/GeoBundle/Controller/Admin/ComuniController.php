<?php
// 19/04/17, 15.43
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin")
 */
class ComuniController extends Controller
{

    /**
     * @Route("/comuni/options", name="comuni_options")
     */
    public function getSelectOptions(Request $request)
    {

        $provincia = $request->request->get("provincia");

        $provinciaHelper = $this->get('app.geo.comune_helper');

        $return            = [];
        $return['result']  = true;
        $return['options'] = $provinciaHelper->getOptions($provincia, $request->getLocale());

        return new JsonResponse($return);

    }

}