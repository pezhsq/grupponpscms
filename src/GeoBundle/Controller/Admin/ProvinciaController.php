<?php
// 19/04/17, 11.43
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Controller\Admin;

use GeoBundle\Entity\Provincia;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin")
 */
class ProvinciaController extends Controller
{

    /**
     * @Route("/provincia/options", name="provincia_options")
     */
    public function getSelectOptions(Request $request)
    {

        $nazione = $request->request->get("nazione");

        $provinciaHelper = $this->get('app.geo.provincia_helper');

        $return            = [];
        $return['result']  = true;
        $return['options'] = $provinciaHelper->getOptions($nazione, $request->getLocale());

        return new JsonResponse($return);

    }

}