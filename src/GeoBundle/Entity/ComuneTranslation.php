<?php
// 19/04/17, 9.28
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="comuni_translations")
 */
class ComuneTranslation
{

    use ORMBehaviours\Translatable\Translation;

    /**
     * @ORM\Column(type="string")
     */
    private $nome;

    /**
     * @return mixed
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }


}