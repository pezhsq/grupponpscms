<?php
// 19/04/17, 8.52
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="regioni")
 */
class Regione
{

    use ORMBehaviours\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="GeoBundle\Entity\Provincia", mappedBy="province")
     */
    private $province;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $nazione;

    /**
     * @ORM\OneToMany(targetEntity="GeoBundle\Entity\Comune", mappedBy="comuni")
     */
    private $comuni;


    /**
     * Regione constructor.
     */
    public function __construct()
    {

        $this->province = new ArrayCollection();
        $this->comuni   = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {

        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {

        return $this->province;
    }

    /**
     * @param mixed $province
     */
    public function setProvince($province)
    {

        $this->province = $province;
    }

    /**
     * @return mixed
     */
    public function getComuni()
    {

        return $this->comuni;
    }

    /**
     * @param mixed $comuni
     */
    public function setComuni($comuni)
    {

        $this->comuni = $comuni;
    }

    /**
     * @return mixed
     */
    public function getNazione()
    {

        return $this->nazione;
    }

    /**
     * @param mixed $nazione
     */
    public function setNazione($nazione)
    {

        $this->nazione = $nazione;
    }


}