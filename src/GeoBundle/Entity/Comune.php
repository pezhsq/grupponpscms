<?php
// 19/04/17, 9.23
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="comuni")
 */
class Comune
{

    use ORMBehaviours\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Provincia", inversedBy="comuni")
     */
    private $provincia;

    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Regione", inversedBy="comuni")
     */
    private $regione;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $nazione;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {

        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getProvincia()
    {

        return $this->provincia;
    }

    /**
     * @param mixed $provincia
     */
    public function setProvincia($provincia)
    {

        $this->provincia = $provincia;
    }

    /**
     * @return mixed
     */
    public function getRegione()
    {

        return $this->regione;
    }

    /**
     * @param mixed $regione
     */
    public function setRegione($regione)
    {

        $this->regione = $regione;
    }

    /**
     * @return mixed
     */
    public function getNazione()
    {

        return $this->nazione;
    }

    /**
     * @param mixed $nazione
     */
    public function setNazione($nazione)
    {

        $this->nazione = $nazione;
    }

    function __toString()
    {

        return (string)$this->translate()->getNome();
    }

}