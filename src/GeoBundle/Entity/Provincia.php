<?php
// 19/04/17, 8.48
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * Class Provincia
 * @ORM\Entity(repositoryClass="GeoBundle\Repository\ProvinciaRepository")
 * @ORM\Table(name="province")
 */
class Provincia
{

    use ORMBehaviours\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $sigla;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $nazione;

    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Regione", inversedBy="province")
     */
    private $regione;

    /**
     * @ORM\OneToMany(targetEntity="GeoBundle\Entity\Comune", mappedBy="provincia")
     */
    private $comuni;


    /**
     * Provincia constructor.
     */
    public function __construct()
    {

        $this->comuni = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {

        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getRegione()
    {

        return $this->regione;
    }

    /**
     * @param mixed $regione
     */
    public function setRegione($regione)
    {

        $this->regione = $regione;
    }

    /**
     * @return mixed
     */
    public function getComuni()
    {

        return $this->comuni;
    }

    /**
     * @param mixed $comuni
     */
    public function setComuni($comuni)
    {

        $this->comuni = $comuni;
    }

    /**
     * @return mixed
     */
    public function getSigla()
    {

        return $this->sigla;
    }

    /**
     * @param mixed $sigla
     */
    public function setSigla($sigla)
    {

        $this->sigla = $sigla;
    }

    /**
     * @return mixed
     */
    public function getNazione()
    {

        return $this->nazione;
    }

    /**
     * @param mixed $nazione
     */
    public function setNazione($nazione)
    {

        $this->nazione = $nazione;
    }

    function __toString()
    {

        return (string)$this->translate()->getNome();
    }


}