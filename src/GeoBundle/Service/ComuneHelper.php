<?php
// 19/04/17, 15.44
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Service;


use Doctrine\Bundle\DoctrineBundle\Registry;
use GeoBundle\Entity\Comune;

class ComuneHelper
{

    /**
     * @var Registry
     */
    private $doctrine;

    public function __construct(Registry $doctrine)
    {

        $this->doctrine = $doctrine;
    }

    public function getOptions($provinciaId, $locale, $format = 'json')
    {

        $options = [];

        $repo = $this->doctrine->getRepository('GeoBundle:Provincia');

        $Provincia = $repo->findOneBy(['id' => $provinciaId]);

        if ($Provincia) {

            $Comuni = $Provincia->getComuni();

            foreach ($Comuni as $Comune) {

                if ($format == 'json') {

                    /**
                     * @var $Comune Comune
                     */
                    $record         = [];
                    $record['id']   = $Comune->getId();
                    $record['nome'] = $Comune->translate()->getNome();

                    $options[] = $record;

                } else {

                    $options[$Comune->translate()->getNome()] = $Comune->getId();

                }

            }
        }

        return $options;


    }
}