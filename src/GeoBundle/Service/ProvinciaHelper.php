<?php
// 19/04/17, 14.11
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace GeoBundle\Service;


use Doctrine\Bundle\DoctrineBundle\Registry;

class ProvinciaHelper
{

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * ProvinciaHelper constructor.
     */
    public function __construct(Registry $doctrine)
    {

        $this->doctrine = $doctrine;
    }

    public function getOptions($nazione, $locale, $format = 'json')
    {

        $repo = $this->doctrine->getRepository('GeoBundle:Provincia');

        $Province = $repo->findByNation($nazione, $locale);

        $options = [];

        foreach ($Province as $Provincia) {

            if ($format == 'json') {

                /**
                 * @var $Provincia Provincia
                 */
                $record         = [];
                $record['id']   = $Provincia->getId();
                $record['nome'] = $Provincia->translate()->getNome();

                $options[] = $record;
            } else {
                
                $options[$Provincia->translate()->getNome()] = $Provincia->getId();

            }

        }

        return $options;


    }

}