<?php
// 27/04/17, 14.26
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\Attribute;
use Webtek\EcommerceBundle\Entity\GroupAttribute;
use Webtek\EcommerceBundle\Entity\Listino;

class ListinoForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'isDefault',
            ChoiceType::class,
            [
                'label'       => 'listino.labels.is_default',
                'choices'     => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required'    => false,
                'help'        => 'listino.help.is_default',
            ]
        );

        $fields = [
            'titolo' => [
                'label'    => 'listino.labels.titolo',
                'required' => true,
                'attr'     => ['class' => 'titolo'],
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales'          => array_keys($options['langs']),
                'fields'           => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'     => Listino::class,
                'error_bubbling' => true,
                'langs'          => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}