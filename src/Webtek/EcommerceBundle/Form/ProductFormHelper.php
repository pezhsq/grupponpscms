<?php
// 28/04/17, 14.53
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Form;

use AppBundle\Form\TypeExtension\SeoDescription;
use AppBundle\Form\TypeExtension\SeoTitle;
use AppBundle\Service\Languages;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use TagBundle\Entity\Tag;
use TagBundle\Form\TagForm;
use Webtek\EcommerceBundle\Entity\Brand;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\Delivery;
use Webtek\EcommerceBundle\Entity\Feature;
use Webtek\EcommerceBundle\Entity\GroupAttribute;
use Webtek\EcommerceBundle\Entity\Listino;
use Webtek\EcommerceBundle\Entity\MarketPlace;
use Webtek\EcommerceBundle\Entity\Prezzo;
use Webtek\EcommerceBundle\Entity\PrezzoMarketPlace;
use Webtek\EcommerceBundle\Entity\PrezzoVariante;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\ProductAttachment;
use Webtek\EcommerceBundle\Entity\ProductFeature;
use Webtek\EcommerceBundle\Entity\ProductType;
use Webtek\EcommerceBundle\Entity\Tax;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;
use Webtek\EcommerceBundle\Event\ProductSavedEvent;
use Webtek\EcommerceBundle\Repository\BrandsRepository;
use Webtek\EcommerceBundle\Service\GroupAttributeHelper;
use Webtek\EcommerceBundle\Service\GroupFeatureHelper;
use Webtek\EcommerceBundle\Service\TaxCalculator;

class ProductFormHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Translator
     */
    private $translator;
    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var AuthorizationChecker
     */
    private $auth;
    /**
     * @var GroupAttributeHelper
     */
    private $groupAttributeHelper;
    /**
     * @var GroupFeatureHelper
     */
    private $groupFeatureHelper;

    private $listini;
    /**
     * @var Languages
     */
    private $languages;
    /**
     * @var TaxCalculator
     */
    private $taxCalculator;
    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * ProductFormHelper constructor.
     */
    public function __construct(ContainerInterface $container)
    {


        $this->container = $container;
        $this->entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        $this->formFactory = $this->container->get('form.factory');
        $this->translator = $this->container->get('translator');
        $this->auth = $this->container->get('security.authorization_checker');
        $this->groupAttributeHelper = $this->container->get('app.webtek_ecommerce.services.group_attribute_helper');
        $this->groupFeatureHelper = $this->container->get('app.webtek_ecommerce.services.group_feature_helper');
        $this->languages = $this->container->get('app.languages');
        $this->taxCalculator = $this->container->get('app.webtek_ecommerce.services.tax_calculator');
        $this->marketPlacesHelper = $this->container->get('app.webtek_ecommerce.services.market_place_helper');


    }

    public function createForm(
        $defaultData = [],
        $options
    ) {

        $form = $this->formFactory->createBuilder(
            FormType::class,
            $defaultData,
            [
                'validation_groups' => ['Default', $defaultData['validationGroup']],
                'constraints' => [
                    new Callback([$this, 'checkRedirect']),
                    new Callback([$this, 'checkCategorie']),
                ],
                'mapped' => false,
                'allow_extra_fields' => true,
                'error_bubbling' => true,
                'attr' => ['data-id' => $defaultData['uuid']],
            ]
        );
        $form->add('uuid', HiddenType::class, ['data' => $defaultData['uuid']]);
        /** TAB DESCRITTIVA - INIZIO **/
        $form->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $form->add(
            'available',
            ChoiceType::class,
            [
                'label' => 'products.labels.available',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $form->add(
            'typeOfRedirect',
            ChoiceType::class,
            [
                'label' => 'product.labels.comportamento_offline',
                'choices' => [
                    'product.labels.prodotto_301' => '301P',
                    'product.labels.prodotto_302' => '302P',
                    'product.labels.categoria_301' => '301C',
                    'product.labels.categoria_302' => '302C',
                    'product.labels.errore404' => '404',
                ],
                'constraints' => [
                    new NotBlank(),
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $form->add('targetId', HiddenType::class);
        $form->add(
            'redirectionHelper',
            TextType::class,
            ['label' => 'product.labels.redirection_helper', 'required' => false]
        );
        $form->add(
            'codice',
            TextType::class,
            [
                'required' => true,
                'label' => 'product.labels.codice',
                'constraints' => [
                    new NotBlank(),
                ],
            ]
        );
        $form->add(
            'ean',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.ean',
            ]
        );
        $form->add(
            'codice_produttore',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.codice_produttore',
            ]
        );
        $form->add(
            'features',
            CollectionType::class,
            [
                'entry_type' => ProductFeatureForm::class,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_extra_fields' => true,
                'label' => false,
                'attr' => [],
            ]
        );
        $translation = $this->formFactory->createNamedBuilder(
            'translations',
            FormType::class
        );
        foreach ($this->languages->getActiveLanguages() as $k => $lang) {

            $langForm = $this->formFactory->createNamedBuilder($k, FormType::class);
            $langForm->add(
                'titolo',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'product.labels.nome',
                    'constraints' => [new NotBlank()],
                ]
            );
            $langForm->add('titolo_breve', TextType::class, ['required' => false]);
            $langForm->add('sottotitolo', TextType::class, ['required' => false]);
            $langForm->add(
                'summary',
                TextareaType::class,
                [
                    'required' => false,
                    'attr' => ['class' => 'ck'],
                ]
            );
            $langForm->add(
                'testo',
                TextareaType::class,
                [
                    'required' => false,
                    'attr' => ['class' => 'ck'],
                ]
            );
            $langForm->add(
                'dati_aggiuntivi',
                TextareaType::class,
                [
                    'required' => false,
                    'attr' => ['class' => 'ck'],
                ]
            );
            if ($this->auth->isGranted('ROLE_EXTRA_SEO')) {

                $langForm->add(
                    'metaTitle',
                    SeoTitle::class,
                    [
                        'label' => 'default.labels.meta_title',
                        'required' => false,
                    ]
                );
                $langForm->add(
                    'metaDescription',
                    SeoDescription::class,
                    [
                        'label' => 'default.labels.meta_description',
                        'required' => false,
                    ]
                );
                $langForm->add(
                    'slug',
                    TextType::class,
                    [
                        'label' => 'default.labels.slug',
                        'required' => false,
                    ]
                );
            }
            $translation->add($langForm);

        }
        $form->add($translation);
        $form->add('listImg', FileType::class);
        $form->add('listImgData', HiddenType::class);
        $form->add('listImgAlt', TextType::class, []);
        $form->add('listImgDelete', HiddenType::class, []);
        /** TAB DESCRITTIVA - FINE **/
        /** TAB SPEDIZIONE - INIZIO **/
        $form->add(
            'peso',
            NumberType::class,
            [
                'required' => false,
                'label' => 'product.labels.peso',
                'attr' => ['class' => 'numeric'],
            ]
        );
        $form->add(
            'altezza',
            NumberType::class,
            [
                'required' => false,
                'label' => 'product.labels.altezza',
                'attr' => ['class' => 'numeric'],
            ]
        );
        $form->add(
            'larghezza',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.larghezza',
                'attr' => ['class' => 'numeric'],
            ]
        );
        $form->add(
            'profondita',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.profondita',
                'attr' => ['class' => 'numeric'],
            ]
        );
        // multipli
        $form->add(
            'quantita_minima',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.quantita_minima',
                'attr' => ['class' => 'numeric'],
                'constraints' => [
                    new NotBlank(),
                    new Range(['min' => 1]),
                ],
            ]
        );
        $form->add(
            'multipli',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.multipli',
                'attr' => ['class' => 'numeric'],
            ]
        );
        $form->add(
            'allCorrieri',
            ChoiceType::class,
            [
                'label' => 'product.labels.tutti_corrieri',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $form->add(
            'corrieri',
            EntityType::class,
            [
                'class' => Delivery::class,
                'multiple' => true,
                'expanded' => true,
                'help' => 'product.labels.corrieri_help',
            ]
        );
        $form->add(
            'costo_spedizione_aggiuntivo',
            NumberType::class,
            [
                'iconAfter' => 'fa fa-eur',
                'attr' => ['class' => 'numeric'],
            ]
        );
        /** TAB SPEDIZIONE - FINE   **/
        /** TAB PREZZI - INIZIO **/
        $form->add(
            'tax',
            EntityType::class,
            [
                'class' => Tax::class,
                'choice_attr' => function ($val) {

                    /**
                     * @var $val Tax
                     */
                    return ['data-multiplier' => ($val->getAliquota() / 100) + 1];
                },
            ]
        );
        if ($this->container->getParameter('ecommerce')['manage_attributes']) {
            $form->add(
                'prezzoDipendeDaVariante',
                ChoiceType::class,
                [
                    'label' => 'product.labels.prezzo_dipende_da_variante',
                    'choices' => [
                        'default.labels.si' => true,
                        'default.labels.no' => false,
                    ],
                    'placeholder' => false,
                    'required' => false,
                ]
            );
        }
        $form->add(
            'isAlwaysAvailable',
            ChoiceType::class,
            [
                'label' => 'products.labels.always_available',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'help' => 'product.help.always_available',
                'placeholder' => false,
                'required' => false,
            ]
        );
        $help = false;
        if ($this->container->getParameter('ecommerce')['manage_attributes']) {

            $help = $this->translator->trans('product.help.giacenza_default');

        }
        $form->add(
            'giacenza',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.giacenza',
                'attr' => ['class' => 'numeric'],
                'help' => $help,
            ]
        );
        $form->add(
            'soglia',
            TextType::class,
            [
                'required' => false,
                'attr' => ['class' => 'numeric'],
                'label' => 'product.labels.soglia',
                'help' => $help,
            ]
        );
        $form->add(
            'prezzoCivetta',
            NumberType::class,
            [
                'label' => 'product.labels.prezzo_civetta',
                'iconAfter' => 'fa fa-eur',
                'attr' => ['class' => 'numeric prezzo-ivato'],
            ]
        );
        $prezzi = $this->formFactory->createNamedBuilder(
            'prezzi',
            FormType::class
        );
        $Listini = $this->entityManager->getRepository('WebtekEcommerceBundle:Listino')->findAll();
        foreach ($Listini as $Listino) {

            $listinoForm = $this->formFactory->createNamedBuilder(
                $Listino->getId(),
                FormType::class,
                $defaultData['prezzi'][$Listino->getId()]
            );
            $listinoForm->add(
                'prezzoNetto',
                NumberType::class,
                [
                    'label' => $this->translator->trans(
                        'product.labels.prezzo_netto',
                        ['%listino%' => $Listino->translate()->getTitolo()]
                    ),
                    'grouping' => true,
                    'attr' => ['class' => 'numeric prezzo-netto'],
                    'iconAfter' => 'fa fa-eur',
                ]
            );
            $listinoForm->add(
                'prezzoIvato',
                NumberType::class,
                [
                    'label' => $this->translator->trans(
                        'product.labels.prezzo_ivato',
                        ['%listino%' => $Listino->translate()->getTitolo()]
                    ),
                    'attr' => ['class' => 'numeric prezzo-ivato'],
                    'iconAfter' => 'fa fa-eur',
                ]
            );
            $listinoForm->add(
                'entityId',
                HiddenType::class
            );
            $prezzi->add($listinoForm);

        }
        $form->add($prezzi);
        $marketPlaces = [];
        if ($this->container->getParameter('ecommerce')['manage_marketplaces']) {

            $marketPlaces = $this->entityManager->getRepository('WebtekEcommerceBundle:MarketPlace')->findAllNotDeleted(
                true
            );
            // collection delle varianti
            $form->add(
                'prezzi_marketplaces',
                CollectionType::class,
                [
                    'entry_type' => PrezzoMarketPlaceForm::class,
                    'allow_add' => false,
                    'allow_delete' => true,
                    'allow_extra_fields' => false,
                    'label' => false,
                    'attr' => [],
                ]
            );
        }
        // famiglia prodotto
        $form->add(
            'product_type',
            EntityType::class,
            [
                'class' => ProductType::class,
                'label' => 'products.labels.tipo_prodotto',
                'choice_attr' => function (ProductType $productType) {

                    $gruppiAttributi = $productType->getGruppiAttributi();
                    $ids = [];
                    foreach ($gruppiAttributi as $gruppoAttributi) {

                        /**
                         * @var $gruppoAttributi GroupAttribute
                         */
                        $ids[] = $gruppoAttributi->getId();

                    }

                    return ['data-ids' => implode(',', $ids)];

                },
            ]
        );
        // collection delle varianti
        $form->add(
            'varianti',
            CollectionType::class,
            [
                'entry_type' => VarianteProdottoForm::class,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_extra_fields' => true,
                'label' => false,
                'entry_options' => [
                    'langs' => $this->languages->getActiveLanguages(),
                    'listini' => $Listini,
                    'marketPlaces' => $marketPlaces,
                ],
                'attr' => [],
            ]
        );
        /** TAB PREZZI - FINE   **/
        /** TAB POSIZIONAMENTO - INIZIO **/
        if ($this->container->getParameter('ecommerce')['manage_brands']) {
            $form->add(
                'brand',
                EntityType::class,
                [
                    'class' => Brand::class,
                    'query_builder' => function (BrandsRepository $er) {

                        return $er->createQueryBuilder('brand')->leftJoin(
                            'Webtek\EcommerceBundle\Entity\BrandTranslation',
                            'brandT',
                            \Doctrine\ORM\Query\Expr\Join::WITH,
                            'brand = brandT.translatable and brandT.locale = :locale'
                        )->setParameter('locale', 'it')->addOrderBy('brandT.titolo', 'asc');
                    },
                ]
            );
        }
        $form->add(
            'categorie',
            EntityType::class,
            [
                'class' => Category::class,
                'expanded' => true,
                'multiple' => true,
                'attr' => ['class' => 'categorie-ecommerce'],
                'query_builder' => $this->entityManager->getRepository('WebtekEcommerceBundle:Category')->getAllButRoot(
                ),
            ]
        );
        $form->add(
            'tagsList',
            TextType::class,
            [
                'label' => 'product.labels.tags',
                'required' => false,
                'attr' => ['class' => 'tags'],
            ]
        );
        $form->get('tagsList')->addModelTransformer(
            new CallbackTransformer(
                function ($tags) {

                    $tagList = [];
                    foreach ($tags as $tag) {
                        /**
                         * @var $tag Tag
                         */
                        $tagList[] = $tag->translate()->getNome();

                    }

                    return implode(',', $tagList);
                }, function ($tags) {

                $TagList = new ArrayCollection();
                if ($tags) {
                    $tags = explode(',', $tags);
                    $tags = array_map('trim', $tags);
                    $tags = array_filter($tags);
                    foreach ($tags as $tag) {

                        $Tag = $this->entityManager->getRepository('TagBundle:Tag')->findOneBy(['valore' => $tag]);
                        if ($Tag) {
                            $TagList->add($Tag);
                        } else {
                            $Tag = new Tag();
                            $Tag->setValore($tag);
                            $Tag->translate('it')->setNome($tag);
                            $Tag->setCategoria(
                                $this->entityManager->getRepository('TagBundle:CategoriaTag')->findOneBy(
                                    ['id' => 1]
                                )
                            );
                            $Tag->setIsEnabled(1);
                            $Tag->mergeNewTranslations();
                            $TagList->add($Tag);
                        }

                    }
                }

                return $TagList;
            }
            )
        );
        $form->add(
            'correlatiAutocomplete',
            TextType::class,
            [
                'label' => 'product.labels.correlati',
                'required' => false,
                'attr' => ['class' => 'correlati'],
                'help' => 'product.help.correlati',
            ]
        );
        $form->add(
            'correlati',
            HiddenType::class
        );
        $form->get('correlati')->addModelTransformer(
            new CallbackTransformer(
                function ($correlati) {

                    $ProductList = [];
                    if ($correlati) {

                        foreach ($correlati as $ProdottoCorrelato) {
                            /**
                             * @var $tag Tag
                             */
                            $ProductList[] = $ProdottoCorrelato->getId();

                        }
                    }

                    return implode(',', $ProductList);
                }, function ($correlati) {

                $ProductList = new ArrayCollection();
                if ($correlati) {
                    $correlati = explode(',', $correlati);
                    $correlati = array_map('trim', $correlati);
                    $correlati = array_filter($correlati);
                    foreach ($correlati as $prodottoId) {

                        $ProdottoCorrelato = $this->entityManager->getRepository(
                            'WebtekEcommerceBundle:Product'
                        )->findOneBy(
                            ['id' => $prodottoId]
                        );
                        if ($ProdottoCorrelato) {
                            $ProductList->add($ProdottoCorrelato);
                        }

                    }
                }

                return $ProductList;
            }
            )
        );
        /** TAB POSIZIONAMENTO - FINE   **/
        $supportData = [];
        $supportData['attributes'] = $this->getAttributes();
        $supportData['listImgUrl'] = $defaultData['listImgUrl'];
        $supportData['marketPlaces'] = array_keys($this->getMarketPlaces());

        return [
            'supportData' => $supportData,
            'form' => $form->getForm(),
        ];

    }


    public function retrieveDefaultData(Product $Product)
    {

        $data = [];
        /**
         * TAB DATI DESCRITTIVI PRODOTTO
         */
        // DATI STRUTTURALI - INIZIO
        //
        // riga1
        $data['isEnabled'] = $Product->getIsEnabled();
        $data['available'] = $Product->getAvailable();
        $data['typeOfRedirect'] = $Product->getTypeOfRedirect();
        $data['targetId'] = $Product->getTargetId();
        $data['uuid'] = $Product->getUuid();
        if ($Product->getTargetId()) {
            switch ($data['typeOfRedirect']) {
                case '301P':
                case '302P':
                    $ProductRedirect = $this->entityManager->getRepository(
                        'WebtekEcommerceBundle:Product'
                    )->findOneBy(
                        ['id' => $Product->getTargetId()]
                    );
                    $data['redirectionHelper'] = $ProductRedirect->translate()->getTitolo();
                    break;
                case '301C':
                case '302C':
                    $CategoryRedirect = $this->entityManager->getRepository(
                        'WebtekEcommerceBundle:Category'
                    )->findOneBy(
                        ['id' => $Product->getTargetId()]
                    );
                    $data['redirectionHelper'] = $CategoryRedirect->translate()->getNome();
            }
        }
        // riga2
        $data['codice'] = $Product->getCodice();
        $data['ean'] = $Product->getEan();
        $data['codice_produttore'] = $Product->getCodiceProduttore();
        // CARATTERISTICHE - INIZIO
        $data['features'] = [];
        $Features = $Product->getFeatures();
        foreach ($Features as $ProductFeature) {
            /**
             * @var $ProductFeature ProductFeature
             */
            /**
             * @var $Feature Feature
             */
            $Feature = $ProductFeature->getFeature();
            $feature = [];
            $feature['id'] = $ProductFeature->getId();
            $feature['textValue'] = $ProductFeature->getTextValue();
            if ($Feature) {
                $feature['feature'] = $Feature;
                $feature['featureGroup'] = $Feature->getFeatureGroup();
            } else {
                $feature['feature'] = ' - 1';
            }
            $data['features'][] = $feature;

        }
        // CARATTERISTICHE - FINE
        // IMMAGINE ELENCHI - INIZIO
        $data['listImgUrl'] = '';
        if ($Product->getListImgFileName()) {
            $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
            $data['listImgUrl'] = $helper->asset($Product, 'listImg');
        }
        // TRADUZIONI - INIZIO
        foreach ($this->languages->getActiveLanguages() as $shortLang => $linguaEstesa) {

            $Translation = $Product->translate($shortLang, false);
            $data['translations'][$shortLang]['titolo'] = $Translation->getTitolo();
            $data['translations'][$shortLang]['titolo_breve'] = $Translation->getTitoloBreve();
            $data['translations'][$shortLang]['sottotitolo'] = $Translation->getSottotitolo();
            $data['translations'][$shortLang]['summary'] = $Translation->getSummary();
            $data['translations'][$shortLang]['testo'] = $Translation->getTesto();
            $data['translations'][$shortLang]['metaTitle'] = $Translation->getMetaTitle();
            $data['translations'][$shortLang]['metaDescription'] = $Translation->getMetaDescription();
            $data['translations'][$shortLang]['slug'] = $Translation->getSlug();
            $data['translations'][$shortLang]['dati_aggiuntivi'] = $Translation->getDatiAggiuntivi();

        }
        // TRADUZIONI - FINE
        /**
         * TAB DATI SPEDIZIONE
         */
        // riga 1
        $data['peso'] = $Product->getPeso();
        // riga 2
        $data['altezza'] = $Product->getAltezza();
        $data['larghezza'] = $Product->getLarghezza();
        $data['profondita'] = $Product->getProfondita();
        // riga 3
        $data['quantita_minima'] = $Product->getQuantitaMinima();
        $data['multipli'] = $Product->getMultipli();
        // riga 4
        $data['costo_spedizione_aggiuntivo'] = $Product->getCostoSpedizioneAggiuntivo();
        $data['allCorrieri'] = $Product->getAllCorrieri();
        $data['corrieri'] = $Product->getCorrieri();
        $data['listImgAlt'] = $Product->getListImgAlt();
        if ($Product->getId()) {
            $data['uuid'] = $Product->getId();
        } else {
            $data['uuid'] = Uuid::uuid1()->toString();
        }
        $data['attachments'] = $Product->getAttachments();
        /**
         * TAB DATI PREZZI, VARIANTI E GIANCEZE
         */
        $data['isAlwaysAvailable'] = $Product->getisAlwaysAvailable();
        $data['giacenza'] = $Product->getGiacenza();
        $data['soglia'] = $Product->getSoglia();
        $data['prezzoCivetta'] = $Product->getPrezzoCivetta();
        /**
         * @var $Tax Tax
         */
        $Tax = $Product->getTax();
        if ($Tax) {
            $data['tax'] = $Tax;
        }
        if ($this->container->getParameter('ecommerce')['manage_attributes']) {
            $data['prezzoDipendeDaVariante'] = $Product->getPrezzoDipendeDaVariante();
            $data['product_type'] = $Product->getProductType();
        }
        // prezzi
        $Prezzi = $Product->getPrezzi();
        $Listini = $this->entityManager->getRepository('WebtekEcommerceBundle:Listino')->findAll();
        foreach ($Prezzi as $Prezzo) {

            $idListino = $Prezzo->getListino()->getId();
            /**
             * @var $Prezzo Prezzo
             */
            $data['prezzi'][$idListino]['prezzoIvato'] = $this->taxCalculator->getPrezzoIvato(
                $Prezzo->getValore(),
                $Tax->getAliquota(),
                4
            );
            $data['prezzi'][$idListino]['entityId'] = $Prezzo->getId();
            $data['prezzi'][$idListino]['prezzoNetto'] = $Prezzo->getValore();

        }
        foreach ($Listini as $Listino) {

            $idListino = $Listino->getId();
            if (!isset($data['prezzi']) || !isset($data['prezzi'][$idListino])) {
                $data['prezzi'][$idListino]['entityId'] = 0;
                $data['prezzi'][$idListino]['prezzoNetto'] = 0;
                $data['prezzi'][$idListino]['prezzoIvato'] = 0;
            }
        }
        // prezzi MarketPlace
        if ($this->container->getParameter('ecommerce')['manage_marketplaces']) {

            $data['prezzi_marketplaces'] = $Product->getPrezziMarketPlace();
            $marketPlaces = $this->entityManager->getRepository(
                'WebtekEcommerceBundle:MarketPlace'
            )->findAllNotDeleted(
                true
            );
            $idGiaPresenti = [];
            foreach ($data['prezzi_marketplaces'] as $prezziMarketplace) {

                /**
                 * @var $prezziMarketPlace PrezzoMarketPlace
                 */
                $idGiaPresenti[] = $prezziMarketplace->getMarketPlace()->getId();

            }
            foreach ($marketPlaces as $marketPlace) {
                /**
                 * @var $marketPlace MarketPlace
                 */
                if (!in_array($marketPlace->getId(), $idGiaPresenti)) {
                    $PrezzoMarketPlace = new PrezzoMarketPlace();
                    $PrezzoMarketPlace->setValore(0);
                    $PrezzoMarketPlace->setIsEnabled(0);
                    $PrezzoMarketPlace->setMarketPlace($marketPlace);
                    $data['prezzi_marketplaces']->add($PrezzoMarketPlace);
                }
                $marketPlaceAttivi[] = $marketPlace->getId();
            }
            foreach ($data['prezzi_marketplaces'] as $prezziMarketplace) {
                if (!in_array($prezziMarketplace->getMarketPlace()->getId(), $marketPlaceAttivi)) {
                    $data['prezzi_marketplaces']->removeElement($prezziMarketplace);
                }
            }

        }
        $data['varianti'] = $Product->getVarianti();
        $data['tagsList'] = $Product->getTags();
        $data['correlati'] = $Product->getCorrelati();
        /**
         * TAB POSIZIONAMENTO
         */
        $data['brand'] = $Product->getBrand();
        $data['categorie'] = $Product->getCategorie();

        return $data;

    }

    public function handleData($form, Product $Product)
    {

        $data = $form->getData();
        /**
         * TAB DATI DESCRITTIVI PRODOTTO
         */
        // DATI STRUTTURALI - INIZIO
        //
        // riga1
        $Product->setIsEnabled($data['isEnabled']);
        $Product->setAvailable($data['available']);
        $Product->setTypeOfRedirect($data['typeOfRedirect']);
        $Product->setTargetId($data['targetId']);
        $Product->setUuid($data['uuid']);
        // riga2
        $Product->setCodice($data['codice']);
        $Product->setCodiceProduttore($data['codice_produttore']);
        $Product->setEan($data['ean']);
        // DATI STRUTTURALI - FINE
        // CARATTERISTICHE - INIZIO
        // features
        $Features = clone($Product->getFeatures());
        $featuresId = [];
        foreach ($data['features'] as $k => $featureData) {

            $ProductFeature = false;
            if ($featureData['id']) {

                $featuresId[] = $featureData['id'];
                $ProductFeature = $this->entityManager->getRepository(
                    'WebtekEcommerceBundle:ProductFeature'
                )->findOneBy(['id' => $featureData['id']]);
            }
            if (!$ProductFeature) {
                $ProductFeature = new ProductFeature();
                $Product->addFeature($ProductFeature);
            }
            $ProductFeature->setFeatureGroup($featureData['featureGroup']);
            if ($featureData['feature']) {
                $Feature = $this->entityManager->getRepository('WebtekEcommerceBundle:Feature')->findOneBy(
                    ['id' => $featureData['feature']]
                );
                $ProductFeature->setFeature($Feature);
            } else {
                $ProductFeature->setTextValue($featureData['textValue']);
            }


        }
        foreach ($Features as $Feature) {
            if (!in_array($Feature->getId(), $featuresId)) {
                $Product->removeFeature($Feature);
                $this->entityManager->remove($Feature);
            }
        }
        // CARATTERISTICHE - FINE
        // IMMAGINE ELENCHI - FINE
        // TRADUZIONI - INIZIO
        foreach ($this->languages->getActiveLanguages() as $shortLang => $linguaEstesa) {

            $Translation = $Product->translate($shortLang);
            $Translation->setTitolo($data['translations'][$shortLang]['titolo']);
            $Translation->setTitoloBreve($data['translations'][$shortLang]['titolo_breve']);
            $Translation->setSottotitolo($data['translations'][$shortLang]['sottotitolo']);
            $Translation->setSummary($data['translations'][$shortLang]['summary']);
            $Translation->setTesto($data['translations'][$shortLang]['testo']);
            $Translation->setMetaTitle($data['translations'][$shortLang]['metaTitle']);
            $Translation->setMetaDescription($data['translations'][$shortLang]['metaDescription']);
            $Translation->setSlug($data['translations'][$shortLang]['slug']);
            $Translation->setDatiAggiuntivi($data['translations'][$shortLang]['dati_aggiuntivi']);
            $Product->addTranslation($Translation);

        }
        $Product->mergeNewTranslations();
        // TRADUZIONI - FINE
        // IMMAGINE ELENCHI - INIZIO
        if ($data['listImgDelete']) {
            $this->container->get('vich_uploader.upload_handler')->remove($Product, 'listImg');
            $Product->setListImg(null);
            $Product->setListImgAlt('');
        } else {
            $Product->setListImgData($data['listImgData']);
            if ($data['listImg']) {
                $Product->setListImg($data['listImg']);
            }
            $Product->setListImgAlt($data['listImgAlt']);
        }
        /**
         * TAB DATI SPEDIZIONE
         */
        // Dimensioni prodotto - INIZIO
        // riga 1
        $Product->setPeso($data['peso']);
        // riga 2
        $Product->setAltezza($data['altezza']);
        $Product->setLarghezza($data['larghezza']);
        $Product->setProfondita($data['profondita']);
        // riga 3
        $Product->setQuantitaMinima($data['quantita_minima']);
        $Product->setMultipli($data['multipli']);
        // riga 4
        $Product->setCostoSpedizioneAggiuntivo($data['costo_spedizione_aggiuntivo']);
        $Product->setAllCorrieri($data['allCorrieri']);
        if ($Product->getAllCorrieri()) {
            $Product->setCorrieri(new ArrayCollection());
        } else {
            $Product->setCorrieri($data['corrieri']);
        }
        /**
         * TAB PREZZI
         */
        // riga 1
        $Product->setTax($data['tax']);
        $Product->setPrezzoCivetta($data['prezzoCivetta']);
        if ($this->container->getParameter('ecommerce')['manage_attributes']) {
            $Product->setProductType($data['product_type']);
            $Product->setPrezzoDipendeDaVariante($data['prezzoDipendeDaVariante']);
        } else {
            $Product->setPrezzoDipendeDaVariante(0);
        }
        $Product->setIsAlwaysAvailable($data['isAlwaysAvailable']);
        // riga 2 (listini)
        $PrezziOriginali = $Product->getPrezzi();
        $Listini = $this->entityManager->getRepository('WebtekEcommerceBundle:Listino')->findAll();
        foreach ($Listini as $Listino) {

            $Prezzo = false;
            if (isset($data['prezzi'][$Listino->getId()]['entityId']) && $data['prezzi'][$Listino->getId(
                )]['entityId']) {
                $Prezzo = $this->entityManager->getRepository('WebtekEcommerceBundle:Prezzo')->findOneBy(
                    ['id' => $data['prezzi'][$Listino->getId()]['entityId']]
                );

            }
            if (!$Prezzo) {
                $Prezzo = new Prezzo();
            }
            /**
             * @var $Listino Listino
             */
            $Prezzo->setListino($Listino);
            $Prezzo->setValore($data['prezzi'][$Listino->getId()]['prezzoNetto']);
            $Prezzo->setProdotto($Product);
            $Product->addPrezzo($Prezzo);

        }
        foreach ($PrezziOriginali as $Prezzo) {
            if (false === $Product->getPrezzi()->contains($Prezzo)) {
                $this->entityManager->remove($Prezzo);
            }
        }
        if ($this->container->getParameter('ecommerce')['manage_marketplaces']) {
            foreach ($data['prezzi_marketplaces'] as $prezziMarketplace) {
                /**
                 * @var $prezziMarketplace PrezzoMarketPlace
                 */
                if (!$prezziMarketplace->getisEnabled()) {
                    $prezziMarketplace->setValore(0);
                }
                if (!is_numeric($prezziMarketplace->getValore())) {
                    $prezziMarketplace->setValore(0);
                }
                $prezziMarketplace->setProdotto($Product);
            }
        }
        if ($this->container->getParameter('ecommerce')['manage_attributes']) {
            // VARIANTI
            foreach ($data['varianti'] as $VarianteProdotto) {

                foreach ($VarianteProdotto->getPhotos() as $Photo) {
                    /**
                     * @var $Photo ProductAttachment
                     */
                    $Photo->addVarianti($VarianteProdotto);
                }
                foreach ($VarianteProdotto->getPrezzi() as $Prezzo) {
                    /**
                     * @var $Prezzo PrezzoVariante
                     */
                    $Prezzo->setVarianteProdotto($VarianteProdotto);
                }
                /**
                 * @var $VarianteProdotto VarianteProdotto
                 */
                if (!$VarianteProdotto->getProdotto()) {
                    $VarianteProdotto->setProdotto($Product);

                }
                $Product->addVariante($VarianteProdotto);
            }
        }
        /**
         * TAB POSIZIONAMENTO
         */
        if ($this->container->getParameter('ecommerce')['manage_brands']) {
            if ($data['brand']) {
                $Brand = $this->entityManager->getRepository('WebtekEcommerceBundle:Brand')->findOneBy(
                    ['id' => $data['brand']]
                );
                $Product->setBrand($Brand);
            }
        }
        // categorie
        $Categorie = $Product->getCategorie();
        foreach ($data['categorie'] as $categoria) {

            $Product->addCategoria($categoria);

        }
        foreach ($Categorie as $categoria) {
            if (!$data['categorie']->contains($categoria)) {
                $Product->removeCategoria($categoria);
            }
        }
        $Tags = $Product->getTags();
        foreach ($data['tagsList'] as $Tag) {
            $Product->addTag($Tag);
        }
        foreach ($Tags as $Tag) {
            if (!$data['tagsList']->contains($Tag)) {
                $Product->removeTag($Tag);
            }
        }
        $Correlati = $Product->getCorrelati();
        foreach ($data['correlati'] as $Correlato) {
            $Product->addCorrelati($Correlato);
        }
        foreach ($Correlati as $Correlato) {
            if (!$data['correlati']->contains($Correlato)) {
                $Product->removeCorrelati($Correlato);
            }
        }
        $this->entityManager->persist($Product);
        $this->entityManager->flush();

        return true;

    }

    private function getAttributes()
    {

        return $this->groupAttributeHelper->getSimpleArray();
    }

    private function getMarketPlaces()
    {

        return $this->marketPlacesHelper->getSimpleArray();

    }

    /** Metodi di validazione - INIZIO */
    public function checkRedirect($data, ExecutionContextInterface $context)
    {

        if ($data['typeOfRedirect'] !== '404' && (!is_numeric($data['targetId']) || !$data['targetId'])) {

            $Violation = $context->buildViolation($this->translator->trans('products.errors.redirect_non_valido'));
            $Violation->atPath('[redirectionHelper]')->addViolation();

        }


    }

    public function checkCategorie($data, ExecutionContextInterface $context)
    {

        if ($data['categorie']->isEmpty()) {
            $Violation = $context->buildViolation($this->translator->trans('products.errors.seleziona_categoria'));
            $Violation->atPath('[categorie]')->addViolation();
        }


    }

    public function checkUniqueCode($data, ExecutionContextInterface $context)
    {


    }

    /** Metodi di validazione - FINE */
}
