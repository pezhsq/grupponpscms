<?php
/* gabricom */

namespace Webtek\EcommerceBundle\Form;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\Recensione;

class RecensioneForm extends AbstractType
{

    public function __construct(EntityManager $em)
    {

        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('valore', NumberType::class, ['attr' => ['class' => 'valore']])
            ->add('messaggio', TextareaType::class);

//        $builder->get('news')->addModelTransformer(new CallbackTransformer(
//            function ($NewsCategory) {
//
//                return $NewsCategory;
//            },
//            function ($news_id) {
//
//                return $this->em->getRepository('AppBundle:News')->findOneBy(['id' => $news_id]);
//
//            }
//        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(['data_class' => Recensione::class,
            'allow_extra_fields' => true]);
    }


}