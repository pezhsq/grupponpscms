<?php
// 12/01/17, 16.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Form\TypeExtension\SeoDescription;
use AppBundle\Form\TypeExtension\SeoTitle;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Webtek\EcommerceBundle\Entity\Brand;

class BrandForm extends AbstractType
{

    /**
     * @var AuthorizationChecker
     */
    private $auth;

    public function __construct(AuthorizationChecker $auth)
    {

        $this->auth = $auth;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('uuid', HiddenType::class, ['data' => Uuid::uuid1()->__toString()]);

        $builder->add('listImg', FileType::class);
        $builder->add('listImgData', HiddenType::class);
        $builder->add('listImgAlt', TextType::class, []);
        $builder->add('listImgDelete', HiddenType::class, []);

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $fields = [
            'titolo' => [
                'label' => 'brands.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            'sottotitolo' => [
                'label' => 'brands.labels.sottotitolo',
                'required' => false,
            ],
        ];

        $excluded_fields = [];

        if (!$this->auth->isGranted('ROLE_EXTRA_SEO')) {
            $excluded_fields = ['metaTitle', 'metaDescription', 'slug'];
        } else {

            $fields = array_merge(
                $fields,
                [
                    'metaTitle' => [
                        'label' => 'default.labels.meta_title',
                        'field_type' => SeoTitle::class,
                        'required' => false,
                    ],
                    'metaDescription' => [
                        'label' => 'default.labels.meta_description',
                        'field_type' => SeoDescription::class,
                        'required' => false,
                    ],
                    'slug' => [
                        'label' => 'default.labels.slug',
                        'required' => false,
                        'attr' => ['class' => 'slug'],
                    ],
                ]
            );

        }

        $fields['testo'] = [
            'label' => 'brands.labels.testo',
            'required' => false,
            'attr' => ['class' => 'ck'],
        ];


        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'exclude_fields' => $excluded_fields,
            ]
        );
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Brand::class,
                'error_bubbling' => true,
                'allow_extra_fields' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}