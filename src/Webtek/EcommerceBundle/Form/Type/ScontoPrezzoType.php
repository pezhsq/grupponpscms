<?php
// 20/03/2017 , 14:30
// @author : Gabriele "gabricom" Colombera <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Webtek\EcommerceBundle\Entity\ScontiSpedizionePerPrezzo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ScontoPrezzoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label'       => 'metodi_spedizione_ecommerce.labels.is_public',
                'choices'     => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required'    => false,
            ]
        );
        $builder->add(
            'soglia',
            MoneyType::class,
            ['label' => 'ecommerce.labels.soglia', 'attr' => ['class' => ''], 'required' => true]
        );
        $builder->add(
            'prezzo',
            MoneyType::class,
            ['label' => 'ecommerce.labels.prezzo', 'attr' => ['class' => 'js-prezzo'], 'required' => true]
        );
        $builder->add(
            'codiceIva',
            EntityType::class,
            [
                'class'        => 'Webtek\EcommerceBundle\Entity\Tax',
                'choice_label' => "codice",
                'attr'         => ['class' => 'js-codiceiva'],
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => ScontiSpedizionePerPrezzo::class,
            ]
        );
    }
}