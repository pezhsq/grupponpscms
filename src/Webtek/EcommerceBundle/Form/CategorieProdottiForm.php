<?php
// 12/01/17, 16.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Service\Explorer;
use Webtek\EcommerceBundle\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategorieProdottiForm extends AbstractType
{

    /**
     * @var AuthorizationChecker
     */
    private $auth;
    /**
     * @var Explorer
     */
    private $explorer;

    public function __construct(AuthorizationChecker $auth, Explorer $explorer)
    {

        $this->auth = $auth;

        $this->explorer = $explorer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $templateChoices = $this->explorer->listFiles(
            'Resources/views/public/layouts/'.$options['layout'].'/CONTENT/',
            '.html.twig'
        );

        $builder->add(
            'template',
            ChoiceType::class,
            [
                'label' => 'pages.labels.template',
                'choices' => $templateChoices,
            ]
        );

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add(
            'amazonNode',
            TextType::class,
            ['label' => 'categorie_prodotti.labels.amazon_node', 'required' => false]
        );

        $builder->add(
            'icona',
            TextType::class,
            ['label' => 'icona', 'required' => false]
        );

        $builder->add('CategoryImg', HiddenType::class);
        $builder->add('CategoryImgFileName', HiddenType::class);
        $builder->add(
            'CategoryImgFileNameAlt',
            TextType::class,
            ['label' => 'news.labels.immagine_elenchi_alt', 'attr' => ['class' => 'altTXT'], 'required' => false]
        );
        $builder->add('deleteCategoryImg', HiddenType::class);

        $fields = [
            'nome' => [
                'label' => 'categorie_prodotti.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            'sottotitolo' => [
                'label' => 'categorie_prodotti.labels.sottotitolo',
                'required' => false,
                'attr' => ['class' => 'titolo'],
            ],
            'descrizione' => [
                'label' => 'categorie_prodotti.labels.descrizione',
                'required' => false,
                'attr' => ['class' => 'ck'],
            ],
        ];

        $excluded_fields = [];


        if (!$this->auth->isGranted('ROLE_EXTRA_SEO')) {
            $excluded_fields = ['metaTitle', 'metaDescription', 'slug'];
            $builder->add('robots', HiddenType::class);

        } else {

            $fields = array_merge(
                $fields,
                [
                    'metaTitle' => [
                        'label' => 'default.labels.meta_title',
                        'required' => false,
                    ],
                    'metaDescription' => [
                        'label' => 'default.labels.meta_description',
                        'required' => false,
                    ],
                    'slug' => [
                        'label' => 'default.labels.slug',
                        'required' => false,
                        'attr' => ['class' => 'slug'],
                    ],
                ]
            );

            $builder->add(
                'robots',
                ChoiceType::class,
                [
                    'label' => 'pages.labels.robots',
                    'choices' => [
                        'index,follow' => 'index,follow',
                        'index,nofollow' => 'index,nofollow',
                        'noindex,follow' => 'noindex,follow',
                        'noindex,nofollow' => 'noindex,nofollow',
                    ],
                ]
            );

        }


        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'exclude_fields' => $excluded_fields,
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Category::class,
                'error_bubbling' => true,
                'layout' => 'webtek',
                'langs' => [
                    'it' => 'Italiano',
                    'allow_extra_fields' => true,
                ],
                'data' => new Category(),
            ]
        );
    }


}
