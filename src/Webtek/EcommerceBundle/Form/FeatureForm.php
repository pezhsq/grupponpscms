<?php
// 27/04/17, 14.26
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\Feature;
use Webtek\EcommerceBundle\Entity\GroupFeature;

class FeatureForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $fields = [
            'titolo' => [
                'label'    => 'attribute.labels.titolo',
                'required' => true,
                'attr'     => ['class' => 'titolo'],
            ],
        ];

        $builder->add(
            'featureGroup',
            EntityType::class,
            [
                'class'       => GroupFeature::class,
                'label'       => 'feature.labels.gruppo',
                'placeholder' => false,
                'required'    => false,
            ]
        );

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales'          => array_keys($options['langs']),
                'fields'           => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'     => Feature::class,
                'error_bubbling' => true,
                'langs'          => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}