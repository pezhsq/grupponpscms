<?php
// 12/01/17, 16.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Form;

use Webtek\EcommerceBundle\Entity\Tax;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;

class TaxForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label'       => 'tax_ecommerce.labels.is_public',
                'choices'     => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required'    => false,
            ]
        );
        $builder->add(
            'codice',
            TextType::class,
            ['label' => 'tax_ecommerce.labels.codice', 'attr' => ['class' => ''], 'required' => true]
        );
        $builder->add(
            'aliquota',
            TextType::class,
            [
                'label'     => 'tax_ecommerce.labels.aliquota',
                'attr'      => ['class' => ''],
                'iconAfter' => 'fa fa-percent',
                'required'  => true,
            ]
        );
        $builder->add(
            'descrizione',
            TextAreaType::class,
            ['label' => 'tax_ecommerce.labels.descrizione', 'attr' => ['class' => ''], 'required' => true]
        );


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'     => Tax::class,
                'error_bubbling' => true,
            ]
        );
    }


}
