<?php
// 06/11/17, 9.42
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\Coupon;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Repository\ProductRepository;

class CouponForm extends AbstractType
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * CouponForm constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $builder->add('code', TextType::class, ['label' => 'coupon.labels.code']);
        $builder->add(
            'type',
            ChoiceType::class,
            [
                'label' => 'coupon.labels.type',
                'choices' => [
                    'coupon.labels.percentuale' => Coupon::TYPE_PERCENTAGE,
                    'coupon.labels.valore' => Coupon::TYPE_EURO,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $builder->add('value', TextType::class, ['label' => 'coupon.labels.value', 'iconAfter' => 'fa fa-percent']);
        $builder->add(
            'illimitato',
            ChoiceType::class,
            [
                'label' => 'coupon.labels.illimitato',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $builder->add('available', TextType::class, ['label' => 'coupon.labels.available', 'required' => false]);
        $builder->add(
            'allProducts',
            ChoiceType::class,
            [
                'label' => 'coupon.labels.allproducts',
                'choices' => [
                    'default.labels.si' => 1,
                    'default.labels.no' => 0,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );
        $builder->add('minimumOrder', TextType::class, ['label' => 'coupon.labels.minimumorder']);
        $fields = [
            'titolo' => [
                'label' => 'coupon.labels.titolo',
                'required' => true,
            ],
        ];
        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );
        $root = $this->entityManager->getRepository('Webtek\EcommerceBundle\Entity\Category')->getTreeOrdered();
        $builder->add(
            'categorie',
            ChoiceType::class,
            [
                'choices' => $this->getCategories($root, 0),
                'multiple' => true,
                'choice_label' => function ($value, $key, $index) {

                    return preg_replace('/\[\d+\]/', '', $key);

                },
            ]
        );
        $builder->get('categorie')->addModelTransformer(
            new CallbackTransformer(
                function ($categorie) {

                    $data = [];
                    if ($categorie) {

                        foreach ($categorie as $category) {
                            $data[] = $category->getId();

                        }

                    }

                    return $data;

                }, function ($categorie) {

                $categorieOut = new ArrayCollection();
                foreach ($categorie as $CategoryId) {

                    $Category = $this->entityManager->getRepository(
                        'WebtekEcommerceBundle:Category'
                    )->findOneBy(
                        ['id' => $CategoryId]
                    );
                    $categorieOut->add($Category);
                }

                return $categorieOut;

            }
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Coupon::class,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }

    private function getCategories($root, $level = 0)
    {

        $tree = [];
        $prefix = str_repeat('---', $level);
        $children = $root->getChildNodes();
        $level++;
        foreach ($children as $child) {
            $tree['['.$child->getId().']'.$prefix.$child->translate('it')->getNome()] = $child->getId();
            $childrends = $this->getCategories($child, $level);
            $tree = array_merge($tree, $childrends);
        }

        return $tree;

    }


}