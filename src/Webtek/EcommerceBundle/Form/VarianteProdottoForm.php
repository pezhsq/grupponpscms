<?php
// 12/05/17, 8.52
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Guzzle\Http\Message\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\Prezzo;
use Webtek\EcommerceBundle\Entity\PrezzoMarketPlace;
use Webtek\EcommerceBundle\Entity\PrezzoVariante;
use Webtek\EcommerceBundle\Entity\PrezzoVarianteMarketPlace;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;

class VarianteProdottoForm extends AbstractType
{

    public function __construct(EntityManager $em)
    {

        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('id', HiddenType::class, ['attr' => ['class' => 'rowid']]);
        $builder->add('sort', HiddenType::class, ['attr' => ['class' => 'sort-variante']]);
        $builder->add('deletedAt', HiddenType::class, ['attr' => ['class' => 'deletedat']]);
        $builder->add(
            'attributes',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\Attribute',
                'attr' => [
                    'class' => 'attributiVariante',
                ],
                'label' => false,
                'block_name' => 'custom_name',
                'expanded' => true,
                'multiple' => true,
            ]

        );

        $builder->add(
            'giacenza',
            NumberType::class,
            [
                'label' => 'product.labels.attuale',
                'required' => false,
                'attr' => [
                    'class' => 'numeric',
                ],

            ]
        );
        $builder->add(
            'soglia',
            NumberType::class,
            [
                'label' => 'product.labels.soglia_minima',
                'required' => false,
                'attr' => ['class' => 'numeric'],
            ]
        );

        $builder->add(
            'peso',
            NumberType::class,
            [
                'required' => false,
                'label' => 'product.labels.peso',
                'attr' => ['class' => 'numeric'],
            ]
        );

        $builder->add(
            'altezza',
            NumberType::class,
            [
                'required' => false,
                'label' => 'product.labels.altezza',
                'attr' => ['class' => 'numeric'],
            ]
        );

        $builder->add(
            'larghezza',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.larghezza',
                'attr' => ['class' => 'numeric'],
            ]
        );

        $builder->add(
            'profondita',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.profondita',
                'attr' => ['class' => 'numeric'],
            ]
        );

        $builder->add(
            'codice',
            TextType::class,
            [
                'required' => true,
                'label' => 'product.labels.codice',
            ]
        );

        $builder->add(
            'ean',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.ean',
            ]
        );

        $builder->add(
            'codice_produttore',
            TextType::class,
            [
                'required' => false,
                'label' => 'product.labels.codice_produttore',
            ]
        );

        $builder->add('isDefault', CheckboxType::class, ['label' => false, 'required' => false]);
        $builder->add(
            'photos',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\ProductAttachment',
                'label' => false,
                'required' => false,
                'multiple' => true,
                'attr' => [
                    'class' => 'hide photolist',
                ],
            ]
        );

        //$builder->get('photos')->resetViewTransformers();

        $builder->get('photos')->addModelTransformer(
            new CallbackTransformer(
                function ($photos) {

                    if ($photos) {
                        return $photos;

                    } else {
                        return new ArrayCollection();
                    }

                }, function ($photos) {

                $returnPhotos = new ArrayCollection();

                foreach ($photos as $photo) {

                    $returnPhotos[] = $this->em->getRepository(
                        'WebtekEcommerceBundle:ProductAttachment'
                    )->findOneBy(
                        ['id' => $photo]
                    );

                }

                return $returnPhotos;

            }
            )
        );

        $builder->get('deletedAt')->addModelTransformer(
            new CallbackTransformer(
                function ($deletedAt) {

                    if ($deletedAt) {
                        return $deletedAt->format('Y-m-d H:i:s');
                    } else {
                        return '';
                    }

                }, function ($deletedAt) {

                dump($deletedAt);

                $deleted = null;
                if ($deletedAt) {
                    $deleted = \DateTime::createFromFormat('Y-m-d H:i:s', $deletedAt);
                    dump($deleted);
                }

                return $deleted;


            }
            )
        );

        $fields = [
            'descrizione' => [
                'label' => false,
                'required' => true,
                'attr' => ['class' => 'descrizione'],
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'label' => false,
                'required_locales' => array_keys($options['langs']),
            ]
        );

//        $builder->addEventListener(
//            FormEvents::PRE_SUBMIT,
//            function (FormEvent $event) use ($options) {
//
//                /**
//                 * @var $VarianteProdotto VarianteProdotto
//                 */
//                $VarianteProdotto = $event->getData();
//
//            }
//        );
//
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($options) {

                /**
                 * @var $VarianteProdotto VarianteProdotto
                 */
                $VarianteProdotto = $event->getData();
                $form = $event->getForm();
                if (!$VarianteProdotto) {
                    $VarianteProdotto = new VarianteProdotto();
                    $Prezzi = new ArrayCollection();
                    $PrezziMarketPlace = new ArrayCollection();
                } else {
                    $Prezzi = $VarianteProdotto->getPrezzi();
                    $PrezziMarketPlace = $VarianteProdotto->getPrezziMarketPlace();
                }


                foreach ($options['listini'] as $listino) {
                    $listinoFound = false;
                    foreach ($Prezzi as $Prezzo) {
                        /**
                         * @var $Prezzo Prezzo
                         */
                        if ($Prezzo->getListino()->getId() == $listino->getId()) {
                            $listinoFound = true;
                        }
                    }
                    if (!$listinoFound) {

                        $PrezzoVariante = new PrezzoVariante();
                        $PrezzoVariante->setListino($listino);
                        $PrezzoVariante->setValore(0);

                        $VarianteProdotto->addPrezzo($PrezzoVariante);
                    }
                }

                $form->add(
                    'prezzi',
                    CollectionType::class,
                    [
                        'entry_type' => PrezzoVarianteForm::class,
                        'label' => false,
                        'allow_add' => true,
                        'allow_extra_fields' => true,
                        'attr' => ['class' => 'prezziVariante'],
                    ]
                );

                foreach ($options['marketPlaces'] as $marketPlace) {

                    $marketPlaceFound = false;
                    foreach ($PrezziMarketPlace as $PrezzoMarketPlace) {
                        /**
                         * @var $Prezzo PrezzoMarketPlace
                         */
                        if ($PrezzoMarketPlace->getMarketPlace()->getId() == $marketPlace->getId()) {
                            $marketPlaceFound = true;
                        }
                    }
                    if (!$marketPlaceFound) {

                        $PrezzoVaroamteMarketPlace = new PrezzoVarianteMarketPlace();
                        $PrezzoVaroamteMarketPlace->setMarketPlace($marketPlace);
                        $PrezzoVaroamteMarketPlace->setValore(0);

                        $VarianteProdotto->addPrezzoMarketPlace($PrezzoVaroamteMarketPlace);
                    }

                }

                $form->add(
                    'prezzi_marketplace',
                    CollectionType::class,
                    [
                        'entry_type' => PrezzoVarianteMarketPlaceForm::class,
                        'label' => false,
                        'allow_add' => true,
                        'allow_extra_fields' => true,
                        'attr' => ['class' => 'prezziVarianteMarketPlace'],
                    ]
                );


            }
        );


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => VarianteProdotto::class,
                'allow_extra_fields' => true,
                'label' => false,
                'langs' => [
                    'it' => 'Italiano',
                ],
                'listini' => [],
                'marketPlaces' => [],
            ]
        );
    }


}