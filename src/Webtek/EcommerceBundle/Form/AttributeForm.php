<?php
// 27/04/17, 14.26
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\Attribute;
use Webtek\EcommerceBundle\Entity\GroupAttribute;

class AttributeForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('value', TextType::class, ['iconAfter' => '']);
        $builder->add('codice', TextType::class);

        $builder->add(
            'attributeGroup',
            EntityType::class,
            [
                'class'       => GroupAttribute::class,
                'label'       => 'attribute.labels.gruppo',
                'choice_attr' => function ($val) {

                    /**
                     * @var $val GroupAttribute
                     */
                    return ['data-type' => $val->getType()];
                },
                'placeholder' => false,
                'required'    => false,
            ]
        );

        $fields = [
            'titolo' => [
                'label'    => 'attribute.labels.titolo',
                'required' => true,
                'attr'     => ['class' => 'titolo'],
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales'          => array_keys($options['langs']),
                'fields'           => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'     => Attribute::class,
                'error_bubbling' => true,
                'langs'          => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}