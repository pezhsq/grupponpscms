<?php
// 12/01/17, 16.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Webtek\EcommerceBundle\Entity\MetodiPagamento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;

class MetodiPagamentoForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $data = $builder->getData();

        $typePaypal = HiddenType::class;
        
        if ($data && $data->getCodice()) {
            switch ($data->getCodice()) {
                case 'PAYPAL':
                    $typePaypal = TextType::class;
                    break;
            }
        }

        $builder->add(
            'client_id',
            $typePaypal,
            [
                'label' => 'metodi_pagamento_ecommerce.labels.client_id',
                'required' => false,
            ]
        );
        $builder->add(
            'client_secret',
            $typePaypal,
            [
                'label' => 'metodi_pagamento_ecommerce.labels.client_secret',
                'required' => false,
            ]
        );

        $builder->add(
            'statoDefault',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\OrdersStatus',
            ]
        );


        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add(
            'codice',
            TextType::class,
            [
                'label' => 'metodi_pagamento_ecommerce.labels.codice',
                'required' => true,
            ]
        );

        $builder->add(
            'isDebug',
            ChoiceType::class,
            [
                'label' => 'metodi_pagamento_ecommerce.labels.is_debug',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $fields = [
            'nome' => [
                'label' => 'metodi_pagamento_ecommerce.labels.nome',
                'required' => true,
            ],
            'descrizione' => [
                'label' => 'metodi_pagamento_ecommerce.labels.descrizione',
                'field_type' => TextareaType::class,
                'required' => true,
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => MetodiPagamento::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}
