<?php
// 15/05/17, 14.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\MarketPlace;

class MarketPlaceForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label'       => 'default.labels.is_public',
                'choices'     => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required'    => false,
            ]
        );

        $builder->add(
            'nome',
            TextType::class,
            [
                'label' => 'market_place.labels.nome',
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'     => MarketPlace::class,
                'error_bubbling' => true,
                'langs'          => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}