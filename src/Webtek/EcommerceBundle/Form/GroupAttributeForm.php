<?php
// 27/04/17, 11.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\GroupAttribute;

class GroupAttributeForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $fields = [
            'titolo' => [
                'label'    => 'group_attribute.labels.titolo',
                'required' => true,
                'attr'     => ['class' => 'titolo'],
            ],
        ];

        $builder->add(
            'type',
            ChoiceType::class,
            [
                'label'       => 'group_attribute.labels.type',
                'choices'     => [
                    'group_attribute.labels.radio'  => 'RADIO',
                    'group_attribute.labels.color'  => 'COLOR',
                    'group_attribute.labels.select' => 'SELECT',
                ],
                'placeholder' => false,
                'required'    => false,
            ]
        );

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales'          => array_keys($options['langs']),
                'fields'           => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'     => GroupAttribute::class,
                'error_bubbling' => true,
                'langs'          => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }

}