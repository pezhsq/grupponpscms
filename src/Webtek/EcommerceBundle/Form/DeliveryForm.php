<?php

namespace Webtek\EcommerceBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\Delivery;
use GeoBundle\Service\NazioniHelper;

class DeliveryForm extends AbstractType
{

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var NazioniHelper
     */
    private $nazioniHelper;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add('ragioneSociale', TextType::class, ['label' => 'delivery.labels.ragione_sociale']);
        $builder->add('indirizzo', TextType::class, ['label' => 'delivery.labels.indirizzo']);
        $builder->add('comune', TextType::class, ['label' => 'delivery.labels.comune']);
        $builder->add('cap', TextType::class, ['label' => 'delivery.labels.cap']);
        $builder->add('provincia', TextType::class, ['label' => 'delivery.labels.provincia']);
        $builder->add('nazione', TextType::class, ['label' => 'delivery.labels.nazione']);
        $builder->add('telefono', TextType::class, ['label' => 'delivery.labels.telefono']);

        $builder->add(
            'metodi_pagamento_validi',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\MetodiPagamento',
//                'choices'           => $this->nazioniHelper->getNazioniChoices(),
                'multiple' => true,
                'required' => false
            ]
        );

        $fields = [
            'nome' => [
                'label' => 'delivery.labels.nome',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            'descrizione' => [
                'label' => 'delivery.labels.descrizione',
                'required' => false,
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

        $builder->add(
            'prices',
            CollectionType::class,
            [
                'entry_type' => DeliveryPricesForm::class,
                'empty_data' => function (FormInterface $form) {

                    return null;
                },
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_extra_fields' => true,
                'label' => false,
                'attr' => [],
            ]
        );

        $builder->add(
            'tax',
            EntityType::class,
            [
                'class' => 'Webtek\EcommerceBundle\Entity\Tax',
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Delivery::class,
                'error_bubbling' => true,
                'langs' => [
                    'it' => 'Italiano',
                ],
            ]
        );
    }


}