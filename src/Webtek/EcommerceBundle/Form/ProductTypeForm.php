<?php
// 16/03/2017 , 09:53
// @author : Gabriele "gabricom" Colombera <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Webtek\EcommerceBundle\Entity\MetodiSpedizione;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Webtek\EcommerceBundle\Entity\ProductType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProductTypeForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'nome',
            TextType::class,
            ['label' => 'product_type.labels.nome', 'attr' => ['class' => ''], 'required' => true]
        );

        $builder->add(
            'gruppiAttributi',
            EntityType::class,
            [
                'multiple' => true,
                'class'    => 'Webtek\EcommerceBundle\Entity\GroupAttribute',
                'label'    => 'product_type.labels.gruppi',
            ]

        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'     => ProductType::class,
                'error_bubbling' => true,
                'locale'         => 'en',
            ]
        );
    }


}
