<?php
// 20/05/17, 7.58
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Webtek\EcommerceBundle\Entity\PrezzoMarketPlace;

class PrezzoMarketPlaceForm extends AbstractType
{

    public function __construct(EntityManager $em)
    {

        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('marketPlace', HiddenType::class);

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label'       => 'default.labels.abilitato',
                'choices'     => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required'    => false,
                'attr'        => ['class' => 'marketPlaceEnabler'],
            ]
        );

        $builder->get('marketPlace')->addModelTransformer(
            new CallbackTransformer(
                function ($marketPlace) {

                    return $marketPlace;
                },
                function ($marketPlace) {

                    return $this->em->getRepository('WebtekEcommerceBundle:MarketPlace')->findOneBy(
                        ['id' => $marketPlace]
                    );
                }
            )
        );

        $builder->add(
            'valore',
            NumberType::class,
            [
                'label'       => 'product.labels.prezzo_netto_market_place',
                'iconAfter'   => 'fa fa-eur',
                'attr'        => ['class' => 'numeric prezzo-netto prezzo-market-place'],
                'constraints' => [new Range(['min' => 0])],
            ]
        );
        $builder->add(
            'valoreIvato',
            NumberType::class,
            [
                'label'     => 'product.labels.prezzo_ivato_market_place',
                'iconAfter' => 'fa fa-eur',
                'attr'      => ['class' => 'numeric prezzo-ivato prezzo-market-place'],
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'         => PrezzoMarketPlace::class,
                'allow_extra_fields' => true,
                'label'              => false,
            ]
        );
    }


}