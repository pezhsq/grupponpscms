<?php
// 15/05/17, 16.42
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Form;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webtek\EcommerceBundle\Entity\PrezzoVariante;

class PrezzoVarianteForm extends AbstractType
{

    public function __construct(EntityManager $em)
    {

        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('listino', HiddenType::class);
        $builder->add('varianteProdotto', HiddenType::class);

        $builder->get('listino')->addModelTransformer(
            new CallbackTransformer(
                function ($listino) {

                    return $listino;
                },
                function ($listino) {

                    return $this->em->getRepository('WebtekEcommerceBundle:Listino')->findOneBy(['id' => $listino]);
                }
            )
        );

        $builder->get('varianteProdotto')->addModelTransformer(
            new CallbackTransformer(
                function ($varianteProdotto) {

                    return $varianteProdotto;
                },
                function ($varianteProdotto) {

                    return $this->em->getRepository('WebtekEcommerceBundle:VarianteProdotto')->findOneBy(
                        ['id' => $varianteProdotto]
                    );
                }
            )
        );

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($options) {

                /**
                 * @var $PrezzoVariante PrezzoVariante
                 */
                $PrezzoVariante = $event->getData();
                $form           = $event->getForm();

                $lbl = '';

                if ($PrezzoVariante) {
                    $lbl = $PrezzoVariante->getListino()->translate()->getTitolo();
                }

                //
                $form->add(
                    'valore',
                    NumberType::class,
                    [
                        'label'     => $lbl.' (netto)',
                        'iconAfter' => 'fa fa-eur',
                        'attr'      => ['class' => 'numeric prezzo-netto'],
                    ]
                );
                $form->add(
                    'valoreIvato',
                    NumberType::class,
                    [
                        'label'     => $lbl.'(ivato)',
                        'iconAfter' => 'fa fa-eur',
                        'attr'      => ['class' => 'numeric prezzo-ivato'],
                    ]
                );

            }
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'         => PrezzoVariante::class,
                'allow_extra_fields' => true,
                'label'              => false,
            ]
        );
    }


}