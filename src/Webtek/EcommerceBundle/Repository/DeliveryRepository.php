<?php
// 24/04/17, 10.38
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Repository;


use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use Doctrine\ORM\EntityRepository;
use Webtek\EcommerceBundle\Entity\Carrello;

class DeliveryRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('delivery')
            ->andWhere('delivery.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function getByAddressAndCartQB(IndirizzoAnagrafica $address, $totaleCarrello)
    {

        $qb = $this->createQueryBuilder('delivery')
            ->leftJoin(
                'Webtek\EcommerceBundle\Entity\DeliveryPrices',
                'dp',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'dp.delivery = delivery.id'
            )->leftJoin(
                'dp.nations',
                'n',
                'WITH',
                'n.countryCode = :nazione'
            );

        $qb->setParameter('nazione', $address->getNazione()->getCountryCode());
        $qb->andWhere('dp.sogliaMin <= :totale')->andWhere('dp.sogliaMax >= :totale')->setParameter(
            'totale',
            $totaleCarrello
        )->andWhere('(dp.tutte = 1 AND n.countryCode is NULL) OR (n.countryCode IS NOT NULL)');

        return $qb;


    }

    function getByAddressAndCart(IndirizzoAnagrafica $address, $totaleCarrello)
    {

        $qb = $this->getByAddressAndCartQB($address, $totaleCarrello);
        
        return $qb->getQuery()->execute();

    }

}
