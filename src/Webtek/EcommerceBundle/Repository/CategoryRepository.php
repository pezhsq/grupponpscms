<?php
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Knp\DoctrineBehaviors\ORM as ORMBehaviors;

class CategoryRepository extends EntityRepository
{

    use ORMBehaviors\Tree\Tree;

    /**
     * Returns a node hydrated with its children and parents ordered by sort
     *
     * @api
     *
     * @param string $path
     * @param string $rootAlias
     *
     * @return NodeInterface a node
     */
    public function getTreeOrdered($path = '', $rootAlias = 't')
    {

        $resultsQB = $this->getFlatTreeQB($path, $rootAlias);
        $results = $resultsQB->addOrderBy($rootAlias.'.sort', 'ASC')->getQuery()->execute();

        return $this->buildTree($results);
    }

    /**
     * @param $array
     * @param $mp
     * @return array
     * Funzione ricorsiva per la creazione di un array ad albero a partire da una query secca che coinvolge le categorie
     */
    private function CategoryTreeBuilder($array, $mp)
    {

        $tree = [];
        foreach ($array as $cat) {
            if ($cat[0]->getMaterializedPath() == $mp) {
                $listImg = "";
                if ($cat[0]->getCategoryImgFilename() != "") {
                    $listImg = $cat[0]->getUploadDir().$cat[0]->getCategoryImgFilename();
                }
                $categoria = [
                    "id" => $cat[0]->getId(),
                    "listImg" => $listImg,
                    "icona" => $cat[0]->getIcona(),
                    "name" => $cat['nome'],
                    "slug" => $cat['slug'],
                    "children" => $this->CategoryTreeBuilder(
                        $array,
                        $cat[0]->getMaterializedPath()."/".$cat[0]->getId()
                    ),
                ];
                $tree[] = $categoria;
            }
        }

        return $tree;
    }


    public function getActiveByMaterializedPath($locale, $mp)
    {

        return $this->createQueryBuilder('category')->addSelect("ct.nome,ct.slug")->leftJoin(
            'Webtek\EcommerceBundle\Entity\CategoryTranslation',
            'ct',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'ct.translatable = category.id AND ct.locale = :locale'
        )->setParameter('locale', $locale)->andWhere('category.deletedAt IS NULL')->andWhere(
            'category.id != 1'
        )->andWhere('category.isEnabled = 1')->andWhere('category.materializedPath = :mp')->setParameter(
            'mp',
            $mp
        )->orderBy("category.sort")->getQuery()->execute();
    }

    /** VERSIONE CON 1 QUERY SOLA PER LA CREAZIONE DELLA STRUTTURA AD ALBERO */
    public function getAlberoFormattatoPerformance($locale, $root = "/1")
    {

        $query = $this->createQueryBuilder('category')->addSelect("ct.nome,ct.slug")->leftJoin(
            'Webtek\EcommerceBundle\Entity\CategoryTranslation',
            'ct',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'ct.translatable = category.id AND ct.locale = :locale'
        )->setParameter('locale', $locale)->andWhere('category.deletedAt IS NULL')->andWhere(
            'category.id != 1'
        )->andWhere('category.isEnabled = 1')->orderBy("category.sort")->getQuery();
        $categorie = $query->execute();
        $tree = $this->CategoryTreeBuilder($categorie, $root);

        return $tree;

    }

    /**
     * @param $locale
     * @param bool $root
     * @return array
     * Ritorna l'albero formattato utilizzando la ricerca dei figli di ogni categoria ricorsivamente (funziona ma fa molte query)
     */
    public function getAlberoFormattato($locale, $root = false)
    {

        if (!$root) {
            $root = $this->getTreeOrdered();
        }
        $tree = [];
        $children = $root->getChildNodes();
        foreach ($children as $child) {
            $tree[] = [
                "id" => $child->getId(),
                "name" => $child->translate($locale)->getNome(),
                "children" => $this->getAlberoFormattato($locale, $child),
            ];
        }

        return $tree;
    }

    function findBySlugPathButNotId($slug, $locale, $path, $id)
    {

        $query = $this->createQueryBuilder('category')->leftJoin(
            'Webtek\EcommerceBundle\Entity\CategoryTranslation',
            'ct',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'ct.translatable = category.id AND ct.locale = :locale'
        )->setParameter('locale', $locale)->andWhere('ct.slug = :slug')->setParameter('slug', $slug)->andWhere(
            'category.id != :id'
        )->setParameter('id', $id)->andWhere('category.materializedPath = :path')->setParameter(
            'path',
            $path
        )->andWhere('category.deletedAt IS NULL')->getQuery();

        return $query->execute();
    }

    public function getAllButRoot()
    {

        $QB = $this->createQueryBuilder('category')->where('category.id != 1');

        return $QB;
    }


    function countAllNotDeleted($locale = 'it')
    {

        $qb = $this->createQueryBuilder('cat');
        $qb->select($qb->expr()->count('cat'))->where('cat.deletedAt is NULL');
        $qb->leftJoin(
            'Webtek\EcommerceBundle\Entity\CategoryTranslation',
            'ct',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'ct.translatable = cat.id AND ct.locale = :locale'
        )->setParameter('locale', $locale);
        $qb->andWhere('ct.nome != :root')->setParameter('root', 'Radice');
        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }


    function search($searchTerm, $locale = 'it')
    {

        $qb = $this->createQueryBuilder('category')->leftJoin(
            'Webtek\EcommerceBundle\Entity\CategoryTranslation',
            'ct',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'ct.translatable = category.id AND ct.locale = :locale'
        )->setParameter('locale', $locale)->andWhere('ct.nome LIKE :searchTerm')->setParameter(
            'searchTerm',
            $searchTerm.'%'
        );

        return $qb->getQuery()->execute();

    }

    function getBySlug($slug, $locale = 'it', $onlyActive = true)
    {

        $qb = $this->createQueryBuilder('category')->leftJoin(
            'Webtek\EcommerceBundle\Entity\CategoryTranslation',
            'ct',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'ct.translatable = category.id AND ct.locale = :locale'
        )->setParameter('locale', $locale)->andWhere('ct.slug = :slug')->setParameter('slug', $slug);
        if ($onlyActive) {
            $qb->andWhere('category.isEnabled = 1')->andWhere('category.deletedAt is NULL');
        }

        return $qb->getQuery()->execute();

    }

    function getByIdsOrdered($ids)
    {

        $qb = $this->createQueryBuilder('c')->addSelect(
            'FIELD(c.id,'.implode(',', $ids).') as HIDDEN ordinamento'
        )->where('c.id IN (:ids)')->setParameter('ids', $ids)->orderBy('ordinamento');
        $query = $qb->getQuery();

        return $query->getResult();

    }

}
