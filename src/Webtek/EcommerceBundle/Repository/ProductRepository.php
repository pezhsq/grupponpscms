<?php
// 24/04/17, 10.38
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Repository;

use AppBundle\Traits\NotDeleted;
use Doctrine\ORM\EntityRepository;
use Webtek\EcommerceBundle\Entity\Category;
use AnagraficaBundle\Entity\Anagrafica;

class ProductRepository extends EntityRepository
{

	use NotDeleted;

	function getLastCreated($limit = false, $category = false)
	{

		$qb = $this->createQueryBuilder("product")->andWhere("product.isEnabled=1")->andWhere(
			"product.deletedAt IS NULL"
		)->orderBy("product.createdAt", "DESC");
		if ($limit) {
			$qb = $qb->setMaxResults($limit);
		}
		if ($category) {
			$qb = $qb->join("product.categorie", "categorie")->andWhere("categorie = :categoria")->setParameter(
				"categoria",
				$category
			);
		}

		return $qb->getQuery()->getResult();
	}

	public function getProdottiAttiviCategory(Category $Category, $order = false, $dir = false, $locale = false)
	{

		$query = $this->createQueryBuilder("products")->join("products.categorie", "categorie")->andWhere(
			"products.isEnabled = 1"
		)->andWhere("categorie.id=:category")->andWhere("products.deletedAt is NULL")->setParameter(
			"category",
			$Category->getId()
		);
		if ($order && $dir) {
			if ($locale) {
				if ($order == "name") {
					$query->join("products.translations", "pt")->andWhere("pt.locale = :locale")->setParameter(
						"locale",
						$locale
					)->orderBy("pt.titolo", $dir);
				}
			}
			if ($order == "price") {
				$query->leftJoin("products.varianti", "varianti")->leftJoin("varianti.prezzi", "prezzi")->andWhere(
					"varianti.isDefault = 'true' OR varianti.isDefault is NULL "
				)->addSelect(
					"(CASE WHEN prezzi.valore IS NOT NULL AND products.prezzo_dipende_da_variante  = 1 THEN prezzi.valore ELSE prezziprod.valore END) AS HIDDEN ORD"
				)->join("products.prezzi", "prezziprod")->OrderBy("ORD", $dir);
			}

		}

		return $query->getQuery()->execute();
	}

	function getWishlistProducts(
		Anagrafica $anagrafica,
		$order = false,
		$dir = false,
		$locale = 'it',
		$searchTerm = false
	)
	{

		$q = $this->createQueryBuilder('products')->join("products.wishlist_holders", "anagrafica")->andWhere(
			'products.isEnabled=1'
		)->andWhere('products.deletedAt IS NULL')->andWhere('anagrafica.id = :anagrafica')->setParameter(
			'anagrafica',
			$anagrafica->getId()
		);
		$q = $this->createQueryBuilder('products')->join("products.wishlist_holders", "anagrafica")->andWhere(
			'products.isEnabled=1'
		)->andWhere('products.deletedAt IS NULL')->andWhere('anagrafica.id = :anagrafica')->setParameter(
			'anagrafica',
			$anagrafica->getId()
		);
		if (isset($searchTerm) && $searchTerm != "") {
			$q = $q->setParameter(
				'searchTerm',
				'%' . $searchTerm . '%'
			)->leftJoin("products.tags", "tags")->leftJoin("products.translations", "pt")->andWhere(
				"pt.locale = :locale OR pt.locale IS NULL"
			)->setParameter(
				"locale",
				$locale
			)->leftJoin("tags.translations", "tag_trans")->andWhere(
				"tag_trans.locale = :locale OR tag_trans.locale IS NULL"
			)->andWhere(
				'pt.titolo LIKE :searchTerm
             OR tag_trans.nome LIKE :searchTerm
             OR products.codice LIKE :searchTerm
             OR products.codice_produttore LIKE :searchTerm
             OR products.ean LIKE :searchTerm'
			);
		}
		if ($order && $dir) {
			if ($locale) {
				if ($order == "name") {
					$q->join("products.translations", "pt")->andWhere("pt.locale = :locale")->setParameter(
						"locale",
						$locale
					)->orderBy("pt.titolo", $dir);
				}
			}
			if ($order == "price") {
				$q->leftJoin("products.varianti", "varianti")->leftJoin("varianti.prezzi", "prezzi")->andWhere(
					"varianti.isDefault = 'true' OR varianti.isDefault is NULL "
				)->addSelect(
					"(CASE WHEN prezzi.valore IS NOT NULL AND products.prezzo_dipende_da_variante  = 1 THEN prezzi.valore ELSE prezziprod.valore END) AS HIDDEN ORD"
				)->join("products.prezzi", "prezziprod")->OrderBy("ORD", $dir);
			}

		}

		return $q->getQuery()->execute();
	}

	function search(
		$searchTerm,
		$exclude = [],
		$locale = 'it',
		$brand = false,
		$category = false,
		$order = false,
		$dir = false
	)
	{

		$qb = $this->createQueryBuilder('product')->leftJoin(
			'Webtek\EcommerceBundle\Entity\ProductTranslation',
			'pt',
			\Doctrine\ORM\Query\Expr\Join::WITH,
			'pt.translatable = product.id AND pt.locale = :locale'
		)->setParameter('locale', $locale)->andWhere("product.deletedAt is NULL")->andWhere("product.isEnabled = 1");
		if (isset($searchTerm) && $searchTerm != "") {
			$qb = $qb->andWhere("product.isEnabled = 1")->setParameter(
				'searchTerm',
				'%' . $searchTerm . '%'
			)->leftJoin("product.tags", "tags")->leftJoin("tags.translations", "tag_trans")->andWhere(
				"tag_trans.locale = :locale OR tag_trans.locale IS NULL"
			)->leftJoin("product.brand", "brand")->leftJoin("brand.translations", "brand_trans")->leftJoin(
				"product.categorie",
				"category"
			)->leftJoin("category.translations", "cat_trans")->andWhere(
				'pt.titolo LIKE :searchTerm
             OR tag_trans.nome LIKE :searchTerm 
             OR product.codice LIKE :searchTerm
             OR product.codice_produttore LIKE :searchTerm
             OR product.ean LIKE :searchTerm
             OR brand_trans.titolo LIKE :searchTerm
             OR cat_trans.nome LIKE :searchTerm'
			)->andWhere("cat_trans.locale= :locale OR cat_trans.locale IS NULL");
		}
		if ($exclude) {
			$qb->andWhere($qb->expr()->notIn('product.id', $exclude));
		}
		if ($brand && $brand != "") {
			$qb = $qb->join("product.brand", "brand")->join("brand.translations", "brand_trans")->andWhere(
				"brand_trans.titolo = :brand"
			)->andWhere("brand_trans.locale = :locale")->setParameter("brand", $brand);
		}
		if ($category && $category != "") {
			$qb = $qb->join("product.categorie", "category")->join("category.translations", "cat_trans")->andWhere(
				"cat_trans.nome = :category"
			)->andWhere("cat_trans.locale= :locale")->setParameter("category", $category);
		}
		if ($order && $dir) {
			if ($locale) {
				if ($order == "name") {
					$qb->andWhere("pt.locale = :locale")->setParameter("locale", $locale)->orderBy("pt.titolo", $dir);
				}
			}
			if ($order == "price") {
				$qb->leftJoin("product.varianti", "varianti")->leftJoin("varianti.prezzi", "prezzi")->andWhere(
					"varianti.isDefault = 'true' OR varianti.isDefault is NULL "
				)->addSelect(
					"(CASE WHEN prezzi.valore IS NOT NULL AND product.prezzo_dipende_da_variante  = 1 THEN prezzi.valore ELSE prezziprod.valore END) AS HIDDEN ORD"
				)->join("product.prezzi", "prezziprod")->OrderBy("ORD", $dir);
			}

		}

		return $qb->getQuery()->execute();

	}

	function getPiuVenduti($limit = false, $category = false)
	{

		$qb = $this->createQueryBuilder("product")->andWhere("product.isEnabled=1")->andWhere(
			"product.deletedAt IS NULL"
		)->orderBy(
			"product.saleCounter",
			"DESC"
		);
		if ($limit) {
			$qb = $qb->setMaxResults($limit);
		}
		if ($category) {
			$qb = $qb->join("product.categorie", "categorie")->andWhere("categorie = :categoria")->setParameter(
				"categoria",
				$category
			);
		}

		return $qb->getQuery()->getResult();
	}

	function findBySlug($slug, $locale = 'it')
	{

		$qb = $this->createQueryBuilder('product')->leftJoin(
			'Webtek\EcommerceBundle\Entity\ProductTranslation',
			'pt',
			\Doctrine\ORM\Query\Expr\Join::WITH,
			'pt.translatable = product.id AND pt.locale = :locale'
		)->setParameter('locale', $locale)->andWhere('pt.slug = :searchTerm')->setParameter(
			'searchTerm',
			$slug
		)->andWhere('product.deletedAt is NULL');

		return $qb->getQuery()->getOneOrNullResult();

	}

	function findByBrand($locale, $brand, $limit = false, $random = false, $start = false, $order = false, $dir = false)
	{

		$qb = $this->createQueryBuilder('product')->leftJoin(
			'Webtek\EcommerceBundle\Entity\ProductTranslation',
			'pt',
			\Doctrine\ORM\Query\Expr\Join::WITH,
			'pt.translatable = product.id AND pt.locale = :locale'
		)->setParameter('locale', $locale)->join("product.brand", 'brand')->join(
			'brand.translations',
			'brand_trans'
		)->andWhere("product.isEnabled = 1")->andWhere('brand_trans.slug = :brand')->andWhere(
			'brand_trans.locale = :localedef'
		)->setParameter("localedef", 'it')->setParameter('brand', $brand)->andWhere('product.deletedAt is NULL');
		if ($random) {
			$qb = $qb->orderBy("RAND()");
		}
		if ($limit) {
			$qb = $qb->setMaxResults($limit);
		}
		if ($start) {
			$qb = $qb->setFirstResult($start);
		}
		if ($order && $dir) {
			if ($locale) {
				if ($order == "name") {
					$qb->andWhere("pt.locale = :locale")->setParameter("locale", $locale)->orderBy("pt.titolo", $dir);
				}
			}
			if ($order == "price") {
				$qb->leftJoin("product.varianti", "varianti")->leftJoin("varianti.prezzi", "prezzi")->andWhere(
					"varianti.isDefault = 'true' OR varianti.isDefault is NULL "
				)->addSelect(
					"(CASE WHEN prezzi.valore IS NOT NULL AND product.prezzo_dipende_da_variante  = 1 THEN prezzi.valore ELSE prezziprod.valore END) AS HIDDEN ORD"
				)->join("product.prezzi", "prezziprod")->OrderBy("ORD", $dir);
			}

		}

		return $qb->getQuery()->execute();

	}

	function findBySlugButNotId($slug, $locale, $id)
	{

		$qb = $this->createQueryBuilder('p')->leftJoin(
			'Webtek\EcommerceBundle\Entity\ProductTranslation',
			'pt',
			\Doctrine\ORM\Query\Expr\Join::WITH,
			'pt.translatable = p.id AND pt.locale = :locale'
		)->setParameter('locale', $locale)->andWhere('pt.slug = :slug')->setParameter('slug', $slug);
		if ($id) {

			$qb->andWhere(
				'p.id != :id'
			)->setParameter('id', $id);
		}
		$query = $qb->andWhere('p.deletedAt IS NULL')->getQuery();

		return $query->execute();
	}

	function getSortedByIds(array $ids)
	{

		$qb = $this->createQueryBuilder('p')->addSelect(
			'FIELD(p.id,' . implode(',', $ids) . ') as HIDDEN ordinamento'
		)->where('p.id IN (:ids)')->setParameter('ids', $ids)->orderBy('ordinamento');
		$query = $qb->getQuery();

		return $query->getResult();

	}

	function getSortedByCategory($category)
	{

		$qb = $this->createQueryBuilder('p');
		$qb->leftJoin(
			'Webtek\EcommerceBundle\Entity\ProductSort',
			'ps',
			\Doctrine\ORM\Query\Expr\Join::WITH,
			'ps.product = p'
		);
		$qb->andWhere('ps.category = :category')->setParameter('category', $category);
		$qb->orderBy('ps.sort', 'ASC');
		$query = $qb->getQuery();

		return $query->getResult();

	}

}
