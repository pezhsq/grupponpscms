<?php

namespace Webtek\EcommerceBundle\Repository;


use Doctrine\ORM\EntityRepository;

class FeatureRepository extends EntityRepository
{

    function findAllNotDeleted($parent = 0)
    {

        $qb = $this->createQueryBuilder('g')
            ->andWhere('g.deletedAt is NULL');
        if ($parent) {
            $qb->andWhere('g.featureGroup = :parent')
                ->setParameter('parent', $parent);
        }

        return $qb->getQuery()->execute();
    }

    function getSortedByIds(array $ids)
    {

        $qb = $this->createQueryBuilder('t')
            ->addSelect('FIELD(t.id,'.implode(',', $ids).') as HIDDEN ordinamento')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('ordinamento');

        $query = $qb->getQuery();

        return $query->getResult();

    }

}