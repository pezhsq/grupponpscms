<?php
// 06/11/17, 8.58
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Service;

use AppBundle\Traits\NotDeleted;
use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\Coupon;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\RecordCarrello;

class CouponHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * CouponsHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Coupons = $this->entityManager->getRepository('WebtekEcommerceBundle:Coupon')->findAll();
        } else {
            $Coupons = $this->entityManager->getRepository('WebtekEcommerceBundle:Coupon')->findAllNotDeleted();
        }
        $records = [];
        foreach ($Coupons as $Coupon) {


            $prodottiAttivi = 'Tutti';
            if (!$Coupon->getAllProducts()) {
                $categorie = $Coupon->getCategorie();
                $prodottiAttivi = [];
                foreach ($categorie as $categoria) {
                    $prodottiAttivi[] = (string)$categoria;
                }
                $prodottiAttivi = implode(', ', $prodottiAttivi);
            }
            /**
             * @var $Coupon Coupon;
             */
            $record = [];
            $record['id'] = $Coupon->getId();
            $record['code'] = $Coupon->getCode();
            $record['titolo'] = $Coupon->translate()->getTitolo();
            $record['minimoordine'] = $Coupon->getMinimumOrder();
            $record['value'] = ' -'.$Coupon->getValue().' '.(($Coupon->getType()) ? '€' : '%');
            $record['prodotti_attivi'] = $prodottiAttivi;
            $record['available'] = $Coupon->getIllimitato() ? 'Illimitato' : $Coupon->getAvailable();
            $record['available'] .= ' (utilizzati: '.$this->getUtilizzati($Coupon).')';
            $record['deleted'] = $Coupon->isDeleted();
            $record['isEnabled'] = $Coupon->getIsEnabled();
            $record['createdAt'] = $Coupon->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Coupon->getUpdatedAt()->format('d/m/Y H:i:s');
            $records[] = $record;
        }

        return $records;

    }

    public function getUtilizzati(Coupon $coupon)
    {

        $utilizzati = $this->entityManager->getRepository('WebtekEcommerceBundle:Carrello')->contaCoupons(
                $coupon
            ) + $this->entityManager->getRepository('WebtekEcommerceBundle:Order')->contaCoupons($coupon);

        return $utilizzati;


    }

    public function isAppliable($categorieCouponId, Product $Prodotto)
    {

        $categorieProdotto = $Prodotto->getCategorie();
        $categorieProdottoId = [];
        foreach ($categorieProdotto as $Categoria) {
            /**
             * @var $Categoria Category
             */
            $categorieProdottoId[] = $Categoria->getId();
        }
        if (array_intersect($categorieCouponId, $categorieProdottoId)) {
            return true;
        }

        return false;

    }


}