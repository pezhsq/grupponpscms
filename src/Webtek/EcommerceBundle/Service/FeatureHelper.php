<?php

namespace Webtek\EcommerceBundle\Service;


use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Feature;

class FeatureHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * FeaturesHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Features = $this->entityManager->getRepository('WebtekEcommerceBundle:Feature')->findAll();
        } else {
            $Features = $this->entityManager->getRepository('WebtekEcommerceBundle:Feature')->findAllNotDeleted();
        }

        $records = [];

        foreach ($Features as $Feature) {


            /**
             * @var $Feature Feature;
             */

            $record              = [];
            $record['id']        = $Feature->getId();
            $record['titolo']    = $Feature->translate()->getTitolo();
            $record['sort']      = $Feature->getSort();
            $record['gruppo']    = (string)$Feature->getFeatureGroup();
            $record['deleted']   = $Feature->isDeleted();
            $record['createdAt'] = $Feature->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Feature->getUpdatedAt()->format('d/m/Y H:i:s');


            $records[] = $record;
        }

        return $records;

    }

    public function getOptions($parent)
    {

        $Features = $this->entityManager->getRepository('WebtekEcommerceBundle:Feature')->findAllNotDeleted($parent);

        $records = [];
        
        foreach ($Features as $Feature) {

            /**
             * @var $Feature Feature;
             */

            $record          = [];
            $record['id']    = $Feature->getId();
            $record['label'] = $Feature->translate()->getTitolo();


            $records[] = $record;
        }

        return $records;

    }

}