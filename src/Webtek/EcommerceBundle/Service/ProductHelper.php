<?php

namespace Webtek\EcommerceBundle\Service;

use AppBundle\Entity\Attachment;
use AppBundle\Service\WebDir;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Imagine\Filter\Basic\Save;
use Imagine\Gd\Imagine;
use Liip\ImagineBundle\Controller\ImagineController;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Liip\ImagineBundle\Imagine\Filter\Loader\ThumbnailFilterLoader;
use Liip\ImagineBundle\Model\Binary;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Webtek\EcommerceBundle\Entity\Carrello;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\Coupon;
use Webtek\EcommerceBundle\Entity\Listino;
use Webtek\EcommerceBundle\Entity\Prezzo;
use Webtek\EcommerceBundle\Entity\PrezzoVariante;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\ProductAttachment;
use Webtek\EcommerceBundle\Entity\ProductSort;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;

class ProductHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var TaxCalculator
     */
    private $taxCalculator;

    private $ecommerceConfig;

    private $varianteDefault = null;
    /**
     * @var UploaderHelper
     */
    private $vichUploaderHelper;
    /**
     * @var WebDir
     */
    private $webDir;
    /**
     * @var ImagineController
     */
    private $liipImagineController;
    /**
     * @var CacheManager
     */
    private $liipCacheManager;

    private $pathManager;

    private $listinoHelper;
    /**
     * @var CouponHelper
     */
    private $couponHelper;

    /**
     * ProductsHelper constructor.
     */
    public function __construct(
        EntityManager $entityManager,
        TaxCalculator $taxCalculator,
        $ecommerceConfig,
        WebDir $webDir,
        ImagineController $liipImagineController,
        CacheManager $liipCacheManager,
        $pathManager,
        $listinoHelper,
        CouponHelper $couponHelper
    ) {

        $this->entityManager = $entityManager;
        $this->taxCalculator = $taxCalculator;
        $this->ecommerceConfig = $ecommerceConfig;
        $this->webDir = $webDir;
        $this->liipImagineController = $liipImagineController;
        $this->liipCacheManager = $liipCacheManager;
        $this->pathManager = $pathManager;
        $this->listinoHelper = $listinoHelper;
        $this->couponHelper = $couponHelper;
    }


    public function guessCategory(Product $Product)
    {

        return $this->entityManager->createQueryBuilder()->select("cat")->from(
            "WebtekEcommerceBundle:Category",
            "cat"
        )->join("cat.products", "p")->andWhere("p = :prodotto")->setParameter("prodotto", $Product)->orderBy(
            "LENGTH(cat.materializedPath)",
            "desc"
        )->setMaxResults(1)->getQuery()->getOneOrNullResult();

    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Products = $this->entityManager->getRepository('WebtekEcommerceBundle:Product')->findAll();
        } else {
            $Products = $this->entityManager->getRepository('WebtekEcommerceBundle:Product')->findAllNotDeleted();
        }
        $records = [];
        foreach ($Products as $Product) {


            /**
             * @var $Product Product;
             */
            $record = [];
            $record['id'] = $Product->getId();
            $record['titolo'] = $Product->translate()->getTitolo();
            $record['immagine'] = false;
            $options = [];
            $options['size'] = ['w' => 100, 'h' => 100];
            if ($Product->getListImgFileName()) {
                $img = $this->getListImg($Product, 'src', $options);
                $record['immagine'] = $img;
            }
            $record['categorie'] = '';
            $categorie = [];
            $Categorie = $Product->getCategorie();
            foreach ($Categorie as $Categoria) {
                if ($Categoria->getId() != 1) {
                    $categorie[] = (string)$Categoria;
                }
            }
            $Brand = $Product->getBrand();
            $record['brand'] = $Brand;
            if ($Brand) {
                $record['brand'] = (string)$Brand;
            }
            $record['categorie'] = implode(', ', $categorie);
            $record['codice'] = $Product->getCodice();
            $record['tags'] = $Product->getTagsList();
            $record['codice_produttore'] = $Product->getCodiceProduttore();
            $record['url'] = $Product->translate('it')->getSlug();
            $record['isEnabled'] = $Product->getIsEnabled() ? 'Pubblicato' : 'NON PUBBLICATO';
            $varianti = $Product->getVarianti();
            $varianti = $varianti->filter(
                function ($entry) {

                    /**
                     * @var $entry VarianteProdotto
                     */
                    if (!$entry->isDeleted()) {
                        return $entry;
                    }
                }
            );
            $record['conta_varianti'] = count($varianti);
            $record['deleted'] = $Product->isDeleted();
            $record['isEnabled'] = $Product->getIsEnabled();
            $record['createdAt'] = $Product->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Product->getUpdatedAt()->format('d/m/Y H:i:s');
            $record['recensioni'] = count($Product->getRecensioni());
            $records[] = $record;
        }

        return $records;

    }

    public function getListImg(Product $product, $attribute = 'src', $options = [])
    {

        $img = [];
        $img['src'] = '/images/loghi/placeholder.png';
        if ($product->getListImgFileName()) {
            $path = '/'.$product->getUploadDir().$product->getListImgFileName();
            $fullPath = $this->webDir->get().$path;
            if (file_exists($fullPath)) {
                $img['src'] = '/'.$product->getUploadDir().$product->getListImgFileName();
            } else {
                return $img[$attribute];
            }
        }
        if (isset($options['size'])) {
            $name = $product->getListImgFileName();
            $name = explode('.', $name);
            $ext = array_pop($name);
            $name = implode($name);
            $name .= $options['size']['w'].'x'.$options['size']['h'].'.'.$ext;
            $newPath = $this->webDir->get().'/'.$product->getUploadDir().$name;
            if (!file_exists($newPath)) {

                $Imagine = new Imagine();
                $percorso = $this->webDir->get().$img['src'];
                $toResize = $Imagine->open($percorso);
                $thumb = new ThumbnailFilterLoader();
                $thumb = $thumb->load(
                    $toResize,
                    [
                        'mode' => 'inset',
                        'size' => [$options['size']['w'], $options['size']['h'][1]],
                    ]
                );
                $Saver = new Save($newPath);
                $Saver->apply($thumb);
            }
            $img['src'] = $product->getUploadDir().$name;

        }

        return $img[$attribute];

    }


    public function getPrezzoImponibile(
        Product $product,
        Listino $listino,
        VarianteProdotto $Variante = null,
        Coupon $coupon = null
    ) {

        $prezzo = 0;
        if ($this->ecommerceConfig['manage_attributes'] && $product->getPrezzoDipendeDaVariante(
            ) && $product->getVarianteDefault()) {

            /**
             * @var $Variante VarianteProdotto
             */
            if (!$Variante) {

                $Variante = $this->getVarianteDefault($product);

            }
            if ($Variante) {


                $PrezziVariante = $Variante->getPrezzi()->filter(
                    function ($entry) use ($listino) {

                        /**
                         * @var $entry PrezzoVariante
                         */
                        if ($entry->getListino() == $listino) {
                            return $entry;
                        }
                    }
                );
                if ($PrezziVariante) {

                    $PrezzoVariante = $PrezziVariante->first();
                    /**
                     * @var $PrezzoVariante PrezzoVariante
                     */
                    $prezzo = $PrezzoVariante->getValore();

                }

            }


        } else {

            $Prezzi = $product->getPrezzi();
            foreach ($Prezzi as $Prezzo) {

                /**
                 * @var $Prezzo Prezzo
                 */
                if ($Prezzo->getListino() == $listino) {

                    $prezzo = $Prezzo->getValore();

                }

            }


        }
        if ($coupon) {
            $prezzo = $this->applicaCoupon($product, $prezzo, $coupon);
        }

        return $prezzo;

    }

    public function getPrezzo(
        Product $product,
        Listino $listino,
        VarianteProdotto $Variante = null,
        Coupon $coupon = null
    ) {

        $prezzo = $this->taxCalculator->CalcoloImpostaRaw(
            $this->getPrezzoImponibile($product, $listino, $Variante, $coupon),
            $product->getTax()->getAliquota(),
            2
        );

        return $prezzo['prezzo_tassato'];
    }

    public function applicaCoupon(Product $product, $prezzo, Coupon $coupon)
    {

        if ($coupon->getType() == Coupon::TYPE_PERCENTAGE) {
            if ($coupon->getAllProducts()) {
                $prezzo = $prezzo - ($prezzo * $coupon->getValue() / 100);
            } else {
                $categorieCoupon = $coupon->getCategorie();
                $categorieCouponId = [];
                foreach ($categorieCoupon as $Categoria) {
                    /**
                     * @var $Categoria Category
                     */
                    $categorieCouponId[] = $Categoria->getId();
                }
                if ($this->couponHelper->isAppliable($categorieCouponId, $product)) {
                    $prezzo = $prezzo - ($prezzo * $coupon->getValue() / 100);
                }

            }
        }

        return $prezzo;

    }


    public function getImmaginePrincipale(Product $product)
    {

        $var = $product->getVarianteDefault();
        if ($this->ecommerceConfig['manage_attributes'] && $var) {
            if ($this->getFirstPhoto($var)) {
                return $this->getFirstPhoto($var);
            } else {
                return $product->getUploadDir().$product->getListImgFilename();
            }
        } else {
            return $product->getUploadDir().$product->getListImgFilename();
        }

    }

    private function VarianteParser(VarianteProdotto $Variante)
    {

        $return['prezzo'] = $this->getPrezzo(
            $Variante->getProdotto(),
            $this->listinoHelper->getDefault(),
            $Variante
        );
        $return['available'] = $this->isVarianteAvailable($Variante);
        $return['firstPhoto'] = $this->getFirstPhoto($Variante);

        return $return;
    }


    private function set_element(&$path, $data)
    {

        return ($key = array_pop($path)) ? $this->set_element($path, [$key => $data]) : $data;
    }


    public function getMatrice(Product $product, $locale)
    {

        $varianti = $product->getVarianti();
        $matrice = [];
        foreach ($varianti as $var) {
            $attributi = $var->getAttributesOrderedbyGruppo();
            $index = "";
            $prova = [];
            foreach ($attributi as $attr) {
                if ($index != "") {
                    $index .= "-";
                }
                $index .= $attr->getId();
                $prova[] = $attr->getId();
            }
            $prova = $this->set_element($prova, $this->VarianteParser($var));
            $matrice = array_replace_recursive($matrice, $prova);
        }

        return json_encode($matrice);
    }


    public function getAttributi(Product $product, $locale)
    {

        $attributi = [];
        $matrice = [];
        $default = $product->getVarianteDefault();
        $attr_default = $default->getAttributes();
        $gruppoAttributi = $product->getProductType()->getGruppiAttributi();
        foreach ($gruppoAttributi as $gruppo) {
            $attr = $product->getAttributesByGroup($gruppo);
            if (count($attr)) {
                $record = [];
                $record['nome'] = $gruppo->translate($locale)->getTitolo();
                $record['type'] = $gruppo->getType();
                $v = [];
                foreach ($attr as $at) {
//                    $matrice[$at->getId()]
                    $selected = false;
                    foreach ($attr_default as $a) {
                        if ($a == $at) {
                            $selected = true;
                        }
                    }
                    $v[] = [
                        "id" => $at->getId(),
                        "nome" => $at->translate($locale)->getTitolo(),
                        "selected" => $selected,
                        "value" => $at->getValue(),
                    ];
                }
                $record['attributi'] = $v;
                $attributi[] = $record;
            }

        }

        return [
            "selezione" => $attributi,
        ];
    }


    public function getVariantByAttributes(Product $product, array $attributes = [])
    {

        if ($attributes && is_array($attributes)) {

            $attributes = array_filter($attributes);
            $attributes = array_filter($attributes, 'is_numeric');
            $varianti = $product->getVarianti()->filter(
                function ($variante) use ($attributes) {

                    /**
                     * @var $variante VarianteProdotto
                     */
                    $arrayId = [];
                    foreach ($variante->getAttributes() as $attribute) {
                        $arrayId[] = $attribute->getId();
                    }
                    sort($arrayId);
                    sort($attributes);
                    if ($arrayId == $attributes) {
                        return $variante;
                    }

                }
            );
            if ($varianti) {
                return $varianti->first();
            }


        }

        return false;
    }

    /**
     * @param Product $product
     * @return VarianteProdotto
     */
    public function getVarianteDefault(Product $product)
    {

//        if (!$this->varianteDefault) {
        $this->varianteDefault = $product->getVarianteDefault();

//        }
        return $this->varianteDefault;

    }


    public function isVarianteAvailable(VarianteProdotto $Variante)
    {

        /**
         * @var $Product Product
         */
        $Product = $Variante->getProdotto();
        if ($Product->getAvailable() && $Product->getisAlwaysAvailable()) {
            return true;
        }
        if ($Variante && $Variante->getGiacenza()) {
            return true;
        }

        return false;

    }

    public function getFirstPhoto(VarianteProdotto $Variante)
    {

        $Photos = $Variante->getPhotos();
        if ($Photos->count()) {

            /**
             * @var $Photo Attachment
             */
            $Photo = $Photos->first();
            $path = $Variante->getProdotto()->getUploadDir().$Photo->getName();
            $response = $this->liipImagineController->filterAction(
                new Request(),
                $path,
                'product_middle'
            );

            return $this->liipCacheManager->getBrowserPath(
                $path,
                'product_middle'
            );

        }

        return false;

    }


    /**
     * @param Product $Product
     * @return bool
     */
    public function isAvailable(Product $Product, VarianteProdotto $varianteProdotto = null)
    {


        if ($this->ecommerceConfig['manage_attributes'] && $Product->getVarianti()->count()) {

            if ($varianteProdotto) {

                return $this->isVarianteAvailable($varianteProdotto);

            } else {

                return $this->isVarianteAvailable($this->getVarianteDefault($Product));
            }

        } else {

            if ($Product->getAvailable() && $Product->getisAlwaysAvailable()) {
                return true;
            }

            return (bool)$Product->getGiacenza();

        }


    }

    public function productsEntityParser($Products, $request)
    {

        $data = [];
        foreach ($Products as $p) {
            $data[] = $this->productEntityParser($p, $request);
        }

        return $data;
    }

    public function productsQueryResultParser($Products, $request)
    {

        $data = [];
        foreach ($Products as $p) {
            $data[] = $this->productQueryResultParser($p, $request);
        }

        return $data;
    }

    public function productQueryResultParser($Product, $request)
    {

        $record = [];
        $record['titolo'] = $Product['titolo'];
        $record['img'] = "/images/defaults/placeholder.png";
        $record['imgAlt'] = $Product['titolo'];
        //TODO METTERE FILTRO GIUSTo
        if ($Product[0]->getListImgFileName()) {
            $this->liipImagineController->filterAction(
                $request,
                $Product[0]->getUploadDir().$Product[0]->getListImgFileName(),
                'news4'
            );
            $record['img'] = $this->liipCacheManager->getBrowserPath(
                $Product[0]->getUploadDir().$Product[0]->getListImgFileName(),
                'news4'
            );
//                $record['imgAlt'] = $News->getHeaderImgAlt();
        }
        $record['inWishlist'] = "";
        if ($Product[0]->getInWishlist()) {
            $record['inWishlist'] = "active";
        }
        $record['id'] = $Product[0]->getId();
        $record['brand'] = $Product[0]->getBrand()->translate($request->getLocale())->getTitolo();
        $record['prezzo'] = twig_localized_currency_filter(
            $this->getPrezzo($Product[0], $this->listinoHelper->getDefault()),
            "EUR"
        );
        $record['url'] = $this->pathManager->generateUrl('prodotto_ecommerce', ["slug" => $Product['slug']]);

        return $record;
    }

    public function productEntityParser(Product $Product, $request)
    {

        $record = [];
        $record['titolo'] = $Product->translate($request->getLocale())->getTitolo();
        $record['img'] = "/images/defaults/placeholder.png";
        $record['imgAlt'] = $Product->translate($request->getLocale())->getTitolo();
        $record['categoria'] = $this->guessCategory($Product)->translate($request->getLocale())->getNome();
        if ($Product->getListImgFileName()) {
            $this->liipImagineController->filterAction(
                $request,
                $Product->getUploadDir().$Product->getListImgFileName(),
                'news4'
            );
            $record['img'] = $this->liipCacheManager->getBrowserPath(
                $Product->getUploadDir().$Product->getListImgFileName(),
                'news4'
            );
//                $record['imgAlt'] = $News->getHeaderImgAlt();
        }
        $record['inWishlist'] = "";
        if ($Product->getInWishlist()) {
            $record['inWishlist'] = "active";
        }
        $record['id'] = $Product->getId();
        $record['brand'] = $Product->getBrand()->translate($request->getLocale())->getTitolo();
        $record['review_score'] = $Product->getMediaRecensioni();
        $record['prezzo'] = twig_localized_currency_filter(
            $this->getPrezzo($Product, $this->listinoHelper->getDefault()),
            "EUR"
        );
        $record['url'] = $this->pathManager->generateUrl(
            'prodotto_ecommerce',
            ["slug" => $Product->translate($request->getLocale())->getSlug()]
        );

        return $record;
    }

    public function getGiacenza(Product $Product, VarianteProdotto $varianteProdotto = null)
    {

        if ($varianteProdotto) {
            return $varianteProdotto->getGiacenza();
        }

        return $Product->getGiacenza();
    }

    public function formatText($text)
    {

        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        $text = preg_replace_callback(
            '/<youtube>([^\<].*)<\/youtube>/',
            function ($data) {

                return '<div class="panel panel-default col-md-6 panel-video"><div class="panel-heading"><h3 class="panel-title">Video</h3></div><div class="panel-body">
                    <iframe src="https://www.youtube.com/embed/'.$data[1].'?rel=0" frameborder="0" allowfullscreen></iframe></div>
                </div>';
            },
            $text
        );

        return $text;
    }

    public function saveOrderForCategory($products, Category $category)
    {

        foreach ($products as $k => $product) {

            $toDelete = $this->entityManager->getRepository('WebtekEcommerceBundle:ProductSort')->findBy(
                ['product' => $product, 'category' => $category]
            );
            foreach ($toDelete as $delet) {
                $this->entityManager->remove($delet);
            }
            $this->entityManager->flush();
            $ProductSort = new ProductSort();
            $ProductSort->setProduct($product);
            $ProductSort->setSort($k);
            $ProductSort->setCategory($category);
            $this->entityManager->persist($ProductSort);
            $this->entityManager->flush();

        }

        return true;

    }

    public function createSortEntry(Product $product, Category $category)
    {

        $ProductSort = $this->entityManager->getRepository('WebtekEcommerceBundle:ProductSort')->findBy(
            ['product' => $product, 'category' => $category]
        );
        if (!$ProductSort) {

            $ProductSortPresenti = $this->entityManager->getRepository('WebtekEcommerceBundle:ProductSort')->findBy(
                ['category' => $category]
            );
            $ProductSort = new ProductSort();
            $ProductSort->setProduct($product);
            $ProductSort->setSort(count($ProductSortPresenti));
            $ProductSort->setCategory($category);
            $this->entityManager->persist($ProductSort);
            $this->entityManager->flush();
        }

    }

}