<?php
// 11/05/17, 10.55
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;
use Webtek\EcommerceBundle\Entity\Product;

class ProductFileNamer implements NamerInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * ProductFileNamer constructor.
     */
    public function __construct(ContainerInterface $container)
    {

        $this->container = $container;
    }

    public function name($object, PropertyMapping $mapping)
    {


        return $this->generaNome(
            $object,
            $mapping->getUploadDestination(),
            $mapping->getFile($object)->getClientOriginalExtension(),
            $mapping->getFile($object)->getClientOriginalName()
        );


    }

    public function generaNome($object, $uploadDir, $extension, $default)
    {

        if ($this->container->getParameter('ecommerce')['rinomina_immagini']) {

            /**
             * @var $object Product
             */
            if (!$object->translate('it')->getSlug()) {
                $slug = $object->translate('it')->getSlug();
            } else {
                $slug = $this->container->get('cocur_slugify')->slugify($object->translate('it')->getTitolo());
            }
            $finder = new Finder();
            $finder->name($slug.'*');

            $finder->files()->in($uploadDir);

            $index = '';

            $i = 0;

            foreach ($finder as $k => $file) {
                $index = ++$i;
            }

            if ($index) {
                $index = '-'.$index;
            }

            $name = $slug.$index.'.'.$extension;

            $name = strtolower($name);

            return $name;

        }

        return strtolower($default);

    }

}