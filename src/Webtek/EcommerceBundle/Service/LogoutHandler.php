<?php
// 05/06/17, 11.04
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Service;


use AppBundle\Service\PathManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutHandler implements LogoutSuccessHandlerInterface
{

    /**
     * @var PathManager
     */
    private $pathManager;


    /**
     * LogoutHandler constructor.
     */
    public function __construct(PathManager $pathManager)
    {

        $this->pathManager = $pathManager;
    }

    public function onLogoutSuccess(Request $request)
    {

        $referer = $request->headers->get('referer');

        $targetPath = $this->pathManager->generateUrl('checkout', ['locale' => $request->request->get('locale')]);

        return new RedirectResponse($targetPath);

    }

}