<?php

namespace Webtek\EcommerceBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class WishlistHelper
{

    /**
     * @var EntityManager
     */
    private $em;

    /*
     * @var AuthorizationChecker
     */
    private $auth;

    private $user;

    /**
     * AttributesHelper constructor.
     */
    public function __construct(EntityManager $entityManager, AuthorizationChecker $auth, TokenStorage $token)
    {
        $this->auth = $auth;
        $this->em = $entityManager;
        $this->user = $token->getToken()->getUser();
    }

    public function checkProductsWishlist($products)
    {
        if ($this->user != 'anon.' && $this->auth->isGranted('ROLE_ANAGRAFICA')) {
            $this->em->getRepository('AnagraficaBundle:Anagrafica')->areProductsInWishlist($this->em->getRepository('AnagraficaBundle:Anagrafica')->findOneByUser($this->user), $products);
        }
    }

    public function checkProductsWishlistArray($products)
    {
        if ($this->user != 'anon.' && $this->auth->isGranted('ROLE_ANAGRAFICA')) {
            $user = $this->em->getRepository('AnagraficaBundle:Anagrafica')->findOneByUser($this->user);
            if ($user) {
                $this->em->getRepository('AnagraficaBundle:Anagrafica')->areProductsInWishlistArray($user, $products);
            }
        }
    }


}