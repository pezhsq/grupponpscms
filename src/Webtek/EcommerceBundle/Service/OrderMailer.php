<?php
/**
 * Created by PhpStorm.
 * User: gabricom
 * Date: 03/08/17
 * Time: 17.35
 */

namespace Webtek\EcommerceBundle\Service;


use AppBundle\Entity\User;
use AppBundle\Service\EmailTemplateHelper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\Translator;
use Webtek\EcommerceBundle\Entity\Order;

class OrderMailer
{


    private $entityManager;
    private $mailer;
    private $templating;
    private $translator;
    private $templateHelper;
    private $parametri;

    public function __construct(EntityManager $entityManager, $templating, Translator $translator, EmailTemplateHelper $templateHelper, $generali, $ecommerce)
    {

        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->templating = $templating;
        $this->templateHelper = $templateHelper;
        $this->parametri = [
            "generali" => $generali,
            "ecommerce" => $ecommerce
        ];
    }

    public function generateEmail(Order $Order)
    {
        $datiAdmin = $this->entityManager->getRepository("WebtekEcommerceBundle:Setup")->findAll()[0];
        $emailAdmin = $datiAdmin->getEmail();
        $datiUtente = $Order->getAnagraficaId()->getUser();
        $emailUtente = $datiUtente->getEmail();
        $mail = [
            "ordine" => $Order,
        ];

        $codice = 'ECOMMERCE_ORDINE_DETTAGLIO';

        /**
         * @var $user User
         */
        $user = $Order->getAnagraficaId()->getUser();

        $twig = $this->templating;

        $dettaglioOrdine = $twig->render(
            'public/layouts/' . $this->parametri['generali']['layout'] . '/includes/order-detail.twig',
            ['data' => ['Order' => $Order]]
        );

        $linkPagamento = '';

        if ($Order->getPaymentUrl() && !$Order->getPagato()) {
            $linkPagamento = $this->translator->trans(
                    'ecommerce.labels.paga_ora'
                ) . ': <a href="' . $Order->getPaymentUrl() . '">' . $this->translator->trans(
                    'ecommerce.labels.paga'
                ) . '</a>';
        }

        $messaggio = "";
        if ($Order->getStatus()->translate()->getMessaggioEmail()) {
            $messaggio = $Order->getStatus()->translate()->getMessaggioEmail();
        }

        $replace = [
            'CLIENTE' => $user->getNome(),
            'NUMERO_ORDINE' => $Order->getId(),
            "MESSAGGIO" => $messaggio,
            'DETTAGLIO' => $dettaglioOrdine,
            'PAGAMENTO' => $Order->getPaymentDescription(),
            'LINK_PAGAMENTO' => $linkPagamento,
            'STATO' => $Order->getStatus()->translate()->getTitolo()
        ];

        $bcc = explode(';', $this->parametri['ecommerce']['orders_ccn']);
        $bcc = array_map('trim', $bcc);
        $bcc = array_filter($bcc);

        $parametri = [
            "from" => $emailAdmin,
            "from_name" => "Ghebagas"
        ];


        $this->templateHelper->send(
            $codice,
            $emailUtente,
            $replace,
            $Order->getLocale(),
            $bcc,
            $emailAdmin,
            'simple',
            $parametri
        );
        $parametri = [
            "from" => $emailUtente,
            "from_name" => "Ghebagas"
        ];
        if ($Order->getStatus() == $this->entityManager->getRepository("WebtekEcommerceBundle:OrdersStatus")->findOneByCodice("PREPARAZIONE")) {
            $codice = 'ECOMMERCE_ORDINE_DETTAGLIO_ADMIN';
            $this->templateHelper->send(
                $codice,
                $emailAdmin,
                $replace,
                $Order->getLocale(),
                $bcc,
                $emailAdmin,
                'simple',
                $parametri
            );
        }

        // Invio Mail all'utente
    }
}