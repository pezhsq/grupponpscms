<?php
// 08/06/17, 11.17
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Service;


use AppBundle\Service\PathManager;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\RelatedResources;
use PayPal\Api\Sale;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Router;
use Webtek\EcommerceBundle\Entity\MetodiPagamento;
use Webtek\EcommerceBundle\Entity\Order;
use Webtek\EcommerceBundle\Entity\RecordOrder;

class PaymentHelper
{

    /**
     * @var $Order Order
     */
    private $Order;

    /**
     * @var $Payment MetodiPagamento
     */
    private $Payment;
    /**
     * @var TaxCalculator
     */
    private $taxCalculator;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var PathManager
     */
    private $pathManager;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var OrderHelper
     */
    private $orderHelper;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(
        TaxCalculator $taxCalculator,
        RequestStack $requestStack,
        PathManager $pathManager,
        EntityManager $entityManager,
        OrderHelper $orderHelper,
        Logger $logger
    )
    {

        $this->taxCalculator = $taxCalculator;
        $this->requestStack = $requestStack;
        $this->pathManager = $pathManager;
        $this->entityManager = $entityManager;
        $this->orderHelper = $orderHelper;
        $this->logger = $logger;
    }


    public function getPaymentUrl(Order $order, $session = false)
    {

        $this->Order = $order;

        $this->Payment = $this->Order->getPayment();

        if ($this->Payment) {
            $method = 'get' . ucfirst(strtolower($this->Payment->getCodice())) . 'PaymentUrl';
            $url = $this->$method($session);
        }
        $this->Order->setPaymentUrl($url);

        $this->entityManager->persist($this->Order);
        $this->entityManager->flush();

        return $this->Order;

    }

    private function getSedePaymentUrl($session = false)
    {
        return $this->pathManager->generate(
            'checkout_grazie',
            ['order-id' => $this->Order->getId()],
            $this->requestStack->getCurrentRequest()->getLocale(),
            true
        );
    }


    private function getPaypalPaymentUrl($session = false)
    {
        $apiContext = $this->connectPaypal();
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $records = $this->Order->getRecords();

        $items = [];

        $tax = 0;
        $total = 0;

        foreach ($records as $RecordOrdine) {
            /**
             * @var $RecordOrdine RecordOrder
             */
            $name = $RecordOrdine->getDescrizione();
            if ($RecordOrdine->getVariante()) {
                $name .= ' (' . $RecordOrdine->getVariante() . ')';
            }

            $prezzoRounded = round($RecordOrdine->getPrezzo(), 2);
            $item = new Item();
            $item->setName($name);
            $item->setCurrency('EUR');
            $item->setQuantity($RecordOrdine->getQty());

            $item->setPrice($prezzoRounded);
            $total += $prezzoRounded * $RecordOrdine->getQty();
            $items[] = $item;

        }
        $tax = $this->Order->getTotal() - $total;
        $ItemList = new ItemList();
        $ItemList->setItems($items);

        $deliveryCost = $this->taxCalculator->scorpora(
            $this->Order->getDeliveryCost(),
            $this->Order->getDeliverById()->getTax()->getAliquota(),
            2
        );

        $tax += $deliveryCost['imposta'];

        $details = new Details();

        $details->setShipping($deliveryCost['imponibile'])
            ->setTax($tax)
            ->setSubtotal($total);


        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($this->Order->getTotal() + $this->Order->getDeliveryCost())
            ->setDetails($details);


        $Transaction = new Transaction();
        $Transaction->setAmount($amount)
            ->setItemList($ItemList)
            ->setDescription('Ordine Web n.' . $this->Order->getId())
            ->setInvoiceNumber($this->Order->getId());

        $returnUrl = $this->pathManager->generateUrl(
            'checkout_verify_paypal',
            [],
            $this->requestStack->getCurrentRequest()->getLocale(),
            true
        );

        $cancelUrl = $this->pathManager->generateUrl(
            'checkout_cancel_paypal',
            [],
            $this->requestStack->getCurrentRequest()->getLocale(),
            true
        );

        $redirectUrls = new RedirectUrls();

        $redirectUrls->setReturnUrl($returnUrl);
        $redirectUrls->setCancelUrl($cancelUrl);

        $payment = new Payment();
        $payment->setIntent('sale')->setPayer($payer)->setRedirectUrls($redirectUrls)->setTransactions([$Transaction]);

        $req = clone $payment;

        $url = false;
        try {

            $payment->create($apiContext);
            $url = $payment->getApprovalLink();

        } catch (\Exception $e) {
            $this->logger->error(
                $e->getMessage(),
                ['where' => basename(__FILE__) . ':' . __LINE__]
            );
            throw new Exception($e->getMessage());

        }

        return $url;

    }


    public function paypalVerify($paypalData)
    {


        $this->getPayment('PAYPAL');

        $apiContext = $this->connectPaypal();

        $payment = Payment::get($paypalData['paymentId'], $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($paypalData['PayerID']);

        $payment = $payment->execute($execution, $apiContext);

        try {

            $transactions = $payment->getTransactions();
            /**
             * @var $transaction Transaction
             */
            $transaction = $transactions[0];

            // id dell'ordine sull'ecommerce
            $OrderId = $transaction->getInvoiceNumber();

            $this->Order = $this->entityManager->getRepository('WebtekEcommerceBundle:Order')->findOneBy(
                ['id' => $OrderId]
            );

            if ($this->Order) {

                $relatedResources = $transaction->getRelatedResources();
                /**
                 * @var $relatedResource RelatedResources
                 */
                $relatedResource = $relatedResources[0];

                /**
                 * @var $sale Sale
                 */
                $sale = $relatedResource->getSale();

                if ($sale->getState() == 'completed') {

                    $this->orderHelper->pay($this->Order, $payment->getId(), $transaction->getAmount()->getTotal());

                    return $this->Order;

                } else {

                    $this->logger->error(
                        'Vendita paypal non conclusa: {state}',
                        ['state' => $sale->getState(), 'where' => basename(__FILE__) . ':' . __LINE__]
                    );

                }

            } else {

                $this->logger->error(
                    'Ordine n. {idOrdine} non trovato',
                    ['idOrdine' => $OrderId, 'where' => basename(__FILE__) . ':' . __LINE__]
                );

                throw new Exception(sprintf('Ordine n. %s non trovato', $OrderId));

            }
        } catch (\Exception $e) {

            $this->logger->error(
                $e->getMessage(),
                ['where' => basename(__FILE__) . ':' . __LINE__]
            );

        }

        return false;

    }

    private function connectPaypal()
    {

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->Payment->getClientId(),
                $this->Payment->getClientSecret()
            )
        );

        $logLevel = "INFO";

        $mode = "live";
        if ($this->Payment->getisDebug()) {
            $logLevel = "DEBUG";
            $mode = "sandbox";
        }

        $apiContext->setConfig(
            array(
                'log.LogEnabled' => true,
                'log.FileName' => '../var/logs/PayPal.log',
                'log.LogLevel' => $logLevel,
                'mode' => $mode
            )
        );

        return $apiContext;


    }

    private function getPayment($code)
    {

        $this->Payment = $this->entityManager->getRepository('WebtekEcommerceBundle:MetodiPagamento')->findOneBy(
            ['codice' => $code]
        );

    }


}