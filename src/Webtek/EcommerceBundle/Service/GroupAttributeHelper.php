<?php

namespace Webtek\EcommerceBundle\Service;


use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Attribute;
use Webtek\EcommerceBundle\Entity\GroupAttribute;

class GroupAttributeHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * GroupAttributeHelpersHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $GroupAttributeHelpers = $this->entityManager->getRepository(
                'WebtekEcommerceBundle:GroupAttribute'
            )->findAll();
        } else {
            $GroupAttributeHelpers = $this->entityManager->getRepository(
                'WebtekEcommerceBundle:GroupAttribute'
            )->findAllNotDeleted();
        }

        $records = [];

        foreach ($GroupAttributeHelpers as $GroupAttribute) {


            /**
             * @var $GroupAttribute GroupAttribute;
             */

            $record              = [];
            $record['id']        = $GroupAttribute->getId();
            $record['titolo']    = $GroupAttribute->translate()->getTitolo();
            $record['type']      = $GroupAttribute->getType();
            $record['deleted']   = $GroupAttribute->isDeleted();
            $record['createdAt'] = $GroupAttribute->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $GroupAttribute->getUpdatedAt()->format('d/m/Y H:i:s');


            $records[] = $record;
        }

        return $records;

    }

    public function getSimpleArray()
    {

        $GroupAttributes = $this->entityManager->getRepository(
            'WebtekEcommerceBundle:GroupAttribute'
        )->findAllNotDeleted();

        $data = [];

        foreach ($GroupAttributes as $GroupAttribute) {

            /**
             * @var $GroupAttribute GroupAttribute;
             */

            $record               = [];
            $record['id']         = $GroupAttribute->getId();
            $record['type']       = $GroupAttribute->getType();
            $record['nome']       = $GroupAttribute->translate()->getTitolo();
            $record['attributes'] = [];

            $attributes = $GroupAttribute->getAttribute();

            foreach ($attributes as $Attribute) {

                /**
                 * @var $Attribute Attribute
                 */

                $attribute           = [];
                $attribute['value']  = $Attribute->getValue();
                $attribute['titolo'] = $Attribute->translate()->getTitolo();
                $attribute['id']     = $Attribute->getId();

                $record['attributes'][] = $attribute;
            }

            if ($record['attributes']) {
                $data[] = $record;
            }

        }

        return $data;

    }


}