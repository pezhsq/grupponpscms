<?php
// @author : Gabriele "gabricom" Colombera <gabricom-kun@live.it>

namespace Webtek\EcommerceBundle\Service;


class TaxCalculator
{


    public function CalcoloImposta($entity)
    {

        $imposta = $entity->getPrezzo() * ($entity->getCodiceIva()->getAliquota() / 100);
        $prezzo_tassato = $entity->getPrezzo() + $imposta;
        $entity->setImposta($imposta);
        $entity->setPrezzoTassato($prezzo_tassato);

    }


    /**
     * @param $prezzo
     * @param $aliquota
     * @param int $precision
     * @return array
     */
    public function CalcoloImpostaRaw($prezzo, $aliquota, $precision = 4)
    {

        return [
            "imposta" => $this->getImposta($prezzo, $aliquota, $precision),
            "prezzo_tassato" => $this->getPrezzoIvato($prezzo, $aliquota, $precision),
        ];
    }

    public function getPrezzoIvato($prezzo, $aliquota, $precision = 4)
    {

        $imposta = $this->getImposta($prezzo, $aliquota, $precision);
        $prezzo_ivato = round($prezzo + $imposta, $precision);
        
        return $prezzo_ivato;

    }

    public function getImposta($prezzo, $aliquota, $precision)
    {

        $imposta = round($prezzo * ($aliquota / 100), $precision);

        return $imposta;

    }

    public function scorpora($prezzo, $aliquota, $precision = 4)
    {

        $imponibile = round($prezzo / (1 + $aliquota / 100), $precision);

        $imposta = $prezzo - $imponibile;

        return ['imponibile' => $imponibile, 'imposta' => $imposta];
    }


}
