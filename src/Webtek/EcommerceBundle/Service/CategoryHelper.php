<?php
// 04/05/17, 14.56
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Service;


use AppBundle\Service\PathManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\CategoryTranslation;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CategoryHelper
{

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var EntityManager
     */
    private $em;

    private $cat;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;
    /**
     * @var PathManager
     */
    private $pathManager;
    /**
     * @var Request
     */
    private $request;


    public function __construct(
        Slugify $slugify,
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker,
        PathManager $pathManager,
        RequestStack $request
    )
    {

        $this->slugify = $slugify;

        $this->slugify->addRule('à', 'a');
        $this->slugify->addRule('è', 'e');
        $this->slugify->addRule('é', 'e');
        $this->slugify->addRule('ì', 'i');
        $this->slugify->addRule('ò', 'o');
        $this->slugify->addRule('ù', 'u');

        $this->authorizationChecker = $authorizationChecker;

        $this->em = $em;

        $this->pathManager = $pathManager;
        $this->request = $request->getCurrentRequest();
    }

    public function slugifyCategory(Category $Category, $force = false)
    {

        $translations = $Category->getTranslations();
        foreach ($translations as $trans) {
            if (!$this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') || ($this->authorizationChecker->isGranted(
                        'ROLE_EXTRA_SEO'
                    ) && !$trans->getSlug() || $force)
            ) {
                $trans->setSlug($this->slugify($trans));
            }
        }
        $this->em->persist($Category);
        $this->em->flush();

    }

    public function slugify(CategoryTranslation $CategoryTranslation, $slug = null)
    {

        $this->cat = $CategoryTranslation->getTranslatable();

        if (!$slug) {
            $slug = $this->slugify->slugify($CategoryTranslation->getNome());
        }

        $newSlug = $slug;

        $i = 1;

        while (!$this->checkSlug($newSlug, $CategoryTranslation->getLocale())) {

            $newSlug = $slug . '-' . $i;

            $i++;

        }

        return $newSlug;

    }

    private function checkSlug($slug, $locale)
    {

        $data = $this->em->getRepository('Webtek\EcommerceBundle\Entity\Category')->findBySlugPathButNotId(
            $slug,
            $locale,
            $this->cat->getMaterializedPath(),
            $this->cat->getId()
        );

        return !(bool)count($data);

    }

    /**
     * @param $slugs
     * @param string $locale
     * @return Category
     */
    public function retrieve($slugs, $locale = 'it', $onlyActive = true)
    {

        $_slugs = explode('/', $slugs);

        $path = 'prodotti/' . $slugs;

        if ($locale !== 'it') {
            $path = $locale . '/' . $path;
        }

        $path = '/' . $path;

        $slug = array_pop($_slugs);

        // vengono estratte tutte le pagine che hanno questo slug
        // attenzione, potrebbero esserci pagine con stesso slug e parent diverso,
        // quindi il risultato potrebbe essere multiplo, se lo è devo calcolarmi il path per vedere quale
        // è la pagina che davvero sto cercando.
        $Categories = $this->em->getRepository('WebtekEcommerceBundle:Category')->getBySlug(
            $slug,
            $locale
        );

        if ($Categories) {

            foreach ($Categories as $Category) {

                $CategoryUrl = $this->generaUrl($Category, $locale);
                if ($CategoryUrl == $path) {
                    return $Category;
                }

            }

        }

        return false;
    }


    /**
     * @param Category $category
     * @param string $locale
     * @param array $query
     * @param bool $absolute
     * @return string
     */
    public function generaUrl(Category $category, $locale = 'it', $query = [], $absolute = false, $routeOnly = false)
    {

        $path = '';

        $slugs = [];
        if (!$routeOnly) {
            $slugs[] = 'prodotti';
        }


        $slug = $category->translate($locale)->getSlug();

        $Path = $category->getMaterializedPath();

        $ids = explode('/', $Path);

        $ids = array_filter($ids);

        array_shift($ids);

        if ($ids) {

            $CategorieTree = $this->em->getRepository('WebtekEcommerceBundle:Category')->getByIdsOrdered($ids);

            foreach ($CategorieTree as $CategoryTree) {

                $slugs[] = $CategoryTree->translate($locale)->getSlug();

            }

        }

        $slugs[] = $category->translate($locale)->getSlug();

        $slugs = implode('/', $slugs);


        if ($locale && $locale != 'it') {
            $slugs = $locale . '/' . $slugs;
        }

        $url = '';

        if ($absolute) {
            $url .= $this->request->getSchemeAndHttpHost();
        }

        $url .= '/' . $slugs;

        if ($query) {

            $queryRows = [];

            foreach ($query as $k => $v) {

                $queryRows[] = urlencode($k) . '=' . urlencode($v);

            }

            $url .= '?' . implode('&', $queryRows);

        }

        if ($routeOnly) {
            return substr($url, 1);
        }

        return $url;

    }


}