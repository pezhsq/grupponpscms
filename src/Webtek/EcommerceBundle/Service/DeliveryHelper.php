<?php

namespace Webtek\EcommerceBundle\Service;


use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Delivery;
use Webtek\EcommerceBundle\Entity\DeliveryPrices;

class DeliveryHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * DeliverysHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Deliverys = $this->entityManager->getRepository('WebtekEcommerceBundle:Delivery')->findAll();
        } else {
            $Deliverys = $this->entityManager->getRepository('WebtekEcommerceBundle:Delivery')->findAllNotDeleted();
        }

        $records = [];

        foreach ($Deliverys as $Delivery) {


            /**
             * @var $Delivery Delivery;
             */

            $record = [];
            $record['id'] = $Delivery->getId();
            $record['nome'] = $Delivery->translate()->getNome();
            $record['deleted'] = $Delivery->isDeleted();
            $record['isEnabled'] = $Delivery->getIsEnabled();
            $record['createdAt'] = $Delivery->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Delivery->getUpdatedAt()->format('d/m/Y H:i:s');


            $records[] = $record;
        }

        return $records;

    }

    public function getPrice(IndirizzoAnagrafica $address = null, Delivery $corriere = null, $totale)
    {

        if (!$address) {
            return 0;
        }

        /**
         * @var $prices ArrayCollection
         */
        $prices = $corriere->getPrices();

        $prices = $prices->filter(
            function ($entry) use ($address, $totale) {

                /**
                 * @var $entry DeliveryPrices
                 */
                if ($entry->getTutte() && $entry->getSogliaMin() <= $totale && $entry->getSogliaMax() >= $totale) {
                    return true;
                } elseif ($entry->getNations()->contains($address->getNazione()) && $entry->getSogliaMin(
                    ) <= $totale && $entry->getSogliaMax() >= $totale
                ) {
                    return true;
                }
            }
        );

        if ($prices->count()) {
            return $prices->first()->getCost();
        }

        return 0;
    }


}