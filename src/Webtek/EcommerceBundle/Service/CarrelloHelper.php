<?php
// 27/05/17, 10.30
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Webtek\EcommerceBundle\Entity\Carrello;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\Coupon;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\RecordCarrello;
use Webtek\EcommerceBundle\Entity\Tax;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;

class CarrelloHelper
{

    /**
     * @var Session
     */
    private $session;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Carrello
     */
    private $carrello = null;
    /**
     * @var ProductHelper
     */
    private $productHelper;
    /**
     * @var ListinoHelper
     */
    private $listinoHelper;
    /**
     * @var TaxCalculator
     */
    private $taxCalculator;
    /**
     * @var CouponHelper
     */
    private $couponHelper;

    /**
     * CarrelloHelper constructor.
     */
    public function __construct(
        EntityManager $entityManager,
        Session $session,
        ProductHelper $productHelper,
        ListinoHelper $listinoHelper,
        TaxCalculator $taxCalculator,
        CouponHelper $couponHelper
    ) {

        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->productHelper = $productHelper;
        $this->listinoHelper = $listinoHelper;
        $this->taxCalculator = $taxCalculator;
        $this->couponHelper = $couponHelper;
    }

    public function getCarrello()
    {


        if (!$this->carrello) {

            $Carrello = $this->entityManager->getRepository('WebtekEcommerceBundle:Carrello')->findOneBy(
                ['id' => $this->session->get('uuid')]
            );
            if (!$Carrello) {
                $Carrello = new Carrello();
                $Carrello->setId($this->session->get('uuid'));
            }
            $this->carrello = $Carrello;
        }

        return $this->carrello;

    }

    public function setQuantity(RecordCarrello $recordCarrello, $qty)
    {

        $this->getCarrello();
        $return = false;
        if (is_numeric($qty)) {

            $Product = $recordCarrello->getProdotto();
            $VarianteProdotto = $recordCarrello->getVariante();
            if ($Product->getisAlwaysAvailable()) {
                $recordCarrello->setQty($qty);
            } else {

                $giacenza = $this->productHelper->getGiacenza($Product, $VarianteProdotto);
                if ($giacenza >= $qty) {
                    $recordCarrello->setQty($qty);
                } else {
                    $notEnough = 1;
//                    $qty = $recordCarrello->getQty() + $giacenza;
//                    $recordCarrello->setQty($qty);
                }
            }
            $this->carrello->addRecordCarrello($recordCarrello);
            $this->entityManager->persist($this->carrello);
            $this->entityManager->flush();
            $return = [];
            if (isset($notEnough)) {
                $return['notEnough'] = 1;
                $return['giacenza'] = $giacenza;
                $return['total'] = $recordCarrello->getQty();
                $return['maxAddable'] = $giacenza - $recordCarrello->getQty();
            }
            $return['qty'] = $qty;


        }
        $this->garbageCollector();

        return $return;

    }

    public function add(Product $product, VarianteProdotto $varianteProdotto = null, $qty)
    {

        $this->getCarrello();
        $return = false;
        if (is_numeric($qty)) {

            $records = $this->carrello->getRecordsCarrello()->filter(
                function ($recordCarrello) use ($product, $varianteProdotto) {

                    /**
                     * @var $recordCarrello RecordCarrello
                     */
                    if ($recordCarrello->getProdotto() == $product && $recordCarrello->getVariante(
                        ) == $varianteProdotto) {
                        return true;
                    }

                }
            );
            if (!$records->count()) {

                $recordCarrello = new RecordCarrello();
                $recordCarrello->setCarrello($this->carrello);
                $recordCarrello->setProdotto($product);
                $recordCarrello->setVariante($varianteProdotto);

            } else {
                $recordCarrello = $records->first();
            }
            $added = $qty;
            $notEnough = 0;
            if ($product->getisAlwaysAvailable()) {
                $recordCarrello->setQty($recordCarrello->getQty() + $qty);
            } else {

                $giacenza = $this->productHelper->getGiacenza($product, $varianteProdotto);
                $qtyCarrello = $recordCarrello->getQty();
                if (($giacenza - $qtyCarrello) >= $qty) {
                    $recordCarrello->setQty($recordCarrello->getQty() + $qty);
                } else {
                    $notEnough = 1;
                    $added = 0;
//                    $added = $giacenza - $qtyCarrello;
//                    if ($added < 0) $added = 0;
//                    $recordCarrello->setQty($recordCarrello->getQty() + $added);
                }
            }
            if (!$notEnough) {
                $this->carrello->addRecordCarrello($recordCarrello);
                $this->entityManager->persist($this->carrello);
                $this->entityManager->flush();
            }
            $return = [];
            if (isset($notEnough) && $notEnough != 0) {
                $return['notEnough'] = $notEnough;
            }
            if (isset($giacenza)) {
                $return['giacenza'] = $giacenza;
            }
            $return['added'] = $added;
            $return['total'] = $recordCarrello->getQty();
            $return['requested'] = $qty;
            $return['description'] = (string)$recordCarrello;


        }
        $this->garbageCollector();

        return $return;

    }

    private function garbageCollector()
    {


        if ($this->carrello && !$this->carrello->getRecordsCarrello()->count()) {
            $this->entityManager->remove($this->carrello);
            $this->entityManager->flush();
            $this->carrello = null;
        } elseif ($this->carrello) {
            foreach ($this->carrello->getRecordsCarrello()->getIterator() as $recordCarrello) {
                /**
                 * @var $recordCarrello RecordCarrello
                 */
                if (!$recordCarrello->getQty()) {
                    $this->carrello->removeRecordCarrello($recordCarrello);
                    $this->entityManager->remove($recordCarrello);
                }
                $this->entityManager->persist($this->carrello);
                $this->entityManager->flush();
            }
        }
    }

    public function getTasse(Carrello $carrello)
    {

        $records = $this->carrello->getRecordsCarrello();
        $data = [];
        foreach ($records->getIterator() as $riga) {
            /**
             * @var $riga RecordCarrello
             */
            /**
             * @var $Product Product
             */
            $Product = $riga->getProdotto();
            /**
             * @var $Variante VarianteProdotto
             */
            $Variante = $riga->getVariante();
            /**
             * @var $Tax Tax
             */
            $Tax = $Product->getTax();
            $aliquota = $Tax->getAliquota();
            if (!isset($data[$aliquota])) {
                $data[$aliquota] = [];
                $data[$aliquota]['description'] = $Tax->getDescrizione();
                $data[$aliquota]['imponibile'] = 0;
                $data[$aliquota]['imposta'] = 0;
                $data[$aliquota]['lordo'] = 0;

            }
            $data[$aliquota]['lordo'] += $this->productHelper->getPrezzo(
                    $Product,
                    $this->listinoHelper->getDefault(),
                    $Variante,
                    $carrello->getCoupon()
                ) * $riga->getQty();

        }
        foreach ($data as $aliquota => $dati) {
            $nettoEImposta = $this->taxCalculator->scorpora($dati['lordo'], $aliquota, 2);
            $data[$aliquota]['imposta'] = $nettoEImposta['imposta'];
            $data[$aliquota]['imponibile'] = $nettoEImposta['imponibile'];
        }

        return $data;
    }

    public function getImponibile(Carrello $carrello)
    {

        $datiTassazione = $this->getTasse($carrello);
        $imponibile = 0;
        foreach ($datiTassazione as $aliquota => $dati) {

            $imponibile += $dati['imponibile'];

        }

        return $imponibile;

    }

    public function getTotale(Carrello $carrello, $spese = 0, $subtotale = false)
    {


        $datiTassazione = $this->getTasse($carrello);
        $lordo = 0;
        foreach ($datiTassazione as $aliquota => $dati) {

            $lordo += $dati['lordo'];

        }
        $lordo += $spese;
        if (!$subtotale && $carrello->getCoupon() && $carrello->getCoupon()->getType() == Coupon::TYPE_EURO) {
            $lordo -= $carrello->getCoupon()->getValue();
        }

        return $lordo;

    }

    public function getQtyProductInCart(Product $Product, VarianteProdotto $varianteProdotto = null)
    {

        $carrello = $this->getCarrello();
        foreach ($carrello->getRecordsCarrello() as $RecordCarrello) {
            if ($varianteProdotto) {
                if ($RecordCarrello->getVariante() == $varianteProdotto) {
                    return $RecordCarrello->getQty();
                }
            } else {
                if ($RecordCarrello->getProdotto() == $Product) {
                    return $RecordCarrello->getQty();
                }
            }

        }
    }

    public function getNumberOfItems(Carrello $carrello, $itemsOnly = false)
    {

        $count = 0;
        foreach ($carrello->getRecordsCarrello() as $RecordCarrello) {
            /**
             * @var $RecordCarrello RecordCarrello
             */
            if ($itemsOnly) {
                $count++;
            } else {
                $count += $RecordCarrello->getQty();
            }

        }

        return $count;

    }

    public function delete(Carrello $carrello)
    {

        $this->entityManager->remove($carrello);
        $this->entityManager->flush();
    }

    public function removeCoupon(Carrello $carrello)
    {

        $carrello->setCoupon(null);
        $this->entityManager->persist($carrello);
        $this->entityManager->flush();

    }

    public function addCoupon(Carrello $carrello, Coupon $coupon)
    {

        if ($coupon->getMinimumOrder() <= $this->getTotale($carrello)) {

            if ($coupon->getIllimitato() || $this->couponHelper->getUtilizzati($coupon) < $coupon->getAvailable()) {

                $utilizzabile = null;
                if (!$coupon->getAllProducts()) {
                    $categorieCoupon = $coupon->getCategorie();
                    $categorieCouponId = [];
                    foreach ($categorieCoupon as $Categoria) {
                        /**
                         * @var $Categoria Category
                         */
                        $categorieCouponId[] = $Categoria->getId();
                    }
                } else {
                    $utilizzabile = true;
                }
                if (!$utilizzabile) {
                    foreach ($carrello->getRecordsCarrello() as $Record) {
                        /**
                         * @var $Record RecordCarrello
                         */
                        if ($this->couponHelper->isAppliable($categorieCouponId, $Record->getProdotto())) {
                            $utilizzabile = true;
                        }
                    }
                }
                if ($utilizzabile) {
                    $carrello->setCoupon($coupon);
                    $this->entityManager->persist($carrello);
                    $this->entityManager->flush();

                    return true;
                } else {
                    return 'ecommerce.labels.coupon_non_utilizzabile';
                }

            } else {
                return 'ecommerce.labels.coupon_non_valido';
            }

        } else {
            return 'ecommerce.labels.minimo_ordine';
        }

    }


}