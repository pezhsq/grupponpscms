<?php

namespace Webtek\EcommerceBundle\Service;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class BrandDirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping)
    {

        $dir = $mapping->getUriPrefix();

        $dir = $object->getId();

        return $dir;

    }
    
}