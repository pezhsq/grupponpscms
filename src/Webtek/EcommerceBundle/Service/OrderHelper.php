<?php
// 07/06/17, 13.43
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Service;

use AnagraficaBundle\Entity\Anagrafica;
use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Webtek\EcommerceBundle\Entity\Carrello;
use Webtek\EcommerceBundle\Entity\Coupon;
use Webtek\EcommerceBundle\Entity\Delivery;
use Webtek\EcommerceBundle\Entity\MetodiPagamento;
use Webtek\EcommerceBundle\Entity\Order;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\RecordCarrello;
use Webtek\EcommerceBundle\Entity\RecordOrder;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;
use Webtek\EcommerceBundle\Event\OrderStatusChangeEvent;
use Webtek\EcommerceBundle\EventSubscriber\OrderStatusSubscriber;

class OrderHelper
{

    /**
     * @var Container
     */
    private $container;


    /**
     * OrdersStatussHelper constructor.
     */
    public function __construct(Container $container)
    {

        $this->container = $container;

    }

    public function createOrder(
        Carrello $carrello,
        Anagrafica $Anagrafica,
        IndirizzoAnagrafica $Address,
        Delivery $Delivery,
        MetodiPagamento $Payment
    ) {

        $carrelloHelper = $this->container->get('app.webtek_ecommerce.services.carrello_helper');
        $Order = new Order();
        $Order->setBilling((string)$Anagrafica);
        $Order->setAnagraficaId($Anagrafica);
        $Order->setStatus($Payment->getStatoDefault());
        $Order->setDelivery((string)$Address);
        $Order->setPayment($Payment);
        $Order->setPaymentDescription((string)$Payment);
        $Order->setDeliverBy((string)$Delivery);
        $Order->setDeliverById($Delivery);
        $Order->setDeliveryTax($Delivery->getTax()->getAliquota());
        $Order->setTotal($carrelloHelper->getTotale($carrello));
        $Order->setLocale($this->container->get('app.localization_helper')->getLocale());
        $Order->setCoupon($carrello->getCoupon());
        $Order->setCouponDescription((string)$carrello->getCoupon());
        $Order->setDeliveryCost(
            $this->container->get('app.webtek_ecommerce.services.delivery_helper')->getPrice(
                $Address,
                $Delivery,
                $carrelloHelper->getTotale($carrello, 0, true)
            )
        );
        if ($carrello->getCoupon() && $carrello->getCoupon()->getType() == Coupon::TYPE_EURO) {
            $Order->setCouponScontoEuro($carrello->getCoupon()->getValue());
        } else {
            $Order->setCouponScontoEuro(0);
        }
        $records = new ArrayCollection();
        foreach ($carrello->getRecordsCarrello()->getIterator() as $RecordCarrello) {
            /**
             * @var $RecordCarrello RecordCarrello
             */
            /**
             * @var $Product Product
             */
            $Product = $RecordCarrello->getProdotto();
            $OrderRecord = new RecordOrder();
            $OrderRecord->setQty($RecordCarrello->getQty());
            $code = $Product->getCodice();
            /**
             * @var $Variante VarianteProdotto
             */
            $Variante = $RecordCarrello->getVariante();
            if ($Variante) {
                if ($Variante->getCodice()) {
                    $code .= ' - '.$Variante->getCodice();
                }
            }
            $OrderRecord->setCode($code);
            $OrderRecord->setDescrizione((string)$Product);
            $OrderRecord->setProduct($Product);
            $OrderRecord->setTax($Product->getTax()->getAliquota());
            $OrderRecord->setVariante($RecordCarrello->getVariante());
            $OrderRecord->setDescrizioneVariante((string)$RecordCarrello->getVariante());
            $OrderRecord->setPrezzo(
                $this->container->get('app.webtek_ecommerce.services.product_helper')->getPrezzoImponibile(
                    $Product,
                    $this->container->get('app.webtek_ecommerce.services.listino_helper')->getDefault(),
                    $RecordCarrello->getVariante(),
                    $carrello->getCoupon()
                )
            );
            $OrderRecord->setOrder($Order);
            $records->add($OrderRecord);

        }
        $Order->setRecords($records);
        $entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        $entityManager->persist($Order);
        $entityManager->flush();
        $carrelloHelper->delete($carrello);

        return $Order;

    }


    /* INVIA MAIL DI STATO DELL'ORDINE */
    public function callStatusChangeEvent(Order $Order)
    {

        $event = new OrderStatusChangeEvent($Order);
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber($this->container->get("app.event_listners.order_status_subscriber"));
        $dispatcher->dispatch(OrderStatusChangeEvent::NAME, $event);
    }

    public function pay(Order $order, $transactionId = false, $payedAmount = false)
    {

        $entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        if ($payedAmount === false) {
            $payedAmount = $order->getTotal() + $order->getDeliveryCost();
        }
        $order->setTransactionId($transactionId);
        $order->setPayedAmount($payedAmount);
        $order->setPaymentDate(new \DateTime());
        $order->setStatus(
            $entityManager->getRepository('WebtekEcommerceBundle:OrdersStatus')->findOneBy(
                ['codice' => 'PREPARAZIONE']
            )
        );
        $entityManager->persist($order);
        $entityManager->flush();
        $carrello_helper = $this->container->get('app.webtek_ecommerce.services.carrello_helper');
        $carrello_helper->delete($carrello_helper->getCarrello());
        $this->callStatusChangeEvent($order);

    }

    public function getTasse(Order $Order)
    {

        $records = $Order->getRecords();
        $data = [];
        foreach ($records->getIterator() as $riga) {
            /**
             * @var $riga RecordOrder
             */
            /**
             * @var $Product Product
             */
            $Product = $riga->getProduct();
            $aliquota = $riga->getTax();
            if (!isset($data[$aliquota])) {
                $data[$aliquota] = [];
                $data[$aliquota]['description'] = 'IVA';
                $data[$aliquota]['imponibile'] = 0;
                $data[$aliquota]['imposta'] = 0;
                $data[$aliquota]['lordo'] = 0;

            }
            $data[$aliquota]['imponibile'] += $riga->getPrezzo() * $riga->getQty();

        }
        foreach ($data as $aliquota => $dati) {
            $ivato = $this->container->get('app.webtek_ecommerce.services.tax_calculator')->getPrezzoIvato(
                $dati['imponibile'],
                $aliquota,
                2
            );
            $data[$aliquota]['imposta'] = $ivato - $data[$aliquota]['imponibile'];
            $data[$aliquota]['lordo'] = $ivato;
        }

        return $data;
    }

    public function getImponibile(Order $Order)
    {

        $records = $Order->getRecords();
        $imponibile = 0;
        foreach ($records->getIterator() as $riga) {

            $imponibile += $riga->getPrezzo() * $riga->getQty();

        }

        return $imponibile;

    }

    private function OrdersFormatter($Orders)
    {

        $localizationHelper = $this->container->get('app.localization_helper');
        $records = [];
        foreach ($Orders as $Order) {

            /**
             * @var $Order Order;
             */
            $record = [];
            $record['id'] = $Order->getId();
            $record['billing'] = nl2br($Order->getBilling());
            $record['delivery'] = nl2br($Order->getDelivery());
            $record['coupon'] = ($Order->getCouponDescription()) ? (string)$Order->getCouponDescription() : 'N.A.';
            $record['totale'] = $localizationHelper->formatCurrency(
                $Order->getGranTotale(),
                'EUR'
            );
            $record['payment'] = $Order->getPaymentDescription();
            $record['deliverBy'] = nl2br($Order->getDeliverBy());
            $record['status'] = $Order->getStatus()->translate($localizationHelper->getLocale())->getTitolo();
            $record['pagato'] = $Order->getGranTotale() == $Order->getPayedAmount();
            $record['data_pagamento'] = false;
            $record['transactionId'] = false;
            if ($Order->getPaymentDate()) {
                $record['data_pagamento'] = $Order->getPaymentDate()->format('d/m/Y H:i:s');
                $record['transactionId'] = $Order->getTransactionId();
            }
            $record['createdAt'] = $Order->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Order->getUpdatedAt()->format('d/m/Y H:i:s');
            $records[] = $record;
        }

        return $records;
    }

    public function getListofAnagrafica(Anagrafica $Anagrafica)
    {

        $entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        $Orders = $entityManager->getRepository('WebtekEcommerceBundle:Order')->findByAnagraficaId(
            $Anagrafica->getId()
        );

        return $this->OrdersFormatter($Orders);
    }

    /**
     * Ritorna l'elenco degli elementi in db in un array formattato per datatables
     * @return array
     */
    public function getList()
    {

        $entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        $localizationHelper = $this->container->get('app.localization_helper');
        $Orders = $entityManager->getRepository('WebtekEcommerceBundle:Order')->findAll();

        return $this->OrdersFormatter($Orders);

    }

    public function getStatisticheAnnue()
    {

        $repo = $this->container->get('doctrine.orm.default_entity_manager')->getRepository(
            'WebtekEcommerceBundle:Order'
        );
        $Ordini = $repo->findBy([], ['createdAt' => 'ASC']);
        $totali = [];
        foreach ($Ordini as $Order) {
            /**
             * @var $Order Order
             */
            $anno = $Order->getCreatedAt()->format('Y');
            if (!isset($totali[$anno])) {
                $totali[$anno] = [];
                $totali[$anno]['effettuati'] = 0;
                $totali[$anno]['pagati'] = 0;
            }
            $totali[$anno]['effettuati'] += $Order->getTotal();
            if ($Order->getPagato()) {
                $totali[$anno]['pagati'] += $Order->getTotal();
            }
        }
        foreach ($totali as $anno => $totaliAnno) {
            $totali[$anno]['effettuati'] = round($totaliAnno['effettuati'], 0);
            $totali[$anno]['pagati'] = round($totaliAnno['pagati'], 0);
        }

        return $totali;

    }

    public function getStatisticheMensili()
    {

        $repo = $this->container->get('doctrine.orm.default_entity_manager')->getRepository(
            'WebtekEcommerceBundle:Order'
        );
        $Ordini = $repo->findBy([], ['createdAt' => 'ASC']);
        $totali = [];
        foreach ($Ordini as $Order) {
            /**
             * @var $Order Order
             */
            $anno = $Order->getCreatedAt()->format('Y');
            $mese = $Order->getCreatedAt()->format('M');
            $key = $mese.' '.$anno;
            if (!isset($totali[$key])) {
                $totali[$key] = [];
                $totali[$key]['effettuati'] = 0;
                $totali[$key]['pagati'] = 0;
            }
            $totali[$key]['effettuati'] += $Order->getTotal();
            if ($Order->getPagato()) {
                $totali[$key]['pagati'] += $Order->getTotal();
            }
        }
        foreach ($totali as $key => $totaliAnno) {
            $totali[$key]['effettuati'] = round($totaliAnno['effettuati'], 0);
            $totali[$key]['pagati'] = round($totaliAnno['pagati'], 0);
        }

        return $totali;

    }

}