<?php

namespace Webtek\EcommerceBundle\Service;


use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Feature;
use Webtek\EcommerceBundle\Entity\GroupFeature;

class GroupFeatureHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * GroupFeaturesHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $GroupFeatures = $this->entityManager->getRepository('WebtekEcommerceBundle:GroupFeature')->findAll();
        } else {
            $GroupFeatures = $this->entityManager->getRepository(
                'WebtekEcommerceBundle:GroupFeature'
            )->findAllNotDeleted();
        }

        $records = [];

        foreach ($GroupFeatures as $GroupFeature) {


            /**
             * @var $GroupFeature GroupFeature;
             */

            $record              = [];
            $record['id']        = $GroupFeature->getId();
            $record['titolo']    = $GroupFeature->translate()->getTitolo();
            $record['chiave']    = $GroupFeature->getChiave();
            $record['deleted']   = $GroupFeature->isDeleted();
            $record['createdAt'] = $GroupFeature->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $GroupFeature->getUpdatedAt()->format('d/m/Y H:i:s');


            $records[] = $record;
        }

        return $records;

    }

    public function getSimpleArray()
    {

        $GroupFeatures = $this->entityManager->getRepository(
            'WebtekEcommerceBundle:GroupFeature'
        )->findAllNotDeleted();


        $data = [];

        foreach ($GroupFeatures as $groupFeature) {

            $data[$groupFeature->translate()->getTitolo()] = $groupFeature->getId();

        }

        return $data;

    }


}