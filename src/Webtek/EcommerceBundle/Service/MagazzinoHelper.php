<?php
// 02/10/17, 11.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Service;

use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;
use Webtek\EcommerceBundle\Form\ProductFormHelper;

class MagazzinoHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var ProductHelper
     */
    private $productHelper;

    /**
     * ListinosHelper constructor.
     */
    public function __construct(EntityManager $entityManager, ProductHelper $productHelper)
    {

        $this->entityManager = $entityManager;
        $this->productHelper = $productHelper;
    }

    public function getList($deleted = false)
    {

        $Products = $this->entityManager->getRepository('WebtekEcommerceBundle:Product')->findAllNotDeleted();
        $records = [];
        foreach ($Products as $Product) {

            /**
             * @var $Product Product
             */
            $Varianti = $Product->getVarianti();
            if ($Varianti->count()) {

                foreach ($Varianti as $Variante) {

                    /**
                     * @var $Variante VarianteProdotto
                     */
                    $record = [];
                    $record['id'] = $Variante->getId();
                    $options = [];
                    $options['size'] = ['w' => 100, 'h' => 100];
                    $record['immagine'] = false;
                    if ($Product->getListImgFileName()) {
                        $img = $this->productHelper->getListImg($Product, 'src', $options);
                        $record['immagine'] = $img;
                    }
                    $record['codice'] = $Product->getCodice();
                    $record['titolo'] = (string)$Product.' ('.(string)$Variante.')';
                    $record['type'] = 'v';
                    $record['giacenza'] = $Variante->getGiacenza();
                    $records[] = $record;

                }

            } else {

                $record = [];
                $record['id'] = $Product->getId();
                $options = [];
                $options['size'] = ['w' => 100, 'h' => 100];
                $record['immagine'] = false;
                if ($Product->getListImgFileName()) {
                    $img = $this->productHelper->getListImg($Product, 'src', $options);
                    $record['immagine'] = $img;
                }
                $record['codice'] = $Product->getCodice();
                $record['titolo'] = (string)$Product;
                $record['type'] = 'p';
                $record['giacenza'] = $Product->getGiacenza();
                $records[] = $record;

            }
        }

        return $records;

    }


}