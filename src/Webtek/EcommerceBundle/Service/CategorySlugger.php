<?php
// 09/01/17, 10.09
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Service;


use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\CategoryTranslation;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Tree\Fixture\Closure\News;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CategorySlugger
{

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var EntityManager
     */
    private $em;

    private $cat;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(
        Slugify $slugify,
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker
    ) {

        $this->slugify = $slugify;
        $this->authorizationChecker = $authorizationChecker;
        $this->slugify->addRule('à', 'a');
        $this->slugify->addRule('è', 'e');
        $this->slugify->addRule('é', 'e');
        $this->slugify->addRule('ì', 'i');
        $this->slugify->addRule('ò', 'o');
        $this->slugify->addRule('ù', 'u');

        $this->em = $em;
    }


    public function slugifyCategory(Category $Category, $force = false)
    {

        $translations = $Category->getTranslations();
        foreach ($translations as $trans) {
            if (!$this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') || ($this->authorizationChecker->isGranted(
                        'ROLE_EXTRA_SEO'
                    ) && !$trans->getSlug() || $force)
            ) {
                $trans->setSlug($this->slugify($trans));
            }
        }
        $this->em->persist($Category);
        $this->em->flush();

    }

    public function slugify(CategoryTranslation $CategoryTranslation, $slug = null)
    {

        $this->cat = $CategoryTranslation->getTranslatable();

        if (!$slug) {
            $slug = $this->slugify->slugify($CategoryTranslation->getNome());
        }

        $newSlug = $slug;

        $i = 1;

        while (!$this->checkSlug($newSlug, $CategoryTranslation->getLocale())) {

            $newSlug = $slug.'-'.$i;

            $i++;

        }

        return $newSlug;


    }


    private function checkSlug($slug, $locale)
    {

        $data = $this->em->getRepository('Webtek\EcommerceBundle\Entity\Category')->findBySlugPathButNotId(
            $slug,
            $locale,
            $this->cat->getMaterializedPath(),
            $this->cat->getId()
        );

        return !(bool)count($data);


    }


}
