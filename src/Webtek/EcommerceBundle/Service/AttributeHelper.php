<?php

namespace Webtek\EcommerceBundle\Service;


use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\Attribute;

class AttributeHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * AttributesHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Attributes = $this->entityManager->getRepository('WebtekEcommerceBundle:Attribute')->findAll();
        } else {
            $Attributes = $this->entityManager->getRepository('WebtekEcommerceBundle:Attribute')->findAllNotDeleted();
        }

        $records = [];

        foreach ($Attributes as $Attribute) {


            /**
             * @var $Attribute Attribute;
             */

            $record              = [];
            $record['id']        = $Attribute->getId();
            $record['titolo']    = $Attribute->translate()->getTitolo();
            $record['gruppo']    = (string)$Attribute->getAttributeGroup();
            $record['value']     = $Attribute->getValue();
            $record['deleted']   = $Attribute->isDeleted();
            $record['createdAt'] = $Attribute->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Attribute->getUpdatedAt()->format('d/m/Y H:i:s');


            $records[] = $record;
        }

        return $records;

    }


}