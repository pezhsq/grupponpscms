<?php
// 27/05/17, 10.10
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Controller\Front;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Webtek\EcommerceBundle\Entity\Carrello;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\RecordCarrello;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;
use Webtek\EcommerceBundle\Event\CarrelloAddEvent;

class CarrelloController extends Controller
{

    /**
     * @Route("/carrello/svuota", defaults={"_locale"="it"}, name="carrello_svuota")
     */
    public function carrelloSvuota(Request $request)
    {

        $CarrelloHelper = $this->get('app.webtek_ecommerce.services.carrello_helper');
        $CarrelloHelper->delete($CarrelloHelper->getCarrello());

        return new JsonResponse(
            ["success" => 1]
        );
    }

    /**
     * @Route("/carrello/remove-coupon", defaults={"_locale"="it"}, name="carrello_coupon_remove")
     */
    public function carrelloRemoveCoupon(Request $request)
    {

        $CarrelloHelper = $this->get('app.webtek_ecommerce.services.carrello_helper');
        $CarrelloHelper->removeCoupon($CarrelloHelper->getCarrello());
        $return = [];
        $return['result'] = true;

        return new JsonResponse($return);

    }

    /**
     * @Route("/carrello/add-coupon", defaults={"_locale"="it"}, name="carrello_coupon")
     */
    public function carrelloAddCoupon(Request $request)
    {

        $errors = [];
        $result = true;
        if ($request->request->get('coupon')) {

            $em = $this->getDoctrine();
            $Coupon = $em->getRepository('WebtekEcommerceBundle:Coupon')->findOneBy(
                ['code' => $request->request->get('coupon')]
            );
            if ($Coupon && !$Coupon->isDeleted() && $Coupon->getIsEnabled()) {

                $CarrelloHelper = $this->get('app.webtek_ecommerce.services.carrello_helper');
                $addResult = $CarrelloHelper->addCoupon($CarrelloHelper->getCarrello(), $Coupon);
                $result = true;
                if ($addResult !== true) {
                    $result = false;
                    $errors[] = sprintf(
                        $this->get('translator')->trans($addResult, [], 'public'),
                        $Coupon->getMinimumOrder()
                    );
                }

            } else {

                $result = false;
                $errors[] = $this->get('translator')->trans('ecommerce.labels.coupon_inesistente', [], 'public');

            }

        } else {
            $result = false;
            $errors[] = $this->get('translator')->trans('ecommerce.labels.coupon_non_valido', [], 'public');
        }
        $return = [];
        $return['result'] = $result;
        $return['errors'] = $errors;

        return new JsonResponse($return);


    }

    /**
     * @Route("/carrello/set-qty", defaults={"_locale"="it"}, name="carrello_set_qty")
     */
    public function setQuantity(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        if ($request->request->get('recordCarrelloId')) {

            $RecordCarrello = $em->getRepository('WebtekEcommerceBundle:RecordCarrello')->findOneBy(
                ['id' => $request->request->get('recordCarrelloId')]
            );
            $CarrelloHelper = $this->get('app.webtek_ecommerce.services.carrello_helper');
            $return['result'] = true;
            $return['data'] = $CarrelloHelper->setQuantity($RecordCarrello, $request->request->get('qty'));;

        } else {
            $return = [];
            $return['result'] = false;
            $return['errors'] = ['bad_params'];
        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/carrello/add", defaults={"_locale"="it"}, name="carrello_addd")
     */
    public function add(Request $request)
    {

        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $productHelper = $this->get('app.webtek_ecommerce.services.product_helper');
        $pathManager = $this->get('app.path_manager');
        $return = [];
        if ($request->request->get('prodotto') && is_numeric($request->request->get('prodotto'))) {

            $Product = $em->getRepository('WebtekEcommerceBundle:Product')->findOneBy(
                ['id' => $request->request->get('prodotto')]
            );
            if ($Product && !$Product->getDeletedAt() && $Product->getIsEnabled()) {

                $VarianteProdotto = null;
                if ($this->getParameter('ecommerce')['manage_attributes'] && $request->request->get('attributi')) {

                    $VarianteProdotto = $productHelper->getVariantByAttributes(
                        $Product,
                        $request->request->get('attributi')
                    );

                }
                $isAvailable = $productHelper->isAvailable($Product, $VarianteProdotto);
                if ($isAvailable) {

                    $CarrelloHelper = $this->get('app.webtek_ecommerce.services.carrello_helper');
                    $data = $CarrelloHelper->add($Product, $VarianteProdotto, $request->request->get('qty'));
                    $event = new CarrelloAddEvent($Product, $VarianteProdotto);
                    $this->container->get('event_dispatcher')->dispatch(CarrelloAddEvent::NAME, $event);
                    $return['result'] = 1;
                    $return['data'] = $data;
                    $return['routeCarrello'] = $pathManager->generate('carrello_list');

                } else {

                    $return['result'] = 0;
                    $return['errors'] = [$translator->trans('ecommerce.labels.prodotto_non_disponibile')];

                }

            } else {
                $return['result'] = 0;
                $return['errors'] = [$translator->trans('ecommerce.labels.prodotto_non_trovato')];
            }


        } else {
            $return['result'] = 0;
            $return['errors'] = ['bad params'];
        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/carrello", defaults={"_locale"="it"}, name="carrello_list_it")
     * @Route("/{_locale}/carrello", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="carrello_list")
     */
    public function viewCarrello(Request $request)
    {

        $CarrelloHelper = $this->get('app.webtek_ecommerce.services.carrello_helper');
        $CarrelloHelper->getCarrello();
        $Languages = $this->get('app.languages');
        $AdditionalData = [];
        $AdditionalData['Entity'] = $CarrelloHelper->getCarrello();
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];
        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);
        $pathManager = $this->get('app.path_manager');
        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
            $META['alternate'][$key] = $pathManager->generateUrl(
                'carrello_list',
                [],
                $sigla
            );
        }
        $AdditionalData['META'] = $META;
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');
        $twigs = $TemplateLoader->getTwigs(
            'carrello',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );
        $META = $MetaManager->merge($twigs, $META);
        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];
            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'carrello',
                    $META,
                    $metaVars
                );
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'carrello', $META, $metaVars);
            }
        }

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }

    /**
     * @Route("/carrello/json", defaults={"_locale"="it"}, name="carrello_json")
     */
    public function carrelloAjax(Request $request)
    {

        $CarrelloHelper = $this->get('app.webtek_ecommerce.services.carrello_helper');
        $ProductHelper = $this->get('app.webtek_ecommerce.services.product_helper');
        $ListinoHelper = $this->get('app.webtek_ecommerce.services.listino_helper');
        $LanguageHelper = $this->get('app.languages');
        $liipImagineController = $this->get('liip_imagine.controller');
        $liipCacheManager = $this->get('liip_imagine.cache.manager');
        $translator = $this->get('translator');
        $numberFormatter = new \NumberFormatter($LanguageHelper->getLanguageLocale(), \NumberFormatter::CURRENCY);
        $Carrello = $CarrelloHelper->getCarrello();
        $data = [];
        $data['recordsCarrello'] = [];
        $localeConv = localeconv();
        foreach ($Carrello->getRecordsCarrello()->getIterator() as $recordCarrello) {
            /**
             * @var $recordCarrello RecordCarrello
             */
            /**
             * @var $Product Product
             */
            $Product = $recordCarrello->getProdotto();
            /**
             * @var $VarianteProdotto VarianteProdotto
             */
            $VarianteProdotto = $recordCarrello->getVariante();
            $prezzo = $ProductHelper->getPrezzo($Product, $ListinoHelper->getDefault(), $VarianteProdotto);
            $riga = [];
            $riga['id'] = $recordCarrello->getId();
            $riga['listImgAlt'] = $Product->getListImgAlt();
            $riga['brand'] = false;
            if ($this->getParameter('ecommerce')['manage_brands']) {
                $riga['brand'] = (string)$Product->getBrand();
            }
            $riga['prodotto'] = (string)$Product;
            $riga['variante'] = (string)$VarianteProdotto;
            $riga['prezzo'] = $numberFormatter->format($prezzo);
            $riga['qty'] = $recordCarrello->getQty();
            $riga['subtotale'] = $numberFormatter->format($riga['qty'] * $prezzo);
            $riga['thumbnail'] = false;
            if ($Product->getListImgFileName()) {

                $path = $Product->getUploadDir().$Product->getListImgFileName();
                $response = $liipImagineController->filterAction(
                    new Request(),
                    $path,
                    'product_mini'
                );
                $riga['thumbnail'] = $liipCacheManager->getBrowserPath(
                    $path,
                    'product_mini'
                );
            }
            $data['recordsCarrello'][] = $riga;

        }
        $data['imponibile'] = $numberFormatter->format($CarrelloHelper->getImponibile($Carrello));
        $data['tasse'] = $CarrelloHelper->getTasse($Carrello);
        foreach ($data['tasse'] as $aliquota => $datiTasse) {
            $data['tasse'][$aliquota]['imponibile'] = $numberFormatter->format($data['tasse'][$aliquota]['imponibile']);
            $data['tasse'][$aliquota]['imposta'] = $numberFormatter->format($data['tasse'][$aliquota]['imposta']);
            $data['tasse'][$aliquota]['lordo'] = $numberFormatter->format($data['tasse'][$aliquota]['lordo']);
        }
        $data['totale'] = $numberFormatter->format($CarrelloHelper->getTotale($Carrello));
        $data['lang'] = [];
        $data['lang']['imponibile'] = $translator->trans('ecommerce.labels.imponibile', [], 'public');
        $data['lang']['totale'] = $translator->trans('ecommerce.labels.totale', [], 'public');
        $data['result'] = true;

        return new JsonResponse($data);

    }


}