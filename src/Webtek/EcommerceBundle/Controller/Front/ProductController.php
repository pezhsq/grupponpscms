<?php
// 25/05/17, 16.15
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;

class ProductController extends Controller
{


    /**
     * @Route("/productssearch-json/{page}"))
     */
    public function infiniteLoadingAction(Request $request, $page)
    {
        $term = $request->get("term");
        $brand = $request->get("brand");
        $category = $request->get("category");
        $em = $this->getDoctrine()->getManager();

        $data = [];

        $NewsHeader = [];

        $next = false;
        $prev = false;

        $order = $request->get("orderby");
        $dir = $request->get("dir");

        $products = $em->getRepository("WebtekEcommerceBundle:Product")->search($request->get("term"), [], $request->getLocale(), $brand, $category, $order, $dir);

        $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);

        $paginator = $this->container->get('knp_paginator');
        $pagination = $paginator->paginate(
            $products,
            $page,
            $this->getParameter('generali')['elementi_pagina_paginatore']
        );

        $data = [];


        $data = $this->get("app.webtek_ecommerce.services.product_helper")->productsEntityParser($pagination, $request);

        $return = [];
        $return['result'] = true;
        $return['data'] = $data;
        if (isset($page)) {
            $return['page'] = $page;
        }

        return new JsonResponse($return);


    }

    /**
     * @Route("/prodotto/api/get-variant-data/{prodotto}", defaults={"_locale"="it"}, name="prodotto_price_it")
     */
    public function getPriceAndAvailability(Request $request, Product $prodotto)
    {

        $productHelper = $this->get('app.webtek_ecommerce.services.product_helper');
        $listinoHelper = $this->get('app.webtek_ecommerce.services.listino_helper');
        $translator = $this->get('translator');

        $return = [];
        if (is_array($request->request->get('attributes'))) {

            $attributi = $request->request->get('attributes');
            $attributi = array_filter($attributi, "is_numeric");


            if ($attributi) {

                /**
                 * @var $Variante VarianteProdotto
                 */
                $Variante = $productHelper->getVariantByAttributes($prodotto, $attributi);

                if ($Variante) {
                    $return['result'] = 1;
                    $return['prezzo'] = $productHelper->getPrezzo(
                        $prodotto,
                        $listinoHelper->getDefault(),
                        $Variante
                    );
                    $return['available'] = $productHelper->isVarianteAvailable($Variante);
                    $return['firstPhoto'] = $productHelper->getFirstPhoto($Variante);

                } else {

                    $return['result'] = 1;
                    $return['warning'] = $translator->trans('product.labels.combinazione_non_disponibile');
                    $return['available'] = false;
                    $return['prezzo'] = 'N.A.';
                    $return['firstPhoto'] = false;

                }

                return new JsonResponse($return);
            }


        }

        $return['result'] = 0;
        $return['errors'] = ['bad params'];

        return new JsonResponse($return);

    }

    /**
     * @Route("/prodotto/{slug}", defaults={"_locale"="it"}, name="prodotto_ecommerce_it")
     * @Route("/{_locale}/prodotto/{slug}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="prodotto_ecommerce")
     */
    public function readAction(Request $request, $slug, $_locale)
    {

        $em = $this->getDoctrine()->getManager();
        $productHelper = $this->get('app.webtek_ecommerce.services.product_helper');
        $categoryHelper = $this->get('app.webtek_ecommerce.services.category_helper');
        $Languages = $this->get('app.languages');
        $pathManager = $this->get('app.path_manager');
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');


        if ($slug) {

            /**
             * @var $Product Product
             */
            $Product = $em->getRepository('WebtekEcommerceBundle:Product')->findBySlug(
                $slug,
                $_locale
            );

            if ($Product) {

                if ($Product->getIsEnabled()) {

                    $AdditionalData = [];
                    $AdditionalData['Entity'] = $Product;
                    $AdditionalData['langs'] = $Languages->getActivePublicLanguages();

                    $META = [];
                    $META['title'] = $Product->translate($request->getLocale())->getMetaTitle();
                    $META['description'] = $Product->translate($request->getLocale())->getMetaDescription();
                    $META['alternate'] = [];

                    $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
                    $linguePreferite = explode(',', $linguePreferite);

                    foreach ($AdditionalData['langs'] as $sigla => $estesa) {
                        $key = $sigla;
                        if ($key == $linguePreferite[0]) {
                            $key = 'x-default';
                        }

                        $META['alternate'][$key] = $pathManager->generateUrl(
                            'prodotto_ecommerce',
                            ['slug' => $Product->translate($sigla)->getSlug()],
                            $sigla
                        );
                    }

                    $AdditionalData['META'] = $META;

                    $twigs = $TemplateLoader->getTwigs(
                        'prodotto',
                        $Languages->getActivePublicLanguages(),
                        $AdditionalData
                    );

                    $META = $MetaManager->merge($twigs, $META);
                    if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

                        $metaVars = ['{NOME}' => $Product->translate()->getTitolo()];

                        if (!isset($META['description']) || !$META['description']) {

                            $META = $MetaManager->manageDefaults(
                                'description',
                                'prodotti-ecommerce',
                                $META,
                                $metaVars
                            );
                        }
                        if (!isset($META['title']) || !$META['title']) {
                            $META = $MetaManager->manageDefaults('title', 'prodotti-ecommerce', $META, $metaVars);
                        }
                    }

                    $META = array_merge($META, $this->get('app.open_graph')->generateOpenGraphData($Product));


                    $img = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . $this->get("assets.packages")->getUrl($productHelper->getImmaginePrincipale($Product));


                    $META['social']['og']['description'] = $META['description'];
                    $META['social']['twitter']['description'] = $META['description'];
                    $META['social']['og']['title'] = $META['title'];
                    $META['social']['og']['type'] = 'Product';
                    $META['social']['og']['image'] = $img;
                    $META['social']['twitter']['image'] = $img;
                    //TODO PERMETTERE INSERIMENTO DI PIU' IMAGE
                    $disponibile = 0;
                    if ($productHelper->isAvailable($Product)) {
                        $disponibile = "http://schema.org/InStock";
                    } else {
                        $disponibile = "http://schema.org/OutOfStock";
                    }

                    $organization = $em->getRepository("WebtekEcommerceBundle:Setup")->findAll()[0]->getRagioneSociale();

                    $microData = [];
                    $microData['@context'] = "http://schema.org/";
                    $microData['@type'] = 'Product';
                    $microData['name'] = $Product->translate($request->getLocale())->getTitolo();
                    $microData['image'] = $img;
                    $microData['description'] = $Product->translate($request->getLocale())->getTesto();

                    if ($Product->getBrand()) {
                        $microData['brand'] = [
                            "@type" => "Thing",
                            "name" => $Product->getBrand()->translate($request->getLocale())->getTitolo()
                        ];
                    }

                    $microData['offers'] = [
                        "@type" => "Offer",
                        "priceCurrency" => "EUR",
                        "price" => $productHelper->getPrezzo($Product, $this->get("app.webtek_ecommerce.services.listino_helper")->getDefault()),
                        "itemCondition" => "http://schema.org/NewCondition",
                        "availability" => $disponibile,
                        "url" => $request->getUri(),
                        "seller" => [
                            "@type" => "Organization",
                            "name" => $organization
                        ]
                    ];
                    $microData = json_encode($microData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);


                    return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META, "microData" => $microData]);

                } elseif ($Product->getTypeOfRedirect() != '404') {

                    $redirectUrl = false;

                    switch ($Product->getTypeOfRedirect()) {

                        case '301P':
                        case '302P':

                            $RedirectObj = $em->getRepository(
                                'WebtekEcommerceBundle:Product'
                            )->findOneBy(['id' => $Product->getTargetId()]);

                            if ($RedirectObj) {
                                $redirectUrl = $this->generateUrl(
                                    'prodotto_ecommerce',
                                    ['slug' => $RedirectObj->translate()->getSlug()]
                                );
                            }

                            break;
                        case '301C':
                        case '302C':

                            $RedirectObj = $em->getRepository(
                                'WebtekEcommerceBundle:Category'
                            )->findOneBy(['id' => $Product->getTargetId()]);

                            $redirectUrl = $categoryHelper->generaUrl($RedirectObj, $_locale);

                            break;
                    }

                    if ($redirectUrl) {

                        return $this->redirect($redirectUrl, substr($Product->getTypeOfRedirect(), 0, 3));

                    }


                }

            }

        }

        throw new NotFoundHttpException("Pagina non trovata");


    }

}