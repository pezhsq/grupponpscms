<?php
// 03/03/17, 11.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Controller\Front;


use AppBundle\Entity\Commento;
use AppBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Recensione;
use Webtek\EcommerceBundle\Form\RecensioneForm;
use Webtek\EcommerceBundle\Entity\Product;

class RecensioneController extends Controller
{

    /**
     * @Route("/recensione_prodotto/{prodotto}", defaults={"_locale"="it"}, name="recensione_prodotto_ecommerce_it")
     * @Route("/{_locale}/recensione_prodotto/{prodotto}", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="recensione_prodotto_ecommerce")
     */
    public function readAction(Request $request, Product $prodotto, $_locale)
    {

        $em = $this->getDoctrine()->getManager();
        $productHelper = $this->get('app.webtek_ecommerce.services.product_helper');
        $Languages = $this->get('app.languages');
        $pathManager = $this->get('app.path_manager');
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');
        $Product = $prodotto;

        if ($Product) {

            $AdditionalData = [];
            $AdditionalData['Entity'] = $Product;
            $AdditionalData['langs'] = $Languages->getActivePublicLanguages();

            $META = [];
            $META['title'] = $Product->translate($request->getLocale())->getMetaTitle();
            $META['description'] = $Product->translate($request->getLocale())->getMetaDescription();
            $META['alternate'] = [];

            $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
            $linguePreferite = explode(',', $linguePreferite);

            foreach ($AdditionalData['langs'] as $sigla => $estesa) {
                $key = $sigla;
                if ($key == $linguePreferite[0]) {
                    $key = 'x-default';
                }

                $META['alternate'][$key] = $pathManager->generateUrl(
                    'recensione_prodotto',
                    ['prodotto' => $Product],
                    $sigla
                );
            }

            $AdditionalData['META'] = $META;

            $twigs = $TemplateLoader->getTwigs(
                'recensione_prodotto',
                $Languages->getActivePublicLanguages(),
                $AdditionalData
            );

            $META = $MetaManager->merge($twigs, $META);
            if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

                $metaVars = ['{NOME}' => $Product->translate()->getTitolo()];

                if (!isset($META['description']) || !$META['description']) {

                    $META = $MetaManager->manageDefaults(
                        'description',
                        'prodotti-ecommerce',
                        $META,
                        $metaVars
                    );
                }
                if (!isset($META['title']) || !$META['title']) {
                    $META = $MetaManager->manageDefaults('title', 'prodotti-ecommerce', $META, $metaVars);
                }
            }

            $META = array_merge($META, $this->get('app.open_graph')->generateOpenGraphData($Product));


            $img = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . $this->get("assets.packages")->getUrl($productHelper->getImmaginePrincipale($Product));


            return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

        }


        throw new NotFoundHttpException("Pagina non trovata");


    }


    /**
     * @Route("/xhr/recensiona/{id}", defaults={"_locale"="it"}, name="recensiona_it")
     * @Route("/xhr/{_locale}/recensiona/{id}", requirements={"_locale" = "de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="recensiona")
     */
    public
    function indexController(Request $request)
    {

        $Recensione = new Recensione();

        $form = $this->createForm(RecensioneForm::class);

        $trans = $this->get('translator');

        $form->handleRequest($request);

        $return = [];
        $return['result'] = false;
        $return['errors'] = [$trans->trans('default.labels.undefined_error')];

        if ($form->isSubmitted()) {

//            foreach ($request->request->all() as $k => $v) {
//                if (preg_match('/.*_wt$/', $k)) {
//                    if ($v) {
//                        throw new HttpException(400, 'Richiesta non valida');
//                    }
//                }
//            }

            if ($form->isValid()) {

                $return['result'] = true;
                $return['errors'] = [];
                /**
                 * @var $Recensione Commento
                 */
                $Recensione = $form->getData();

                $Recensione->setIsEnabled(true);

                $em = $this->getDoctrine()->getManager();
                $em->persist($Recensione);
                $em->flush();

//                /**
//                 * @var $News News;
//                 */
//                $News = $Recensione->getNews();
//
//                $data['email'] = $Recensione->getEmail();
//                $data['nome'] = $Recensione->getNome();
//                $data['messaggio'] = $Recensione->getMessaggio();
//                $data['News'] = $News->translate($request->getLocale())->getTitolo();
//                $data['createdAt'] = $Recensione->getCreatedAt()->format('d-m-Y');
//                $data['NewsUrl'] = $this->get('app.path_manager')->generateUrl('blog_read_news',
//                    [
//                        'slug' => $News->getPrimaryCategory()->translate($request->getLocale())->getSlug(),
//                        'slugnotizia' => $News->translate($request->getLocale())->getSlug(),
//                    ],
//                    $request->getLocale(),
//                    true
//                );
//
//
//                $message = \Swift_Message::newInstance()
//                    ->setSubject('Nuovo commento')
//                    ->setFrom($this->getParameter('emails')['default_from'], 'Webtek CMS')
//                    ->setTo($this->getParameter('emails')['destinatario_contatti'])
//                    ->setBody(
//                        $this->renderView(
//                            'public/email-commenti.twig',
//                            ['data' => $data]
//                        ),
//                        'text/html'
//                    );
//
//                $this->get('mailer')->send($message);


                return new JsonResponse($return);

            } else {

                $errors = $form->getErrors(true);

                $return['errors'] = [];

                foreach ($errors as $error) {
                    /**
                     * @var $error FormError
                     */
                    $return['errors'][] = $error->getMessage();

                }


                return new JsonResponse($return);

            }

        }

        return new JsonResponse($return);


    }

}