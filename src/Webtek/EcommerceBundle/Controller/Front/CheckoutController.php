<?php
// 30/05/17, 10.14
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Controller\Front;


use AnagraficaBundle\Entity\Anagrafica;
use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webtek\EcommerceBundle\Entity\Order;
use Webtek\EcommerceBundle\Form\SceltaPagamentoForm;
use Webtek\EcommerceBundle\Event\OrderStatusChangeEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Webtek\EcommerceBundle\EventSubscriber\OrderStatusSubscriber;


class CheckoutController extends Controller
{

    /**
     * @Route("/checkout", defaults={"_locale"="it"}, name="checkout_it")
     * @Route("/{_locale}/checkout", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="checkout")
     */
    public function checkout(Request $request)
    {

        $Languages = $this->get('app.languages');

        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];

        $AdditionalData = [];
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);
        $pathManager = $this->get('app.path_manager');

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
            $META['alternate'][$key] = $pathManager->generateUrl(
                'checkout',
                [],
                $sigla
            );
        }

        $AdditionalData['META'] = $META;

        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');

        $twigs = $TemplateLoader->getTwigs(
            'checkout',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );

        $META = $MetaManager->merge($twigs, $META);

        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];

            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'checkout',
                    $META,
                    $metaVars
                );
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'checkout', $META, $metaVars);
            }
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $authorizationChecker = $this->container->get('security.authorization_checker');
        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }

    /**
     * @Route("/checkout/now", defaults={"_locale"="it"}, name="checkout_now")
     */
    public function doCheckout(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $carrelloHelper = $this->get('app.webtek_ecommerce.services.carrello_helper');
        $orderHelper = $this->get('app.webtek_ecommerce.services.order_helper');
        $paymenthHelper = $this->get('app.webtek_ecommerce.services.payment_helper');

        $carrello = $carrelloHelper->getCarrello();

        $errors = [];

        /**
         * @var $user User
         */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $authorizationChecker = $this->get('security.authorization_checker');

        if ($carrello->hasRecords() && $user != 'anon.' && $authorizationChecker->isGranted('ROLE_ANAGRAFICA')) {
            /**
             * @var $Customer Anagrafica
             */
            $Customer = $em->getRepository('AnagraficaBundle:Anagrafica')->getByUser($user);

            $form = $this->createForm(SceltaPagamentoForm::class);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $data = $form->getData();

                if ($data['Address'] && $data['Delivery']) {

                    $selectedAddress = null;
                    $selectedDelivery = null;

                    $Indirizzi = $Customer->getIndirizzi();

                    foreach ($Indirizzi as $IndirizzoAnagrafica) {
                        if ($IndirizzoAnagrafica->getId() == $data['Address']) {
                            $selectedAddress = $IndirizzoAnagrafica;
                        }
                    }


                    if ($selectedAddress) {
                        $Corrieri = $em->getRepository('WebtekEcommerceBundle:Delivery')->getByAddressAndCart(
                            $selectedAddress,
                            $carrelloHelper->getTotale($carrello)
                        );

                        foreach ($Corrieri as $Corriere) {

                            if ($Corriere->getId() == $data['Delivery']) {
                                $selectedDelivery = $Corriere;
                            }

                        }

                    }

                    if ($selectedDelivery && $selectedAddress && $data['Payment']) {

                        /**
                         * @var Order $Order
                         */
                        $Order = $orderHelper->createOrder(
                            $carrello,
                            $Customer,
                            $selectedAddress,
                            $selectedDelivery,
                            $data['Payment']
                        );
                        // Estraggo l'url da chiamare per autorizzare il pagamento
                        $Order = $paymenthHelper->getPaymentUrl($Order, $request->getSession()->getId());

                        $orderHelper->callStatusChangeEvent($Order);


                        return $this->redirect($Order->getPaymentUrl());


                    } else {
                        $errors[] = 'ecommerce.error.dati_incompleti';
                    }

                } else {
                    $errors[] = 'ecommerce.error.delivery_o_address_false';
                }
            } else {
                $errors[] = 'ecommerce.error.form_checkout_non_valido';
            }


        } else {

            $pathManager = $this->get('app.path_manager');

            return $this->redirect($pathManager->generateUrl('home', [], $request->getLocale()));
        }

        return new Response('<body></body>');
    }

    /**
     * @Route("/checkout/pay/{order_id}", defaults={"_locale"="it"}, name="checkout_pay")
     */
    function pay(Request $request)
    {

        $logger = $this->get('monolog.logger.payments');

        $logger->info('Richiesto pagamento dell\'ordine n. {idOrdine}', ['idOrdine' => $request->get('order_id')]);

        $em = $this->getDoctrine()->getManager();
        $paymenthHelper = $this->get('app.webtek_ecommerce.services.payment_helper');

        $Order = $em->getRepository('WebtekEcommerceBundle:Order')->findOneBy(
            ['id' => $request->get('order_id')]
        );

        if (!$Order) {
            $logger->error(
                'Ordine n. {idOrdine} non trovato',
                ['idOrdine' => $request->get('order_id'), 'where' => basename(__FILE__) . ':' . __LINE__]
            );

            throw new Exception(sprintf('Ordine n. %s non trovato', $request->get('order_id')));

        } else {

            $url = $paymenthHelper->getPaymentUrl($Order);

            if (!$url) {
                throw new Exception('Problemi nella connessione con il gateway di pagamento');
            }

//            $this->get('app.webtek_ecommerce.services.order_helper')->sendMailOrder($Order);
//            $this->get('swiftmailer.mailer.default.spool')->flushQueue($this->get('swiftmailer.transport'));

            return new Response('<body></body>');

            //return new RedirectResponse($this->redirect($url));

        }

    }

    /**
     * @Route("/checkout/keyclientverify_srv", defaults={"_locale"="it"}, name="checkout_verify_keyclient_server_it")
     * @Route("/{_locale}/checkout/keyclientverify_srv", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="checkout_verify_keyclient_server")
     */
    function verifyKeyclientServer(Request $request)
    {

        $paymenthHelper = $this->get('app.webtek_ecommerce.services.payment_helper');
        $Order = $paymenthHelper->keyClientVerifyServer($request);
        return new Response(1);
    }

    /**
     * @Route("/checkout/keyclientverifysession", defaults={"_locale"="it"}, name="checkout_verify_keyclientsession_it")
     * @Route("/{_locale}/checkout/keyclientverifysession", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="checkout_verify_keyclientsession")
     */
    function verifyKeyclientSessione(Request $request)
    {
        $pathManager = $this->get('app.path_manager');
        $paymenthHelper = $this->get('app.webtek_ecommerce.services.payment_helper');
        $Order = $paymenthHelper->keyClientVerify($request);
        if ($Order) {

            return $this->redirect(
                $pathManager->generate(
                    'checkout_grazie',
                    [],
                    $request->getLocale(),
                    true
                )
            );
        } else {
            throw new Exception('Il pagamento dell\'ordine non è andato a buon fine.');
        }
    }

    /**
     * @Route("/checkout/keyclientverify", defaults={"_locale"="it"}, name="checkout_verify_keyclient_it")
     * @Route("/{_locale}/checkout/keyclientverify", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="checkout_verify_keyclient")
     */
    function verifyKeyclient(Request $request)
    {
        $pathManager = $this->get('app.path_manager');
        if ($request->get("esito") == "OK") {
            return $this->redirect(
                $pathManager->generate(
                    'checkout_grazie',
                    [],
                    $request->getLocale(),
                    true
                )
            );
        } else {
            throw new Exception('Il pagamento dell\'ordine non è andato a buon fine.');
        }

        die();

    }

    /**
     * @Route("/checkout/paypalverify", defaults={"_locale"="it"}, name="checkout_verify_paypal_it")
     * @Route("/{_locale}/checkout/paypalverify", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="checkout_verify_paypal")
     */
    function verifyPaypal(Request $request)
    {

        $paymenthHelper = $this->get('app.webtek_ecommerce.services.payment_helper');
        $pathManager = $this->get('app.path_manager');

        $Order = $paymenthHelper->paypalVerify($request->query->all());
        if ($Order) {

            return $this->redirect(
                $pathManager->generate(
                    'checkout_grazie',
                    ['order-id' => $Order->getId(), 'transactionId' => $request->query->get('paymentId')],
                    $request->getLocale(),
                    true
                )
            );

        } else {

            throw new Exception('Il pagamento dell\'ordine non è andato a buon fine.');

        }
        die;

    }

    /**
     * @Route("/checkout/grazie", defaults={"_locale"="it"}, name="checkout_grazie_it")
     * @Route("/{_locale}/checkout/grazie", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="checkout_grazie")
     */
    function grazie(Request $request)
    {


        $Languages = $this->get('app.languages');

        $META = [];
        $META['title'] = '';
        $META['description'] = '';
        $META['robots'] = 'noindex';
        $META['alternate'] = [];
        $AdditionalData = [];
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();
        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);
        $pathManager = $this->get('app.path_manager');

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
            $META['alternate'][$key] = $pathManager->generateUrl(
                'checkout_grazie',
                [],
                $sigla
            );
        }
        $AdditionalData['META'] = $META;

        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');

        $twigs = $TemplateLoader->getTwigs(
            'grazie',
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );

        $META = $MetaManager->merge($twigs, $META);


        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {

            $metaVars = [];

            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults(
                    'description',
                    'grazie',
                    $META,
                    $metaVars
                );
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'grazie', $META, $metaVars);
            }
        }

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }


    /**
     * @Route("/checkout/paypal-cancel", defaults={"_locale"="it"}, name="checkout_cancel_paypal_it")
     * @Route("/{_locale}/checkout/paypal-cancel", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="checkout_cancel_paypal")
     */
    function cancelPaypal(Request $request)
    {


        die;

    }

    /**
     * @Route("/checkout/save-address", defaults={"_locale"="it"}, name="checkout_save_address")
     */
    function saveAddress(Request $request)
    {

        if ($request->request->has('indirizzo_anagrafica_form')) {

            $formIndirizziHelper = $this->get('app.anagrafica.form.indirizzo_form_helper');

            $Anagrafica = new Anagrafica();

            $defaultData = [
                'anagrafica' => $Anagrafica->getId(),
                'nazione' => $Anagrafica->getNazione(),
                'provincia' => 0,
                'comune' => 0,
            ];

            $form = $formIndirizziHelper->createForm($defaultData, $request->getLocale());

            $form->submit($request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $AddressData = $form->getData();
                $IndirizzoAnagrafica = new IndirizzoAnagrafica();
                $result = $this->container->get('app.anagrafica.form.indirizzo_form_helper')->handleData(
                    $AddressData,
                    $IndirizzoAnagrafica
                );

                if ($result) {

                    return new JsonResponse(['result' => true]);

                }

            } else {
                $errors = [];
                $trans = $this->get('translator');
                foreach ($form->getErrors(true) as $error) {
                    /**
                     * @var $error FormError
                     */

                    /**
                     * @var $Config FormConfigInterface
                     */
                    $Config = $error->getOrigin()->getConfig();

                    $lbl = $Config->getOptions()['label'];
                    if ($lbl) {
                        $lbl = $trans->trans($lbl);
                    } else {
                        $lbl = ucfirst($Config->getName());
                    }
                    $errors[] = $lbl . ': ' . $error->getMessage();
                }

                $errors = array_unique($errors);

                $return = [];
                $return['result'] = false;
                $return['errors'] = $errors;

                return new JsonResponse($return);

            }

        }
    }
}