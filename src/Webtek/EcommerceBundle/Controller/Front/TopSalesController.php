<?php
// 25/05/17, 16.15
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\Brand;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;

class TopSalesController extends Controller
{
//    /**
//     * @Route("/productsbrand-json/{id}/{page}"))
//     */
//    public function infiniteLoadingAction(Request $request, Brand $id, $page)
//    {
//
//        $Brand = $id;
//        $em = $this->getDoctrine()->getManager();
//
//        $data = [];
//
//        $NewsHeader = [];
//
//        $next = false;
//        $prev = false;
//
//        $products = $em->getRepository("WebtekEcommerceBundle:Product")->findByBrand($request->getLocale(), $Brand->translate($request->getLocale())->getSlug(), $this->getParameter('generali')['elementi_pagina_paginatore'], false, $this->getParameter('generali')['elementi_pagina_paginatore'] * $page);
//        $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlistArray($products);
//
//        $paginator = $this->container->get('knp_paginator');
//        $pagination = $paginator->paginate(
//            $products,
//            $page,
//            $this->getParameter('generali')['elementi_pagina_paginatore']
//        );
//
//        $data = [];
//
////        $blogService = $this->get('app.blog_front_end');
//
//        $data = $this->get("app.webtek_ecommerce.services.product_helper")->productsQueryResultParser($products, $request);
//
//        $return = [];
//        $return['result'] = true;
//        $return['data'] = $data;
//        if (isset($page)) {
//            $return['page'] = $page;
//        }
//
//        return new JsonResponse($return);
//
//
//    }

    /**
     * @Route("/prodotti-piu-venduti", defaults={"_locale"="it", "page"="1"}, name="topsales_ecommerce_it")
     * @Route("/{_locale}/prodotti-piu-venduti", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it", "page"="1"}, name="topsales_ecommerce")
     */
    public function readAction(Request $request)
    {
        $em = $this->getDoctrine();
        $Languages = $this->get('app.languages');
        $TemplateLoader = $this->get('app.template_loader');
        $MetaManager = $this->get('app.meta_manager');

        $AdditionalData = [];
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();

        $META = [];
        $META['alternate'] = [];

        $linguePreferite = $this->getParameter('parametri-avanzati')['ordine_lingue_default'];
        $linguePreferite = explode(',', $linguePreferite);

        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            $key = $sigla;
            if ($key == $linguePreferite[0]) {
                $key = 'x-default';
            }
//                    $META['alternate'][$key] = $brandHelper->generaUrl($Brand, $sigla, [], true);
        }

        $AdditionalData['META'] = $META;

        $twigs = $TemplateLoader->getTwigs(
            "prodotti_piu_venduti",
            $Languages->getActivePublicLanguages(),
            $AdditionalData
        );

        $META = $MetaManager->merge($twigs, $META);
        if (!isset($META['description']) || !$META['description'] || !isset($META['title']) || !$META['title']) {


            if (!isset($META['description']) || !$META['description']) {
                $META = $MetaManager->manageDefaults('description', 'prodotti-piu-venduti-ecommerce', $META, []);
            }
            if (!isset($META['title']) || !$META['title']) {
                $META = $MetaManager->manageDefaults('title', 'prodotti-piu-venduti-ecommerce', $META, []);
            }
        }
        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);


    }

}