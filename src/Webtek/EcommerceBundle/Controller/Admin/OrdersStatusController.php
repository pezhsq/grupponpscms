<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\OrdersStatus;
use Webtek\EcommerceBundle\Form\OrdersStatusForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_ORDERSSTATUS')")
 */
class OrdersStatusController extends Controller
{

    /**
     * @Route("/orders-status", name="orders_status")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/orders_status/list.html.twig');
    }

    /**
     * @Route("/orders-status/json", name="orders_status_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.orders_status_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/orders-status/new", name="orders_status_new")
     * @Route("/orders-status/edit/{id}",  name="orders_status_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $OrdersStatus = new OrdersStatus();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $OrdersStatus = $em->getRepository('WebtekEcommerceBundle:OrdersStatus')->findOneBy(['id' => $id]);

            if (!$OrdersStatus) {
                return $this->redirectToRoute('orders_status_new');
            }

        }

        $form = $this->createForm(
            OrdersStatusForm::class,
            $OrdersStatus,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $OrdersStatus OrdersStatus
             */

            $OrdersStatus = $form->getData();

            $new = false;

            if (!$OrdersStatus->getId()) {
                $new = true;
            }

            $em->persist($OrdersStatus);
            $em->flush();

            $elemento = $OrdersStatus->translate($request->getLocale())->getTitolo();

            $em->persist($OrdersStatus);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'OrdersStatus "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'OrdersStatus "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('orders_status');

        }

        $view = '@WebtekEcommerce/admin/orders_status/new.html.twig';

        if ($OrdersStatus->getId()) {
            $view = '@WebtekEcommerce/admin/orders_status/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/orders-status/toggle-enabled/{id}", name="orders_status_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, OrdersStatus $OrdersStatus)
    {

        $elemento = '"'.$OrdersStatus->translate()->getTitolo().'" ('.$OrdersStatus->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'OrdersStatus '.$elemento.' ';

        if ($OrdersStatus->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $OrdersStatus->setIsEnabled(!$OrdersStatus->getIsEnabled());

        $em->persist($OrdersStatus);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('orders_status');


    }

    /**
     * @Route("/orders-status/delete/{id}/{force}", name="orders_status_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, OrdersStatus $OrdersStatus)
    {

        if ($OrdersStatus) {

            $elemento = $OrdersStatus->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($OrdersStatus->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($OrdersStatus);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'OrdersStatus "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'OrdersStatus "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$OrdersStatus->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'OrdersStatus "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($OrdersStatus);
                $em->flush();


            }

        }

        return $this->redirectToRoute('orders_status');

    }

    /**
     * @Route("/orders-status/restore/{id}", name="orders_status_restore")
     */
    public function restoreAction(Request $request, OrdersStatus $OrdersStatus)
    {

        if ($OrdersStatus->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $OrdersStatus->translate($request->getLocale())->getTitolo().' ('.$OrdersStatus->getId().')';

            $OrdersStatus->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'OrdersStatus "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('orders_status');

    }

}