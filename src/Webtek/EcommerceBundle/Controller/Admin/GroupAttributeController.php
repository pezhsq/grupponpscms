<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\GroupAttribute;
use Webtek\EcommerceBundle\Form\GroupAttributeForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_ATTRIBUTES')")
 */
class GroupAttributeController extends Controller
{

    /**
     * @Route("/group-attribute", name="group_attribute")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/group_attribute/list.html.twig');

    }

    /**
     * @Route("/group-attribute/json", name="group_attribute_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return           = [];
        $return['result'] = true;
        $return['data']   = $this->get('app.webtek_ecommerce.services.group_attribute_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/group-attribute/new", name="group_attribute_new")
     * @Route("/group-attribute/edit/{id}",  name="group_attribute_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs        = $this->get('app.languages')->getActiveLanguages();
        $translator   = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $GroupAttribute = new GroupAttribute();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $GroupAttribute = $em->getRepository('WebtekEcommerceBundle:GroupAttribute')->findOneBy(
                ['id' => $id]
            );

            if (!$GroupAttribute) {
                return $this->redirectToRoute('group_attribute_new');
            }

            $uuid = $id;

        }

        $form = $this->createForm(
            GroupAttributeForm::class,
            $GroupAttribute,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $GroupAttribute GroupAttribute
             */

            $GroupAttribute = $form->getData();

            $new = false;

            if (!$GroupAttribute->getId()) {
                $new = true;
            }

            $em->persist($GroupAttribute);
            $em->flush();

            $elemento = $GroupAttribute->translate($request->getLocale())->getTitolo();

            $em->persist($GroupAttribute);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'GroupAttribute "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash(
                    'success',
                    'GroupAttribute "'.$elemento.'" '.$translator->trans('default.labels.creato')
                );
            }


            return $this->redirectToRoute('group_attribute');

        }

        $view = '@WebtekEcommerce/admin/group_attribute/new.html.twig';

        if ($GroupAttribute->getId()) {
            $view = '@WebtekEcommerce/admin/group_attribute/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/group-attribute/delete/{id}/{force}", name="group_attribute_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, GroupAttribute $GroupAttribute)
    {

        if ($GroupAttribute) {

            $elemento = $GroupAttribute->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($GroupAttribute->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($GroupAttribute);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'GroupAttribute "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'GroupAttribute "'.$elemento.'" '.$translator->trans(
                            'categoria_vetrina.errors.non_cancellabile'
                        )
                    );

                }


            } elseif (!$GroupAttribute->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'GroupAttribute "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($GroupAttribute);
                $em->flush();


            }

        }

        return $this->redirectToRoute('group_attribute');

    }

    /**
     * @Route("/group-attribute/restore/{id}", name="group_attribute_restore")
     */
    public function restoreAction(Request $request, GroupAttribute $GroupAttribute)
    {

        if ($GroupAttribute->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $GroupAttribute->translate($request->getLocale())->getTitolo().' ('.$GroupAttribute->getId(
                ).')';

            $GroupAttribute->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'GroupAttribute "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('group_attribute');

    }

}