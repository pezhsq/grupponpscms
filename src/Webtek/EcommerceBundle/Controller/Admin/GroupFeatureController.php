<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\GroupFeature;
use Webtek\EcommerceBundle\Form\GroupFeatureForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_FEATURES')")
 */
class GroupFeatureController extends Controller
{

    /**
     * @Route("/group-feature", name="group_feature")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/group_feature/list.html.twig');

    }

    /**
     * @Route("/group-feature/json", name="group_feature_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return           = [];
        $return['result'] = true;
        $return['data']   = $this->get('app.webtek_ecommerce.services.group_feature_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/group-feature/new", name="group_feature_new")
     * @Route("/group-feature/edit/{id}",  name="group_feature_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs        = $this->get('app.languages')->getActiveLanguages();
        $translator   = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $GroupFeature = new GroupFeature();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $GroupFeature = $em->getRepository('WebtekEcommerceBundle:GroupFeature')->findOneBy(['id' => $id]);

            if (!$GroupFeature) {
                return $this->redirectToRoute('group_feature_new');
            }

        }

        $form = $this->createForm(
            GroupFeatureForm::class,
            $GroupFeature,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $GroupFeature GroupFeature
             */

            $GroupFeature = $form->getData();

            $new = false;

            if (!$GroupFeature->getId()) {
                $new = true;
            }

            $em->persist($GroupFeature);
            $em->flush();

            $elemento = $GroupFeature->translate($request->getLocale())->getTitolo();

            $em->persist($GroupFeature);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'GroupFeature "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'GroupFeature "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('group_feature');

        }

        $view = '@WebtekEcommerce/admin/group_feature/new.html.twig';

        if ($GroupFeature->getId()) {
            $view = '@WebtekEcommerce/admin/group_feature/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/group-feature/toggle-enabled/{id}", name="brand_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, GroupFeature $GroupFeature)
    {

        $elemento = '"'.$GroupFeature->translate()->getTitolo().'" ('.$GroupFeature->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'GroupFeature '.$elemento.' ';

        if ($GroupFeature->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $GroupFeature->setIsEnabled(!$GroupFeature->getIsEnabled());

        $em->persist($GroupFeature);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('group_feature');


    }

    /**
     * @Route("/group-feature/delete/{id}/{force}", name="group_feature_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, GroupFeature $GroupFeature)
    {

        if ($GroupFeature) {

            $elemento = $GroupFeature->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($GroupFeature->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($GroupFeature);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'GroupFeature "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'GroupFeature "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$GroupFeature->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'GroupFeature "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($GroupFeature);
                $em->flush();


            }

        }

        return $this->redirectToRoute('group_feature');

    }

    /**
     * @Route("/group-feature/restore/{id}", name="group_feature_restore")
     */
    public function restoreAction(Request $request, GroupFeature $GroupFeature)
    {

        if ($GroupFeature->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $GroupFeature->translate($request->getLocale())->getTitolo().' ('.$GroupFeature->getId().')';

            $GroupFeature->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'GroupFeature "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('group_feature');

    }

}