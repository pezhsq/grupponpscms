<?php
// 09/01/17, 16.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Controller\Admin;

use Webtek\EcommerceBundle\Entity\Tax;
use Webtek\EcommerceBundle\Form\TaxForm;

use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_TAX_ECOMMERCE')")
 */
class TaxController extends Controller
{

    /**
     * @Route("/tax-ecommerce", name="tax_ecommerce")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/tax/list.html.twig');

    }

    /**
     * @Route("/tax-ecommerce/new", name="tax_ecommerce_new")
     */
    public function newAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(TaxForm::class, null);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $NewsCategory = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($NewsCategory);
                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Tassa '.$NewsCategory->getCodice()." ".$translator->trans('default.labels.creata')
                );

                return $this->redirectToRoute('tax_ecommerce');
            } else {
                $this->addFlash('error', 'Impossibile creare l\'elemento , controlla gli errori');
            }


        }

        return $this->render('@WebtekEcommerce/admin/tax/new.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/tax_ecommerce/toggle-enabled/{id}", name="tax_ecommerce_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Tax $newsCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $translator = $this->get('translator');

        $elemento = $newsCategory->getCodice();

        $flash = 'Tassa '.$elemento.' ';

        if ($newsCategory->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $newsCategory->setIsEnabled(!$newsCategory->getIsEnabled());

        $em->persist($newsCategory);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('tax_ecommerce');


    }


    /**
     * @Route("/tax_ecommerce/edit/{id}", name="tax_ecommerce_edit")
     */
    public function editAction(Request $request, Tax $newsCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(TaxForm::class, $newsCategory);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($newsCategory);
                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Tassa '.$newsCategory->getCodice().' '.$translator->trans('default.labels.modificata')
                );

                return $this->redirectToRoute('tax_ecommerce');
            } else {
                $this->addFlash('error', 'Impossibile modificare l\'elemento , controlla gli errori');
            }


        }

        return $this->render('@WebtekEcommerce/admin/tax/new.html.twig', ['form' => $form->createView()]);

    }


    /**
     * @Route("/tax_ecommerce/json", name="tax_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $CategorieNewsList = $em->getRepository('Webtek\EcommerceBundle\Entity\Tax')->findAll();
        } else {
            $CategorieNewsList = $em->getRepository('AppBundle:NewsCategory')->findAllNotDeleted();
        }

        $retData = [];

        $vetrine = [];

        foreach ($CategorieNewsList as $NewsCategory) {
            /**
             * @var $NewsCategory NewsCategory
             */
            $record                = [];
            $record['id']          = $NewsCategory->getId();
            $record['codice']      = $NewsCategory->getCodice();
            $record['aliquota']    = $NewsCategory->getAliquota();
            $record['descrizione'] = $NewsCategory->getDescrizione();
            $record['isEnabled']   = $NewsCategory->getIsEnabled();
            $record['deleted']     = $NewsCategory->isDeleted();
            $record['createdAt']   = $NewsCategory->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt']   = $NewsCategory->getUpdatedAt()->format('d/m/Y H:i:s');

            $vetrine[] = $record;
        }

        $retData['data'] = $vetrine;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/tax_ecommerce/restore/{id}", name="newscat_restore")
     */
    public function restoreAction(Request $request, Tax $newsCategory)
    {

        if ($newsCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $newsCategory->getCodice();

            $newsCategory->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Tassa '.$elemento.' '.$translator->trans('default.labels.ripristinata'));

        }

        return $this->redirectToRoute('tax_ecommerce');

    }


    /**
     * @Route("/tax_ecommerce/delete/{id}/{force}", name="tax_ecommerce_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Tax $newsCategory)
    {

        $elemento = $newsCategory->getCodice();

        $translator = $this->get('translator');


        $em = $this->getDoctrine()->getManager();
        if ($newsCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }

            }

            // remove the entity
            $em->remove($newsCategory);

            try {

                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash('success', 'Tassa "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));


            } catch (ForeignKeyConstraintViolationException $e) {

                $this->addFlash('error', 'Tassa "'.$elemento.'" '.$translator->trans('news.errors.non_cancellabile'));

            }


        } elseif (!$newsCategory->isDeleted()) {

            $newsCategory->setIsEnabled(0);
            $em->flush();

            $em->persist($newsCategory);

            $translator = $this->get('translator');

            $this->addFlash('success', 'Tassa "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));

            $em->remove($newsCategory);
            $em->flush();


        }


        return $this->redirectToRoute('tax_ecommerce');

    }


}
