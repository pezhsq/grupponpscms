<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Delivery;
use Webtek\EcommerceBundle\Entity\DeliveryPrices;
use Webtek\EcommerceBundle\Form\DeliveryForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_DELIVERY')")
 */
class DeliveryController extends Controller
{

    /**
     * @Route("/delivery", name="delivery")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/delivery/list.html.twig');

    }

    /**
     * @Route("/delivery/json", name="delivery_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return           = [];
        $return['result'] = true;
        $return['data']   = $this->get('app.webtek_ecommerce.services.delivery_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/delivery/new", name="delivery_new")
     * @Route("/delivery/edit/{id}",  name="delivery_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs        = $this->get('app.languages')->getActiveLanguages();
        $translator   = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $Delivery = new Delivery();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Delivery = $em->getRepository('WebtekEcommerceBundle:Delivery')->findOneBy(['id' => $id]);

            if (!$Delivery) {
                return $this->redirectToRoute('delivery_new');
            }

        }

        $form = $this->createForm(
            DeliveryForm::class,
            $Delivery,
            ['langs' => $langs]
        );


        $originalPrices = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($Delivery->getPrices() as $Price) {
            $originalPrices->add($Price);
        }

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Delivery Delivery
             */

            $Delivery = $form->getData();

            $new = false;

            if (!$Delivery->getId()) {
                $new = true;
            }


            // remove the relationship between the tag and the Task
            foreach ($originalPrices as $Price) {
                /**
                 * @var $Price DeliveryPrices
                 */
                if (false === $Delivery->getPrices()->contains($Price)) {
                    $Price->setDelivery(null);

                    $em->remove($Price);
                }
            }


            $em->persist($Delivery);
            $em->flush();

            $elemento = $Delivery->translate($request->getLocale())->getNome();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Delivery "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Delivery "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('delivery');

        }

        $view = '@WebtekEcommerce/admin/delivery/new.html.twig';

        if ($Delivery->getId()) {
            $view = '@WebtekEcommerce/admin/delivery/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/delivery/toggle-enabled/{id}", name="delivery_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Delivery $Delivery)
    {

        $elemento = '"'.$Delivery->translate()->getNome().'" ('.$Delivery->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Delivery '.$elemento.' ';

        if ($Delivery->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Delivery->setIsEnabled(!$Delivery->getIsEnabled());

        $em->persist($Delivery);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('delivery');


    }

    /**
     * @Route("/delivery/delete/{id}/{force}", name="delivery_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Delivery $Delivery)
    {

        if ($Delivery) {

            $elemento = $Delivery->translate($request->getLocale())->getNome();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Delivery->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Delivery);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Delivery "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Delivery "'.$elemento.'" '.$translator->trans('delivery.errors.non_cancellabile')
                    );

                }


            } elseif (!$Delivery->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Delivery "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($Delivery);
                $em->flush();


            }

        }

        return $this->redirectToRoute('delivery');

    }

    /**
     * @Route("/delivery/restore/{id}", name="delivery_restore")
     */
    public function restoreAction(Request $request, Delivery $Delivery)
    {

        if ($Delivery->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Delivery->translate($request->getLocale())->getNome().' ('.$Delivery->getId().')';

            $Delivery->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Delivery "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('delivery');

    }

}