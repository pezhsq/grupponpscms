<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormConfigInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Attribute;
use Webtek\EcommerceBundle\Form\AttributeForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_ATTRIBUTES')")
 */
class AttributeController extends Controller
{

    /**
     * @Route("/attribute", name="attribute")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/attribute/list.html.twig');

    }

    /**
     * @Route("/attribute/json", name="attribute_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.attribute_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/attribute/new", name="attribute_new")
     * @Route("/attribute/edit/{id}",  name="attribute_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $Attribute = new Attribute();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Attribute = $em->getRepository('WebtekEcommerceBundle:Attribute')->findOneBy(['id' => $id]);

            if (!$Attribute) {
                return $this->redirectToRoute('attribute_new');
            }

            $uuid = $id;

        } else {

            $errori = [];

            $contaGruppi = $em->getRepository('WebtekEcommerceBundle:GroupAttribute')->countAllNotDeleted();

            if (!$contaGruppi) {
                $errori[] = $translator->trans('attribute.errors.no_group');
            }

            if (count($errori)) {
                $this->addFlash(
                    'error',
                    'Per poter procedere devi prima risovere i seguenti problemi: <br />'.implode('<br />', $errori)
                );

                return $this->redirectToRoute('attribute');
            }

        }

        $form = $this->createForm(
            AttributeForm::class,
            $Attribute,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        $errors = false;

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Attribute Attribute
             */

            $Attribute = $form->getData();

            $new = false;

            if (!$Attribute->getId()) {
                $new = true;
            }

            $em->persist($Attribute);
            $em->flush();

            $elemento = $Attribute->translate($request->getLocale())->getTitolo();

            $em->persist($Attribute);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Attribute "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Attribute "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('attribute');

        } else {
            $errors = [];
            $trans = $this->get('translator');
            foreach ($form->getErrors(true) as $error) {
                /**
                 * @var $error FormError
                 */

                /**
                 * @var $Config FormConfigInterface
                 */
                $Config = $error->getOrigin()->getConfig();

                $lbl = $Config->getOptions()['label'];
                if ($lbl) {
                    $lbl = $trans->trans($lbl);
                } else {
                    $lbl = ucfirst($Config->getName());
                }
                $errors[] = $lbl.': '.$error->getMessage();
            }

            $errors = array_unique($errors);
            $errors = implode('<br />', $errors);

        }

        $view = '@WebtekEcommerce/admin/attribute/new.html.twig';

        if ($Attribute->getId()) {
            $view = '@WebtekEcommerce/admin/attribute/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView(), 'errors' => $errors]);


    }

    /**
     * @Route("/attribute/delete/{id}/{force}", name="attribute_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Attribute $Attribute)
    {

        if ($Attribute) {

            $elemento = $Attribute->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Attribute->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Attribute);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Attribute "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Attribute "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Attribute->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Attribute "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($Attribute);
                $em->flush();


            }

        }

        return $this->redirectToRoute('attribute');

    }

    /**
     * @Route("/attribute/restore/{id}", name="attribute_restore")
     */
    public function restoreAction(Request $request, Attribute $Attribute)
    {

        if ($Attribute->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Attribute->translate($request->getLocale())->getTitolo().' ('.$Attribute->getId().')';

            $Attribute->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Attribute "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('attribute');

    }

}