<?php
// 24/04/17, 9.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Controller\Admin;


use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Webtek\EcommerceBundle\Entity\Brand;
use Webtek\EcommerceBundle\Form\BrandForm;

/**
 * Class BrandsController
 * @package Webtek\EcommerceBundle\Controller\Admin
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_BRANDS')")
 */
class BrandController extends Controller
{

    /**
     * @Route("/brands", name="brands")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/brands/list.html.twig');

    }

    /**
     * @Route("/brands/json", name="brands_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.brand_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/brands/new", name="brands_new")
     * @Route("/brands/edit/{id}",  name="brands_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $Brand = new Brand();

        $id = null;

        $supportData = [];
        $supportData['listImgUrl'] = false;

        $uuid = $Brand->getUuid();

        if ($request->get('id')) {

            $id = $request->get('id');

            $Brand = $em->getRepository('WebtekEcommerceBundle:Brand')->findOneBy(['id' => $id]);

            if (!$Brand) {
                return $this->redirectToRoute('brands_new');
            }

            if ($Brand->getListImgFileName()) {
                $supportData['listImgUrl'] = '/'.$Brand->getUploadDir().$Brand->getListImgFileName();
            }


            $uuid = $id;

        }

        $form = $this->createForm(
            BrandForm::class,
            $Brand,
            ['langs' => $langs, 'attr' => ['data-id' => $uuid]]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Brand Brand
             */

            $Brand = $form->getData();

            $new = false;

            if (!$Brand->getId()) {
                $new = true;
            }

            $em->persist($Brand);
            $em->flush();

            $elemento = $Brand->translate($request->getLocale())->getTitolo();

            $cancellaListPrecedente = $request->get('brand_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($Brand, 'listImg');
                $Brand->setListImg(null);
                $Brand->setListImgAlt('');
            }

            $em->persist($Brand);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Brand "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Brand "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('brands');

        }

        $view = '@WebtekEcommerce/admin/brands/new.html.twig';

        if ($Brand->getId()) {
            $view = '@WebtekEcommerce/admin/brands/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView(), 'supportData' => $supportData]);


    }

    /**
     * @Route("/brands/toggle-enabled/{id}", name="brand_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Brand $Brand)
    {

        $elemento = '"'.$Brand->translate()->getTitolo().'" ('.$Brand->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Brand '.$elemento.' ';

        if ($Brand->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Brand->setIsEnabled(!$Brand->getIsEnabled());

        $em->persist($Brand);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('brands');


    }

    /**
     * @Route("/brands/delete/{id}/{force}", name="brands_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     * FIXME controllare se il marchio può essec
     */
    public function deleteAction(Request $request, Brand $Brand)
    {

        if ($Brand) {

            $elemento = $Brand->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Brand->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Brand);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Appartamento "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Appartamento "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Brand->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Appartamento "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($Brand);
                $em->flush();


            }

        }

        return $this->redirectToRoute('brands');

    }

    /**
     * @Route("/brands/restore/{id}", name="brands_restore")
     */
    public function restoreAction(Request $request, Brand $Brand)
    {

        if ($Brand->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Brand->translate($request->getLocale())->getTitolo().' ('.$Brand->getId().')';

            $Brand->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Appartamento "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('brands');

    }

}