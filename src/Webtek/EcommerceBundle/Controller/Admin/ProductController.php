<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormConfigInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\ProductAttachment;
use Webtek\EcommerceBundle\Entity\Recensione;
use Webtek\EcommerceBundle\Entity\VarianteProdotto;
use Webtek\EcommerceBundle\Event\ProductSavedEvent;
use Webtek\EcommerceBundle\Form\ProductForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_PRODUCTS')")
 */
class ProductController extends Controller
{

    /**
     * @Route("/product", name="product")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/product/list.html.twig');

    }

    /**
     * @Route("/product/sort", name="product_sort")
     */
    public function sortProductAction()
    {

        $em = $this->getDoctrine()->getManager();
        $trans = $this->get('translator');
        $categorie = [];
        $categorie[0] = $trans->trans('default.labels.select_one');
        $Categories = $em->getRepository('WebtekEcommerceBundle:Category')->findAll();
        foreach ($Categories as $category) {

            /**
             * @var $category Category
             */
            $categorie[$category->getId()] = $category->translate()->getNome();
        }

        return $this->render('@WebtekEcommerce/admin/product/sort.html.twig', ['categorie' => $categorie]);

    }

    /**
     * @Route("/product/save-sort", name="products_save_sort")
     */
    public function saveSorting(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $trans = $this->get('translator');
        $return = [];
        if ($request->get('sorted')) {

            $em = $this->getDoctrine()->getManager();
            $res = $em->getRepository('WebtekEcommerceBundle:Product')->getSortedByIds($request->get('sorted'));
            $Category = $em->getRepository('WebtekEcommerceBundle:Category')->findOneBy(
                ['id' => $request->get('category')]
            );
            $return['result'] = $this->get('app.webtek_ecommerce.services.product_helper')->saveOrderForCategory(
                $res,
                $Category
            );

            return new JsonResponse($return);

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);


    }

    /**
     * @Route("/product/getproducts/{id}", name="products_json")
     */
    public function getProductsForSortingAction(Category $category)
    {

        $em = $this->getDoctrine()->getManager();
        $trans = $this->get('translator');
        $return = [];
        if ($category) {

            $Prodotti = $em->getRepository('WebtekEcommerceBundle:Product')->getSortedByCategory($category);
            $data = [];
            $data['items'] = [];
            foreach ($Prodotti as $Prodotto) {
                /**
                 * @var $Prodotto Product
                 */
                $label = $Prodotto->translate()->getTitolo();
                if (!$Prodotto->getIsEnabled()) {
                    $label .= ' ('.$trans->trans('default.labels.disabilitato').')';
                }
                $record = [];
                $record['id'] = $Prodotto->getId();
                $record['label'] = $label;
                $data['items'][] = $record;


            }
            $return['result'] = true;
            $return['data'] = $data;

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('vetrina.labels.category_not_found')];

        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/product/json", name="product_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.product_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/product/recensioni/{id}", name="product_recensioni_list")
     */
    public function recensioniAction(Request $request)
    {

        return $this->render('@WebtekEcommerce/admin/product/list_recensioni.html.twig', ['id' => $request->get('id')]);

    }

    /**
     * @Route("/product/recensioni/json/{id}", name="product_recensioni_list_json")
     */
    public function listRecensioniJson(Request $request, Product $product)
    {

        $recensioni = $product->getRecensioni();
        $return = [];
        $return['data'] = [];
        foreach ($recensioni as $Recensione) {

            /**
             * @var $Recensione Commento;
             */
            $record = [];
            $record['id'] = $Recensione->getId();
            $record['cliente'] = $Recensione->getAnagrafica()->getUser()->getEmail();
            $record['valore'] = $Recensione->getValore();
            $record['messaggio'] = $Recensione->getMessaggio();
            $record['isEnabled'] = $Recensione->getIsEnabled();
            $record['deleted'] = $Recensione->isDeleted();
            $record['createdAt'] = $Recensione->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Recensione->getUpdatedAt()->format('d/m/Y H:i:s');
            $return['data'][] = $record;
        }

        return new JsonResponse($return);

    }


    /**
     * @Route("/product/toggle-recensioni-enabled/{id}", name="product_toggle_recensioni_enabled")
     */
    public function RecensionetoggleIsEnabledAction(
        Request $request,
        Recensione $Recensione
    ) {

        $em = $this->getDoctrine()->getManager();
        $flash = " Recensione ";
        if ($Recensione->getIsEnabled()) {
            $flash .= 'disabilitata';
        } else {
            $flash .= 'abilitata';
        }
        $Recensione->setIsEnabled(!$Recensione->getIsEnabled());
        $em->persist($Recensione);
        $em->flush();
        $this->addFlash('success', $flash);

        return $this->redirectToRoute('product_recensioni_list', ['id' => $Recensione->getProdotto()->getId()]);


    }


    /**
     * @Route("/product/delete-recensione/{id}/{force}", name="product_delete_recensione",  requirements={"id" = "\d+"},
     *     defaults={"force" = false}))
     */
    public function deleteRecensioneAction(
        Request $request,
        Recensione $Recensione
    ) {

        $elemento = " Recensione ";
        $em = $this->getDoctrine()->getManager();
        if ($Recensione->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];
            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;
                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }
            }
            // remove the entity
            $em->remove($Recensione);
            $em->flush();

        } elseif (!$Recensione->isDeleted()) {
            $em->remove($Recensione);
            $em->flush();
        }
        $this->addFlash('success', $elemento.' eliminata');

        return $this->redirectToRoute('product_recensioni_list', ['id' => $Recensione->getProdotto()->getId()]);

    }

    /**
     * @Route("/product/restore-recensione/{id}", name="product_restore_recensione")
     */
    public function restoreRecensioneAction(Request $request, Recensione $Recensione)
    {

        if ($Recensione->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();
            $Recensione->restore();
            $em->flush();
            $this->addFlash('success', 'Recensione ripristinata');

        }

        return $this->redirectToRoute('product_recensioni_list', ['id' => $Recensione->getProdotto()->getId()]);

    }

    /**
     * @Route("/product/new", name="product_new")
     * @Route("/product/edit/{id}",  name="product_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $formHelper = $this->get('app.webtek_ecommerce.forms.product_form_helper');
        $em = $this->getDoctrine()->getManager();
        $Product = new Product();
        $id = null;
        $azione = $translator->trans('default.labels.creato');
        $VariantiOriginali = null;
        $CategorieOriginali = null;
        if ($request->get('id')) {

            $id = $request->get('id');
            $Product = $em->getRepository('WebtekEcommerceBundle:Product')->findOneBy(['id' => $id]);
            if ($Product) {

                if (!$Product) {
                    return $this->redirectToRoute('product_new');
                }
                $VariantiOriginali = new ArrayCollection();
                $PhotoOriginali = [];
                foreach ($Product->getVarianti() as $variante) {
                    $photo = new ArrayCollection();
                    foreach ($variante->getPhotos() as $Photo) {
                        $photo->add($Photo);
                    }
                    $PhotoOriginali[$variante->getId()] = $photo;
                    $VariantiOriginali->add($variante);
                }
                $CategorieOriginali = new ArrayCollection();
                foreach ($Product->getCategorie() as $Category) {
                    $CategorieOriginali->add($Category);
                }
                $azione = $translator->trans('default.labels.modificato');

            } else {
                return $this->redirectToRoute('product_new');

            }
        } else {

            // verifico alcune cose, tipo se ci sono categorie, listini, brands
            $errori = [];
            $contaCategorie = $em->getRepository('WebtekEcommerceBundle:Category')->countAllNotDeleted();
            if (!$contaCategorie) {
                $errori[] = $translator->trans('products.errors.no_category');
            }
            $contaListino = $em->getRepository('WebtekEcommerceBundle:Listino')->countAllNotDeleted();
            if (!$contaListino) {
                $errori[] = $translator->trans('products.errors.no_listino');
            }
            if ($this->getParameter('ecommerce')['manage_brands']) {

                $contaBrands = $em->getRepository('WebtekEcommerceBundle:Brand')->countAllNotDeleted();
                if (!$contaBrands) {
                    $errori[] = $translator->trans('products.errors.no_brands');
                }

            }
            if ($this->getParameter('ecommerce')['manage_attributes']) {

                $contaFamiglie = $em->getRepository('WebtekEcommerceBundle:ProductType')->countAllNotDeleted();
                if (!$contaFamiglie) {
                    $errori[] = $translator->trans('products.errors.no_families');
                }

            }
            if (count($errori)) {
                $this->addFlash(
                    'error',
                    'Per poter procedere devi prima risovere i seguenti problemi: <br />'.implode('<br />', $errori)
                );

                return $this->redirectToRoute('product');
            }


        }
        $defaultData = $formHelper->retrieveDefaultData($Product);
        $defaultData['validationGroup'] = ($Product->getId()) ? 'update' : 'create';
        /**
         * @var $form FormInterface
         */
        $formOptions = [
            'locale' => $request->getLocale(),
        ];
        $formData = $formHelper->createForm($defaultData, $formOptions);
        $form = $formData['form'];
        $form->handleRequest($request);
        $errors = [];
        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                if ($CategorieOriginali) {

                    foreach ($CategorieOriginali as $Category) {
                        if (false == $Product->getCategorie()->contains($Category)) {
                            $Product->removeCategoria($Category);
                            $em->persist($Product);
                        }
                    }

                }
                $event = new ProductSavedEvent($Product);
                $this->get('event_dispatcher')->dispatch(ProductSavedEvent::NAME, $event);
                if ($VariantiOriginali) {
                    foreach ($VariantiOriginali as $VarianteProdotto) {
                        if (false === $Product->getVarianti()->contains($VarianteProdotto)) {
                            $em->remove($VarianteProdotto);
                        } else {
                            foreach ($Product->getVarianti() as $Variante) {

                                if (isset($PhotoOriginali[$Variante->getId()])) {

                                    foreach ($PhotoOriginali[$Variante->getId()] as $Photo) {
                                        if (false == $Variante->getPhotos()->contains($Photo)) {
                                            /**
                                             * @var $Photo ProductAttachment
                                             */
                                            $Photo->setVarianti(null);
                                            $em->persist($Photo);
                                        }
                                    }
                                }

                            }
                        }
                    }

                }
                $em->flush();
                if (!$formHelper->handleData($form, $Product)) {

                    $errors = $formHelper->getErrors();
                    $errors = array_unique($errors);

                } else {

                    $this->addFlash(
                        'success',
                        'Prodotto '.$Product->translate()->getTitolo().' '.$azione
                    );

                    return $this->redirectToRoute('product_edit', ['id' => $Product->getId()]);

                }

            } else {

                $errors = [];
                $trans = $this->get('translator');
                foreach ($form->getErrors(true) as $error) {
                    /**
                     * @var $error FormError
                     */
                    /**
                     * @var $formErrore FormInterface
                     */
                    $formErrore = $error->getOrigin();
                    /**
                     * @var $Config FormConfigInterface
                     */
                    $Config = $formErrore->getConfig();
                    $lbl = $Config->getOptions()['label'];
                    if ($lbl) {
                        $lbl = $trans->trans($lbl);
                    } else {
                        $lbl = ucfirst($Config->getName());
                    }
                    $name = $formErrore->getParent()->getName();
                    if (in_array($name, array_keys($langs))) {

                        $lbl .= ' ('.$langs[$name].')';

                    }
                    $errors[] = $lbl.': '.$error->getMessage();
                }
                $errors = array_unique($errors);

            }
        }
        $view = '@WebtekEcommerce/admin/product/new.html.twig';
        if ($Product->getId()) {
            $view = '@WebtekEcommerce/admin/product/edit.html.twig';
        }

        return $this->render(
            $view,
            [
                'form' => $form->createView(),
                'supportData' => $formData['supportData'],
                'errors' => $errors,
                'listini' => $em->getRepository('WebtekEcommerceBundle:Listino')->findAllNotDeleted(),
                'action' => $defaultData['validationGroup'],
            ]
        );


    }

    /**
     * @Route("/product/toggle-enabled/{id}", name="product_toggle_enabled")
     */
    public function toggleIsEnabledAction(
        Request $request,
        Product $Product
    ) {

        $elemento = '"'.$Product->translate()->getTitolo().'" ('.$Product->getId().')';
        $em = $this->getDoctrine()->getManager();
        $flash = 'Product '.$elemento.' ';
        if ($Product->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }
        $Product->setIsEnabled(!$Product->getIsEnabled());
        $em->persist($Product);
        $em->flush();
        $this->addFlash('success', $flash);

        return $this->redirectToRoute('product');


    }

    /**
     * @Route("/product/delete/{id}/{force}", name="product_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(
        Request $request,
        Product $Product
    ) {

        if ($Product) {

            $elemento = $Product->translate($request->getLocale())->getTitolo();
            $translator = $this->get('translator');
            $em = $this->getDoctrine()->getManager();
            if (!$Product->isDeleted()) {

                $translator = $this->get('translator');
                $this->addFlash(
                    'success',
                    'Product "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );
                $em->remove($Product);
                $em->flush();

            }

        }

        return $this->redirectToRoute('product');

    }

    /**
     * @Route("/product/restore/{id}", name="product_restore")
     */
    public function restoreAction(Request $request, Product $Product)
    {

        if ($Product->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();
            $elemento = $Product->translate($request->getLocale())->getTitolo().' ('.$Product->getId().')';
            $Product->restore();
            $em->flush();
            $translator = $this->get('translator');
            $this->addFlash(
                'success',
                'Product "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('product');

    }

    /**
     * @Route("/product/search", name="product_search")
     */
    function searchAction(Request $request)
    {

        $vichHelper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $termine = $request->get('query');
        $exclude = $request->get('exclude');
        $exclude = explode(',', $exclude);
        $exclude = array_filter($exclude);
        $em = $this->getDoctrine()->getManager();
        $Products = $em->getRepository('WebtekEcommerceBundle:Product')->search($termine, $exclude);
        $return = [];
        $return['suggestions'] = [];
        foreach ($Products as $Product) {

            /**
             * @var $Product Product
             */
            $record = [];
            $record['value'] = $Product->translate('it')->getTitolo();
            $record['data'] = [];
            $record['data']['nome'] = $Product->translate('it')->getTitolo();
            $record['data']['id'] = $Product->getId();
            $record['data']['img'] = false;
            if ($Product->getListImgFileName()) {
                $record['data']['img'] = $vichHelper->asset($Product, 'listImg');
            }
            $return['suggestions'][] = $record;

        }

        return new JsonResponse($return);


    }

    /**
     * @Route("/product/search-redirect", name="product_search_redirect")
     */
    function searchRedirect(Request $request)
    {

        $termine = $request->get('query');
        $exclude = $request->get('exclude');
        $exclude = explode(',', $exclude);
        $exclude = array_filter($exclude);
        $em = $this->getDoctrine()->getManager();
        $Products = $em->getRepository('WebtekEcommerceBundle:Product')->search($termine, $exclude);
        $return = [];
        $return['suggestions'] = [];
        foreach ($Products as $Product) {

            /**
             * @var $Product Product
             */
            $record = [];
            $record['value'] = $Product->translate('it')->getTitolo();
            $record['data'] = [];
            $record['data']['nome'] = $Product->translate('it')->getTitolo();
            $record['data']['id'] = $Product->getId();
            $return['suggestions'][] = $record;

        }

        return new JsonResponse($return);


    }

}