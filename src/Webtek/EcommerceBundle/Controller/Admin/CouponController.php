<?php
// 06/11/17, 8.53
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Coupon;
use Webtek\EcommerceBundle\Form\CouponForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_COUPON')")
 */
class CouponController extends Controller
{

    /**
     * @Route("/coupon", name="coupon")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/coupon/list.html.twig');

    }

    /**
     * @Route("/coupon/json", name="coupon_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.coupon_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/coupon/new", name="coupon_new")
     * @Route("/coupon/edit/{id}",  name="coupon_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');
        $em = $this->getDoctrine()->getManager();
        $Coupon = new Coupon();
        $id = null;
        if ($request->get('id')) {

            $id = $request->get('id');
            $Coupon = $em->getRepository('WebtekEcommerceBundle:Coupon')->findOneBy(['id' => $id]);
            if (!$Coupon) {
                return $this->redirectToRoute('coupon_new');
            }

        }
        $form = $this->createForm(
            CouponForm::class,
            $Coupon,
            ['langs' => $langs]
        );
        $form->handleRequest($request);
        $errors = [];
        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Coupon Coupon
             */
            $Coupon = $form->getData();
            $new = false;
            if (!$Coupon->getId()) {
                $new = true;
            }
            $em->persist($Coupon);
            $em->flush();
            $elemento = $Coupon->translate($request->getLocale())->getTitolo();
            $em->persist($Coupon);
            $em->flush();
            if (!$new) {
                $this->addFlash(
                    'success',
                    'Coupon "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Coupon "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }

            return $this->redirectToRoute('coupon');

        } else {
            $errors = $this->get('app.form_error_helper')->getErrors($form);

        }
        $view = '@WebtekEcommerce/admin/coupon/new.html.twig';
        if ($Coupon->getId()) {
            $view = '@WebtekEcommerce/admin/coupon/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView(), 'errors' => $errors]);


    }

    /**
     * @Route("/coupon/toggle-enabled/{id}", name="coupon_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Coupon $Coupon)
    {

        $elemento = '"'.$Coupon->translate()->getTitolo().'" ('.$Coupon->getId().')';
        $em = $this->getDoctrine()->getManager();
        $flash = 'Coupon '.$elemento.' ';
        if ($Coupon->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }
        $Coupon->setIsEnabled(!$Coupon->getIsEnabled());
        $em->persist($Coupon);
        $em->flush();
        $this->addFlash('success', $flash);

        return $this->redirectToRoute('coupon');


    }

    /**
     * @Route("/coupon/delete/{id}/{force}", name="coupon_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Coupon $Coupon)
    {

        if ($Coupon) {

            $elemento = $Coupon->translate($request->getLocale())->getTitolo();
            $translator = $this->get('translator');
            $em = $this->getDoctrine()->getManager();
            if ($Coupon->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];
                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;
                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }
                // remove the entity
                $em->remove($Coupon);
                try {

                    $em->flush();
                    $translator = $this->get('translator');
                    $this->addFlash(
                        'success',
                        'Coupon "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Coupon "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Coupon->isDeleted()) {

                $translator = $this->get('translator');
                $this->addFlash(
                    'success',
                    'Coupon "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );
                $em->remove($Coupon);
                $em->flush();


            }

        }

        return $this->redirectToRoute('coupon');

    }

    /**
     * @Route("/coupon/restore/{id}", name="coupon_restore")
     */
    public function restoreAction(Request $request, Coupon $Coupon)
    {

        if ($Coupon->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();
            $elemento = $Coupon->translate($request->getLocale())->getTitolo().' ('.$Coupon->getId().')';
            $Coupon->restore();
            $em->flush();
            $translator = $this->get('translator');
            $this->addFlash(
                'success',
                'Coupon "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('coupon');

    }

}