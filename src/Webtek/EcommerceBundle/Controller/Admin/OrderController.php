<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Order;
use Webtek\EcommerceBundle\Form\OrderForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_ORDER')")
 */
class OrderController extends Controller
{

    /**
     * @Route("/order", name="order")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/order/list.html.twig');

    }

    /**
     * @Route("/order/json", name="order_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.order_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/order/edit/{id}",  name="order_edit", requirements={"id": "\d+"})
     */
    public function editAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');

        $em = $this->getDoctrine()->getManager();

        $Order = new Order();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Order = $em->getRepository('WebtekEcommerceBundle:Order')->findOneBy(['id' => $id]);

            $oldStatus = $Order->getStatus();

            if (!$Order) {

                $this->addFlash(
                    'error',
                    'Ordine n. '.$request->get('id').' '.$translator->trans('default.labels.non_trovato')
                );

                return $this->redirectToRoute('order');
            }

        }

        $form = $this->createForm(
            OrderForm::class,
            $Order,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            /**
             * @var $OrderOrder
             */
            $Order = $form->getData();

            $em->persist($Order);
            $em->flush();

            $NewStatus = $Order->getStatus();

//            if ($oldStatus != $NewStatus || 1) {
//                $orderHelper = $this->get('app.webtek_ecommerce.services.order_helper');
//                $orderHelper->manageChangedStatus($Order);
//            }
            $em->persist($Order);
            $em->flush();

            $this->addFlash('success', 'Ordine n. '.$Order->getId().' '.$translator->trans('default.labels.salvato'));


            return $this->redirectToRoute('order');

        }

        $view = '@WebtekEcommerce/admin/order/edit.html.twig';

        return $this->render($view, ['form' => $form->createView(), 'order' => $Order]);


    }

    /**
     * @Route("/order/send-mail/{id}",  name="order_mail", requirements={"id": "\d+"})
     */
    public function sendMail(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $Order = new Order();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Order = $em->getRepository('WebtekEcommerceBundle:Order')->findOneBy(['id' => $id]);

            $oldStatus = $Order->getStatus();

            if (!$Order) {

                $this->addFlash(
                    'error',
                    'Ordine n. '.$request->get('id').' '.$translator->trans('default.labels.non_trovato')
                );

                return $this->redirectToRoute('order');
            }

        }

        $this->get('app.webtek_ecommerce.services.order_helper')->sendMailOrder($Order);

        return new Response('<body></body>');

    }

    /**
     * @Route("/order/statistiche",  name="order_stats")
     */
    public function statisticheAction(Request $request)
    {

        return $this->render(
            '@WebtekEcommerce/admin/order/statistiche.html.twig',
            []
        );

    }

    /**
     * @Route("/order/statistiche-anno",  name="order_stats_year")
     */
    public function statisticheAnno(Request $request)
    {

        $orderHelper = $this->get('app.webtek_ecommerce.services.order_helper');
        $stats = $orderHelper->getStatisticheAnnue();

        $data = [];

        foreach ($stats as $anno => $totali) {

            $record = array_merge(['y' => $anno], $totali);

            $data[] = $record;

        }

        $return = [];
        $return['result'] = true;
        $return['data'] = $data;

        return new JsonResponse($return);

    }

    /**
     * @Route("/order/statistiche-mese",  name="order_stats_month")
     */
    public function statisticheMese(Request $request)
    {

        $orderHelper = $this->get('app.webtek_ecommerce.services.order_helper');

        $stats = $orderHelper->getStatisticheMensili();

        $data = [];

        foreach ($stats as $periodo => $totali) {

            $record = array_merge(['y' => $periodo], $totali);

            $data[] = $record;

        }

        $return = [];
        $return['result'] = true;
        $return['data'] = $data;

        return new JsonResponse($return);

    }

}