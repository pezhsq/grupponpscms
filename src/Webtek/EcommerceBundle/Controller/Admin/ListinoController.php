<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Listino;
use Webtek\EcommerceBundle\Form\ListinoForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_LISTINO')")
 */
class ListinoController extends Controller
{

    /**
     * @Route("/listino", name="listino")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/listino/list.html.twig');

    }

    /**
     * @Route("/listino/json", name="listino_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return           = [];
        $return['result'] = true;
        $return['data']   = $this->get('app.webtek_ecommerce.services.listino_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/listino/new", name="listino_new")
     * @Route("/listino/edit/{id}",  name="listino_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs        = $this->get('app.languages')->getActiveLanguages();
        $translator   = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $Listino = new Listino();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Listino = $em->getRepository('WebtekEcommerceBundle:Listino')->findOneBy(['id' => $id]);

            if (!$Listino) {
                return $this->redirectToRoute('listino_new');
            }

        }

        $form = $this->createForm(
            ListinoForm::class,
            $Listino,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Listino Listino
             */

            $Listino = $form->getData();

            $new = false;

            if (!$Listino->getId()) {
                $new = true;
            }

            $em->persist($Listino);
            $em->flush();

            $elemento = $Listino->translate($request->getLocale())->getTitolo();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Listino "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Listino "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('listino');

        }

        $view = '@WebtekEcommerce/admin/listino/new.html.twig';

        if ($Listino->getId()) {
            $view = '@WebtekEcommerce/admin/listino/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/listino/toggle-default/{id}", name="listino_toggle_default")
     */
    public function toggleIsEnabledAction(Request $request, Listino $Listino)
    {

        $elemento = '"'.$Listino->translate()->getTitolo().'" ('.$Listino->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Listino '.$elemento.' reso predefinito';

        $Listino->setIsDefault(1);

        $em->persist($Listino);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('listino');


    }

    /**
     * @Route("/listino/delete/{id}/{force}", name="listino_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Listino $Listino)
    {

        if ($Listino) {

            $elemento = $Listino->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Listino->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Listino);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Listino "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Listino "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Listino->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Listino "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($Listino);
                $em->flush();


            }

        }

        return $this->redirectToRoute('listino');

    }

    /**
     * @Route("/listino/restore/{id}", name="listino_restore")
     */
    public function restoreAction(Request $request, Listino $Listino)
    {

        if ($Listino->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Listino->translate($request->getLocale())->getTitolo().' ('.$Listino->getId().')';

            $Listino->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Listino "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('listino');

    }

}