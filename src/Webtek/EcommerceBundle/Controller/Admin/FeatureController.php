<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\Feature;
use Webtek\EcommerceBundle\Form\FeatureForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_FEATURES')")
 */
class FeatureController extends Controller
{

    /**
     * @Route("/feature", name="feature")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/feature/list.html.twig');

    }

    /**
     * @Route("/feature/json", name="feature_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.feature_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/feature/get-options", name="feature_options")
     */
    public function getOptions(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['options'] = $this->get('app.webtek_ecommerce.services.feature_helper')->getOptions(
            $request->get('id')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/feature/new", name="feature_new")
     * @Route("/feature/edit/{id}",  name="feature_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $Feature = new Feature();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Feature = $em->getRepository('WebtekEcommerceBundle:Feature')->findOneBy(['id' => $id]);

            if (!$Feature) {
                return $this->redirectToRoute('feature_new');
            }

        } else {
            $errori = [];

            $contaGruppi = $em->getRepository('WebtekEcommerceBundle:GroupFeature')->countAllNotDeleted();

            if (!$contaGruppi) {
                $errori[] = $translator->trans('features.errors.no_group');
            }

            if (count($errori)) {
                $this->addFlash(
                    'error',
                    'Per poter procedere devi prima risovere i seguenti problemi: <br />' . implode('<br />', $errori)
                );
                return $this->redirectToRoute('feature');
            }

        }

        $form = $this->createForm(
            FeatureForm::class,
            $Feature,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Feature Feature
             */

            $Feature = $form->getData();

            $new = false;

            if (!$Feature->getId()) {
                $new = true;
            }

            $em->persist($Feature);
            $em->flush();

            $elemento = $Feature->translate($request->getLocale())->getTitolo();

            $em->persist($Feature);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Feature "' . $elemento . '" ' . $translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Feature "' . $elemento . '" ' . $translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('feature');

        }

        $view = '@WebtekEcommerce/admin/feature/new.html.twig';

        if ($Feature->getId()) {
            $view = '@WebtekEcommerce/admin/feature/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/feature/sort", name="feature_sort")
     */
    public function sortAction()
    {

        $em = $this->getDoctrine()->getManager();

        $Features = $em->getRepository('WebtekEcommerceBundle:Feature')->findBy([], ['sort' => 'asc']);

        $list = [];

        $group = 0;

        foreach ($Features as $Feature) {

            if ($Feature->getFeatureGroup()->getId() != $group) {
                $list['[grp]' . $group] = (string)$Feature->getFeatureGroup();
                $group = $Feature->getFeatureGroup()->getId();
            }

            $list[$Feature->getId()] = $Feature->translate()->getTitolo();

        }

        return $this->render('@WebtekEcommerce/admin/feature/sort.html.twig', ['list' => $list]);

    }

    /**
     * @Route("/feature/save-sort", name="feature_save_sort")
     */
    public function saveSortAction(Request $request)
    {

        $trans = $this->get('translator');

        $return = [];

        if ($request->get('sorted')) {

            $em = $this->getDoctrine()->getManager();

            $Features = $em->getRepository('WebtekEcommerceBundle:Feature')->getSortedByIds($request->get('sorted'));

            foreach ($Features as $k => $Feature) {
                /**
                 * @var $Feature Feature
                 */

                $Feature->setSort($k);
                $em->persist($Feature);
            }

            $return['result'] = true;

            $em->flush();

            return new JsonResponse($return);

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/feature/delete/{id}/{force}", name="feature_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Feature $Feature)
    {

        if ($Feature) {

            $elemento = $Feature->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Feature->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Feature);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Feature "' . $elemento . '" ' . $translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Feature "' . $elemento . '" ' . $translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Feature->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Feature "' . $elemento . '" ' . $translator->trans('default.labels.eliminata')
                );

                $em->remove($Feature);
                $em->flush();


            }

        }

        return $this->redirectToRoute('feature');

    }

    /**
     * @Route("/feature/restore/{id}", name="feature_restore")
     */
    public function restoreAction(Request $request, Feature $Feature)
    {

        if ($Feature->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Feature->translate($request->getLocale())->getTitolo() . ' (' . $Feature->getId() . ')';

            $Feature->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Feature "' . $elemento . '" ' . $translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('feature');

    }

}