<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\MarketPlace;
use Webtek\EcommerceBundle\Form\MarketPlaceForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_MARKETPLACE')")
 */
class MarketPlaceController extends Controller
{

    /**
     * @Route("/market-place", name="market_place")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/market_place/list.html.twig');

    }

    /**
     * @Route("/market-place/json", name="market_place_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return           = [];
        $return['result'] = true;
        $return['data']   = $this->get('app.webtek_ecommerce.services.market_place_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/market-place/new", name="market_place_new")
     * @Route("/market-place/edit/{id}",  name="market_place_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs        = $this->get('app.languages')->getActiveLanguages();
        $translator   = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $MarketPlace = new MarketPlace();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $MarketPlace = $em->getRepository('WebtekEcommerceBundle:MarketPlace')->findOneBy(['id' => $id]);

            if (!$MarketPlace) {
                return $this->redirectToRoute('market_place_new');
            }

        }

        $form = $this->createForm(
            MarketPlaceForm::class,
            $MarketPlace,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $MarketPlace MarketPlace
             */

            $MarketPlace = $form->getData();

            $new = false;

            if (!$MarketPlace->getId()) {
                $new = true;
            }

            $em->persist($MarketPlace);
            $em->flush();

            $elemento = $MarketPlace->getNome();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'MarketPlace "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'MarketPlace "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('market_place');

        }

        $view = '@WebtekEcommerce/admin/market_place/new.html.twig';

        if ($MarketPlace->getId()) {
            $view = '@WebtekEcommerce/admin/market_place/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/market-place/toggle-enabled/{id}", name="market_place_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, MarketPlace $MarketPlace)
    {

        $elemento = '"'.$MarketPlace->getNome().'" ('.$MarketPlace->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'MarketPlace '.$elemento.' ';

        if ($MarketPlace->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $MarketPlace->setIsEnabled(!$MarketPlace->getIsEnabled());

        $em->persist($MarketPlace);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('market_place');


    }

    /**
     * @Route("/market-place/delete/{id}/{force}", name="market_place_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, MarketPlace $MarketPlace)
    {

        if ($MarketPlace) {

            $elemento = $MarketPlace->getNome();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($MarketPlace->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($MarketPlace);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'MarketPlace "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'MarketPlace "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$MarketPlace->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'MarketPlace "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($MarketPlace);
                $em->flush();


            }

        }

        return $this->redirectToRoute('market_place');

    }

    /**
     * @Route("/market-place/restore/{id}", name="market_place_restore")
     */
    public function restoreAction(Request $request, MarketPlace $MarketPlace)
    {

        if ($MarketPlace->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $MarketPlace->getNome().' ('.$MarketPlace->getId().')';

            $MarketPlace->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'MarketPlace "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('market_place');

    }

}