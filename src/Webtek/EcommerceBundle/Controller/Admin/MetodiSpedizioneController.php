<?php
// 16/03/2017 , 10:46
// @author : Gabriele "gabricom" Colombera <gabricom-kun@live.it>

namespace Webtek\EcommerceBundle\Controller\Admin;

use Webtek\EcommerceBundle\Entity\MetodiSpedizione;
use Webtek\EcommerceBundle\Form\MetodiSpedizioneForm;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_SHIPMENT_METHODS')")
 */
class MetodiSpedizioneController extends Controller
{

    /**
     * @Route("/metodi-spedizione-ecommerce", name="metodi-spedizione-ecommerce")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin//shipments/list.html.twig');

    }


    /**
     * @Route("/metodi-spedizione-ecommerce/json", name="metodi-spedizione-ecommerce_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $MetodiSpedizione = $em->getRepository('Webtek\EcommerceBundle\Entity\MetodiSpedizione')->findAll();

        $retData = [];

        $vetrine = [];

        foreach ($MetodiSpedizione as $MetodoSpedizione) {
            /**
             * @var $MetodoSpedizione MetodiSpedizione
             */
            $record                    = [];
            $record['id']              = $MetodoSpedizione->getId();
            $record['isEnabled']       = $MetodoSpedizione->getIsEnabled();
            $record['nome']            = $MetodoSpedizione->getNome();
            $record['prezzoTassato']   = $MetodoSpedizione->getPrezzoTassato();
            $record['validitaNazioni'] = $MetodoSpedizione->getValiditaNazioni();
            $record['deleted']         = $MetodoSpedizione->isDeleted();
            $record['createdAt']       = $MetodoSpedizione->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt']       = $MetodoSpedizione->getUpdatedAt()->format('d/m/Y H:i:s');


            $vetrine[] = $record;
        }

        $retData['data'] = $vetrine;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/metodi-spedizione-ecommerce/new", name="metodi-spedizione-ecommerce_new")
     */
    public function newAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(MetodiSpedizioneForm::class, null, ['locale' => $request->getLocale()]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $MetodoSpedizione = $form->getData();
                if (count($MetodoSpedizione->getScontiPrezzo()) > 0) {
                    foreach ($MetodoSpedizione->getScontiPrezzo() as $sconto_prezzo) {
                        $this->get("tax_calculator")->CalcoloImposta($sconto_prezzo);
                    };
                }

                $this->get("tax_calculator")->CalcoloImposta($MetodoSpedizione);

                $em = $this->getDoctrine()->getManager();
                $em->persist($MetodoSpedizione);
                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Metodo di spedizione '.$MetodoSpedizione->getNome()." ".$translator->trans('default.labels.creato')
                );

                return $this->redirectToRoute('metodi-spedizione-ecommerce');
            } else {
                $this->addFlash('error', 'Impossibile creare l\'elemento , controlla gli errori');
            }


        }

        return $this->render('@WebtekEcommerce/admin//shipments/new.html.twig', ['newsCatForm' => $form->createView()]);

    }


    /**
     * @Route("/metodi-spedizione-ecommerce/toggle-enabled/{id}", name="metodi-spedizione-ecommerce_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, MetodiSpedizione $newsCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $translator = $this->get('translator');

        $elemento = $newsCategory->getNome();

        $flash = 'Metodo di spedizione '.$elemento.' ';

        if ($newsCategory->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $newsCategory->setIsEnabled(!$newsCategory->getIsEnabled());

        $em->persist($newsCategory);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('metodi-spedizione-ecommerce');


    }

    /**
     * @Route("/metodi-spedizione-ecommerce/edit/{id}", name="metodi-spedizione-ecommerce_edit")
     */
    public function editAction(Request $request, MetodiSpedizione $newsCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form                 = $this->createForm(
            MetodiSpedizioneForm::class,
            $newsCategory,
            ['locale' => $request->getLocale()]
        );
        $originalScontiPrezzo = new ArrayCollection();
        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($newsCategory->getScontiPrezzo() as $sconto) {
            $originalScontiPrezzo->add($sconto);
        }
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->isValid()) {

                $this->get("tax_calculator")->CalcoloImposta($newsCategory);
                foreach ($originalScontiPrezzo as $sconto) {
                    if (false === $newsCategory->getScontiPrezzo()->contains($sconto)) {
                        // remove the Task from the Tag


                        $em->remove($sconto);
                    }
                }
                foreach ($newsCategory->getScontiPrezzo() as $sconto_prezzo) {
                    $this->get("tax_calculator")->CalcoloImposta($sconto_prezzo);
                };
                $em = $this->getDoctrine()->getManager();
                $em->persist($newsCategory);
                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Metodo Pagamento '.$newsCategory->getNome().' '.$translator->trans('default.labels.modificato')
                );

                return $this->redirectToRoute('metodi-spedizione-ecommerce');
            } else {
                $this->addFlash('error', 'Impossibile modificare l\'elemento , controlla gli errori');
            }


        }

        return $this->render('@WebtekEcommerce/admin//shipments/new.html.twig', ['newsCatForm' => $form->createView()]);

    }

    /**
     * @Route("/metodi-spedizione-ecommerce/restore/{id}", name="metodi-spedizione-ecommerce_restore")
     */
    public function restoreAction(Request $request, MetodiSpedizione $newsCategory)
    {

        if ($newsCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $newsCategory->getNome();

            $newsCategory->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Metodo '.$elemento.' '.$translator->trans('default.labels.ripristinata'));

        }

        return $this->redirectToRoute('metodi-spedizione-ecommerce');

    }


    /**
     * @Route("/metodi-spedizione-ecommerce/delete/{id}/{force}", name="metodi-spedizione-ecommerce_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, MetodiSpedizione $newsCategory)
    {

        $elemento = $newsCategory->getNome();

        $translator = $this->get('translator');


        $em = $this->getDoctrine()->getManager();
        if ($newsCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }

            }

            // remove the entity
            $em->remove($newsCategory);

            try {

                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash('success', 'Metodo "'.$elemento.'" '.$translator->trans('default.labels.eliminato'));


            } catch (ForeignKeyConstraintViolationException $e) {

                $this->addFlash('error', 'Metodo "'.$elemento.'" '.$translator->trans('news.errors.non_cancellabile'));

            }


        } elseif (!$newsCategory->isDeleted()) {

            $newsCategory->setIsEnabled(0);
            $em->flush();

            $em->persist($newsCategory);

            $translator = $this->get('translator');

            $this->addFlash('success', 'Metodo "'.$elemento.'" '.$translator->trans('default.labels.eliminato'));

            $em->remove($newsCategory);
            $em->flush();


        }


        return $this->redirectToRoute('metodi-spedizione-ecommerce');

    }
}