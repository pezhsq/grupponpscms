<?php

namespace Webtek\EcommerceBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\ProductType;
use Webtek\EcommerceBundle\Form\ProductTypeForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ECOMMERCE_TYPES')")
 */
class ProductTypeController extends Controller
{

    /**
     * @Route("/product-type", name="product_type")
     */
    public function listAction()
    {

        return $this->render('@WebtekEcommerce/admin/product_type/list.html.twig');

    }

    /**
     * @Route("/product-type/json", name="product_type_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.webtek_ecommerce.services.product_type_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/product-type/new", name="product_type_new")
     * @Route("/product-type/edit/{id}",  name="product_type_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $ProductType = new ProductType();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $ProductType = $em->getRepository('WebtekEcommerceBundle:ProductType')->findOneBy(['id' => $id]);

            if (!$ProductType) {
                return $this->redirectToRoute('product_type_new');
            }

        }

        $form = $this->createForm(
            ProductTypeForm::class,
            $ProductType
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $ProductType ProductType
             */

            $ProductType = $form->getData();

            $new = false;

            if (!$ProductType->getId()) {
                $new = true;
            }

            $em->persist($ProductType);
            $em->flush();

            $elemento = $ProductType->getNome();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'ProductType "' . $elemento . '" ' . $translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'ProductType "' . $elemento . '" ' . $translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('product_type');

        }

        $view = '@WebtekEcommerce/admin/product_type/new.html.twig';

        if ($ProductType->getId()) {
            $view = '@WebtekEcommerce/admin/product_type/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/product-type/toggle-enabled/{id}", name="product_type_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, ProductType $ProductType)
    {

        $elemento = '"' . $ProductType->translate()->getTitolo() . '" (' . $ProductType->getId() . ')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'ProductType ' . $elemento . ' ';

        if ($ProductType->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $ProductType->setIsEnabled(!$ProductType->getIsEnabled());

        $em->persist($ProductType);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('product_type');


    }

    /**
     * @Route("/product-type/delete/{id}/{force}", name="product_type_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, ProductType $ProductType)
    {

        if ($ProductType) {

            $elemento = $ProductType->getNome();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($ProductType->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($ProductType);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'ProductType "' . $elemento . '" ' . $translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'ProductType "' . $elemento . '" ' . $translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$ProductType->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'ProductType "' . $elemento . '" ' . $translator->trans('default.labels.eliminata')
                );

                $em->remove($ProductType);
                $em->flush();


            }

        }

        return $this->redirectToRoute('product_type');

    }

    /**
     * @Route("/product-type/restore/{id}", name="product_type_restore")
     */
    public function restoreAction(Request $request, ProductType $ProductType)
    {

        if ($ProductType->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $ProductType->getNome() . ' (' . $ProductType->getId() . ')';

            $ProductType->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'ProductType "' . $elemento . '" ' . $translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('product_type');

    }

}