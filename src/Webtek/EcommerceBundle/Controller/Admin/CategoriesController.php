<?php
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Form\CategorieProdottiForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\FormError;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;

/**
 * @Route("/admin")
 */
class CategoriesController extends Controller
{

    /**
     * Creo la struttura necessaria al tree per recuperare i dati dall'albero su db
     * @param  [type] $root [description]
     * @return [type]       [description]
     */
    private function getFigli($root)
    {

        $tree = [];
        $children = $root->getChildNodes();
        foreach ($children as $child) {
            $tree[] = [
                "id" => $child->getId(),
                "text" => $child->translate('it')->getNome(),
                "children" => $this->getFigli($child),
            ];
        }

        return $tree;
    }


    /**
     * @Route("/categorie-prodotti/json-tree", name="products_categories_json_tree")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $root = $em->getRepository('Webtek\EcommerceBundle\Entity\Category')->getTreeOrdered();
        $tree = $this->getFigli($root);

        return new JsonResponse($tree);

    }

    /**
     * @Route("/categorie-prodotti/new/child/{id}", name="products_categories_new_child")
     */
    public function newChildAction(Request $request, Category $Category)
    {

        $em = $this->getDoctrine()->getManager();
        $langs = $this->get('app.languages')->getActiveLanguages();
        $url = $this->generateUrl("products_categories_new_child", ["id" => $Category->getId()]);
        $form = $this->createForm(
            CategorieProdottiForm::class,
            null,
            [
                'langs' => $langs,
                'action' => $url,
                'layout' => $this->getParameter('generali')['layout'],
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $CategorySlugger = $this->get('app.webtek_ecommerce.services.category_helper');
                $imageHandler = $this->get('app.base64_image');
                $Categoria = $form->getData();
                $Categoria->setIsEnabled(1);
                $Categoria->setSort(
                    count(
                        $em->getRepository('Webtek\EcommerceBundle\Entity\Category')->getTree(
                            $Category->getRealMaterializedPath()
                        )->getChildNodes()
                    )
                );
                $em->persist($Categoria);
                $em->flush();
                $Categoria->setChildNodeOf($Category);
                $em->persist($Categoria);
                $em->flush();
                $elemento = '"'.$Categoria->translate($request->getLocale())->getNome().'" ('.$Categoria->getId().')';
                $CategorySlugger->slugifyCategory($Categoria);
                $categoryIMG = $imageHandler->saveTo(
                    $request->get('categorie_prodotti_form')['CategoryImg'],
                    $Categoria,
                    basename($request->get('categorie_prodotti_form')['CategoryImgFileName'])
                );
                if ($categoryIMG) {
                    $Categoria->setCategoryImgFileName($categoryIMG);
                    $em->persist($Categoria);
                    $em->flush();
                }
                $this->addFlash(
                    'success',
                    'Categoria '.$elemento.' '.$this->get('translator')->trans('default.labels.creata')
                );

                return new JsonResponse(
                    [
                        'nodo_creato' => $Categoria->getId(),
                    ], 200
                );
            } else {
                $error = new FormError($this->get('translator')->trans("pages.errors.missing_parameters"));
                $form->addError($error);

                return new JsonResponse(
                    [
                        'form' => $this->renderView(
                            '@WebtekEcommerce/admin/categorie_prodotti/form.html.twig',
                            [
                                'form' => $form->createView(),
                            ]
                        ),
                    ], 400
                );
            }

        }

        return $this->render(
            '@WebtekEcommerce/admin/categorie_prodotti/form.html.twig',
            ['form' => $form->createView()]
        );

    }

    /**
     * @Route("/categorie-prodotti/new", name="products_categories_new")
     */
    public function newAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $langs = $this->get('app.languages')->getActiveLanguages();
        $form = $this->createForm(
            CategorieProdottiForm::class,
            null,
            [
                'langs' => $langs,
                'layout' => $this->getParameter('generali')['layout'],
            ]
        );

        return $this->render(
            '@WebtekEcommerce/admin/categorie_prodotti/form.html.twig',
            ['form' => $form->createView()]
        );

    }

    /**
     * @Route("/categorie-prodotti/edit/{id}", name="products_categories_edit")
     */
    public function editAction(Request $request, Category $Category)
    {

        $em = $this->getDoctrine()->getManager();
        $langs = $this->get('app.languages')->getActiveLanguages();
        $url = $this->generateUrl("products_categories_edit", ["id" => $Category->getId()]);
        $form = $this->createForm(
            CategorieProdottiForm::class,
            $Category,
            [
                'langs' => $langs,
                'action' => $url,
                'layout' => $this->getParameter('generali')['layout'],
            ]
        );
        $id_elemento = $Category->getId();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($Category);
            $em->flush();
            $imageHandler = $this->get('app.base64_image');
            $cancellaListPrecedente = $request->get('categorie_prodotti_form')['deleteCategoryImg'];
            if ($cancellaListPrecedente) {
                $Category->setCategoryImgFileName('');
            }
            $categoryIMG = $imageHandler->saveTo(
                $request->get('categorie_prodotti_form')['CategoryImg'],
                $Category,
                basename($request->get('categorie_prodotti_form')['CategoryImgFileName'])
            );
            if ($categoryIMG) {
                $Category->setCategoryImgFileName($categoryIMG);
                $em->persist($Category);
                $em->flush();
            } else {
                if ($cancellaListPrecedente) {
                    $em->persist($Category);
                    $em->flush();
                }
            }
            $elemento = '"'.$Category->translate($request->getLocale())->getNome().'" ('.$Category->getId().')';
            $translator = $this->get('translator');
            $this->addFlash('success', 'Categoria '.$elemento.' '.$translator->trans('default.labels.modificata'));

            return new JsonResponse(1);

        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $error = new FormError($this->get('translator')->trans("pages.errors.missing_parameters"));
            $form->addError($error);

            return new JsonResponse(
                [
                    'form' => $this->renderView(
                        '@WebtekEcommerce/admin/categorie_prodotti/form.html.twig',
                        [
                            'form' => $form->createView(),
                            'id_elemento' => $id_elemento,
                        ]
                    ),
                ], 400
            );
        }

        return $this->render(
            '@WebtekEcommerce/admin/categorie_prodotti/form.html.twig',
            ['form' => $form->createView(), 'id_elemento' => $id_elemento]
        );

    }

    /**
     * @Method("POST")
     * @Route("/categorie-prodotti/move", name="products_categories_move")
     * @Security("is_granted('ROLE_MANAGE_CATPRODUCTS')")
     */
    public function moveCategory(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $dati = $request->request->all();
        if ($dati['parent'] == "#") {
            $dati['parent'] = 1;
        }
        if ($dati['old_parent'] == "#") {
            $dati['old_parent'] = 1;
        }
        $categoria = $em->getRepository('Webtek\EcommerceBundle\Entity\Category')->find($dati['categoria']);
        $old_parent = $em->getRepository('Webtek\EcommerceBundle\Entity\Category')->find($dati['old_parent']);
        $parent = $em->getRepository('Webtek\EcommerceBundle\Entity\Category')->find($dati['parent']);
        $parent_mp = $parent->getRealMaterializedPath();
        $old_parent_mp = $old_parent->getRealMaterializedPath();
        if ($dati['old_parent'] == $dati['parent']) {
            if ($dati['old_position'] < $dati['position']) {
                $query = $em->createQuery(
                    "UPDATE Webtek\EcommerceBundle\Entity\Category c SET c.sort = c.sort -1 WHERE c.sort >= :old_pos AND c.sort <= :pos AND c.materializedPath = :mp"
                )->setParameter('old_pos', $dati['old_position'])->setParameter('pos', $dati['position'])->setParameter(
                        'mp',
                        $parent_mp
                    );
                $query->getResult();
                $categoria->setSort($dati['position']);
                $em->persist($categoria);
                $em->flush();
            } else {
                if ($dati['old_position'] > $dati['position']) {
                    $query = $em->createQuery(
                        "UPDATE Webtek\EcommerceBundle\Entity\Category c SET c.sort = c.sort +1 WHERE c.sort >= :pos AND c.sort <= :old_pos AND c.materializedPath = :mp"
                    )->setParameter('old_pos', $dati['old_position'])->setParameter(
                            'pos',
                            $dati['position']
                        )->setParameter('mp', $parent_mp);
                    $query->getResult();
                    $categoria->setSort($dati['position']);
                    $em->persist($categoria);
                    $em->flush();
                }
            }
        } else {
            $query = $em->createQuery(
                "UPDATE Webtek\EcommerceBundle\Entity\Category c SET c.sort = c.sort +1 WHERE c.sort >= :pos AND c.materializedPath = :mp"
            )->setParameter('pos', $dati['position'])->setParameter('mp', $parent_mp);
            $query->getResult();
            $categoria->setSort($dati['position']);
            $categoria->setChildNodeOf($parent);
            $em->persist($categoria);
            $em->flush();
            $query = $em->createQuery(
                "UPDATE Webtek\EcommerceBundle\Entity\Category c SET c.sort = c.sort -1 WHERE c.sort >= :old_pos AND c.materializedPath = :old_mp"
            )->setParameter('old_pos', $dati['old_position'])->setParameter('old_mp', $old_parent_mp);
            $query->getResult();
            $em->flush();
        }
        $CategorySlugger = $this->get("app.webtek_ecommerce.services.category_helper");
        $CategorySlugger->slugifyCategory($categoria, true);

        return new JsonResponse(1);


    }


    /**
     * @Method("POST")
     * @Route("/categorie-prodotti", name="products_categories_submit")
     * @Security("is_granted('ROLE_MANAGE_CATPRODUCTS')")
     */
    public function formSubmit(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $form = $this->createForm(
            CategorieProdottiForm::class,
            null,
            [
                'langs' => $langs,
                'layout' => $this->getParameter('generali')['layout'],
                'method' => 'POST',
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $root = $em->getRepository('Webtek\EcommerceBundle\Entity\Category')->getTree();
            $Categoria = $form->getData();
            $Categoria->setSort(count($root->getChildNodes()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($Categoria);
            $em->flush();
            $Categoria->setChildNodeOf($root);
            $em->persist($Categoria);
            $em->flush();
            $CategorySlugger = $this->get("app.webtek_ecommerce.services.category_helper");
            $CategorySlugger->slugifyCategory($Categoria);
            $imageHandler = $this->get('app.base64_image');
            $categoryIMG = $imageHandler->saveTo(
                $request->get('categorie_prodotti_form')['CategoryImg'],
                $Categoria,
                basename($request->get('categorie_prodotti_form')['CategoryImgFileName'])
            );
            if ($categoryIMG) {
                $Categoria->setCategoryImgFileName($categoryIMG);
                $em->persist($Categoria);
                $em->flush();
            }
            $elemento = '"'.$Categoria->translate($request->getLocale())->getNome().'" ('.$Categoria->getId().')';
            $this->addFlash(
                'success',
                'Categoria '.$elemento.' '.$this->get('translator')->trans('default.labels.creata')
            );

            return new JsonResponse(
                [
                    'nodo_creato' => $Categoria->getId(),
                ], 200
            );
        } else {
            $error = new FormError($this->get('translator')->trans("pages.errors.missing_parameters"));
            $form->addError($error);

            return new JsonResponse(
                [
                    'form' => $this->renderView(
                        '@WebtekEcommerce/admin/categorie_prodotti/form.html.twig',
                        [
                            'form' => $form->createView(),
                        ]
                    ),
                ], 400
            );
        }
    }


    /**
     * @Route("/categorie-prodotti/delete/{id}/{force}", name="products_categories_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Category $Category)
    {

        $elemento = '"'.$Category->translate($request->getLocale())->getNome().'" ('.$Category->getId().')';
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        if ($Category->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                'force'
            ) == 1 || true) {
            // initiate an array for the removed listeners
            $originalEventListeners = [];
            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;
                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }

            }
            // RECUPERO LA STRUTTURA A ALBERO E UCCIDO TUTTI I FIGLI
            $elemento_albero = $em->getRepository('Webtek\EcommerceBundle\Entity\Category')->getTree(
                $Category->getRealMaterializedPath()
            );
            $query = $em->createQuery(
                "UPDATE Webtek\EcommerceBundle\Entity\Category c SET c.sort = c.sort -1 WHERE c.sort > :old_pos  AND c.materializedPath = :mp"
            )->setParameter('old_pos', $Category->getSort())->setParameter(
                    'mp',
                    $Category->getMaterializedPath()
                )->getResult();
            foreach ($elemento_albero->getChildNodes() as $child) {
                $em->remove($child);
            }
            // remove the entity
            $em->remove($Category);
            try {

                $em->flush();
                $translator = $this->get('translator');
                $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));


            } catch (ForeignKeyConstraintViolationException $e) {

                $this->addFlash(
                    'error',
                    'Categoria "'.$elemento.'" '.$translator->trans('news.errors.non_cancellabile')
                );

            }


        } elseif (!$Category->isDeleted()) {
            $Category->setIsEnabled(0);
            $em->flush();
            $em->persist($Category);
            $translator = $this->get('translator');
            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));
            $em->remove($Category);
            $em->flush();


        }

        return new JsonResponse(1);

    }


    /**
     * @Route("/categorie-prodotti", name="products_categories")
     * @Security("is_granted('ROLE_MANAGE_CATPRODUCTS')")
     */
    public function listAction()
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $form = $this->createForm(
            CategorieProdottiForm::class,
            null,
            [
                'langs' => $langs,
                'action' => $this->generateUrl('products_categories_submit'),
                'layout' => $this->getParameter('generali')['layout'],
            ]
        );

        return $this->render(
            '@WebtekEcommerce/admin/categorie_prodotti/list.html.twig',
            ['form' => $form->createView()]
        );


    }


    /**
     * @Route("/categorie-prodotti/search-redirect", name="categorie_search_redirect")
     */
    function searchRedirect(Request $request)
    {

        $termine = $request->get('query');
        $em = $this->getDoctrine()->getManager();
        $return = [];
        $return['suggestions'] = [];
        $Categories = $em->getRepository('WebtekEcommerceBundle:Category')->search($termine);
        foreach ($Categories as $category) {

            /**
             * @var $category Category
             */
            $record = [];
            $record['value'] = $category->translate('it')->getNome();
            $record['data'] = [];
            $record['data']['nome'] = $category->translate('it')->getNome();
            $record['data']['id'] = $category->getId();
            $return['suggestions'][] = $record;

        }

        return new JsonResponse($return);


    }


}
