<?php

namespace Webtek\EcommerceBundle\Controller\Xhr;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/xhr")
 */
class PriceController extends Controller
{

    /**
     * @Route("/prices/calcolo-imposta")
     */
    public function CalcoloImpostaAction(Request $request)
    {

        $prezzo              = $request->request->get("prezzo");
        $codice_iva          = $request->request->get("codice_iva");
        $aliquota            = $this->getDoctrine()->getRepository("WebtekEcommerceBundle:Tax")->find(
            $codice_iva
        )->getAliquota();
        $results             = $this->get("tax_calculator")->CalcoloImpostaRaw($prezzo, $aliquota);
        $results['aliquota'] = $aliquota;

        return new JsonResponse($results);
    }


}
