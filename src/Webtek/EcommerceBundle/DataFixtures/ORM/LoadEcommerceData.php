<?php
// 18/03/2017 , 09:33
// @author : Gabriele "gabricom" Colombera <gabricom-kun@live.it>
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Intl\Intl;
use GeoBundle\Entity\Nazioni;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Webtek\EcommerceBundle\Entity\Setup;
use Webtek\EcommerceBundle\Entity\Category;

class LoadEcommerceData implements FixtureInterface, ContainerAwareInterface
{

    private $manager;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {

        $this->container = $container;

    }

    public function load(ObjectManager $manager)
    {

        $user = $manager->getRepository('AppBundle:User')->findOneBy(
            ['email' => 'giovanni.lenoci.webtek@gmail.com']
        );
        $token = new UsernamePasswordToken(
            $user, null, 'main', $user->getRoles()
        );
        $this->container->get('security.token_storage')->setToken($token);
        Fixtures::load(
            __DIR__.'/*.yml',
            $manager,
            ['providers' => [$this]]
        );
        $this->manager = $manager;
    }

}