var modulo = 'metodi-spedizione-ecommerce';
function calcoloImposta() {
  $.ajax({
      url: '/xhr/prices/calcolo-imposta',
      type: 'POST',
      dataType: 'json',
      data: { prezzo: $(".js-prezzo").val(), codice_iva: $(".js-codiceiva").val() }
    }
  ).done(function(r) {
    $(".js-aliquota").val(r.aliquota);
    $(".js-imposta").val(r.imposta);
    $(".js-prezzo-tassato").val(r.prezzo_tassato);
  });

}
var timeout;
var $collectionHolder;
var $addTagLink = $('<a href="#" class="add_tag_link">Add a tag</a>');
var $newLinkLi = $('<li></li>').append($addTagLink);

function addTagForm($collectionHolder, $newLinkLi) {
  // Get the data-prototype explained earlier
  var prototype = $collectionHolder.data('prototype');

  // get the new index
  var index = $collectionHolder.data('index');

  // Replace '__name__' in the prototype's HTML to
  // instead be a number based on how many items we have
  var newForm = prototype.replace(/__name__/g, index);

  // increase the index with one for the next item
  $collectionHolder.data('index', index + 1);


  // Display the form in the page in an li, before the "Add a tag" link li
  var $newFormLi = $('<li></li>').append(newForm);
  $newLinkLi.before($newFormLi);
  addTagFormDeleteLink($newFormLi);
}

function addTagFormDeleteLink($tagFormLi) {
  var $removeFormA = $('<a href="#">delete this tag</a>');
  $tagFormLi.append($removeFormA);

  $removeFormA.on('click', function(e) {
    // prevent the link from creating a "#" on the URL
    e.preventDefault();

    // remove the li for the tag form
    $tagFormLi.remove();
  });
}

$(function() {

  $collectionHolder = $('ul.tags');

  // add the "add a tag" anchor and li to the tags ul
  $collectionHolder.append($newLinkLi);

  // count the current form inputs we have (e.g. 2), use that as the new
  // index when inserting a new item (e.g. 2)
  $collectionHolder.data('index', $collectionHolder.find(':input').length);
  $collectionHolder.find('li').each(function() {
    addTagFormDeleteLink($(this));
  });
  $addTagLink.on('click', function(e) {
    // prevent the link from creating a "#" on the URL
    e.preventDefault();

    // add a new tag form (see next code block)
    addTagForm($collectionHolder, $newLinkLi);
  });

  if ($(".scelta-nazioni").length) {
    if ($(".scelta-nazioni").val() == false) {
      $(".pannello-nazioni").show();
    }
    $(".scelta-nazioni").change(function() {
      if ($(this).val() == false) {
        $(".pannello-nazioni").show();
      }
      else {
        $(".pannello-nazioni").hide();
      }
    });
  }
  if ($(".js-prezzo").length) {
    calcoloImposta();
  }


  $(".js-prezzo").on('keyup', function() {
    clearTimeout(timeout);
    timeout = setTimeout(calcoloImposta, 500);
  });
  $(".js-codiceiva").on('change', calcoloImposta);


  $(".ck").each(function(index, el) {
    attivaCkEditor(el);
  });


  /** Datatable della pagina di elenco */
  if ($('#datatable').length) {

    var cols = [];

    var col = {
      'title': 'Modifica',
      'className': 'dt0 dt-body-center',
      'searchable': false
    };
    cols.push(col);

    col = {
      'title': 'Nome Spedizione',
      'className': 'dt2',
      searchable: true,
      data: 'nome'
    };

    cols.push(col);


    col = {
      'title': 'Prezzo',
      'className': 'dt6',
      searchable: true,
      data: 'prezzoTassato'
    };

    cols.push(col);

    col = {
      'title': 'Valido per',
      'className': 'dt7',
      searchable: true,
      data: 'validitaNazioni'
    };

    cols.push(col);
    col = { 'className': 'dt-body-center' };

    // placeholder cancellazione
    cols.push(col);
    cols.push(col);


    var columnDefs = [];

    var columnDef = {
      targets: 0,
      searchable: false,
      orderable: false,
      className: 'dt-body-center',
      render: function(data, type, full, meta) {
        var toString = full.nome;
        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
      }
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 2,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.nome;
        var icon = 'check-square-o';
        var title = 'Disabilita: ';
        if (!full.isEnabled) {
          var icon = 'square-o';
          var title = 'Abilita: ';
        }
        var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
        ret += '<i class="fa fa-' + icon + '"></i>';
        ret += '</a>';
        return ret;
      }
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 1,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.nome;
        if (full.deleted) {
          var title = _('Recupera:');
          var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
          var title = _('Elimina definitivamente: ');
          ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
        } else {
          var title = 'Elimina: ';
          var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
        }
        return ret;
      }
    };

    columnDefs.push(columnDef);

    $('#datatable').dataTable({
      ajax: {
        "url": "/admin/" + modulo + "/json",
        error: function(r) {
          console.log(r);
        }
      },
      aaSorting: [[2, 'asc']],
      stateSave: true,
      iDisplayLength: 15,
      responsive: true,
      columns: cols,
      columnDefs: columnDefs,
      createdRow: function(row, data, index) {
        if (data.deleted) {
          $(row).addClass("danger");
        }
      }
    });

  }

});

/** FINE document.ready **/
/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {

  var id = textarea.id;

  if (typeof(CKEDITOR.instances[id]) == 'undefined') {

    CKEDITOR.replace(textarea, {
      customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
    });

  }

}
