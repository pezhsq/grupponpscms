var modulo = 'attribute';

$(function() {


  $('#attribute_form_value').on('focus', function() {
    if ($('#attribute_form_value').find('option:selected').data('type') == 'COLOR') {
      $('#attribute_form_value').closest('div').colorpicker('show');
    }
  });


  $('#attribute_form_attributeGroup').on('change', function(e) {
    var $inputGroup = $('#attribute_form_value').closest('div');
    if ($(this).find('option:selected').data('type') == 'COLOR') {
      $inputGroup.find('.input-group-addon').removeClass('hide');
      if ($.trim($('#attribute_form_value').val()) == '') {
        $('#attribute_form_value').val('#000000');
      }
      $('#attribute_form_value').closest('div').colorpicker();
    } else {
      $('#attribute_form_value').closest('div').colorpicker('destroy');

      $inputGroup.find('.input-group-addon').addClass('hide');
      $inputGroup.css('width', '100%');
    }
  });

  $('#attribute_form_attributeGroup').trigger('change');


  /** Datatable della pagina di elenco */
  if ($('#datatable').length) {

    var cols = [];

    var col = {
      'title': 'Modifica',
      'className': 'dt0 dt-body-center',
      'searchable': false
    };
    cols.push(col);

    col = {
      'title': 'Titolo',
      'className': 'dt1',
      searchable: true,
      data: 'titolo'
    };

    cols.push(col);

    col = {
      'title': 'Valore',
      'className': 'dt2',
      searchable: true,
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        patt = new RegExp("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");
        if (patt.test(full.value)) {
          return '<i class="color" style="background-color:' + full.value + '"></i>';
        }
        return full.value

      },
      data: 'value'
    };

    cols.push(col);

    col = {
      'title': 'Gruppo',
      'className': 'dt3',
      searchable: true,
      data: 'gruppo'
    };

    cols.push(col);

    // placeholder cancellazione
    col = { 'className': 'dt4 dt-body-center' };
    cols.push(col);

    var columnDefs = [];

    var columnDef = {
      targets: 0,
      searchable: false,
      orderable: false,
      className: 'dt-body-center',
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
      }
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 1,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        if (full.deleted) {
          var title = _('Recupera:');
          var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
          var title = _('Elimina definitivamente: ');
          ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
        } else {
          var title = 'Elimina: ';
          var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
        }
        return ret;
      }
    };

    columnDefs.push(columnDef);


    $('#datatable').dataTable({
      ajax: {
        "url": "/admin/" + modulo + "/json"
      },
      aaSorting: [[0, 'asc']],
      stateSave: true,
      iDisplayLength: 15,
      responsive: true,
      columns: cols,
      columnDefs: columnDefs,
      createdRow: function(row, data, index) {
        if (data.deleted) {
          $(row).addClass("danger");
        }
      }
    });

  }


});