var modulo = 'magazzino';
$(function() {

    sezione = window.location.getPositionalUrl(2);

    switch (sezione) {

        case null:
            $(document).on('keypress', '.form-control', function(event) {
                if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                    event.preventDefault(); //stop character from entering input
                }
            });

            $(document).on('change', '.form-control', function(e) {
                $(this).closest('tr').addClass('warning');
                var table = $('#datatable').DataTable();
                var cell = table.cell(this.closest('td'));
                console.log(cell);
                cell.data(this.value).draw('page');
                $('#salva_variazioni').removeClass('disabled');
            });

            $(document).on('click', '#salva_variazioni', function(e) {
                e.preventDefault();
                var table = $('#datatable').DataTable();

                var varianti = [];
                var prodotti = [];
                table.rows('.warning').every(function(rowIdx, tableLoop, rowLoop) {
                    var row = this;
                    data = row.data();
                    if (data.type == 'p') {
                        prodotti.push({ 'id': data.id, giacenza: data.giacenza });
                    } else {
                        varianti.push({ 'id': data.id, giacenza: data.giacenza });
                    }
                });

                var data = { 'prodotti': prodotti, 'varianti': varianti };

                HandleBarHelper.lockScreen({ 'message': _('Salvataggio in corso...') });

                $.ajax({
                    type: 'POST',
                    url: '/admin/' + modulo + '/save',
                    data: data,
                    dataType: 'json',
                    success: function(ret_data) {
                        if (ret_data.result) {
                            // success
                            var table = $('#datatable').DataTable();
                            $('#salva_variazioni').addClass('disabled');
                            table.ajax.reload(function() {
                                HandleBarHelper.unlockScreen();
                            });
                        } else {
                            var opts = {
                                'type': 'danger',
                                'titolo': _('Attenzione'),
                                'content': ret_data.errors.join('<br />'),
                                'OK': 'Ok',
                                'callback': null
                            };
                            HandleBarHelper.alert(opts);
                        }
                    }
                });

                console.log(prodotti, varianti);

            });


            /** Datatable della pagina di elenco */
            if ($('#datatable').length) {

                var cols = [];

                col = {
                    'className': 'dt0',
                    searchable: false,
                    data: 'immagine'
                };

                cols.push(col);

                col = {
                    'title': _('Codice', 'magazzino'),
                    'className': 'dt1',
                    data: 'codice'
                };
                cols.push(col);

                col = {
                    'title': 'Titolo',
                    'className': 'dt2',
                    data: 'titolo'
                };
                cols.push(col);

                col = {
                    'title': _('Giacenza', 'magazzino'),
                    'className': 'dt1',
                    orderable: false,
                    data: 'giacenza'
                };

                cols.push(col);

                var columnDefs = [];

                var columnDef = {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        return '<img src="/' + full.immagine + '" alt="' + full.titolo + '" />';
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 1,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    bDeferRender: true,
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        var name = 'product';
                        if (full.type == 'v') {
                            name = 'variant'
                        }
                        name += '[' + full.id + ']';
                        var markup = '<input class="form-control" type="text" name="' + name + '" value="' + full.giacenza + '" />';
                        return markup;
                    }
                };

                columnDefs.push(columnDef);

                $('#datatable').dataTable({
                    ajax: {
                        "url": "/admin/" + modulo + "/json"
                    },
                    aaSorting: [[0, 'asc']],
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs,
                    createdRow: function(row, data, index) {
                        if (data.deleted) {
                            $(row).addClass("danger");
                        }
                    }
                });

            }

            break;

    }


});