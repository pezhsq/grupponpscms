var modulo = 'product';
var $alberoCategorie;
var params = [];
$(function() {
    switch (window.location.getPositionalUrl(2)) {
        case 'new':
        case 'edit':

            /** EVENTI CUSTOM - INIZIO **/
            /* Al cambio dei contenuti relativi agli attributi presenti la tabella
             viene aggiornata di conseguenza */
            $(".attributesList").on("change", function(event) {
                if ($(this).find('.btn-attribute-selected').length == 0) {
                    $('.attributesList').find('.help-block').show();
                    $('.btn-crea-variante').addClass('disabled');
                } else {
                    $('.attributesList').find('.help-block').hide();
                    $('.btn-crea-variante').removeClass('disabled');
                }
            });

            /* Al cambio dei contenuti relativi alle presenti la tabella
             viene aggiornata di conseguenza */
            $(".table-varianti").on("change", function(event) {
                if ($(this).find('.variante').length == 0) {
                    $('.emptyVarianteRow').show();
                } else {
                    $('.emptyVarianteRow').hide();
                }
            });

            /* Al cambio dei contenuti relativi alle presenti la tabella
             viene aggiornata di conseguenza */
            $(".table-caratteristiche").on("change", function(event) {
                if ($(this).find('.feature').length == 0) {
                    $('.emptyFeatureRow').show();
                } else {
                    $('.emptyFeatureRow').hide();
                }
            });

            /* Allo scaturire dell'evento viene inizializzato ckeditor per ogni area di testo
             con classe .ck per la quale non è stato già istanziato */
            $(document).on('ckeditorInitialize', function() {
                $('.ck').each(function() {
                    var idCk = $(this).attr('id');
                    found = false;
                    for(var i in CKEDITOR.instances) {
                        if (i == idCk) {
                            found = true;
                        }
                    }
                    if (!found) {
                        CKEDITOR.replace(this, {
                            customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
                        });
                    }
                });
            });


            $(document).on('change', '.photolist', function(e) {
                $td = $(this).closest('td');
                $td.find('i').remove();
                $td.find('img').remove();
                val = $(this).val();
                if (val) {
                    var src = $('#filesAllegati').find('tr[data-id="' + val[0] + '"] img').attr('src');
                    $('<img class="img-responsive" src="' + src + '"/>').appendTo($td);
                } else {
                    $('<i class="fa fa-picture-o edit-variant-photos"></i>').appendTo($td);
                }
            });
            /** EVENTI CUSTOM - FINE **/

            $(".table-varianti").trigger("change");
            $(document).trigger('ckeditorInitialize');

            if (!$($('#featureRowTemplate').html()).find('#form_features___name___featureGroup option').length) {
                $('.btn-add-feature').attr('disabled', true).addClass('disabled').attr('title', _('Crea delle caratteristiche prima di poterle aggiungere', 'product'));
            }

            /* Click sul bottone che permette di aggiungere una nuova riga alla tabella delle features */
            $('.btn-add-feature').on('click', function(e) {
                e.preventDefault();
                compiled = HandleBarHelper.compile('featureRow');
                var $tbody = $(".table-caratteristiche").find('tbody');
                var markup = compiled();
                markup = markup.replace(/__name__/g, $tbody.find('.feature').length);
                $row = $(markup);
                $row.appendTo($tbody);
                $(".table-caratteristiche").trigger('change');
                $row.find('.group-feature').trigger('change');
            });

            $(document).on('change', '.group-feature', function() {
                if ($(this).val() !== '0') {
                    var $row = $(this).closest('tr');
                    var data = { 'id': $(this).val() };
                    $.ajax({
                        type: 'POST',
                        url: '/admin/feature/get-options',
                        data: data,
                        dataType: 'json',
                        success: function(ret_data) {
                            if (ret_data.result) {
                                var $select = $row.find('.predefined-features');
                                var markup = '<option value="-1">' + _('Seleziona una caratteristica') + '</option>';
                                markup += '<option value="0">' + _('Valore libero') + '</option>';

                                for(var i = 0; i < ret_data.options.length; i++) {
                                    markup += '<option value="' + ret_data.options[i]['id'] + '">' + ret_data.options[i]['label'] + '</option>';
                                }
                                $select.html(markup);

                                if ($select.data('selected') !== null) {
                                    $select.val($select.data('selected'));
                                    $select.trigger('change');
                                    $select.data('selected', null);
                                }
                            } else {
                                var opts = {
                                    'type': 'danger',
                                    'titolo': _('Attenzione'),
                                    'content': ret_data.errors.join('<br />'),
                                    'OK': 'Ok',
                                    'callback': null
                                };
                                HandleBarHelper.alert(opts);
                            }
                        }
                    });
                }
            });

            $('.group-feature').each(function() {
                $(this).trigger('change');
            });

            $('span.tag').each(function() {
                if ($(this).find('input:checked').length) {
                    $(this).removeClass('hide');
                }
            });

            $(document).on('click', '.btn-delete-feature', function(e) {
                e.preventDefault();
                $(this).closest('tr').remove();
                $('.table-caratteristiche').trigger('change');
            });

            $(document).on('change', '.predefined-features', function(e) {
                var $row = $(this).closest('tr');
                if ($(this).val() == '0') {
                    $row.find('.free-feature').removeClass('disabled').removeAttr('disabled');
                } else {
                    $row.find('.free-feature').addClass('disabled').prop('disabled', true);
                }
            });


            /** Scelta della famiglia prodotto, al change vengono mostrati solo gli attributi corrispondenti **/
            $('#form_product_type').on('change', function(e) {
                $selectedOption = $(this).find('option:selected');
                if ($selectedOption) {
                    var ids = $selectedOption.data('ids') + '';
                    var gruppiAttributi = ids.split(',');
                    console.log(gruppiAttributi);
                    $('.panel-attributi').each(function() {
                        if ($.inArray($(this).data('id') + '', gruppiAttributi) == -1) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    });
                }
            });
            $('#form_product_type').trigger('change');


            /**
             * click sui bottoni relativi agli attributi
             */
            $('.btn-attribute').on('click', function(e) {
                    e.preventDefault();
                    var idAttributo = $(this).data('id');
                    var attributiGiaInseriti = [];

                    $('.btn-attribute-selected').each(function() {
                        attributiGiaInseriti.push($(this).data('id'));
                    });

                    if ($.inArray(idAttributo, attributiGiaInseriti) === -1) {

                        var compiled = HandleBarHelper.compile('tag');

                        data = {
                            id: idAttributo,
                            label: $.trim($(this).text())
                        };

                        $(compiled(data)).appendTo($('.attributesList'));
                    }
                    $('.attributesList').trigger('change');
                }
            );

            /**
             * click su bottone per creare variante
             */
            $(document).on('click', '.btn-crea-variante:not(.disabled)', function(e) {

                var compiled = HandleBarHelper.compile('attributeRow');
                var $tbody = $('.table-varianti').find('tbody');
                var index = $tbody.find('tr.variante').length;

                var attributes = [];
                var attributesList = [];

                $('.btn-attribute-selected').each(function() {
                    var attribute = {
                        label: $.trim($(this).text()),
                        id: $(this).data('id')
                    };
                    attributes.push(attribute);
                    attributesList.push(attribute.label);
                });

                var data = {
                    attributes: attributes,
                    attributesList: attributesList.join(', '),
                    index: index
                };

                var markUp = compiled(data);

                var $tr = $(markUp.replace(/__name__/g, $('tr.variante').length));
                $tr.appendTo($tbody);
                // ora abilito gli attributi selezionati nella fase precedente
                $('.btn-attribute-selected').each(function() {
                    $span = $tr.find('.attributiVariante').find('[data-value="' + $(this).data('id') + '"]');
                    $span.find('input').attr('checked', true);
                    $span.removeClass('hide');
                });
                // imposto i valori di default
                var index = 0;
                $tr.find('td:eq(5) .variante-listino').each(function() {
                    $elem = $(this).closest('.form-group');
                    $elem.html($elem.html().replace(/__listino__/g, index));
                    index++;
                });

                console.log($tr.html());
                $tr.find('td:eq(5) input[type="text"]').val('0');
                $tr.find('td:eq(6) input[type="text"]').val('0');

                $tr.find('.prezzo-netto').trigger('change');


                $('.btn-attribute-selected').remove();
                $('.attributesList').trigger('change');
                $(".table-varianti").trigger('change');

                updateVariantiSort();
            });

            /* click sul bottone per fare l'edit fine della variante */
            $(document).on('click', '.btn-edit-variante', function(e) {

                e.preventDefault();

                var $btn = $(this);

                getImages(function(data) {
                    showEditVarianteForm($btn, data);
                });

            });

            /* click sulla variante per selezionare un'immagine per le varianti */
            $(document).on('click', '.variante-thumbnail', function(e) {
                e.preventDefault();
                $thumbnail = $(this);

                if ($thumbnail.hasClass('thumbnail-variante-selected')) {
                    $thumbnail.removeClass('thumbnail-variante-selected');
                } else {
                    $thumbnail.addClass('thumbnail-variante-selected');
                }

            });

            /**
             * click sul bottone che permette l'undelete di una variante
             */
            $(document).on('click', '.btn-restore-variante', function(e) {

                e.preventDefault();
                var $row = $(this).closest('tr');
                $row.find('.deletedat').val('');
                $row.removeClass('deletedVariante');
                $(this).addClass('hide');

            });
            /**
             * click sul bottone per la cancellazione della riga di una variante
             */
            $(document).on('click', '.btn-delete-variante', function(e) {
                e.preventDefault();

                $row = $(this).closest('tr');

                var descrizione = [];


                $row.find('td:eq(2) span.tag:not(.hide)').each(function() {
                    descrizione.push($(this).text());
                });

                descrizione = descrizione.join(', ');

                var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': _('Vuoi rimuovere la variante "<strong>%s</strong>"? ', 'product', $.trim(descrizione)),
                        'OK': 'Ok',
                        'CANCEL': 'Annulla',
                        'onOK': function($modal) {
                            $modal.modal('hide');
                            if ($row.find('td:eq(2) .rowid').length) {

                                $row.find('.deletedat').val(moment().format('YYYY-MM-DD HH:mm:ss'));
                                if ($('body').data('sa')) {
                                    $row.addClass('deletedVariante');
                                    $row.find('.btn-restore-variante').removeClass('hide');
                                } else {
                                    $row.addClass('hide');
                                }
                            } else {
                                $row.remove();
                            }
                            $(".table-varianti").trigger('change');
                        }

                        ,
                        'onCANCEL': null
                    }
                ;
                HandleBarHelper.confirm(opts);

            });


            /**
             * Click sul cestino di un attributo precedentemente selezionato per la creazioen di una variante
             */
            $(document).on('click', '.remove-attribute', function(e) {
                e.preventDefault();
                $(this).closest('.btn-attribute-selected').remove();
                if ($('.attributesList').find('.btn-attribute-selected').length == 0) {
                    $('.attributesList').find('.help-block').show();
                    $('.btn-crea-variante').addClass('disabled');
                }
            });


            $('.panel-accordion').on('shown.bs.collapse', function() {
                $(this).find(".glyphicon-chevron-right").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
            });

            $('.panel-accordion').on('hidden.bs.collapse', function() {
                $(this).find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
            });

            $('#form_tax').on('change', function() {
                $('.prezzo-netto').each(function() {
                    $(this).trigger('change');
                });
            });

            $('.prezzo-ivato').on('change', function() {
                var $row = $(this).closest('.listino,.rowMarketPlace');
                var taxMultiplier = $('#form_tax').find('option:selected').data('multiplier');
                var $prezzoNetto = $row.find('.prezzo-netto');
                var prezzo = $(this).val().replace(',', '.');
                var valore = Math.round(prezzo / taxMultiplier * 1000000) / 1000000;
                valore = valore.toString().replace('.', ',');
                $prezzoNetto.val(valore);
            });

            $('.prezzo-netto').on('change', function() {
                var $row = $(this).closest('.listino,.rowMarketPlace');
                var taxMultiplier = $('#form_tax').find('option:selected').data('multiplier');
                var $prezzoIvato = $row.find('.prezzo-ivato');
                var prezzo = $(this).val().replace(',', '.');
                var valore = Math.round(prezzo * taxMultiplier * 1000000) / 1000000;
                valore = valore.toString().replace('.', ',');
                $prezzoIvato.val(valore);
            });

            $(document).on('change', '.prezziVariante .prezzo-ivato,.prezziVarianteMarketPlace .prezzo-ivato', function(e) {
                var $formGroup = $(this).closest('.form-group');
                var $row = $formGroup.parent();
                var taxMultiplier = $('#form_tax').find('option:selected').data('multiplier');
                var $prezzoNetto = $row.find('.prezzo-netto');
                var prezzo = $(this).val().replace(',', '.');
                var valore = Math.round(prezzo / taxMultiplier * 1000000) / 1000000;
                valore = valore.toString().replace('.', ',');
                $prezzoNetto.val(valore);
            });

            $(document).on('change', '.prezziVariante .prezzo-netto, .prezziVarianteMarketPlace .prezzo-netto', function(e) {
                var $formGroup = $(this).closest('.form-group');
                var $row = $formGroup.parent();
                var taxMultiplier = $('#form_tax').find('option:selected').data('multiplier');
                var $prezzoIvato = $row.find('.prezzo-ivato');
                var prezzo = $(this).val().replace(',', '.');
                var valore = Math.round(prezzo * taxMultiplier * 1000000) / 1000000;
                valore = valore.toString().replace('.', ',');
                $prezzoIvato.val(valore);
            });

            $('.prezziVariante .prezzo-netto').trigger('change');
            $('.prezziVarianteMarketPlace .prezzo-netto').trigger('change');
            $('.rowMarketPlace .prezzo-netto').trigger('change');

            $('.marketPlaceEnabler').on('change', function(e) {
                if ($(this).val() == '0') {
                    $(this).closest('.rowMarketPlace').find('.prezzo-market-place').addClass('disabled').prop('disabled', true);
                } else {
                    $(this).closest('.rowMarketPlace').find('.prezzo-market-place').removeClass('disabled').removeAttr('disabled');
                }
            });
            $('.marketPlaceEnabler').trigger('change');

            $('#attribute_form_attributeGroup').on('change', function(e) {
                var $inputGroup = $('#attribute_form_value').closest('div');
                if ($(this).find('option:selected').data('type') == 'COLOR') {
                    $inputGroup.find('.input-group-addon').removeClass('hide');
                    if ($.trim($('#attribute_form_value').val()) == '') {
                        $('#attribute_form_value').val('#000000');
                    }
                    $('#attribute_form_value').closest('div').colorpicker();
                    $('#attribute_form_value').on('focus', function() {
                        $('#attribute_form_value').closest('div').colorpicker('show');
                    });
                } else {
                    $inputGroup.find('.input-group-addon').addClass('hide');
                    $inputGroup.css('width', '100%');
                }
            });

            // refresh immagini
            refreshPhotoListVariants();

            $('#attribute_form_attributeGroup').trigger('change');

            // ordinamento varianti
            $('.table-varianti tbody').sortable({
                    handle: ".btn-sort",
                    placeholder: {
                        element: function(currentItem) {
                            return '<tr class="variante-sortable-placeholder"><td colspan="8"><div></div></td></tr>';
                        },
                        update: function(container, p) {
                            return;
                        }
                    },
                    start: function(event, ui) {
                        var height = ui.helper.find('td:first').height();
                        $('.variante-sortable-placeholder div').height(height);
                    }
                    ,
                    stop: function(event, ui) {
                        updateVariantiSort();
                    }
                }
            );


            // change sul tipo di corriere che potrà spedire il prodotto
            $('#form_allCorrieri').on('change', function(e) {
                if ($(this).val() == 1) {
                    $('.corrieri_scelta_singola').slideUp();
                } else {
                    $('.corrieri_scelta_singola').slideDown();
                }
            });
            $('#form_allCorrieri').trigger('change');

            $alberoCategorie = $(".js-tree.category_list");

            $.jstree.defaults.checkbox.three_state = false;

            $alberoCategorie.jstree({
                'core': {
                    'check_callback': true,
                    'data': {
                        'url': '/admin/categorie-prodotti/json-tree',
                        'type': 'post',
                        'dataType': 'json',
                        "error": function(r) {

                        },
                        'data': function(node) {
                            return {
                                'id': node.id
                            };
                        },


                    }
                },
                "force_text": true,
                'plugins': ["search", "wholerow", "checkbox"],
                'checkbox': {
                    three_state: false, // to avoid that fact that checking a node also check others
                    whole_node: false,  // to avoid checking the box just clicking the node
                    tie_selection: false // for checking without selecting and selecting without checking
                }
            });

            $alberoCategorie.on('uncheck_node.jstree', function(e, data) {
                updateCategorie($alberoCategorie.jstree('get_checked'));
            });
            $alberoCategorie.on('check_node.jstree', function(e, data) {
                updateCategorie($alberoCategorie.jstree('get_checked'));
            });
            $alberoCategorie.on('loaded.jstree', function(e, data) {
                $('.categorie-ecommerce').closest('.form-group').hide();
                updateTree();
                var opened = $alberoCategorie.jstree('get_checked');
                for(var i = 0; i < opened.length; i++) {
                    $alberoCategorie.jstree()._open_to(opened[i]);
                }
            });

            // tags
            if ($('.tags').length) {
                $('.tags').tagsInput(
                    {
                        'height': '100px',
                        'width': '900px',
                        'interactive': true,
                        'defaultText': 'aggiungi un tag e premi [TAB]',
                        'delimiter': ',',   // Or a string with a single delimiter. Ex: ';'
                        'removeWithBackspace': true,
                    }
                );
            }

            var $autocompleteRedirect = $("#form_redirectionHelper").devbridgeAutocomplete({
                serviceUrl: '/admin/' + modulo + '/search-redirect',
                onSearchStart: function(params) {
                    var exclude = [$('form').data('id')];

                    $(this).devbridgeAutocomplete('setOptions',
                        {
                            'params': {
                                'exclude': exclude.join(',')
                            }
                        }
                    );
                },
                onSelect: function(suggestion) {
                    $('#form_targetId').val(suggestion.data.id);
                }
            });

            $autocompleteRedirect.devbridgeAutocomplete('setOptions',
                {
                    'params': {
                        'exclude': $('form').data('id')
                    }
                }
            );

            $('#form_typeOfRedirect').on('change', function() {
                $('#form_redirectionHelper').val('');
                $('#form_targetId').val(0);
                if ($(this).val() == '404') {
                    $('#form_redirectionHelper').addClass('disabled').prop('disabled', true);
                } else {
                    $('#form_redirectionHelper').removeClass('disabled').removeAttr('disabled');
                }

                switch ($(this).val()) {
                    case '301P':
                    case '302P':
                        $autocompleteRedirect.devbridgeAutocomplete('setOptions', { 'serviceUrl': '/admin/' + modulo + '/search-redirect' });
                        break;
                    case '301C':
                    case '302C':
                        $autocompleteRedirect.devbridgeAutocomplete('setOptions', { 'serviceUrl': '/admin/categorie-prodotti/search-redirect' });
                }

            });

            if ($('#form_typeOfRedirect').val() == '404') {
                $('#form_redirectionHelper').val('');
                $('#form_targetId').val(0);
                $('#form_redirectionHelper').addClass('disabled').prop('disabled', true);
            }


            var $autocomplete = $("#form_correlatiAutocomplete").devbridgeAutocomplete({
                serviceUrl: '/admin/' + modulo + '/search',
                onSearchStart: function(params) {
                    console.log('onSearchStart');
                    var exclude = [$('form').data('id')];

                    $('.correlato').each(function() {
                        exclude.push($(this).data('id'));
                    });

                    $(this).devbridgeAutocomplete('setOptions',
                        {
                            'params': {
                                'exclude': exclude.join(',')
                            }
                        }
                    );
                },
                onSelect: function(suggestion) {
                    var compiled = HandleBarHelper.compile('correlato');
                    markup = compiled(suggestion.data);
                    $(markup).appendTo('.correlatiScelti');
                    $('#form_correlatiAutocomplete').val('');
                    updateCorrelati();
                }
            });

            $autocomplete.devbridgeAutocomplete('setOptions',
                {
                    'params': {
                        'exclude': $('form').data('id')
                    }
                }
            );

            $(document).on('shown.bs.tab', function(e) {
                $(document).trigger('ckeditorInitialize');
                switch ($(e.target).data('target')) {
                    case '.dati-posizionamento':
                        $('.correlatiScelti').width($('#form_correlatiAutocomplete').outerWidth(false) - 24);
                        break;

                }
            });

            $(document).on('click', '.btn-remove-correlato', function(e) {
                e.preventDefault();
                $(this).closest('.correlato').remove();
                updateCorrelati();
            });


            break;
        case null:
            /** Datatable della pagina di elenco */
            if ($('#datatable').length) {

                var cols = [];

                var col = {
                    'title': 'Mod.',
                    'className': 'dt0 dt-body-center',
                    'searchable': false
                };
                cols.push(col);

                col = {
                    'className': 'dt1',
                    searchable: false,
                    data: 'immagine'
                };

                cols.push(col);

                col = {
                    'title': _('Codice', 'product'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'codice'
                };

                cols.push(col);

                col = {
                    'title': _('Codice Produttore', 'product'),
                    'className': 'dt3',
                    searchable: true,
                    data: 'codice_produttore'
                };

                cols.push(col);

                col = {
                    'title': 'Titolo',
                    'className': 'dt4',
                    searchable: true,
                    data: 'titolo'
                };

                cols.push(col);

                col = {
                    'title': _('Varianti', 'product'),
                    'className': 'dt5',
                    searchable: true,
                    data: 'conta_varianti'
                };

                cols.push(col);

                col = {
                    'title': _('Categorie', 'product'),
                    'className': 'dt6',
                    searchable: true,
                    data: 'categorie'
                };

                cols.push(col);

                col = {
                    'title': _('url', 'product'),
                    'className': 'dt7 never',
                    searchable: true,
                    data: 'url'
                };

                cols.push(col);

                col = {
                    'title': _('Tags', 'product'),
                    'className': 'dt7',
                    searchable: true,
                    data: 'tags'
                };

                cols.push(col);

                col = {
                    searchable: true,
                    data: 'isEnabled'
                };

                cols.push(col);

                col = {
                    searchable: true,
                    data: 'brand'
                };

                cols.push(col);

                // placeholder cancellazione
                col = { 'className': 'dt8 dt-body-center' };
                cols.push(col);
                col = { 'className': 'dt9 dt-body-center' };
                cols.push(col);

                var columnDefs = [];

                var columnDef = {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                    }
                };

                columnDefs.push(columnDef);

                var columnDef = {
                    targets: 1,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        return '<img src="/' + full.immagine + '" alt="' + full.titolo + '" />';
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 2,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        var icon = 'star-o';
                        var title = 'Recensioni';
                        var ret = full.recensioni + '&nbsp;<a href="/admin/' + modulo + '/recensioni/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                        ret += '<i class="fa fa-' + icon + '"></i>';
                        ret += '</a>';
                        return ret;
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 1,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        if (full.deleted) {
                            var title = _('Recupera:');
                            var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                        } else {
                            var title = 'Elimina: ';
                            var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        }
                        return ret;
                    }
                };

                columnDefs.push(columnDef);


                $('#datatable').dataTable({
                    ajax: {
                        "url": "/admin/" + modulo + "/json"
                    },
                    aaSorting: [[0, 'asc']],
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs,
                    createdRow: function(row, data, index) {
                        if (data.deleted) {
                            $(row).addClass("danger");
                        }
                    }
                });

            }


            break;

        case 'sort':

            $('#categoria').on('change', function(e) {
                e.preventDefault();
                if ($(this).val() == 0) {
                    $('.sortList').remove();
                    $('.help-block-default').show();
                    $('.help-block-assente').addClass('hide');
                } else {

                    HandleBarHelper.lockScreen();
                    $.ajax({

                        type: 'POST',
                        url: '/admin/' + modulo + '/getproducts/' + $(this).val(),
                        dataType: 'json',
                        success: function(ret_data) {
                            if (ret_data.result) {
                                HandleBarHelper.unlockScreen();

                                $('.help-block-default').hide();
                                $('.sortList').remove();

                                if (ret_data.data.items.length > 1) {
                                    compiled = HandleBarHelper.compile('sort');
                                    $(compiled(ret_data.data)).appendTo('.toSort');
                                    $('.sortList ul').sortable();
                                    $('.btn-main-submit').removeClass('disabled');
                                    $('.help-block-assente').addClass('hide');
                                } else {
                                    $('.help-block-assente').removeClass('hide');
                                }

                            } else {
                                var opts = {
                                    'type': 'danger',
                                    'titolo': _('Attenzione'),
                                    'content': ret_data.errors.join('<br />'),
                                    'OK': 'Ok',
                                    'callback': null
                                };
                                HandleBarHelper.alert(opts);
                            }
                        }
                    });
                }
            });

            $('.save-sort').on('click', function(e) {
                e.preventDefault();
                var sorted = $('.ui-sortable').sortable("toArray");

                var data = {
                    'sorted': sorted,
                    'category': $('#categoria').val()
                };
                HandleBarHelper.lockScreen();

                $.ajax({
                    type: 'POST',
                    url: '/admin/' + modulo + '/save-sort',
                    data: data,
                    dataType: 'json',
                    success: function(ret_data) {
                        if (ret_data.result) {
                            HandleBarHelper.unlockScreen();
                            var params = {
                                'closeText': _('Chiudi'),
                                icon: 'check-mark-2',
                                theme: 'materialish',
                                color: 'lilrobot'
                            };

                            $.notific8(_('Ordinamento per la categoria "%s" salvato', 'default', $('#categoria option:selected').text()), params);
                        } else {
                            var opts = {
                                'type': 'danger',
                                'titolo': _('Attenzione'),
                                'content': ret_data.errors.join('<br />'),
                                'OK': 'Ok',
                                'callback': null
                            };
                            HandleBarHelper.alert(opts);
                        }
                    }
                });

            });

            break;

        case "recensioni":
            /** Datatable della pagina di elenco recensioni */
            if ($('#datatableRecensioni').length) {

                var cols = [];

                // col = {
                //     'title': 'Nome',
                //     'className': 'dt2',
                //     searchable: true,
                //     data: 'nome'
                // };
                //
                // cols.push(col);
                col = {
                    'title': 'Cliente',
                    'className': 'dt2',
                    searchable: true,
                    data: 'cliente'
                };

                cols.push(col);
                col = {
                    'title': 'Valore',
                    'className': 'dt2',
                    searchable: true,
                    data: 'valore'
                };

                cols.push(col);

                col = {
                    'title': 'Messaggio',
                    'className': 'dt2',
                    searchable: true,
                    data: 'messaggio'
                };

                cols.push(col);
                col = { 'className': 'dt-body-center' };
                // placeholder toggle
                cols.push(col);

                // placeholder cancellazione
                cols.push(col);

                var columnDefs = [];

                columnDef = {
                    targets: cols.length - 2,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        var icon = 'check-square-o';
                        var title = 'Disabilita: ';
                        if (!full.isEnabled) {
                            var icon = 'square-o';
                            var title = 'Abilita: ';
                        }
                        var ret = '<a href="/admin/' + modulo + '/toggle-recensioni-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                        ret += '<i class="fa fa-' + icon + '"></i>';
                        ret += '</a>';
                        return ret;
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 1,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.nome + ' (' + full.email + ')';
                        console.log(full);
                        if (full.deleted) {
                            var title = _('Recupera:');
                            var ret = '<a href="/admin/' + modulo + '/restore-recensione/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                            var title = _('Elimina definitivamente: ');
                            ret += '<a href="/admin/' + modulo + '/delete-recensione/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        } else {
                            var title = 'Elimina: ';
                            var ret = '<a href="/admin/' + modulo + '/delete-recensione/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        }
                        return ret;
                    }
                };

                columnDefs.push(columnDef);


                $('#datatableRecensioni').dataTable({
                    ajax: {
                        "url": "/admin/" + modulo + "/recensioni/json/" + $('#datatableRecensioni').data('id')
                    },
                    aaSorting: [[2, 'asc']],
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs,
                    createdRow: function(row, data, index) {
                        if (data.deleted) {
                            $(row).addClass("danger");
                        }
                    }
                });

            }

    }


    /** Gestione dell'albero delle categorie **/

});


function showEditVarianteForm($btn, images) {
    var $tr = $btn.closest('tr');

    var settings = {
        id: 'modalVariante',
        title: _('Modifica variante'),
        size: ' modal-lg',
        data: {},
        onShow: function($modal) {

            $modal.find('.close').hide();
            $modal.find('.btn-default').hide();

            var $full = $modal.find('#dettaglioVarianteForm')

            $tr.find('.codici').removeClass('hide').appendTo($full);
            $tr.find('.dimensioni').removeClass('hide').appendTo($full);

            $tr.find('.giacenze').appendTo($full);
            $tr.find('.prezzi').appendTo($full);

            $('<div class="photosContainer"><div class="photos"></div></div>').appendTo($full);

            $photos = $('.photos');

            var fotoSelezionate = $tr.find('.photolist').val();

            for(var i = 0; i < images.length; i++) {
                var className = '';
                if ($.inArray(images[i]['id'] + '', fotoSelezionate) !== -1) {
                    className = ' thumbnail-variante-selected';
                }
                $('<div style="background-image:url(\'' + images[i]['path'] + '\')" data-id="' + images[i]['id'] + '" class="variante-thumbnail' + className + '" ></div>').appendTo($photos);
            }

            $photos.css({ 'width': $photos.find('.variante-thumbnail').length * $photos.find('.variante-thumbnail:first').outerWidth(true) });

            $tr.find('.translations').removeClass('hide').appendTo($full);


            $descrizioni = $modal.find('.a2lix_translations');

            $descrizioni.on('shown.bs.tab', function(e) {
                var target = $(e.target).data('target');
                var $tab = $(target);
                var $textarea = $tab.find('textarea');
                if (typeof(CKEDITOR.instances[$textarea.attr('id')]) == 'undefined') {
                    CKEDITOR.replace($textarea.get(0), {
                        customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
                    });
                }
            });

            $textarea = $descrizioni.find('.active textarea');


            CKEDITOR.replace($textarea.get(0), {
                customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
            });

        },
        onOk: function($Modal) {

            var $firstTd = $tr.find('td:eq(1)');

            $descrizioni.find('textarea').each(function() {
                var $textarea = $(this)
                var id = $textarea.get('id');
                if (typeof(CKEDITOR.instances[$textarea.attr('id')]) != 'undefined') {
                    CKEDITOR.instances[$textarea.attr('id')].destroy();
                }
            });
            $Modal.find('.translations').addClass('hide').appendTo($firstTd);

            $Modal.find('.codici').addClass('hide').appendTo($firstTd);
            $Modal.find('.dimensioni').addClass('hide').appendTo($firstTd);
            $Modal.find('.giacenze').appendTo($tr.find('td:eq(6)'));
            $Modal.find('.prezzi').appendTo($tr.find('td:eq(5)'));

            var thumbSelezionate = [];
            $Modal.find('.thumbnail-variante-selected').each(function() {
                thumbSelezionate.push($(this).data('id'));
            });

            console.log(thumbSelezionate);
            console.log($tr.find('.photolist'));

            $photolist = $tr.find('.photolist');

            $photolist.val(thumbSelezionate);
            $photolist.trigger('change');

            $Modal.modal('hide');

        },
        onCancel: function($Modal) {
            var $firstTd = $tr.find('td:eq(1)');

            $descrizioni.find('textarea').each(function() {
                var $textarea = $(this)
                var id = $textarea.get('id');
                if (typeof(CKEDITOR.instances[$textarea.attr('id')]) !== 'undefined') {
                    CKEDITOR.instances[$textarea.attr('id')].destroy();
                }
            });
            $Modal.find('.translations').addClass('hide').appendTo($firstTd);

            $Modal.find('.codici').addClass('hide').appendTo($firstTd);
            $Modal.find('.dimensioni').addClass('hide').appendTo($firstTd);
            $Modal.find('.giacenze').appendTo($tr.find('td:eq(4)'));
            $Modal.find('.prezzi').appendTo($tr.find('td:eq(3)'));

        },
        cancelLbl: _('Annulla'),
        okLbl: _('Ok'),
        modalOptions: {
            backdrop: 'static',
            keyboard: false
        },
        parentElement: 'body'
    };

    HandleBarHelper.modalForm(settings);

}

var onUploadedFiles = function() {
    refreshPhotoListVariants();
}

function refreshPhotoListVariants() {

    getImages(function(data) {

        var options = '';

        for(var i = 0; i < data.length; i++) {
            options += '<option value="' + data[i]['id'] + '">' + data[i]['alt'] + '</option>';
        }

        $('.photolist').each(function() {
            // qui devo salvare gli eventuali elementi già selezionati
            var val = $(this).val();
            // poi metto i nuovi valori
            $(this).html(options);

            // riseleziono gli elementi salvati in precedenza
            $(this).val(val);


        });

        $('.photolist').trigger('change');

    });

}


function getImages(callback) {

    $.ajax({
        type: 'GET',
        url: '/admin/_attachments/' + modulo + '/get-images/' + $('form').data('id'),
        dataType: 'json',
        success: function(ret_data) {
            if (ret_data.result) {
                callback(ret_data.data);
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function updateCategorie(checkedNodes) {
    $('.categorie-ecommerce').find('input').each(function() {
        if ($.inArray($(this).val(), checkedNodes) != -1) {
            $(this).prop('checked', true);
        } else {
            $(this).removeAttr('checked');
        }
    });
}

function updateTree() {
    var checkedCat = [];
    $('.categorie-ecommerce input:checked').each(function() {
        checkedCat.push($(this).val());
    });
    $.jstree.reference('.js-tree.category_list').check_node(checkedCat);
}

function updateCorrelati() {
    var correlati = [];
    $('.correlato').each(function(e) {
        correlati.push($(this).data('id'));
    });
    $('#form_correlati').val(correlati.join(','));
}

function updateVariantiSort() {
    $('.table-varianti tbody').find('.sort-variante').each(function(index) {
        $(this).val(index);
    });
}