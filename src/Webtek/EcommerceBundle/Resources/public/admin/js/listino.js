var modulo = 'listino';
var statoInizialeDefault;
$(function() {

  statoInizialeDefault = $('#listino_form_isDefault').val();

  $('#listino_form_isDefault').on('change', function() {

    if ($(this).val() == 0) {
      if (statoInizialeDefault != '0') {
        $(this).val('1');
      }
    } else {
      var opts = {
        'type': 'danger',
        'titolo': _('Attenzione'),
        'content': _('Sei sicuro di voler impostare questo listino come default?', 'listino'),
        'OK': 'Ok',
        'CANCEL': 'Annulla',
        'onOK': function($modal) {
          $modal.modal('hide');
        },
        'onCANCEL': function($modal) {
          $('#listino_form_isDefault').val('0');
          $modal.modal('hide');

        }
      };
      HandleBarHelper.confirm(opts);
    }

  });

  /** Datatable della pagina di elenco */
  if ($('#datatable').length) {

    var cols = [];

    var col = {
      'title': 'Modifica',
      'className': 'dt0 dt-body-center',
      'searchable': false
    };
    cols.push(col);

    var col = {
      'title': 'Titolo',
      'className': 'dt1',
      data: 'titolo'
    };
    cols.push(col);

    // isDefault
    col = { 'className': 'dt2 dt-body-center' }
    cols.push(col);

    // placeholder cancellazione
    col = { 'className': 'dt3 dt-body-center' }
    cols.push(col);

    var columnDefs = [];

    var columnDef = {
      targets: 0,
      searchable: false,
      orderable: false,
      className: 'dt-body-center',
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
      }
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 2,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        if (full.isDefault) {
          var title = _('Listino predefinito: ');
          ret = '<a href="#" class="btn btn-success disabled btn-xs"><i title="' + title + '" class="fa fa-star"></i></a>';
        } else {
          var title = _('Rendi listino predefinito: ');
          var ret = '<a href="/admin/' + modulo + '/toggle-default/' + full.id + '" class="btn btn-success btn-xs btn-set-default"';
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-star-o"></i></a>';
        }
        return ret;
      }
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 1,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        if (full.deleted) {
          var title = _('Recupera:');
          var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
          var title = _('Elimina definitivamente: ');
          ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
        } else {
          var title = 'Elimina: ';
          var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
        }
        return ret;
      }
    };

    columnDefs.push(columnDef);


    $('#datatable').dataTable({
      ajax: {
        "url": "/admin/" + modulo + "/json"
      },
      aaSorting: [[1, 'asc']],
      stateSave: true,
      iDisplayLength: 15,
      responsive: true,
      columns: cols,
      columnDefs: columnDefs,
      createdRow: function(row, data, index) {
        if (data.deleted) {
          $(row).addClass("danger");
        }
      }
    });

  }

});