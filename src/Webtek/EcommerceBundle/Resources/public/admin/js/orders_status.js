var modulo = 'orders-status';

$(function() {

    /** Datatable della pagina di elenco */
    if ($('#datatable').length) {

        var cols = [];

        var col = {
            'title': 'Modifica',
            'className': 'dt0 dt-body-center',
            'searchable': false
        };
        cols.push(col);

        var col = {
            'title': 'Codice',
            'className': 'dt1',
            data: 'codice'
        };
        cols.push(col);

        var col = {
            'title': 'Titolo',
            'className': 'dt2',
            data: 'titolo'
        };
        cols.push(col);

        var columnDefs = [];

        var columnDef = {
            targets: 0,
            searchable: false,
            orderable: false,
            className: 'dt-body-center',
            render: function(data, type, full, meta) {
                var toString = full.titolo;
                return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
            }
        };

        columnDefs.push(columnDef);


        $('#datatable').dataTable({
            ajax: {
                "url": "/admin/" + modulo + "/json"
            },
            aaSorting: [[0, 'asc']],
            stateSave: true,
            iDisplayLength: 15,
            responsive: true,
            columns: cols,
            columnDefs: columnDefs,
            createdRow: function(row, data, index) {
                if (data.deleted) {
                    $(row).addClass("danger");
                }
            }
        });

    }


});