var modulo = 'order';

$(function() {

    switch (window.location.getPositionalUrl(2)) {
        case null:
            /** Datatable della pagina di elenco */
            if ($('#datatable').length) {

                var cols = [];

                var col = {
                    'title': 'Modifica',
                    'className': 'dt0 dt-body-center',
                    'searchable': false
                };
                cols.push(col);

                col = {
                    'title': _('Numero', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'id'
                };

                cols.push(col);

                col = {
                    'title': _('Fatturazione', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'billing'
                };

                cols.push(col);

                col = {
                    'title': _('Indirizzo di consegna', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'delivery'
                };

                cols.push(col);

                col = {
                    'title': _('Coupon', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'coupon'
                };

                cols.push(col);

                col = {
                    'title': _('Consegna a mezzo', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'deliverBy'
                };

                cols.push(col);

                col = {
                    'title': _('Totale', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'totale'
                };

                cols.push(col);

                col = {
                    'title': _('Metodo di pagamento', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'payment'
                };

                cols.push(col);

                col = {
                    'title': _('Pagato', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: '-'
                };

                cols.push(col);

                col = {
                    'title': _('Stato', 'order'),
                    'className': 'dt2',
                    searchable: true,
                    data: 'status'
                };

                cols.push(col);


                col = {
                    'title': 'Creato',
                    'className': 'dt6',
                    searchable: true,
                    data: 'createdAt'
                };

                cols.push(col);

                col = {
                    'title': 'Ultima modifica',
                    'className': 'dt7',
                    searchable: true,
                    data: 'updatedAt'
                };

                cols.push(col);

                var columnDefs = [];

                var columnDef = {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 4,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        if (full.pagato) {
                            var ret = full.data_pagamento;
                            if (full.transactionId) {
                                ret += '<br />';
                                ret += full.transactionId;
                            }
                            return ret;
                        } else {
                            return _('No');
                        }
                    }
                };

                columnDefs.push(columnDef);


                $('#datatable').dataTable({
                    ajax: {
                        "url": "/admin/" + modulo + "/json"
                    },
                    aaSorting: [[1, 'desc']],
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs,
                    createdRow: function(row, data, index) {
                        if (data.deleted) {
                            $(row).addClass("danger");
                        }
                    }
                });

            }
            break;
        case 'statistiche':

            $.ajax({
                type: 'POST',
                url: '/admin/' + modulo + '/statistiche-anno',
                dataType: 'json',
                success: function(ret_data) {
                    if (ret_data.result) {
                        Morris.Bar({
                            element: 'ordini-annui',
                            data: ret_data.data,
                            xkey: 'y',
                            ykeys: ['effettuati', 'pagati'],
                            labels: [_('Ordini effettuati', 'orders'), _('Ordini pagati', 'orders')]
                        });
                    } else {
                        var opts = {
                            'type': 'danger',
                            'titolo': _('Attenzione'),
                            'content': ret_data.errors.join('<br />'),
                            'OK': 'Ok',
                            'callback': null
                        };
                        HandleBarHelper.alert(opts);
                    }
                }
            });

            $.ajax({
                type: 'POST',
                url: '/admin/' + modulo + '/statistiche-mese',
                dataType: 'json',
                success: function(ret_data) {
                    if (ret_data.result) {
                        Morris.Bar({
                            element: 'ordini-mensili',
                            data: ret_data.data,
                            xkey: 'y',
                            ykeys: ['effettuati', 'pagati'],
                            labels: [_('Ordini effettuati', 'orders'), _('Ordini pagati', 'orders')]
                        });
                    } else {
                        var opts = {
                            'type': 'danger',
                            'titolo': _('Attenzione'),
                            'content': ret_data.errors.join('<br />'),
                            'OK': 'Ok',
                            'callback': null
                        };
                        HandleBarHelper.alert(opts);
                    }
                }
            });

            columnDefs.push(columnDef);


            $('#datatable').dataTable({
                ajax: {
                    "url": "/admin/" + modulo + "/json"
                },
                stateSave: true,
                iDisplayLength: 15,
                responsive: true,
                columns: cols,
                columnDefs: columnDefs,
                createdRow: function(row, data, index) {
                    if (data.deleted) {
                        $(row).addClass("danger");
                    }
                }
            });

            break;
    }

});

/** FINE document.ready **/