var modulo = 'setup_ecommerce';
$(function() {

  $(".ck").each(function(index, el) {
    attivaCkEditor(el);
  });


});

/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {
  var id = textarea.id;
  // var data=$(textarea).val();

  if (typeof(CKEDITOR.instances[id]) == 'undefined') {
    CKEDITOR.replace(textarea, {
      customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
    });
  } else {
    CKEDITOR.instances[id].destroy();
    CKEDITOR.replace(textarea, {
      customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
    });
  }

}

function destroyCkEditor() {
  for(var instance in CKEDITOR.instances) {
    CKEDITOR.instances[instance].destroy();
  }

}
