var modulo = 'market-place';

$(function() {

    /** Datatable della pagina di elenco */
    if ($('#datatable').length) {

        var cols = [];

        var col = {
            'title': 'Modifica',
            'className': 'dt0 dt-body-center',
            'searchable': false
        };
        cols.push(col);

        var col = {
            'title': 'Nome',
            'className': 'dt1',
            data: 'nome'
        };
        cols.push(col);

        // placeholder toggle
        var col = {
            'className': 'dt2 dt-body-center'
        };
        cols.push(col);

        // placeholder cancellazione
        var col = {
            'className': 'dt3 dt-body-center'
        };
        cols.push(col);

        var columnDefs = [];

        var columnDef = {
            targets: 0,
            searchable: false,
            orderable: false,
            className: 'dt-body-center',
            render: function(data, type, full, meta) {
                var toString = full.titolo;
                return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
            }
        };

        columnDefs.push(columnDef);

        columnDef = {
            targets: cols.length - 2,
            searchable: false,
            className: 'dt-body-center',
            orderable: false,
            render: function(data, type, full, meta) {
                console.log(full);
                var toString = full.nome;
                var icon = 'check-square-o';
                var title = 'Disabilita: ';
                if (!full.isEnabled) {
                    var icon = 'square-o';
                    var title = 'Abilita: ';
                }
                var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                ret += '<i class="fa fa-' + icon + '"></i>';
                ret += '</a>';
                return ret;
            }
        };
        columnDefs.push(columnDef);

        columnDef = {
            targets: cols.length - 1,
            searchable: false,
            className: 'dt-body-center',
            orderable: false,
            render: function(data, type, full, meta) {
                var toString = full.nome;
                if (full.deleted) {
                    var title = _('Recupera:');
                    var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                    ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                    var title = _('Elimina definitivamente: ');
                    ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                    if (parseInt(full.cancellabile, 10) == 0) {
                        ret += ' disabled';
                    }
                    ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                } else {
                    var title = 'Elimina: ';
                    var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                    if (parseInt(full.cancellabile, 10) == 0) {
                        ret += ' disabled';
                    }
                    ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                }
                return ret;
            }
        };

        columnDefs.push(columnDef);


        $('#datatable').dataTable({
            ajax: {
                "url": "/admin/" + modulo + "/json"
            },
            aaSorting: [[2, 'asc']],
            stateSave: true,
            iDisplayLength: 15,
            responsive: true,
            columns: cols,
            columnDefs: columnDefs,
            createdRow: function(row, data, index) {
                if (data.deleted) {
                    $(row).addClass("danger");
                }
            }
        });

    }


});