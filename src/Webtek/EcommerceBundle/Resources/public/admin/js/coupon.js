var modulo = 'coupon';

$(function() {

    sezione = window.location.getPositionalUrl(2);

    switch (sezione) {

        case 'new':
        case 'edit':

            var $illimitatoSelect = $('#coupon_form_illimitato');
            var $typeSelect = $('#coupon_form_type');
            var $allProduct = $('#coupon_form_allProducts');

            /** On change della select relativa al numero di elementi utilizzabili **/
            $allProduct.on('change', function(e) {
                var $categorie = $('#coupon_form_categorie');
                if (parseInt($(this).val(), 10) == 1) {
                    $categorie.val([]);
                    $categorie.closest('.form-group').addClass('hide');
                } else {
                    $categorie.closest('.form-group').removeClass('hide');
                }
            });

            /** On change della select relativa al numero di elementi utilizzabili **/
            $illimitatoSelect.on('change', function(e) {
                var $quantitaCoupon = $('#coupon_form_available');
                if (parseInt($(this).val(), 10) == 1) {
                    $quantitaCoupon.val(10000);
                    $quantitaCoupon.closest('.form-group').addClass('hide');
                } else {
                    $quantitaCoupon.closest('.form-group').removeClass('hide');
                }
            });

            /** On change della select del tipo di coupon **/
            $typeSelect.on('change', function(e) {
                var $couponFormValueInput = $('#coupon_form_value');
                var $icon = $couponFormValueInput.closest('.form-group').find('.fa');
                if (parseInt($(this).val(), 10) == 1) {
                    $icon.attr('class', 'fa fa-eur');
                } else {
                    $icon.attr('class', 'fa fa-percent');
                }
            });

            $typeSelect.trigger('change');
            $illimitatoSelect.trigger('change');
            $allProduct.trigger('change');
            break;

        case null:
            /** Datatable della pagina di elenco */
            if ($('#datatable').length) {

                var cols = [];

                var col = {
                    'title': 'Modifica',
                    'className': 'dt0 dt-body-center',
                    'searchable': false
                };
                cols.push(col);

                col = {
                    'title': 'Code',
                    'className': 'dt2',
                    searchable: true,
                    data: 'code'
                };

                cols.push(col);

                col = {
                    'title': 'Titolo',
                    'className': 'dt2',
                    searchable: true,
                    data: 'titolo'
                };

                cols.push(col);

                col = {
                    'title': 'Valore',
                    'className': 'dt3',
                    searchable: true,
                    data: 'value'
                };

                cols.push(col);

                col = {
                    'title': 'Minimo ordine',
                    'className': 'dt3',
                    searchable: true,
                    data: 'minimoordine'
                };

                cols.push(col);

                col = {
                    'title': 'Prodotti attivi',
                    'className': 'dt3',
                    searchable: true,
                    data: 'prodotti_attivi'
                };

                cols.push(col);

                col = {
                    'title': 'Quantità',
                    'className': 'dt3',
                    searchable: true,
                    data: 'available'
                };

                cols.push(col);

                col = {
                    'title': 'Creato',
                    'className': 'dt6',
                    searchable: true,
                    data: 'createdAt'
                };

                cols.push(col);

                col = {
                    'title': 'Ultima modifica',
                    'className': 'dt7',
                    searchable: true,
                    data: 'updatedAt'
                };

                cols.push(col);
                col = { 'className': 'dt-body-center' };

                // placeholder toggle
                cols.push(col);

                // placeholder cancellazione
                cols.push(col);

                var columnDefs = [];

                var columnDef = {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 2,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        var icon = 'check-square-o';
                        var title = 'Disabilita: ';
                        if (!full.isEnabled) {
                            var icon = 'square-o';
                            var title = 'Abilita: ';
                        }
                        var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                        ret += '<i class="fa fa-' + icon + '"></i>';
                        ret += '</a>';
                        return ret;
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 1,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        if (full.deleted) {
                            var title = _('Recupera:');
                            var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                            var title = _('Elimina definitivamente: ');
                            ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        } else {
                            var title = 'Elimina: ';
                            var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        }
                        return ret;
                    }
                };

                columnDefs.push(columnDef);


                $('#datatable').dataTable({
                    ajax: {
                        "url": "/admin/" + modulo + "/json"
                    },
                    aaSorting: [[2, 'asc']],
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs,
                    createdRow: function(row, data, index) {
                        if (data.deleted) {
                            $(row).addClass("danger");
                        }
                    }
                });

            }

            break;
    }

});

/** FINE document.ready **/