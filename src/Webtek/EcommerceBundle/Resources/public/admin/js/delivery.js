var modulo = 'delivery';

$(function() {

  $(document).on('click', '.btn-delete-row', function(e) {
    $(this).closest('.row').remove();
  });

  $('.btn-add-delivery').on('click', function(e) {

    e.preventDefault();

    i = $('.costi').find('.row').length;
    var matrice = $('#empty_price_row').html().replace(/__name__/g, i);

    console.log(matrice);

    $(matrice).appendTo('.costi');

  });

  $('.tutteLeNazioni').on('change', function() {
    var $row = $(this).closest('.row');
    if ($(this).val() == 0) {
      $row.find('.pannello-nazioni').removeClass('disabled');
      $row.find('.pannello-nazioni').removeAttr('disabled');
    } else {
      $row.find('.pannello-nazioni').addClass('disabled');
      $row.find('.pannello-nazioni').prop('disabled', true);
    }
  });

  $('.tutteLeNazioni').each(function() {
    $(this).trigger('change');

  });

  /** Datatable della pagina di elenco */
  if ($('#datatable').length) {


    var cols = [];

    var col = {
      'title': 'Modifica',
      'className': 'dt0 dt-body-center',
      'searchable': false
    };
    cols.push(col);

    col = {
      'title': 'Nome',
      'className': 'dt1',
      orderable: false,
      searchable: true,
      data: 'nome'
    };

    cols.push(col);

    // placeholder toggle
    col = {
      'className': 'dt4 dt-body-center',
      orderable: false,
    };
    cols.push(col);

    // placeholder cancellazione
    col = {
      'className': 'dt4 dt-body-center',
      orderable: false,
    };
    cols.push(col);

    var columnDefs = [];

    var columnDef = {
      targets: 0,
      searchable: false,
      orderable: false,
      className: 'dt-body-center',
      render: function(data, type, full, meta) {
        var toString = full.nome;
        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
      }
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 2,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.nome;
        var icon = 'check-square-o';
        var title = 'Disabilita: ';
        if (!full.isEnabled) {
          var icon = 'square-o';
          var title = 'Abilita: ';
        }
        var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
        ret += '<i class="fa fa-' + icon + '"></i>';
        ret += '</a>';
        return ret;
      }
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 1,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.nome;
        if (full.deleted) {
          var title = _('Recupera:');
          var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
          var title = _('Elimina definitivamente: ');
          ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
        } else {
          var title = 'Elimina: ';
          var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
          if (parseInt(full.cancellabile, 10) == 0) {
            ret += ' disabled';
          }
          ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
        }
        return ret;
      }
    };

    columnDefs.push(columnDef);

    $('#datatable').dataTable({
      ajax: {
        "url": "/admin/" + modulo + "/json"
      },
      aaSorting: [[3, 'asc']],
      stateSave: true,
      iDisplayLength: 15,
      responsive: true,
      columns: cols,
      columnDefs: columnDefs,
      createdRow: function(row, data, index) {
        if (data.deleted) {
          $(row).addClass("danger");
        }
      }
    });

  }


});