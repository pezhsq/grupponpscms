<?php
// 09/01/17, 16.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use AppBundle\Validator\Constraints as WebTekAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\BrandsRepository")
 * @ORM\Table(name="brands")
 * @Vich\Uploadable()
 */
class Brand
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * @Vich\UploadableField(mapping="brands", fileNameProperty="listImgFileName")
     */
    private $listImg;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    private $listImgData;
    private $listImgDelete;


    private $uuid;

    public function __construct()
    {

        $this->setUuid(Uuid::uuid1());
        $this->setSort(0);

    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    public function __toString()
    {

        return (string)$this->translate()->getTitolo();
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {

        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {

        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {

        $this->sort = $sort;
    }


    public function getListImgFileName()
    {

        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {

        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {

        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {

        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return File
     */
    public function getListImg()
    {

        return $this->listImg;
    }

    /**
     * @return Brand
     */
    public function setListImg($listImg = null)
    {

        $this->listImg = $listImg;
        $this->updatedAt = new \DateTimeImmutable();

        return $this;

    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {

        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {

        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {

        return $this->listImgData;
    }

    /**
     * @param mixed $listImgData
     */
    public function setListImgData($listImgData)
    {

        $this->listImgData = $listImgData;
    }


    function getUploadDir()
    {

        return 'files/brands/'.$this->getId().'/';

    }

}
