<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\FeatureRepository")
 * @ORM\Table(name="feature")
 */
class Feature
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\GroupFeature")
     */
    private $featureGroup;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * Feature constructor.
     */
    public function __construct()
    {

        $this->sort = 0;
    }


    public function __toString()
    {

        return (string)$this->translate()->getTitolo();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;

    }

    /**
     * @return mixed
     */
    public function getFeatureGroup()
    {

        return $this->featureGroup;
    }

    /**
     * @param mixed $featureGroup
     */
    public function setFeatureGroup($featureGroup)
    {

        $this->featureGroup = $featureGroup;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {

        $this->sort = $sort;
    }

}
