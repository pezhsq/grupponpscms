<?php
// 06/02/17, 11.02
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\CategoryRepository")
 * @ORM\Table(name="ecommerce_products_category")
 */
class Category implements ORMBehaviours\Tree\NodeInterface
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, ORMBehaviours\Tree\Node, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $amazonNode;

    /**
     * @ORM\Column(type="string")
     */
    private $template;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $icona;

    private $categoryImg;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\Product", inversedBy="categorie")
     * @ORM\JoinTable(name="product_category",
     *   inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *   joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $products;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $categoryImgFileName;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $categoryImgFileNameAlt;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort;

    private $deleteCategoryImg;

    /**
     * @ORM\Column(type="string")
     */
    private $robots;

    public function __construct()
    {

        $this->products = new ArrayCollection();
        $this->setRobots('index,follow');
        $this->sort = 0;

    }

    function __toString()
    {

        return (string)$this->translate()->getNome();
    }

    /**
     * @return mixed
     */
    public function getIcona()
    {

        return $this->icona;
    }

    /**
     * @param mixed $icona
     */
    public function setIcona($icona)
    {

        $this->icona = $icona;
    }

    /**
     * @return mixed
     */
    public function getDeleteCategoryImg()
    {

        return $this->deleteCategoryImg;
    }

    /**
     * @param mixed $deleteHeaderImg
     */
    public function setDeleteCategoryImg($deleteHeaderImg)
    {

        $this->deleteCategoryImg = $deleteHeaderImg;
    }

    function getUploadDir()
    {

        return 'files/ecommerce/categorie/'.$this->getId().'/';

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return Category
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * Set categoryImgFileName
     *
     * @param string $categoryImgFileName
     *
     * @return Category
     */
    public function setCategoryImgFileName($categoryImgFileName)
    {

        $this->categoryImgFileName = $categoryImgFileName;

        return $this;
    }

    /**
     * Get categoryImgFileName
     *
     * @return string
     */
    public function getCategoryImgFileName()
    {

        return $this->categoryImgFileName;
    }


    /**
     * @return mixed
     */
    public function getCategoryImg()
    {

        return $this->categoryImg;
    }

    /**
     * @param mixed $listImg
     */
    public function setCategoryImg($listImg)
    {

        $this->categoryImg = $listImg;
    }


    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Category
     */
    public function setSort($sort)
    {

        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * Set categoryImgFileNameAlt
     *
     * @param string $categoryImgFileNameAlt
     *
     * @return Category
     */
    public function setCategoryImgFileNameAlt($categoryImgFileNameAlt)
    {

        $this->categoryImgFileNameAlt = $categoryImgFileNameAlt;

        return $this;
    }

    /**
     * Get categoryImgFileNameAlt
     *
     * @return string
     */
    public function getCategoryImgFileNameAlt()
    {

        return $this->categoryImgFileNameAlt;
    }

    /**
     * @return mixed
     */
    public function getAmazonNode()
    {

        return $this->amazonNode;
    }

    /**
     * @param mixed $amazonNode
     */
    public function setAmazonNode($amazonNode)
    {

        $this->amazonNode = $amazonNode;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {

        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {

        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getRobots()
    {

        return $this->robots;
    }

    /**
     * @param mixed $robots
     */
    public function setRobots($robots)
    {

        $this->robots = $robots;

    }

    /**
     * @return mixed
     */
    public function getProducts()
    {

        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {

        $this->products = $products;
    }

    public function getProdottiAttivi()
    {

        $criteria = Criteria::create()->where(Criteria::expr()->eq('isEnabled', 1));
        $return = $this->products->matching($criteria);

        return $return;

    }

    public function addProduct(Product $product)
    {

        if (!$this->products->contains($product)) {
            $this->products->add($product);
        }

    }

    public function removeProduct(Product $product)
    {

        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

    }

}
