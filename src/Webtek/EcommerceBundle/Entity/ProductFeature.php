<?php
// 10/05/17, 9.02
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="products_features")
 */
class ProductFeature
{

    use Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\GroupFeature")
     */
    private $featureGroup;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Feature")
     */
    private $feature;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $textValue;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Product", inversedBy="features")
     */
    private $product;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFeature()
    {

        return $this->feature;
    }

    /**
     * @param mixed $feature
     */
    public function setFeature($feature)
    {

        $this->feature = $feature;
        $this->textValue = '';
    }

    /**
     * @return mixed
     */
    public function getTextValue()
    {

        return $this->textValue;
    }

    /**
     * @param mixed $textValue
     */
    public function setTextValue($textValue)
    {

        $this->textValue = $textValue;
        $this->feature = null;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {

        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {

        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getFeatureGroup()
    {

        return $this->featureGroup;
    }

    /**
     * @param mixed $featureGroup
     */
    public function setFeatureGroup($featureGroup)
    {

        $this->featureGroup = $featureGroup;
    }


}