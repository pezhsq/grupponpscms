<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\VarianteProdottoRepository")
 * @ORM\Table(name="variante_prodotto")
 */
class VarianteProdotto
{

    use ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, ORMBehaviours\SoftDeletable\SoftDeletable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $giacenza;
    /**
     * @ORM\Column(type="integer")
     */
    private $soglia;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDefault;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $codice;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ean;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $codice_produttore;

    /**
     * @ORM\Column(type="decimal",precision=5, scale=2)
     */
    private $peso;
    /**
     * @ORM\Column(type="decimal",precision=5, scale=2)
     */
    private $altezza;
    /**
     * @ORM\Column(type="decimal",precision=5, scale=2)
     */
    private $larghezza;

    /**
     * @ORM\Column(type="decimal",precision=5, scale=2)
     */
    private $profondita;


    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\Attribute", cascade={"persist"}, indexBy="id")
     * @ORM\JoinTable(name="varianti_prodotto_has_attributes",
     * inverseJoinColumns={@ORM\JoinColumn(name="attribute_id", referencedColumnName="id")},
     * joinColumns={@ORM\JoinColumn(name="variante_prodotto_id", referencedColumnName="id")})
     */
    private $attributes;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Product", inversedBy="varianti")
     */
    private $prodotto;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\ProductAttachment", mappedBy="varianti",
     *     cascade={"persist"})
     */
    private $photos;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\PrezzoVariante", mappedBy="varianteProdotto",
     *     cascade={"persist", "remove"})
     */
    private $prezzi;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\PrezzoVarianteMarketPlace", mappedBy="varianteProdotto",
     *     cascade={"persist", "remove"})
     */
    private $prezziMarketPlace;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * VarianteProdotto constructor.
     */
    public function __construct()
    {

        $this->photos = new ArrayCollection();
        $this->prezzi = new ArrayCollection();
        $this->prezziMarketPlace = new ArrayCollection();
        $this->attributes = new ArrayCollection();
        $this->isDefault = false;
        $this->sort = 0;

    }

    public function getAttributesOrderedByGruppo()
    {

        $criteria = Criteria::create()->orderBy(["attributeGroup" => Criteria::ASC]);

        return $this->attributes->matching($criteria);
    }

    public function __toString()
    {

        $attributi = [];
        foreach ($this->getAttributes()->getIterator() as $attribute) {

            if ($attribute) {

                /**
                 * @var $attribute Attribute
                 */
                $attributi[] = $attribute->getAttributeGroup()->translate()->getTitolo().": ".(string)$attribute;
            }

        }

        return implode(', ', $attributi);
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {

        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getImpattoPrezzo()
    {

        return $this->impattoPrezzo;
    }

    /**
     * @param mixed $impattoPrezzo
     */
    public function setImpattoPrezzo($impattoPrezzo)
    {

        $this->impattoPrezzo = $impattoPrezzo;
    }

    /**
     * @return mixed
     */
    public function getGiacenza()
    {

        return $this->giacenza;
    }

    /**
     * @param mixed $giacenza
     */
    public function setGiacenza($giacenza)
    {

        $this->giacenza = $giacenza;
    }

    /**
     * @return mixed
     */
    public function getSoglia()
    {

        return $this->soglia;
    }

    /**
     * @param mixed $soglia
     */
    public function setSoglia($soglia)
    {

        $this->soglia = $soglia;
    }

    /**
     * @return mixed
     */
    public function getisDefault()
    {

        return $this->isDefault;
    }

    /**
     * @param mixed $isDefault
     */
    public function setIsDefault($isDefault)
    {

        $this->isDefault = $isDefault;
    }

    /**
     * @return mixed
     */
    public function getProdotto()
    {

        return $this->prodotto;
    }

    /**
     * @param mixed $prodotto
     */
    public function setProdotto($prodotto)
    {

        $this->prodotto = $prodotto;
    }

    /**
     * @return mixed
     */
    public function getPhotos()
    {

        return $this->photos;
    }

    /**
     * @param mixed $photos
     */
    public function setPhotos($photos)
    {

        $this->photos = $photos;
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {

        return $this->attributes;
    }

    /**
     * @param mixed $attributes
     */
    public function setAttributes($attributes)
    {

        $this->attributes = $attributes;
    }

    /**
     * @return mixed
     */
    public function getPrezzi()
    {

        return $this->prezzi;
    }

    /**
     * @param mixed $prezzi
     */
    public function setPrezzi($prezzi)
    {

        $this->prezzi = $prezzi;
    }

    public function addPrezzo(PrezzoVariante $prezzoVariante)
    {

        $prezzoVariante->setVarianteProdotto($this);
        $this->prezzi[] = $prezzoVariante;

        return $this;

    }

    /**
     * @return mixed
     */
    public function getCodice()
    {

        return $this->codice;
    }

    /**
     * @param mixed $codice
     */
    public function setCodice($codice)
    {

        $this->codice = $codice;
    }

    /**
     * @return mixed
     */
    public function getEan()
    {

        return $this->ean;
    }

    /**
     * @param mixed $ean
     */
    public function setEan($ean)
    {

        $this->ean = $ean;
    }

    /**
     * @return mixed
     */
    public function getCodiceProduttore()
    {

        return $this->codice_produttore;
    }

    /**
     * @param mixed $codice_produttore
     */
    public function setCodiceProduttore($codice_produttore)
    {

        $this->codice_produttore = $codice_produttore;
    }

    /**
     * @return mixed
     */
    public function getPeso()
    {

        return $this->peso;
    }

    /**
     * @param mixed $peso
     */
    public function setPeso($peso)
    {

        $this->peso = $peso;
    }

    /**
     * @return mixed
     */
    public function getAltezza()
    {

        return $this->altezza;
    }

    /**
     * @param mixed $altezza
     */
    public function setAltezza($altezza)
    {

        $this->altezza = $altezza;
    }

    /**
     * @return mixed
     */
    public function getLarghezza()
    {

        return $this->larghezza;
    }

    /**
     * @param mixed $larghezza
     */
    public function setLarghezza($larghezza)
    {

        $this->larghezza = $larghezza;
    }

    /**
     * @return mixed
     */
    public function getProfondita()
    {

        return $this->profondita;
    }

    /**
     * @param mixed $profondita
     */
    public function setProfondita($profondita)
    {

        $this->profondita = $profondita;
    }

    /**
     * @return mixed
     */
    public function getPrezziMarketPlace()
    {

        return $this->prezziMarketPlace;
    }

    /**
     * @param mixed $prezziMarketPlace
     */
    public function setPrezziMarketPlace($prezziMarketPlace)
    {

        $this->prezziMarketPlace = $prezziMarketPlace;
    }

    public function addPrezzoMarketPlace(PrezzoVarianteMarketPlace $prezzoVarianteMarketPlace)
    {

        $prezzoVarianteMarketPlace->setVarianteProdotto($this);
        $this->prezziMarketPlace[] = $prezzoVarianteMarketPlace;

        return $this;

    }

    /**
     * @return mixed
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {

        $this->sort = $sort;
    }

    public function setDeletedAt($date = null)
    {

        $this->deletedAt = $date;

        return $this;
    }


}
