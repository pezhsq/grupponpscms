<?php
// 06/11/17, 8.57
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\CouponRepository")
 * @ORM\Table(name="coupon")
 * @UniqueEntity(fields={"code"})
 */
class Coupon
{

    const TYPE_PERCENTAGE = 0;
    const TYPE_EURO = 1;

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $code;

    /**
     * @ORM\Column(type="string")
     */
    private $type = self::TYPE_PERCENTAGE;

    /**
     * @ORM\Column(type="decimal", scale=2, precision=5)
     * @Assert\NotBlank()
     */
    private $value;

    /**
     * @ORM\Column(type="boolean")
     */
    private $illimitato;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $available;

    /**
     * @ORM\Column(type="string")
     */
    private $allProducts;

    /**
     * @ORM\Column(type="decimal", scale=2, precision=5)
     * @Assert\NotBlank()
     * @Assert\Range(min="0")
     */
    private $minimumOrder;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\Category")
     * @ORM\JoinTable(name="coupons_categorie",
     *     joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="coupon_id", referencedColumnName="id")})
     */
    private $categorie;

    /**
     * @Assert\Valid()
     */
    protected $translations;

    /**
     * Coupon constructor.
     */
    public function __construct()
    {

        $this->categorie = new ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    public function __toString()
    {

        $descrizione = $this->translate()->getTitolo();
        if ($this->getType() == Coupon::TYPE_PERCENTAGE) {
            $descrizione .= ' | -'.$this->getValue().'%';
        } else {
            $descrizione .= ' | -'.$this->getValue().'€';
        }

        return (string)$descrizione;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {

        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {

        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {

        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {

        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {

        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getIllimitato()
    {

        return $this->illimitato;
    }

    /**
     * @param mixed $illimitato
     */
    public function setIllimitato($illimitato)
    {

        $this->illimitato = $illimitato;
    }

    /**
     * @return mixed
     */
    public function getAvailable()
    {

        return $this->available;
    }

    /**
     * @param mixed $available
     */
    public function setAvailable($available)
    {

        $this->available = $available;
    }

    /**
     * @return mixed
     */
    public function getAllProducts()
    {

        return $this->allProducts;
    }

    /**
     * @param mixed $allProducts
     */
    public function setAllProducts($allProducts)
    {

        $this->allProducts = $allProducts;
    }

    /**
     * @return mixed
     */
    public function getMinimumOrder()
    {

        return $this->minimumOrder;
    }

    /**
     * @param mixed $minimumOrder
     */
    public function setMinimumOrder($minimumOrder)
    {

        $this->minimumOrder = $minimumOrder;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {

        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {

        $this->categorie = $categorie;
    }


}
