<?php
// 02/01/17, 15.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Entity\Attachment;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 * @ORM\Table(name="product_attachments")
 */
class ProductAttachment extends Attachment
{

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Product", inversedBy="attachments")
     */
    private $product;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\VarianteProdotto", inversedBy="photos")
     */
    private $varianti;

    public function __construct()
    {

        $this->varianti = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getParent()
    {

        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setParent($product)
    {

        $this->token = '';
        $this->parent_id = $product->getId();
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getVarianti()
    {

        return $this->varianti;
    }

    public function addVarianti($varianti)
    {

        if (!$this->varianti->contains($varianti)) {
            $this->varianti->add($varianti);
        }

    }

    public function removeVarianti($varianti)
    {

        $this->varianti->removeElement($varianti);

    }

}