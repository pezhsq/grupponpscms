<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\HttpFoundation\File\File;
use TagBundle\Entity\Tag;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\ProductRepository")
 * @ORM\EntityListeners({"Webtek\EcommerceBundle\EntityListener\ProductListener"})
 * @ORM\Table(name="product")
 * @Vich\Uploadable()
 */
class Product
{
    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="decimal",precision=8, scale=2)
     */
    private $peso;
    /**
     * @ORM\Column(type="decimal",precision=8, scale=2)
     */
    private $altezza;
    /**
     * @ORM\Column(type="decimal",precision=8, scale=2)
     */
    private $larghezza;

    /**
     * @ORM\Column(type="decimal",precision=8, scale=2)
     */
    private $profondita;

    /**
     * @ORM\Column(type="decimal",precision=5, scale=2)
     */
    private $costo_spedizione_aggiuntivo;
    /**
     * @ORM\Column(type="string")
     */
    private $typeOfRedirect;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $targetId;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $codice;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ean;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $codice_produttore;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantita_minima;
    /**
     * @ORM\Column(type="integer")
     */
    private $multipli;

    /**
     * @ORM\Column(type="boolean")
     */
    private $available;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allCorrieri;

    /**
     * @ORM\Column(type="boolean")
     */
    private $prezzo_dipende_da_variante;

    /**
     * @ORM\Column(type="integer")
     */
    private $giacenza;

    /**
     * @ORM\Column(type="integer")
     */
    private $soglia;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\ProductType")
     * @ORM\JoinColumn(name="product_type_id")
     */
    private $product_type;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Tax")
     * @ORM\JoinColumn(name="tax_id", referencedColumnName="id")
     */
    private $tax;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\Category", mappedBy="products", cascade={"persist"})
     */
    private $categorie;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\MetodiSpedizione")
     */
    private $metodiSpedizione;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\Prezzo", mappedBy="prodotto", cascade={"persist"})
     */
    private $prezzi;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Brand", cascade={"persist"})
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     */
    private $brand;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\VarianteProdotto", mappedBy="prodotto",
     *     cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $varianti;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\ProductAttachment", mappedBy="product",
     *     cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $attachments;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\Delivery")
     */
    private $corrieri;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\ProductFeature", mappedBy="product",
     *     cascade={"persist", "remove"})
     */
    private $features;

    /**
     * @ORM\ManyToMany(targetEntity="TagBundle\Entity\Tag", cascade={"persist"})
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\Product", cascade={"persist"})
     */
    private $correlati;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\PrezzoMarketPlace", mappedBy="prodotto",
     *     cascade={"persist", "remove"})
     */
    private $prezziMarketPlace;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAlwaysAvailable;

    private $uuid;

    /**
     * @Vich\UploadableField(mapping="prodotti", fileNameProperty="listImgFileName")
     */
    private $listImg;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $saleCounter;

    /**
     * @ORM\Column(type="decimal",  precision=12, scale=6, nullable=true)
     */
    private $prezzoCivetta;

    /**
     * @ORM\ManyToMany(targetEntity="AnagraficaBundle\Entity\Anagrafica", mappedBy="wishlistProducts")
     */
    private $wishlist_holders;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\Recensione", mappedBy="prodotto",
     *     cascade={"persist", "remove"})
     */
    private $recensioni;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    private $listImgData;
    private $listImgDelete;

    private $inWishlist;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->setPeso(0.00);
        $this->setLarghezza(0.00);
        $this->setAltezza(0.00);
        $this->setProfondita(0.00);
        $this->setCostoSpedizioneAggiuntivo(0);
        $this->setTypeOfRedirect('404');
        $this->setSoglia(0);
        $this->setGiacenza(0);
        $this->setMultipli(1);
        $this->setQuantitaMinima(1);
        $this->recensioni = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->prezzi = new ArrayCollection();
        $this->categorie = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->corrieri = new ArrayCollection();
        $this->varianti = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->correlati = new ArrayCollection();
        $this->prezziMarketPlace = new ArrayCollection();
        $this->wishlist_holders = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->translate()->getTitolo();
    }

    /**
     * @return mixed
     */
    public function getRecensioni()
    {
        return $this->recensioni;
    }

    /**
     * @param mixed $recensioni
     */
    public function setRecensioni($recensioni)
    {
        $this->recensioni = $recensioni;
    }

    /**
     * @return mixed
     */
    public function getSaleCounter()
    {
        return $this->saleCounter;
    }

    /**
     * @param mixed $saleCounter
     */
    public function setSaleCounter($saleCounter)
    {
        $this->saleCounter = $saleCounter;
    }

    /**
     * @return mixed
     */
    public function getInWishlist()
    {
        return $this->inWishlist;
    }

    /**
     * @param mixed $inWishlist
     */
    public function setInWishlist($inWishlist)
    {
        $this->inWishlist = $inWishlist;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * @param mixed $peso
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;
    }

    /**
     * @return mixed
     */
    public function getAltezza()
    {
        return $this->altezza;
    }

    /**
     * @param mixed $altezza
     */
    public function setAltezza($altezza)
    {
        $this->altezza = $altezza;
    }

    /**
     * @return mixed
     */
    public function getLarghezza()
    {
        return $this->larghezza;
    }

    /**
     * @param mixed $larghezza
     */
    public function setLarghezza($larghezza)
    {
        $this->larghezza = $larghezza;
    }

    /**
     * @return mixed
     */
    public function getCostoSpedizioneAggiuntivo()
    {
        return $this->costo_spedizione_aggiuntivo;
    }

    /**
     * @param mixed $costo_spedizione_aggiuntivo
     */
    public function setCostoSpedizioneAggiuntivo($costo_spedizione_aggiuntivo)
    {
        $this->costo_spedizione_aggiuntivo = $costo_spedizione_aggiuntivo;
    }

    /**
     * @return mixed
     */
    public function getTypeOfRedirect()
    {
        return $this->typeOfRedirect;
    }

    /**
     * @param mixed $typeOfRedirect
     */
    public function setTypeOfRedirect($typeOfRedirect)
    {
        $this->typeOfRedirect = $typeOfRedirect;
    }

    /**
     * @return mixed
     */
    public function getCodice()
    {
        return $this->codice;
    }

    /**
     * @param mixed $codice
     */
    public function setCodice($codice)
    {
        $this->codice = $codice;
    }

    /**
     * @return mixed
     */
    public function getQuantitaMinima()
    {
        return $this->quantita_minima;
    }

    /**
     * @param mixed $quantita_minima
     */
    public function setQuantitaMinima($quantita_minima)
    {
        $this->quantita_minima = $quantita_minima;
    }

    /**
     * @return mixed
     */
    public function getMultipli()
    {
        return $this->multipli;
    }

    /**
     * @param mixed $multipli
     */
    public function setMultipli($multipli)
    {
        $this->multipli = $multipli;
    }

    /**
     * @return mixed
     */
    public function getAllCorrieri()
    {
        return $this->allCorrieri;
    }

    /**
     * @param mixed $allCorrieri
     */
    public function setAllCorrieri($allCorrieri)
    {
        $this->allCorrieri = $allCorrieri;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param mixed $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return mixed
     */
    public function getProfondita()
    {
        return $this->profondita;
    }

    /**
     * @param mixed $profondita
     */
    public function setProfondita($profondita)
    {
        $this->profondita = $profondita;
    }

    /**
     * @return mixed
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * @param mixed $available
     */
    public function setAvailable($available)
    {
        $this->available = $available;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param mixed $attachments
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return mixed
     */
    public function getMetodiSpedizione()
    {
        return $this->metodiSpedizione;
    }

    /**
     * @param mixed $metodiSpedizione
     */
    public function setMetodiSpedizione($metodiSpedizione)
    {
        $this->metodiSpedizione = $metodiSpedizione;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getVarianti()
    {
        return $this->varianti;
    }

    /**
     * Aggiunge una variante prodotto alla collection solo se non questa non è già presente.
     *
     * @param VarianteProdotto $varianteProdotto
     */
    public function addVarianti(VarianteProdotto $varianteProdotto)
    {
        if (!$this->varianti->contains($varianteProdotto)) {
            $this->varianti->add($varianteProdotto);
        }
    }

    /**
     * @param VarianteProdotto $varianteProdotto
     */
    public function removeVarianti(VarianteProdotto $varianteProdotto)
    {
        $this->varianti->removeElement($varianteProdotto);
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getPrezzi()
    {
        return $this->prezzi;
    }

    /**
     * @param mixed $prezzi
     */
    public function setPrezzi($prezzi)
    {
        $this->prezzi = $prezzi;
    }

    public function addPrezzo(Prezzo $prezzo)
    {
        $prezzo->setProdotto($this);
        $this->prezzi[] = $prezzo;

        return $this;
    }

    public function addCategoria(Category $category)
    {
        $category->addProduct($this);
        if ($this->categorie->contains($category)) {
            $this->categorie[] = $category;
        }

        return $this;
    }

    public function removeCategoria(Category $category)
    {
        $category->removeProduct($this);
        $this->categorie->removeElement($category);
    }

    /**
     * @return mixed
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param mixed $features
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }

    public function addFeature(ProductFeature $productFeature)
    {
        $productFeature->setProduct($this);
        $this->features[] = $productFeature;

        return $this;
    }

    public function removeFeature(ProductFeature $productFeature)
    {
        $this->features->removeElement($productFeature);
    }

    /**
     * @return mixed
     */
    public function getCorrieri()
    {
        return $this->corrieri;
    }

    /**
     * @param mixed $corrieri
     */
    public function setCorrieri($corrieri)
    {
        $this->corrieri = $corrieri;
    }

    public function addVariante(VarianteProdotto $varianteProdotto)
    {
        $varianteProdotto->setProdotto($this);
        if (!$this->varianti->contains($varianteProdotto)) {
            $this->varianti[] = $varianteProdotto;
        }

        return $this;
    }

    public function removeVariante(VarianteProdotto $varianteProdotto)
    {
        $this->varianti->removeElement($varianteProdotto);
    }

    /**
     * @return mixed
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * @param mixed $ean
     */
    public function setEan($ean)
    {
        $this->ean = $ean;
    }

    /**
     * @return mixed
     */
    public function getCodiceProduttore()
    {
        return $this->codice_produttore;
    }

    /**
     * @param mixed $codice_produttore
     */
    public function setCodiceProduttore($codice_produttore)
    {
        $this->codice_produttore = $codice_produttore;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    public function getTagsList()
    {
        $TagList = [];
        foreach ($this->getTags() as $Tag) {
            /*
             * @var $Tag Tag
             */
            $TagList[] = $Tag->translate()->getNome();
        }

        return implode(', ', $TagList);
    }

    public function addTag(Tag $Tag)
    {
        if (false === $this->tags->contains($Tag)) {
            $this->tags->add($Tag);
        }

        return $this;
    }

    public function removeTag(Tag $Tag)
    {
        $this->tags->removeElement($Tag);
    }

    /**
     * @return mixed
     */
    public function getCorrelati()
    {
        return $this->correlati;
    }

    public function getCorrelatiAttivi()
    {
        $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('isEnabled', 1))->andWhere(
            Criteria::expr()->isNull('deletedAt')
        );
        $return = $this->correlati->matching($criteria);
        $return = $return->filter(
            function ($p) {
                return null === $p->getDeletedAt();
            }
        );

        return $return;
    }

    /**
     * @param mixed $correlati
     */
    public function setCorrelati($correlati)
    {
        $this->correlati = $correlati;
    }

    public function addCorrelati(self $Product)
    {
        if (false === $this->correlati->contains($Product)) {
            $this->correlati->add($Product);
        }

        return $this;
    }

    public function removeCorrelati(self $Product)
    {
        $this->correlati->removeElement($Product);
    }

    public function getUploadDir()
    {
        return 'files/prodotti/'.$this->getId().'/';
    }

    /**
     * @return mixed
     */
    public function getPrezziMarketPlace()
    {
        return $this->prezziMarketPlace;
    }

    /**
     * @param mixed $prezziMarketPlace
     */
    public function setPrezziMarketPlace($prezziMarketPlace)
    {
        $this->prezziMarketPlace = $prezziMarketPlace;
    }

    public function addPrezzoMarketPlace(PrezzoMarketPlace $prezzoMarketPlace)
    {
        if ($this->prezziMarketPlace->contains($prezzoMarketPlace)) {
            $this->prezziMarketPlace->add($prezzoMarketPlace);
            $prezzoMarketPlace->setProdotto($prezzoMarketPlace);
        }
    }

    /**
     * @return mixed
     */
    public function getTargetId()
    {
        return $this->targetId;
    }

    /**
     * @param mixed $targetId
     */
    public function setTargetId($targetId)
    {
        $this->targetId = $targetId;
    }

    /**
     * @return mixed
     */
    public function getPrezzoDipendeDaVariante()
    {
        return $this->prezzo_dipende_da_variante;
    }

    /**
     * @param mixed $prezzo_dipende_da_variante
     */
    public function setPrezzoDipendeDaVariante($prezzo_dipende_da_variante)
    {
        $this->prezzo_dipende_da_variante = $prezzo_dipende_da_variante;
    }

    /**
     * @return mixed
     */
    public function getProductType()
    {
        return $this->product_type;
    }

    /**
     * @param mixed $product_type
     */
    public function setProductType($product_type)
    {
        $this->product_type = $product_type;
    }

    /**
     * @return mixed
     */
    public function getisAlwaysAvailable()
    {
        return $this->isAlwaysAvailable;
    }

    /**
     * @param mixed $isAlwaysAvailable
     */
    public function setIsAlwaysAvailable($isAlwaysAvailable)
    {
        $this->isAlwaysAvailable = $isAlwaysAvailable;
    }

    /**
     * @return mixed
     */
    public function getGiacenza()
    {
        return $this->giacenza;
    }

    /**
     * @param mixed $giacenza
     */
    public function setGiacenza($giacenza)
    {
        $this->giacenza = $giacenza;
    }

    /**
     * @return mixed
     */
    public function getSoglia()
    {
        return $this->soglia;
    }

    /**
     * @param mixed $soglia
     */
    public function setSoglia($soglia)
    {
        $this->soglia = $soglia;
    }

    public function getAttributesByGroup(GroupAttribute $groupAttribute)
    {
        $attributes = new ArrayCollection();
        $this->getVarianti()->filter(
            function ($entry) use ($groupAttribute, $attributes) {
                /**
                 * @var VarianteProdotto
                 */
                foreach ($entry->getAttributes() as $attribute) {
                    /**
                     * @var Attribute
                     */
                    if ($attribute->getAttributeGroup() == $groupAttribute && !$attributes->contains($attribute)) {
                        if ($entry->getisDefault()) {
                            $attribute->setSelected(true);
                        }
                        $attributes->add($attribute);
                    }
                }
            }
        );

        return $attributes;
    }

    public function getVarianteDefault()
    {
        if ($this->getVarianti()->count()) {
            $this->getVarianti()->filter(
                function ($entry) {
                    /**
                     * @var VarianteProdotto
                     */
                    if ($entry->getisDefault()) {
                        return $entry;
                    }
                }
            );

            return $this->getVarianti()->first();
        }

        return false;
    }

    public function getListImgFileName()
    {
        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {
        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {
        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {
        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return File
     */
    public function getListImg()
    {
        return $this->listImg;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $listImg
     *
     * @return Product
     */
    public function setListImg(File $listImg = null)
    {
        $this->listImg = $listImg;
        $this->updatedAt = new \DateTimeImmutable();

        return $this;
    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {
        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {
        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {
        return $this->listImgData;
    }

    /**
     * @param mixed $listImgData
     */
    public function setListImgData($listImgData)
    {
        $this->listImgData = $listImgData;
    }

    /**
     * @return mixed
     */
    public function getPrezzoCivetta()
    {
        return $this->prezzoCivetta;
    }

    /**
     * @param mixed $prezzoCivetta
     */
    public function setPrezzoCivetta($prezzoCivetta)
    {
        $this->prezzoCivetta = $prezzoCivetta;
    }

    public function getMediaRecensioni()
    {
        $tot = 0;
        if (!count($this->recensioni)) {
            return 0;
        }
        foreach ($this->recensioni as $rec) {
            $tot += $rec->getValore();
        }

        return round($tot / count($this->recensioni), 2);
    }

    public function getMediaRecensioniPercentuale()
    {
        $tot = 0;
        if (!count($this->recensioni)) {
            return 0;
        }
        foreach ($this->recensioni as $rec) {
            $tot += $rec->getValore();
        }

        return round(($tot / count($this->recensioni)) * 100 / 5, 2).'%';
    }
}
