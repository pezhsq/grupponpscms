<?php
// 06/02/17, 11.02
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_tax")
 * @UniqueEntity("codice")
 */
class Tax
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;


    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $codice;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank()
     *
     * @Assert\Range(
     *      min = 0,
     *      max = 100,
     *      minMessage = "Il volore percentuale di aliquota deve essere almeno {{ limit }}",
     *      maxMessage = "Il volore percentuale di aliquota deve essere al massimo {{ limit }}"
     * )
     */
    private $aliquota;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $descrizione;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Set codice
     *
     * @param string $codice
     *
     * @return Tax
     */
    public function setCodice($codice)
    {

        $this->codice = $codice;

        return $this;
    }

    /**
     * Get codice
     *
     * @return string
     */
    public function getCodice()
    {

        return $this->codice;
    }

    /**
     * Set aliquota
     *
     * @param integer $aliquota
     *
     * @return Tax
     */
    public function setAliquota($aliquota)
    {

        $this->aliquota = $aliquota;

        return $this;
    }

    /**
     * Get aliquota
     *
     * @return integer
     */
    public function getAliquota()
    {

        return $this->aliquota;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return Tax
     */
    public function setDescrizione($descrizione)
    {

        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {

        return $this->descrizione;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return Tax
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    function __toString()
    {

        return $this->getDescrizione();
    }


}
