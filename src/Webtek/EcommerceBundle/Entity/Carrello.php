<?php
// 27/05/17, 8.13
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;


/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\CarrelloRepository")
 * @ORM\Table(name="carrello")
 */
class Carrello
{

    use ORMBehaviours\Timestampable\Timestampable;
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Coupon")
     */
    private $coupon;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\RecordCarrello", mappedBy="carrello", cascade={"persist", "remove"})
     */
    private $recordsCarrello;

    /**
     * Carrello constructor.
     */
    public function __construct()
    {

        $this->recordsCarrello = new ArrayCollection();

    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {

        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getRecordsCarrello()
    {

        return $this->recordsCarrello;
    }

    /**
     * @param mixed $recordsCarrello
     */
    public function setRecordsCarrello($recordsCarrello)
    {

        $this->recordsCarrello = $recordsCarrello;
    }

    public function addRecordCarrello(RecordCarrello $recordCarrello)
    {

        $recordTrovato = $this->recordsCarrello->filter(
            function ($record) use ($recordCarrello) {

                if ($record->getProdotto() == $recordCarrello->getProdotto() && $record->getVariante(
                    ) == $recordCarrello->getVariante()
                ) {

                    return true;
                }

            }
        );

        if ($recordTrovato->count()) {
            $recordTrovato = $recordTrovato->first();
            /**
             * @var $recordTrovato RecordCarrello
             */
            $recordTrovato->setQty($recordCarrello->getQty());
        } else {
            $recordCarrello->setCarrello($this);
            $this->recordsCarrello->add($recordCarrello);
        }

    }

    public function removeRecordCarrello(RecordCarrello $recordCarrello)
    {

        $this->recordsCarrello->removeElement($recordCarrello);

    }

    public function hasRecords()
    {

        return (bool)$this->recordsCarrello->count();
    }

    /**
     * @return mixed
     */
    public function getCoupon()
    {

        return $this->coupon;
    }

    /**
     * @param mixed $coupon
     */
    public function setCoupon($coupon)
    {

        $this->coupon = $coupon;
    }
    

}