<?php
// 27/05/17, 8.17
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="carrello_records")
 */
class RecordCarrello
{

    use ORMBehaviours\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Carrello", inversedBy="recordsCarrello")
     * @Assert\NotBlank()
     */
    private $carrello;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\Product")
     * @Assert\NotBlank()
     */
    private $prodotto;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\VarianteProdotto")
     */
    private $variante;

    /**
     * @ORM\Column(type="integer")
     */
    private $qty;

    function __toString()
    {

        $string = (string)$this->getProdotto();
        if ($this->getVariante()) {
            $string .= ' ('.(string)$this->getVariante().')';
        }

        return $string;
    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCarrello()
    {

        return $this->carrello;
    }

    /**
     * @param mixed $carrello
     */
    public function setCarrello($carrello)
    {

        $this->carrello = $carrello;
    }

    /**
     * @return mixed
     */
    public function getProdotto()
    {

        return $this->prodotto;
    }

    /**
     * @param mixed $prodotto
     */
    public function setProdotto($prodotto)
    {

        $this->prodotto = $prodotto;
    }

    /**
     * @return mixed
     */
    public function getVariante()
    {

        return $this->variante;
    }

    /**
     * @param mixed $variante
     */
    public function setVariante($variante)
    {

        $this->variante = $variante;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {

        return $this->qty;
    }

    /**
     * @param mixed $qty
     */
    public function setQty($qty)
    {

        $this->qty = $qty;
    }


}