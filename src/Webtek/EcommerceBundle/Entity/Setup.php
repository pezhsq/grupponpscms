<?php
// 06/02/17, 11.02
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_setup")
 */
class Setup
{

    use Loggable;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $ragioneSociale;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     * @Assert\Email( checkMX= true)
     */
    private $email;

    /**
     * TODO CHECK PARTITA IVA
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $partitaIva;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nomeLegale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $indirizzoLegale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $capLegale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $cittaLegale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $provinciaLegale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nazioneLegale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $telefonoLegale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $faxLegale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nomeOperativa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $indirizzoOperativa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $capOperativa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $cittaOperativa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $provinciaOperativa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nazioneOperativa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $telefonoOperativa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $faxOperativa;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $privacyPolicy;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $condizioniVendita;

    /**
     * Set ragioneSociale
     *
     * @param string $ragioneSociale
     *
     * @return Setup
     */
    public function setRagioneSociale($ragioneSociale)
    {

        $this->ragioneSociale = $ragioneSociale;

        return $this;
    }

    /**
     * Get ragioneSociale
     *
     * @return string
     */
    public function getRagioneSociale()
    {

        return $this->ragioneSociale;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Setup
     */
    public function setEmail($email)
    {

        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Set partitaIva
     *
     * @param string $partitaIva
     *
     * @return Setup
     */
    public function setPartitaIva($partitaIva)
    {

        $this->partitaIva = $partitaIva;

        return $this;
    }

    /**
     * Get partitaIva
     *
     * @return string
     */
    public function getPartitaIva()
    {

        return $this->partitaIva;
    }

    /**
     * Set nomeLegale
     *
     * @param string $nomeLegale
     *
     * @return Setup
     */
    public function setNomeLegale($nomeLegale)
    {

        $this->nomeLegale = $nomeLegale;

        return $this;
    }

    /**
     * Get nomeLegale
     *
     * @return string
     */
    public function getNomeLegale()
    {

        return $this->nomeLegale;
    }

    /**
     * Set indirizzoLegale
     *
     * @param string $indirizzoLegale
     *
     * @return Setup
     */
    public function setIndirizzoLegale($indirizzoLegale)
    {

        $this->indirizzoLegale = $indirizzoLegale;

        return $this;
    }

    /**
     * Get indirizzoLegale
     *
     * @return string
     */
    public function getIndirizzoLegale()
    {

        return $this->indirizzoLegale;
    }

    /**
     * Set capLegale
     *
     * @param string $capLegale
     *
     * @return Setup
     */
    public function setCapLegale($capLegale)
    {

        $this->capLegale = $capLegale;

        return $this;
    }

    /**
     * Get capLegale
     *
     * @return string
     */
    public function getCapLegale()
    {

        return $this->capLegale;
    }

    /**
     * Set cittaLegale
     *
     * @param string $cittaLegale
     *
     * @return Setup
     */
    public function setCittaLegale($cittaLegale)
    {

        $this->cittaLegale = $cittaLegale;

        return $this;
    }

    /**
     * Get cittaLegale
     *
     * @return string
     */
    public function getCittaLegale()
    {

        return $this->cittaLegale;
    }

    /**
     * Set provinciaLegale
     *
     * @param string $provinciaLegale
     *
     * @return Setup
     */
    public function setProvinciaLegale($provinciaLegale)
    {

        $this->provinciaLegale = $provinciaLegale;

        return $this;
    }

    /**
     * Get provinciaLegale
     *
     * @return string
     */
    public function getProvinciaLegale()
    {

        return $this->provinciaLegale;
    }

    /**
     * Set nazioneLegale
     *
     * @param string $nazioneLegale
     *
     * @return Setup
     */
    public function setNazioneLegale($nazioneLegale)
    {

        $this->nazioneLegale = $nazioneLegale;

        return $this;
    }

    /**
     * Get nazioneLegale
     *
     * @return string
     */
    public function getNazioneLegale()
    {

        return $this->nazioneLegale;
    }

    /**
     * Set telefonoLegale
     *
     * @param string $telefonoLegale
     *
     * @return Setup
     */
    public function setTelefonoLegale($telefonoLegale)
    {

        $this->telefonoLegale = $telefonoLegale;

        return $this;
    }

    /**
     * Get telefonoLegale
     *
     * @return string
     */
    public function getTelefonoLegale()
    {

        return $this->telefonoLegale;
    }

    /**
     * Set faxLegale
     *
     * @param string $faxLegale
     *
     * @return Setup
     */
    public function setFaxLegale($faxLegale)
    {

        $this->faxLegale = $faxLegale;

        return $this;
    }

    /**
     * Get faxLegale
     *
     * @return string
     */
    public function getFaxLegale()
    {

        return $this->faxLegale;
    }

    /**
     * Set nomeOperativa
     *
     * @param string $nomeOperativa
     *
     * @return Setup
     */
    public function setNomeOperativa($nomeOperativa)
    {

        $this->nomeOperativa = $nomeOperativa;

        return $this;
    }

    /**
     * Get nomeOperativa
     *
     * @return string
     */
    public function getNomeOperativa()
    {

        return $this->nomeOperativa;
    }

    /**
     * Set indirizzoOperativa
     *
     * @param string $indirizzoOperativa
     *
     * @return Setup
     */
    public function setIndirizzoOperativa($indirizzoOperativa)
    {

        $this->indirizzoOperativa = $indirizzoOperativa;

        return $this;
    }

    /**
     * Get indirizzoOperativa
     *
     * @return string
     */
    public function getIndirizzoOperativa()
    {

        return $this->indirizzoOperativa;
    }

    /**
     * Set capOperativa
     *
     * @param string $capOperativa
     *
     * @return Setup
     */
    public function setCapOperativa($capOperativa)
    {

        $this->capOperativa = $capOperativa;

        return $this;
    }

    /**
     * Get capOperativa
     *
     * @return string
     */
    public function getCapOperativa()
    {

        return $this->capOperativa;
    }

    /**
     * Set cittaOperativa
     *
     * @param string $cittaOperativa
     *
     * @return Setup
     */
    public function setCittaOperativa($cittaOperativa)
    {

        $this->cittaOperativa = $cittaOperativa;

        return $this;
    }

    /**
     * Get cittaOperativa
     *
     * @return string
     */
    public function getCittaOperativa()
    {

        return $this->cittaOperativa;
    }

    /**
     * Set provinciaOperativa
     *
     * @param string $provinciaOperativa
     *
     * @return Setup
     */
    public function setProvinciaOperativa($provinciaOperativa)
    {

        $this->provinciaOperativa = $provinciaOperativa;

        return $this;
    }

    /**
     * Get provinciaOperativa
     *
     * @return string
     */
    public function getProvinciaOperativa()
    {

        return $this->provinciaOperativa;
    }

    /**
     * Set nazioneOperativa
     *
     * @param string $nazioneOperativa
     *
     * @return Setup
     */
    public function setNazioneOperativa($nazioneOperativa)
    {

        $this->nazioneOperativa = $nazioneOperativa;

        return $this;
    }

    /**
     * Get nazioneOperativa
     *
     * @return string
     */
    public function getNazioneOperativa()
    {

        return $this->nazioneOperativa;
    }

    /**
     * Set telefonoOperativa
     *
     * @param string $telefonoOperativa
     *
     * @return Setup
     */
    public function setTelefonoOperativa($telefonoOperativa)
    {

        $this->telefonoOperativa = $telefonoOperativa;

        return $this;
    }

    /**
     * Get telefonoOperativa
     *
     * @return string
     */
    public function getTelefonoOperativa()
    {

        return $this->telefonoOperativa;
    }

    /**
     * Set faxOperativa
     *
     * @param string $faxOperativa
     *
     * @return Setup
     */
    public function setFaxOperativa($faxOperativa)
    {

        $this->faxOperativa = $faxOperativa;

        return $this;
    }

    /**
     * Get faxOperativa
     *
     * @return string
     */
    public function getFaxOperativa()
    {

        return $this->faxOperativa;
    }

    /**
     * Set privacyPolicy
     *
     * @param string $privacyPolicy
     *
     * @return Setup
     */
    public function setPrivacyPolicy($privacyPolicy)
    {

        $this->privacyPolicy = $privacyPolicy;

        return $this;
    }

    /**
     * Get privacyPolicy
     *
     * @return string
     */
    public function getPrivacyPolicy()
    {

        return $this->privacyPolicy;
    }

    /**
     * Set condizioniVendita
     *
     * @param string $condizioniVendita
     *
     * @return Setup
     */
    public function setCondizioniVendita($condizioniVendita)
    {

        $this->condizioniVendita = $condizioniVendita;

        return $this;
    }

    /**
     * Get condizioniVendita
     *
     * @return string
     */
    public function getCondizioniVendita()
    {

        return $this->condizioniVendita;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {

        return $this->id;
    }
}
