<?php

namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\AttributeRepository")
 * @ORM\Table(name="attribute")
 */
class Attribute
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\GroupAttribute")
     */
    private $attributeGroup;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $value;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $codice;

    /**
     * @Assert\Valid()
     */
    protected $translations;

    private $selected;

    /**
     * Attribute constructor.
     */
    public function __construct()
    {

        $this->selected = false;
    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    public function __toString()
    {

        return (string)$this->translate()->getTitolo();
    }

    /**
     * @return mixed
     */
    public function getAttributeGroup()
    {

        return $this->attributeGroup;
    }

    /**
     * @param mixed $attributeGroup
     */
    public function setAttributeGroup($attributeGroup)
    {

        $this->attributeGroup = $attributeGroup;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {

        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {

        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getCodice()
    {

        return $this->codice;
    }

    /**
     * @param mixed $codice
     */
    public function setCodice($codice)
    {

        $this->codice = $codice;
    }

    /**
     * @return mixed
     */
    public function getSelected()
    {

        return $this->selected;
    }

    /**
     * @param mixed $selected
     */
    public function setSelected($selected)
    {

        $this->selected = $selected;
    }


}
