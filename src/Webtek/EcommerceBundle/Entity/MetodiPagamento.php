<?php
// 06/02/17, 11.02
// @author : Gabriele Colombera - gabricom <gabricom-kun@live.it>
namespace Webtek\EcommerceBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Webtek\EcommerceBundle\Repository\PaymentRepository")
 * @ORM\Table(name="ecommerce_payment_methods")
 */
class MetodiPagamento
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDebug;

    /**
     * @ORM\Column(type="string")
     */
    private $codice;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $client_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $client_secret;

    /**
     * @ORM\ManyToOne(targetEntity="Webtek\EcommerceBundle\Entity\OrdersStatus")
     */
    private $statoDefault;
    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\Delivery",mappedBy="metodi_pagamento_validi")
     */
    private $deliveryMethods;

    /**
     * @return mixed
     */
    public function getDeliveryMethods()
    {

        return $this->deliveryMethods;
    }

    /**
     * @param mixed $deliveryMethods
     */
    public function setDeliveryMethods($deliveryMethods)
    {

        $this->deliveryMethods = $deliveryMethods;
    }

    public function addDeliveryMethod(Delivery $delivery)
    {

        $this->deliveryMethods[] = $delivery;

        return $this;
    }

    public function removeDeliveryMethod(Delivery $delivery)
    {

        $this->deliveryMethods->removeElement($delivery);

        return $this;
    }


    public function __construct()
    {

        $this->deliveryMethods = new ArrayCollection();
    }


    function __toString()
    {

        return (string)$this->translate()->getNome();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {

        return $this->id;
    }


    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return MetodiPagamento
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @return mixed
     */
    public function getisDebug()
    {

        return $this->isDebug;
    }

    /**
     * @param mixed $isDebug
     */
    public function setIsDebug($isDebug)
    {

        $this->isDebug = $isDebug;
    }

    /**
     * @return mixed
     */
    public function getCodice()
    {

        return $this->codice;
    }

    /**
     * @param mixed $codice
     */
    public function setCodice($codice)
    {

        $this->codice = $codice;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {

        return $this->client_id;
    }

    /**
     * @param mixed $client_id
     */
    public function setClientId($client_id)
    {

        $this->client_id = $client_id;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {

        return $this->client_secret;
    }

    /**
     * @param mixed $client_secret
     */
    public function setClientSecret($client_secret)
    {

        $this->client_secret = $client_secret;
    }

    /**
     * @return mixed
     */
    public function getStatoDefault()
    {

        return $this->statoDefault;
    }

    /**
     * @param mixed $statoDefault
     */
    public function setStatoDefault($statoDefault)
    {

        $this->statoDefault = $statoDefault;
    }


}
