<?php
// 18/09/17, 7.34
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Webtek\EcommerceBundle\Entity\Product;

class ProductSavedEvent extends Event
{

    const NAME = 'product.saved';

    protected $Product;

    public function __construct(Product $product)
    {

        $this->Product = $product;
    }

    public function getProduct()
    {

        return $this->Product;
    }
}