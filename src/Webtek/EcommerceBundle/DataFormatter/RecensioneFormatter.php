<?php
/**
 * Created by PhpStorm.
 * User: gabricom
 * Date: 24/08/17
 * Time: 12.06
 */

namespace Webtek\EcommerceBundle\DataFormatter;


use Webtek\EcommerceBundle\Entity\Recensione;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Form\RecensioneForm;
use AppBundle\DataFormatter\DataFormatter;

class RecensioneFormatter extends DataFormatter
{

    private $form;
    /**
     * @var $Product Product
     */
    private $Product;

    private $errors;

    private $customer;
    private $auth = false;
    private $alreadyReviewed = false;

    private $done = false;

    public function getData()
    {
        $data = [];
        if (!$this->auth) {
            return [];
        }
        if ($this->alreadyReviewed) {
            return [
                "Product" => $this->AdditionalData['Entity'],
                "messaggio" => "Hai già recensito questo prodotto"
            ];
        }
        if ($this->done) {
            return [
                "Product" => $this->AdditionalData['Entity'],
                "messaggio" => "Recensione effettuata"
            ];
        }
        if ($this->Product) {
            {
                $data['Product'] = $this->AdditionalData['Entity'];
                $data['form'] = $this->form->createView();
                $data['errors'] = $this->errors;
//                $data['uniqid'] = uniqid();
            }

            return $data;
        }
    }

    public
    function extractData()
    {

        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Product) {
            $this->Product = $this->AdditionalData['Entity'];
            if ($this->AuthCheck() && !$this->CheckAlreadyReviewed()) {
                $this->getForm();
                if ('POST' === $this->request->getMethod()) {

                    if ($this->request->request->has('recensione_form')) {

                        $this->form->handleRequest($this->request);

                        if ($this->form->isSubmitted()) {

                            if ($this->form->isValid()) {
                                $Recensione = $this->form->getData();
                                $Recensione->setIsEnabled(true);
                                $this->em->persist($Recensione);
                                $this->em->flush();
                                $this->done = true;
                            }

                        } else {

                            $errors = [];
                            foreach ($this->form->getErrors(true) as $error) {
                                /**
                                 * @var $error FormError
                                 */

                                /**
                                 * @var $Config FormConfigInterface
                                 */
                                $Config = $error->getOrigin()->getConfig();

                                $lbl = $Config->getOptions()['label'];
                                if ($lbl) {
                                    $lbl = $this->container->get('translator')->trans($lbl);
                                } else {
                                    $lbl = ucfirst($Config->getName());
                                }
                                $errors[] = $lbl . ': ' . $error->getMessage();
                            }

                            $this->errors = array_unique($errors);

                        }

                    }

                }
            }


        }


    }


    private function checkAlreadyReviewed()
    {
        $this->alreadyReviewed = $this->em->getRepository("Webtek\\EcommerceBundle\\Entity\\Recensione")->findOneBy(["anagrafica" => $this->customer, "prodotto" => $this->Product]);
        return $this->alreadyReviewed;
    }

    /*
     * Controllo che l'utente sia un utente e abbia ordinato  il prodotto almeno una volta
     */
    private function authCheck()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $authorizationChecker = $this->container->get('security.authorization_checker');
        if ($user != 'anon.' && $authorizationChecker->isGranted('ROLE_ANAGRAFICA')) {
            $this->customer = $this->em->getRepository('AnagraficaBundle:Anagrafica')->getByUser($user);

            $this->auth = $this->em->getRepository("Webtek\\EcommerceBundle\\Entity\\Order")->checkVenditaProdotto($this->Product, $this->customer);
            return $this->auth;

        }
        return false;

    }

    private
    function getForm()
    {
        $Recensione = new Recensione();
        $Recensione->setProdotto($this->Product);
        $Recensione->setAnagrafica($this->customer);
        if ($this->Product) {
            $this->form = $this->container->get('form.factory')->create(RecensioneForm::class, $Recensione);
        }

    }


}