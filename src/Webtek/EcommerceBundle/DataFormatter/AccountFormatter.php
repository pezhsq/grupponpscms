<?php
// 30/05/17, 12.04
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;

use AnagraficaBundle\Entity\Anagrafica;
use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use AppBundle\DataFormatter\DataFormatter;
use AppBundle\Entity\User;
use AppBundle\Form\LoginForm;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Webtek\EcommerceBundle\Entity\Order;
use AppBundle\Form\LostPassForm;
use AppBundle\Form\ResetPassForm;
use Symfony\Component\Validator\Constraints\Uuid;

class AccountFormatter extends DataFormatter
{

    /**
     * @var $customer Anagrafica
     */
    private $customer = null;

    private $formAnagraficaHelper;

    /**
     * @var $session Session
     */
    private $session;

    public function getData()
    {

        // TODO: Implement getData() method.
    }


    private function getLostPasswordEdit()
    {

        $error = false;

        $form = $this->container->get("form.factory")->create(
            ResetPassForm::class,
            [
                'uuid' => $this->request->query->get('uuid'),
                'code' => $this->request->query->get('code'),
            ]
        );
        $completed = 0;
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $em = $this->em;

            $User = $em->getRepository('AppBundle:User')->findOneBy(['id' => $data['uuid']]);

            if ($User && md5($User->getUpdatedAt()->format('d/m/Y H:i:s')) == $data['code']) {

                $User->setPlainPassword($data['plainPassword']);

                $em->persist($User);
                $em->flush();

                $this->request->getSession()->getFlashBag()->add('success', $this->container->get('translator')->trans('operatori.messages.password_resettata'));

                $this->container->get('security.authentication.guard_handler')
                    ->authenticateUserAndHandleSuccess(
                        $User,
                        $this->request,
                        $this->container->get('app.security.login_form_authenticator'),
                        'main'
                    );
                $completed = 1;


            } else {

                $this->request->getSession()->getFlashBag()->add('error', 'operatori.errors.form_manomesso');


            }


        }

        $uuidConstraint = new Uuid();
        $errors = $this->container->get('validator')->validate($this->request->query->get('uuid'), $uuidConstraint);


        if (!count($errors)) {

            $em = $this->em;

            $User = $em->getRepository('AppBundle:User')->findOneBy(['id' => $this->request->query->get('uuid')]);

            if ($User && md5($User->getUpdatedAt()->format('d/m/Y H:i:s')) == $this->request->query->get('code')) {


            } else {

                $this->request->getSession()->getFlashBag()->add('error', 'operatori.errors.url_manomesso');


            }

        } else {
            $this->request->getSession()->getFlashBag()->add('error', 'operatori.errors.url_manomesso');


        }
        return
            [
                'formEdit' => $form->createView(),
                'error' => $error,
                'completed' => $completed
            ];

    }


    public function getLostPasswordForm()
    {
        $data = null;
        $error = false;

        if ($this->request->query->get('uuid') && $this->request->query->get('code')) {
            return $this->getLostPasswordEdit();
        }


        $form = $this->container->get('form.factory')->create(LostPassForm::class);

        $form->handleRequest($this->request);

        $router = $this->container->get('router');


        if ($form->isSubmitted() && $form->isValid()) {

            $email = $this->request->get('lost_pass_form')['email'];

            $em = $this->em;

            $emailAdmin = $em->getRepository("Webtek\\EcommerceBundle\\Entity\\Setup")->findAll()[0]->getEmail();

            /**
             * @var User;
             */
            $User = $em->getRepository('AppBundle:User')->findOneBy(['email' => $email]);


            if ($User && $User->getIsEnabled()) {

                $link = $router->getContext()->getScheme() . '://' . $router->getContext()->getHost();

                if ($router->getContext()->getHttpPort() !== 80) {
                    $link .= ':' . $router->getContext()->getHttpPort();
                }

                $link .= $router->generate('lost_password_ecommerce');
                $link .= '?uuid=' . $User->getId() . '&code=' . md5($User->getUpdatedAt()->format('d/m/Y H:i:s'));

                $message = \Swift_Message::newInstance()
                    ->setSubject('Recupero password')
                    ->setFrom($emailAdmin)
                    ->setTo($User->getEmail())
                    ->setBody(
                        $this->container->get("templating")->render(
                            'public/layouts/' . $this->container->getParameter('generali')['layout'] . '/includes/recover_pass.twig',
                            ['nome' => $User->getNome(), 'link' => $link]
                        ),
                        'text/html'
                    );

                $this->container->get('mailer')->send($message);
                $this->request->getSession()->getFlashBag()->add('success', 'operatori.messages.email_reset_inviata');

            } else {
                $error = 'operatori.labels.user_non_trovato';
            }

        }

        return
            [
                'formRecupero' => $form->createView(),
                'error' => $error,
            ];

    }

    public
    function getOrders()
    {
        if ($this->customer) {
            $ordini = $this->container->get("app.webtek_ecommerce.services.order_helper")->getListofAnagrafica($this->customer);
            return [
                'ordini' => $ordini
            ];

        } else {
            return false;
        }
    }

    public
    function getOrder()
    {
        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Order) {

            if ($this->AdditionalData['Entity']->getStatus()->getCodice() == "EVASO")
                $this->AdditionalData["Entity"]->setRecensibile(true);

            return [
                "order" => $this->AdditionalData['Entity']];

        }
        return null;
    }

    public
    function getWishlist()
    {
        if ($this->customer) {
            $returnData = [];


            $paginator = $this->container->get('knp_paginator');
            $categoryHelper = $this->container->get('app.webtek_ecommerce.services.category_helper');
            $translator = $this->container->get('translator');

            $next = false;
            $prev = false;

            $currentPage = $this->request->query->getInt('page', 1);
            $elementiPerPagina = $this->container->getParameter('generali')['elementi_pagina_paginatore'];
            $order = $this->request->get("orderby");
            $dir = $this->request->get("dir");
            $term = $this->request->get("term");
            $products = $this->em->getRepository("Webtek\\EcommerceBundle\\Entity\\Product")->getWishlistProducts($this->customer, $order, $dir, $this->locale, $term);

//            if ($paginationData['current'] > 1) {
//
//                $prev = $categoryHelper->generaUrl(
//                    $products,
//                    $this->locale,
//                    ['page' => $paginationData['current'] - 1],
//                    false
//                );
//
//            }
//            if ($paginationData['current'] < $paginationData['last']) {
//
//                $next = $categoryHelper->generaUrl(
//                    $products,
//                    $this->locale,
//                    ['page' => $paginationData['current'] + 1],
//                    false
//                );
//
//            }

            $returnData['META']['prev'] = $prev;
            $returnData['META']['next'] = $next;
            $returnData['pagination'] = $products;
//        $returnData['nome'] = $Category->translate()->getNome();
//        $returnData['sottotitolo'] = $Category->translate()->getSottotitolo();


            return $returnData;
        }
        return false;

    }


    public
    function getMenu()
    {
        $trans = $this->container->get('translator');
        $currentPage = "";
        $urlInCuiMiTrovo = $this->request->get("_route");
        $menu = [
            [
                "titolo" => $trans->trans("ecommerce.labels.modifica_i_tuoi_dati"),
                "route" => "account_edit",
                "active" => false
            ],
            [
                "titolo" => $trans->trans("ecommerce.labels.wishlist"),
                "route" => "account_wishlist",
                "active" => false
            ],
            [
                "titolo" => $trans->trans("ecommerce.labels.ordini"),
                "route" => "account_orders",
                "active" => false
            ],
            [
                "titolo" => $trans->trans("ecommerce.labels.logout"),
                "route" => "security_ecommerce_logout",
                "url" => $this->container->get("router")->generate("security_ecommerce_logout"),
                "active" => false
            ]

        ];
        foreach ($menu as &$m) {
            if ($m['route'] == $urlInCuiMiTrovo || $m['route'] . "_it" == $urlInCuiMiTrovo) {
                $m['active'] = true;
                $currentPage = $m['titolo'];
            }
        }
        return [
            "menu" => $menu,
            "currentPage" => $currentPage
        ];

    }

    public
    function extractData()
    {

        $this->formAnagraficaHelper = $this->container->get('app.anagrafica.form.anagrafica_form_helper');
        $this->session = $this->container->get('session');
        /**
         * @var $user User
         */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $authorizationChecker = $this->container->get('security.authorization_checker');
        if ($user != 'anon.' && $authorizationChecker->isGranted('ROLE_ANAGRAFICA')) {
            $this->customer = $this->em->getRepository('AnagraficaBundle:Anagrafica')->getByUser($user);
        } else {
            $this->customer = false;
        }

    }

    public
    function getLogin()
    {
        $formLogin = $this->getFormLogin();
        $data = [];
        if ($formLogin) {
            $data['formLogin'] = $formLogin->createView();
        }
        return $data;
    }

    public
    function getEditForm()
    {
//        $langs = $this->container->get('app.languages')->getActiveLanguages();
//        $translator = $this->container->get('translator');

        $Anagrafica = $this->customer;


        $formHelper = $this->container->get('app.anagrafica.form.anagrafica_form_helper');

        $defaultData = $formHelper->retrieveDefaultData($Anagrafica);

        $defaultData['validationGroup'] = ($Anagrafica->getId()) ? 'update' : 'create';

        $form = $formHelper->createForm($defaultData, $this->locale);

        $form->handleRequest($this->request);

        $errors = [];
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_MANAGE_ANAGRAFICA')) {
                    if ($this->container->get('security.token_storage')->getToken()->getUser() != $Anagrafica->getUser()) {
                        throw $this->createAccessDeniedException();
                    }
                }

                if (!$formHelper->handleData($form, $Anagrafica)) {

                    $errors = $formHelper->getErrors();

                    $errors = array_unique($errors);

                } else {

//                    return $this->redirectToRoute('anagrafica_edit', ['id' => $Anagrafica->getId()]);

                }

            } else {

                $errors = [];
                $trans = $this->container->get('translator');
                foreach ($form->getErrors(true) as $error) {
                    $Config = $error->getOrigin()->getConfig();

                    $lbl = $Config->getOptions()['label'];
                    if ($lbl) {
                        $lbl = $trans->trans($lbl);
                    } else {
                        $lbl = ucfirst($Config->getName());
                    }
                    $errors[] = $lbl . ': ' . $error->getMessage();
                }

                $errors = array_unique($errors);

            }
        }

        $data = [
            'form' => $form->createView(),
            'errors' => $errors,
            'action' => $defaultData['validationGroup'],
            'Anagrafica' => $Anagrafica,
        ];
        return $data;
    }

    public
    function getRegistration()
    {

        $errors = false;
        $trans = $this->container->get('translator');

        $data = [];
        $data['customer'] = $this->customer;
        $data['success'] = 0;
//        $data['formOspite'] = $this->getFormOspite();

        $formRegistrazione = $this->getFormRegistrazione();
//        $formLogin = $this->getFormLogin();

        if ('POST' === $this->request->getMethod()) {

            if ($this->request->request->has('registrationForm')) {

                $formRegistrazione->handleRequest($this->request);

                if ($formRegistrazione->isSubmitted()) {

                    if ($formRegistrazione->isValid()) {
                        $Anagrafica = new Anagrafica();
                        $Anagrafica = $this->formAnagraficaHelper->handleData($formRegistrazione, $Anagrafica);

                        if (!$Anagrafica) {

                            $errors = $this->formAnagraficaHelper->getErrors();

                            $errors = array_unique($errors);

                        } else {

                            $token = new UsernamePasswordToken(
                                $Anagrafica->getUser(),
                                null,
                                'main',
                                $Anagrafica->getUser()->getRoles()
                            );

                            $this->container->get('security.token_storage')->setToken($token);
                            $this->session->set('_security_main', serialize($token));
                            $user = $this->container->get('security.token_storage')->getToken()->getUser();
                            $data['success'] = 1;
                        }

                    } else {

                        $errors = [];
                        foreach ($formRegistrazione->getErrors(true) as $error) {
                            /**
                             * @var $error FormError
                             */

                            /**
                             * @var $Config FormConfigInterface
                             */
                            $Config = $error->getOrigin()->getConfig();

                            $lbl = $Config->getOptions()['label'];
                            if ($lbl) {
                                $lbl = $trans->trans($lbl);
                            } else {
                                $lbl = ucfirst($Config->getName());
                            }
                            $errors[] = $lbl . ': ' . $error->getMessage();
                        }

                        $errors = array_unique($errors);

                    }

                }

            }

        }

        if ($formRegistrazione) {
            $data['formRegistrazione'] = $formRegistrazione->createView();
        }
//        if ($formLogin) {
//            $data['formLogin'] = $formLogin->createView();
//        }


        $data['errors'] = $errors;

        return $data;

    }

    private
    function getFormLogin()
    {

        if (!$this->customer) {
            $authenticationUtils = $this->container->get('security.authentication_utils');

            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError();

            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();

            $form = $this->container->get('form.factory')->create(LoginForm::class, ['_username' => $lastUsername]);


            return $form;
        }

        return false;

    }

    private
    function getFormRegistrazione()
    {

        if (!$this->customer) {

            $Anagrafica = new Anagrafica();
            $Anagrafica->setIsGuest(true);

            $defaultData = [];
            $defaultData = $this->formAnagraficaHelper->retrieveDefaultData($Anagrafica);
            $defaultData['validationGroup'] = 'update';

            return $this->formAnagraficaHelper->createForm(
                $defaultData,
                $this->locale,
                'registrationForm'
            );
        }

    }


}