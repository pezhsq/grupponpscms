<?php
// 25/05/17, 17.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;


use AppBundle\DataFormatter\DataFormatter;
use AppBundle\Entity\Attachment;

class SearchFormatter extends DataFormatter
{


    public function getData()
    {

    }

    public function getResults()
    {
        $paginator = $this->container->get('knp_paginator');
        $elementiPerPagina = $this->container->getParameter('generali')['elementi_pagina_paginatore'];
        $currentPage = $this->request->query->getInt('page', 1);
        $brand = $this->request->get("brand");
        $category = $this->request->get("category");

        $order = $this->request->get("orderby");
        $dir = $this->request->get("dir");

        $prodotti = $this->em->getRepository("WebtekEcommerceBundle:Product")->search($this->request->get("term"), [], $this->locale, $brand, $category, $order, $dir);
        $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($prodotti);
        $pagination = $paginator->paginate(
            $prodotti,
            $currentPage,
            $elementiPerPagina
        );
        return ["pagination" => $pagination,
            "term" => $this->request->get("term"),
            "brand" => $brand,
            "category" => $category];
    }


    public function getMarchiAndCategorie()
    {
        $data = [];
        $data['brands'] = $this->em->getRepository("WebtekEcommerceBundle:Brand")->findAllNotDeletedAndFormatted($this->locale);
        $data['categories'] = $this->em->getRepository("WebtekEcommerceBundle:Category")->getActivebyMaterializedPath($this->locale, "/1");
        return $data;
    }

    public function extractData()
    {

    }
}