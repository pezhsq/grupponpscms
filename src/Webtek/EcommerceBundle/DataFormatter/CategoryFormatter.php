<?php
// 25/05/17, 8.50
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;

use AppBundle\DataFormatter\DataFormatter;
use Symfony\Component\HttpFoundation\Response;
use Webtek\EcommerceBundle\Entity\Category;

class CategoryFormatter extends DataFormatter
{

    private $page = 1;
    private $paginator = false;

    public function getProducts()
    {

        $returnData = [];

        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Category) {

            $paginator = $this->container->get('knp_paginator');
            $categoryHelper = $this->container->get('app.webtek_ecommerce.services.category_helper');
            $translator = $this->container->get('translator');

            $next = false;
            $prev = false;

            $currentPage = $this->request->query->getInt('page', 1);
            $elementiPerPagina = $this->container->getParameter('generali')['elementi_pagina_paginatore'];
            /**
             * @var $Category Category
             */
            $Category = $this->AdditionalData['Entity'];

            $order = $this->request->get("orderby");
            $dir = $this->request->get("dir");

            $products = $this->em->getRepository("Webtek\\EcommerceBundle\\Entity\\Product")->getProdottiAttiviCategory($Category, $order, $dir, $this->locale);

            $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);
            $pagination = $paginator->paginate(
                $products,
                $currentPage,
                $elementiPerPagina
            );

            $paginationData = $pagination->getPaginationData();

            if ($paginationData['current'] > 1) {

                $prev = $categoryHelper->generaUrl(
                    $Category,
                    $this->locale,
                    ['page' => $paginationData['current'] - 1],
                    false
                );

            }
            if ($paginationData['current'] < $paginationData['last']) {

                $next = $categoryHelper->generaUrl(
                    $Category,
                    $this->locale,
                    ['page' => $paginationData['current'] + 1],
                    false
                );

            }

            $returnData['META']['prev'] = $prev;
            $returnData['META']['next'] = $next;
            $returnData['page'] = $paginationData['current'];
            $returnData['pagination'] = $pagination;
            $returnData['category'] = $Category->getId();
            $returnData['nome'] = $Category->translate()->getNome();
            $returnData['sottotitolo'] = $Category->translate()->getSottotitolo();

        }

        return $returnData;

    }

    public function getTree()
    {
        $tree = $this->em->getRepository("WebtekEcommerceBundle:Category")->getAlberoFormattatoPerformance($this->locale);
        $data = ["categorie" => $tree];
        return $data;
    }

    public function getMenuAllCategories()
    {
        $tree = $this->em->getRepository("WebtekEcommerceBundle:Category")->getAlberoFormattatoPerformance($this->locale);
        $data = ["categorie" => $tree];
        return $data;
    }


    public function getMenu()
    {
        $Category = $this->AdditionalData['Entity'];
        $path = $Category->getMaterializedPath();
        $paths = explode("/", $path);
        if (count($paths) > 2) {
            $categoriaMenuMp = "/" . $paths[1] . "/" . $paths[2];
            $categoriaMenu = $this->em->getRepository("WebtekEcommerceBundle:Category")->find($paths[2]);
        } else {
            $categoriaMenuMp = "/" . $paths[1] . "/" . $Category->getId();
            $categoriaMenu = $Category;
        }

        $tree = $this->em->getRepository("WebtekEcommerceBundle:Category")->getAlberoFormattatoPerformance($this->locale, $categoriaMenuMp);
        $data = ["categorie" => $tree, "categoriaMenu" => $categoriaMenu];
        return $data;
    }

    protected function getData()
    {
        // TODO: Implement getData() method.
    }

    protected function extractData()
    {


    }


}