<?php
// 25/05/17, 17.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;


use AppBundle\DataFormatter\DataFormatter;
use AppBundle\Entity\Attachment;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\Product;

class ProductFormatter extends DataFormatter
{

    private $Product = false;


    public function getLastCreated()
    {
        $limit = false;
        if (isset($this->data[$this->locale]['limit']['val']) && $this->data[$this->locale]['limit']['val'] != "") {
            $limit = intval($this->data[$this->locale]['limit']['val']);
        }
        if ((isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Category) || (isset($this->data[$this->locale]['categoria']['val']) && $this->data[$this->locale]['categoria']['val'] != "")) {


            $category = null;
            if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Category) {
                $category = $this->AdditionalData['Entity'];
            }
            if (isset($this->data[$this->locale]['categoria']['val']) && $this->data[$this->locale]['categoria']['val'] != "") {
                $category = $this->em->getRepository("Webtek\\EcommerceBundle\\Entity\\Category")->getBySlug($this->data[$this->locale]['categoria']['val'], $this->locale, false);
            }

            if ($category == null) {
                $products = $this->em->getRepository("WebtekEcommerceBundle:Product")->getLastCreated($limit);
                $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);
                return [
                    "prodotti" => $products];
            }


            $products = $this->em->getRepository("WebtekEcommerceBundle:Product")->getLastCreated($limit, $category);
            $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);
            return [
                "prodotti" => $products];
        } else {
            $products = $this->em->getRepository("WebtekEcommerceBundle:Product")->getLastCreated($limit, false, true);
            $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);
            return [
                "prodotti" => $products];
        }


    }

    public function getLastCreatedPaged()
    {
        $limit = false;
        $products = $this->em->getRepository("WebtekEcommerceBundle:Product")->getLastCreated(200);
        $paginator = $this->container->get('knp_paginator');

        $next = false;
        $prev = false;

        $currentPage = $this->request->query->getInt('page', 1);
        $elementiPerPagina = $this->container->getParameter('generali')['elementi_pagina_paginatore'];
        $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);
        $pagination = $paginator->paginate(
            $products,
            $currentPage,
            $elementiPerPagina
        );
        $paginationData = $pagination->getPaginationData();
        if ($paginationData['current'] > 1) {
            $prev = $this->container->get("app.path_manager")->generate("topsales_ecommerce", ['page' => $paginationData['current'] - 1]);

        }
        if ($paginationData['current'] < $paginationData['last']) {
            $next = $this->container->get("app.path_manager")->generate("topsales_ecommerce", ['page' => $paginationData['current'] + 1]);

        }

        $returnData['META']['prev'] = $prev;
        $returnData['META']['next'] = $next;
        $returnData['page'] = $paginationData['current'];
        $returnData['pagination'] = $pagination;
        return $returnData;
    }

    public function getPiuVendutiPaged()
    {
        $limit = false;
        $products = $this->em->getRepository("WebtekEcommerceBundle:Product")->getPiuVenduti();
        $paginator = $this->container->get('knp_paginator');

        $next = false;
        $prev = false;

        $currentPage = $this->request->query->getInt('page', 1);
        $elementiPerPagina = $this->container->getParameter('generali')['elementi_pagina_paginatore'];
        $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);
        $pagination = $paginator->paginate(
            $products,
            $currentPage,
            $elementiPerPagina
        );
        $paginationData = $pagination->getPaginationData();
        if ($paginationData['current'] > 1) {
            $prev = $this->container->get("app.path_manager")->generate("topsales_ecommerce", ['page' => $paginationData['current'] - 1]);

        }
        if ($paginationData['current'] < $paginationData['last']) {
            $next = $this->container->get("app.path_manager")->generate("topsales_ecommerce", ['page' => $paginationData['current'] + 1]);

        }

        $returnData['META']['prev'] = $prev;
        $returnData['META']['next'] = $next;
        $returnData['page'] = $paginationData['current'];
        $returnData['pagination'] = $pagination;
        return $returnData;
    }

    public function getCorrelati()
    {
        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Product) {
            $correlati = $this->AdditionalData['Entity']->getCorrelatiAttivi();
            $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($correlati);
            return [
                "prodotti" => $correlati
            ];
        }
        return false;
    }

    public function getPiuVenduti()
    {
        $limit = false;
        if (isset($this->data[$this->locale]['limit']['val']) && $this->data[$this->locale]['limit']['val'] != "") {
            $limit = intval($this->data[$this->locale]['limit']['val']);
        }
        $products = $this->em->getRepository("WebtekEcommerceBundle:Product")->getPiuVenduti($limit);
        $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);

        return [
            "prodotti" => $products];
    }

    public function getPiuVendutiCategoria()
    {
        if ((isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Category) || (isset($this->data[$this->locale]['categoria']['val']) && $this->data[$this->locale]['categoria']['val'] != "")) {
            $limit = false;
            if (isset($this->data[$this->locale]['limit']['val']) && $this->data[$this->locale]['limit']['val'] != "") {
                $limit = intval($this->data[$this->locale]['limit']['val']);
            }
            $category = null;
            if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Category) {
                $category = $this->AdditionalData['Entity'];
            }
            if (isset($this->data[$this->locale]['categoria']['val']) && $this->data[$this->locale]['categoria']['val'] != "") {
                $category = $this->em->getRepository("webtek\\EcommerceBundle\\Entity\\Category")->getBySlug($this->data[$this->locale]['categoria']['val'], $this->locale, false);
            }


            $products = $this->em->getRepository("WebtekEcommerceBundle:Product")->getPiuVenduti($limit, $category);
            $this->container->get("app.webtek_ecommerce.services.wishlist_helper")->checkProductsWishlist($products);
            return [
                "prodotti" => $products];
        } else
            return false;


    }


    private function socialShare()
    {
        $request = $this->request;

        $attributes = $request->attributes->all();

        $data = [];

        if (isset($attributes['_route'])) {

            $route = preg_replace('/_it$/', '', $attributes['_route']);
            switch ($route) {

                case 'blog_read_news':

                    /**
                     * @var $News News
                     */
                    $News = $this->AdditionalData['Entity'];

                    $data = [];
                    $data['twitter']['url'] = $News->translate($this->locale)->getShortUrl();
                    $data['twitter']['text'] = substr($News->translate($this->locale)->getTitolo(), 0, 120);
                    if (isset($this->AdditionalData['META']) && isset($this->AdditionalData['META']['social']) && isset($this->AdditionalData['META']['social']['twitter']) && isset($this->AdditionalData['META']['social']['twitter']['site']) && $this->AdditionalData['META']['social']['twitter']['site']) {
                        $data['twitter']['via'] = $this->AdditionalData['META']['social']['twitter']['site'];
                    }

                    $data['linkedin']['url'] = $data['twitter']['url'];
                    $data['linkedin']['title'] = substr($News->translate($this->locale)->getTitolo(), 0, 200);
                    $data['linkedin']['summary'] = $News->translate($request->getLocale())->getMetaDescription();

                    break;

                case 'page':

                    /**
                     * @var $Page Page
                     */
                    $Page = $this->AdditionalData['Entity'];

                    if (!$Page->translate($this->locale)->getShortUrl()) {

                        $link = new Link();
                        $link->setLongUrl($request->getUri());


                        $chainProvider = $this->container->get('mremi_url_shortener.chain_provider');

                        $chainProvider->getProvider('bitly')->shorten($link);

                        $Page->translate($request->getLocale())->setShortUrl($link->getShortUrl());

                        $this->em->persist($Page);
                        $this->em->flush();

                    }

                    $data = [];
                    $data['twitter']['url'] = $Page->translate($this->locale)->getShortUrl();
                    $data['twitter']['text'] = substr($Page->translate($this->locale)->getTitolo(), 0, 120);
                    if (isset($this->AdditionalData['META']) && isset($this->AdditionalData['META']['social']) && isset($this->AdditionalData['META']['social']['twitter']) && isset($this->AdditionalData['META']['social']['twitter']['site']) && $this->AdditionalData['META']['social']['twitter']['site']) {
                        $data['twitter']['via'] = $this->AdditionalData['META']['social']['twitter']['site'];
                    }

                    $data['linkedin']['url'] = $data['twitter']['url'];
                    $data['linkedin']['title'] = substr($Page->translate($this->locale)->getTitolo(), 0, 200);
                    $data['linkedin']['summary'] = $Page->translate($request->getLocale())->getMetaDescription();

                    break;
                case 'prodotto_ecommerce':
                    $data = [];
                    $data['twitter']['url'] = $request->getUri();
                    $data['twitter']['text'] = $this->AdditionalData['META']['title'];
                    if (isset($this->AdditionalData['META']) && isset($this->AdditionalData['META']['social']) && isset($this->AdditionalData['META']['social']['twitter']) && isset($this->AdditionalData['META']['social']['twitter']['site']) && $this->AdditionalData['META']['social']['twitter']['site']) {
                        $data['twitter']['via'] = $this->AdditionalData['META']['social']['twitter']['site'];
                    }

                    $data['linkedin']['url'] = $data['twitter']['url'];
                    $data['linkedin']['title'] = $this->AdditionalData['META']['title'];;
                    $data['linkedin']['summary'] = $this->AdditionalData['META']['description'];

                    break;


            }

        }

        return $data;
    }


    public function getData()
    {

        if ($this->Product) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();

            $authorizationChecker = $this->container->get('security.authorization_checker');
            if ($user != 'anon.' && $authorizationChecker->isGranted('ROLE_ANAGRAFICA')) {
                $customer = $this->em->getRepository('AnagraficaBundle:Anagrafica')->getByUser($user);
                if ($this->em->getRepository("AnagraficaBundle:Anagrafica")->isProductInWishlist($this->Product, $customer)) {
                    $this->Product->setInWishlist(1);
                }
            }
            $data = [
                'Product' => $this->Product,
            ];

            $data = array_merge($data, $this->getImages(), $this->socialShare());


            return $data;

        }

    }

    public function getMenu()
    {
        $Product = $this->AdditionalData['Entity'];
        $Category = $this->container->get("app.webtek_ecommerce.services.product_helper")->guessCategory($Product);
        $path = $Category->getMaterializedPath();
        $paths = explode("/", $path);
        if (count($paths) > 2) {
            $categoriaMenuMp = "/" . $paths[1] . "/" . $paths[2];
            $categoriaMenu = $this->em->getRepository("WebtekEcommerceBundle:Category")->find($paths[2]);
        } else {
            $categoriaMenuMp = "/" . $paths[1] . "/" . $Category->getId();
            $categoriaMenu = $Category;
        }

        $tree = $this->em->getRepository("WebtekEcommerceBundle:Category")->getAlberoFormattatoPerformance($this->locale, $categoriaMenuMp);
        $data = ["categorie" => $tree, "categoriaMenu" => $categoriaMenu];
        return $data;
    }

    public function extractData()
    {

        if (isset($this->AdditionalData['Entity'])) {

            $this->Product = $this->AdditionalData['Entity'];

        }
    }


    public function getImages()
    {

        $data = ['immagini' => false];

        if ($this->Product) {

            $attachments = $this->Product->getAttachments();

            foreach ($attachments as $attachment) {
                /**
                 * @var $attachment Attachment;
                 */
                if (in_array($attachment->getType(), ['jpeg', 'png', 'jpg', 'gif'])) {
                    $data['immagini'][] = $attachment;
                }

            }
        }

        return $data;

    }
}