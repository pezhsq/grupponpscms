<?php
// 30/05/17, 12.04
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;

use AnagraficaBundle\Entity\Anagrafica;
use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use AppBundle\DataFormatter\DataFormatter;
use AppBundle\Entity\User;
use AppBundle\Form\LoginForm;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Webtek\EcommerceBundle\Entity\Carrello;
use Webtek\EcommerceBundle\Entity\Delivery;
use Webtek\EcommerceBundle\Entity\MetodiPagamento;
use Webtek\EcommerceBundle\Event\AnagraficaSavedEvent;
use Webtek\EcommerceBundle\Form\SceltaIndirizzoForm;
use Webtek\EcommerceBundle\Form\SceltaPagamentoForm;
use Webtek\EcommerceBundle\Form\SceltaSpedizionieriForm;
use Webtek\EcommerceBundle\Service\CarrelloHelper;

class RegistrationFormatter extends DataFormatter
{

    /**
     * @var $customer Anagrafica
     */
    private $customer = null;

    private $formAnagraficaHelper;

    /**
     * @var $session Session
     */
    private $session;

    public function getData()
    {

        // TODO: Implement getData() method.
    }

    public function extractData()
    {

        $this->formAnagraficaHelper = $this->container->get('app.anagrafica.form.anagrafica_form_helper');
        $this->session = $this->container->get('session');
        /**
         * @var $user User
         */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $authorizationChecker = $this->container->get('security.authorization_checker');
        if ($user != 'anon.' && $authorizationChecker->isGranted('ROLE_ANAGRAFICA')) {
            $this->customer = $this->em->getRepository('AnagraficaBundle:Anagrafica')->getByUser($user);
        } else {
            $this->customer = false;
        }

    }

    public function getLogin()
    {

        $formLogin = $this->getFormLogin();
        $data = [];
        if ($formLogin) {
            $data['formLogin'] = $formLogin['form']->createView();
            $data['error'] = $formLogin['error'];
            $data['lastUsername'] = $formLogin['lastUsername'];
        }

        return $data;
    }

    public function getRegistration()
    {

        $errors = false;
        $trans = $this->container->get('translator');

        $data = [];
        $data['customer'] = $this->customer;
        $data['success'] = 0;
//        $data['formOspite'] = $this->getFormOspite();

        $formRegistrazione = $this->getFormRegistrazione();
//        $formLogin = $this->getFormLogin();

        if ('POST' === $this->request->getMethod()) {

            if ($this->request->request->has('registrationForm')) {

                $formRegistrazione->handleRequest($this->request);

                if ($formRegistrazione->isSubmitted()) {

                    if ($formRegistrazione->isValid()) {
                        $Anagrafica = new Anagrafica();
                        $Anagrafica = $this->formAnagraficaHelper->handleData($formRegistrazione, $Anagrafica);

                        if (!$Anagrafica) {

                            $errors = $this->formAnagraficaHelper->getErrors();

                            $errors = array_unique($errors);


                        } else {

                            $token = new UsernamePasswordToken(
                                $Anagrafica->getUser(), null, 'main', $Anagrafica->getUser()->getRoles()
                            );

                            $this->container->get('security.token_storage')->setToken($token);
                            $this->session->set('_security_main', serialize($token));
                            $user = $this->container->get('security.token_storage')->getToken()->getUser();
                            $data['success'] = 1;

                            $event = new AnagraficaSavedEvent($Anagrafica);
                            $this->container->get('event_dispatcher')->dispatch(AnagraficaSavedEvent::NAME, $event);

                        }

                    } else {
                        $errors = [];
                        foreach ($formRegistrazione->getErrors(true) as $error) {
                            /**
                             * @var $error FormError
                             */

                            /**
                             * @var $Config FormConfigInterface
                             */
                            $Config = $error->getOrigin()->getConfig();

                            $lbl = $Config->getOptions()['label'];
                            if ($lbl) {
                                $lbl = $trans->trans($lbl);
                            } else {
                                $lbl = ucfirst($Config->getName());
                            }
                            $errors[] = $lbl.': '.$error->getMessage();
                        }

                        $errors = array_unique($errors);

                    }

                }

            }

        }

        if ($formRegistrazione) {
            $data['formRegistrazione'] = $formRegistrazione->createView();
        }
//        if ($formLogin) {
//            $data['formLogin'] = $formLogin->createView();
//        }


        $data['errors'] = $errors;

        return $data;

    }

    private function getFormLogin()
    {

        if (!$this->customer) {
            $authenticationUtils = $this->container->get('security.authentication_utils');

            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError(false);

            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();

            $form = $this->container->get('form.factory')->create(LoginForm::class, ['_username' => $lastUsername]);


            return [
                "form" => $form,
                "error" => $error,
                "lastUsername" => $lastUsername,
            ];
        }

        return false;

    }

    private function getFormRegistrazione()
    {

        if (!$this->customer) {

            $Anagrafica = new Anagrafica();
            $Anagrafica->setIsGuest(true);

            $defaultData = [];
            $defaultData = $this->formAnagraficaHelper->retrieveDefaultData($Anagrafica);
            $defaultData['validationGroup'] = 'create';

            return $this->formAnagraficaHelper->createForm(
                $defaultData,
                $this->locale,
                'registrationForm'
            );
        }

    }


}