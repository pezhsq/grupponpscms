<?php
// 30/05/17, 12.04
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\DataFormatter;

use AnagraficaBundle\Entity\Anagrafica;
use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use AppBundle\DataFormatter\DataFormatter;
use AppBundle\Entity\User;
use AppBundle\Form\LoginForm;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Webtek\EcommerceBundle\Entity\Carrello;
use Webtek\EcommerceBundle\Entity\Delivery;
use Webtek\EcommerceBundle\Entity\MetodiPagamento;
use Webtek\EcommerceBundle\Form\SceltaIndirizzoForm;
use Webtek\EcommerceBundle\Form\SceltaPagamentoForm;
use Webtek\EcommerceBundle\Form\SceltaSpedizionieriForm;
use Webtek\EcommerceBundle\Service\CarrelloHelper;

class CheckoutFormatter extends DataFormatter
{

    /**
     * @var $customer Anagrafica
     */
    private $customer = null;

    private $formAnagraficaHelper;

    /**
     * @var $Address IndirizzoAnagrafica
     */
    private $Address = null;

    /**
     * @var $Delivery Delivery
     */
    private $Delivery = null;

    /**
     * @var $Payment MetodiPagamento
     */
    private $Payment = null;

    /**
     * @var $session Session
     */
    private $session;

    /**
     * @var $carrelloHelper CarrelloHelper
     */
    private $carrelloHelper;

    /**
     * @var $carrello Carrello
     */
    private $carrello;

    public function getData()
    {

        // TODO: Implement getData() method.
    }

    public function extractData()
    {
        $this->formAnagraficaHelper = $this->container->get('app.anagrafica.form.anagrafica_form_helper');
        $this->session = $this->container->get('session');
        $this->carrelloHelper = $this->container->get('app.webtek_ecommerce.services.carrello_helper');
        $this->carrello = $this->carrelloHelper->getCarrello(true);
        /**
         * @var $user User
         */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $authorizationChecker = $this->container->get('security.authorization_checker');
        if ($user != 'anon.' && $authorizationChecker->isGranted('ROLE_ANAGRAFICA')) {
            $this->customer = $this->em->getRepository('AnagraficaBundle:Anagrafica')->getByUser($user);
        } else {
            $this->customer = false;
        }

    }

    public function getCheckout()
    {

        $errors = false;
        $trans = $this->container->get('translator');

        $data = [];
        $data['customer'] = $this->customer;
        $data['formOspite'] = $this->getFormOspite();
        $data['carrello'] = $this->carrello;
        $data['registrationError'] = "";
        $data['loginError'] = "";
        $formRegistrazione = $this->getFormRegistrazione();
        $formLogin = $this->getFormLogin();
        if ($formLogin) {
            $data['loginError'] = $formLogin['error'];
            $formLogin = $formLogin['form'];

        }


        $formIndirizzi = $this->getFormIndirizzi();
        $formCorrieri = $this->getFormCorrieri();
        $formPagamenti = $this->getFormPagamenti();
        $formNuovoIndirizzo = $this->getFormNuovoIndirizzo();

        if ('POST' === $this->request->getMethod()) {

            if ($this->request->request->has('registrationForm')) {

                $formRegistrazione->handleRequest($this->request);

                if ($formRegistrazione->isSubmitted()) {

                    if ($formRegistrazione->isValid()) {
                        $Anagrafica = new Anagrafica();
                        $Anagrafica = $this->formAnagraficaHelper->handleData($formRegistrazione, $Anagrafica);

                        if (!$Anagrafica) {

                            $errors = $this->formAnagraficaHelper->getErrors();

                            $errors = array_unique($errors);

                        } else {

                            $token = new UsernamePasswordToken(
                                $Anagrafica->getUser(), null, 'main', $Anagrafica->getUser()->getRoles()
                            );

                            $this->container->get('security.token_storage')->setToken($token);
                            $this->session->set('_security_main', serialize($token));
                            $user = $this->container->get('security.token_storage')->getToken()->getUser();
                            $authorizationChecker = $this->container->get('security.authorization_checker');
                            $this->customer = $this->em->getRepository('AnagraficaBundle:Anagrafica')->getByUser($user);
                            $data['customer'] = $this->customer;

                            //RIEFFETTUO LA GET DEI FORM IN QUANTO LA VARIABILE CUSTOMER E' CAMBIATA COSI' DA PERMETTERE LA VISUALIZZAZIONE DEL CHECKOUT
                            $formRegistrazione = null;
                            $formLogin = null;
                            $formIndirizzi = $this->getFormIndirizzi();
                            $formCorrieri = $this->getFormCorrieri();
                            $formPagamenti = $this->getFormPagamenti();
                            $formNuovoIndirizzo = $this->getFormNuovoIndirizzo();

                        }

                    } else {

                        $errors = [];
                        foreach ($formRegistrazione->getErrors(true) as $error) {
                            /**
                             * @var $error FormError
                             */

                            /**
                             * @var $Config FormConfigInterface
                             */
                            $Config = $error->getOrigin()->getConfig();

                            $lbl = $Config->getOptions()['label'];
                            if ($lbl) {
                                $lbl = $trans->trans($lbl);
                            } else {
                                $lbl = ucfirst($Config->getName());
                            }
                            $errors[] = $lbl . ': ' . $error->getMessage();
                        }

                        $errors = array_unique($errors);
                        $data['registrationError'] = $errors;

                    }

                }

            }

            if ($this->request->request->has('scelta_indirizzo_form')) {
                $formIndirizzi->submit($this->request->request->get($formIndirizzi->getName()));
                if ($formIndirizzi->isSubmitted() && $formIndirizzi->isValid()) {
                    $dataIndirizzi = $formIndirizzi->getData();

                    $this->setAddress($dataIndirizzi['indirizzoId']);
                    $formIndirizzi = $this->getFormIndirizzi();
                }
            }


            if ($this->request->request->has('scelta_spedizionieri_form')) {
                $formCorrieri->submit($this->request->request->get($formCorrieri->getName()));
                if ($formCorrieri->isSubmitted() && $formCorrieri->isValid()) {
                    $dataCorrieri = $formCorrieri->getData();
                    $Address = $this->em->getRepository('AnagraficaBundle:IndirizzoAnagrafica')->findOneBy(
                        ['id' => $dataCorrieri['address']]
                    );
                    $this->setAddress($Address);
                    $this->setDelivery($dataCorrieri['delivery']);
                }
            }

            if ($this->request->request->has('indirizzo_anagrafica_form')) {
                $formNuovoIndirizzo->submit($this->request->request->get($formNuovoIndirizzo->getName()));
                if ($formNuovoIndirizzo->isSubmitted() && $formNuovoIndirizzo->isValid()) {
                    $AddressData = $formNuovoIndirizzo->getData();
                    $IndirizzoAnagrafica = new IndirizzoAnagrafica();
                    $this->container->get('app.anagrafica.form.indirizzo_form_helper')->handleData(
                        $AddressData,
                        $IndirizzoAnagrafica
                    );
                }
            }
        }
        // questo è importante, perchè rigenera il form dopo il submit dei form precedenti, rispecchia
        // i veri corrieri dipendenti dall'indirizzo selezionato all'ultima request.
        $formIndirizzi = $this->getFormIndirizzi();
        $formCorrieri = $this->getFormCorrieri();
        $formPagamenti = $this->getFormPagamenti();
        if ($formRegistrazione) {
            $data['formRegistrazione'] = $formRegistrazione->createView();
        }
        if ($formLogin) {
            $data['formLogin'] = $formLogin->createView();
        }
        if ($formIndirizzi) {
            $data['formIndirizzi'] = $formIndirizzi->createView();
        }

        if ($formCorrieri) {
            $data['corrieri'] = $this->getCorrieriDisponibili();
            $data['formCorrieri'] = $formCorrieri->createView();
        }

        if ($formPagamenti) {
            $data['pagamenti'] = $this->getPagamentiDisponibili();
            $data['formPagamenti'] = $formPagamenti->createView();
        }

        if ($formNuovoIndirizzo) {
            $data['formNuovoIndirizzo'] = $formNuovoIndirizzo->createView();
        }

        $data['selected'] = $this->getDatiSessione();
        $data['errors'] = $errors;

        return $data;

    }

    private function getFormLogin()
    {

        if (!$this->customer) {
            $authenticationUtils = $this->container->get('security.authentication_utils');

            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError();
            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();

            $form = $this->container->get('form.factory')->create(LoginForm::class, ['_username' => $lastUsername]);

            return [
                "form" => $form,
                "error" => $error
            ];
        }

        return false;

    }

    private function getFormRegistrazione()
    {

        if (!$this->customer) {

            $Anagrafica = new Anagrafica();
            $Anagrafica->setIsGuest(true);

            $defaultData = [];
            $defaultData = $this->formAnagraficaHelper->retrieveDefaultData($Anagrafica);
            $defaultData['validationGroup'] = 'create';

            return $this->formAnagraficaHelper->createForm(
                $defaultData,
                $this->locale,
                'registrationForm'
            );
        }

    }

    private function getFormOspite()
    {

    }

    private function getFormIndirizzi()
    {

        if ($this->customer) {

            $instance = $this;
            $addresses = $this->customer->getIndirizzi()->filter(
                function ($entry) use ($instance) {
                    if (!$instance->Address && $entry->getIsDefault()) {
                        return true;
                    }
                }
            );
            if (!$this->Address) {
                if ($addresses->count()) {
                    $this->Address = $addresses->first();
                }
            }
            $data = ['indirizzoId' => $this->Address];

            $form = $this->container->get('form.factory')->create(
                SceltaIndirizzoForm::class,
                $data,
                ['anagrafica' => $this->customer,
                    "action" => "#headingIndirizzi"]
            );

            return $form;

        }

        return false;
    }

    private function getFormCorrieri()
    {
        if ($this->Address) {
            $options = [
                'address' => $this->Address,
                'totaleCarrello' => $this->carrelloHelper->getTotale($this->carrello),
                "action" => "#headingCorriere"
            ];

            if (!$this->Delivery || !in_array($this->Delivery, $this->getCorrieriDisponibili())) {
                $this->Delivery = $this->getCorrieriDisponibili()[0];
                $this->setDelivery($this->Delivery);
            }

            $form = $this->container->get('form.factory')->create(
                SceltaSpedizionieriForm::class,
                [
                    'delivery' => $this->Delivery,
                    'address' => $this->Address->getId(),
                ],
                $options
            );

            return $form;
        }


        return false;
    }

    private function getCorrieriDisponibili()
    {

        if ($this->Address) {

            $Corrieri = $this->em->getRepository('WebtekEcommerceBundle:Delivery')->getByAddressAndCart(
                $this->Address,
                $this->carrelloHelper->getTotale($this->carrello)
            );

            return $Corrieri;

        }
    }

    private function getDatiSessione()
    {

        if ($this->session->get('Address')) {
            $this->Address = $this->em->getRepository('AnagraficaBundle:IndirizzoAnagrafica')->findOneBy(
                ['id' => $this->session->get('Address')]
            );
        };
        if ($this->session->get('Delivery')) {
            $this->Delivery = $this->em->getRepository('WebtekEcommerceBundle:Delivery')->findOneBy(
                ['id' => $this->session->get('Delivery')]
            );
        };

        if ($this->session->get('Payment')) {
            $this->Payment = $this->em->getRepository('WebtekEcommerceBundle:MetodiPagamento')->findOneBy(
                ['id' => $this->session->get('Payment')]
            );
        };

        $selected = [
            'Address' => $this->Address,
            'Delivery' => $this->Delivery,
            'Payment' => $this->Payment,
        ];
        return $selected;
    }

    private function setAddress(IndirizzoAnagrafica $Address)
    {

        $this->Address = $Address;
        $this->session->set('Address', $this->Address->getId());
    }

    private function setDelivery(Delivery $Delivery)
    {

        $this->Delivery = $Delivery;
        $this->session->set('Delivery', $this->Delivery->getId());

    }

    private function getFormPagamenti()
    {

        if ($this->Address && $this->Delivery) {

            if (!$this->Payment || !in_array($this->Payment, $this->getPagamentiDisponibili())) {
                $this->Payment = $this->getPagamentiDisponibili()[0];
                $this->setPayment($this->Payment);
            }

            $form = $this->container->get('form.factory')->create(
                SceltaPagamentoForm::class,
                [
                    'Payment' => $this->Payment,
                    'Address' => $this->Address->getId(),
                    'Delivery' => $this->Delivery->getId(),
                ],
                ['action' => $this->container->get('router')->generate('checkout_now')]
            );
            return $form;
        }

    }

    private function getPagamentiDisponibili()
    {

        if ($this->Address && $this->Delivery) {

            $Payments = $this->em->getRepository(
                'WebtekEcommerceBundle:MetodiPagamento'
            )->getActivePaymentsForDeliveryMethod($this->Delivery);

            return $Payments;

        }

    }

    private function setPayment(MetodiPagamento $Payment)
    {

        $this->Payment = $Payment;
        $this->session->set('Payment', $this->Payment->getId());
    }

    /**
     * Crea un nuovo form per l'aggiunta di un nuovo indirizzo
     * @return $form FormInterface
     */
    private function getFormNuovoIndirizzo()
    {

        if ($this->customer) {
            $formIndirizziHelper = $this->container->get('app.anagrafica.form.indirizzo_form_helper');


            $defaultData = [
                'anagrafica' => $this->customer->getId(),
                'nazione' => $this->customer->getNazione(),
                'provincia' => 0,
                'comune' => 0,
            ];

            $action = '';

            $form = $formIndirizziHelper->createForm($defaultData, $this->locale);

            return $form;
        }

        return null;

    }

}