<?php
// 26/09/17, 10.32
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\EventSubscriber;

use AppBundle\Entity\User;
use AppBundle\Service\EmailTemplateHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Event\WishlistAddEvent;

class WishlistSubscriber implements EventSubscriberInterface
{

    /**
     * @var TokenStorage
     */
    private $tokenStorage;
    /**
     * @var EmailTemplateHelper
     */
    private $emailTemplateHelper;
    /**
     * @var
     */
    private $parametriEmails;

    /**
     * CarrelloSubscriber constructor.
     *
     * @param EmailTemplateHelper $emailTemplateHelper
     */
    public function __construct(TokenStorage $tokenStorage, EmailTemplateHelper $emailTemplateHelper, $parametriEmails)
    {

        $this->tokenStorage = $tokenStorage;
        $this->emailTemplateHelper = $emailTemplateHelper;
        $this->parametriEmails = $parametriEmails;
    }


    public static function getSubscribedEvents()
    {

        $events = [
            WishlistAddEvent::NAME => 'onWishlistAdd',
        ];

        return $events;

    }

    public function onWishlistAdd(WishlistAddEvent $event)
    {

        /**
         * @var $Product Product
         */
        $Product = $event->getProduct();
        /**
         * @var $User User
         */
        $User = $this->tokenStorage->getToken()->getUser();
        $to = explode(';', $this->parametriEmails['alert_azioni_utente']);
        $to = array_map('trim', $to);
        $to = array_filter($to);
        $replace = [
            'NOME' => $User->getNome().' '.$User->getCognome().' ('.$User->getEmail().')',
            'PRODOTTO' => (string)$Product,
        ];
        $this->emailTemplateHelper->send(
            'ECOMMERCE_WISHLIST',
            $to,
            $replace,
            'it',
            false,
            'ordini'
        );

    }


}