<?php
// 18/09/17, 7.41
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\EventSubscriber;

use AnagraficaBundle\Entity\Anagrafica;
use AppBundle\Entity\User;
use AppBundle\Service\EmailTemplateHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Webtek\EcommerceBundle\Event\AnagraficaSavedEvent;

class AnagraficaSubscriber implements EventSubscriberInterface
{

    /**
     * @var EmailTemplateHelper
     */
    private $emailTemplateHelper;
    /**
     * Impostazioni derivanti dal pannello amministrativo
     *
     * @var $parametriEmails
     */
    private $parametriEmails;

    /**
     * AnagraficaSubscriber constructor.
     *
     * @param EmailTemplateHelper $emailTemplateHelper
     */
    public function __construct(EmailTemplateHelper $emailTemplateHelper, $parametriEmails)
    {

        $this->emailTemplateHelper = $emailTemplateHelper;
        $this->parametriEmails = $parametriEmails;
    }

    public static function getSubscribedEvents()
    {

        $events = [
            AnagraficaSavedEvent::NAME => 'onAnagraficaSaved',
        ];

        return $events;

    }

    /**
     * Metodo per l'invio dell'email di registrazione
     *
     * Al momento del salvataggio di una anagrafica è possibile far partire una mail di conferma
     * facendo il dispatch dell'evento "anagrafica.saved".
     * Questo metodo si occupa di reperire l'anagrafica e inviare la mail di conferma avvalendosi
     * della classe EmailTemplateHelper reperita tramite dependency injection
     *
     * @param AnagraficaSavedEvent $event
     */
    public function onAnagraficaSaved(AnagraficaSavedEvent $event)
    {

        /**
         * @var $Anagrafica Anagrafica
         */
        $Anagrafica = $event->getAnagrafica();

        /**
         * @var $User User
         */
        $User = $Anagrafica->getUser();

        $destinatari = [
            $User->getEmail() => $User->getNome().' '.$Anagrafica->getUser()->getCognome(),
        ];

        $bcc = explode(';', $this->parametriEmails['destinatario_contatti']);
        $bcc = array_map('trim', $bcc);
        $bcc = array_filter($bcc);

        $replace = [
            'NOME' => $User->getNome(),
            'COGNOME' => $User->getCognome(),
            'EMAIL' => $User->getEmail(),
        ];

        $this->emailTemplateHelper->send(
            'ECOMMERCE_REGISTRAZIONE',
            $destinatari,
            $replace,
            'it',
            false
        );

    }

}