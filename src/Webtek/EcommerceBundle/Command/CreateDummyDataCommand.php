<?php
// 29/11/17, 12.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Command;

use AppBundle\Command\BaseCommand;
use Doctrine\Common\Collections\ArrayCollection;
use Imagine\Filter\Basic\Save;
use Imagine\Gd\Imagine;
use Liip\ImagineBundle\Imagine\Filter\Loader\ThumbnailFilterLoader;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Webtek\EcommerceBundle\Entity\Attribute;
use Webtek\EcommerceBundle\Entity\Brand;
use Webtek\EcommerceBundle\Entity\Category;
use Webtek\EcommerceBundle\Entity\GroupAttribute;
use Webtek\EcommerceBundle\Entity\GroupFeature;
use Webtek\EcommerceBundle\Entity\Prezzo;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Entity\ProductType;
use Webtek\EcommerceBundle\Entity\Tax;

class CreateDummyDataCommand extends BaseCommand
{

    private $categorie;
    private $brands;
    private $langs;
    private $gruppiAttributi;
    private $famiglieProdotti;
    private $prodotti;

    protected function configure()
    {

        $this->setName('ecommerce:dummy')->setDescription(
            'Tool per la creazione di un ecommerce con configurazione di base già eseguita'
        )->setHelp(
            ''
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->setUp($output);
        $this->outTitle($output, 'Webtek CMS', 'Tool per la creazione di configurazioni di base dell\'ecommerce');
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(
            '<question>Il comando svuota il db attuale, sei sicuro di voler procedere? [si|NO]</question> ',
            false,
            '/^[y|s]/i'
        );
        $truncate = (bool)$helper->ask($input, $output, $question);
        $this->configureDefaultData();
        if ($truncate) {
            $this->fixturesLoad($output);
        }
        $this->createCategories($output);
        $this->createBrands($output);
        $this->createGruppiSpecifiche($output);
        $this->createAttributi($output);
        $this->creaProdotti($output);

    }

    private function configureDefaultData()
    {

        // langs
        $this->langs = ['it'];
        // CATEGORIE
        $categorie = [];
        $figlie = [];
        $figlia = ['it' => ['titolo' => 'Maniche Corte']];
        $figlie[] = $figlia;
        $figlia = ['it' => ['titolo' => 'Maniche Lunghe']];
        $figlie[] = $figlia;
        $categoria = [
            'it' => ['titolo' => 'Magliette'],
            'childs' => $figlie,
        ];
        $categorie[] = $categoria;
        $figlie = [];
        $figlia = ['it' => ['titolo' => 'Corti']];
        $figlie[] = $figlia;
        $figlia = ['it' => ['titolo' => 'Lunghi']];
        $figlie[] = $figlia;
        $categoria = [
            'it' => ['titolo' => 'Pantaloni'],
            'childs' => $figlie,
        ];
        $categorie[] = $categoria;
        $categoria = [
            'it' => ['titolo' => 'Novità'],
            'childs' => [],
        ];
        $categorie[] = $categoria;
        $this->categorie = $categorie;
        // BRANDS
        $brands = [];
        $brands[] = ['it' => ['titolo' => 'Nike']];
        $brands[] = ['it' => ['titolo' => 'Puma']];
        $this->brands = $brands;
        // GRUPPI SPECIFICHE
        $gruppiAttributi = [];
        $gruppiAttributi[] = ['it' => ['titolo' => 'Taglia'], 'tipo' => 'SELECT'];
        $gruppiAttributi[] = ['it' => ['titolo' => 'Colore'], 'tipo' => 'COLOR'];
        $gruppiAttributi[] = ['it' => ['titolo' => 'Numero'], 'tipo' => 'SELECT'];
        $this->gruppiAttributi = $gruppiAttributi;
        // FAMIGLIE/ATTRIBUTI
        $attributi = [];
        $attributo = ['tipo' => 'Taglia', 'value' => 'X', 'codice' => 'X', 'it' => ['titolo' => 'X']];
        $attributi[] = $attributo;
        $attributo = ['tipo' => 'Taglia', 'value' => 'XL', 'codice' => 'XL', 'it' => ['titolo' => 'XL']];
        $attributi[] = $attributo;
        $attributo = ['tipo' => 'Colore', 'value' => '#FF0000', 'codice' => 'red', 'it' => ['titolo' => 'Rosso']];
        $attributi[] = $attributo;
        $attributo = ['tipo' => 'Colore', 'value' => '#00FF00', 'codice' => 'green', 'it' => ['titolo' => 'Verde']];
        $attributi[] = $attributo;
        $Famiglie = [];
        $Famiglie[] = ['nome' => 'Vestiario', 'attributi' => $attributi];
        $attributi = [];
        $attributo = ['tipo' => 'Numero', 'value' => '37', 'codice' => '37', 'it' => ['titolo' => '37']];
        $attributi[] = $attributo;
        $attributo = ['tipo' => 'Numero', 'value' => '37.5', 'codice' => '37.5', 'it' => ['titolo' => '37 e 1/2']];
        $attributi[] = $attributo;
        $attributo = ['tipo' => 'Colore', 'value' => '#FF0000', 'codice' => 'red', 'it' => ['titolo' => 'Rosso']];
        $attributi[] = $attributo;
        $attributo = ['tipo' => 'Colore', 'value' => '#FFFFFF', 'codice' => 'white', 'it' => ['titolo' => 'Bianco']];
        $attributi[] = $attributo;
        $Famiglie[] = ['nome' => 'Scarpe', 'attributi' => $attributi];
        $this->famiglieProdotti = $Famiglie;
        // PRODOTTI
        $prodotti = [];
        $prodotto = [
            'codice' => 'M01',
            'isEnabled' => '1',
            'available' => 1,
            'prezzoCivetta' => 100,
            'prezzo' => 50,
            'iva' => 22,
            'prezzo_dipende_da_variante' => true,
            'giacenza' => 10,
            'isAlwaysAvailable' => true,
            'brand' => 'Puma',
            'categorie' => ['Magliette', 'Maniche Corte', 'Novità'],
            'it' => ['titolo' => 'Maglietta 1'],
        ];
        $prodotti[] = $prodotto;
        $prodotto = [
            'codice' => 'M02',
            'isEnabled' => '1',
            'available' => 1,
            'prezzoCivetta' => 100,
            'prezzo' => 50,
            'iva' => 22,
            'prezzo_dipende_da_variante' => true,
            'giacenza' => 10,
            'isAlwaysAvailable' => true,
            'brand' => 'Puma',
            'categorie' => ['Magliette', 'Maniche Lunghe', 'Novità'],
            'it' => ['titolo' => 'Maglietta a maniche lunghe novità'],
        ];
        $prodotti[] = $prodotto;
        $prodotto = [
            'codice' => 'M02',
            'isEnabled' => '1',
            'available' => 1,
            'prezzoCivetta' => 100,
            'prezzo' => 50,
            'iva' => 22,
            'prezzo_dipende_da_variante' => true,
            'giacenza' => 10,
            'isAlwaysAvailable' => true,
            'brand' => 'Puma',
            'categorie' => ['Magliette', 'Maniche Lunghe'],
            'it' => ['titolo' => 'Maglietta a maniche lunghe'],
        ];
        $prodotti[] = $prodotto;
        $prodotto = [
            'codice' => 'P01',
            'isEnabled' => '1',
            'available' => 1,
            'prezzoCivetta' => 100,
            'prezzo' => 50,
            'iva' => 22,
            'prezzo_dipende_da_variante' => true,
            'giacenza' => 10,
            'isAlwaysAvailable' => true,
            'brand' => 'Puma',
            'categorie' => ['Pantaloni', 'Corti', 'Novità'],
            'it' => ['titolo' => 'Pantalone Corto novità'],
        ];
        $prodotti[] = $prodotto;
        $prodotto = [
            'codice' => 'P02',
            'isEnabled' => '1',
            'available' => 1,
            'prezzoCivetta' => 100,
            'prezzo' => 50,
            'iva' => 22,
            'prezzo_dipende_da_variante' => true,
            'giacenza' => 10,
            'isAlwaysAvailable' => true,
            'brand' => 'Puma',
            'categorie' => ['Pantaloni', 'Lunghi'],
            'it' => ['titolo' => 'Pantalone Lungo'],
        ];
        $prodotti[] = $prodotto;
        $this->prodotti = $prodotti;

    }

    private function fixturesLoad(OutputInterface $output)
    {

        $command = $this->getApplication()->find('doctrine:fixtures:load');
        $arguments = [];
        $input = new ArrayInput($arguments);
        $command->run($input, $output);

    }

    private function createCategories(OutputInterface $output)
    {

        foreach ($this->categorie as $categoria) {

            $Category = $this->createCategory($categoria);
            $output->writeln('<info>CATEGORIA: '.$Category->translate('it')->getNome().'</info>');
            if ($categoria['childs']) {
                foreach ($categoria['childs'] as $categoria) {
                    $createdCategory = $this->createCategory($categoria, $Category);
                    $output->writeln(
                        '<info>CATEGORIA: '.$createdCategory->translate('it')->getNome().'</info>'
                    );
                }
            }

        }

    }

    private function createCategory($categoria, $parent = null)
    {

        if (!$parent) {
            $Root = $this->em->getRepository('WebtekEcommerceBundle:Category')->findBySlugPathButNotId(
                'radice',
                'it',
                '',
                0
            );
            if ($Root) {
                $parent = $Root[0];
            }

        }
        $Categoria = new Category();
        $Categoria->setTemplate('categorie');
        $Categoria->setIsEnabled(1);
        $Categoria->setRobots('index,follow');
        foreach ($this->langs as $lang) {
            $titolo = $categoria['it']['titolo'];
            if (isset($categoria[$lang])) {
                $titolo = $categoria[$lang]['titolo'];
            }
            $Categoria->translate($lang)->setNome($titolo);
        }
        $Categoria->mergeNewTranslations();
        $this->em->persist($Categoria);
        $this->em->flush();
        $Categoria->setParentNode($parent);
        $this->em->persist($Categoria);
        $this->em->flush();

        return $Categoria;

    }

    private function createBrands(OutputInterface $output)
    {

        foreach ($this->brands as $brand) {
            $this->createBrand($brand, $output);
        }

    }

    private function createBrand($brand, $output)
    {

        $Brand = new Brand();
        $Brand->setIsEnabled(1);
        foreach ($this->langs as $lang) {
            $titolo = $brand['it']['titolo'];
            if (isset($brand[$lang])) {
                $titolo = $brand[$lang]['titolo'];
            }
            $Brand->translate($lang)->setTitolo($titolo);
        }
        $Brand->mergeNewTranslations();
        $this->em->persist($Brand);
        $this->em->flush();
        $output->writeln('<info>BRAND: '.$Brand->translate('it')->getTitolo().'</info>');

    }


    private function createGruppiSpecifiche(OutputInterface $output)
    {

        foreach ($this->gruppiAttributi as $gruppoAttributi) {
            $GruppoAttributi = new GroupAttribute();
            $GruppoAttributi->setType($gruppoAttributi['tipo']);
            foreach ($this->langs as $lang) {
                $titolo = $gruppoAttributi['it']['titolo'];
                if (isset($gruppoAttributi[$lang])) {
                    $titolo = $gruppoAttributi[$lang]['titolo'];
                }
                $GruppoAttributi->translate($lang)->setTitolo($titolo);
            }
            $GruppoAttributi->mergeNewTranslations();
            $this->em->persist($GruppoAttributi);
            $output->writeln('<info>GRUPPO ATTRIBUTI '.$gruppoAttributi['it']['titolo'].'</info>');

        }
        $this->em->flush();

    }

    private function createAttributi(OutputInterface $output)
    {

        foreach ($this->famiglieProdotti as $Famiglia) {

            $Family = $this->em->getRepository('WebtekEcommerceBundle:ProductType')->findBy(
                ['nome' => $Famiglia['nome']]
            );
            if (!$Family) {
                $Family = new ProductType();
                $Family->setNome($Famiglia['nome']);
                $GruppiAttributi = new ArrayCollection();
                $cacheGruppi = [];
                foreach ($Famiglia['attributi'] as $attributo) {
                    $GruppoAttributi = $this->em->getRepository(
                        'WebtekEcommerceBundle:GroupAttributeTranslation'
                    )->findOneBy(
                        ['titolo' => $attributo['tipo'], 'locale' => 'it']
                    );
                    if ($GruppoAttributi) {
                        $GruppoAttributi = $GruppoAttributi->getTranslatable();
                        if (!isset($cacheGruppi[$GruppoAttributi->getId()])) {
                            $cacheGruppi[$GruppoAttributi->getId()] = $GruppoAttributi;
                            $GruppiAttributi->add($GruppoAttributi);
                        }

                    }
                }
                $Family->setGruppiAttributi($GruppiAttributi);
                $this->em->persist($Family);
                $this->em->flush();
                $output->writeln('<info>FAMIGLIA: '.$Family->getNome().'</info>');
            }
            foreach ($Famiglia['attributi'] as $attributo) {
                $Attributo = $this->em->getRepository('WebtekEcommerceBundle:Attribute')->findOneBy(
                    ['codice' => $attributo['codice']]
                );
                if (!$Attributo) {
                    $GruppoAttributi = $this->em->getRepository(
                        'WebtekEcommerceBundle:GroupAttributeTranslation'
                    )->findOneBy(
                        ['titolo' => $attributo['tipo'], 'locale' => 'it']
                    );
                    if ($GruppoAttributi) {

                        $GruppoAttributi = $GruppoAttributi->getTranslatable();
                        $Attributo = new Attribute();
                        $Attributo->setCodice($attributo['codice']);
                        $Attributo->setValue($attributo['value']);
                        $Attributo->setAttributeGroup($GruppoAttributi);
                        foreach ($this->langs as $lang) {
                            $titolo = $attributo['it']['titolo'];
                            if (isset($attributo[$lang])) {
                                $titolo = $attributo[$lang]['titolo'];
                            }
                            $Attributo->translate($lang)->setTitolo($titolo);
                        }
                        $Attributo->mergeNewTranslations();
                        $this->em->persist($Attributo);
                        $output->writeln('<info>ATTRIBUTO:  '.$attributo['it']['titolo'].'</info>');

                    } else {

                        $output->writeln(
                            '<error>Gruppo attributi con nome '.$attributo['it']['titolo'].' non trovato</error>'
                        );

                    }

                }
            }
            $this->em->flush();


        }
    }

    private function creaProdotti(OutputInterface $output)
    {

        $taxHelper = $this->container->get('app.webtek_ecommerce.services.tax_calculator');
        $listinoHelper = $this->container->get('app.webtek_ecommerce.services.listino_helper');
        foreach ($this->prodotti as $prodotto) {

            $Brand = $this->em->getRepository('WebtekEcommerceBundle:BrandTranslation')->findOneBy(
                ['titolo' => $prodotto['brand']]
            );
            if (!$Brand) {

                $brand = [];
                $brand['it']['titolo'] = $prodotto['brand'];
                $this->createBrand($brand, $output);

            } else {
                $Brand = $Brand->getTranslatable();
            }
            $Iva = $this->em->getRepository('WebtekEcommerceBundle:Tax')->findOneBy(['aliquota' => $prodotto['iva']]);
            if (!$Iva) {
                $Iva = $this->createTax($prodotto['iva'], $output);
            }
            $Prodotto = new Product();
            $Prodotto->setCodice($prodotto['codice']);
            $Prodotto->setAvailable($prodotto['available']);
            $Prodotto->setAllCorrieri(true);
            $Prodotto->setIsEnabled($prodotto['isEnabled']);
            $Prodotto->setPrezzoCivetta($prodotto['prezzoCivetta']);
            $Prodotto->setGiacenza($prodotto['giacenza']);
            $Prodotto->setPrezzoDipendeDaVariante($prodotto['prezzo_dipende_da_variante']);
            $Prodotto->setTax($Iva);
            $Prodotto->setBrand($Brand);
            $Prodotto->setIsAlwaysAvailable($prodotto['isAlwaysAvailable']);
            $prezzo = $taxHelper->scorpora($prodotto['prezzo'], $prodotto['iva'], 4);
            $Prezzo = new Prezzo();
            $Prezzo->setValore($prezzo['imponibile']);
            $Prezzo->setListino($listinoHelper->getDefault());
            $Prodotto->addPrezzo($Prezzo);
            foreach ($this->langs as $lang) {
                $titolo = $prodotto['it']['titolo'];
                if (isset($prodotto[$lang])) {
                    $titolo = $prodotto[$lang]['titolo'];
                }
                $Prodotto->translate($lang)->setTitolo($titolo);

            }
            foreach ($prodotto['categorie'] as $category) {
                $Category = $this->em->getRepository('WebtekEcommerceBundle:CategoryTranslation')->findOneBy(
                    ['nome' => $category]
                );
                if ($Category) {
                    $Category = $Category->getTranslatable();
                    $Prodotto->addCategoria($Category);
                }
            }
            $Prodotto->mergeNewTranslations();
            $this->em->persist($Prodotto);
            $this->em->flush();
            // immagine
            $dimensioni = $this->container->getParameter('ecommerce')['prd_list_img_dimensions'];
            $dimensioni = explode('x', $dimensioni);
            $imageHelper = $this->container->get('app.image_helper');
            $vichHelper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
            $Imagine = new Imagine();
            $file = $this->getRandomImage($dimensioni);
            $listImgFileName = basename($file);
            $listImgFileName = explode('.', $listImgFileName);
            $listImgFileName = $listImgFileName[0].'-l.'.$listImgFileName[1];
            copy($file, dirname($file).'/'.$listImgFileName);
            $toResize = $Imagine->open(dirname($file).'/'.$listImgFileName);
            $thumb = new ThumbnailFilterLoader();
            $thumb = $thumb->load(
                $toResize,
                [
                    'mode' => 'inset',
                    'size' => [$dimensioni[0], $dimensioni[1]],
                ]
            );
            $imagePath = $this->container->get('app.web_dir')->get().'/files/prodotti/'.$Prodotto->getId(
                ).'/'.$listImgFileName;
            if (!is_dir(dirname($imagePath))) {
                mkdir(dirname($imagePath), 0755, true);
            }
            $Saver = new Save($imagePath);
            $Saver->apply($thumb);
            $Prodotto->setListImgFileName(basename($listImgFileName));
            $Prodotto->setListImgAlt($Prodotto->translate()->getTitolo());
            $this->em->persist($Prodotto);
            $this->em->flush();
            $output->writeln('PRODOTTO: '.$Prodotto->translate('it')->getTitolo());

        }
        $command = $this->getApplication()->find('ecommerce:fixsort');
        $arguments = [];
        $input = new ArrayInput($arguments);
        $command->run($input, $output);

    }

    private function createTax($aliquota, OutputInterface $output)
    {

        $Iva = new Tax();
        $Iva->setAliquota($aliquota);
        $Iva->setIsEnabled(1);
        $Iva->setCodice($aliquota);
        $Iva->setDescrizione('Iva '.$aliquota.'%');
        $this->em->persist($Iva);
        $this->em->flush();
        $output->writeln('<info>IVA: '.$aliquota.'%</info>');

        return $Iva;

    }

    private function getRandomImage($dimensioni)
    {

        $path = $this->container->get('kernel')->getRootDir().'/../var/tmp/';
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
        $path = realpath($path).'/';
        $randomFileName = uniqid().'.jpg';
        if ($this->downloadIfExists(
            'https://picsum.photos/'.$dimensioni[0].'/'.$dimensioni[1].'/?random',
            $path.$randomFileName
        )) {
            return $path.$randomFileName;
        }
    }

    protected function downloadIfExists($from, $to)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $from);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404) {
            curl_close($ch);

            return false;
        }
        curl_close($ch);
        file_put_contents($to, $data);

        return true;

    }

}