<?php
// 29/11/17, 17.56
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\Command;

use AppBundle\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Webtek\EcommerceBundle\Entity\Product;

class ProductFixSortCommand extends BaseCommand
{

    protected function configure()
    {

        $this->setName('ecommerce:fixsort')->setDescription(
            'Tool per la correzione del db per la gestione del sorting'
        )->setHelp(
            ''
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->setUp($output);
        $this->outTitle($output, 'Webtek CMS', 'Tool per il fix del sorting dei prodotti');
        $Products = $this->em->getRepository('WebtekEcommerceBundle:Product')->findAllNotDeleted();
        foreach ($Products as $Product) {
            /**
             * @var $Product Product
             */
            $Root = $this->em->getRepository('WebtekEcommerceBundle:CategoryTranslation')->findOneBy(
                ['nome' => 'Radice']
            );
            if ($Root) {
                $Root = $Root->getTranslatable();
            }
            $Categories = $Product->getCategorie();
            $Categories->add($Root);
            foreach ($Categories as $category) {
                $this->container->get('app.webtek_ecommerce.services.product_helper')->createSortEntry(
                    $Product,
                    $category
                );
            }
            $output->writeln('<info>Prodotto "'.$Product->translate('it')->getTitolo().'" fixato</info>');

        }


    }

}