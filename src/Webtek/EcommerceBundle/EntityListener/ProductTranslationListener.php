<?php
// 05/01/17, 16.31
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\EntityListener;


use Doctrine\ORM\EntityManager;
use Webtek\EcommerceBundle\Entity\ProductTranslation;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ProductTranslationListener
{

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var TokenStorage
     */
    private $token;
    /**
     * @var Slugify
     */
    private $slugify;

    /**
     * @var $em EntityManager
     */
    private $em;

    /**
     * @var $Entity ProductTranslation
     */
    private $Entity;


    public function __construct(
        Slugify $slugify,
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorage $token
    ) {

        $this->authorizationChecker = $authorizationChecker;
        $this->token = $token;
        $this->slugify = $slugify;
    }


    /**
     * @ORM\PrePersist()
     * @param $page ProductTranslation
     * @param $event LifecycleEventArgs
     */
    public function prePersist(ProductTranslation $ProductTranslation, LifecycleEventArgs $event)
    {

        $this->em = $event->getEntityManager();
        $this->Entity = $ProductTranslation;

        $this->generaSlug($ProductTranslation);

    }

    /**
     * @ORM\PreUpdate()
     * @param ProductTranslation $page
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(ProductTranslation $ProductTranslation, PreUpdateEventArgs $event)
    {

        $this->em = $event->getEntityManager();
        $this->Entity = $ProductTranslation;

        $this->generaSlug($ProductTranslation);

    }

    private function generaSlug(ProductTranslation $productTranslation)
    {

        if ($this->token->getToken()) {
            if (!$this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') || ($this->authorizationChecker->isGranted(
                        'ROLE_EXTRA_SEO'
                    ) && !$productTranslation->getSlug())) {

                $seme = $productTranslation->getTitolo();
                if (strlen($seme) > 30) {
                    $seme = wordwrap($seme, 30);
                    $seme = substr($seme, 0, strpos($seme, "\n"));
                }

                $seme = $this->slugify->slugify($seme);


                $i = 0;

                $slug = $seme;

                while (!$this->checkSlug($slug)) {
                    $i++;
                    $slug = substr($seme, 0, -1 * (strlen('-'.$i))).'-'.$i;
                }

                $productTranslation->setSlug($slug);
            }
        }


    }

    private function generaMetaTitle(ProductTranslation $productTranslation)
    {

        if ($this->token->getToken()) {
            if (!$this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') || ($this->authorizationChecker->isGranted(
                        'ROLE_EXTRA_SEO'
                    ) && !$productTranslation->getMetaTitle())) {

                $seme = $productTranslation->getTitolo();
                if (strlen($seme) > 65) {
                    $seme = wordwrap($seme, 65);
                    $seme = substr($seme, 0, strpos($seme, "\n"));
                }


                $productTranslation->setMetaTitle($seme);
            }
        }

    }

    private function generaMetaDescription(ProductTranslation $productTranslation)
    {

        $productTranslation->setMetaDescription(str_replace('"', "'", $productTranslation->getMetaDescription()));

        if ($this->token->getToken()) {
            if (!$this->authorizationChecker->isGranted('ROLE_EXTRA_SEO') || ($this->authorizationChecker->isGranted(
                        'ROLE_EXTRA_SEO'
                    ) && !$productTranslation->getMetaDescription())) {

                $seme = $productTranslation->getSummary();
                if (!$seme) {
                    $seme = $productTranslation->getTesto();
                }
                $seme = html_entity_decode(strip_tags($seme), ENT_QUOTES, 'UTF-8');

                if (strlen($seme) > 180) {
                    $seme = wordwrap($seme, 180);
                    $seme = substr($seme, 0, strpos($seme, "\n"));
                }

                $productTranslation->setMetaDescription($seme);

            }
        }

    }

    private function checkSlug($slug)
    {


        $repo = $this->em->getRepository('WebtekEcommerceBundle:Product');

        $data = $repo->findBySlugButNotId(
            $slug,
            $this->Entity->getLocale(),
            $this->Entity->getTranslatable()->getId()
        );

        return !(bool)count($data);


    }

}