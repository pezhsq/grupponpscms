<?php
// 13/06/17, 18.00
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace Webtek\EcommerceBundle\EntityListener;


use AppBundle\Service\AttachmentFSManager;
use AppBundle\Service\Base64Image;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Webtek\EcommerceBundle\Entity\Brand;
use Doctrine\ORM\Mapping as ORM;

class BrandListener
{
    
    public function __construct(Base64Image $base64Image)
    {

        $this->base64Image = $base64Image;

    }

    /**
     * @ORM\PrePersist()
     * @param $Brand Brand
     * @param $event LifecycleEventArgs
     */
    public function prePersist(Brand $Brand, LifecycleEventArgs $event)
    {

        if ($Brand->getListImgFileName()) {
            $Brand->setListImgFileName($this->base64Image->cleanFileName($Brand->getListImgFileName()));
        }


    }

}