<?php
// 21/06/17, 10.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace Webtek\EcommerceBundle\EntityListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\Mapping as ORM;
use Webtek\EcommerceBundle\Entity\Product;

class ProductListener
{

    public function __construct(
        ContainerInterface $container
    ) {

        $this->container = $container;
        $this->attachmentFSManager = $container->get('app.attachment_manager');
    }

    /**
     * @ORM\PostPersist()
     * @param $args LifecycleEventArgs
     */
    public function postPersist(Product $Product, LifecycleEventArgs $args)
    {

        $this->attachmentFSManager->moveFiles($Product);
        if ($Product->getListImgFileName()) {
            if (!is_dir($this->container->get('app.web_dir')->get().'/'.$Product->getUploadDir())) {
                mkdir($this->container->get('app.web_dir')->get().'/'.$Product->getUploadDir(), 0755, true);
            }
            rename(
                $this->container->get('app.web_dir')->get().'/'.$Product->getUploadDir(
                ).'../'.$Product->getListImgFileName(),
                $this->container->get('app.web_dir')->get().'/'.$Product->getUploadDir().$Product->getListImgFileName()
            );
        }
    }

    /**
     * @ORM\PreRemove()
     */
    public function clean(Product $Product, LifecycleEventArgs $event)
    {

        if ($Product->getDeletedAt()) {
            $dir = $this->container->get('app.web_dir')->get().'/'.$Product->getUploadDir();
            $fs = new Filesystem();
            $fs->remove($dir);
        }

    }


}