<?php
// 08/03/17, 9.48
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class SettingsRepository extends EntityRepository {

    function findByRole($role = 'ROLE_USER') {

        return $this->createQueryBuilder('s')
            ->andWhere('s.requiredRole = :requiredRole')
            ->setParameter('requiredRole', $role)
            ->getQuery()->getResult();

    }

}