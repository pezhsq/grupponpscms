<?php
// 03/03/17, 16.21
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use AppBundle\Entity\News;
use Doctrine\ORM\EntityRepository;

class CommentoRepository extends EntityRepository {

    public function getCommentiForNews(News $news, $isEnabled = 1) {

        return $this->createQueryBuilder('c')
            ->andWhere('c.news = :news')
            ->setParameter('news', $news)
            ->andWhere('c.isEnabled = :isEnabled')
            ->setParameter('isEnabled', $isEnabled)
            ->andWhere('c.deletedAt IS NULL')
            ->getQuery()->getResult();
    }

    public function getCommentiCountForNews(News $news, $isEnabled = 1) {

        return $this->createQueryBuilder('c')
            ->select('COUNT(c)')
            ->andWhere('c.news = :news')
            ->setParameter('news', $news)
            ->andWhere('c.isEnabled = :isEnabled')
            ->setParameter('isEnabled', $isEnabled)
            ->andWhere('c.deletedAt IS NULL')
            ->getQuery()->getSingleScalarResult();
    }

}