<?php
// 09/01/17, 16.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;

use AppBundle\Entity\NewsCategory;
use Doctrine\ORM\EntityRepository;

class CategorieNewsRepository extends EntityRepository {

    function findAllNotDeleted() {

        return $this->createQueryBuilder('catnews')
            ->andWhere('catnews.deletedAt is NULL')
            ->orderBy('catnews.sort', 'asc')
            ->getQuery()
            ->execute();
    }

    function countAllActive($excludeThis = null) {

        $qb = $this->createQueryBuilder('n');

        $qb->select('count(n.id)')->andWhere('n.deletedAt is NULL')->andWhere('n.isEnabled = 1');

        if($excludeThis) {
            $qb->andWhere('n.id != :newsId');
            $qb->setParameter('newsId', $excludeThis);
        }

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }
    
    function getSortedByIds(array $ids) {

        $qb = $this->createQueryBuilder('t')
            ->addSelect('FIELD(t.id,'.implode(',', $ids).') as HIDDEN ordinamento')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('ordinamento');

        $query = $qb->getQuery();

        return $query->getResult();

    }


}