<?php
// 09/01/17, 15.59
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class VetrinaRepository extends EntityRepository {

    function findAllNotDeleted() {

        return $this->createQueryBuilder('n')
            ->andWhere('n.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function getSortedByIds(array $ids) {

        $qb = $this->createQueryBuilder('t')
            ->addSelect('FIELD(t.id,'.implode(',', $ids).') as HIDDEN ordinamento')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('ordinamento');

        $query = $qb->getQuery();

        return $query->getResult();

    }

}