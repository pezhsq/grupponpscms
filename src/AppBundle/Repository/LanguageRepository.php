<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 05/12/16
 * Time: 11.12
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Page;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class LanguageRepository extends EntityRepository {

    public function countActiveLanguages() {

        return $this->createQueryBuilder('languages')
            ->select('COUNT(languages)')
            ->where('languages.isEnabled = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countActiveLanguagesPublic() {

        return $this->createQueryBuilder('languages')
            ->select('COUNT(languages)')
            ->where('languages.isEnabledPublic = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getBackendLanguages() {

        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->createQueryBuilder('languages')
            ->where('languages.isEnabled = 1')
            ->addOrderBy('languages.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getPublicLanguages() {

        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->createQueryBuilder('languages')
            ->where('languages.isEnabledPublic = 1')
            ->addOrderBy('languages.sort', 'ASC');

        return $qb->getQuery()->getResult();

    }

}