<?php
// 10/07/17, 9.09
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class GalleryItemsRepository extends EntityRepository
{

    function getSortedByIds(array $ids)
    {

        $qb = $this->createQueryBuilder('t')
            ->addSelect('FIELD(t.id,'.implode(',', $ids).') as HIDDEN ordinamento')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('ordinamento');

        $query = $qb->getQuery();

        return $query->getResult();

    }

}