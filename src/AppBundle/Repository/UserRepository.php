<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 05/12/16
 * Time: 11.12
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    public function findAllButUser(User $user)
    {

        return $this->createQueryBuilder('user')->andWhere('user.role != :role')->setParameter(
            'role',
            'ROLE_ANAGRAFICA'
        )->andWhere('user.id != :userId')->setParameter(
            'userId',
            $user->getId()
        )->getQuery()->execute();

    }

    function getByEmailCount($email, $role, $excluded = 0)
    {

        $qb = $this->createQueryBuilder('a')->select('count(a.id)')->where('a.email = :email')->andWhere(
                'a.role = :role'
            )->setParameter('role', $role)->setParameter('email', $email);
        if ($excluded) {
            $qb->andWhere('a.id != :excluded')->setParameter('excluded', $excluded);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }


}