<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 22/12/16
 * Time: 14.57
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class SettingsGroupRepository extends EntityRepository {

    function findAllNotDeleted() {

        return $this->createQueryBuilder('n')
            ->andWhere('n.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

}