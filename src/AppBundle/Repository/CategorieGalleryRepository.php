<?php
// 09/01/17, 16.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;

use AppBundle\Entity\NewsCategory;
use Doctrine\ORM\EntityRepository;

class CategorieGalleryRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('cat')
            ->andWhere('cat.deletedAt is NULL')
            ->orderBy('cat.sort', 'asc')
            ->getQuery()
            ->execute();
    }

    function getSortedByIds(array $ids)
    {

        $qb = $this->createQueryBuilder('t')
            ->addSelect('FIELD(t.id,'.implode(',', $ids).') as HIDDEN ordinamento')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('ordinamento');

        $query = $qb->getQuery();

        return $query->getResult();

    }


}