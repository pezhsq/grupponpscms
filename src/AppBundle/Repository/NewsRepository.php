<?php
// 12/01/17, 16.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;

use AppBundle\Entity\News;
use AppBundle\Entity\NewsCategory;
use Doctrine\ORM\EntityRepository;
use Faker\Provider\cs_CZ\DateTime;
use Knp\DoctrineBehaviors\Model\Tree\NodeInterface;
use Knp\DoctrineBehaviors\ORM as ORMBehaviors;


class NewsRepository extends EntityRepository
{

    function search($query, $locale)
    {

        $qb = $this->createQueryBuilder('news')->leftJoin(
            'AppBundle\Entity\NewsTranslation',
            'nt',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nt.translatable = news AND nt.locale = :locale and nt.isEnabled = 1'
        )->leftJoin(
            'AppBundle\Entity\NewsRows',
            'nr',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nr.news = news AND nr.locale = :locale'
        )->setParameter('locale', $locale)->andWhere(
            'nt.titolo LIKE :search OR nt.sottotitolo LIKE :search OR nr.slot1 LIKE :search'
        )->setParameter('search', '%' . $query . '%')->andWhere('news.publishAt < NOW()');

        return $qb->getQuery()->execute();
    }

   /**
     * Ritorna il numero di News attive:
     *
     * - Devono avere il campo isEnabled = 1
     * - Devono avere il campo deletedAt = NULL
     * - Devono avere la data di pubblicazione antecedente alla DateTime attuale
     *
     * @return mixed
     */
    function countActiveNews()
    {

        $d = new \DateTime();

        $qb = $this->createQueryBuilder('n')
            ->select('count(n.id)')
            ->andWhere('n.deletedAt IS NULL')
            ->andWhere('n.isEnabled =1')
            ->andWhere('n.publishAt <= :dateTime')
            ->setParameter('dateTime', $d->format('Y-m-d H:i:s'))
            ->getQuery();

        return $qb->getSingleScalarResult();

    }

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('n')
            ->andWhere('n.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function findBySlugButNotId($slug, $locale, $id)
    {

        $query = $this->createQueryBuilder('news')
            ->leftJoin(
                'AppBundle\Entity\NewsTranslation',
                'nt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nt.translatable = news.id AND nt.locale = :locale'
            )
            ->setParameter('locale', $locale)
            ->andWhere('nt.slug = :slug')
            ->setParameter('slug', $slug)
            ->andWhere('news.id != :id')
            ->setParameter('id', $id)
            ->andWhere('news.deletedAt IS NULL')
            ->getQuery();

        return $query->execute();
    }

    function findNewsButIds($excluded = [], $locale = 'it', $start = 0, $limit = 0)
    {

        $qb = $this->createQueryBuilder('news')
            ->leftJoin(
                'AppBundle\Entity\NewsTranslation',
                'nt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nt.translatable = news.id AND nt.locale = :locale'
            )->setParameter('locale', $locale);
        $qb->andWhere('news.isEnabled = 1');
        $qb->andWhere('news.deletedAt IS NULL');
        if ($excluded) {
            $qb->andWhere('news NOT IN (:news)')
                ->setParameter('news', $excluded);
        }
        $qb->orderBy('news.publishAt', 'DESC');

        if ($limit) {


            $qb->setMaxResults($limit)
                ->setFirstResult($start);
        }

        return $qb->getQuery()->getResult();

    }

    function findNewsButIdsQB($excluded = [], $locale = 'it', $mese = null)
    {

        $qb = $this->createQueryBuilder('news')
            ->leftJoin(
                'AppBundle\Entity\NewsTranslation',
                'nt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nt.translatable = news.id AND nt.locale = :locale'
            )->setParameter('locale', $locale);
        $qb->andWhere('news.isEnabled = 1');
        $qb->andWhere('news.deletedAt IS NULL');
        if ($excluded) {
            $qb->andWhere('news NOT IN (:news)')
                ->setParameter('news', $excluded);
        }

        if ($mese) {

            list($mese, $anno) = explode('-', $mese);

            $start = new \DateTime();
            $start->setTimestamp(mktime(0, 0, 0, $mese, 1, $anno));

            $end = new \DateTime();
            $end->setTimestamp(mktime(23, 59, 59, $mese, $start->format('t'), $anno));


            $qb->andWhere('news.publishAt >= (:start)')
                ->setParameter('start', $start)
                ->andWhere('news.publishAt <= (:end)')
                ->setParameter('end', $end);

        }
        $qb->orderBy('news.publishAt', 'DESC');

        return $qb;


    }

    function findMostVisited($locale = 'it', $limit = 3)
    {

        $qb = $this->createQueryBuilder('news')
            ->leftJoin(
                'AppBundle\Entity\NewsTranslation',
                'nt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nt.translatable = news.id AND nt.locale = :locale and nt.isEnabled = 1'
            )->setParameter('locale', $locale)
            ->andWhere('news.deletedAt IS NULL');
        $qb->orderBy('nt.visits', 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult(0);

        return $qb->getQuery()->getResult();

    }

    function getMonths($locale = 'it', $limit = 3)
    {

        $qb = $this->createQueryBuilder('news')
            ->select('date_format(news.publishAt, \'%Y-%m-01\') as date');

        $qb->leftJoin(
            'AppBundle\Entity\NewsTranslation',
            'nt',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            'nt.translatable = news AND nt.locale = :locale and nt.isEnabled = 1'
        )->setParameter('locale', $locale);
        $qb->andWhere('news.isEnabled = 1');

        $qb->orderBy('date', 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult(0)
            ->groupBy('date');

        return $qb->getQuery()->getResult();

    }

    function getCorrelate(News $news, NewsCategory $newsCategory, $locale = 'it', $limit = 2)
    {

        $qb = $this->createQueryBuilder('news')
            ->leftJoin(
                'AppBundle\Entity\NewsHasCategory',
                'nhc',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nhc.news = news AND nhc.isMain = 1'
            )
            ->leftJoin(
                'AppBundle\Entity\NewsTranslation',
                'nt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'nt.translatable = nhc.news AND nt.locale = :locale and nt.isEnabled = 1'
            )->setParameter('locale', $locale)
            ->andWhere('nhc.category = :newsCat')
            ->setParameter('newsCat', $newsCategory)
            ->andWhere('news.publishAt <= :now')
            ->setParameter('now', new \DateTime())
            ->orderBy('news.publishAt', 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult(0)
            ->andWhere('news != :news')
            ->setParameter('news', $news)
            ->andWhere('news.isEnabled = 1');

        return $qb->getQuery()->getResult();


    }


}