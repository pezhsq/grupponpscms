<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 05/12/16
 * Time: 11.12
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Page;
use AppBundle\Entity\PageTranslation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Knp\DoctrineBehaviors\Model\Tree\NodeInterface;
use Knp\DoctrineBehaviors\ORM as ORMBehaviors;

class PageRepository extends EntityRepository
{

    use ORMBehaviors\Filterable\FilterableRepository;
    use ORMBehaviors\Tree\Tree;

    function findAllNotDeleted($onlyActive = false)
    {

        $qb = $this->createQueryBuilder('page')->andWhere('page.deletedAt is NULL');

        if ($onlyActive) {
            $qb->andWhere('page.isEnabled = 1');
        }

        return $qb->getQuery()->execute();
    }

    function countActivePages()
    {

        return $this->createQueryBuilder('p')->select('count(p.id)')->andWhere('p.deletedAt IS NULL')->andWhere(
                'p.isEnabled =1'
            )->getQuery()->getSingleScalarResult();

    }

    function getActivePages($locale = 'it')
    {

        $qb = $this->createQueryBuilder('p')->leftJoin(
                'AppBundle\Entity\PageTranslation',
                'ot',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = page.id AND pt.locale = :locale'
            )->setParameter('locale', $locale)->andWhere('p.deletedAt IS NULL')->andWhere('pt.isEnabled = 1')->andWhere(
                'p.isEnabled = 1'
            );

        return $qb->getQuery()->getResult();

    }


    function findBySlugPathButNotId($slug, $locale, $path, $id)
    {

        $query = $this->createQueryBuilder('page')->leftJoin(
                'AppBundle\Entity\PageTranslation',
                'pt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = page.id AND pt.locale = :locale'
            )->setParameter('locale', $locale)->andWhere('pt.slug = :slug')->setParameter('slug', $slug)->andWhere(
                'page.id != :id'
            )->setParameter('id', $id)->andWhere('page.materializedPath = :path')->setParameter(
                'path',
                $path
            )->andWhere('page.deletedAt IS NULL')->getQuery();

        return $query->execute();
    }


    public function getFlatTree($path, $rootAlias = 't')
    {

        $query = $this->getFlatTreeQB($path, $rootAlias)->getQuery();

        return $query->execute();
    }

    public function getLikeFilterColumns()
    {

        return [];
    }

    public function getEqualFilterColumns()
    {

        return ['pt:slug'];
    }

    protected function createFilterQueryBuilder()
    {

        return $this->createQueryBuilder('e')->leftJoin('e.translations', 'pt');
    }

    public function getILikeFilterColumns()
    {

        return [];
    }

    public function getInFilterColumns()
    {

        return [];
    }

    public function getBySlug($criteria)
    {

        $qb = $this->filterBy($criteria);

        $query = $qb->getQuery();

        return $query->execute();

    }

    public function getByParent($parent = 0, $getDisabled = false, $excludePage = null, $locale = 'it')
    {

        $QB = $this->createQueryBuilder('page')->leftJoin(
                'AppBundle\Entity\PageTranslation',
                'pt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = page.id AND pt.locale = :locale'
            )->setParameter('locale', $locale);
        if (!$getDisabled) {
            $QB->andWhere('page.isEnabled = 1')->andWhere('pt.isEnabled = 1');
        }

        if ($excludePage && $excludePage->getId()) {
            $QB->andWhere('page.id != :excludedPage')->setParameter('excludedPage', $excludePage);
        }
        $QB->andWhere('page.parent = :parent')->setParameter('parent', $parent);

        return $QB->getQuery()->execute();

    }

    public function getByParentExcluded(
        $parent = 0,
        $exclude = [],
        $limit = 0,
        $sort = 'updatedAt:DESC',
        $getDisabled = false,
        $locale = 'it',
        $debug = 0
    ) {

        $QB = $this->createQueryBuilder('page')->leftJoin(
                'AppBundle\Entity\PageTranslation',
                'pt',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'pt.translatable = page.id AND pt.locale = :locale'
            )->setParameter('locale', $locale);
        $QB->where('page.parent = :parent')->setParameter('parent', $parent);
        if (!$getDisabled) {
            $QB->andWhere('page.isEnabled = 1')->andWhere('pt.isEnabled = 1');
        }

        if ($exclude) {
            $QB->andWhere('page.id NOT in (:exclude)');
            $QB->setParameter('exclude', $exclude);
        }

        $sort = explode(':', $sort);

        $QB->addOrderBy('page.'.$sort[0], $sort[1]);

        if ($debug) {
            dump($QB->getQuery()->getSQL());
            die;
        }

        return $QB->getQuery()->execute();

    }

}
