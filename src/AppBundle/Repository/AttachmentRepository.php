<?php
// 30/12/16, 8.20
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class AttachmentRepository extends EntityRepository
{

    public function getByToken($token)
    {

        return $this->createQueryBuilder('attachment')
            ->andWhere('attachment.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->execute();

    }

    function getSortedByIds(array $ids)
    {

        $qb = $this->createQueryBuilder('attachment')
            ->addSelect('FIELD(attachment.id,' . implode(',', $ids) . ') as HIDDEN ordinamento')
            ->where('attachment.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('ordinamento');

        $query = $qb->getQuery();

        return $query->getResult();

    }
}