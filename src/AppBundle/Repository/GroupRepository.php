<?php
// 14/08/17, 6.50
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use AppBundle\Entity\Group;
use Doctrine\ORM\EntityRepository;

class GroupRepository extends EntityRepository
{

    function getSortedByIds(array $ids)
    {

        $qb = $this->createQueryBuilder('t')
            ->addSelect('FIELD(t.id,'.implode(',', $ids).') as HIDDEN ordinamento')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('ordinamento');

        $query = $qb->getQuery();

        return $query->getResult();

    }

    function findLessImportant(Group $Group)
    {

        $qb = $this->createQueryBuilder('group')->andWhere('group.level >= :level')
            ->setParameter('level', $Group->getLevel())->orderBy('group.level', 'ASC');
    }


}