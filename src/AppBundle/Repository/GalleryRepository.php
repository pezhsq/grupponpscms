<?php
// 07/07/17, 13.47
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class GalleryRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('g')
            ->andWhere('g.deletedAt is NULL')
            ->orderBy('g.sort', 'asc')
            ->getQuery()
            ->execute();
    }

    function getSortedByIds(array $ids)
    {

        $qb = $this->createQueryBuilder('t')
            ->addSelect('FIELD(t.id,'.implode(',', $ids).') as HIDDEN ordinamento')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('ordinamento');

        $query = $qb->getQuery();

        return $query->getResult();

    }


}