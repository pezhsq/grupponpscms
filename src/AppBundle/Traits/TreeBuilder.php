<?php
// 18/01/17, 11.05
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Traits;


trait TreeBuilder {

    public function createTree($element, $allElements) {

        $element->buildTree($allElements);

        $Childs = $element->getChildNodes();

        if($Childs) {
            foreach($Childs as $child) {
                $this->createTree($child, $allElements);
            }
        }

        return $element;

    }

    public function jsonTree($element, $allElements, $arr = []) {

        $element->buildTree($allElements);

        $json['text']     = $element->__toString();
        $json['id']       = $element->getId();
        $json['children'] = [];

        $Childs = $element->getChildNodes();

        if($Childs) {
            foreach($Childs as $child) {
                $json['children'][] = array_merge($this->jsonTree($child, $allElements, $arr), $arr);
            }
        }

        $json = array_merge($json, $arr);

        return $json;

    }

}