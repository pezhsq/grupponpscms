<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace AppBundle\Traits;

trait Loggable
{
    public function getUpdateLogMessage(array $changeSets = [])
    {
        $message = [];
        foreach ($changeSets as $property => $changeSet) {
            for ($i = 0, $s = count($changeSet); $i < $s; ++$i) {
                if ($changeSet[$i] instanceof \DateTime) {
                    $changeSet[$i] = $changeSet[$i]->format('Y-m-d H:i:s');
                } elseif ($changeSet[$i] instanceof \DateTimeImmutable) {
                    $changeSet[$i] = $changeSet[$i]->format('Y-m-d H:i:s');
                }
            }
            if ($changeSet[0] != $changeSet[1]) {
                $message[] = sprintf(
                    'Proprietà "**%s**" cambiata da "%s" a "%s"',
                    $property,
                    !is_array($changeSet[0]) ? $changeSet[0] : 'an array',
                    !is_array($changeSet[1]) ? $changeSet[1] : 'an array'
                );
            }
        }

        return implode('||', $message);
    }

    public function getCreateLogMessage(\ReflectionClass $reflectionClass)
    {
        foreach ($reflectionClass->getMethods() as $method) {
            /**
             * @var \ReflectionMethod
             */
            $methodName = $method->getName();
            if (!in_array(
                    $methodName,
                    [
                        'getCreateLogMessage',
                        'getRemoveLogMessage',
                        'getUpdateLogMessage',
                        'getUploadDir',
                        'getCurrentLocale',
                        'getTranslationEntityClass',
                        'getDefaultLocale',
                        'getTranslatableEntityClass',
                    ]
                ) && 0 === strpos(
                    $methodName,
                    'get'
                ) && !$method->getParameters()) {
                $data = $this->{$method->getName()}();
                if (is_string($data) || is_numeric($data) || is_bool($data)) {
                    $message[] = sprintf(
                        'Proprietà **%s** creata con "%s"',
                        str_replace('get', '', strtolower($methodName)),
                        $data
                    );
                } elseif (is_array($data)) {
                    foreach ($data as $k => $element) {
                        if (is_string($data) || is_numeric($data) || is_bool($data)) {
                            $message[] = sprintf(
                                'Proprietà **%s** creata con "%s"',
                                str_replace('get', '', strtolower($methodName)).'('.$k.')',
                                $element
                            );
                        }
                    }
                }
            }
        }

        return implode('||', $message);
    }

    public function getRemoveLogMessage()
    {
        return 'Entity rimossa';
    }
}
