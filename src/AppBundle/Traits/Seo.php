<?php
// 26/04/17, 9.17
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait Seo
{

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $slug;
    
    /**
     * @return mixed
     */
    public function getMetaTitle()
    {

        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {

        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {

        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {

        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {

        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {

        $this->slug = $slug;
    }

}
