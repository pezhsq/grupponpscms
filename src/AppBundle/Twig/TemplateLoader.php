<?php
// 30/01/17, 8.36
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Twig;


class TemplateLoader extends \Twig_Extension
{

    public function getFunctions()
    {

        return [
            new \Twig_SimpleFunction(
                'loadWidgets', [$this, 'loadWidgets'], [
                'needs_context' => true,
                'is_safe'       => ['html'],
            ]
            ),
        ];
    }

    public function loadWidgets($context, $file, $sezione)
    {

        $fileContent = false;

        if (isset($context['twigs']['sections'][$file])) {
            $fileContent = $context['twigs']['sections'][$file];
        }

        $out = '';

        if ($fileContent) {

            if (isset($fileContent['widgets'][$sezione])) {

                $data = $fileContent['widgets'][$sezione];

                foreach ($data as $widget) {
                    $out .= $widget['out'][$context['locale']];
                }

            }

        }

        return $out;
    }

}