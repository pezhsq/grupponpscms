<?php
// 10/03/17, 16.14
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Twig;


use Symfony\Component\DependencyInjection\ContainerInterface;

class Bundles extends \Twig_Extension {

    protected $container;

    public function __construct(ContainerInterface $container) {

        $this->container = $container;
    }

    public function getFunctions() {

        return [new \Twig_SimpleFunction('bundleExists', [$this, 'bundleExists'])];

    }

    public function bundleExists($bundle) {

        return array_key_exists($bundle, $this->container->getParameter('kernel.bundles'));

    }

    public function getName() {

        return 'admin_bundles';
    }

}
