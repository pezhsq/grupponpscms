<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 07/12/16
 * Time: 9.28
 */

namespace AppBundle\Twig;


use AppBundle\Entity\User;
use AppBundle\Service\Base64Image;

class ProfilePhoto extends \Twig_Extension {

    /**
     * @var Base64Image
     */
    private $imageService;

    public function __construct(Base64Image $imageService) {
        $this->imageService = $imageService;
    }

    public function getFilters() {
        return [
            new \Twig_SimpleFilter('base64Url', [$this, 'composeUrl']),
            new \Twig_SimpleFilter('jsonDecode', [$this, 'jsonDecode']),
        ];
    }

    public function composeUrl($Obj, $filename, $default) {

        return $this->imageService->getUrl($Obj, $filename, $default);

    }

    public function getName() {

        return 'profile_photo';

    }

    public function jsonDecode($str) {
        return json_decode($str);
    }

}