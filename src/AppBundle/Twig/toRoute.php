<?php
// 09/02/17, 13.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Twig;


use AppBundle\Service\PathManager;

class toRoute extends \Twig_Extension
{

    /**
     * @var PathManager
     */
    private $pathManager;

    public function __construct(PathManager $pathManager)
    {

        $this->pathManager = $pathManager;

    }


    public function getFunctions()
    {

        return [
            new \Twig_SimpleFunction('to_route', [$this, 'getRoute'], ['needs_context' => true]),
        ];
    }

    public function getRoute($context, $routeName, $params = [], $absoluteUrl = false, $forzaLocale = false)
    {

        $locale = $context['app']->getRequest()->getLocale();

        if ($forzaLocale) {
            $locale = 'it';
        }
        
        $url = $this->pathManager->generate($routeName, $params, $locale, $absoluteUrl, $debug = $absoluteUrl);

        return $url;

    }


}