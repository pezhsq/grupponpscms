<?php
/**
 * Created by PhpStorm.
 * User: gabricom
 * Date: 18/07/17
 * Time: 11.41
 */

namespace AppBundle\Twig;

class LayoutAssets extends \Twig_Extension
{

    private $assetManager;
    private $path;

    public function __construct($assetManager, $generali)
    {

        $this->assetManager = $assetManager;
        $this->path = "public/" . $generali['layout'] . "/";
    }

    public function getFunctions()
    {

        return [
            new \Twig_SimpleFunction('assetLayout', [$this, 'assetLayout'])];
    }

    public function assetLayout($string)
    {
        return $this->assetManager->getUrl($this->path . $string);
    }

}
