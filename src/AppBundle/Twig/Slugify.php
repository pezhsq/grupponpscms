<?php
// 02/03/17, 14.10
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Twig;


use AppBundle\Service\PathManager;

class Slugify extends \Twig_Extension {

    /**
     * @var \Cocur\Slugify\Slugify
     */
    private $slugify;

    public function __construct(\Cocur\Slugify\Slugify $slugify) {

        $this->slugify = $slugify;

        $this->slugify->addRule('à', 'a');
        $this->slugify->addRule('è', 'e');
        $this->slugify->addRule('é', 'e');
        $this->slugify->addRule('ì', 'i');
        $this->slugify->addRule('ò', 'o');
        $this->slugify->addRule('ù', 'u');

    }
    
    public function getFunctions() {

        return [
            new \Twig_SimpleFunction('slugify', [$this, 'Slugify'])];
    }

    public function slugify($string) {

        return $this->slugify->slugify($string);
    }

}
