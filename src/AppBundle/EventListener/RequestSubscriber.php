<?php
// 26/01/17, 11.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\EventListener;


use AppBundle\Service\AssetsManager;
use AppBundle\Service\Languages;
use AppBundle\Service\LocalizationUrls;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RequestSubscriber implements EventSubscriberInterface
{

    /**
     * @var Languages
     */
    private $languages;
    /**
     * @var LocalizationUrls
     */
    private $localizationUrls;
    /**
     * @var
     */
    private $environment;
    /**
     * @var AssetsManager
     */
    private $assetsManager;

    private $maintenance;
    private $maintenanceMessage;
    private $container;

    public function __construct(ContainerInterface $container)
    {

        $this->container = $container;

        // Languages $languages, LocalizationUrls $localizationUrls, $environment, AssetsManager $assetsManager
        $this->languages = $container->get('app.languages');
        $this->localizationUrls = $container->get('app.localization_urls');
        $this->environment = $container->getParameter('kernel.environment');
        $this->assetsManager = $container->get('app.assets_manager');

        $parametriAvanzati = $container->getParameter('parametri-avanzati');

        $this->maintenance = $parametriAvanzati['maintenance'];
        $this->maintenanceMessage = $parametriAvanzati['maintenance_message'];
        $this->router = $container->get('router');


    }

    public function onKernelRequest(KernelEvent $event)
    {

        $request = $event->getRequest();
        $grant = $this->container->get('security.authorization_checker');

        $isAdmin = (strpos($request->getPathInfo(), '/admin') !== false) || (strpos(
                    $request->getPathInfo(),
                    '/login'
                ) !== false);

        if (!in_array($this->environment, ['test', 'dev']) && $this->maintenance && !$isAdmin && !$grant->isGranted(
                'ROLE_USER'
            )
        ) {

            $twig = $this->container->get('templating');

            $content = $twig->render(
                ':public:manutenzione.html.twig',
                ['maintenanceUntil' => $this->maintenanceMessage]
            );
            $event->setResponse(new Response($content, 503));
            $event->stopPropagation();

        } else {


            $requestedLocale = $request->getLocale();

            $this->localizationUrls->setLocalizedUrls();

            if (in_array('WebtekEcommerceBundle', array_keys($this->container->getParameter('kernel.bundles')))) {
                if (!$this->container->get('session')->get('uuid')) {
                    $uuid = Uuid::uuid1();
                    $this->container->get('session')->set('uuid', (string)$uuid);
                }
            }

            if (!in_array($requestedLocale, array_keys($this->languages->getActivePublicLanguages()))) {
                $attributes = $request->attributes->all();
                $query = false;
                if (isset($attributes['_route_params'])) {
                    unset($attributes['_route_params']['_locale']);
                    $query = $attributes['_route_params'];
                }
                if (!$query) {
                    //dump('Hai richiesto una lingua non attiva, senza path, ti rimando alla home(301)');
                    $response = new RedirectResponse('/', 301);
                    $event->setResponse($response);
                } else {
                    throw new NotFoundHttpException('L\'url richiesto non esiste');
                }
            }

            if ($this->environment == 'dev' && !$isAdmin) {
                $this->assetsManager->compile();
            }


        }
    }

    public
    static function getSubscribedEvents()
    {

        return ['kernel.request', 'onKernelRequest'];
    }


}