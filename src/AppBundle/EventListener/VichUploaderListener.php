<?php
// 21/06/17, 6.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\EventListener;

use AppBundle\Service\ImageHelper;
use AppBundle\Service\ParameterReader;
use Imagine\Gd\Image;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Event\Event;

class VichUploaderListener
{

    /**
     * @var ImageHelper
     */
    private $imageHelper;
    private $liipImagineFilters;
    /**
     * @var CacheManager
     */
    private $cacheManager;

    public function __construct(ImageHelper $imageHelper, $liipImagineFilters, CacheManager $cacheManager)
    {

        $this->imageHelper = $imageHelper;
        $this->liipImagineFilters = $liipImagineFilters;
        $this->cacheManager = $cacheManager;
    }

    public function onVichuploaderPreUpload(Event $event)
    {

        $object = $event->getObject();
        $mapping = $event->getMapping();

        $directoryNamer = $mapping->getDirectoryNamer();

        if ($mapping->getFileName($object)) {
            $path = $mapping->getUploadDestination().'/'.$mapping->getUploadDir($object).'/'.$mapping->getFileName(
                    $object
                );
            unlink($path);
        }

    }

    public function onVichuploaderPostupload(Event $event)
    {

        $object = $event->getObject();
        $mapping = $event->getMapping();

        $method = 'get'.$mapping->getFilePropertyName();

        /**
         * @var $File File
         */
        $File = $object->$method();

        if (in_array($File->getExtension(), ['jpg', 'png'])) {

            $directoryNamer = $mapping->getDirectoryNamer();

            $path = $mapping->getUploadDestination().'/'.$mapping->getUploadDir($object).'/'.$mapping->getFileName(
                    $object
                );

            $property = $mapping->getFilePropertyName().'Data';
            if (property_exists($object, $property)) {

                $method = 'get'.$property;
                $resizeData = json_decode($object->$method(), true);
                if ($resizeData) {
                    $this->imageHelper->resize($path, $resizeData);
                } else {
                    $this->imageHelper->compress($path);
                }
            } else {
                $this->imageHelper->compress($path);
            }

            foreach ($this->liipImagineFilters as $filter => $filterData) {
                try {
                    $path = realpath($path);

                    $path = $mapping->getUriPrefix().'/'.$mapping->getUploadDir($object).'/'.$mapping->getFileName(
                            $object
                        );

                    $pathRisolto = $this->cacheManager->resolve($path, $filter);

                    $this->cacheManager->remove($path, $filter);
                } catch (\Exception $e) {

                }
            }

        }

    }
}