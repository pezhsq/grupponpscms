<?php
// 28/06/17, 6.23
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupRepository")
 * @ORM\Table(name="groups")
 * @UniqueEntity("name")
 */
class Group
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string")
     */
    private $role;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\MappedRoles", inversedBy="groups")
     * @ORM\JoinTable(name="group_has_roles",
     *   inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
     *   joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")})
     */
    private $roles;

    public function __construct()
    {

        $this->level = 10000;
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {

        $this->name = $name;
        $this->setRole('ROLE_'.strtoupper(str_replace(' ', '_', $name)));
    }

    /**
     * @return mixed
     */
    public function getRole()
    {

        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {

        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {

        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {

        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getisEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    public function __toString()
    {

        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {

        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {

        $this->level = $level;
    }


}