<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 19/12/16
 * Time: 16.49
 */

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRowRepository")
 * @ORM\Table(name="news_rows")
 * @Vich\Uploadable()
 */
class NewsRows
{

    use ORMBehaviours\Sortable\Sortable, ORMBehaviours\Timestampable\Timestampable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\News", inversedBy="newsRows")
     */
    private $news;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $slot1;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $slot1Image;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $slot2Image;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $slot3Image;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $slot1ImageAlt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $slot2ImageAlt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $slot3ImageAlt;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $slot2;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $slot3;

    /**
     * @ORM\Column(type="text")
     */
    private $structure;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $locale;

    /**
     * @Vich\UploadableField(mapping="news", fileNameProperty="slot1Image")
     *
     * @var File
     */
    private $slot1ImageFile;

    /**
     * @Vich\UploadableField(mapping="news", fileNameProperty="slot2Image")
     *
     * @var File
     */
    private $slot2ImageFile;

    /**
     * @Vich\UploadableField(mapping="news", fileNameProperty="slot3Image")
     *
     * @var File $slot3ImageFile
     */
    private $slot3ImageFile;

    /**
     * @return mixed
     */
    public function getSlot1ImageAlt()
    {

        return $this->slot1ImageAlt;
    }

    /**
     * @param mixed $slot1ImageAlt
     */
    public function setSlot1ImageAlt($slot1ImageAlt)
    {

        $this->slot1ImageAlt = $slot1ImageAlt;
    }

    /**
     * @return mixed
     */
    public function getSlot2ImageAlt()
    {

        return $this->slot2ImageAlt;
    }

    /**
     * @param mixed $slot2ImageAlt
     */
    public function setSlot2ImageAlt($slot2ImageAlt)
    {

        $this->slot2ImageAlt = $slot2ImageAlt;
    }

    /**
     * @return mixed
     */
    public function getSlot3ImageAlt()
    {

        return $this->slot3ImageAlt;
    }

    /**
     * @param mixed $slot3ImageAlt
     */
    public function setSlot3ImageAlt($slot3ImageAlt)
    {

        $this->slot3ImageAlt = $slot3ImageAlt;
    }

    /**
     * @return File
     */
    public function getSlot1ImageFile()
    {

        return $this->slot1ImageFile;
    }

    /**
     * @param File|UploadedFile $slot1ImageFile
     */
    public function setSlot1ImageFile(File $slot1ImageFile = null)
    {

        $this->slot1ImageFile = $slot1ImageFile;
        if ($slot1ImageFile) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getSlot2ImageFile()
    {

        return $this->slot2ImageFile;
    }

    /**
     * @param File $slot2ImageFile
     */
    public function setSlot2ImageFile($slot2ImageFile)
    {

        $this->slot2ImageFile = $slot2ImageFile;
        if ($slot2ImageFile) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getSlot3ImageFile()
    {

        return $this->slot3ImageFile;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $slot3ImageFile
     */
    public function setSlot3ImageFile(File $slot3ImageFile)
    {

        $this->slot3ImageFile = $slot3ImageFile;
        if ($slot3ImageFile) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSlot1()
    {

        return $this->slot1;
    }

    /**
     * @param mixed $slot1
     */
    public function setSlot1($slot1)
    {

        $this->slot1 = $slot1;
    }

    /**
     * @return mixed
     */
    public function getSlot2()
    {

        return $this->slot2;
    }

    /**
     * @param mixed $slot2
     */
    public function setSlot2($slot2)
    {

        $this->slot2 = $slot2;
    }

    /**
     * @return mixed
     */
    public function getSlot3()
    {

        return $this->slot3;
    }

    /**
     * @param mixed $slot3
     */
    public function setSlot3($slot3)
    {

        $this->slot3 = $slot3;
    }

    /**
     * @return mixed
     */
    public function getStructure()
    {

        return $this->structure;
    }

    public function getStructureObject()
    {

        return json_decode($this->structure, true);

    }

    /**
     * @param mixed $structure
     */
    public function setStructure($structure)
    {

        $this->structure = $structure;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {

        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {

        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getNews()
    {

        return $this->news;
    }

    /**
     * @param mixed $news
     */
    public function setNews(News $news)
    {

        $this->news = $news;
    }

    function __toString()
    {

        return $this->getId().' - '.json_encode($this->getStructure());
    }

    /**
     * @return mixed
     */
    public function getSlot1Image()
    {

        return $this->slot1Image;
    }

    /**
     * @param mixed $slot1Image
     */
    public function setSlot1Image($slot1Image)
    {

        $this->slot1Image = $slot1Image;
    }

    /**
     * @return mixed
     */
    public function getSlot2Image()
    {

        return $this->slot2Image;
    }

    /**
     * @param mixed $slot2Image
     */
    public function setSlot2Image($slot2Image)
    {

        $this->slot2Image = $slot2Image;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getSlot3Image()
    {

        return $this->slot3Image;
    }

    /**
     * @param mixed $slot3Image
     */
    public function setSlot3Image($slot3Image)
    {

        $this->slot3Image = $slot3Image;

        return $this;
    }


}
