<?php
// 09/01/17, 16.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use AppBundle\Validator\Constraints as WebTekAssert;

/**
 * @WebTekAssert\MaxCategories
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategorieNewsRepository")
 * @ORM\Table(name="newscategory")
 */
class NewsCategory
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Sortable\Sortable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\NewsHasCategory", mappedBy="category_id")
     */
    private $news_categories_association;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $template = 'blog';

    public function __construct()
    {

        $this->news_categories_association = new ArrayCollection();

    }

    public function addNewsCategoriesAssociation(NewsHasCategory $newsHasCategory)
    {

        $this->news_categories_association[] = $newsHasCategory;
    }

    public function removeNewsCategoriesAssociation(NewsHasCategory $newsHasCategory)
    {

        $this->news_categories_association->removeElement($newsHasCategory);

    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {

        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {

        $this->template = $template;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {

        /**
         * @var $object NewsCategory
         */
        $object = $context->getObject();
        $translations = $object->getTranslations();
        foreach ($translations as $translation) {
            if (false == $translation->getTitolo()) {
                $context->buildViolation('newscat.titolo_empty_no_lang')->atPath(
                        'translations['.$translation->getLocale().'].titolo'
                    )->addViolation();
                $context->buildViolation('newscat.titolo_empty', ['lang' => $translation->getLocale()])->atPath(
                        'translations_'.$translation->getLocale().'_titolo'
                    )->addViolation();
            }
        }

    }

    public function __toString()
    {

        return (string)$this->getId();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }


}
