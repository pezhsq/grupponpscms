<?php
// 09/01/17, 16.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategorieGalleryRepository")
 * @ORM\Table(name="gallerycategory")
 * @Vich\Uploadable()
 */
class GalleryCategory
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Sortable\Sortable, ORMBehaviours\Translatable\Translatable, Loggable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $template = 'default';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gallery", mappedBy="category")
     */
    private $galleries;

    /**
     * @Vich\UploadableField(mapping="catgallery", fileNameProperty="listImgFileName")
     */
    private $listImg;

    public $listImgData;
    private $listImgDelete;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgAlt;

    /**
     * @Vich\UploadableField(mapping="catgallery", fileNameProperty="headerImgFileName")
     */
    private $headerImg;

    public $headerImgData;
    private $headerImgDelete;

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {

        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {

        $this->template = $template;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {

        /**
         * @var $object NewsCategory
         */
        $object = $context->getObject();
        $translations = $object->getTranslations();
        foreach ($translations as $translation) {
            if (false == $translation->getTitolo()) {
                $context->buildViolation('gallerycat.titolo_empty_no_lang')->atPath(
                        'translations['.$translation->getLocale().'].titolo'
                    )->addViolation();
                $context->buildViolation('gallerycat.titolo_empty', ['lang' => $translation->getLocale()])->atPath(
                        'translations_'.$translation->getLocale().'_titolo'
                    )->addViolation();
            }
        }

    }

    public function __toString()
    {

        return (string)$this->translate()->get;
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return File
     */
    public function getListImg()
    {

        return $this->listImg;
    }

    public function setListImg($listImg = null)
    {

        $this->listImg = $listImg;
        $this->setUpdatedAt(new \DateTime());

        return $this;

    }

    /**
     * @return mixed
     */
    public function getListImgFileName()
    {

        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {

        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {

        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {

        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {

        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {

        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {

        return $this->listImgData;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgFileName()
    {

        return $this->headerImgFileName;
    }

    /**
     * @param mixed $headerImgFileName
     */
    public function setHeaderImgFileName($headerImgFileName)
    {

        $this->headerImgFileName = $headerImgFileName;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgAlt()
    {

        return $this->headerImgAlt;
    }

    /**
     * @param mixed $headerImgAlt
     */
    public function setHeaderImgAlt($headerImgAlt)
    {

        $this->headerImgAlt = $headerImgAlt;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgDelete()
    {

        return $this->headerImgDelete;
    }

    /**
     * @param mixed $headerImgDelete
     */
    public function setHeaderImgDelete($headerImgDelete)
    {

        $this->headerImgDelete = $headerImgDelete;
    }


    /**
     * @return mixed
     */
    public function getHeaderImg()
    {

        return $this->headerImg;
    }

    /**
     * @param mixed $headerImg
     */
    public function setHeaderImg($headerImg)
    {

        $this->headerImg = $headerImg;
        if ($headerImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgData()
    {

        return $this->headerImgData;
    }

    function getUploadDir()
    {

        return 'files/catgallery/'.$this->getId().'/';

    }


}
