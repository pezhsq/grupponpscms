<?php

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity()
 * @ORM\Table(name="email_template_translations")
 */
class EmailTemplateTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @return mixed
     */
    public function getSubject()
    {

        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {

        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getText()
    {

        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {

        $this->text = $text;
    }

}

