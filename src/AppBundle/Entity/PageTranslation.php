<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 19/12/16
 * Time: 8.39
 */

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @Gedmo\Loggable
 * @ORM\EntityListeners({"AppBundle\EntityListener\PageTranslationListener"})
 * @ORM\Table(name="page_translations")
 */
class PageTranslation
{

    use ORMBehaviours\Translatable\Translation, Loggable;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $titolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sottotitolo;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $metaTitle;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $metaDescription;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $shortUrl;

    /**
     * @ORM\Column(type = "string", nullable=true)
     * @var string $titolo_menu
     * Un titolo breve, ideato per essere inserito nel menu, in caso di necessità
     */
    private $titolo_menu;

    /**
     * @return string
     */
    public function getTitoloMenu()
    {

        return $this->titolo_menu;
    }

    /**
     * @param string $titolo_menu
     * @return $this
     */
    public function setTitoloMenu($titolo_menu)
    {

        $this->titolo_menu = $titolo_menu;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getTitolo()
    {

        return $this->titolo ? $this->titolo : '-';
    }

    /**
     * @param mixed $titolo
     * @return $this
     */
    public function setTitolo($titolo)
    {

        if ($this->titolo_menu === null || $this->titolo_menu === "") {
            $this->titolo_menu = $titolo;
        }
        $this->titolo = $titolo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSottotitolo()
    {

        return $this->sottotitolo;
    }

    /**
     * @param mixed $sottotitolo
     */
    public function setSottotitolo($sottotitolo)
    {

        $this->sottotitolo = $sottotitolo;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {

        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {

        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {

        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {

        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {

        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {

        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getTranslatable()
    {

        return $this->translatable;
    }

    public function setTranslatableId($id)
    {

        $this->translatable_id = $id;
    }

    /**
     * @return mixed
     */
    public function getShortUrl()
    {

        return $this->shortUrl;
    }

    /**
     * @param mixed $shortUrl
     */
    public function setShortUrl($shortUrl)
    {

        $this->shortUrl = $shortUrl;
    }


}