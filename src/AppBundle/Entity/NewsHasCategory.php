<?php
// 16/01/17, 14.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsHasCategoriesRepository")
 * @ORM\Table(name="news_has_categories")
 */
class NewsHasCategory {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\News", inversedBy="news_categories_association")
     * @ORM\JoinColumn(name="news_id", referencedColumnName="id")
     */
    private $news;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\NewsCategory", inversedBy="news_categories_association")
     * @ORM\JoinColumn(name="news_category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isMain;

    public function setNews(News $news = null) {

        $this->news = $news;

        return $this;

    }

    public function getNews() {

        return $this->news;

    }

    public function setCategory(NewsCategory $newsCategory) {

        $this->category = $newsCategory;

        return $this;

    }

    public function getCategory() {

        return $this->category;

    }

    /**
     * @return mixed
     */
    public function getIsMain() {

        return $this->isMain;
    }

    /**
     * @param mixed $isMain
     */
    public function setIsMain($isMain) {

        $this->isMain = $isMain;
    }

    /**
     * @return mixed
     */
    public function getId() {

        return $this->id;
    }

    public function __toString() {

        return $this->getId().': News('.$this->getNews()->getId().'), Category('.$this->getCategory()->getId().'), isMain('.$this->getIsMain().')';

    }


}