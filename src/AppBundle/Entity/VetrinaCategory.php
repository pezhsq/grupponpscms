<?php
// 09/01/17, 16.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategorieVetrinaRepository")
 * @ORM\Table("vetrina_category")
 */
class VetrinaCategory
{

    use ORMBehaviours\SoftDeletable\SoftDeletable, ORMBehaviours\Timestampable\Timestampable, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $nome;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Regex("/^[\d+x\d+]|[nessuna]$/")
     */
    private $dimensioni;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Vetrina", mappedBy="category")
     * @ORM\OrderBy({"sort" ="ASC"})
     */
    private $vetrine;

    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDimensioni()
    {

        return $this->dimensioni;
    }

    /**
     * @param mixed $dimensioni
     */
    public function setDimensioni($dimensioni)
    {

        $this->dimensioni = $dimensioni;
    }

    function __toString()
    {

        return $this->getNome().' ('.$this->getDimensioni().')';
    }

    /**
     * @return mixed
     */
    public function getVetrine()
    {

        return $this->vetrine;
    }

    /**
     * @param mixed $vetrine
     */
    public function setVetrine($vetrine)
    {

        $this->vetrine = $vetrine;
    }


}