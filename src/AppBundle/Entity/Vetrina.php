<?php
// 09/01/17, 15.48
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VetrinaRepository")
 * @ORM\EntityListeners({"AppBundle\EntityListener\VetrinaListener"})
 * @ORM\Table(name="vetrina")
 * @Vich\Uploadable()
 */
class Vetrina
{

    use Loggable, ORMBehaviours\Sortable\Sortable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, ORMBehaviours\SoftDeletable\SoftDeletable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\VetrinaCategory")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    /**
     * @Vich\UploadableField(mapping="vetrina", fileNameProperty="listImgFileName")
     */
    private $listImg;

    public $listImgData;
    private $listImgDelete;
    private $imageData;


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {

        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {

        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory(VetrinaCategory $category)
    {

        $this->category = $category;
    }

    function getUploadDir()
    {

        return 'files/vetrina/'.$this->getId().'/';

    }

    /**
     * @return mixed
     */
    public function getImageData()
    {

        return $this->imageData;
    }

    /**
     * @param mixed $imageData
     */
    public function setImageData($imageData)
    {

        $this->imageData = $imageData;
    }

    /**
     * @return File
     */
    public function getListImg()
    {

        return $this->listImg;
    }

    public function setListImg($listImg = null)
    {

        $this->listImg = $listImg;
        if ($listImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;

    }

    /**
     * @return mixed
     */
    public function getListImgFileName()
    {

        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {

        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {

        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {

        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {

        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {

        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {

        return $this->listImgData;
    }


}