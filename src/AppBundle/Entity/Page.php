<?php

namespace AppBundle\Entity;

use AppBundle\Traits\Loggable;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 * @ORM\EntityListeners({"AppBundle\EntityListener\PageListener"})
 * @ORM\Table(name="pages")
 * @Vich\Uploadable()
 */
class Page implements ORMBehaviours\Tree\NodeInterface
{

    use ORMBehaviours\Sortable\Sortable, ORMBehaviours\Timestampable\Timestampable, ORMBehaviours\Translatable\Translatable, ORMBehaviours\Tree\Node, ORMBehaviours\SoftDeletable\SoftDeletable, \AppBundle\Traits\TreeBuilder, Loggable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PageRows", mappedBy="page", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $pageRows;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PageAttachment", mappedBy="page", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC", "id" = "ASC"})
     *
     */
    private $attachments;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     */
    private $template;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string")
     */
    private $robots;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $listImgAlt;

    /**
     * @Vich\UploadableField(mapping="pagine", fileNameProperty="listImgFileName")
     */
    private $listImg;

    public $listImgData;
    private $listImgDelete;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgFileName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $headerImgAlt;

    /**
     * @Vich\UploadableField(mapping="pagine", fileNameProperty="headerImgFileName")
     */
    private $headerImg;

    public $headerImgData;
    private $headerImgDelete;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $parent;


    private $uuid;


    public function __construct()
    {

        $this->pageRows = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->setRobots('index,follow');

    }

    public function setId($id)
    {

        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRobots()
    {

        return $this->robots;
    }

    /**
     * @param mixed $robots
     */
    public function setRobots($robots)
    {

        $this->robots = $robots;
    }


    /**
     * @return int Id pagina genitore
     */
    public function getParent()
    {

        return $this->parent;

    }

    /**
     * @param Page $parent
     */
    public function setParent($parent)
    {

        $this->parent = $parent;
    }


    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {

        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {

        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {

        $this->template = $template;
    }


    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }


    /**
     * @return ArrayCollection
     */
    public function getPageRows()
    {

        return $this->pageRows;
    }


    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {

        /**
         * @var $object Page
         */
        $object = $context->getObject();
        $translations = $object->getTranslations();
        foreach ($translations as $translation) {

            if ($this->getTemplate() == 'home' && !$translation->getIsEnabled()) {
                $context->buildViolation('pages.isnotenabledhome')->atPath(
                    'translations['.$translation->getLocale().'].isEnabled'
                )->addViolation();
                $context->buildViolation('pages.isnotenabledhome')->addViolation();
            }
            if ($translation->getIsEnabled()) {
                if ($translation->getTitolo() == '-') {
                    $context->buildViolation('pages.titolo_empty')->atPath(
                        'translations['.$translation->getLocale().'].titolo'
                    )->addViolation();
                    $context->buildViolation('pages.titolo_empty')->addViolation();
                }
            }
        }

    }


    /**
     * Add pageRow
     *
     * @param \AppBundle\Entity\PageRows $pageRow
     *
     * @return Page
     */
    public function addPageRow(\AppBundle\Entity\PageRows $pageRow)
    {

        $pageRow->setPage($this);
        $this->pageRows[] = $pageRow;

        return $this;
    }

    /**
     * Remove pageRow
     *
     * @param \AppBundle\Entity\PageRows $pageRow
     */
    public function removePageRow(\AppBundle\Entity\PageRows $pageRow)
    {

        $this->pageRows->removeElement($pageRow);
    }


    public function getOptionLabel()
    {

        return str_repeat(
                html_entity_decode('&nbsp;&nbsp;&nbsp;&nbsp;', ENT_QUOTES, 'UTF-8'),
                $this->getNodeLevel() - 1
            ).' +--+ '.$this->translate('it')->getTitolo();
    }

    function __toString()
    {

        return $this->translate('it')->getTitolo();
    }

    function getUploadDir()
    {

        return 'files/pages/'.$this->getId().'/';

    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {

        return $this->attachments;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {

        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {

        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {

        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {

        $this->level = $level;
    }


    /**
     * Add attachment
     *
     * @param \AppBundle\Entity\PageAttachment $attachment
     *
     * @return Page
     */
    public function addAttachment(\AppBundle\Entity\PageAttachment $attachment)
    {

        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \AppBundle\Entity\PageAttachment $attachment
     */
    public function removeAttachment(\AppBundle\Entity\PageAttachment $attachment)
    {

        $this->attachments->removeElement($attachment);
    }
    
    /** IMMAGINI **/
    /**
     * @return File
     */
    public function getListImg()
    {

        return $this->listImg;
    }

    public function setListImg($listImg = null)
    {

        $this->listImg = $listImg;
        if ($listImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;

    }

    /**
     * @return mixed
     */
    public function getListImgFileName()
    {

        return $this->listImgFileName;
    }

    /**
     * @param mixed $listImgFileName
     */
    public function setListImgFileName($listImgFileName)
    {

        $this->listImgFileName = $listImgFileName;
    }

    /**
     * @return mixed
     */
    public function getListImgAlt()
    {

        return $this->listImgAlt;
    }

    /**
     * @param mixed $listImgAlt
     */
    public function setListImgAlt($listImgAlt)
    {

        $this->listImgAlt = $listImgAlt;
    }

    /**
     * @return mixed
     */
    public function getListImgDelete()
    {

        return $this->listImgDelete;
    }

    /**
     * @param mixed $listImgDelete
     */
    public function setListImgDelete($listImgDelete)
    {

        $this->listImgDelete = $listImgDelete;
    }

    /**
     * @return mixed
     */
    public function getListImgData()
    {

        return $this->listImgData;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgFileName()
    {

        return $this->headerImgFileName;
    }

    /**
     * @param mixed $headerImgFileName
     */
    public function setHeaderImgFileName($headerImgFileName)
    {

        $this->headerImgFileName = $headerImgFileName;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgAlt()
    {

        return $this->headerImgAlt;
    }

    /**
     * @param mixed $headerImgAlt
     */
    public function setHeaderImgAlt($headerImgAlt)
    {

        $this->headerImgAlt = $headerImgAlt;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgDelete()
    {

        return $this->headerImgDelete;
    }

    /**
     * @param mixed $headerImgDelete
     */
    public function setHeaderImgDelete($headerImgDelete)
    {

        $this->headerImgDelete = $headerImgDelete;
    }


    /**
     * @return mixed
     */
    public function getHeaderImg()
    {

        return $this->headerImg;
    }

    /**
     * @param mixed $headerImg
     */
    public function setHeaderImg($headerImg)
    {

        $this->headerImg = $headerImg;
        if ($headerImg) {

            $this->setUpdatedAt(new \DateTime());

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaderImgData()
    {

        return $this->headerImgData;
    }


}
