<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MetaRepository")
 * @ORM\Table(name="meta",uniqueConstraints={@UniqueConstraint(name="unicita_gruppo", columns={"chiave", "group_id"})})
 * @UniqueEntity(
 *     fields={"chiave", "group"},
 *     errorPath="chiave",
 *     message="Esiste già una chiave con questo nome per questo gruppo"
 * )
 */
class Meta
{

    use ORMBehaviours\SoftDeletable\SoftDeletable,
        ORMBehaviours\Timestampable\Timestampable,
        ORMBehaviours\Translatable\Translatable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MetaGroup", inversedBy="meta")
     */
    private $group;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     */
    private $chiave;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $aiuto;

    /**
     * @ORM\Column(type="string")
     */
    private $etichetta;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    public function __toString()
    {

        return (string)$this->getId();
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {

        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {

        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getChiave()
    {

        return $this->chiave;
    }

    /**
     * @param mixed $chiave
     */
    public function setChiave($chiave)
    {

        $this->chiave = $chiave;
    }

    /**
     * @return mixed
     */
    public function getEtichetta()
    {

        return $this->etichetta;
    }

    /**
     * @param mixed $etichetta
     */
    public function setEtichetta($etichetta)
    {

        $this->etichetta = $etichetta;
    }

    /**
     * @return mixed
     */
    public function getAiuto()
    {

        return $this->aiuto;
    }

    /**
     * @param mixed $aiuto
     */
    public function setAiuto($aiuto)
    {

        $this->aiuto = $aiuto;
    }
    

}
