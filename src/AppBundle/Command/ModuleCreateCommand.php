<?php
// 21/03/17, 17.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Finder\Finder;

class ModuleCreateCommand extends ContainerAwareCommand
{

    private $container;

    private $modulesDir;

    private $modules;

    private $stats
        = [
            'contaModuli' => 0,
            'versioni'    => 0,
        ];

    private $types
        = [
            'simpleText',
            'mediumText',
            'link',
            'image',
            'file',
            'simpleTextarea',
            'boolean',
            'arrayOf',
            'backgroundImage',
        ];

    protected function configure()
    {

        $this->setName('app:module:create')
            ->setDescription('Tool per la creazione di un modulo')
            ->setHelp('Grazie a questo tool creare un modulo sarà meno palloso');
    }

    private function setUp(OutputInterface $output)
    {

        $style = new OutputFormatterStyle('white', 'green', ['blink']);
        $output->getFormatter()->setStyle('webtek', $style);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getContainer();

        $this->modulesDir = $this->container->get('kernel')->getRootDir().'/Resources/views/modules';

        $this->setUp($output);
        $output->writeln('');
        $output->writeln('<webtek>--------------------------------------</webtek>');
        $output->writeln('<webtek>  Comando per la creazione di moduli  </webtek>');
        $output->writeln('<webtek>--------------------------------------</webtek>');
        $output->writeln('');

        $output->writeln('Indicizzazione in corso...');
        $this->moduleIndex();
        $output->writeln('Indicizzati '.$this->stats['contaModuli'].' moduli in '.$this->stats['versioni'].' versioni');


        $newModule          = [];
        $newModule['stats'] = [];
        foreach ($this->types as $type) {
            $newModule['stats'] = array_merge($newModule['stats'], $this->askHowManyForType($input, $output, $type));
        }

        $trovati = $this->cerca($newModule['stats']);

        if ($trovati) {
            $scelta = $this->showChoice($input, $output, $trovati);

            if ($scelta == '[NEW]') {
                $this->creaNuovoModulo($input, $output, $newModule);
            } else {
                $this->creaNuovaVersione($input, $output, $scelta);
            }
        } else {
            $this->creaNuovoModulo($input, $output, $newModule);
        }

    }


    protected function askHowManyForType(InputInterface $input, OutputInterface $output, $type)
    {

        $helper = $this->getHelper('question');

        $question = new Question('<question>Di quanti campi di tipo "'.$type.'" necessiti? [0]:</question> ', 0);

        while (!is_numeric($howMany = ($helper->ask($input, $output, $question)))) {
            $output->writeln('<error>Mi prendi in giro? Mi aspetto un numero</error>');
        }

        $howMany = intval($howMany);

        $questionMultilang = new Question(
            '<question>Di quanti campi di tipo "'.$type.'" multilingua necessiti? [0] </question>', 0
        );

        while (!is_numeric($howManyMultilang = ($helper->ask($input, $output, $questionMultilang)))) {

            if (!is_numeric($howManyMultilang)) {
                $output->writeln('<error>Mi prendi in giro? Mi aspetto un numero</error>');
            }

        }

        $output->writeln('');


        return [$type => $howMany, $type.':multilang' => intval($howManyMultilang)];


    }

    private function moduleIndex()
    {

        $Finder = new Finder();

        $Finder->depth(' < 1');

        $Dirs = $Finder->directories()->in($this->modulesDir);

        $modules = [];

        foreach ($Dirs as $Dir) {
            /**
             * @var $Dir \SplFileInfo
             */
            $dirs[] = $Dir->getRealPath();

            if (file_exists($Dir->getRealPath().'/config.json')) {

                $configurazioneModulo = json_decode(file_get_contents($Dir->getRealPath().'/config.json'), true);

                $stats = [];

                foreach ($this->types as $type) {
                    $stats[$type]              = 0;
                    $stats[$type.':multilang'] = 0;
                }

                if (isset($configurazioneModulo['config'])) {

                    $this->stats['contaModuli']++;

                    foreach ($configurazioneModulo['config'] as $k => $data) {
                        $key = $data['type'];
                        if (isset($data['multilang']) && $data['multilang']) {
                            $key .= ':multilang';
                        }
                        $stats[$key]++;
                    }

                    $modulo             = [];
                    $modulo['path']     = $Dir->getRealPath();
                    $modulo['name']     = $configurazioneModulo['nome'];
                    $modulo['stats']    = $stats;
                    $modulo['config']   = $configurazioneModulo['config'];
                    $modulo['versioni'] = [];


                    $Finder = new Finder();

                    $Finder->depth(' < 1');

                    $VersioniDirs = $Finder->directories()->in($Dir->getRealPath());


                    foreach ($VersioniDirs as $VersioneDir) {
                        /**
                         * @var \SplFileInfo $VersioneDir
                         */
                        $modulo['versioni'][] = $VersioneDir->getBasename();

                        $this->stats['versioni']++;

                    }


                    $modules[] = $modulo;
                }
            }
        }

        $this->modules = $modules;
    }

    protected function cerca($stats)
    {

        $trovati = [];

        foreach ($this->modules as $module) {
            if ($module['stats'] == $stats) {
                $trovati[] = $module['name'];
            }
        }

        return $trovati;


    }

    protected function showChoice(InputInterface $input, OutputInterface $output, $trovati)
    {

        $output->writeln('');

        $output->writeln(
            '<info>Sono stati trovati altri moduli che usano la medesima configurazione, decidi cosa fare: </info>'
        );
        $output->writeln('');

        foreach ($trovati as $k => $trovato) {
            $output->writeln(($k + 1).'. Creo una nuova versione di "'.$trovato.'"');
        }

        $ultima = $k + 2;
        $output->writeln($ultima.'. Creo un nuovo modulo ');

        $helper = $this->getHelper('question');

        $output->writeln('');

        $question = new Question('<question>Effettua una scelta ['.$ultima.']:</question> ', $ultima);

        while (!is_numeric($scelta = $helper->ask($input, $output, $question)) || $scelta < 1 || $scelta > $ultima) {
            if (!is_numeric($scelta)) {
                $output->writeln('<error>Cerchi di mettermi in difficoltà? Mi aspetto un numero maledizione</error>');
            } else {
                $output->writeln('<error>Devi indicare un numero tra 1 e '.$ultima.'</error>');
            }
        }

        if ($scelta == $ultima) {
            return '[NEW]';
        } else {
            return $trovati[$scelta - 1];
        }

    }

    protected function creaNuovaVersione(
        InputInterface $input,
        OutputInterface $output,
        $nomeModulo,
        $dataModulo = null
    ) {

        $re = '/^([A-Z]+)\.([\d+])\.([\d+])\.([\d+])$/';

        if (!$dataModulo) {

            $modulo = $this->cercaPerNome($output, $nomeModulo);

            $output->writeln('');
            $output->writeln('<info>Il modulo "'.$nomeModulo.'" ha le seguenti versioni:</info>');

            $ultimaVersione = false;

            foreach ($modulo['versioni'] as $k => $versione) {
                $output->writeln(($k + 1).'. '.$versione);


                preg_match_all($re, $versione, $m);

                if (!$ultimaVersione || $m[2][0] > $ultimaVersione[2][0]) {
                    $ultimaVersione = $m;
                }

            }


            $nuovaVersione = $ultimaVersione[1][0].'.'.($ultimaVersione[2][0] + 1).'.'.$ultimaVersione[3][0].'.'.$ultimaVersione[4][0];
        } else {

            preg_match_all('#([A-Z]+)#', $this->camelCase($dataModulo['nome']), $matches);

            $sigla = strtoupper($dataModulo['nome'][0].implode('', $matches[1]));

            $nuovaVersione = $sigla.'.1.0.0';

            $modulo           = [];
            $modulo['path']   = $this->modulesDir.'/'.$this->camelCase($dataModulo['nome']);
            $modulo['config'] = $dataModulo['config'];
        }

        $output->writeln('');

        $question = new Question(
            '<question>Quale numero di versione vuoi usare? Effettua una scelta ['.$nuovaVersione.']:</question> ',
            $nuovaVersione
        );

        $helper = $this->getHelper('question');

        while (!preg_match($re, $versione = $helper->ask($input, $output, $question)) || !$this->verificaVersioneUsata(
                $versione
            )) {

            if (!$this->verificaVersioneUsata($versione)) {

                $output->writeln('<error>Spiacente, questo numero di versione è stato già preso.</error>');

            } else {

                $output->writeln('<error>Il nome di versione scelto non è in un formato ritenuto accettabile.</error>');

            }

        }

        $path = $modulo['path'].'/'.$versione;

        if ($this->createIndexFile($input, $output, $path, $versione, $dataModulo)) {

            $this->createSampleTwig($input, $output, $modulo, $path);

            $question = new ConfirmationQuestion(
                '<question>Vuoi creare un javascript di default ?</question> ',
                false,
                '/^[y|s]/i'
            );

            if ($helper->ask($input, $output, $question)) {

                $this->creatSampleJs($input, $output, $modulo, $path);


            }

        }

    }

    protected function creaNuovoModulo(InputInterface $input, OutputInterface $output, $nuovaVersione)
    {

        $question = new Question('<question>Scegli un nome per il tuo modulo</question> ');

        $helper = $this->getHelper('question');

        while (!($nome = $helper->ask($input, $output, $question)) || !$this->isValidName($nome)) {

            if (!$nome) {
                $output->writeln('<error>E su dai, il nome non può essere vuoto, mi sembra il minimo</error>');
            } elseif (!$this->isValidName($nome)) {
                $output->writeln(
                    '<error>Sembrerebbe che esista già un modulo con questo nome ('.$this->camelCase($nome).')</error>'
                );
            }

        }

        $config                = [];
        $config['nome']        = $nome;
        $config['duplicabile'] = true;

        $question = new Question('<question>Indica un eventuale classe da chiamare sul backend</question>: ');

        $config['backEnd']          = [];
        $config['backEnd']['class'] = $helper->ask($input, $output, $question);
        if ($config['backEnd']['class']) {
            $question = new Question(
                '<question>Indica il nome del metodo da chiamare [getData]</question>: ', 'getData'
            );

            $config['backEnd']['method'] = $helper->ask($input, $output, $question);
        } else {
            $config['backEnd']['method'] = '';
        }

        $config['config'] = [];

        $denied = [];

        foreach ($nuovaVersione['stats'] as $tipo => $quanti) {
            if ($quanti) {
                for ($i = 0; $i < $quanti; $i++) {


                    $configType = $this->askConfigurationForType($input, $output, $tipo, $denied);

                    $config['config'][] = $configType;
                    $denied[]           = $configType['var'];
                }
            }
        }

        $output->writeln('');
        $output->writeln('<info>La configurazione del modulo è terminata.</info>');
        $output->writeln('<info>Di seguito un riepilogo di ciò che hai inserito.</info>');
        dump($config);
        $output->writeln('');

        $question = new ConfirmationQuestion(
            '<question>Continuiamo con la creazione della versione? [SI|no]</question> ', true, '/^[y|s]/i'
        );

        if ($helper->ask($input, $output, $question)) {

            $this->creaNuovaVersione($input, $output, '[NEW]', $config);

        } else {

            $output->writeln('<error>Operazione bloccata dalla tua risposta</error>');

        }

    }

    private function cercaPerNome(OutputInterface $output, $nomeModulo)
    {

        foreach ($this->modules as $module) {
            if ($module['name'] == $nomeModulo) {
                return $module;
            }
        }

        $output->writeln('<error>Qualcosa è andato terribilmente storto</error>');

    }

    protected function createIndexFile(
        InputInterface $input,
        OutputInterface $output,
        $path,
        $versione,
        $dataModulo = null
    ) {

        $index                = [];
        $index['codice']      = $versione;
        $index['release']     = "1.0";
        $index['releaseDate'] = date('Y-m-d');
        $index['descrizione'] = '';
        $index['author']      = '';

        $helper = $this->getHelper('question');

        $output->writeln('');

        $question = new Question('<question>Descrivi cosa fa il modulo:</question> ');

        while (!($index['descrizione'] = $helper->ask($input, $output, $question))) {
            $output->writeln('<error>Non essere pigro scrivi una riga di descrizione</error>');
        }

        $question = new Question('<question>Autore del modulo:</question> ');

        while (!($index['author'] = $helper->ask($input, $output, $question))) {
            $output->writeln('<error>Non essere timido, prenditi oneri e onori</error>');
        }

        $question = new ConfirmationQuestion(
            '<question>Il tuo modulo ha richieste di javascript o css esterni? [si/NO]?</question> ', false, '/^[y|s]/i'
        );

        if ($helper->ask($input, $output, $question)) {

            $index['assets'] = [];

            $output->writeln('<info>Ti verranno ora chiesti i percorsi agli eventuali file css esterni.</info>');
            $output->writeln('<info>una risposta valida potrebbe essere questa: /public/lib/slick/slick.css');
            $output->writeln('<info>Nota che il percorso comincia con uno slash e parte dalla web root</info>');
            $output->writeln(
                '<info>Quando hai finito di inserire premi invio senza inserire nulla e ti verranno chiesti i file js</info>'
            );

            $question = new Question(
                '<question>Aggiungi il percorso ad un file css a partire dalla web dir:</question> '
            );

            while ($answer = $helper->ask($input, $output, $question)) {
                if (!isset($index['assets']['css'])) {
                    $index['assets']['css'] = [];
                }
                $index['assets']['css'][] = $answer;

                $answer = $helper->ask($input, $output, $question);

            }


            $output->writeln('<info>Ti verranno ora chiesti i percorsi agli eventuali file css esterni.</info>');
            $output->writeln(
                '<info>una risposta valida potrebbe essere questa: /public/lib/matchHeight/jquery.matchHeight.js'
            );
            $output->writeln('<info>Nota che il percorso comincia con uno slash e parte dalla web root</info>');
            $output->writeln('<info>Quando hai finito di inserire premi invio senza inserire nulla</info>');

            $question = new Question(
                '<question>Aggiungi il percorso ad un file js a partire dalla web root:</question> '
            );

            while ($answer = $helper->ask($input, $output, $question)) {
                if (!isset($index['assets']['js'])) {
                    $index['assets']['js'] = [];
                }
                $index['assets']['js'][] = $answer;

                $answer = $helper->ask($input, $output, $question);

            }

            if (!$index['assets']) {
                unset($index['assets']);
            }

        }

        $output->writeln('');
        $output->writeln('<info>Questi sono i dati raccolti: </info>');
        $output->writeln('');
        foreach ($index as $k => $v) {
            if ($k == 'assets') {
                if (isset($v['css'])) {
                    $output->writeln('File css richiesti: ');
                    foreach ($v['css'] as $css) {
                        $output->writeln('- '.$css);
                    }
                }
                if (isset($v['js'])) {
                    $output->writeln('File js richiesti: ');
                    foreach ($v['js'] as $js) {
                        $output->writeln('- '.$js);
                    }
                }
            } else {
                $output->writeln($k.': <options=bold>'.$v.'</>');
            }
        }

        $output->writeln('');
        $question = new ConfirmationQuestion(
            '<question>Confermi la corretteza dei dati inseriti? [SI/no]?</question> ',
            true,
            '/^[y|s]/i'
        );

        if ($helper->ask($input, $output, $question)) {

            mkdir($path, 0755, true);

            if ($dataModulo) {
                file_put_contents(dirname($path).'/config.json', json_encode($dataModulo, JSON_PRETTY_PRINT));
            }

            file_put_contents($path.'/index.json', json_encode($index, JSON_PRETTY_PRINT));

            return true;

        }

        return false;

    }

    private function createSampleTwig(InputInterface $input, OutputInterface $output, $modulo, $path)
    {

        $rows = [];

        foreach ($modulo['config'] as $field) {

            $commento = '{# '.$field['label'];

            if ($field['mandatory']) {
                $commento .= ' - campo obbligatorio ';
            } else {
                $commento .= ' - campo facoltativo ';
            }
            $commento .= ' #}';
            $rows[]   = $commento;

            switch ($field['type']) {

                case 'simpleText':
                case 'mediumText':
                case 'simpleTextArea':
                    if ($field['mandatory']) {
                        $variabile = '{{ '.$field['var'].'.val }}';
                    } else {
                        $variabile = '{% if '.$field['var'].'.val|default("") %} {{ '.$field['var'].'.val }} {% endif %}';
                    }

                    break;

                case 'link':

                    if ($field['mandatory']) {

                        $variabile = '<a href="{{'.$field['var'].'.link}}">{{ '.$field['var'].'.label }}</a>';

                    } else {

                        $var   = [];
                        $var[] = '{% if '.$field['var'].'.link|default("") %}';
                        $var[] = '<a href="{{'.$field['var'].'.link}}">{{ '.$field['var'].'.label }}</a>';
                        $var[] = '{% endif %}';

                        $variabile = implode("\n", $var);

                    }

                    break;

                case 'file':
                    if ($field['mandatory']) {
                        $variabile = '{{ '.$field['var'].'.src }}';
                    } else {
                        $var       = [];
                        $var[]     = '{% if '.$field['var'].'.src|default("") %}';
                        $var[]     = '{{ '.$field['var'].'.src }}';
                        $var[]     = '{% endif %}';
                        $variabile = implode("\n", $var);
                    }
                    break;

                case 'image':
                    if ($field['mandatory']) {
                        $variabile = '<img src="{{'.$field['var'].'.src}}" alt="{{ '.$field['var'].'.alt|default("") }}" />';
                    } else {
                        $var       = [];
                        $var[]     = '{% if '.$field['var'].'.src|default("") %}';
                        $var[]     = '<img src="{{'.$field['var'].'.src}}" alt="{{ '.$field['var'].'.alt|default("") }}" />';
                        $var[]     = '{% endif %}';
                        $variabile = implode("\n", $var);
                    }
                    break;

                case 'backgroundImage':

                    $variabile = '<div';

                    if ($field['mandatory']) {
                        $variabile .= ' style="background-image:("url:{{ '.$field['var'].'.src }}")"';
                    } else {
                        $var   = [];
                        $var[] = '{% if '.$field['var'].'.src|default("") %}';
                        $var[] = ' style="background-image:("url:{{ '.$field['var'].'.src }}")"';
                        $var[] = '{% endif %}';

                        $variabile .= implode('', $var);
                    }
                    $variabile .= '></div>';

                    break;

                case 'boolean':

                    $var   = [];
                    $var[] = '{% if '.$field['var'].' %}';
                    $var[] = '  {# fai qualcosa qui #}';
                    $var[] = '{% endif %}';

                    $variabile = implode("\n", $var);

                    break;

            }

            $rows[] = $variabile;


        }

        file_put_contents($path.'/'.basename(dirname($path)).'.twig', implode("\n", $rows));

    }

    private function creatSampleJs(InputInterface $input, OutputInterface $output, $modulo, $path)
    {

        $file   = [];
        $file[] = '$(function() {';
        $file[] = '    if($(\'#\' + instance).length) {';
        $file[] = '        // fai qualcosa qui';
        $file[] = '    }';
        $file[] = '});';

        file_put_contents($path.'/'.basename(dirname($path)).'.js', implode("\n", $file));

    }

    private function camelCase($str, array $noStrip = [])
    {

        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9'.implode("", $noStrip).']+/i', ' ', $str);
        $str = trim($str);
        // uppercase the first character of each word
        $str = ucwords($str);
        $str = str_replace(" ", "", $str);
        $str = lcfirst($str);

        return $str;
    }

    private function isValidName($name)
    {

        if (!$name) {
            return false;
        }

        $name = $this->camelCase($name);

        if (file_exists($this->modulesDir.'/'.$name)) {
            return false;
        }

        return $name;

    }

    private function askConfigurationForType(InputInterface $input, OutputInterface $output, $tipo, $reservedNames)
    {

        @list($formType, $multilang) = explode(':', $tipo);

        $messaggio = 'Procediamo con la configurazione di un campo "'.$formType.'"';

        if ((bool)$multilang) {
            $messaggio .= ' (multilingua)';
        }

        $output->writeln('<info>'.$messaggio.'</info>');


        $data              = [];
        $data['type']      = $formType;
        $data['multilang'] = (bool)($multilang);

        $helper = $this->getHelper('question');

        $question = new Question(
            '<question>Mi dai un nome di variabile da usare? (no spazi, no primo carattere numerico):</question> ', ''
        );

        while (!($data['var'] = ($helper->ask($input, $output, $question))) || is_numeric($data['var'][0]) || strpos(
                $data['var'],
                ' '
            ) !== false || in_array($data['var'], $reservedNames)) {

            if (!$data['var']) {
                $output->writeln('<error>Qui non si scherza, scrivi qualcosa di sensato</error>');
            } elseif (strpos($data['var'], ' ') !== false) {
                $output->writeln('<error>Quale parte di "no spazi" non è chiara? </error>');
            } elseif (in_array($data['var'], $reservedNames)) {
                $output->writeln('<error>Hai già usato questo nome, memoria corta? </error>');
            } elseif (is_numeric($data['var'][0])) {
                $output->writeln(
                    '<error>Il primo carattere non può essere un numero, non te l\'avevo già detto?  </error>'
                );
            }

        }

        $question = new Question('<question>Inserisci un\'etichetta descrittiva per la variabile:</question> ', '');

        while (!($data['label'] = ($helper->ask($input, $output, $question)))) {

            if (!$data['label']) {
                $output->writeln(
                    '<error>Senti, ho altro da fare che correggere i tuoi errori, inserisci una dannata etichetta per la variabile</error>'
                );
            }

        }

        $question = new ConfirmationQuestion(
            '<question>Il campo sarà obbligatorio? [si|NO]</question> ',
            false,
            '/^[y|s]/i'
        );

        $data['mandatory'] = (bool)$helper->ask($input, $output, $question);

        $question = new Question('<question>Inserisci (se vuoi) un help in linea per la variabile:</question> ', '');

        $data['help'] = $helper->ask($input, $output, $question);

        $question = new Question(
            '<question>Inserisci (se vuoi) un valore di default per la variabile:</question> ', ''
        );

        $data['default'] = $helper->ask($input, $output, $question);

        if ($formType == 'image' || $formType == 'backgroundImage') {

            $types = 'jpg,png,gif,jpeg';

            $question = new Question(
                '<question>Indica le estensioni accettate per questo campo ['.$types.']:</question> ', $types
            );

            $data['types'] = explode(',', $helper->ask($input, $output, $question));

            $question = new Question('<question>Indica le dimensioni richieste [300x200]:</question> ', '300x200');

            while (!(preg_match(
                '/^([\d]+)x([\d]+)$/',
                $data['dimensioni'] = ($helper->ask($input, $output, $question))
            ))) {

                $output->writeln('<error>Lo so che è difficile, ma provaci, base x altezza, in numeri</error>');

            }
        }

        return $data;


    }

    function verificaVersioneUsata($versione)
    {

        $versioni = [];

        foreach ($this->modules as $module) {

            $versioni = array_merge($versioni, $module['versioni']);

        }

        return !in_array($versione, $versioni);

    }


}