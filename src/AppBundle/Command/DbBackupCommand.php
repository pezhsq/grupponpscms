<?php

/*
 * Questo file è parte di webtekCMS.
 *
 * (c) Webtek SPA <clienti@webtek.it>
 *     Author Giovanni Battista Lenoci <gianiaz@gmail.com>
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DbBackupCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:db:backup')->setDescription('Tool per il backup del db');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $Host = $container->getParameter('database_host');
        $User = $container->getParameter('database_user');
        $Password = $container->getParameter('database_password');
        $DbName = $container->getParameter('database_name');
        $em = $container->get('doctrine.orm.default_entity_manager');
        $d = new \DateTime();
        $filename = $DbName.'-'.$d->format('d-m-Y-H-i-s').'.sql';
        $process = new Process('which mysqldump');
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $mysqlDumpCommand = trim($process->getOutput());
        $cmd = '%s -h %s -u %s -p%s %s > %s';
        $cmd = sprintf($cmd, $mysqlDumpCommand, $Host, $User, $Password, $DbName, $filename);
        $process = new Process($cmd);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output->writeln('<info>Generato file di backup '.$filename.'</info>');
    }
}
