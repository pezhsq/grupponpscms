<?php
// 15/03/17, 11.59
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class TemplateScaffoldCommand extends ContainerAwareCommand {


    protected function configure() {

        $this->setName('app:template:scaffold')
            ->setDescription('Crea l\'alberatura necessaria per la creazione di un nuovo layout')
            ->setHelp('Questo comando copia la directory /app/Resources/views/public/layouts/default nel nome layout fornito via shell')
            ->addArgument('name', InputArgument::REQUIRED, 'Quale nome dovrà avere il layout?');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $slugger = $this->getContainer()->get('cocur_slugify');
        $name    = $slugger->slugify($input->getArgument('name'));

        $helper = $this->getHelper('question');

        $to = $this->getContainer()->get('kernel')->getRootDir().'/Resources/views/public/layouts/'.$name;

        $question = new ConfirmationQuestion('Continuare con la creazione della directory "'.$to.'" ?', '/^(y|s)/i');

        if(!$helper->ask($input, $output, $question)) {
            return;
        }
        $output->writeln('<fg=green>Procedo con la copia...</>');

        $this->doCopy($to, $output);

        $this->git($to, $output);


    }

    private function doCopy($to, $output) {

        $from = $this->getContainer()->get('kernel')->getRootDir().'/Resources/views/public/layouts/default';

        $this->getContainer()->get('app.fs_helper')->copyDir($from, $to);

        $output->writeln('<fg=green>Creata la directory '.$to.'</>');

        @unlink($to.'/README');

    }

    private function git($to, $output) {

        $process = new Process('git add -f '.$to);
        $process->run();

        if(!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $output->writeln('<fg=green>Aggiunta la directory '.$to.' a git</>');

    }

}