<?php
// 29/08/17, 14.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class BaseCommand extends ContainerAwareCommand
{

    /**
     * @var $container ContainerInterface
     */
    protected $container;

    /**
     * @var $em EntityManager
     */
    protected $em;


    protected function configure()
    {

        $this->setName('app:base:command')->setDescription('')->setHelp('');
    }


    protected function setUp(OutputInterface $output)
    {

        $this->container = $this->getContainer();
        $this->em = $this->container->get('doctrine.orm.default_entity_manager');
        $style = new OutputFormatterStyle('white', 'green', ['blink']);
        $output->getFormatter()->setStyle('webtek', $style);
        $user = $this->em->getRepository('AppBundle:User')->findOneBy(['username' => 'gianiaz']);
        $token = new UsernamePasswordToken(
            $user, null, 'main', $user->getRoles()
        );
        $this->container->get('security.token_storage')->setToken($token);


    }

    protected function outTitle(OutputInterface $output, $title = '', $subtitle = '')
    {

        $lengths = [];
        array_push($lengths, mb_strlen($title));
        array_push($lengths, mb_strlen($subtitle));
        $larghezzaRiga = max($lengths) + 4;
        $output->writeln('');
        $output->writeln('<webtek>'.str_repeat('-', $larghezzaRiga).'</webtek>');
        $output->writeln('<webtek>'.str_pad($title, $larghezzaRiga, ' ', STR_PAD_BOTH).'</webtek>');
        $output->writeln('<webtek>'.str_repeat(' ', $larghezzaRiga).'</webtek>');
        $output->writeln('<webtek>'.str_pad($subtitle, $larghezzaRiga, ' ', STR_PAD_BOTH).'</webtek>');
        $output->writeln('<webtek>'.str_repeat('-', $larghezzaRiga).'</webtek>');
        $output->writeln('');

    }

}