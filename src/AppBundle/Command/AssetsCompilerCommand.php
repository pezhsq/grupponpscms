<?php
// 24/07/17, 5.59
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Command;

use AppBundle\Service\AssetsManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AssetsCompilerCommand extends ContainerAwareCommand
{

    private $container;

    protected function configure()
    {

        $this->setName('app:assets:compile')->setDescription('Tool per la compilazione di css e js')->setHelp(
                'Comando utilizzabile per la compilazione automatica dei css e js'
            );
    }

    private function setUp(OutputInterface $output)
    {

        $style = new OutputFormatterStyle('white', 'green', ['blink']);
        $output->getFormatter()->setStyle('webtek', $style);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getContainer();
        $this->setUp($output);
        $AssetsManager = $this->container->get('app.assets_manager');
        /**
         * @var $AssetsManager AssetsManager
         */
        $output->writeln($AssetsManager->compile());

    }
}