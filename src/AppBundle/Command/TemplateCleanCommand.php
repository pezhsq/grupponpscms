<?php
// 20/03/17, 13.52
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Command;


use AppBundle\Entity\News;
use AppBundle\Entity\NewsAttachment;
use AppBundle\Entity\Page;
use AppBundle\Entity\Template;
use Doctrine\DBAL\Schema\Table;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class TemplateCleanCommand extends ContainerAwareCommand {

    private $em;
    private $container;
    private $webDir;

    protected function configure() {

        $this->setName('app:template:clean')
            ->setDescription('Cancella i moduli non utilizzati nel template')
            ->setHelp('Viene ciclata la tabella template_config alla ricerca dei moduli presenti per poi cancellare quelli che non rientrano nell\'elenco')
            ->addOption(
                'debug',
                null,
                InputOption::VALUE_NONE,
                'Il comando va in debug mode di default');

    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        /**
         * @var $container ContainerInterface
         */
        $this->container = $this->getContainer();

        /**
         * @var $em EntityManager
         */
        $this->em = $this->container->get('doctrine')->getManager();

        $this->webDir = $this->container->get('app.web_dir')->get();

        $this->cancellaModuli($input, $output, $input->getOption('debug'));

        $this->cancellaNews($input, $output, $input->getOption('debug'));

        $this->cancellaPagine($input, $output, $input->getOption('debug'));

        $this->deleteAssets($output, $input->getOption('debug'));

        $this->askForBundles($input, $output, $input->getOption('debug'));

        $this->ripulisciDB($input, $output, $input->getOption('debug'));

        $command = $this->getApplication()->find('cache:clear');

        $rootDir = $this->container->get('kernel')->getRootDir();

        $cacheDir = $rootDir.'/../var/cache/';

        if(file_exists($cacheDir.'dev')) {
            $input      = new ArrayInput(['--env' => 'dev']);
            $returnCode = $command->run($input, $output);
        }

        if(file_exists($cacheDir.'prod')) {
            $input      = new ArrayInput(['--env' => 'prod']);
            $returnCode = $command->run($input, $output);
        }

    }

    protected function askForBundles(InputInterface $input, OutputInterface $output, $debug = false) {

        $helper = $this->getHelper('question');

        // Appartamenti
        $question = new ConfirmationQuestion('<question>Utilizzi il bundle degli appartamenti? [Y/n]:</question> ', true,
            '/^(y|j)/i');

        if(!$helper->ask($input, $output, $question)) {

            $bundle = 'AppartamentiBundle';

            $this->deleteBundle($input, $output, $bundle, $debug);

        }

        // Ecommerce
        $question = new ConfirmationQuestion('<question>Utilizzi il bundle dell\'ecommerce? [Y/n]:</question> ', true,
            '/^(y|j)/i');

        if(!$helper->ask($input, $output, $question)) {

            $bundle = 'WebtekEcommerceBundle';

            $this->deleteBundle($input, $output, $bundle, $debug);

        }

    }

    protected
    function cancellaModuli(InputInterface $input, OutputInterface $output, $debug = false) {

        $templateUsati = $this->em->getRepository('AppBundle:Template')->findAll();

        $dirModuli = $this->container->getParameter('kernel.root_dir').'/Resources/views/modules';

        $datiUsati = [];

        $assetsModuli = [];

        foreach($templateUsati as $Template) {
            /**
             * @var $Template Template
             */
            $datiUsati[] = $dirModuli.'/'.$Template->getModule().'/'.$Template->getCodice();

            $assetsModuli[] = $this->webDir.'/mods/'.$Template->getModule().'/'.$Template->getInstance();
        }

        /**
         * Vengono collezionate le directory dove vengono salvate le informazioni dei moduli presenti attualmente sul
         * filesystem. Questa informazione verrà incrociata con i moduli attualmente configurati in db ($assetsModuli).
         * Tutto ciò che non è presente in db verrà cancellato
         */
        $finder = new Finder();
        $finder->depth('> 0');
        $finder->depth('< 2');
        $assetsPresenti = $finder->directories()->in($this->webDir.'/mods/');

        $assets = [];
        foreach($assetsPresenti as $AssetsDir) {
            /**
             * @var $AssetsDir SplFileInfo
             */
            $assets[] = $AssetsDir->getRealPath();
        }

        $cancella = array_diff($assets, $assetsModuli);

        if($debug) {
            $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
            dump($cancella);
        } else {
            $filesystem = new Filesystem();
            $filesystem->remove($cancella);
        }

        $dirs = [];

        foreach($cancella as $dir) {
            $dirs[] = dirname($dir);
        }

        $dirs = array_unique($dirs);

        foreach($dirs as $dir) {
            $finder = new Finder();
            $conta  = count($finder->directories()->in($dir));

            if($debug) {
                $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
                dump($cancella);
            } else {
                if(!$conta) {
                    $filesystem->remove($dir);
                }
            }
        }


        /**
         * Vengono collezionate le directory di moduli attivabili presenti attualmente sul
         * filesystem. Questa informazione verrà incrociata con i moduli attualmente configurati in db ($datiUsati).
         * Tutto ciò che non è presente in db verrà cancellato
         */
        $finder = new Finder();
        $finder->depth('> 0');
        $moduliPresenti = $finder->directories()->in($dirModuli);

        $moduli = [];

        foreach($moduliPresenti as $Dir) {
            /**
             * @var $Dir SplFileInfo
             */
            $moduli[] = $Dir->getRealPath();
        }

        $cancella = array_diff($moduli, $datiUsati);

        if(!$debug) {

            $helper = $this->getHelper('question');

            $question = new ConfirmationQuestion('<question>Continuare con la cancellazione di '.count($cancella).' directory?</question>', '/^(y|n)/i');

            if(!$helper->ask($input, $output, $question)) {

                $output->writeln('<fg=red>Comando annullato</>');

                return;
            }
        }

        if($debug) {
            $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
            dump($cancella);
        } else {
            $filesystem = new Filesystem();
            $filesystem->remove($cancella);
        }

        $dirs = [];
        foreach($cancella as $dir) {
            $dirs[] = dirname($dir);
        }
        $dirs = array_unique($dirs);

        foreach($dirs as $dir) {
            $finder = new Finder();
            $conta  = count($finder->directories()->in($dir));
            if($debug) {
                $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
                dump($cancella);
            } else {
                if(!$conta) {
                    $filesystem->remove($dir);
                }
            }
        }

        $output->writeln('<fg=green>Moduli inutili cancellati</>');

    }

    protected
    function cancellaNews(InputInterface $input, OutputInterface $output, $debug = false) {

        $finder = new Finder();
        //$finder->depth('> 0');
        $directoryUploadPresenti = $finder->directories()->in($this->webDir.'/files/news/', $this->webDir.'/files/news_attachments/');

        $dirPresenti = [];

        foreach($directoryUploadPresenti as $dir) {
            /**
             * @var $dir SplFileInfo
             */
            $dirPresenti[] = $dir->getRealPath();
        }

        $NewsList = $this->em->getRepository('AppBundle:News')->findAll();

        $dirsPresenti  = [];
        $filesPresenti = [];

        foreach($NewsList as $News) {
            /**
             * @var $News News
             */

            if($News->getListImgFileName()) {
                $filesPresenti[] = $this->webDir.'/'.$News->getUploadDir().$News->getListImgFileName();
            }
            if($News->getHeaderImgFileName()) {
                $filesPresenti[] = $this->webDir.'/'.$News->getUploadDir().$News->getHeaderImgFileName();
            }

            $attachments = $News->getAttachments();

            $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');

            foreach($attachments as $attachment) {
                /**
                 * @var $attachment NewsAttachment
                 */
                $filesPresenti[] = $this->webDir.$helper->asset($attachment, 'file');
            }

            $dirsPresenti[] = substr($this->webDir.'/'.$News->getUploadDir(), 0, -1);
            $dirsPresenti[] = $this->webDir.'/files/news_attachments/'.$News->getId();

        }

        $daCancellare = array_diff($dirPresenti, $dirsPresenti);

        if($debug) {
            $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
            dump($daCancellare);
        } else {
            $filesystem = new Filesystem();
            $filesystem->remove($daCancellare);
        }

        $finder = new Finder();

        $filesNews = $finder->files()->in($this->webDir.'/files/news_attachments/');
        $filesNews = $finder->files()->in($this->webDir.'/files/news/');
        $files     = [];

        foreach($filesNews as $File) {
            $files[] = $File->getRealPath();
        }

        $filesDaCancellare = array_diff($files, $filesPresenti);

        if($debug) {
            $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
            dump($filesDaCancellare);
        } else {
            $filesystem = new Filesystem();
            $filesystem->remove($filesDaCancellare);
        }

    }

    protected function cancellaPagine(InputInterface $input, OutputInterface $output, $debug = false) {

        $finder = new Finder();
        //$finder->depth('> 0');
        $directoryUploadPresenti = $finder->directories()->in($this->webDir.'/files/pages/', $this->webDir.'/files/pages_attachment/');

        $dirPresenti = [];

        foreach($directoryUploadPresenti as $dir) {
            /**
             * @var $dir SplFileInfo
             */
            $dirPresenti[] = $dir->getRealPath();
        }

        $PageList = $this->em->getRepository('AppBundle:Page')->findAll();

        $dirsPresenti  = [];
        $filesPresenti = [];

        foreach($PageList as $Page) {
            /**
             * @var $Page Page
             */

            if($Page->getListImgFileName()) {
                $filesPresenti[] = $this->webDir.'/'.$Page->getUploadDir().$Page->getListImgFileName();
            }
            if($Page->getHeaderImgFileName()) {
                $filesPresenti[] = $this->webDir.'/'.$Page->getUploadDir().$Page->getHeaderImgFileName();
            }

            $attachments = $Page->getAttachments();

            $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');

            foreach($attachments as $attachment) {
                /**
                 * @var $attachment NewsAttachment
                 */
                $filesPresenti[] = $this->webDir.$helper->asset($attachment, 'file');
            }

            $dirsPresenti[] = substr($this->webDir.'/'.$Page->getUploadDir(), 0, -1);
            $dirsPresenti[] = $this->webDir.'/files/news_attachments/'.$Page->getId();

        }

        $daCancellare = array_diff($dirPresenti, $dirsPresenti);

        if($debug) {
            $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
            dump($daCancellare);
        } else {
            $filesystem = new Filesystem();
            $filesystem->remove($daCancellare);
        }

        $finder = new Finder();

        $filesNews = $finder->files()->in($this->webDir.'/files/pages_attachment/');
        $filesNews = $finder->files()->in($this->webDir.'/files/pages/');
        $files     = [];

        foreach($filesNews as $File) {
            $files[] = $File->getRealPath();
        }

        $filesDaCancellare = array_diff($files, $filesPresenti);

        if($debug) {
            $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
            dump($daCancellare);
        } else {
            $filesystem = new Filesystem();
            $filesystem->remove($filesDaCancellare);
        }

    }

    protected function deleteAssets(OutputInterface $output, $debug = false) {

        $finder = new Finder();

        $layout = $this->container->getParameter('generali')['layout'];

        $directoryAssets = $finder->directories()->notPath('maintenance')->notPath('#.*'.$layout.'.*#')->in($this->webDir.'/public/css/');

        $dirs = [];

        foreach($directoryAssets as $asset) {
            /**
             * @var $asset SplFileInfo
             */

            $dirs[] = $asset->getRealPath();
        }

        if($debug) {
            $output->writeln('<info>In debug, altrimenti cancellerei queste directory:</>');
            dump($dirs);
        } else {
            $filesystem = new Filesystem();
            $filesystem->remove($dirs);
        }

    }

    private function deleteBundle(InputInterface $input, OutputInterface $output, $bundle, $debug = 0) {

        if($debug) {

            $output->writeln('<info>In debug, altrimenti cancellerei il bundle '.$bundle.':</>');

        }


        $rootDir = $this->container->get('kernel')->getRootDir();

        $dirBundle = $this->container->get('kernel')->getRootDir().'/../src/'.$bundle;

        // pulizia del file appkernel dal bundle
        $fileAppKernel = $rootDir.'/AppKernel.php';
        $appKernel     = file($fileAppKernel);

        foreach($appKernel as $numeroLinea => $linea) {
            if(strpos($linea, $bundle) !== false) {
                $trovato = true;
                unset($appKernel[$numeroLinea]);
            }
        }

        if($debug) {
            echo implode("", $appKernel);
            //dump(implode("", $appKernel));
        } else {
            file_put_contents($fileAppKernel, implode("", $appKernel));
        }

        // pulizia del file routing.yml dal bundle
        $fileRouting = $rootDir.'/config/routing.yml';

        $routing = file($fileRouting);

        $rimuovi = false;

        $search = str_replace('bundle', '', strtolower($bundle));

        foreach($routing as $numeroLinea => $linea) {
            if(strpos($linea, $search) !== false) {
                $rimuovi = true;
                unset($routing[$numeroLinea]);
            } elseif($rimuovi && strpos($linea, '    ') === 0) {
                unset($routing[$numeroLinea]);
            } else {
                $rimuovi = false;
            }
        }

        if($debug) {
            echo implode("", $routing);
        } else {
            file_put_contents($fileRouting, implode("", $routing));
        }

        if(!$debug) {

            $filesystem = new Filesystem();
            $filesystem->remove($dirBundle);

            $output->writeln('<info>Cancellato il bundle '.$bundle.':</>');
        }
    }

    protected function ripulisciDb(InputInterface $input, OutputInterface $output, $debug = 0) {

        $tables = $this->em->getConnection()->getSchemaManager()->listTables();

        $tabelle = [];

        foreach($tables as $table) {
            /**
             * @var $table Table
             */

            $tabelle[] = $table->getName();
        }

        $meta = $this->em->getMetadataFactory()->getAllMetadata();

        $doctrineTables = [];

        foreach($meta as $m) {
            $doctrineTables[] = $m->table['name'];
        }

        $tabelle = array_diff($tabelle, $doctrineTables);

        if(!$debug) {

            $connection = $this->em->getConnection();
            $statement  = $connection->prepare("SET foreign_key_checks = 0");;
            $statement->execute();
            $statement = $connection->prepare("DROP TABLE IF EXISTS ".join(', ', $tabelle));
            $statement->execute();
            $statement = $connection->prepare("SET foreign_key_checks = 1");
            $statement->execute();

        } else {
            $output->writeln('<info> Devono essere cancellate le seguenti tabelle: '.join(', ', $tabelle).'</info>');
        }

    }
}