<?php
// 09/03/17, 10.22
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Command;

use AppBundle\Entity\EmailTemplate;
use AppBundle\Entity\EmailTemplateTranslation;
use AppBundle\Entity\GroupTypes;
use AppBundle\Entity\MappedRoles;
use AppBundle\Entity\Page;
use AppBundle\Entity\PageTranslation;
use AppBundle\Entity\Settings;
use AppBundle\Entity\SettingsGroup;
use AppBundle\Entity\Template;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class CreateSettingsFixturesCommand extends ContainerAwareCommand
{

    private $destinationDir;

    /**
     * @var $em EntityManager
     */
    private $em;

    private $settingsGroup;

    protected function configure()
    {

        $this->setName('app:create-settings-fixtures')->setDescription(
            'Crea il file delle fixtures dei settaggi'
        )->setHelp(
            'Questo comando crea un nuovo file yml nella directory /src/AppBundle/DataFixtures/ contenente i settaggi prelevati da database'
        );

    }

    private function generateHome(OutputInterface $output)
    {

        /**
         * @var $Home Page
         */
        $Home = $this->em->getRepository('AppBundle:Page')->findOneBy(['template' => 'home']);
        $data = [];
        $data[Page::class] = [];
        $fileName = strtolower(basename($this->get_class($Home))).'.yml';
        $data[Page::class]['home0'] = [];
        $data[Page::class]['home0']['id'] = 1;
        $data[Page::class]['home0']['type'] = $Home->getType();
        $data[Page::class]['home0']['template'] = $Home->getTemplate();
        $data[Page::class]['home0']['isEnabled'] = $Home->getIsEnabled();
        $data[Page::class]['home0']['robots'] = $Home->getRobots();
        $data[Page::class]['home0']['level'] = $Home->getLevel();
        $data[Page::class]['home0']['headerImgFileName'] = '';
        $data[Page::class]['home0']['listImgFileName'] = '';
        $data[Page::class]['home0']['listImgAlt'] = $Home->getListImgAlt();
        $data[Page::class]['home0']['headerImgAlt'] = $Home->getHeaderImg();
        $data[Page::class]['home0']['parent'] = $Home->getParent();
        $data[Page::class]['home0']['sort'] = $Home->getSort();
        $data[Page::class]['home0']['createdAt'] = $Home->getCreatedAt();
        $data[Page::class]['home0']['updatedAt'] = $Home->getUpdatedAt();
        $data[Page::class]['home0']['materializedPath'] = $Home->getMaterializedPath();
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[Page::class]
            ).' pagine</>'
        );
        $fileName = strtolower(basename($this->get_class($Home).'Translation')).'.yml';
        $data = [];
        $data[PageTranslation::class] = [];
        $data[PageTranslation::class]['home0_translation'] = [];
        $data[PageTranslation::class]['home0_translation']['id'] = 1;
        $data[PageTranslation::class]['home0_translation']['translatable'] = '@home0';
        $data[PageTranslation::class]['home0_translation']['isEnabled'] = 1;
        $data[PageTranslation::class]['home0_translation']['titolo'] = $Home->translate()->getTitolo();
        $data[PageTranslation::class]['home0_translation']['sottotitolo'] = $Home->translate()->getSottotitolo();
        $data[PageTranslation::class]['home0_translation']['metaTitle'] = $Home->translate()->getMetaTitle();
        $data[PageTranslation::class]['home0_translation']['metaDescription'] = $Home->translate()->getMetaDescription(
        );
        $data[PageTranslation::class]['home0_translation']['slug'] = $Home->translate()->getSlug();
        $data[PageTranslation::class]['home0_translation']['locale'] = $Home->translate()->getLocale();
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[PageTranslation::class]
            ).' pagine</>'
        );

    }

    private function generateSettingsGroup(OutputInterface $output)
    {

        $SettingsGroups = $this->em->getRepository('AppBundle:SettingsGroup')->findAll();
        $data = [];
        $data[SettingsGroup::class] = [];
        $fileName = false;
        foreach ($SettingsGroups as $k => $SettingsGroup) {

            if (!$fileName) {
                $fileName = strtolower(basename($this->get_class($SettingsGroup))).'.yml';
            }
            /**
             * @var $SettingsGroup SettingsGroup
             */
            $data[SettingsGroup::class]['settings_group'.$k] = [];
            $data[SettingsGroup::class]['settings_group'.$k]['id'] = $SettingsGroup->getId();
            $data[SettingsGroup::class]['settings_group'.$k]['name'] = $SettingsGroup->getName();
            $this->settingsGroup[$SettingsGroup->getId()] = 'settings_group'.$k;


        }
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/01_'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[SettingsGroup::class]
            ).' gruppi</>'
        );
    }

    private function generateSettings(OutputInterface $output)
    {

        $Settings = $this->em->getRepository('AppBundle:Settings')->findAll();
        $data = [];
        $data[Settings::class] = [];
        $fileName = false;
        foreach ($Settings as $k => $Setting) {

            if (!$fileName) {
                $fileName = strtolower(basename($this->get_class($Setting))).'.yml';
            }
            $group = '@'.$this->settingsGroup[$Setting->getGroup()->getId()];
            /**
             * @var $Setting Settings
             */
            $data[Settings::class]['settings'.$k] = [];
            $data[Settings::class]['settings'.$k]['id'] = $Setting->getId();
            $data[Settings::class]['settings'.$k]['chiave'] = $Setting->getChiave();
            $data[Settings::class]['settings'.$k]['group'] = $group;
            $data[Settings::class]['settings'.$k]['aiuto'] = $Setting->getAiuto();
            $data[Settings::class]['settings'.$k]['etichetta'] = $Setting->getEtichetta();
            $data[Settings::class]['settings'.$k]['requiredRole'] = $Setting->getRequiredRole();
            $data[Settings::class]['settings'.$k]['tipo'] = $Setting->getTipo();
            $data[Settings::class]['settings'.$k]['valore'] = $Setting->getValore();

        }
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/02_'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[Settings::class]
            ).' settaggi</>'
        );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->destinationDir = realpath(
            $this->getContainer()->get('kernel')->getRootDir().'/../src/AppBundle/DataFixtures/ORM'
        );
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->generateHome($output);
        $this->generateSettingsGroup($output);
        $this->generateSettings($output);
        $this->generateRoles($output);
        $this->generateTemplates($output);
        $this->generateMails($output);

    }

    private function generateTemplates($output)
    {

        $Templates = $this->em->getRepository('AppBundle:Template')->findBy(['layout' => ['ecommerce', 'landing1']]);
        $data = [];
        $data[Template::class] = [];
        $fileName = false;
        foreach ($Templates as $k => $Template) {

            if (!$fileName) {
                $fileName = strtolower(basename($this->get_class($Template))).'.yml_raw';
            }
            /**
             * @var $Template Template
             */
            $data[Template::class]['template'.$k] = [];
            $data[Template::class]['template'.$k]['id'] = $Template->getId();
            $data[Template::class]['template'.$k]['template'] = $Template->getTemplate();
            $data[Template::class]['template'.$k]['view'] = $Template->getView();
            $data[Template::class]['template'.$k]['module'] = $Template->getModule();
            $data[Template::class]['template'.$k]['category'] = $Template->getCategory();
            $data[Template::class]['template'.$k]['position'] = $Template->getPosition();
            $data[Template::class]['template'.$k]['instance'] = $Template->getInstance();
            $data[Template::class]['template'.$k]['type'] = $Template->getType();
            $data[Template::class]['template'.$k]['container'] = $Template->getContainer();
            $data[Template::class]['template'.$k]['config'] = $Template->getConfig();
            $data[Template::class]['template'.$k]['codice'] = $Template->getCodice();
            $data[Template::class]['template'.$k]['isEnabled'] = $Template->getIsEnabled();
            $data[Template::class]['template'.$k]['layout'] = $Template->getLayout();
            $data[Template::class]['template'.$k]['requiredRole'] = $Template->getRequiredRole();

        }
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/05_'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[Template::class]
            ).' moduli configurati</>'
        );
    }

    private function generateRoles(OutputInterface $output)
    {

        $GroupTypes = $this->em->getRepository('AppBundle:GroupTypes')->findAll();
        $data = [];
        $data[GroupTypes::class] = [];
        $fileName = false;
        foreach ($GroupTypes as $k => $GroupType) {

            if (!$fileName) {
                $fileName = strtolower(basename($this->get_class($GroupType))).'.yml';
            }
            /**
             * @var $GroupType GroupTypes
             */
            $data[GroupTypes::class]['grouptype'.$k] = [];
            $data[GroupTypes::class]['grouptype'.$k]['id'] = $GroupType->getId();
            $data[GroupTypes::class]['grouptype'.$k]['nome'] = $GroupType->getNome();
            $data[GroupTypes::class]['grouptype'.$k]['sort'] = $k;
            $groupTypes[$GroupType->getId()] = 'grouptype'.$k;


        }
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/03_'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[GroupTypes::class]
            ).' Group Types ruoli</>'
        );
        $MappedRoles = $this->em->getRepository('AppBundle:MappedRoles')->findAll();
        $data = [];
        $data[MappedRoles::class] = [];
        $fileName = false;
        foreach ($MappedRoles as $k => $MappedRole) {

            if (!$fileName) {
                $fileName = strtolower(basename($this->get_class($MappedRole))).'.yml';
            }
            /**
             * @var $MappedRole MappedRoles
             */
            $data[MappedRoles::class]['mappedrole'.$k] = [];
            $data[MappedRoles::class]['mappedrole'.$k]['id'] = $MappedRole->getId();
            $data[MappedRoles::class]['mappedrole'.$k]['nome'] = $MappedRole->getNome();
            $data[MappedRoles::class]['mappedrole'.$k]['role'] = $MappedRole->getRole();
            $data[MappedRoles::class]['mappedrole'.$k]['groupType'] = '@'.$groupTypes[$MappedRole->getGroupType(
                )->getId()];


        }
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/04_'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[MappedRoles::class]
            ).' Ruoli</>'
        );

    }

    private function generateMails(OutputInterface $output)
    {

        $EmailTemplate = $this->em->getRepository('AppBundle:EmailTemplate')->findAll();
        $data = [];
        $data[EmailTemplate::class] = [];
        $fileName = false;
        foreach ($EmailTemplate as $k => $EmailTemplate) {

            if (!$fileName) {
                $fileName = strtolower(basename($this->get_class($EmailTemplate))).'.yml';
            }
            /**
             * @var $EmailTemplate EmailTemplate
             */
            $data[EmailTemplate::class]['emailtemplate'.$k] = [];
            $data[EmailTemplate::class]['emailtemplate'.$k]['id'] = $EmailTemplate->getId();
            $data[EmailTemplate::class]['emailtemplate'.$k]['codice'] = $EmailTemplate->getCodice();
            $data[EmailTemplate::class]['emailtemplate'.$k]['variabili'] = $EmailTemplate->getVariabili();
        }
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/06_'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[EmailTemplate::class]
            ).' Template Email</>'
        );
        $EmailTemplate = $this->em->getRepository('AppBundle:EmailTemplate')->findAll();
        $fileName = false;
        $data = [];
        $data[EmailTemplateTranslation::class] = [];
        foreach ($EmailTemplate as $k => $EmailTemplate) {

            if (!$fileName) {
                $fileName = strtolower(basename($this->get_class($EmailTemplate))).'_translation.yml';
            }
            /**
             * @var $EmailTemplate EmailTemplate
             */
            $data[EmailTemplateTranslation::class]['emailtemplatetranslation'.$k] = [];
            $data[EmailTemplateTranslation::class]['emailtemplatetranslation'.$k]['id'] = $EmailTemplate->translate(
                'it'
            )->getId();
            $data[EmailTemplateTranslation::class]['emailtemplatetranslation'.$k]['translatable'] = '@emailtemplate'.$k;
            $data[EmailTemplateTranslation::class]['emailtemplatetranslation'.$k]['subject'] = $EmailTemplate->translate(
                'it'
            )->getSubject();
            $data[EmailTemplateTranslation::class]['emailtemplatetranslation'.$k]['text'] = $EmailTemplate->translate(
                'it'
            )->getText();
            $data[EmailTemplateTranslation::class]['emailtemplatetranslation'.$k]['locale'] = 'it';
        }
        $yaml = Yaml::dump($data, 4, 4);
        file_put_contents($this->destinationDir.'/07_'.$fileName, $yaml);
        $output->writeln(
            '<fg=green>Creato il file "'.$this->destinationDir.'/'.$fileName.'" con '.count(
                $data[EmailTemplateTranslation::class]
            ).' Template Email in italiano</>'
        );

    }

    private function get_class($classname)
    {

        $classname = get_class($classname);
        if ($pos = strrpos($classname, '\\')) {
            return substr($classname, $pos + 1);
        }

        return $pos;

    }

}