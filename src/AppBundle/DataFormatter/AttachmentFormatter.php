<?php
// 17/03/17, 17.14
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;

use AppBundle\Entity\Attachment;

class AttachmentFormatter extends DataFormatter {

    private $attachments = [];
    private $uploadDir;

    public function getData() {

        $data              = [];
        $data['immagini']  = [];
        $data['uploadDir'] = $this->uploadDir;

        foreach($this->attachments as $attachment) {
            /**
             * @var $attachment Attachment;
             */

            if(in_array($attachment->getType(), ['jpeg', 'png', 'jpg', 'gif'])) {
                $data['immagini'][] = $attachment;
            }

        }

        return $data;
    }

    public function getFiles() {

        $data              = [];
        $data['files']     = [];


        foreach($this->attachments as $attachment) {
            /**
             * @var $attachment Attachment;
             */

            if(!in_array($attachment->getType(), ['jpeg', 'png', 'jpg', 'gif'])) {
                $data['files'][] = $attachment;
            }

        }

        return $data;
    }

    public function extractData() {

        if(isset($this->AdditionalData['Entity'])) {

            $Entity = $this->AdditionalData['Entity'];

            $this->uploadDir = $Entity->getUploadDir();

            $this->attachments = $Entity->getAttachments();


        }
    }


}