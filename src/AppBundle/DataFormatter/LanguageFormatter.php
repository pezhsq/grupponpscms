<?php
// 08/02/17, 8.49
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


class LanguageFormatter extends DataFormatter {

    public function extractData() {

        // dump($this->AdditionalData);

    }

    public function getData() {

        $META = false;
        
        if(isset($this->AdditionalData['META'])) {
            $META = $this->AdditionalData['META'];
        }

        $return = ['langs' => $this->AdditionalData['langs'], 'META' => $META];

        return $return;

    }

}