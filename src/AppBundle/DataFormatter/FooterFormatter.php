<?php
// 30/01/17, 10.12
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\Tag;
use AppBundle\Entity\Page;
use Decoda\Decoda;

class FooterFormatter extends DataFormatter {

    public function getData() {

        $Footer = [];

        $Footer['img']        = [];
        $Footer['img']['src'] = '';
        $Footer['img']['alt'] = '';

        if($this->AdditionalData && isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity']) {

            $Entity = false;

            if(isset($this->AdditionalData['Entity'])) {
                $Entity = $this->AdditionalData['Entity'];
            }

            if($Entity) {

                switch(true) {
                    case $Entity instanceof Page:
                        $Footer = array_replace_recursive($Footer, $this->getHeaderPage($Entity));

                        break;

                    case $Entity instanceof Appartamento:
                        $Footer = array_replace_recursive($Footer, $this->getHeaderAppartamento($Entity));
                        break;
                }

            }
        }


        return ['footer' => $Footer];

    }

    public function extractData() {

    }

    public function getHeaderAppartamento($Entity) {

        /**
         * @var $Entity Appartamento
         */

        $Footer = [];

        if($Entity->getListImgFileName()) {
            $Footer['img']['src'] = '/'.$Entity->getUploadDir().$Entity->getListImgFileName();
            $Footer['img']['alt'] = $Entity->getListImg();
        }


        return $Footer;

    }

    public function getHeaderPage($Entity) {

        $Footer = [];

        if($Entity->getListImgFileName()) {
            $Footer['img']['src'] = '/'.$Entity->getUploadDir().$Entity->getListImgFileName();
            $Footer['img']['alt'] = $Entity->getListImg();
        }


        return $Footer;


    }


}