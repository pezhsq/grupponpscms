<?php
// 30/01/17, 10.12
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\Tag;
use AppBundle\Entity\News;
use AppBundle\Entity\Page;
use Decoda\Decoda;

class HeaderFormatter extends DataFormatter {

    public function getData() {

        $HEADER = [];

        $HEADER['img']        = [];
        $HEADER['img']['src'] = '/images/header.jpg';
        $HEADER['img']['alt'] = '';
        $HEADER['color']      = 'rgba(0,0,0,0.7)';

        if($this->AdditionalData && isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity']) {

            $Entity = false;

            if(isset($this->AdditionalData['Entity'])) {
                $Entity = $this->AdditionalData['Entity'];
            }

            if($Entity) {

                switch(true) {
                    case $Entity instanceof Page:
                        $HEADER = array_replace_recursive($HEADER, $this->getHeaderPage($Entity));

                        break;

                    case $Entity instanceof News:
                        $HEADER = array_replace_recursive($HEADER, $this->getHeaderNews($Entity));

                        break;

                    case $Entity instanceof Appartamento:
                        $HeaderAppartamento = $this->getHeaderAppartamento($Entity);
                        $HEADER             = array_replace_recursive($HEADER, $HeaderAppartamento);
                        break;
                }

            }
        }

        return ['header' => $HEADER];

    }

    public function extractData() {

    }

    public function getHeaderAppartamento($Entity) {

        /**
         * @var $Entity Appartamento
         */

        $HEADER = [];

        $Servizi = $Entity->getServizi();

        $colore = 'rgba(0,0,0, 0.7)';

        foreach($Servizi as $Servizio) {
            /**
             * @var $Servizio Tag;
             */
            $Categoria = $Servizio->getCategoria();
            if($Categoria->getId() == '5') {
                $colore = $Servizio->getValore();
                break;
            }
        }

        if($Entity->getHeaderImgFileName()) {
            $HEADER['img']['src'] = '/'.$Entity->getUploadDir().$Entity->getHeaderImgFileName();
            $HEADER['img']['alt'] = $Entity->getHeaderImgAlt();
        } else {
            $HEADER['img']['alt'] = $Entity->translate($this->locale)->getTitolo();
        }

        $HEADER['titolo']      = str_replace(' ', '<br />', $Entity->translate($this->locale)->getTitolo());
        $HEADER['sottotitolo'] = $Entity->translate($this->locale)->getSottotitolo();
        $HEADER['color']       = $colore;

        return $HEADER;

    }

    public function getHeaderPage($Entity) {

        $HEADER = [];
        $colore = 'rgba(0,0,0, 0.7)';

        if($Entity->getHeaderImgFileName()) {
            $HEADER['img']['src'] = '/'.$Entity->getUploadDir().$Entity->getHeaderImgFileName();
            $HEADER['img']['alt'] = $Entity->getHeaderImgAlt();
        } else {
            $HEADER['img']['alt'] = $Entity->translate($this->locale)->getTitolo();
        }
        $HEADER['titolo']      = str_replace(' ', '<br />', $Entity->translate($this->locale)->getTitolo());
        $HEADER['sottotitolo'] = $Entity->translate($this->locale)->getSottotitolo();
        $HEADER['color']       = $colore;


        return $HEADER;


    }

    public function getHeaderNews($Entity) {

        $HEADER = [];
        $colore = 'rgba(0,0,0, 0.7)';

        if($Entity->getHeaderImgFileName()) {
            $HEADER['img']['src'] = '/'.$Entity->getUploadDir().$Entity->getHeaderImgFileName();
            $HEADER['img']['alt'] = $Entity->getHeaderImgAlt();
        } else {
            $HEADER['img']['alt'] = $Entity->translate($this->locale)->getTitolo();
        }
        $HEADER['titolo']      = str_replace(' ', '<br />', $Entity->translate($this->locale)->getTitolo());
        $HEADER['sottotitolo'] = $Entity->translate($this->locale)->getSottotitolo();
        $HEADER['color']       = $colore;


        return $HEADER;


    }


}