<?php
// 08/02/17, 11.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;

class SelettoreDateFormatter extends DataFormatter {

    public function getData() {

        $oggi = new \DateTime();

        $data['oggi']         = [];
        $data['oggi']['d']    = $oggi->format('d');
        $data['oggi']['m']    = $oggi->format('M');
        $data['oggi']['Y']    = $oggi->format('Y');
        $data['oggi']['date'] = $oggi->format('Y-m-d');

        $next = new \DateTime();
        $next->modify('+7 days');

        $data['next']         = [];
        $data['next']['d']    = $next->format('d');
        $data['next']['m']    = $next->format('M');
        $data['next']['Y']    = $next->format('Y');
        $data['next']['date'] = $next->format('Y-m-d');

        return $data;
    }

    public function extractData() {


        // TODO: Implement extractData() method.
    }

}