<?php
// 25/01/17, 17.10
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppBundle\Entity\Page;
use AppBundle\Service\PagesUrlGenerator;
use Doctrine\ORM\EntityManager;

class MenuFormatter extends DataFormatter
{

    private $pages;

    protected function extractData()
    {

        $this->pages = $this->em->getRepository('AppBundle:Page')->findBy(
            ['level' => $this->data['it']['level']['val'], 'parent' => 0],
            ['sort' => 'ASC']
        );

    }

    public function getData()
    {

        $Page = null;

        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Page) {
            /**
             * @var $Page Page
             */
            $Page = $this->AdditionalData['Entity'];
        }

        $data = [];
        $data['menu'] = [];
        foreach ($this->pages as $page) {
            /**
             * @var $page Page
             */
            $urlInCuiMiTrovo = $this->request->getRequestUri();

            if ($page->getIsEnabled() && $page->translate($this->locale)->getIsEnabled()) {

                $slugs = new PagesUrlGenerator($this->em, $this->request);

                $record = [];

                $record['titolo'] = $page->translate($this->locale)->getTitolo();
                $record['slug'] = $slugs->generaUrl($page, $this->locale);
                $record['active'] = false;
                $record['img'] = false;

                if ($page->getListImgFileName()) {
                    $record['img'] = [];
                    $record['img']['src'] = '/' . $page->getUploadDir() . $page->getListImgFileName();
                    $record['img']['alt'] = $page->getListImgAlt();
                }

                if ($urlInCuiMiTrovo == $record['slug'] || ($Page && $Page->isChildNodeOf($page))
                ) {
                    $record['active'] = true;
                }

                $data['menu'][] = $record;

            }

        }

        return $data;


    }

    public function getTreeData()
    {

        $data = [];
        $data['menu'] = [];

        $Page = null;

        if (isset($this->AdditionalData['Entity']) && $this->AdditionalData['Entity'] instanceof Page) {
            /**
             * @var $Page Page
             */
            $Page = $this->AdditionalData['Entity'];
        }
        foreach ($this->pages as $page) {

            /**
             * @var $page Page
             */

            $urlInCuiMiTrovo = $this->request->getRequestUri();
            if (($page->getIsEnabled() || $page->getId() == 2) && $page->translate($this->locale)->getIsEnabled()) {
                $slugs = new PagesUrlGenerator($this->em, $this->request);

                $record = [];

                $record['titolo'] = $page->translate($this->locale)->getTitolo();
                $record['slug'] = $slugs->generaUrl($page, $this->locale);
                $record['active'] = false;
                if ($urlInCuiMiTrovo == $record['slug'] ||
                    ($urlInCuiMiTrovo != '/' && $urlInCuiMiTrovo != '/' . $this->locale && strpos($record['slug'], $urlInCuiMiTrovo) === 0)
                    ||
                    ($Page && $Page->isChildNodeOf($page))
                ) {
                    $record['active'] = true;
                }


                $figli = [];
                $figli_pagina = $this->em->getRepository('AppBundle:Page')->findBy(
                    ['level' => $this->data['it']['level']['val'], 'parent' => $page->getId()],
                    ['sort' => 'ASC']
                );
                foreach ($figli_pagina as $figlio) {

                    /**
                     * @var $figlio Page
                     */

                    if ($figlio->getIsEnabled() && $figlio->translate($this->locale)->getIsEnabled()) {

                        $slugs = new PagesUrlGenerator($this->em, $this->request);

                        $record2 = [];
                        $record2['titolo'] = $figlio->translate($this->locale)->getTitolo();
                        $record2['slug'] = $slugs->generaUrl($figlio, $this->locale);
                        $record2['active'] = false;
                        $record2['img'] = false;

                        if ($urlInCuiMiTrovo == $record['slug'] ||
                            ($urlInCuiMiTrovo != '/' && strpos($record2['slug'], $urlInCuiMiTrovo) === 0)
                        ) {
                            $record2['active'] = true;
                        }

                        if ($figlio->getListImgFileName()) {
                            $record2['img'] = [];
                            $record2['img']['src'] = '/' . $figlio->getUploadDir() . $figlio->getListImgFileName();
                            $record2['img']['alt'] = $figlio->getListImgAlt();
                        }

                        $figli[] = $record2;

                    }
                };
                $record['figli'] = $figli;

                $data['menu'][] = $record;

            }


        }

        return $data;


    }

    public function getTreeDataWithDisabled()
    {

        $data = [];
        $data['menu'] = [];

        foreach ($this->pages as $page) {

            /**
             * @var $page Page
             */

            $urlInCuiMiTrovo = $this->request->getRequestUri();

            $slugs = new PagesUrlGenerator($this->em, $this->request);

            $record = [];

            $record['titolo'] = $page->translate($this->locale)->getTitolo();
            $record['slug'] = $slugs->generaUrl($page, $this->locale);
            $record['active'] = false;

            if ($urlInCuiMiTrovo == $record['slug'] ||
                ($urlInCuiMiTrovo != '/' && strpos($record['slug'], $urlInCuiMiTrovo) === 0)
            ) {
                $record['active'] = true;
            }
            $figli = [];
            $figli_pagina = $this->em->getRepository('AppBundle:Page')->findBy(
                ['level' => $this->data['it']['level']['val'], 'parent' => $page->getId()],
                ['sort' => 'ASC']
            );
            foreach ($figli_pagina as $figlio) {

                /**
                 * @var $figlio Page
                 */

                if ($figlio->getIsEnabled() && $figlio->translate($this->locale)->getIsEnabled()) {

                    $slugs = new PagesUrlGenerator($this->em, $this->request);

                    $record2 = [];
                    $record2['titolo'] = $figlio->translate($this->locale)->getTitolo();
                    $record2['slug'] = $slugs->generaUrl($figlio, $this->locale);
                    $record2['active'] = false;
                    $record2['img'] = false;

                    if ($urlInCuiMiTrovo == $record['slug'] ||
                        ($urlInCuiMiTrovo != '/' && strpos($record2['slug'], $urlInCuiMiTrovo) === 0)
                    ) {
                        $record2['active'] = true;
                    }

                    if ($figlio->getListImgFileName()) {
                        $record2['img'] = [];
                        $record2['img']['src'] = '/' . $figlio->getUploadDir() . $figlio->getListImgFileName();
                        $record2['img']['alt'] = $figlio->getListImgAlt();
                    }

                    $figli[] = $record2;

                }
            };
            $record['figli'] = $figli;

            $data['menu'][] = $record;


        }

        return $data;


    }

    public function getFullpageAnchors()
    {

        $Page = null;

        if (isset($this->AdditionalData['Entity'])) {
            /**
             * @var $Page Page
             */
            $Page = $this->AdditionalData['Entity'];
        }

        $data = [];
        $data['menu'] = [];
        foreach ($this->pages as $page) {
            /**
             * @var $page Page
             */
            $urlInCuiMiTrovo = $this->request->getRequestUri();

            if ($page->getIsEnabled() && $page->translate($this->locale)->getIsEnabled()) {

                $slugs = new PagesUrlGenerator($this->em, $this->request);

                $sections = [];
                foreach ($page->getPageRows() as $s) {
                    if ($s->getLocale() == $this->locale) {
                        array_push($sections, $s);
                    }
                }

                $record = [];
                $record['sections'] = $sections;
                $record['titolo'] = $page->translate($this->locale)->getTitolo();
                $record['slug'] = $slugs->generaUrl($page, $this->locale);
                $record['active'] = false;
                $record['img'] = false;

                if ($page->getListImgFileName()) {
                    $record['img'] = [];
                    $record['img']['src'] = '/' . $page->getUploadDir() . $page->getListImgFileName();
                    $record['img']['alt'] = $page->getListImgAlt();
                }

                if ($urlInCuiMiTrovo == $record['slug'] ||
                    ($urlInCuiMiTrovo != '/' && strpos($record['slug'], $urlInCuiMiTrovo) === 0)
                    ||
                    ($Page && $Page->isChildNodeOf($page))
                ) {
                    $record['active'] = true;
                }

                $data['menu'][] = $record;

            }

        }

        return $data;

    }


}
