<?php
// 20/02/17, 9.53
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppBundle\Entity\Page;
use AppBundle\Entity\PageRows;

class PageRowsFormatter extends DataFormatter {

    public function extractData() {
        // TODO: Implement getData() method.
    }

    public function getRows($da = 0, $a = 3) {

        /**
         * @var $Page Page
         */
        $Page = $this->AdditionalData['Entity'];

        $rows = [];

        if($Page && $Page instanceof Page) {

            $out = '';

            foreach($Page->getPageRows() as $PageRow) {
                /**
                 * @var $PageRow PageRows
                 */
                if($PageRow->getLocale() == $this->locale) {
                    $rows[] = $PageRow;
                }
            }

            $rows = array_slice($rows, 0, 3);

        }

        $data         = [];
        $data['rows'] = $rows;

        return $data;


    }

    public function getData() {

        /**
         * @var $Page Page
         */
        $Page = $this->AdditionalData['Entity'];

        $rows = [];

        if($Page) {

            $primaRiga = false;
            if($this->data[$this->locale]['da_riga']['val']) {
                $primaRiga = intval($this->data[$this->locale]['da_riga']['val'] - 1);
            }
            $length = null;

            if(is_numeric($this->data[$this->locale]['a_riga']['val']) && $this->data[$this->locale]['a_riga']['val'] > $primaRiga) {
                $length = $this->data[$this->locale]['a_riga']['val'] - $primaRiga;
            }

            $out = '';

            foreach($Page->getPageRows() as $PageRow) {
                /**
                 * @var $PageRow PageRows
                 */
                if($PageRow->getLocale() == $this->locale) {
                    $rows[] = $PageRow;
                }
            }

            if($primaRiga !== false) {
                $rows = array_slice($rows, $primaRiga, $length);
            }

        }

        $data         = [];
        $data['rows'] = $rows;

        return $data;

    }

}
