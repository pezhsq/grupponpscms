<?php
// 03/03/17, 11.40
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppBundle\Entity\Commento;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsTranslation;
use AppBundle\Form\CommentoForm;

class CommentoFormatter extends DataFormatter {

    private $form;
    /**
     * @var $News News
     */
    private $News;

    public function getData() {

        $data                 = [];
        $data['commenti']     = $this->getCommenti();
        $data['commentabile'] = false;
        if($this->News) {

            $data['commentabile'] = $this->News->getCommentiEnabled();
            if($data['commentabile']) {
                $data['form'] = $this->form->createView();
            }
            $data['uniqid'] = uniqid();
        }

        return $data;
    }

    public function extractData() {

        $this->getNews();
        $this->getForm();


    }

    private function getForm() {

        $Commento = new Commento();
        $Commento->setNews($this->News);
        $Commento->setParent(0);

        if($this->News) {

            $this->form = $this->container->get('form.factory')->create(CommentoForm::class, $Commento, ['action' => $this->container->get('app.path_manager')->generateUrl('commenta', ['id' => $this->News->getId()])]);
        }

    }

    private function getNews() {

        $request = $this->request;

        if(!$request->get('slugnotizia')) {
            return;
        }

        /**
         * @var $NewsTranslation NewsTranslation
         */
        $NewsTranslation = $this->em->getRepository('AppBundle:NewsTranslation')->findOneBy(['slug' => $request->get('slugnotizia'), 'isEnabled' => 1, 'locale' => $this->locale]);

        if(!$NewsTranslation) {
            throw new \Exception('Parametri errati, non posso estrarre la news con slug "'.$request->get('slugnotizia').'"');
        }

        /**
         * @var $News News
         */
        $this->News = $NewsTranslation->getTranslatable();

    }


    private function getCommenti() {

        if($this->News) return $this->em->getRepository('AppBundle:Commento')->getCommentiForNews($this->News);

        return false;
    }


}