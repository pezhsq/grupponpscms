<?php
namespace AppBundle\DataFormatter;

use AppBundle\Entity\NewsCategory;

class CategorieBlogFormatter extends DataFormatter {

    private $NewsCategories;

    public function getData() {

        $data = [];

        $pathManager = $this->container->get('app.path_manager');

        foreach($this->NewsCategories as $newsCategory) {

            $count = $this->em->getRepository('AppBundle:NewsHasCategory')->getNewsCount($newsCategory, $this->locale);

            /**
             * @var $newsCategory NewsCategory
             */

            $record            = [];
            $record['url']     = $pathManager->generateUrl('blog_category', ['slug' => $newsCategory->translate($this->locale)->getSlug()]);
            $record['label']   = $newsCategory->translate($this->locale)->getTitolo();
            $record['summary'] = $count;

            $data['categories'][] = $record;

        }

        return $data;

    }

    public function extractData() {

        $this->NewsCategories = $this->em->getRepository('AppBundle:NewsCategory')->findBy(['isEnabled' => 1], ['sort' => 'ASC']);


    }

}