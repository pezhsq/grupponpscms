<?php
// 25/01/17, 17.12
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppBundle\Service\PathManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class DataFormatter {

    /**
     * @var EntityManager
     */
    protected $em;

    protected $data;

    protected $locale;

    protected $AdditionalData;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(ContainerInterface $container, Request $request, $data = [], $AdditionalData = null) {

        $this->container      = $container;
        $this->request        = $request;
        $this->AdditionalData = $AdditionalData;
        $this->em             = $container->get('doctrine')->getManager();
        $this->data           = $data;
        $this->locale         = $this->request->getLocale();

        $this->extractData();

    }

    public function setRequest(Request $request) {

        $this->request = $request;

    }

    public function setWidgetConfig($config) {

        $this->data = $config;

    }

    public function setAdditionalData($AdditionalData) {

        $this->AdditionalData = $AdditionalData;

    }

    abstract protected function getData();

    abstract protected function extractData();

}