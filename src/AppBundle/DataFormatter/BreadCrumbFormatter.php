<?php
// 12/02/17, 8.50
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppBundle\Entity\News;
use AppBundle\Entity\NewsCategory;
use AppBundle\Entity\Page;

class BreadCrumbFormatter extends DataFormatter
{

    public function getData()
    {

        $request = $this->request;
        $translator = $this->container->get('translator');
        $pathManager = $this->container->get('app.path_manager');
        $pageUrlGenerator = $this->container->get('app.page_url_generator');

        $attributes = $request->attributes->all();

        $items = [];

        if (isset($attributes['_route'])) {

            $route = preg_replace('/_it$/', '', $attributes['_route']);

            $item = [];
            $item['label'] = $translator->trans('default.labels.home', [], 'public');
            $item['url'] = $pathManager->generateUrl('home');

            $items[] = $item;
            switch ($route) {
                case "checkout_grazie":
                    $item = [];
                    $item['label'] = $translator->trans('ecommerce.labels.thankYou', [], "public");
                    $item['url'] = "#";
                    $items[] = $item;
                    break;
                case 'brand_ecommerce':
                    $item = [];
                    $item['label'] = $translator->trans('default.labels.product_brands');
                    $item['url'] = "#";
                    $items[] = $item;
                    $item = [];
                    $item['label'] = $this->AdditionalData['Entity']->translate($this->locale)->getTitolo();
                    $item['url'] = $pathManager->generateUrl("brand_ecommerce", ["slug" => $this->AdditionalData['Entity']->translate($this->locale)->getSlug()], $this->locale);

                    $items[] = $item;
                    break;
                case 'account_edit':
                    $item = [];
                    $item['label'] = "Account";
                    $item['url'] = "#";

                    $items[] = $item;

                    $item = [];
                    $item['label'] = $translator->trans('ecommerce.labels.modifica_i_tuoi_dati');
                    $item['url'] = $pathManager->generateUrl("account_edit", [], $this->locale);

                    $items[] = $item;


                    break;
                case 'account_wishlist':
                    $item = [];
                    $item['label'] = "Account";
                    $item['url'] = "#";

                    $items[] = $item;

                    $item = [];
                    $item['label'] = $translator->trans('ecommerce.labels.wishlist');
                    $item['url'] = $pathManager->generateUrl("account_wishlist", [], $this->locale);

                    $items[] = $item;


                    break;
                case 'account_orders':
                    $item = [];
                    $item['label'] = "Account";
                    $item['url'] = "#";

                    $items[] = $item;

                    $item = [];
                    $item['label'] = $translator->trans('ecommerce.labels.ordini');
                    $item['url'] = $pathManager->generateUrl("account_orders", [], $this->locale);

                    $items[] = $item;


                    break;

                case 'blog':

                    $item = [];
                    $item['label'] = $translator->trans('default.labels.blog', [], 'public');
                    $item['url'] = $pathManager->generateUrl('blog', [], $this->locale);

                    $items[] = $item;


                    break;

                case 'blog_category':


                    $item = [];
                    $item['label'] = $translator->trans('default.labels.blog', [], 'public');
                    $item['url'] = $pathManager->generateUrl('blog', [], $this->locale);

                    $items[] = $item;
                    $item = [];
                    $item['label'] = $this->AdditionalData['NewsCategory']->translate($this->locale)->getTitolo();
                    $item['url'] = $pathManager->generateUrl('blog_category', ['slug' => $this->AdditionalData['NewsCategory']->translate($this->locale)->getSlug()], $this->locale);

                    $items[] = $item;

                    break;

                case 'blog_read_news':

                    /**
                     * @var $News News
                     */
                    $News = $this->AdditionalData['Entity'];

                    /**
                     * @var $NewsCategory NewsCategory
                     */
                    $NewsCategory = $News->getPrimaryCategory();

                    $item = [];
                    $item['label'] = $translator->trans('default.labels.blog', [], 'public');
                    $item['url'] = $pathManager->generateUrl('blog', [], $this->locale);

                    $items[] = $item;


                    $item = [];
                    $item['label'] = $NewsCategory->translate($this->locale)->getTitolo();
                    $item['url'] = $pathManager->generateUrl('blog_category', ['slug' => $NewsCategory->translate($this->locale)->getSlug()], $this->locale);

                    $items[] = $item;

                    $item = [];
                    $item['label'] = $News->translate($this->locale)->getTitolo();
                    $item['url'] = $pathManager->generateUrl('blog_read_news', ['slug' => $NewsCategory->translate($this->locale)->getSlug(), 'slugnotizia' => $News->translate($this->locale)->getSlug()], $this->locale);

                    $items[] = $item;


                    break;

                case 'page':

                    /**
                     * @var $Page Page
                     */
                    $Page = $this->AdditionalData['Entity'];

                    $ids = explode('/', trim($Page->getMaterializedPath(), '/'));

                    foreach ($ids as $id) {

                        $ParentPage = $this->em->getRepository('AppBundle:Page')->findOneBy(['id' => $id]);

                        if ($ParentPage) {

                            $item = [];
                            $item['label'] = $ParentPage->translate($this->locale)->getTitolo();
                            $item['url'] = $pageUrlGenerator->generaUrl($ParentPage, $this->locale);

                            $items[] = $item;
                        }

                    }

                    $item = [];
                    $item['label'] = $Page->translate($this->locale)->getTitolo();
                    $item['url'] = $pageUrlGenerator->generaUrl($Page, $this->locale);

                    $items[] = $item;

                    break;

                case 'categoria_ecommerce':
                    $Category = $this->AdditionalData['Entity'];

                    $ids = explode('/', trim($Category->getMaterializedPath(), '/'));
                    array_shift($ids);
                    $slug = "";
                    foreach ($ids as $id) {

                        $ParentCategory = $this->em->getRepository("WebtekEcommerceBundle:Category")->findOneBy(['id' => $id]);

                        if ($ParentCategory) {
                            if ($slug != "") {
                                $slug .= "/";
                            }
                            $slug .= $ParentCategory->translate($this->locale)->getSlug();
                            $item = [];
                            $item['label'] = $ParentCategory->translate($this->locale)->getNome();
                            $item['url'] = $pathManager->generateUrl('categoria_ecommerce', ['slug' => $slug], $this->locale);

                            $items[] = $item;
                        }

                    }
                    if ($slug != "") {
                        $slug .= "/";
                    }
                    $item = [];
                    $item['label'] = $Category->translate($this->locale)->getNome();
                    $item['url'] = $pathManager->generateUrl('categoria_ecommerce', ['slug' => $slug . $Category->translate($this->locale)->getSlug()], $this->locale);


                    $items[] = $item;

                    break;

                case "prodotto_ecommerce":
                    $Product = $this->AdditionalData['Entity'];
                    $Category = $this->container->get("app.webtek_ecommerce.services.product_helper")->guessCategory($Product);
                    $ids = explode('/', trim($Category->getMaterializedPath(), '/'));
                    array_shift($ids);
                    $slug = "";
                    foreach ($ids as $id) {

                        $ParentCategory = $this->em->getRepository("WebtekEcommerceBundle:Category")->findOneBy(['id' => $id]);

                        if ($ParentCategory) {
                            if ($slug != "") {
                                $slug .= "/";
                            }
                            $slug .= $ParentCategory->translate($this->locale)->getSlug();
                            $item = [];
                            $item['label'] = $ParentCategory->translate($this->locale)->getNome();
                            $item['url'] = $pathManager->generateUrl('categoria_ecommerce', ['slug' => $slug], $this->locale);

                            $items[] = $item;
                        }

                    }

                    $item = [];
                    $item['label'] = $Category->translate($this->locale)->getNome();
                    $item['url'] = $pathManager->generateUrl('categoria_ecommerce', ['slug' => $Category->translate($this->locale)->getSlug()], $this->locale);

                    $item = [];
                    $item['label'] = $Product->translate($this->locale)->getTitolo();
                    $item['url'] = $pathManager->generateUrl('prodotto_ecommerce', ['slug' => $Product->translate($this->locale)->getSlug()], $this->locale);


                    $items[] = $item;
                    break;

                case 'carrello_list':

                    $item = [];
                    $item['label'] = $translator->trans('ecommerce.labels.carrello', [], 'public');
                    $item['url'] = $pathManager->generateUrl('carrello_list', [], $this->locale);

                    $items[] = $item;
                    break;

                case 'checkout':
                    $item = [];
                    $item['label'] = $translator->trans('ecommerce.labels.checkout', [], 'public');
                    $item['url'] = $pathManager->generateUrl('checkout', [], $this->locale);

                    $items[] = $item;
                    break;
                case 'registrazione':
                    $item = [];
                    $item['label'] = $translator->trans('ecommerce.labels.registrazione', [], 'public');
                    $item['url'] = $pathManager->generateUrl('registrazione', [], $this->locale);

                    $items[] = $item;
                    break;


            }

        }


        return ['items' => $items];
    }

    public function extractData()
    {


    }


}