<?php
// 24/01/17, 16.43
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\DataFormatter;


use AppBundle\Entity\Vetrina;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;

class VetrinaFormatter extends DataFormatter
{


    public function getSlide()
    {

        $vetrina = $this->em->getRepository('AppBundle:Vetrina')->findOneBy(
            ['name' => $this->data['it']['id_slide']['val']]
        );

        /**
         * @var $vetrina Vetrina
         */

        $slug = new Slugify();
        $slug->addRule('à', 'a');
        $slug->addRule('è', 'e');
        $slug->addRule('é', 'e');
        $slug->addRule('ì', 'i');
        $slug->addRule('ò', 'o');
        $slug->addRule('ù', 'u');

        $record                 = [];
        $record['titolo']       = $vetrina->translate($this->locale)->getTitolo();
        $record['slug']         = $slug->slugify($record['titolo']);
        $record['sottotitolo']  = $vetrina->translate($this->locale)->getSottotitolo();
        $record['testo']        = $vetrina->translate($this->locale)->getTesto();
        $record['url']          = $vetrina->translate($this->locale)->getUrl();
        $record['image']        = [];
        $record['image']['src'] = "/".$vetrina->getUploadDir().$vetrina->getListImgFileName();
        $record['image']['alt'] = $vetrina->getListImgAlt();

        return $record;

    }

    public function getData()
    {

        $vc = $this->em->getRepository('AppBundle:VetrinaCategory')->findOneBy(
            ['nome' => $this->data['it']['category']['val']]
        );

        $data = [];

        $data['error'] = false;

        if (!$vc) {
            $data['error'] = 'Non è stato configurato correttamente il modulo vetrina (categoria errata)';
        } else {

            $this->vetrine = $vc->getVetrine();

            $data['slides'] = [];
            $data['column'] = isset($this->data['it']['column']) ? $this->data['it']['column']['val'] : 1;
            $data['titolo'] = isset($this->data[$this->locale]['titolo']) ? $this->data[$this->locale]['titolo']['val'] : 1;
            $data['testo']  = isset($this->data[$this->locale]['testo']) ? $this->data[$this->locale]['testo']['val'] : 1;

            foreach ($this->vetrine as $vetrina) {

                /**
                 * @var $vetrina Vetrina
                 */

                if ($vetrina->getIsEnabled() && !$vetrina->getDeletedAt()) {

                    /**
                     * @var $vetrina Vetrina
                     */

                    $slug = new Slugify();
                    $slug->addRule('à', 'a');
                    $slug->addRule('è', 'e');
                    $slug->addRule('é', 'e');
                    $slug->addRule('ì', 'i');
                    $slug->addRule('ò', 'o');
                    $slug->addRule('ù', 'u');

                    $record                 = [];
                    $record['id']           = $vetrina->getId();
                    $record['titolo']       = $vetrina->translate($this->locale)->getTitolo();
                    $record['slug']         = $slug->slugify($record['titolo']);
                    $record['sottotitolo']  = $vetrina->translate($this->locale)->getSottotitolo();
                    $record['testo']        = $vetrina->translate($this->locale)->getTesto();
                    $record['url']          = $vetrina->translate($this->locale)->getUrl();
                    $record['image']        = [];
                    $record['image']['src'] = "/".$vetrina->getUploadDir().$vetrina->getListImgFileName();
                    $record['image']['alt'] = $vetrina->getListImgAlt();

                    $data['slides'][] = $record;
                }

            }

        }

        return $data;


    }

    protected function extractData()
    {


    }


}
