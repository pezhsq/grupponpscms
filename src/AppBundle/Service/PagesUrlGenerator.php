<?php
// 27/01/17, 13.55
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Page;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class PagesUrlGenerator
{

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var RequestStack
     */
    private $request;

    public function __construct(EntityManager $em, $request)
    {

        $this->em = $em;

        if ($request instanceof RequestStack) {
            /**
             * @var $request RequestStack;
             */
            $this->request = $request->getCurrentRequest();
        } else {
            $this->request = $request;
        }

    }

    public function generaUrl(Page $page, $locale = 'it', $absolute = false)
    {

        $path = '';

        if ($absolute) {
            $path = $this->request->getSchemeAndHttpHost();
        }


        $slugs = [];


        $slug = $page->translate($locale)->getSlug();

        if ($slug != 'home') {

            $slugs[] = $slug;

        }

        $parentId = $page->getParent();

        while ($parentId) {

            /**
             * var $parent Page;
             */
            $page = $this->em->getRepository('AppBundle:Page')->findOneBy(['id' => $parentId]);

            $slugs[] = $page->translate($locale)->getSlug();

            $parentId = $page->getParent();

        }


        $slugs = array_reverse($slugs);

        $slugs = implode('/', $slugs);


        if ($locale && $locale != 'it') {
            if ($slugs) {
                $slugs = $locale.'/'.$slugs;
            } else {
                $slugs = $locale;
            }
        }

        return $path.'/'.$slugs;


    }

}