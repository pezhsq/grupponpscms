<?php
// 12/01/17, 11.58
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use Sensio\Bundle\DistributionBundle\Composer\ScriptHandler;
use Symfony\Component\Yaml\Yaml;

class TranslationsSaver
{

    private $rootDir;

    private $yamlDirectory;

    private $yamlFile;

    private $jsFile;

    private $webDir;

    private $jsDirectoryAdmin;

    private $jsDirectoryPublic;

    private $italianData;
    /**
     * @var FsHelper
     */
    private $fsHelper;

    public function __construct($rootDir, WebDir $webDir, FsHelper $fsHelper)
    {

        $this->rootDir = $rootDir;

        $this->yamlDirectory = $this->rootDir.'/'.'Resources'.'/'.'translations'.'/';
        $this->webDir = $webDir->get();
        $this->jsDirectoryAdmin = $this->webDir.'/admin'.'/'.'js'.'/'.'i18n'.'/';
        $this->jsDirectoryPublic = $this->webDir.'/public'.'/'.'js'.'/'.'i18n'.'/';

        $this->fsHelper = $fsHelper;
    }

    private function generateFileName($file, $lang)
    {

        $this->yamlFile = $file.'.'.$lang.'.yml';

        if ($file == 'messages') {

            $this->readItalianYamlFile($file);

            $this->jsFile = $lang.'.js';

        }

    }

    public function readYamlFile($file, $lang)
    {

        $this->generateFileName($file, $lang);

        $path = $this->yamlDirectory.$this->yamlFile;

        if (file_exists($path)) {

            $data = Yaml::parse(file_get_contents($path));

            return $data;

        }

        file_put_contents($path, Yaml::dump([]));

        return [];

    }

    private function readItalianYamlFile($file)
    {

        $yamlFile = $file.'.it.yml';

        $path = $this->yamlDirectory.$yamlFile;

        $this->italianData = [];

        if (file_exists($path)) {
            $this->italianData = Yaml::parse(file_get_contents($path));
        }

    }

    public function setValue($lang, $fileName, $section, $subSection, $key, $value)
    {

        $data = $this->readYamlFile($fileName, $lang);

        if (!isset($data[$section])) {
            $data[$section] = [];
        }

        if (!isset($data[$section][$subSection])) {
            $data[$section][$subSection] = [];
        }

        if (!isset($data[$section][$subSection][$key])) {
            $data[$section][$subSection][$key] = [];
        }

        $data[$section][$subSection][$key] = $value;

        $this->writeToFile($data);

    }

    public function writeToFile($data)
    {

        file_put_contents($this->yamlDirectory.$this->yamlFile, Yaml::dump($data));

        if ($this->jsFile) {

            $this->toJSformat($data);


        }

        $this->fsHelper->clearCache();


    }

    private function toJSformat($data)
    {

        $jsData = [];

        //dump($this->italianData);

        foreach ($data as $section => $sectionData) {
            $jsData[$section] = [];
            if (isset($sectionData['labels'])) {
                foreach ($sectionData['labels'] as $key => $value) {
                    //dump($section.'.'.$key.'- ita:'.$this->italianData[$section]['labels'][$key].' - trans:'.$value);
                    if (isset($this->italianData[$section]['labels'][$key])) {
                        $jsData[$section][$this->italianData[$section]['labels'][$key]] = $value;
                    }
                }
            }
        }

        $string = 'var Locale = '.json_encode($jsData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE).';';

        file_put_contents($this->jsDirectoryAdmin.$this->jsFile, $string);

    }


}