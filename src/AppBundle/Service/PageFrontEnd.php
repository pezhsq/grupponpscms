<?php
// 06/02/17, 11.51
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\News;
use AppBundle\Entity\NewsRows;
use AppBundle\Entity\Page;
use AppBundle\Entity\PageRows;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Translation\TranslatorInterface;

class PageFrontEnd
{

    /**
     * @var Translator
     */
    private $translator;
    /**
     * @var EngineInterface
     */
    private $twig;
    private $layout;
    /**
     * @var EntityManager
     */
    private $em;


    /**
     * BlogFrontend constructor.
     */
    public function __construct(TranslatorInterface $translator, EngineInterface $twig, EntityManager $em, $generali)
    {

        $this->translator = $translator;
        $this->twig       = $twig;
        $this->layout     = $generali['layout'];
        $this->em         = $em;
    }

    private function getRow($row)
    {

        return $this->twig->render('public/layouts/'.$this->layout.'/includes/page-row.twig', ['row' => $row]);

    }


    public function getPageText($pageRows = [], $locale = 'it')
    {

        $out = '';

        foreach ($pageRows as $row) {
            /**
             * @var $row PageRows
             */

            $out .= $this->getRow($row);

        }

        return $out;

    }

    public function getText($row, $slotNumber = 1)
    {

        $method = 'getSlot'.$slotNumber;

        $re = '/\[youtube(.*)\]/';

        $text = $row->$method();

        preg_match_all($re, $text, $m);

        if ($m) {

            foreach ($m[1] as $k => $html) {

                $rawHtml = html_entity_decode($html, ENT_QUOTES, 'UTF-8');

                $reUrl = '/url="([^"]+)"/';

                preg_match($reUrl, $rawHtml, $mUrl);

                $data            = [];
                $data['code']    = false;
                $data['caption'] = false;

                if ($mUrl) {

                    $youtubeData['code'] = trim(parse_url($mUrl[1], PHP_URL_PATH), '/');

                    $reCaption = '/caption="([^"]+)"/';

                    preg_match($reCaption, $rawHtml, $mCaption);

                    if ($mCaption) {
                        $youtubeData['caption'] = $mCaption[1];
                    }

                    $text = str_replace(
                        $m[0][$k],
                        $this->twig->render(
                            'public/layouts/'.$this->layout.'/includes/youtube.twig',
                            ['youtubeData' => $youtubeData]
                        ),
                        $text
                    );

                }
            }

        }

        return $text;

    }


}