<?php
// 09/01/17, 10.09
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;

use Cocur\Slugify\Slugify;

class Slugger
{

    /**
     * @var Slugify
     */
    private $slugify;

    private $repository;

    public function __construct(Slugify $slugify)
    {

        $this->slugify = $slugify;

        $this->slugify->addRule('à', 'a');
        $this->slugify->addRule('è', 'e');
        $this->slugify->addRule('é', 'e');
        $this->slugify->addRule('ì', 'i');
        $this->slugify->addRule('ò', 'o');
        $this->slugify->addRule('ù', 'u');

    }

    public function slugify($translationEntity, $repository, $slug = null)
    {

        $this->repository = $repository;

        if (!$slug) {
            $slug = $this->slugify->slugify($translationEntity->getTitolo());
        }

        $newSlug = $slug;

        $i = 1;

        while (!$this->checkSlug($newSlug, $translationEntity)) {

            $newSlug = $slug.'-'.$i;

            $i++;

        }

        return $newSlug;


    }

    private function checkSlug($slug, $translationEntity)
    {

        $data = $this->repository->findBySlugButNotId(
            $slug,
            $translationEntity->getLocale(),
            $translationEntity->getId()
        );

        return !(bool)count($data);

    }


}