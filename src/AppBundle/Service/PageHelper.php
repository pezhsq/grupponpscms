<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 18/01/17
 * Time: 17.27
 */

namespace AppBundle\Service;


use AppBundle\Entity\Page;
use Doctrine\ORM\EntityManager;
use Gedmo\Translatable\TranslatableListener;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PageHelper
{

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var WebDir
     */
    private $webDir;

    public function __construct(ContainerInterface $container)
    {

        $this->em = $container->get('doctrine.orm.default_entity_manager');
        $this->webDir = $container->get('app.web_dir');
        $this->container = $container;
    }

    public function riordina(array $dati, $parent = null)
    {


        foreach ($dati['children'] as $k => $nodo) {

            $repo = $this->em->getRepository('AppBundle:Page');

            $id = $nodo['id'];

            $Page = $repo->findOneBy(['id' => $id]);

            if ($parent) {
                $Page->setParentNode($parent);
                $Page->setParent($parent->getId());
            } else {
                $Page->setParent(0);
                $Page->setMaterializedPath('');
            }

            $Page->setSort($k);

            $this->em->persist($Page);

            if (isset($nodo['children'])) {
                $this->riordina($nodo, $Page);
            }

        }

        if (!$parent) {

            $this->em->flush();
        }
    }

    function getTree($parent = 0, $getDisabled = false, $excludedPage = null, $locale = 'it')
    {

        $data = [];

        if (!$parent) {

            $data['[000] + Livello Base'] = 0;

        }

        $Pages = $this->em->getRepository('AppBundle:Page')->getByParent($parent, $getDisabled, $excludedPage, $locale);

        $prefix = '+';

        foreach ($Pages as $Page) {

            /**
             * @var $Page Page
             */

            $prefix = '+'.str_repeat('--', $Page->getNodeLevel() - 1);

            if ($Page->isLeafNode()) {
                $prefix .= '-';
            } else {
                $prefix .= '+';
            }

            $data['['.str_pad($Page->getId(), 3, '0').'] '.$prefix.' '.$Page->translate($locale)->getTitolo()]
                = $Page->getId();

            $data = array_merge($data, $this->getTree($Page->getId(), $getDisabled, $excludedPage, $locale));

        }

        return $data;


    }

    public function getParentTitle(Page $Page)
    {

        if ($Page && $Page->getParent()) {
            $Page = $this->em->getRepository('AppBundle:Page')->findOneBy(['id' => $Page->getParent()]);

            return $Page->translate()->getTitolo();
        }

        return 'Livello base';

    }

    public function jsonTree(Page $Page, $allElements, $arr = [])
    {

        $json['text'] = (string)$Page;
        $json['id'] = $Page->getId();
        $json['children'] = [];

        $Childs = $this->em->getRepository('AppBundle:Page')->findBy(['parent' => $Page->getId()], ['sort' => 'asc']);

        if ($Childs) {
            foreach ($Childs as $child) {
                /**
                 * @var $child Page
                 */
                $json['children'][] = array_merge($this->jsonTree($child, $allElements, $arr), $arr);
            }
        }

        $json = array_merge($json, $arr);

        return $json;

    }

    public function correggiDimensioni(Page $page, $field, $dimensioni)
    {

        $method = 'get'.$field.'Filename';

        $percorso = $this->webDir->get().'/'.$page->getUploadDir().$page->$method();
        
        if (file_exists($percorso)) {

            $imageHelper = $this->container->get('app.image_helper');

            $resizeData = json_decode($dimensioni, true);

            $imageHelper->resize('/'.$page->getUploadDir().$page->$method(), $resizeData);

        }

    }
}