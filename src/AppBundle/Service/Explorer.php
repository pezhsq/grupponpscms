<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 15/12/16
 * Time: 16.59
 */

namespace AppBundle\Service;


use Symfony\Component\Finder\Finder;

class Explorer {

    private $rootDir;

    public function __construct($rootDir) {

        $this->rootDir = $rootDir;
    }

    public function getRootDir() {

        return $this->rootDir;
    }

    public function listFiles($relPath, $extension = null) {

        $finder = new Finder();

        $finder = $finder->files()->in($this->rootDir.'/'.$relPath);

        if($extension) {
            $finder = $finder->name('*'.$extension);
        }

        $data = [];

        foreach($finder as $file) {
            $name = basename($file->getRealPath());
            if($extension) {
                $name = str_replace($extension, '', $name);
            }
            $data[$name] = $name;
        }

        asort($data);

        return $data;

    }

}