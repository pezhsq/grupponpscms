<?php
// 10/05/17, 18.00
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use Imagine\Filter\Basic\Crop;
use Imagine\Filter\Basic\Resize;
use Imagine\Filter\Basic\Save;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Imagine\Filter\PostProcessor\JpegOptimPostProcessor;
use Liip\ImagineBundle\Imagine\Filter\PostProcessor\OptiPngPostProcessor;
use Liip\ImagineBundle\Model\Binary;
use Symfony\Component\HttpFoundation\File\File;

class ImageHelper
{

    /**
     * @var WebDir
     */
    private $webDir;

    private $binary;

    private $mimeType;

    private $File = null;

    private $fullPath = false;

    private $jpegOptimPath = "jpegoptim";
    private $optiPngPath = "optipng";

    /**
     * ImageHelper constructor.
     */
    public function __construct(WebDir $webDir, $parametri)
    {

        $this->webDir = $webDir;
        if (isset($parametri['jpegOptimPath']) && trim($parametri['jpegOptimPath']) != "")
            $this->jpegOptimPath = $parametri['jpegOptimPath'];
        if (isset($parametri['optiPngPath']) && trim($parametri['optiPngPath']) != "")
            $this->optiPngPath = $parametri['optiPngPath'];

    }

    private function createAbsolutePath($path)
    {

        $path = $this->webDir->normalizeWindowsPath(realpath($path));

        if (strpos($path, $this->webDir->get()) === false) {
            $this->fullPath = $this->webDir->get() . $path;
        } else {
            $this->fullPath = $path;
        }

        return $this->fullPath;

    }

    private function open($path)
    {

        $path = $this->createAbsolutePath($path);

        if (file_exists($path)) {

            $this->File = new File($path);

            $this->binary = new Binary(file_get_contents($path), $this->File->getMimeType());

            $this->mimeType = $this->File->getMimeType();

            return true;

        }

        throw new \Exception('Il file ' . $path . ' non esiste');

    }

    public function resize($path, $resizeData = null, $compress = true)
    {

        $path = $this->createAbsolutePath($path);


        if ($this->open($path)) {
            $this->doResize($path, $resizeData);
            if ($compress) {
                $this->open($path, true);
                $this->doOptimize($path);
            }
        }

    }

    public function compress($path)
    {

        if ($this->open($path)) {
            $this->doOptimize($path);
        }

    }

    private function doOptimize($path)
    {

        switch ($this->mimeType) {
            case 'image/jpeg':

                $JpegOptim = new JpegOptimPostProcessor($this->jpegOptimPath, true, 70);
                $result = $JpegOptim->process($this->binary);

                break;
            case 'image/png':
                $PngOptim = new OptiPngPostProcessor($this->optiPngPath);
                $result = $PngOptim->process($this->binary);
                break;

        }

        /**
         * @var $result BinaryInterface
         */
        file_put_contents($this->createAbsolutePath($path), $result->getContent());

    }

    private function doResize($path, $resizeData)
    {

        $Imagine = new Imagine();

        $toResize = $Imagine->open($path);

        $data = getimagesize($path);

        $Cropper = new Crop(
            new Point($resizeData['points'][0], $resizeData['points'][1]), new Box(
                $resizeData['points'][2] - $resizeData['points'][0], $resizeData['points'][3] - $resizeData['points'][1]
            )
        );

        try {
            $Cropper->apply($toResize);
            $Resizer = new Resize(new Box($resizeData['dimensioni'][0], $resizeData['dimensioni'][1]));

            $Resizer->apply($toResize);

            $Saver = new Save($path);

            $Saver->apply($toResize);

        } catch (\Exception $e) {
        }

    }


}