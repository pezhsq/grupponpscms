<?php
// 09/01/17, 10.09
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Page;
use AppBundle\Entity\NewsTranslation;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Tree\Fixture\Closure\News;

class NewsSlugger
{

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var EntityManager
     */
    private $em;

    private $news;

    public function __construct(Slugify $slugify, EntityManager $em)
    {

        $this->slugify = $slugify;

        $this->slugify->addRule('à', 'a');
        $this->slugify->addRule('è', 'e');
        $this->slugify->addRule('é', 'e');
        $this->slugify->addRule('ì', 'i');
        $this->slugify->addRule('ò', 'o');
        $this->slugify->addRule('ù', 'u');

        $this->em = $em;
    }

    public function slugify(NewsTranslation $newsTranslation, $slug = null)
    {

        $this->news = $newsTranslation->getTranslatable();

        if (!$slug) {

            $seme = $newsTranslation->getTitolo();
            if (strlen($seme) > 30) {
                $seme = wordwrap($seme, 30);
                $seme = substr($seme, 0, strpos($seme, "\n"));
            }

            $slug = $this->slugify->slugify($seme);

        }

        $newSlug = $slug;

        $i = 1;

        while (!$this->checkSlug($newSlug, $newsTranslation->getLocale())) {

            $newSlug = $slug.'-'.$i;

            $i++;

        }

        return $newSlug;


    }

    private function checkSlug($slug, $locale)
    {

        $data = $this->em->getRepository('AppBundle:News')->findBySlugButNotId($slug, $locale, $this->news->getId());

        return !(bool)count($data);


    }


}