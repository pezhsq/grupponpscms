<?php
// 15/03/17, 14.06
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;

use Symfony\Component\Filesystem\Filesystem;

class FsHelper {

    private $rootDir;

    private $composer = false;

    /**
     * FsHelper constructor.
     */
    public function __construct($rootDir) {

        $this->rootDir = $rootDir;

    }

    function copyDir($from, $to) {

        if($from && $to) {

            $fileSystem = new Filesystem();

            if(file_exists($to)) {
                $fileSystem->remove($to);
            }

            $fileSystem->mkdir($to);

            $directoryIterator = new \RecursiveDirectoryIterator($from, \RecursiveDirectoryIterator::SKIP_DOTS);
            $iterator          = new \RecursiveIteratorIterator($directoryIterator, \RecursiveIteratorIterator::SELF_FIRST);
            foreach($iterator as $item) {
                if($item->isDir()) {
                    $fileSystem->mkdir($to.'/'.$iterator->getSubPathName());
                } else {
                    $fileSystem->copy($item, $to.'/'.$iterator->getSubPathName());
                }
            }

            return true;

        }

        return false;

    }

    private function readComposer() {

        if(!$this->composer) {

            $this->composer = json_decode(file_get_contents($this->rootDir."/".'..'."/".'composer.json'), true);
        }

        return $this->composer;

    }

    function clearCache() {

        $composer = $this->readComposer();

        $fs = new Filesystem();

        $prodCacheDir = realpath($this->rootDir.'/../'.$composer['extra']['symfony-var-dir'].'/cache/prod');
        $devCacheDir  = realpath($this->rootDir.'/../'.$composer['extra']['symfony-var-dir'].'/cache/dev');

        $fs->remove($prodCacheDir);
        $fs->remove($devCacheDir);

    }

}