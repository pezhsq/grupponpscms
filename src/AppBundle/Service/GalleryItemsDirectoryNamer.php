<?php
// 07/07/17, 16.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\GalleryItems;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class GalleryItemsDirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping)
    {

        /**
         * @var $object GalleryItems
         */
        $dir = $object->getGallery()->getId().'/'.$object->getId();
        
        return $dir;

    }

}