<?php
// 28/06/17, 6.22
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Group;
use AppBundle\Entity\MappedRoles;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Yaml;

class GroupHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    private $rootDir;
    private $securityFile;


    /**
     * AttributesHelper constructor.
     */
    public function __construct(EntityManager $entityManager, $rootDir)
    {

        $this->entityManager = $entityManager;
        $this->rootDir = $rootDir;
        $this->securityFile = $this->rootDir . '/config/security.yml';
    }

    public function getList($deleted = false)
    {

        $Groups = $this->entityManager->getRepository('AppBundle:Group')->findAll();

        $records = [];

        foreach ($Groups as $Group) {


            /**
             * @var $Group Group;
             */

            $record = [];
            $record['id'] = $Group->getId();
            $record['nome'] = $Group->getName();
            $record['role'] = $Group->getRole();
            $record['isEnabled'] = $Group->getisEnabled();
            $record['deleted'] = $Group->isDeleted();
            $record['createdAt'] = $Group->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Group->getUpdatedAt()->format('d/m/Y H:i:s');

            $records[] = $record;
        }

        return $records;

    }

    private function readYamlFile()
    {

        $path = $this->securityFile;

        if (file_exists($path)) {

            $data = Yaml::parse(file_get_contents($path));

            return $data;

        }

        return false;

    }

    public function writeSecurity()
    {
        $data = $this->readYamlFile();

        $RoleHierarchy = $this->generateRoleYerarchy();

        $data['security']['role_hierarchy'] = $RoleHierarchy;

        file_put_contents($this->securityFile, Yaml::dump($data, 3));


    }

    private function generateRoleYerarchy()
    {

        $RoleHierarchy = [];

        $MappedRoles = $this->entityManager->getRepository('AppBundle:MappedRoles')->findAll();

        $RoleHierarchy['ROLE_SUPER_ADMIN'] = [];
        foreach ($MappedRoles as $mappedRole) {
            $RoleHierarchy['ROLE_SUPER_ADMIN'][] = $mappedRole->getRole();
        }

        $Groups = $this->entityManager->getRepository('AppBundle:Group')->findAll();

        foreach ($Groups as $group) {
            $RoleHierarchy[$group->getRole()] = [];
            foreach ($group->getRoles() as $Role) {
                /**
                 * @var $Role MappedRoles
                 */
                $RoleHierarchy[$group->getRole()][] = $Role->getRole();
            }
        }


        return $RoleHierarchy;

    }


}