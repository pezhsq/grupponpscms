<?php
namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FormContattiHelper
 *
 * @author  Vkfan
 * @package AppBundle\Service
 */
class FormContattiHelper
{

    /** @var Container $container */
    private $container;
    /** @var Request $request */
    private $request;

    /** @var array $data */
    private $data;
    /** @var array $errors */
    private $errors;

    /**
     * FormContattiHelper constructor.
     *
     * @param Container $container
     */
    public function __construct($container)
    {
        $this->container = $container;

        $this->request = $this->container->get("request_stack")->getCurrentRequest();
    }

    /**
     * Controlla che il form sia valido, e parsa i dati della request;
     * Se il form è valido, ritorna true, false altrimenti
     *
     * @param array $required_fields Tutti i campi richiesti, come si chiamano nella request
     *
     * @return bool
     */
    public function validate($required_fields = [])
    {
        $this->data = [];
        $this->errors = [];

        foreach ($this->request->request as $field_name => $field) {
            $this->data[$field_name] = trim(strip_tags($field, "<br>"));

            // Se il campo è tra quelli richiesti, ed è vuoto, lo aggiunge agli errori
            if (in_array($field_name, $required_fields)) {
                if ($this->data[$field_name] === "") {
                    $this->errors[] = $field_name;
                }
                unset($required_fields[array_search($field_name, $required_fields)]);
            }
        }

        // Aggiunge agli errori tutti i campi richiesti non trovati
        foreach ($required_fields as $r) {
            $this->errors[] = $r;
        }

        // Controlla il campo della privacy
        if (!$this->request->request->has("privacy") || $this->request->request->get("privacy") !== "true") {
            $this->errors[] = "privacy";
        }

        if (count($this->errors) > 0) {
            return false;
        }

        return true;
    }

    /**
     * Restituisce i dati parsati dalla request
     *
     * @return string[]
     */
    public function getData()
    {
        return $this->data;
    }
    /**
     * Crea i messaggi d'errore
     *
     * @return string[]
     */
    public function getErrors()
    {
        $translator = $this->container->get("translator");

        $toRet = [];

        if (in_array("privacy", $this->errors)) {
            $toRet[] = $translator->trans("form_contatti.errors.no_privacy");
        }
        foreach ($this->errors as $e) {
            if ($e !== "privacy") {
                $toRet[] = $translator->trans("form_contatti.errors.empty_field", ["%nome_campo%" => preg_replace("/_/", " ", $e)]);
            }
        }

        return $toRet;
    }

}