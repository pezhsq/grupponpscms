<?php
// 10/03/17, 17.31
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


class WebDir
{

    private $rootDir;

    private $webDir;

    private $webPart;

    public function __construct($rootDir)
    {

        $this->rootDir = $rootDir;
    }

    public function getWebPart()
    {

        if (!$this->webPart) {

            $composer = json_decode(file_get_contents($this->rootDir."/".'..'."/".'composer.json'), true);

            $this->webPart = $composer['extra']['symfony-web-dir'];

        }

        return $this->normalizeWindowsPath($this->webPart);

    }


    public function get()
    {

        if (!$this->webDir) {

            $this->webDir = realpath($this->rootDir."/".'..'."/".$this->getWebPart());

            // questa cosa è un po' una porcheria.
            // è fatto per funzionare con il bundle per il deployment, in pratica la web dir è una dir reale, mentre files è un link
            // quindi per testare l'esistenza dei percorsi devo fare riferimento alla web dir shared e non alla directory fittizia
            // del link.
            // usando real path e aggiungendo il link per poi tornare indietro riesco ad accedere alla vera webdir
            $this->webDir = realpath($this->webDir.'/files/../');

        }

        return $this->normalizeWindowsPath($this->webDir);

    }

    public function normalizeWindowsPath($path)
    {

        return str_replace('\\', '/', $path);
    }

}