<?php
// 14/02/17, 7.52
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppartamentiBundle\Entity\Appartamento;
use AppBundle\Entity\News;
use Doctrine\ORM\EntityManager;
use Liip\ImagineBundle\DependencyInjection\LiipImagineExtension;
use Liip\ImagineBundle\LiipImagineBundle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class OpenGraphManager {

    /**
     * @var EntityManager
     */
    private $em;
    private $Meta;
    private $request;
    /**
     * @var LiipImagineBundle
     */
    private $liipImagineBundle;

    public function __construct(EntityManager $em, RequestStack $requestStack, $liipImagineBundle, $liipImagineCache, $generali, $serviziEsterni) {

        $this->em                = $em;
        $this->liipImagineBundle = $liipImagineBundle;
        $this->liipImagineCache  = $liipImagineCache;


        $this->request = $requestStack->getCurrentRequest();

        $this->Meta = [];

        $this->Meta['social']                    = [];
        $this->Meta['social']['twitter']         = [];
        $this->Meta['social']['twitter']['card'] = "summary";
        if($serviziEsterni['twitter_account']) {
            $this->Meta['social']['twitter']['site'] = $serviziEsterni['twitter_account'];
        }
        $this->Meta['social']['twitter']['description'] = '';
        $this->Meta['social']['og']                     = [];
        $this->Meta['social']['og']['site_name']        = $generali['site_name'];
        $this->Meta['social']['og']['title']            = '';
        $this->Meta['social']['og']['description']      = '';
        $this->Meta['social']['og']['image']            = '';
        $this->Meta['social']['og']['url']              = $this->request->getSchemeAndHttpHost().$this->request->getRequestUri();
        $this->Meta['social']['og']['type']             = 'website';
        $this->Meta['social']['og']['locale']           = $this->request->getLocale().'_'.strtoupper($this->request->getLocale());
        
        $this->liipImagineBundle = $liipImagineBundle;
    }

    public function generateOpenGraphData($Obj) {

        switch(true) {
            case $Obj instanceof News:
            case $Obj instanceof Appartamento:
                if($Obj->getListImgFileName()) {
                    $this->liipImagineBundle->filterAction(
                        $this->request,
                        $Obj->getUploadDir().$Obj->getListImgFileName(),
                        'social'
                    );
                    $this->Meta['social']['twitter']['image'] = $this->liipImagineCache->getBrowserPath($Obj->getUploadDir().$Obj->getListImgFileName(), 'social');
                    $this->Meta['social']['og']['image']      = $this->Meta['social']['twitter']['image'];
                }

                $this->Meta['social']['og']['description']      = $Obj->translate($this->request->getLocale())->getMetaDescription();
                $this->Meta['social']['twitter']['description'] = $Obj->translate($this->request->getLocale())->getMetaDescription();
                $this->Meta['social']['og']['title']            = $Obj->translate($this->request->getLocale())->getMetaTitle();

                break;


        }


        return $this->Meta;


    }


}