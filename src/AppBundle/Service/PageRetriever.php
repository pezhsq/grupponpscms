<?php
// 27/01/17, 14.08
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Page;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class PageRetriever {

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(EntityManager $em, $request) {

        $this->em = $em;
        if($request instanceof RequestStack) {
            /**
             * @var $request RequestStack;
             */
            $this->request = $request->getCurrentRequest();
        } else {
            $this->request = $request;
        }
    }

    /**
     * @param $slugs
     * @param string $locale
     * @return Page
     */
    public function retrievePage($slugs, $locale = 'it') {

        $_slugs = explode('/', $slugs);

        if($locale !== 'it') {
            $slugs = $locale.'/'.$slugs;
        }

        $slugs = '/'.$slugs;

        $slug = array_pop($_slugs);

        // vengono estratte tutte le pagine che hanno questo slug
        // attenzione, potrebbero esserci pagine con stesso slug e parent diverso,
        // quindi il risultato potrebbe essere multiplo, se lo è devo calcolarmi il path per vedere quale
        // è la pagina che davvero sto cercando.
        $Pages = $this->em->getRepository('AppBundle:Page')->getBySlug(['pt:slug' => $slug]);

        if($Pages) {

            foreach($Pages as $Page) {
                $pageUrlGenerator = new PagesUrlGenerator($this->em, $this->requestStack);
                $url              = $pageUrlGenerator->generaUrl($Page, $locale);
                if($url == $slugs) {
                    return $Page;
                }
            }

        }

        return false;
    }


}