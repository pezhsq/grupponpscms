<?php
// 02/01/17, 13.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Service;

use AppartamentiBundle\Entity\Appartamento;
use AppBundle\Entity\Attachment;
use AppBundle\Entity\News;
use AppBundle\Entity\Page;
use AziendaBundle\Entity\Dipendente;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;
use Webtek\EcommerceBundle\Entity\Product;

class AttachmentDirectoryNamer implements DirectoryNamerInterface
{

    public function directoryName($object, PropertyMapping $mapping)
    {

        $dir = '';
        $Parent = $object->getParent();
        switch (true) {

            case $Parent instanceof Page:
                $dir .= 'pages/';
                $dir .= $Parent->getId().'/';
                break;
            case $Parent instanceof News:
                $dir .= 'news/';
                $dir .= $Parent->getId().'/';
                break;
            case $Parent instanceof Appartamento:
                $dir .= 'appartamenti/';
                $dir .= $Parent->getId().'/';
                break;
            case $Parent instanceof Product:
                $dir .= 'prodotti/';
                $dir .= $Parent->getId().'/';
                break;
            case $Parent instanceof Dipendente:
                $dir .= 'dipendenti/';
                $dir .= $Parent->getId().'/';
                break;
            default:
                $dir .= 'temp/'.$object->getToken();
                break;

        }

        return $dir;

    }

}