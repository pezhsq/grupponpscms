<?php
// 24/05/17, 14.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Meta;
use AppBundle\Service\Languages;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\Translator;

class MetaManager
{

    /**
     * @var EntityManager
     */
    private $em;

    private $meta;
    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var Languages
     */
    private $languages;
    /**
     * @var Translator
     */
    private $translator;

    /**
     * MetaManager constructor.
     */
    public function __construct(
        EntityManager $em,
        Languages $languages,
        Slugify $slugify,
        RequestStack $requestStack,
        Translator $translator
    ) {

        $this->em = $em;
        $this->slugify = $slugify;
        $this->languages = $languages;
        $this->request = $requestStack->getCurrentRequest();


        $MetaList = $this->em->getRepository('AppBundle:Meta')->findAll();

        $meta = [];

        foreach ($MetaList as $Meta) {

            /**
             * @var $Meta Meta
             */

            $GroupName = $this->slugify->slugify($Meta->getGroup()->getName());

            if (!isset($meta[$GroupName])) {
                $meta[$GroupName] = [];
            }

            $meta[$GroupName][$Meta->getChiave()] = [];

            foreach ($this->languages->getActivePublicLanguages() as $key => $language) {
                $meta[$GroupName][$Meta->getChiave()][$key] = $Meta->translate($key)->getValore();
            }


        }

        $this->meta = $meta;

        $this->translator = $translator;
    }

    public function merge($twigs, $META)
    {

        foreach ($twigs['sections']['CONTENT']['widgets'] as $container => $widgets) {
            foreach ($widgets as $widget) {
                if (isset($widget['config'][$this->request->getLocale()]) && isset(
                        $widget['config'][$this->request->getLocale()]['data']
                    ) && isset($widget['config'][$this->request->getLocale()]['data']['META'])) {
                    $META = array_replace_recursive(
                        $META,
                        $widget['config'][$this->request->getLocale()]['data']['META']
                    );
                }
            }
        }

        $META = $this->addPage($META);

        return $META;

    }

    public function manageDefaults($field, $section, $META, $vars)
    {

        $META[$field] = $this->meta[$section][$field][$this->request->getLocale()];

        foreach ($vars as $key => $var) {
            $re = '/\[(.*'.preg_quote($key).'[^\]]*)\]/';
            if (preg_match($re, $META[$field], $m)) {
                if ($var) {
                    $META[$field] = str_replace(
                        $m[0],
                        str_replace($key, $var, $m[1]),
                        $META[$field]
                    );
                } else {
                    $META[$field] = str_replace(
                        $m[0],
                        '',
                        $META[$field]
                    );
                }
            } else {
                $META[$field] = str_replace(
                    $key,
                    $var,
                    $META[$field]
                );
            }
        }

        $META = $this->addPage($META);

        return $META;

    }

    public function addPage($META)
    {

        $currentPage = $this->request->query->getInt('page', 1);
        if ($currentPage > 1) {

            if (isset($META['title']) && !preg_match(
                    '/'.' - '.$this->translator->trans('default.labels.pagina').' '.$currentPage.'$/',
                    $META['title']
                )) {
                $META['title'] .= ' - '.$this->translator->trans('default.labels.pagina').' '.$currentPage;
            }
            if (isset($META['description']) && !preg_match(
                    '/'.' - '.$this->translator->trans('default.labels.pagina').' '.$currentPage.'$/',
                    $META['description']
                )) {
                $META['description'] .= ' - '.$this->translator->trans('default.labels.pagina').' '.$currentPage;
            }

        }

        return $META;

    }
}