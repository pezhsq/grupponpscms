<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 16/12/16
 * Time: 15.27
 */

namespace AppBundle\Service;


use AppBundle\Entity\Language;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class Languages
{

    /**
     * @var EntityManager
     */
    private $em;

    private $languages;

    private $publicLanguages;
    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var $currentLanguage Language
     */
    private $currentLanguage;

    public function __construct(EntityManager $em, RequestStack $requestStack)
    {

        $this->em      = $em;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getActiveLanguages()
    {


        if (!$this->languages) {
            $this->languages = $this->em->getRepository('AppBundle:Language')->getBackendLanguages();
        }

        $data = [];

        foreach ($this->languages as $Language) {
            /**
             * @var $Language Language
             */
            $data[$Language->getShortCode()] = $Language->getLanguageName();
        }

        return $data;

    }

    public function getActivePublicLanguages()
    {

        if (!$this->publicLanguages) {
            $this->publicLanguages = $this->em->getRepository('AppBundle:Language')->getPublicLanguages();
        }

        $data = [];

        foreach ($this->publicLanguages as $Language) {
            /**
             * @var $Language Language
             */
            $data[$Language->getShortCode()] = $Language->getLanguageName();
        }

        return $data;

    }

    public function getLanguage()
    {

        if (!$this->currentLanguage) {
            $locale = $this->request->getLocale();

            $this->currentLanguage = $this->em->getRepository('AppBundle:Language')->findOneBy(
                ['shortCode' => $locale]
            );
        }

        return $this->currentLanguage;
    }

    public function getLanguageLocale()
    {

        $this->getLanguage();

        return str_replace('_', '-', $this->currentLanguage->getLocale());

    }

}