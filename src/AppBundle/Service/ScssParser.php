<?php
// 22/02/17, 11.10
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


class ScssParser
{

    private $scssFile = [];

    public function getVariables($cssFile)
    {

        $data = [];

        if (file_exists($cssFile)) {

            $this->scssFile = file($cssFile);

            $selector = [];

            foreach ($this->scssFile as $row) {
                if (strpos($row, '{') !== false) {
                    $selector[] = trim(trim($row), '{');
                } else {
                    if (preg_match('/\s*(.*)\s*\:\s*(\$[a-zA-Z-0-9\-_]+)/', $row, $m)) {

                        if (!isset($data[implode(' ', $selector)])) {
                            $data[implode(' ', $selector)] = [];
                        }
                        $data[implode(' ', $selector)][$m[1]] = $m[2];
                    }
                }
            }

        }

        $return = [];
        $return['colori'] = [];
        $return['textsize'] = [];
        $return['fontweight'] = [];

        $coloriUsati = [];

        foreach ($data as $elem => $variables) {

            foreach ($variables as $property => $variable) {

                $key = false;

                if (strpos($variable, '$textsize') !== false) {
                    $key = 'textsize';
                } elseif (strpos($variable, '$color') !== false) {
                    $key = 'colori';
                } elseif (strpos($variable, '$fontweight') !== false) {
                    $key = 'fontweight';
                }

                if ($key) {
                    if (!isset($return[$key][$variable])) {
                        $return[$key][$variable] = [];
                    }
                    $return[$key][$variable][] = trim($elem);
                }

            }

        }

        foreach ($return as $type => $variabili) {
            foreach ($variabili as $colore => $selettori) {
                $return[$type][$colore] = implode(', ', $selettori);
            }
        }


        return $return;

    }

}