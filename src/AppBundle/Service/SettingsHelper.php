<?php
// 22/02/17, 11.10
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Settings;
use Cocur\Slugify\Slugify;
use Symfony\Component\Yaml\Yaml;

class SettingsHelper {

    /**
     * @var Slugify
     */
    private $slugify;
    private $rootDir;

    public function __construct(Slugify $slugify, $rootDir) {

        $this->slugify = $slugify;
        $this->rootDir = $rootDir;

    }


    public function toYamlFile($arrayOfObjects = []) {

        $data = [];

        foreach($arrayOfObjects as $Setting) {

            /**
             * @var $Setting Settings
             */

            if(!isset($data[$this->slugify->slugify($Setting->getGroup()->getName())])) {
                $data[$this->slugify->slugify($Setting->getGroup()->getName())] = [];
            }

            $data[$this->slugify->slugify($Setting->getGroup()->getName())][$Setting->getChiave()] = $Setting->getValore();

        }

        $file = $this->rootDir.'/config/webtek-new.yml';

        $data = ['parameters' => $data];
        $yaml = Yaml::dump($data);

        file_put_contents($file, $yaml);


    }


}