<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 06/12/16
 * Time: 14.55
 */

namespace AppBundle\Service;

use Cocur\Slugify\Slugify;

class Base64Image {

    private $rootDir;
    /**
     * @var Slugify
     */
    private $slugify;

    public function __construct(WebDir $webDir, Slugify $slugify) {

        $this->rootDir = $webDir->get();
        $this->slugify = $slugify;
    }

    public function getRootDir() {

        return $this->rootDir;
    }

    function saveTo($base64String, $Obj, $filename, $delete = false) {

        $filename = $this->cleanFileName($filename);

        $output_file = $this->rootDir.'/'.$Obj->getUploadDir().$filename;

        if(!is_dir(dirname($output_file))) {
            mkdir(dirname($output_file), 0755, true);
        }

        if($base64String) {

            if(preg_match('/data:image\/(jpeg|png);base64,/', $base64String)) {

                $data = explode(',', $base64String);

                if($this->check_base64_image($data[1])) {

                    $ifp = fopen($output_file, "wb");
                    fwrite($ifp, base64_decode($data[1]));
                    fclose($ifp);

                    return $filename;

                }
            }

        } elseif($delete) {

            @unlink($output_file);

        }

    }

    function delete($Obj, $filename = null, $debug = false) {

        if($filename) {

            $fileToDelete = $this->rootDir.$Obj->getUploadDir().$filename;

            if(file_exists($fileToDelete)) {
                unlink($fileToDelete);
            }
        }

    }


    function check_base64_image($base64) {

        $img = @imagecreatefromstring(base64_decode($base64));
        if(!$img) {
            return false;
        }

        imagepng($img, 'tmp.png');
        $info = getimagesize('tmp.png');

        unlink('tmp.png');

        if($info[0] > 0 && $info[1] > 0 && $info['mime']) {
            return true;
        }

        return false;
    }


    function getUrl($Obj, $filename, $default, $debug = false) {

        $output_file = $this->rootDir.'/'.$Obj->getUploadDir().$filename;

        if($debug) {
            dump($output_file);
        }

        if(file_exists($output_file)) {

            return '/'.$Obj->getUploadDir().$filename;
        }

        return $default;

    }

    function cleanFileName($filename) {

        $pieces = explode('.', $filename);

        $ext = array_pop($pieces);

        $filename = implode('.', $pieces);

        $filename = $this->slugify->slugify($filename);

        return $filename.'.'.$ext;


    }


}