<?php
// 09/01/17, 10.09
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Page;
use AppBundle\Entity\PageTranslation;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;

class PageSlugger {

    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var EntityManager
     */
    private $em;

    private $page;

    public function __construct(Slugify $slugify, EntityManager $em) {

        $this->slugify = $slugify;

        $this->slugify->addRule('à', 'a');
        $this->slugify->addRule('è', 'e');
        $this->slugify->addRule('é', 'e');
        $this->slugify->addRule('ì', 'i');
        $this->slugify->addRule('ò', 'o');
        $this->slugify->addRule('ù', 'u');

        $this->em = $em;
    }

    public function slugify(PageTranslation $pageTranslation, $slug = null) {

        $this->getParentPage($pageTranslation);

        if(!$slug) {
            $slug = $this->slugify->slugify($pageTranslation->getTitolo());
        }

        $newSlug = $slug;

        $i = 1;

        while(!$this->checkSlug($newSlug, $pageTranslation->getLocale())) {

            $newSlug = $slug.'-'.$i;

            $i++;

        }

        return $newSlug;


    }

    private function getParentPage(PageTranslation $pageTranslation) {

        /**
         * @var $page Page
         */
        $page = $pageTranslation->getTranslatable();

        $this->page = $page;

    }

    private function checkSlug($slug, $locale) {

        $data = $this->em->getRepository('AppBundle:Page')->findBySlugPathButNotId($slug, $locale, $this->page->getMaterializedPath(), $this->page->getId());

        return !(bool)count($data);


    }


}