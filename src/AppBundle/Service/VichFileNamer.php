<?php
// 20/06/17, 17.05
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class VichFileNamer implements NamerInterface
{

    private $container;
    private $mapping;
    private $object;

    public function __construct(ContainerInterface $container)
    {

        $this->container = $container;
    }


    public function name($object, PropertyMapping $mapping)
    {

        $this->mapping = $mapping;
        $this->object = $object;

        $file = $mapping->getFile($object);

        $filePath = $mapping->getUploadDestination().'/';

        if ($object) {
            $filePath .= $object->getId().'/';
        }

        $filePath .= $file->getClientOriginalName();

        $newName = $this->generaNuovoNome($filePath);

        return $newName;

    }

    private function checkIfFileExists($filePath)
    {

        return file_exists($filePath);

    }

    private function generaNuovoNome($filePath)
    {

        $file = $this->mapping->getFile($this->object);

        $filePieces = explode('.', $file->getClientOriginalName());
        $estensione = strtolower(array_pop($filePieces));
        $name = implode($filePieces);
        $name = $this->container->get('cocur_slugify')->slugify($name);

        $fileName = $name.'.'.$estensione;

        $filePath = dirname($filePath).'/'.$fileName;

        if ($this->checkIfFileExists($filePath)) {

            $filePieces = explode('.', $fileName);
            $estensione = array_pop($filePieces);
            $name = implode($filePieces);

            $i = 1;

            $dir = dirname($filePath);

            $newName = $dir.'/'.$name.'-'.$i.'.'.$estensione;

            while ($this->checkIfFileExists($newName)) {

                $i++;

                $newName = $dir.'/'.$name.'-'.$i.'.'.$estensione;

            }

            return basename($newName);

        }

        return basename($filePath);

    }

}