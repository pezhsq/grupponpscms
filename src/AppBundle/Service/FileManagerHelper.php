<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 28/05/17
 * Time: 14.02
 */

namespace AppBundle\Service;


use AppBundle\Entity\Template;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Imagine\Filter\Basic\Save;
use Imagine\Gd\Imagine;
use Liip\ImagineBundle\Controller\ImagineController;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Filter\Loader\ThumbnailFilterLoader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class FileManagerHelper
{

    /**
     * @var WebDir
     */
    private $webDir;
    /**
     * @var ImagineController
     */
    private $liipImagineController;
    /**
     * @var CacheManager
     */
    private $liipCacheManager;
    /**
     * @var ImageHelper
     */
    private $imageHelper;
    /**
     * @var Slugify
     */
    private $slugify;

    private $dimensioni;

    private $extensions = [];

    private $tolerance;
    /**
     * @var EntityManager
     */
    private $entityManager;
    private $layout;


    /**
     * FileManagerHelper constructor.
     */
    public function __construct(
        WebDir $webDir,
        ImagineController $liipImagineController,
        CacheManager $liipCacheManager,
        ImageHelper $imageHelper,
        Slugify $slugify,
        EntityManager $entityManager,
        $parametri_generali
    )
    {

        $this->webDir = $webDir;
        $this->liipImagineController = $liipImagineController;
        $this->liipCacheManager = $liipCacheManager;
        $this->imageHelper = $imageHelper;
        $this->slugify = $slugify;
        $this->entityManager = $entityManager;
        $this->layout = $parametri_generali['layout'];
    }

    public function getSubFolders($folder)
    {

        if ($this->isValidPath($folder)) {

            $path = $this->webDir->get() . '/' . $folder;

            if (is_dir($path)) {

                $Finder = new Finder();
                $Finder->depth('== 0');

                $Finder->directories()->in($path);

                $tree = [];

                foreach ($Finder as $dir) {


                    if ($dir->getBasename() != 'cache') {
                        $id = str_replace($this->webDir->get() . '/', '', $this->webDir->normalizeWindowsPath($dir->getRealPath()));
                        $item = [
                            "id" => $id,
                            "text" => $dir->getBasename(),
                            "children" => $this->getSubFolders($id),
                        ];
                        $tree[] = $item;
                    }

                }

                return $tree;

            }

        }

        return false;

    }


    public function loadContent($folder, $filters)
    {

        $this->validateFilters($filters);

        $data = $this->loadDirs($folder);
        $data = array_merge($data, $this->loadFiles($folder));

        return $data;

    }

    public function loadFiles($folder)
    {

        if ($this->isValidPath($folder)) {

            if (!$folder) {
                return [];
            }

            $path = $this->webDir->get() . '/' . $folder;


            if (is_dir($path)) {

                $Finder = new Finder();
                $Finder->sortByName();
                $Finder->depth('== 0');
                $Finder->files()->in($path);

                $files = [];

                foreach ($Finder as $File) {

                    $path = str_replace($this->webDir->get() . '/', '', $this->webDir->normalizeWindowsPath($File->getRealPath()));

                    $lightbox = false;
                    $info = false;
                    $unselectable = false;


                    $file = [
                        'basename' => $File->getBasename(),
                        'basenameTruncated' => substr($File->getBasename(), 0, 10),
                        'extension' => $File->getExtension(),
                        'path' => $path,
                        'type' => 'file',
                    ];

                    if (in_array($file['extension'], ['jpg', 'png', 'jpeg', 'gif'])) {

                        list($w, $h) = getimagesize($File->getRealPath());

                        $response = $this->liipImagineController->filterAction(
                            new Request(),
                            $path,
                            'miniatura'
                        );

                        $file['thumb'] = $this->liipCacheManager->getBrowserPath(
                            $path,
                            'miniatura'
                        );

                        $info = $w . 'x' . $h;

                        $lightbox = true;


                        if ($this->tolerance !== false && $this->dimensioni) {

                            $unselectable = !$this->checkDimensions($w, $h);


                        }

                    } else {
                        $file['thumb'] = '/admin/mime/' . $file['extension'] . '.svg';
                    }

                    $file['lightbox'] = $lightbox;
                    $file['info'] = $info;
                    $file['unselectable'] = $unselectable;

                    if (!$this->extensions || ($this->extensions && in_array($file['extension'], $this->extensions))) {

                        $files[] = $file;

                    }


                }

//                dump($files);
//                die;


                return $files;

            }

        }

        return [];

    }

    public function loadDirs($folder)
    {

        if ($this->isValidPath($folder)) {

            if (!$folder) {

                $files = [];

                foreach (['assets', 'files', 'media'] as $dir) {
                    $file = [
                        'basename' => $dir,
                        'basenameTruncated' => substr($dir, 0, 10),
                        "isImage" => false,
                        'path' => $dir,
                        'lightbox' => false,
                        'type' => 'dir',
                        'thumb' => '/admin/mime/folder.svg',
                    ];

                    $files[] = $file;
                }

                return $files;


            } else {

                $path = $this->webDir->get() . '/' . $this->webDir->normalizeWindowsPath($folder);

                $files = [];

                if (strpos($folder, '/')) {

                    $folder = explode('/', $folder);
                    array_pop($folder);
                    $folder = implode('/', $folder);

                    $file = [
                        'basename' => '..',
                        'basenameTruncated' => '..',
                        "isImage" => false,
                        'path' => $folder,
                        'lightbox' => false,
                        'type' => 'dir-up',
                    ];


                    $file['thumb'] = '/admin/mime/folder-up.svg';

                    $files[] = $file;

                }

                if (is_dir($path)) {

                    $Finder = new Finder();
                    $Finder->sortByName();
                    $Finder->depth('== 0');
                    $Finder->directories()->in($path);

                    foreach ($Finder as $Folder) {

                        if ($Folder->getBasename() != 'cache') {

                            $path = str_replace($this->webDir->get() . '/', '', $this->webDir->normalizeWindowsPath($Folder->getRealPath()));

                            $file = [
                                'basename' => $Folder->getBasename(),
                                'basenameTruncated' => substr($Folder->getBasename(), 0, 10),
                                'path' => $path,
                                "isImage" => false,
                                'type' => 'dir',
                                'lightbox' => false,
                            ];

                            $file['thumb'] = '/admin/mime/folder.svg';


                            $files[] = $file;

                        }

                    }


                    return $files;

                }
            }

        }

        return [];

    }

    public function delete($path)
    {

        if (is_dir($path)) {

            if ($this->isEmpty($path)) {

                $FileSystem = new Filesystem();

                $FileSystem->remove($this->webDir->get() . '/' . $path);

                return true;

            } else {

                return 'filemanager.labels.dir_not_empty';

            }

        } else {

            if ($this->checkIfDeletable($path)) {

                unlink($path);

                return true;

            } else {

                return 'filemanager.labels.file_utilizzato';

            }
        }


    }

    public function creaDir($folder, $dirName)
    {

        if ($this->isValidPath($folder)) {

            $path = $this->webDir->get() . '/' . $folder;

            $dirName = strtolower(preg_replace('/\W/', '', $dirName));

            mkdir($path . '/' . $dirName);

            return true;


        } else {
            return 'filemanager.labels.path_non_valido';
        }


    }

    private function isValidPath($path)
    {

        return strpos($path, '..') === false;

    }

    private function isEmpty($folder)
    {

        if ($this->isValidPath($folder)) {

            $path = $this->webDir->get() . '/' . $folder;

            $Finder = new Finder();
            $Finder->directories()->in($path);

            $contadir = $Finder->count();

            $Finder = new Finder();
            $Finder->files()->in($path);

            $contafiles = $Finder->count();

            return !(bool)($contafiles + $contadir);

        }

        return true;


    }

    /**
     * TODO: Controllare dimensioni rispetto all'originale, e non fare nulla se dimensioni più grandi dell'originale
     * @param $filePath percorso relativo alla root
     * @param $dimensioni dimensioni del nuovo file
     * @param bool $doOptimize flag per la compressione
     * @return bool|string
     */
    public function creaVersione($filePath, $dimensioni, $doOptimize = true)
    {

        $path = $this->webDir->get() . '/' . $filePath;

        if ($this->isValidPath($path) && $this->isAnImage($path) && $this->isValidDimension($dimensioni)) {

            $Imagine = new Imagine();
            $Imagine = $Imagine->open($path);

            $options = [];
            $options['size'] = [];
            list($options['size'][0], $options['size'][1]) = explode('x', $dimensioni);

            $Thumb = new ThumbnailFilterLoader();
            $newVersion = $Thumb->load($Imagine, $options);

            $Saver = new Save($this->generaNome($filePath, $dimensioni));

            $Saver->apply($newVersion);

            if ($doOptimize) {
                $this->imageHelper->compress($filePath);
            }

            return true;

        }

        return 'filemanager.labels.file_non_valido';

    }

    private function isAnImage($filePath)
    {

        if (file_exists($filePath)) {

            $imageExtensions = ['jpg', 'png', 'jpeg', 'gif'];

            $file = basename($filePath);

            $file = explode('.', $file);

            $extension = array_pop($file);

            if (in_array($extension, $imageExtensions) && imagecreatefromstring(file_get_contents($filePath))) {

                return true;

            }

        }

        return false;

    }

    private function isValidDimension($dimensions)
    {

        return preg_match('/^[\d]+x[\d]+$/', $dimensions);
    }

    private function generaNome($filePath, $dimensions)
    {

        $basename = basename($filePath);
        $pieces = explode('.', $basename);
        $estensione = array_pop($pieces);

        return dirname($filePath) . '/' . implode('.', $pieces) . $dimensions . '.' . $estensione;
    }

    public function upload($files, $folder)
    {

        if ($this->isValidPath($folder)) {
            $dest = $this->webDir->get() . '/' . $folder;

            foreach ($files as $file) {
                /**
                 * @var $file UploadedFile
                 */

                if ($this->acceptExtension($file->getClientOriginalExtension())) {
                    $FileName = $this->sanitizeFileName($file->getClientOriginalName());
                    $file->move($dest, $FileName);

                    if (in_array($file->getClientOriginalExtension(), ['png', 'jpg', 'jpeg'])) {
                        $this->imageHelper->compress($dest . '/' . $FileName);
                    }

                }

            }

            return true;
        } else {

            return 'filemanager.labels.path_non_valido';

        }
    }

    private function sanitizeFileName($file)
    {

        $file = explode('.', $file);
        $extension = array_pop($file);

        $file = $this->slugify->slugify(implode('.', $file)) . '.' . $extension;

        return strtolower($file);


    }

    private function acceptExtension($extension)
    {

        return true;
    }

    private function validateFilters($filters)
    {

        if (isset($filters['dimensioni']) && preg_match('/^[\d]+x[\d]+$/', $filters['dimensioni'])) {
            $this->dimensioni = $filters['dimensioni'];
        }
        if (isset($filters['extensions'])) {
            $this->extensions = explode(',', $filters['extensions']);
        }
        if (isset($filters['tolerance']) && is_numeric(
                $filters['tolerance']
            ) && $filters['tolerance'] >= 0 && $filters['tolerance'] <= 100) {
            $this->tolerance = $filters['tolerance'];
        }
    }

    private function checkDimensions($w, $h)
    {

        list($wDim, $hDim) = explode('x', $this->dimensioni);

        $wMin = $wDim - ($wDim * $this->tolerance / 100);
        $wMax = $wDim + ($wDim * $this->tolerance / 100);
        $hMin = $hDim - ($hDim * $this->tolerance / 100);
        $hMax = $hDim + ($hDim * $this->tolerance / 100);

        return ($w >= $wMin && $w <= $wMax && $h >= $hMin && $h <= $hMax);

    }

    /**
     * Controlla nella tabella dei moduli se il file è utilizzato da qualche parte
     *
     * @param $path string Percorso del file da cancellare
     */
    private function checkIfDeletable($path)
    {

        // se sta sotto files o media il file non è cancellabile
        if (strpos($path, 'files') === 0 || strpos($path, 'media') === 0) {
            return false;
        }
        $config = $this->entityManager->getRepository('AppBundle:Template')->findBy(['layout' => $this->layout]);

        $values = [];

        foreach ($config as $templateConfig) {
            /**
             * @var $templateConfig Template
             */
            $savedContent = $templateConfig->getConfig();

            foreach ($savedContent as $shortLang => $savedContentLang) {
                foreach ($savedContentLang as $varName => $data) {
                    if (isset($data['val'])) {
                        $values[$shortLang . '-' . $varName] = $data['val'];
                    }
                }
            }


        }

        if (in_array('/' . $path, $values)) {
            return false;
        }

        return true;


    }


}