<?php
// 07/07/17, 12.24
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Gallery;
use AppBundle\Entity\GalleryItems;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class GalleryHelper
{


    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Languages
     */
    private $languages;


    /**
     * ListinosHelper constructor.
     */
    public function __construct(EntityManager $entityManager, Languages $languages)
    {

        $this->entityManager = $entityManager;
        $this->languages = $languages;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Galleries = $this->entityManager->getRepository('AppBundle:Gallery')->findBy(
                [],
                ['sort' => 'ASC']
            );
        } else {
            $Galleries = $this->entityManager->getRepository('AppBundle:Gallery')->findAllNotDeleted();
        }

        $records = [];

        foreach ($Galleries as $Gallery) {

            /**
             * @var $Gallery Gallery;
             */

            $record = [];
            $record['id'] = $Gallery->getId();
            $record['titolo'] = $Gallery->translate()->getTitolo();
            $record['isEnabled'] = $Gallery->getIsEnabled();
            $record['immagini'] = $Gallery->getItems()->count();
            $record['deleted'] = $Gallery->isDeleted();
            $record['sort'] = $Gallery->getSort();
            $record['createdAt'] = $Gallery->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Gallery->getUpdatedAt()->format('d/m/Y H:i:s');


            $records[] = $record;
        }

        return $records;

    }

    public function upload($files, $galleryId)
    {

        $Gallery = $this->entityManager->getRepository('AppBundle:Gallery')->findOneBy(
            ['id' => $galleryId]
        );

        if (!$Gallery) {
            return false;
        }

        foreach ($files as $file) {

            /**
             * @var $file UploadedFile
             */

            if (in_array($file->guessExtension(), ['jpeg', 'png', 'jpg'])) {

                $GalleryItem = new GalleryItems();
                $this->entityManager->persist($GalleryItem);
                $this->entityManager->flush();
                $GalleryItem->setFile($file);
                foreach ($this->languages->getActiveLanguages() as $shortCode => $language) {
                    $GalleryItem->translate($shortCode)->setAlt('');
                    $GalleryItem->translate($shortCode)->setDescription('');
                }

                $GalleryItem->setGallery($Gallery);
                $GalleryItem->mergeNewTranslations();
                $this->entityManager->persist($GalleryItem);
                $this->entityManager->flush();

            }

        }


        return true;


    }

    public function getImages(Gallery $Gallery)
    {


        $GalleryItems = $Gallery->getItems();

        $items = [];

        foreach ($GalleryItems->getIterator() as $i => $GalleryItem) {
            //do things with $item

            /**
             * @var $GalleryItem GalleryItems
             */
            $item = [];
            $item['id'] = $GalleryItem->getId();
            $item['nome'] = $GalleryItem->getName();
            $item['warning'] = false;
            $item['alt'] = '';
            if (!$GalleryItem->translate()->getalt()) {
                $item['warning'] = true;
            } else {
                $item['alt'] = $GalleryItem->translate()->getAlt();
            }
            $item['src'] = $GalleryItem->getUploadDir().$GalleryItem->getName();

            $items[] = $item;

        }

        return $items;

    }


}