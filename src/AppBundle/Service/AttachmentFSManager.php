<?php
// 03/01/17, 17.22
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Service;

use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\AppartamentoAttachment;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsAttachment;
use AppBundle\Entity\Page;
use AppBundle\Entity\PageAttachment;
use AziendaBundle\Entity\Dipendente;
use AziendaBundle\Entity\DipendenteAttachment;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Webtek\EcommerceBundle\Entity\Product;

class AttachmentFSManager
{

    private $rootDir;
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(WebDir $webDir, EntityManager $em)
    {

        $this->rootDir = $webDir->get().'/files/';
        $this->em = $em;
    }

    public function moveFiles($object)
    {

        $dirFrom = $this->rootDir.'temp/'.$object->getUuid().'/';
        if (is_dir($dirFrom)) {

            switch (true) {
                case $object instanceof Page:
                    $Attachments = $this->em->getRepository('AppBundle:PageAttachment')->getByToken($object->getUuid());
                    foreach ($Attachments as $Attachment) {
                        /**
                         * @var $Attachment PageAttachment
                         */
                        $Attachment->setParentId($object->getId());
                        $Attachment->setToken('');
                        $Attachment->setParent($object);
                        $this->em->persist($Attachment);
                        $this->fsMove(
                            $dirFrom.$Attachment->getName(),
                            $this->rootDir.'pages/'.$object->getId().'/'.$Attachment->getName()
                        );

                    }
                    $this->cleanTempDirs();
                    $this->em->flush();
                    break;
                case $object instanceof News:
                    $Attachments = $this->em->getRepository('AppBundle:NewsAttachment')->getByToken($object->getUuid());
                    foreach ($Attachments as $Attachment) {
                        /**
                         * @var $Attachment NewsAttachment
                         */
                        $Attachment->setParentId($object->getId());
                        $Attachment->setToken('');
                        $Attachment->setParent($object);
                        $this->em->persist($Attachment);
                        $this->fsMove(
                            $dirFrom.$Attachment->getName(),
                            $this->rootDir.'news/'.$object->getId().'/'.$Attachment->getName()
                        );

                    }
                    $this->cleanTempDirs();
                    $this->em->flush();
                    break;
                case $object instanceof Dipendente:
                    $Attachments = $this->em->getRepository('AziendaBundle:DipendenteAttachment')->getByToken(
                        $object->getUuid()
                    );
                    foreach ($Attachments as $Attachment) {
                        /**
                         * @var $Attachment DipendenteAttachment
                         */
                        $Attachment->setParentId($object->getId());
                        $Attachment->setToken('');
                        $Attachment->setParent($object);
                        $this->em->persist($Attachment);
                        $this->fsMove(
                            $dirFrom.$Attachment->getName(),
                            $this->rootDir.'dipendenti/'.$object->getId().'/'.$Attachment->getName()
                        );

                    }
                    $this->cleanTempDirs();
                    $this->em->flush();
                    break;
                case $object instanceof Appartamento:
                    $Attachments = $this->em->getRepository('AppartamentiBundle:AppartamentoAttachment')->getByToken(
                        $object->getUuid()
                    );
                    foreach ($Attachments as $Attachment) {
                        /**
                         * @var $Attachment AppartamentoAttachment
                         */
                        $Attachment->setParentId($object->getId());
                        $Attachment->setToken('');
                        $Attachment->setAppartamento($object);
                        $this->em->persist($Attachment);
                        $this->fsMove(
                            $dirFrom.$Attachment->getName(),
                            $this->rootDir.'appartamenti/'.$object->getId().'/'.$Attachment->getName()
                        );

                    }
                    $this->cleanTempDirs();
                    $this->em->flush();
                    break;
                case $object instanceof Product:
                    $Attachments = $this->em->getRepository('WebtekEcommerceBundle:ProductAttachment')->getByToken(
                        $object->getUuid()
                    );
                    foreach ($Attachments as $Attachment) {
                        /**
                         * @var $Attachment AppartamentoAttachment
                         */
                        $Attachment->setParentId($object->getId());
                        $Attachment->setToken('');
                        $Attachment->setParent($object);
                        $this->em->persist($Attachment);
                        $this->fsMove(
                            $dirFrom.$Attachment->getName(),
                            $this->rootDir.'prodotti/'.$object->getId().'/'.$Attachment->getName()
                        );

                    }
                    $this->cleanTempDirs();
                    $this->em->flush();
                    break;

            }


        }

    }

    private function fsMove($from, $to)
    {

        if (!is_dir(dirname($to))) {
            mkdir(dirname($to), 0755, true);
        }
        rename($from, $to);

    }


    private function cleanTempDirs()
    {

        $tempDir = $this->rootDir.'temp';
        $finder = new Finder();
        $fs = new Filesystem();
        $directories = $finder->directories()->date('until 2 hours ago')->in($tempDir);
        $directoryDaEliminare = [];
        foreach ($directories as $directory) {

            $directoryDaEliminare[] = $directory->getRealPath();

        }
        foreach ($directoryDaEliminare as $directory) {
            $fs->remove($directory);
        }


    }


}