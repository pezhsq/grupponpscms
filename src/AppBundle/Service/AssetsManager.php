<?php

/*
 * This file is part of PHP CS Fixer.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *     Dariusz Rumiński <dariusz.ruminski@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;

use AppBundle\Entity\Template;
use Doctrine\ORM\EntityManager;
use MatthiasMullie\Minify;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class AssetsManager
{
    /**
     * @var EntityManager
     */
    private $em;
    private $rootDir;
    private $layout;
    private $dirLayout;
    private $dirStyles;
    private $dirModuli;
    private $sass = false;
    private $webDir = false;
    private $minimizza = false;

    public function __construct(EntityManager $em, $rootDir, WebDir $webDir, $generali, $parametri_avanzati)
    {
        $this->em = $em;
        $this->rootDir = $rootDir;
        $this->layout = $generali['layout'];
        $this->webDir = $webDir->getWebPart();
        $this->dirLayout = $this->rootDir.'/'.'Resources'.'/'.'views'.'/'.'public'.'/'.'layouts'.'/'.$this->layout.'/';
        $this->dirStyles = $this->dirLayout.'styles'.'/';
        $this->dirModuli = $this->rootDir.'/'.'Resources'.'/'.'views'.'/'.'modules'.'/';
        $this->sass = $parametri_avanzati['sass'];
        $this->minimizza = $parametri_avanzati['script_minimizzati'];
        $this->enableSass = $parametri_avanzati['enable_sass'];
    }

    public function compile()
    {
        if ($this->enableSass) {
            $this->generateModules();
            $scssFile = $this->rootDir.'/Resources/views/public/layouts/'.$this->layout.'/styles/style.scss';
            $compressed = '';
            if ($this->minimizza) {
                $compressed = ' -t compressed';
            }
            $destinazione = $this->rootDir.'/../'.$this->getWebDir().'/public/css/'.$this->layout;
            if (!is_dir($destinazione)) {
                mkdir($destinazione, 0755, true);
            }
            $destinazione = realpath($destinazione);
            $command = $this->sass.' '.$compressed.' "'.$scssFile.'" "'.$destinazione.'/'.'style.';
            if ($compressed) {
                $command .= 'min.';
            }
            $command .= 'css"';
            $process = new Process($command);
            $process->run();
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            return $destinazione;
        }

        return 'Non compilato, sass disabilitato';
    }

    public function searchKey($key)
    {
        $finder = new Finder();
        $finder->name('*.scss');
        $scss = $finder->files()->in($this->dirModuli.'*/*/')->in($this->dirStyles)->contains($key)->notName(
            '_variables.scss'
        );
        $files = [];
        foreach ($scss as $file) {
            $files[] = trim($file->getBasename('.'.$file->getExtension()), '_');
        }

        return $files;
    }

    public function getModules()
    {
        $TemplateList = $this->em->getRepository('AppBundle:Template')->findBy(['layout' => $this->layout]);
        $scssList = [];
        $jsList = [];
        foreach ($TemplateList as $Template) {
            /**
             * @var Template
             */
            $scss = $this->compilaCssModulo($Template);
            if ($scss) {
                $scssList[] = $scss;
            }
            $js = $this->dirModuli.$Template->getCategory().'/'.$Template->getModule().'/'.$Template->getCodice(
                ).'/'.$Template->getModule().'.js';
            if (file_exists($js)) {
                $jsList[$Template->getModule().'_'.$Template->getInstance()] = $js;
            }
        }

        return ['scss' => $scssList, 'js' => $jsList];
    }

    private function getWebDir()
    {
        if (!$this->webDir) {
            $composer = json_decode(file_get_contents($this->rootDir.'/'.'..'.'/'.'composer.json'), true);
            $this->webDir = $composer['extra']['symfony-web-dir'];
        }

        return $this->webDir;
    }

    private function generateScssModulesList($moduleList = [])
    {
        file_put_contents($this->dirStyles.'_modules.scss', implode("\n", $moduleList['scss']));
    }

    /**
     * Dato un file e un'istanza, genera uno scope separato dal resto del codice per l'esecuzione.
     *
     * @param $instance nome dell'istanza
     * @param $jsFileName Nome del file javascript da includere
     *
     * @return bool true
     */
    private function compilaJavascript($instance, $jsFileName)
    {
        $contenuto = file_get_contents($jsFileName);
        $contenuto = trim($contenuto);
        $contenuto = trim($contenuto, ';');
        $out = '';
        $out .= '(function(instance) {';
        $out .= $contenuto;
        $out .= "\n";
        $out .= '})("'.$instance.'");';
        $out .= "\n";

        return $out;
    }

    private function generateJsModulesList($moduleList = [])
    {
        $stream = [];
        if ($moduleList['js']) {
            foreach ($moduleList['js'] as $instance => $js) {
                $stream[] = '// '.basename($js).' '.$instance.' - INIZIO';
                $stream[] = $this->compilaJavascript($instance, $js);
                $stream[] = '';
                $stream[] = '// '.basename($js).' - FINE';
                $stream[] = '';
                $stream[] = '';
            }
        }
        $stream[] = '// mousetrap.js - INIZIO';
        $stream[] = trim(
            file_get_contents($this->rootDir.'/'.'..'.'/'.$this->getWebDir().'/node_modules/mousetrap/mousetrap.min.js')
        );
        $stream[] = '// mousetrap.js - FINE';
        $stream[] = '// utils.js - INIZIO';
        $stream[] = trim(
            file_get_contents($this->rootDir.'/'.'..'.'/'.$this->getWebDir().'/bundles/app/public/js/utils.js')
        );
        $stream[] = '';
        $stream[] = '// utils.js - FINE';
        $stream[] = '';
        $stream[] = '// webtek-admin.js - INIZIO';
        $stream[] = trim(
            file_get_contents($this->rootDir.'/'.'..'.'/'.$this->getWebDir().'/bundles/app/public/js/webtek-admin.js')
        );
        $stream[] = '';
        $stream[] = '// webtek-admin.js - FINE';
        $stream[] = '';
        $stream[] = '';
        $destinazione = $this->rootDir.'/../'.$this->getWebDir().'/public/js/'.$this->layout;
        if (!is_dir($destinazione)) {
            mkdir($destinazione, 0755, true);
        }
        file_put_contents($destinazione.'/'.'modules.js', implode("\n", $stream));
        $minifier = new Minify\JS($destinazione.'/'.'modules.js');
        $minifiedPath = $destinazione.'/'.'modules.min.js';
        $minifier->minify($minifiedPath);
    }

    private function generateModules()
    {
        $moduleList = $this->getModules();
        $this->generateScssModulesList($moduleList);
        $this->generateJsModulesList($moduleList);
    }

    private function compilaCssModulo(Template $Template)
    {
        $scss = $this->dirModuli.$Template->getCategory().'/'.$Template->getModule().'/'.$Template->getCodice(
            ).'/'.$Template->getModule().'.scss';
        if (file_exists($scss)) {
            $config = (object) $Template->getConfig();
            $scssContent = file_get_contents($scss);
            if (isset($config->scssVars)) {
                $scssContent = strtr($scssContent, $config->scssVars);
            }
            $tempFileContent = '';
            $tempFileContent .= '#'.$Template->getModule().'_'.$Template->getInstance().' {';
            $tempFileContent .= "\n";
            $tempFileContent .= $scssContent;
            $tempFileContent .= '}';
            $tempFileContent .= "\n";

            return $tempFileContent;
        }

        return '';
    }
}
