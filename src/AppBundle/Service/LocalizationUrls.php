<?php
// 08/02/17, 9.33
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Routing\Router;


class LocalizationUrls {

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var \Twig_Environment
     */
    private $twig_Environment;
    /**
     * @var Languages
     */
    private $languages;
    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, \Twig_Environment $twig_Environment, Languages $languages, Router $router) {

        $this->em               = $em;
        $this->twig_Environment = $twig_Environment;
        $this->languages        = $languages;
        $this->router           = $router;
    }

    public function setLocalizedUrls() {

        $localizedUrls = [];

        foreach($this->languages->getActivePublicLanguages() as $key => $lang) {

            if($key == 'it') {
                $localizedUrls[$key] = $this->router->generate('home_it');
            } else {
                $localizedUrls[$key] = $this->router->generate('home', ['_locale' => $key]);
            }

        }

        $this->twig_Environment->addGlobal('localizedUrls', $localizedUrls);

    }


}