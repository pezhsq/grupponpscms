<?php
// 20/06/17, 16.59
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\News;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NewsHelper
{

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var WebDir
     */
    private $webDir;

    public function __construct(ContainerInterface $container)
    {

        $this->em = $container->get('doctrine.orm.default_entity_manager');
        $this->webDir = $container->get('app.web_dir');
        $this->container = $container;
    }

    public function correggiDimensioni(News $News, $field, $dimensioni)
    {

        $method = 'get'.$field.'Filename';

        $percorso = $this->webDir->get().'/'.$News->getUploadDir().$News->$method();

        if (file_exists($percorso)) {

            $imageHelper = $this->container->get('app.image_helper');

            $resizeData = json_decode($dimensioni, true);

            $imageHelper->resize('/'.$News->getUploadDir().$News->$method(), $resizeData);

        }

    }
}