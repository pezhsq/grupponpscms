<?php
// 06/02/17, 11.51
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\News;
use AppBundle\Entity\NewsRows;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BlogFrontend
{
	
	/**
	 * @var Translator
	 */
	private $translator;
	/**
	 * @var EngineInterface
	 */
	private $twig;
	private $layout;
	/**
	 * @var EntityManager
	 */
	private $em;
	
	
	/**
	 * BlogFrontend constructor.
	 */
	public function __construct(ContainerInterface $container)
	{
		
		$this->translator = $container->get('translator');
		$this->twig = $container->get('twig');
		$this->layout = $container->getParameter('generali')['layout'];
		$this->em = $container->get('doctrine')->getManager();
		
	}
	
	public function getTextPreview(News $News, $length = 50, $locale = 'it')
	{
		
		/**
		 * @var $newsRow NewsRows
		 */
		foreach ($News->getNewsRows() as $newsRow) {
			
			if ($newsRow->getLocale() == $locale) {
				$struttura = $newsRow->getStructure();
				
				$struttura = json_decode($struttura, true);
				
				foreach ($struttura as $slot => $data) {
					if ($data['type'] == 'ck') {
						$method = 'getSlot'.($slot + 1);
						
						$text = strip_tags($newsRow->$method());
						
						if (strlen($text) > $length) {
							$text = substr(html_entity_decode($text, ENT_QUOTES, 'UTF-8'), 0, $length).'...';
						}
						
						return $text;
					}
				}
			}
		}
		
		return 'Sunt capioes visum grandis, germanus historiaes.';
		
	}
	
	/**
	 * @param News $News
	 * @param int $length
	 * @param string $locale
	 * @return string
	 */
	public function getTextPreviewRaw(News $News, $length = 50, $locale = 'it')
	{
		
		/**
		 * @var $newsRow NewsRows
		 */
		foreach ($News->getNewsRows() as $newsRow) {
			
			if ($newsRow->getLocale() == $locale) {
				$struttura = $newsRow->getStructure();
				
				$struttura = json_decode($struttura, true);
				
				foreach ($struttura as $slot => $data) {
					if ($data['type'] == 'ck') {
						$method = 'getSlot'.($slot + 1);
						
						$text = $newsRow->$method();
						
						if (strlen($text) > $length) {
							$text = substr(html_entity_decode($text, ENT_QUOTES, 'UTF-8'), 0, $length).'...';
						}
						
						return $text;
					}
				}
			}
		}
		
		return 'Sunt capioes visum grandis, germanus historiaes.';
		
	}
	
	
	public function getListImg(News $News, $attribute = 'src', $locale = 'it')
	{
		
		$img = [];
		$img['src'] = '/images/loghi/placeholder.png';
		$img['alt'] = $News->translate($locale)->getTitolo();
		
		if ($News->getListImgFileName()) {
			$img['src'] = '/'.$News->getUploadDir().'/'.$News->getListImgFileName();
		}
		
		return $img[$attribute];
		
	}
	
	public function getCategory(News $News, $locale = 'it')
	{
		
		return $News->getPrimaryCategory()->translate($locale);
		
	}
	
	/**
	 * @param News $News
	 * @param string $locale
	 * @return array|mixed attachments
	 */
	public function getAttachments(News $News, $locale = 'it')
	{
		return $News->getAttachments();
	}
	
	/**
	 * @param News $News
	 * @param string $categoryToCheck
	 * @param string $locale
	 * @return bool as true when News belongs to a specific category
	 */
	public function newsInCategory(News $News, $categoryToCheck = 'default', $locale = 'it')
	{
		if ($News) {
			$categories = $this->getAllCategories($News, $locale);
			foreach ($categories as $category) {
				if ($category->getSlug() == $categoryToCheck) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * @param News $News
	 * @param string $locale
	 * @return array with primary category and related
	 */
	public function getAllCategories(News $News, $locale = 'it')
	{
		$categories = [];
		
		if ($News) {
			array_push($categories, $News->getPrimaryCategory()->translate($locale));
			foreach ($News->getCategorieAggiuntive() as $singleCategory) {
				array_push($categories, $singleCategory->translate($locale));
			}
		}
		
		return $categories;
	}
	
	public function getSlug(News $News, $locale = 'it')
	{
	
	}
	
	public function getDate($Entity, $locale = 'it', $field = 'publishAt')
	{
		
		$method = 'get'.ucfirst($field);
		
		$date = $Entity->$method();
		
		$dataFormattata = $this->translator->trans('cal.labels.giorno_'.$date->format('N'));
		$dataFormattata .= ' ';
		$dataFormattata .= $date->format('d');
		$dataFormattata .= ' ';
		$dataFormattata .= $this->translator->trans('cal.labels.mese_'.$date->format('n'));
		$dataFormattata .= ' ';
		$dataFormattata .= $date->format('Y');
		
		return $dataFormattata;
		
	}
	
	public function getDateFormat($Entity, $format = 'd-m-Y', $field = 'publishAt')
	{
		
		$method = 'get'.ucfirst($field);
		
		$date = $Entity->$method();
		
		if ($format[0] == '%') {
			switch ($format[1]) {
				case 'b':
					$dataFormattata = $this->translator->trans('cal.labels.mese_short_'.$date->format('n'));
					break;
				case 'B':
					$dataFormattata = $this->translator->trans('cal.labels.mese_'.$date->format('n'));
					break;
				default:
					$dataFormattata = $format;
			}
		} else {
			$dataFormattata = $date->format($format);
		}
		
		return $dataFormattata;
		
	}
	
	public function getNewsText($rows, $locale = 'it')
	{
		
		$out = '';
		
		foreach ($rows as $row) {
			
			/**
			 * @var $row NewsRows
			 */
			if ($row->getLocale() == $locale) {
				
				$out .= $this->getRow($row);
				
				
			}
			
		}
		
		return $out;
		
	}
	
	public function getRow($row)
	{
		
		return $this->twig->render('public/layouts/'.$this->layout.'/includes/news-row.twig', ['row' => $row]);
		
	}
	
	public function getCorrelate(News $news, $locale = 'it', $quante = 2)
	{
		
		$category = $news->getPrimaryCategory();
		
		$NewsList = $this->em->getRepository('AppBundle:News')->getCorrelate($news, $category, $locale, $quante);
		
		return $NewsList;
	}
	
	public function getText(NewsRows $row, $slotNumber = 1)
	{
		
		$method = 'getSlot'.$slotNumber;
		
		$re = '/\[youtube(.*)\]/';
		
		$text = $row->$method();
		
		preg_match_all($re, $text, $m);
		
		if ($m) {
			
			foreach ($m[1] as $k => $html) {
				
				$rawHtml = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
				
				$reUrl = '/url="([^"]+)"/';
				
				preg_match($reUrl, $rawHtml, $mUrl);
				
				$data = [];
				$data['code'] = false;
				$data['caption'] = false;
				
				if ($mUrl) {
					
					$youtubeData['code'] = trim(parse_url($mUrl[1], PHP_URL_PATH), '/');
					
					$reCaption = '/caption="([^"]+)"/';
					
					preg_match($reCaption, $rawHtml, $mCaption);
					
					if ($mCaption) {
						$youtubeData['caption'] = $mCaption[1];
					}
					
					$text = str_replace(
						$m[0][$k],
						$this->twig->render(
							'public/layouts/'.$this->layout.'/includes/youtube.twig',
							['youtubeData' => $youtubeData]
						),
						$text
					);
					
				}
			}
			
		}
		
		return $text;
		
	}
	
	public function getCountCommenti(News $News)
	{
		
		return $this->em->getRepository('AppBundle:Commento')->getCommentiCountForNews($News);
		
		
	}
	
	
}