<?php
// 28/09/17, 15.26
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Service;

use AppBundle\Entity\Log;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;

class LogsHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var TokenStorage
     */
    private $tokenStorage;
    /**
     * @var Logger
     */
    private $fileLogger;
    private $rootDir;

    public function __construct(TokenStorage $tokenStorage, Logger $fileLogger, $rootDir)
    {

        $this->fileLogger = $fileLogger;
        $this->tokenStorage = $tokenStorage;
        $this->rootDir = $rootDir;
    }

    public function getList()
    {

        $logFile = realpath($this->rootDir."/../var/logs/actions.log");
        $records = [];
        if ($logFile) {

            $rows = file($logFile);
            foreach ($rows as $row) {

                $re = '/^\[([^\]]+)\][^\:]+\:\s*([^\{]+)\s*({.*})\s*\[\]$/';
                if (preg_match($re, $row, $matches)) {
                    $d = new \DateTime($matches[1]);
                    $data = json_decode($matches[3], true);
                    $record = [];
                    $record['id'] = 0;
                    $record['entityId'] = 'id:'.$data['id'];
                    $record['entity'] = $data['entity'];
                    $record['action'] = $data['action'];
                    $record['author'] = $data['autor'];
                    $record['message'] = str_replace('||', '<br />', $matches[2]);
                    $record['createdAt'] = $d->format('d/m/Y H:i:s');
                    $records[] = $record;

                }
            }
        }

        return $records;

    }

    public function log($class, $id, $message, $action)
    {

        $Token = $this->tokenStorage->getToken();
        if ($Token) {
            $User = $Token->getUser();
            if ($User instanceof User) {
                $UserString = $User->getNome().' '.$User->getCognome();
            } else {
                $UserString = 'Anonimo';
            }
        } else {
            $UserString = 'Anonimo';
        }
        $this->fileLogger->info(
            $message,
            ['autor' => $UserString, 'entity' => $class, 'id' => $id, 'action' => $action]
        );

    }

}