<?php
// 02/01/17, 16.48
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Service;

use AppBundle\Entity\News;
use AppBundle\Entity\Page;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;
use Webtek\EcommerceBundle\Entity\Product;
use Webtek\EcommerceBundle\Service\ProductFileNamer;

class AttachmentFileNamer implements NamerInterface
{

    private $mapping = false;
    private $object = false;
    private $container;

    public function __construct(ContainerInterface $container)
    {

        $this->container = $container;
    }


    public function name($object, PropertyMapping $mapping)
    {

        $this->mapping = $mapping;
        $this->object = $object;
        $Parent = $object->getParent();
        $filePath = $mapping->getUploadDestination().'/';
        $file = $mapping->getFile($object);
        switch (true) {

            case $Parent instanceof Page:
                $filePath .= 'pages/';
                $filePath .= $Parent->getId().'/';
                $filePath .= $file->getClientOriginalName();
                break;
            case $Parent instanceof News:
                $filePath .= 'news/';
                $filePath .= $Parent->getId().'/';
                $filePath .= $file->getClientOriginalName();
                break;
            case $Parent instanceof Product:
                $NamerProdotti = new ProductFileNamer($this->container);

                return $NamerProdotti->generaNome(
                    $Parent,
                    $mapping->getUploadDestination(),
                    $mapping->getFile($object)->getClientOriginalExtension(),
                    $mapping->getFile($object)->getClientOriginalName()
                );
                break;
            default:
                $filePath .= 'temp/'.$object->getToken().'/'.$file->getClientOriginalName();
                break;

        }
        $newName = $this->generaNuovoNome($filePath);

        return $newName;

    }

    private function checkIfFileExists($filePath)
    {

        return file_exists($filePath);

    }

    private function generaNuovoNome($filePath)
    {

        if ($this->checkIfFileExists($filePath)) {

            $file = $this->mapping->getFile($this->object);
            $filePieces = explode('.', $file->getClientOriginalName());
            $estensione = array_pop($filePieces);
            $name = implode('.', $filePieces);
            $i = 1;
            $dir = dirname($filePath);
            $newName = $dir.'/'.$name.'-'.$i.'.'.$estensione;
            while ($this->checkIfFileExists($newName)) {

                $i++;
                $newName = $dir.'/'.$name.'-'.$i.'.'.$estensione;

            }

            return basename($newName);

        }

        return basename($filePath);

    }


}