<?php
// 30/01/17, 15.36
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use Symfony\Component\Finder\Finder;

class ModuleLoader
{

    private $rootDir;
    private $layout;

    public function __construct($rootDir, $generali)
    {

        $this->rootDir = $rootDir;

        $this->moduleDir = $this->rootDir.
            '/'.'Resources'.
            '/'.'views'.
            '/'.'modules';

        $this->layout = $generali['layout'];
    }

    /**
     * Tramite espressione regolare ritorna le chiamate all'estensione Twig "loadWidgets" per estrarre i possibili
     * contenitori di widget
     * @param $type tipo di template (HEADER|FOOTER|CONTENT)
     * @param $file nome del template (e di conseguenza) del file utilizzato
     *
     * @return array
     */
    public function loadSlots($type, $file)
    {

        $fileName = $this->rootDir.
            '/Resources/views/public/layouts/'.$this->layout.
            '/'.$type.
            '/'.$file.'.html.twig';

        if (file_exists($fileName)) {

            $markup = file_get_contents($fileName);


            $re = "/{{\s*loadWidgets\(['|\"][^'\"]+['|\"]\s*,\s*['|\"]([^'\"]+)['|\"]\)\s*}}/";

            if (preg_match_all($re, $markup, $m)) {

                return $m[1];
            }
        }

        return [];


    }

    public function loadModules()
    {

        $finder = new Finder();

        $dirCategorie = $finder->directories()->depth(0)->in($this->moduleDir);

        foreach ($dirCategorie as $dirCategoria) {

            $categoria = ucfirst(strtolower(basename($dirCategoria->getPathname())));

            $moduli[$categoria] = [];

            $finder = new Finder();

            $dirs = $finder->directories()->depth(0)->in($dirCategoria->getPathname());

            foreach ($dirs as $dir) {

                $moduli[$categoria] = array_merge($moduli[$categoria], $this->loadVersions($dir->getPathname()));

            }

        }

        ksort($moduli);

        return $moduli;


    }

    private function loadVersions($dir)
    {

        $finder = new Finder();

        $dirs = $finder->directories()->in($dir);

        $versioni = [];

        foreach ($dirs as $dir) {

            $versioni[$dir->getBasename()] = $this->loadDescription($dir->getPathname());

        }

        return $versioni;

    }

    public function loadDescription($dir)
    {

        $jsonFile = $dir.'/'.'index.json';

        if (!file_exists($jsonFile)) {
            throw new \Exception('File index.json mancante in '.$dir);
        }

        $configFile = realpath($dir.'/../config.json');
        $configData = false;
        if ($configFile) {
            $configData = json_decode(file_get_contents($configFile), true);
        }

        $jsonData = json_decode(file_get_contents($dir.'/'.'index.json'), true);


        if (!$jsonData) {
            throw new \Exception(
                'File '.$dir.'/'.'index.json malformato, controlla il contenuto con http://jsonlint.com/'
            );
        }

        $data = [
            'nome' => 'NOME DEFAULT (compilare l\'apposito campo!!)',
            'descrizione' => 'DESCRIZIONE DEFAULT (compilare l\'apposito campo!!)',
            'release' => 'XXX',
            'dirName' => basename(dirname($dir)),
            'codice' => 'YYY',
            'releaseDate' => '10-10-2010',
            'preview' => '/img/module-default.png',
        ];


        if ($configData && isset($configData['nome']) && $configData['nome']) {
            $data['nome'] = $configData['nome'];
        } else {
            $data['nome'] = basename(realpath($dir.'/../'));
        }
        if (isset($jsonData['descrizione'])) {
            $data['descrizione'] = $jsonData['descrizione'];
        }
        if (isset($jsonData['release'])) {
            $data['release'] = $jsonData['release'];
        }
        if (isset($jsonData['codice'])) {
            $data['codice'] = $jsonData['codice'];
        }
        if (isset($jsonData['releaseDate'])) {
            $releaseDate = new \DateTime($jsonData['releaseDate']);
            $data['releaseDate'] = $releaseDate->format('d/m/Y');
        }

        if (file_exists($dir.'/'.'preview.png')) {
            $data['preview'] = str_replace($this->rootDir.'/Resources/views', '', $dir.'/'.'preview.png');
        }

        return $data;


    }

}