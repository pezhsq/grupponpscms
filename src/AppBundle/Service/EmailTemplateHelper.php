<?php
// 12/06/17, 16.11
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Service;

use AppBundle\Entity\EmailTemplate;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class EmailTemplateHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    private $parametriEmail;
    private $twig;
    private $mailer;

    /**
     * @var Container
     */
    private $container;

    /**
     * EmailTemplateHelper constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(Container $container)
    {

        $this->entityManager = $container->get('doctrine.orm.default_entity_manager');
        $this->parametriEmail = $container->getParameter('emails');
        $this->container = $container;

    }

    /**
     * Ritorna l'elenco degli elementi in db in un array formattato per datatables
     * @return array
     */
    public function getList($deleted)
    {

        if ($deleted) {
            $EmailTemplates = $this->entityManager->getRepository('AppBundle:EmailTemplate')->findAll();
        } else {
            $EmailTemplates = $this->entityManager->getRepository('AppBundle:EmailTemplate')->findAllNotDeleted();
        }
        $records = [];
        foreach ($EmailTemplates as $emailTemplate) {

            /**
             * @var $emailTemplate EmailTemplate;
             */
            $record = [];
            $record['id'] = $emailTemplate->getId();
            $record['codice'] = $emailTemplate->getCodice();
            $record['deleted'] = $emailTemplate->isDeleted();
            $record['subject'] = $emailTemplate->translate()->getSubject();
            $record['createdAt'] = $emailTemplate->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $emailTemplate->getUpdatedAt()->format('d/m/Y H:i:s');
            $records[] = $record;
        }

        return $records;

    }

    function send(
        $codice,
        $to = [],
        $replace,
        $locale,
        $bcc = [],
        $replyTo = [],
        $mailer = 'simple',
        $parametri = false
    ) {

        $emailTemplate = $this->entityManager->getRepository('AppBundle:EmailTemplate')->findOneBy(
            ['codice' => $codice]
        );
        if ($emailTemplate) {

            $subject = $emailTemplate->translate($locale)->getSubject();
            $testo = $emailTemplate->translate($locale)->getText();
            if (isset($parametri['from'])) {
                $fromEmail = $parametri['from'];
            } else {
                $fromEmail = $this->parametriEmail['default_from'];
            }
            if (isset($parametri['from_name'])) {
                $fromDescription = $parametri['from_name'];
            } else {
                $fromDescription = $this->parametriEmail['default_from_name'];
            }
            $template = $this->container->get("twig")->createTemplate($subject);
            $subject = $template->render($replace);
            $templateData = $this->normalizeParameters($replace);
            $message = \Swift_Message::newInstance()->setSubject($subject)->setFrom(
                $fromEmail,
                $fromDescription
            )->setTo($to);
            // Creazione del LOGO
            $templateData['_LOGO'] = $message->embed(
                \Swift_Image::fromPath($this->container->get('app.web_dir')->get().$templateData['_LOGO'])
            );
            if ($bcc) {
                $message->setBcc($bcc);
            }
            if ($replyTo) {
                $message->setReplyTo($replyTo);
            }
            $template = $this->container->get("twig")->createTemplate($testo);
            $templateData['_TESTO'] = $template->render($templateData);
            $html = $this->container->get("templating")->render(
                "public/layouts/".$this->container->getParameter("generali")["layout"]."/includes/email-body.twig",
                $templateData
            );
            $message->setBody($html, 'text/html');

            return $this->container->get('swiftmailer.mailer.'.$mailer)->send($message);

        }

        return false;

    }

    private function normalizeParameters($replace)
    {

        $additionalKeys = [];
        foreach ($replace as $k => $value) {
            $additionalKeys[strtoupper($k)] = $value;
        }
        $data = [
            '_LOGO' => $this->parametriEmail['logo'],
            '_NOME' => $this->parametriEmail['nome'],
            '_LINK' => $this->container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost(),
        ];
        $data = array_merge($data, $replace);

        return $data;

    }

}