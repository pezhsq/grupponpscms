<?php
// 12/06/17, 10.17
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Service;


use AppBundle\Entity\Language;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class LocalizationHelper
{

    private $locale;

    /**
     * @var $Language Language
     */
    private $Language;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * LocalizationHelper constructor
     * @param RequestStack $requestStack
     * @param EntityManager $entityManager
     */
    public function __construct(RequestStack $requestStack, EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;

        $this->locale = $requestStack->getCurrentRequest()->getLocale() ? $requestStack->getCurrentRequest()->getLocale(
        ) : 'it';

        $this->getLanguage();

    }

    /**
     * Estrae l'oggetto lingua corrispondente al locale recuperato dalla request e lo posizione in una proprietà
     * privata
     */
    private function getLanguage()
    {

        if (!$this->Language) {
            $this->Language = $this->entityManager->getRepository('AppBundle:Language')->findOneBy(
                ['shortCode' => $this->locale]
            );
        }

    }

    /**
     * Ritorna la sigla breve della lingua in navigazione
     * @return string $locale
     */
    public function getLocale()
    {

        return $this->locale;
    }

    /**
     * Ritorna un numero formattato con i caratteri di separazione appropriati in base alla lingua in navigazione.
     *
     * @param float $val Valore numerico da formattare in valuta
     * @param string $currency Valore della moneta da usare, default EUR
     * @return string Numero formattato in valuta secondo la lingua in navigazione
     */

    public function formatCurrency($val = 0, $currency = 'EUR')
    {

        $formatter = new \NumberFormatter($this->Language->getLocale(), \NumberFormatter::CURRENCY);

        return $formatter->formatCurrency($val, $currency);

    }
}