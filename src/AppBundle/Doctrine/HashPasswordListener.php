<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 02/12/16
 * Time: 10.35
 */

namespace AppBundle\Doctrine;


use AppBundle\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class HashPasswordListener implements EventSubscriber {

    private $passwordEncoder;


    /**
     * HashPasswordListener constructor.
     */
    public function __construct(UserPasswordEncoder $passwordEncoder) {

        $this->passwordEncoder = $passwordEncoder;


    }

    public function prePersist(LifecycleEventArgs $args) {

        $entity = $args->getEntity();

        if(!$entity instanceof User) {
            return null;
        }

        $this->encodePassword($entity);


    }

    public function preUpdate(PreUpdateEventArgs $args) {


        $entity = $args->getObject();

        if(!$entity instanceof User) {
            return null;
        }

        $this->encodePassword($entity);


        $em   = $args->getEntityManager();
        $meta = $em->getClassMetadata(get_class($entity));
        $em->getUnitOfWork()->recomputeSingleEntityChangeSet($meta, $entity);

    }


    public function getSubscribedEvents() {

        return ['prePersist', 'preUpdate'];
    }

    /**
     * @param User $entity
     */
    private function encodePassword(User $entity) {

        if($entity->getPlainPassword()) {
            $encoded = $this->passwordEncoder->encodePassword($entity, $entity->getPlainPassword());

            $entity->setPassword($encoded);
        }
    }


}