<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 23/12/16
 * Time: 13.39
 */

namespace AppBundle\EntityListener;


use AppBundle\Entity\Page;
use AppBundle\Service\AttachmentFSManager;
use AppBundle\Service\Base64Image;
use AppBundle\Service\PageHelper;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class PageListener
{


    /**
     * @var Slugify
     */
    private $slugify;
    /**
     * @var Base64Image
     */
    private $base64Image;
    /**
     * @var AttachmentFSManager
     */
    private $attachmentFSManager;
    /**
     * @var PageHelper
     */
    private $pageHelper;
    private $datiPagine;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        ContainerInterface $container
    ) {

        $this->container = $container;
        $this->slugify = $container->get('slugify');
        $this->base64Image = $container->get('app.base64_image');
        $this->attachmentFSManager = $container->get('app.attachment_manager');
    }

    /**
     * @ORM\PostPersist()
     * @param $args LifecycleEventArgs
     */
    public function postPersist(Page $Page, LifecycleEventArgs $args)
    {

        if ($Page->getParent()) {
            $TheParent = $args->getEntityManager()->getRepository('AppBundle:Page')->findOneBy(
                ['id' => $Page->getParent()]
            );
            $Page->setParentNode($TheParent);
            $args->getEntityManager()->persist($Page);
        }

        $this->attachmentFSManager->moveFiles($Page);

        if ($Page->getHeaderImgFileName()) {
            if (!is_dir($this->container->get('app.web_dir')->get().'/'.$Page->getUploadDir())) {
                mkdir($this->container->get('app.web_dir')->get().'/'.$Page->getUploadDir(), 0755, true);
            }
            rename(
                $this->container->get('app.web_dir')->get().'/'.$Page->getUploadDir().'../'.$Page->getHeaderImgFileName(
                ),
                $this->container->get('app.web_dir')->get().'/'.$Page->getUploadDir().$Page->getHeaderImgFileName()
            );
        }

        if ($Page->getListImgFileName()) {
            if (!is_dir($this->container->get('app.web_dir')->get().'/'.$Page->getUploadDir())) {
                mkdir($this->container->get('app.web_dir')->get().'/'.$Page->getUploadDir(), 0755, true);
            }
            rename(
                $this->container->get('app.web_dir')->get().'/'.$Page->getUploadDir().'../'.$Page->getListImgFileName(),
                $this->container->get('app.web_dir')->get().'/'.$Page->getUploadDir().$Page->getListImgFileName()
            );
        }

    }

    /**
     * @ORM\PreRemove()
     */
    public function clean(Page $page, LifecycleEventArgs $event)
    {

        if ($page->getDeletedAt()) {
            $dir = $this->base64Image->getRootDir().$page->getUploadDir();

            $fs = new Filesystem();
            $fs->remove($dir);
        }

    }

}