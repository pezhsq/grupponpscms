<?php
// 21/06/17, 10.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\EntityListener;


use AppBundle\Entity\News;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\Mapping as ORM;

class NewsListener
{

    public function __construct(
        ContainerInterface $container
    ) {

        $this->container = $container;
        $this->attachmentFSManager = $container->get('app.attachment_manager');
    }

    /**
     * @ORM\PostPersist()
     * @param $args LifecycleEventArgs
     */
    public function postPersist(News $News, LifecycleEventArgs $args)
    {

        $this->attachmentFSManager->moveFiles($News);

        if ($News->getHeaderImgFileName()) {
            if (!is_dir($this->container->get('app.web_dir')->get().'/'.$News->getUploadDir())) {
                mkdir($this->container->get('app.web_dir')->get().'/'.$News->getUploadDir(), 0755, true);
            }
            rename(
                $this->container->get('app.web_dir')->get().'/'.$News->getUploadDir().'../'.$News->getHeaderImgFileName(
                ),
                $this->container->get('app.web_dir')->get().'/'.$News->getUploadDir().$News->getHeaderImgFileName()
            );
        }

        if ($News->getListImgFileName()) {
            if (!is_dir($this->container->get('app.web_dir')->get().'/'.$News->getUploadDir())) {
                mkdir($this->container->get('app.web_dir')->get().'/'.$News->getUploadDir(), 0755, true);
            }
            rename(
                $this->container->get('app.web_dir')->get().'/'.$News->getUploadDir().'../'.$News->getListImgFileName(),
                $this->container->get('app.web_dir')->get().'/'.$News->getUploadDir().$News->getListImgFileName()
            );
        }

    }

    /**
     * @ORM\PreRemove()
     */
    public function clean(News $News, LifecycleEventArgs $event)
    {

        if ($News->getDeletedAt()) {
            $dir = $this->container->get('app.web_dir')->get().'/'.$News->getUploadDir();

            $fs = new Filesystem();
            $fs->remove($dir);
        }

    }

}