<?php
// 28/09/17, 9.15
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\EventSubscriber;

use AppBundle\Entity\Log;
use AppBundle\Entity\User;
use AppBundle\Service\LogsHelper;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class LoggableSubscriber implements EventSubscriber
{

    /**
     * @var $em EntityManager
     */
    private $em;
    /**
     * @var LogsHelper
     */
    private $logsHelper;


    public function __construct(LogsHelper $logsHelper)
    {

        $this->logsHelper = $logsHelper;
    }

    /**
     * Logs entity changeset
     *
     * @param LifecycleEventArgs $eventArgs
     * @return mixed Bool|String
     */
    public function logChangeSet(LifecycleEventArgs $eventArgs)
    {

        $this->em = $eventArgs->getEntityManager();
        $uow = $this->em->getUnitOfWork();
        $entity = $eventArgs->getEntity();
        $classMetadata = $this->em->getClassMetadata(get_class($entity));
        if ($this->isEntitySupported($classMetadata->reflClass)) {
            $uow->computeChangeSet($classMetadata, $entity);
            $changeSet = $uow->getEntityChangeSet($entity);
            $message = $entity->getUpdateLogMessage($changeSet);

            return $message;
        }

        return false;
    }

    public function getSubscribedEvents()
    {

        $events = [
            Events::postPersist,
            Events::postUpdate,
            Events::preRemove,
        ];

        return $events;
    }

    public function postPersist(LifecycleEventArgs $eventArgs)
    {

        $this->em = $eventArgs->getEntityManager();
        $entity = $eventArgs->getEntity();
        $classMetadata = $this->em->getClassMetadata(get_class($entity));
        if ($this->isEntitySupported($classMetadata->reflClass)) {
            $message = $entity->getCreateLogMessage($classMetadata->reflClass);
            $this->log(get_class($entity), $this->getID($entity), $message, 'CREATE');

        }

    }

    public function postUpdate(LifecycleEventArgs $eventArgs)
    {

        $this->em = $eventArgs->getEntityManager();
        $entity = $eventArgs->getEntity();
        $message = $this->logChangeSet($eventArgs);
        if ($message) {
            $this->log(get_class($entity), $this->getID($entity), $message, 'UPDATE');
        }

    }


    protected function isEntitySupported(\ReflectionClass $reflClass)
    {

        if (in_array('AppBundle\Traits\Loggable', $reflClass->getTraitNames())) {
            return true;
        }

        return false;

    }

    public function preRemove(LifecycleEventArgs $eventArgs)
    {

        $this->em = $eventArgs->getEntityManager();
        $entity = $eventArgs->getEntity();
        $classMetadata = $this->em->getClassMetadata(get_class($entity));
        if ($this->isEntitySupported($classMetadata->reflClass)) {
            $message = $entity->getRemoveLogMessage();
            $this->log(get_class($entity), $this->getID($entity), $message, 'DELETE');
        }

    }

    private function log($class, $id, $message, $action)
    {

        $this->logsHelper->log($class, $id, $message, $action);

    }

    private function getID($entity)
    {

        $classMetadata = $this->em->getClassMetadata(get_class($entity));
        foreach ($classMetadata->reflClass->getProperties() as $property) {
            if (strpos($property->getDocComment(), 'ORM\\Id')) {
                $method = 'get'.$property->getName();

                return $entity->$method();
            }
        }

        return '-';

    }

}