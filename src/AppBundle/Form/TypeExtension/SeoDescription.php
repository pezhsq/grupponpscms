<?php
// 10/04/17, 15.30
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form\TypeExtension;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeoDescription extends AbstractType {

    public function __construct() {

        $this->sogliaMobile  = 757;
        $this->sogliaDesktop = 930;

    }


    public function buildView(FormView $view, FormInterface $form, array $options) {

        if(isset($options['sogliaMobile'])) {
            $this->sogliaMobile = intval($options['sogliaMobile']);
        }
        if(isset($options['sogliaDesktop'])) {
            $this->sogliaDesktop = intval($options['sogliaDesktop']);
        }

        $view->vars['sogliaMobile']  = $this->sogliaMobile;
        $view->vars['sogliaDesktop'] = $this->sogliaDesktop;
    }


    public function getExtendedType() {

        return FormType::class;
    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefault('sogliaMobile', $this->sogliaDesktop);
        $resolver->setDefault('sogliaDesktop', $this->sogliaDesktop);
        $resolver->setDefault('compound', false);
    }


}