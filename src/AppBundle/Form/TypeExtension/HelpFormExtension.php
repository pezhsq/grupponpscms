<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 07/12/16
 * Time: 11.05
 */

namespace AppBundle\Form\TypeExtension;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HelpFormExtension extends AbstractTypeExtension {

    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator) {
        $this->translator = $translator;
    }


    public function buildView(FormView $view, FormInterface $form, array $options) {

        if ($options['help']) {
            $view->vars['help'] = $this->translator->trans($options['help']);
        }
    }


    public function getExtendedType() {
        return FormType::class;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefault('help', null);
    }


}