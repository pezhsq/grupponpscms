<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 07/12/16
 * Time: 11.05
 */

namespace AppBundle\Form\TypeExtension;


use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IconFormExtension extends AbstractTypeExtension {


    public function buildView(FormView $view, FormInterface $form, array $options) {

        if(isset($options['iconBefore'])) {
            $view->vars['iconBefore'] = $options['iconBefore'];
        }
        if(isset($options['iconAfter'])) {
            $view->vars['iconAfter'] = $options['iconAfter'];
        }
    }


    public function getExtendedType() {

        return FormType::class;
    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefault('iconBefore', null);
        $resolver->setDefault('iconAfter', null);

    }


}