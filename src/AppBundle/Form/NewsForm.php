<?php

namespace AppBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsCategory;
use AppBundle\Entity\NewsHasCategory;
use AppBundle\Entity\Page;
use AppBundle\Form\TypeExtension\EntityTreeFormExtension;
use AppBundle\Form\TypeExtension\SeoDescription;
use AppBundle\Form\TypeExtension\SeoTitle;
use AppBundle\Service\Explorer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints\Choice;
use AppBundle\Repository\NewsHasCategoriesRepository;

class NewsForm extends AbstractType
{

    /**
     * @var AuthorizationChecker
     */
    private $auth;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * NewsForm constructor.
     */
    public function __construct(AuthorizationChecker $auth, EntityManager $em)
    {

        $this->auth = $auth;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('uuid', HiddenType::class, ['data' => Uuid::uuid1()->__toString()]);

        $builder->add('listImg', FileType::class);
        $builder->add('listImgData', HiddenType::class);
        $builder->add('listImgAlt', TextType::class, []);
        $builder->add('listImgDelete', HiddenType::class, []);

        $builder->add('headerImg', FileType::class);
        $builder->add('headerImgData', HiddenType::class);
        $builder->add('headerImgAlt', TextType::class, []);
        $builder->add('headerImgDelete', HiddenType::class, []);

        $builder->add(
            'primaryCategory',
            ChoiceType::class,
            [
                'label' => 'news.labels.categoria_principale',
                'choices' => $this->getCategories($options['locale']),
            ]
        );

        $builder->get('primaryCategory')->addModelTransformer(
            new CallbackTransformer(
                function ($NewsCategory) {

                    return $NewsCategory;
                },
                function ($news_category_id) {

                    return $this->em->getRepository('AppBundle:NewsCategory')->findOneBy(['id' => $news_category_id]);

                }
            )
        );


        $builder->add(
            'categorieAggiuntive',
            ChoiceType::class,
            [
                'label' => 'news.labels.categorie_aggiuntive',
                'required' => false,
                'multiple' => true,
                'choices' => $this->getCategories($options['locale']),
            ]
        );

        $builder->get('categorieAggiuntive')->addModelTransformer(
            new CallbackTransformer(
                function ($NewsCategory) {

                    return $NewsCategory;

                },
                function ($news_category_ids) {

                    $ret = $this->em->getRepository('AppBundle:NewsCategory')->findBy(['id' => $news_category_ids]);

                    return $ret;

                }
            )
        );


        $builder->add(
            'commentiEnabled',
            ChoiceType::class,
            [
                'label' => 'news.labels.commenti_enabled',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'news.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );


        $builder->add(
            'publishAt',
            null,
            [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy HH:mm',
                'label' => 'news.labels.data_pubblicazione',
                'iconAfter' => 'glyphicon glyphicon-calendar',
                'attr' => [
                    'class' => 'datepicker',
                ],
            ]
        );

        $fields = [
            'isEnabled' => [
                'label' => 'news.labels.is_enabled',
                'field_type' => ChoiceType::class,
                'required' => false,
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
            ],
            'titolo' => [
                'label' => 'news.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            'sottotitolo' => [
                'label' => 'news.labels.sottotitolo',
                'required' => false,
            ],
        ];

        $excluded_fields = ['visits', 'shortUrl'];

        if (!$this->auth->isGranted('ROLE_EXTRA_SEO')) {
            $excluded_fields = ['visits', 'shortUrl', 'metaTitle', 'metaDescription', 'slug'];
        } else {

            $fields = array_merge(
                $fields,
                [
                    'metaTitle' => [
                        'label' => 'default.labels.meta_title',
                        'field_type' => SeoTitle::class,
                        'required' => false,
                    ],
                    'metaDescription' => [
                        'label' => 'default.labels.meta_description',
                        'field_type' => SeoDescription::class,
                        'required' => false,
                    ],
                    'slug' => [
                        'label' => 'default.labels.slug',
                        'required' => false,
                    ],
                ]
            );

        }

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'exclude_fields' => $excluded_fields,
            ]
        );


        $builder->add(
            'newsRows',
            CollectionType::class,
            [
                'entry_type' => NewsRowType::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_extra_fields' => true,
                'label' => false,
                'attr' => [],
            ]
        );


    }

    private function getCategories($locale = 'en')
    {

        $cat = $this->em->getRepository('AppBundle:NewsCategory')->findAll();

        $data = [];

        foreach ($cat as $c) {

            if (!$c->getDeletedAt()) {
                $data[$c->translate($locale)->getTitolo().($c->getIsEnabled() ? '' : ' (disabilitata)')] = $c->getId();
            }

        }

        return $data;


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => News::class,
                'locale' => 'it',
                'langs' => [
                    'it' => 'Italiano',
                    'allow_extra_fields' => true,
                ],
            ]
        );
    }

    public function getName()
    {

        return 'news';
    }


}
