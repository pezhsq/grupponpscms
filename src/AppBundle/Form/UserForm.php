<?php
// 14/08/17, 6.54
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use AppBundle\Entity\User;
use Doctrine\DBAL\Types\TextType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class UserForm extends AbstractType
{

    /**
     * @var AuthorizationChecker
     */
    private $auth;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(
        AuthorizationChecker $auth,
        EntityManager $em,
        TokenStorage $tokenStorage
    ) {

        $this->auth = $auth;
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /**
         * @var $User User
         */
        $User = $builder->getData();

        $usernameDisabled = false;

        if ($User->getId()) {
            $usernameDisabled = true;
        }

        $builder->add('listImg', FileType::class, ['label' => false, 'attr' => ['class' => 'upload-trigger']]);
        $builder->add('listImgData', HiddenType::class, ['attr' => ['class' => 'data-holder']]);
        $builder->add('listImgDelete', HiddenType::class, []);

        $builder->add('nome', null, ['label' => 'operatori.labels.nome'])->add(
            'cognome',
            null,
            ['label' => 'operatori.labels.cognome']
        )->add('email', EmailType::class, ['label' => 'operatori.labels.email'])->add(
            'isEnabled',
            ChoiceType::class,
            [
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'label' => 'operatori.labels.is_enabled',
                'help' => 'operatori.help.is_enabled',
                'placeholder' => false,
                'required' => false,
            ]
        )->add(
            'role',
            ChoiceType::class,
            [
                'choices' => $this->getGroups(),
                'label' => 'operatori.labels.ruolo',
            ]
        );


        $builder->add(
            'username',
            null,
            ['disabled' => $usernameDisabled, 'required' => !$usernameDisabled, 'label' => 'operatori.labels.username']
        );


        $builder->add(
            'plainPassword',
            RepeatedType::class,
            [
                'type' => PasswordType::class,
                'required' => !$usernameDisabled,
                'first_options' => ['label' => 'operatori.labels.password', 'help' => 'operatori.help.password'],
                'second_options' => ['label' => 'operatori.labels.repeat_password'],
            ]
        )->add(
            'profileText',
            TextareaType::class,
            [
                'label' => 'operatori.labels.profile_text',
                'required' => !$usernameDisabled,
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => User::class,
                'allow_extra_fields' => true,
            ]
        );

    }

    private function getGroups()
    {

        /**
         * @var $LoggedUser User
         */
        $LoggedUser = $this->tokenStorage->getToken()->getUser();

        $choices = [];

        if ($LoggedUser->getRole() == 'ROLE_SUPER_ADMIN') {

            $choices['Superadmin'] = 'ROLE_SUPER_ADMIN';

            $Groups = $this->em->getRepository('AppBundle:Group')->findBy([], ['level' => 'ASC']);

        } else {

            $Group = $this->em->getRepository('AppBundle:Group')->findOneBy(['role' => $LoggedUser->getRole()]);

            $Groups = $this->em->getRepository('AppBundle:Group')->findLessImportant($Group);

        }


        foreach ($Groups as $group) {
            $choices[$group->getName()] = $group->getRole();
        }

        return $choices;


    }

}