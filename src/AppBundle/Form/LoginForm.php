<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 01/12/16
 * Time: 15.52
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('_username', null, ['label' => 'operatori.labels.username'])
            ->add('_password', PasswordType::class, ['label' => 'operatori.labels.password']);
        
    }


}