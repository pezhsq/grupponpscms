<?php
// 12/01/17, 16.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\NewsCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use AppBundle\Service\Explorer;
use AppBundle\Service\PageHelper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class CategorieNewsForm extends AbstractType
{
	
	/**
	 * @var AuthorizationChecker
	 */
	private $auth;
	/**
	 * @var Explorer
	 */
	private $explorer;
	/**
	 * @var EntityRepository
	 */
	private $em;
	/**
	 * @var PageHelper
	 */
	private $pageHelper;
	
	
	public function __construct(
		AuthorizationChecker $auth,
		Explorer $explorer,
		EntityManager $em,
		PageHelper $pageHelper
	) {
		
		$this->auth = $auth;
		$this->explorer = $explorer;
		$this->em = $em;
		$this->pageHelper = $pageHelper;
		
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		$choices = $this->explorer->listFiles(
			'Resources/views/public/layouts/'.$options['layout'].'/CONTENT/',
			'.html.twig'
		);
		
		$builder->add(
			'isEnabled',
			ChoiceType::class,
			[
				'label' => 'newscat.labels.is_public',
				'choices' => [
					'default.labels.si' => true,
					'default.labels.no' => false,
				],
				'placeholder' => false,
				'required' => false,
			]
		);
		
		/*
		* Aggiunta modulo a categoria
		*/
		$builder->add(
			'template',
			ChoiceType::class,
			[
				'label' => 'pages.labels.template',
				'choices' => $choices,
				'placeholder' => false,
				'required' => true,
			]
		);
		
		$fields = [
			'titolo' => [
				'label' => 'newscat.labels.titolo',
				'required' => true,
				'attr' => ['class' => 'titolo'],
			],
			'sottotitolo' => [
				'label' => 'newscat.labels.sottotitolo',
				'required' => false,
			],
		];
		
		$excluded_fields = [];
		
		if (!$this->auth->isGranted('ROLE_EXTRA_SEO')) {
			$excluded_fields = ['metaTitle', 'metaDescription', 'slug'];
		} else {
			
			$fields = array_merge(
				$fields,
				[
					'metaTitle' => [
						'label' => 'default.labels.meta_title',
						'required' => false,
					],
					'metaDescription' => [
						'label' => 'default.labels.meta_description',
						'required' => false,
					],
					'slug' => [
						'label' => 'default.labels.slug',
						'required' => false,
						'attr' => ['class' => 'slug'],
					],
				]
			);
			
		}
		
		$fields['testo'] = [
			'label' => 'newscat.labels.testo',
			'required' => false,
			'attr' => ['class' => 'ck'],
		];
		
		$builder->add(
			'translations',
			TranslationsType::class,
			[
				'locales' => array_keys($options['langs']),
				'fields' => $fields,
				'required_locales' => array_keys($options['langs']),
				'exclude_fields' => $excluded_fields,
			]
		);
		
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		
		$resolver->setDefaults(
			[
				'data_class' => NewsCategory::class,
				'error_bubbling' => true,
				'layout' => 'webtek',
				'langs' => [
					'it' => 'Italiano',
					'allow_extra_fields' => true,
				],
			]
		);
	}
	
	
}