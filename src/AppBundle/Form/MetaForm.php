<?php
// 08/03/17, 12.13
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\Meta;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetaForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'etichetta',
            TextType::class,
            ['label' => 'meta.labels.label', 'help' => 'meta.help.label']
        );
        $builder->add('chiave', TextType::class, ['label' => 'meta.labels.key', 'help' => 'meta.help.key']);
        $builder->add('group', EntityType::class, ['class' => 'AppBundle:MetaGroup']);
        $builder->add('aiuto', TextType::class, ['label' => 'meta.labels.help', 'help' => 'meta.help.help']);

        $fields = [
            'valore' => [
                'label'    => 'meta.labels.valore',
                'required' => true,
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales'          => array_keys($options['langs']),
                'fields'           => $fields,
                'required_locales' => array_keys($options['langs']),
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class'         => Meta::class,
                'allow_extra_fields' => true,
                'langs'              => [
                    'it' => 'Italiano',
                ],
            ]
        );

    }

}