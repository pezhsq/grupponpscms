<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 12/12/16
 * Time: 16.36
 */

namespace AppBundle\Form;


use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class LostPassForm extends AbstractType {


    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('email', null, ['label' => 'operatori.labels.email'])
            ->add('recaptcha', EWZRecaptchaType::class, ['constraints' => [
                new RecaptchaTrue()
            ],
                'label' => false,
                'error_bubbling' => true]);


    }

}