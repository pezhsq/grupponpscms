<?php
// 08/03/17, 9.19
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use AppBundle\Entity\SettingsGroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsGroupForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('name', TextType::class, ['label' => 'settings_group.labels.name']);

    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class' => SettingsGroup::class]);

    }

}