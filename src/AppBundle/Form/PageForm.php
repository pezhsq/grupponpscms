<?php

namespace AppBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\Page;
use AppBundle\Form\TypeExtension\EntityTreeFormExtension;
use AppBundle\Form\TypeExtension\SeoDescription;
use AppBundle\Form\TypeExtension\SeoTitle;
use AppBundle\Service\Explorer;
use AppBundle\Service\PageHelper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class PageForm extends AbstractType
{

    /**
     * @var AuthorizationChecker
     */
    private $auth;
    /**
     * @var Explorer
     */
    private $explorer;
    /**
     * @var EntityRepository
     */
    private $em;
    /**
     * @var PageHelper
     */
    private $pageHelper;

    /**
     * PageForm constructor.
     */
    public function __construct(
        AuthorizationChecker $auth,
        Explorer $explorer,
        EntityManager $em,
        PageHelper $pageHelper
    ) {

        $this->auth = $auth;
        $this->explorer = $explorer;
        $this->em = $em;
        $this->pageHelper = $pageHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $choices = $this->explorer->listFiles(
            'Resources/views/public/layouts/'.$options['layout'].'/CONTENT/',
            '.html.twig'
        );

        $builder->add('uuid', HiddenType::class, ['data' => Uuid::uuid1()->__toString()]);

        $builder->add('listImg', FileType::class);
        $builder->add('listImgData', HiddenType::class);
        $builder->add('listImgAlt', TextType::class, []);
        $builder->add('listImgDelete', HiddenType::class, []);

        $builder->add('headerImg', FileType::class);
        $builder->add('headerImgData', HiddenType::class);
        $builder->add('headerImgAlt', TextType::class, []);
        $builder->add('headerImgDelete', HiddenType::class, []);


        $builder->add(
            'template',
            ChoiceType::class,
            [
                'label' => 'pages.labels.template',
                'choices' => $choices,
            ]
        );

        $builder->add(
            'level',
            ChoiceType::class,
            [
                'label' => 'pages.labels.level',
                'choices' => [
                    'pages.labels.level1' => '10',
                    'pages.labels.level2' => '20',
                    'pages.labels.level3' => '30',
                    'pages.labels.level4' => '40',
                    'pages.labels.level5' => '50',
                ],
            ]
        );

        $choicesParent = $this->getTree($builder->getData());

        $builder->add(
            'parent',
            ChoiceType::class,
            [
                'choices' => $choicesParent,
                'label' => 'pages.labels.parent',
                'choice_label' => function ($value, $key, $index) {

                    return preg_replace('/^\[\d{3}\]/', '', $key);
                },
                'empty_data' => null,
                'placeholder' => false,
                'required' => false,
            ]
        );


        $builder->add(
            'type',
            ChoiceType::class,
            [
                'label' => 'pages.labels.type',
                'choices' => ['pages.labels.type_page' => 'PAGE', 'pages.labels.type_link' => 'LINK'],
            ]
        );

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'pages.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );


        $fields = [
            'isEnabled' => [
                'label' => 'pages.labels.is_enabled',
                'field_type' => ChoiceType::class,
                'required' => false,
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
            ],
            'titolo' => [
                'label' => 'pages.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            "titolo_menu" => [
                "label" => "pages.labels.titolo_menu",
                "required" => false,
            ],
            'sottotitolo' => [
                'label' => 'pages.labels.sottotitolo',
                'required' => false,
            ],
        ];

        $excluded_fields = ['shortUrl'];

        if (!$this->auth->isGranted('ROLE_EXTRA_SEO')) {
            $excluded_fields = ['metaTitle', 'metaDescription', 'slug', 'shortUrl'];
            $builder->add('robots', HiddenType::class);
        } else {

            $fields = array_merge(
                $fields,
                [
                    'metaTitle' => [
                        'label' => 'default.labels.meta_title',
                        'required' => false,
                        'field_type' => SeoTitle::class,
                    ],
                    'metaDescription' => [
                        'label' => 'default.labels.meta_description',
                        'required' => false,
                        'field_type' => SeoDescription::class,
                    ],
                    'slug' => [
                        'label' => 'default.labels.slug',
                        'required' => false,
                    ],
                ]
            );
            $builder->add(
                'robots',
                ChoiceType::class,
                [
                    'label' => 'pages.labels.robots',
                    'choices' => [
                        'index,follow' => 'index,follow',
                        'index,nofollow' => 'index,nofollow',
                        'noindex,follow' => 'noindex,follow',
                        'noindex,nofollow' => 'noindex,nofollow',
                    ],
                ]
            );

        }

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'exclude_fields' => $excluded_fields,
            ]
        );


        $builder->add(
            'pageRows',
            CollectionType::class,
            [
                'entry_type' => PageRowType::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_extra_fields' => true,
                'label' => false,
                'attr' => [],
            ]
        );


    }

    private function getTree(Page $Page = null)
    {

        $parent = 0;

        $choices = $this->pageHelper->getTree($parent, true, $Page, '');

        return $choices;

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Page::class,
                'error_bubbling' => true,
                'layout' => 'webtek',
                'langs' => [
                    'it' => 'Italiano',
                    'allow_extra_fields' => true,
                ],
            ]
        );
    }

    public function getName()
    {

        return 'pages';
    }

}
