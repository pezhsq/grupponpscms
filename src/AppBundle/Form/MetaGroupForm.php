<?php
// 08/03/17, 9.19
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use AppBundle\Entity\MetaGroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MetaGroupForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', TextType::class, ['label' => 'meta_group.labels.name']);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(['data_class' => MetaGroup::class]);

    }

}