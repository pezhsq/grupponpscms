<?php
// 12/01/17, 16.29
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Entity\GalleryCategory;
use AppBundle\Form\TypeExtension\SeoDescription;
use AppBundle\Form\TypeExtension\SeoTitle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use AppBundle\Service\Explorer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class CategorieGalleryForm extends AbstractType
{

    /**
     * @var AuthorizationChecker
     */
    private $auth;
    /**
     * @var Explorer
     */
    private $explorer;
    /**
     * @var EntityRepository
     */
    private $em;

    public function __construct(
        AuthorizationChecker $auth,
        Explorer $explorer,
        EntityManager $em
    ) {

        $this->auth = $auth;
        $this->explorer = $explorer;
        $this->em = $em;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('listImg', FileType::class);
        $builder->add('listImgData', HiddenType::class);
        $builder->add('listImgAlt', TextType::class, []);
        $builder->add('listImgDelete', HiddenType::class, []);

        $builder->add('headerImg', FileType::class);
        $builder->add('headerImgData', HiddenType::class);
        $builder->add('headerImgAlt', TextType::class, []);
        $builder->add('headerImgDelete', HiddenType::class, []);

        $choices = $this->explorer->listFiles(
            'Resources/views/public/layouts/'.$options['layout'].'/CONTENT/',
            '.html.twig'
        );

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label' => 'default.labels.is_public',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        );

        /*
        * Aggiunta modulo a categoria
        */
        $builder->add(
            'template',
            ChoiceType::class,
            [
                'label' => 'gallerycat.labels.template',
                'choices' => $choices,
                'placeholder' => false,
                'required' => true,
            ]
        );

        $fields = [
            'titolo' => [
                'label' => 'gallerycat.labels.titolo',
                'required' => true,
                'attr' => ['class' => 'titolo'],
            ],
            'sottotitolo' => [
                'label' => 'gallerycat.labels.sottotitolo',
                'required' => false,
            ],
        ];

        $excluded_fields = [];

        if (!$this->auth->isGranted('ROLE_EXTRA_SEO')) {
            $excluded_fields = ['metaTitle', 'metaDescription', 'slug'];
        } else {

            $fields = array_merge(
                $fields,
                [
                    'metaTitle' => [
                        'label' => 'default.labels.meta_title',
                        'field_type' => SeoTitle::class,
                        'required' => false,
                    ],
                    'metaDescription' => [
                        'label' => 'default.labels.meta_description',
                        'field_type' => SeoDescription::class,
                        'required' => false,
                    ],
                    'slug' => [
                        'label' => 'default.labels.slug',
                        'required' => false,
                        'attr' => ['class' => 'slug'],
                    ],
                ]
            );

        }

        $fields['testo'] = [
            'label' => 'gallerycat.labels.testo',
            'required' => false,
            'attr' => ['class' => 'ck'],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales' => array_keys($options['langs']),
                'fields' => $fields,
                'required_locales' => array_keys($options['langs']),
                'exclude_fields' => $excluded_fields,
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => GalleryCategory::class,
                'error_bubbling' => true,
                'layout' => 'webtek',
                'langs' => [
                    'it' => 'Italiano',
                    'allow_extra_fields' => true,
                ],
            ]
        );
    }


}