<?php
// 03/03/17, 11.38
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use AppBundle\Entity\Commento;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentoForm extends AbstractType {

    public function __construct(EntityManager $em) {

        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('nome')
            ->add('email', EmailType::class)
            ->add('parent', HiddenType::class)
            ->add('messaggio', TextareaType::class)
            ->add('news', HiddenType::class);

        $builder->get('news')->addModelTransformer(new CallbackTransformer(
            function ($NewsCategory) {

                return $NewsCategory;
            },
            function ($news_id) {

                return $this->em->getRepository('AppBundle:News')->findOneBy(['id' => $news_id]);

            }
        ));

    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class'         => Commento::class,
                                'allow_extra_fields' => true]);
    }


}