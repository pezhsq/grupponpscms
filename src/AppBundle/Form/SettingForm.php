<?php
// 08/03/17, 12.13
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use AppBundle\Entity\Settings;
use AppBundle\Service\RolesHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingForm extends AbstractType {

    /**
     * @var RolesHelper
     */
    private $rolesHelper;

    public function __construct(RolesHelper $rolesHelper) {

        $this->rolesHelper = $rolesHelper;
    }


    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('etichetta', TextType::class, ['label' => 'settings.labels.label', 'help' => 'settings.help.label']);
        $builder->add('aiuto', TextType::class, ['label' => 'settings.labels.help', 'help' => 'settings.help.help']);
        $builder->add('chiave', TextType::class, ['label' => 'settings.labels.key', 'help' => 'settings.help.key']);
        $builder->add('valore', TextType::class, ['label' => 'settings.labels.value']);
        $builder->add('group', EntityType::class, ['class' => 'AppBundle:SettingsGroup']);
        $builder->add('tipo', ChoiceType::class, ['label'   => 'settings.labels.tipo', 'help' => 'settings.help.tipo',
                                                  'choices' => ['Booleano' => 'BOOLEAN',
                                                                'Testo'    => 'TEXT']]);
        $builder->add('requiredRole', ChoiceType::class, ['label'   => 'settings.labels.required_role', 'help' => 'settings.help.required_role',
                                                          'choices' => $this->getRoleChoices()]);

    }

    private function getRoleChoices() {

        $roles = $this->rolesHelper->getAvailableRoles();

        $ruoli = [];

        foreach($roles as $r) {
            $ruoli[$r] = $r;
        }

        return $ruoli;

    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class' => Settings::class]);

    }

}