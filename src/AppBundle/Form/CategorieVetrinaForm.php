<?php
// 09/01/17, 17.15
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Form;


use AppBundle\Entity\VetrinaCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategorieVetrinaForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('nome')
            ->add('dimensioni', null, ['help' => 'vetrinacat.help.dimensioni']);


    }


    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class' => VetrinaCategory::class]);
    }

}