<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('nome', null, ['label' => 'operatori.labels.nome'])
            ->add('cognome', null, ['label' => 'operatori.labels.cognome'])
            ->add('email', EmailType::class, ['label' => 'operatori.labels.email'])
            ->add('isEnabled', ChoiceType::class,
                ['choices'     => ['default.labels.si' => true,
                                   'default.labels.no' => false],
                 'label'       => 'operatori.labels.is_enabled',
                 'help'        => 'operatori.help.is_enabled',
                 'placeholder' => false,
                 'required'    => false])
            ->add('username', null, ['disabled' => true, 'required' => false, 'label' => 'operatori.labels.username'])
            ->add('plainPassword', RepeatedType::class, [
                'type'           => PasswordType::class,
                'required'       => false,
                'first_options'  => ['label' => 'operatori.labels.password', 'help' => 'operatori.help.password'],
                'second_options' => ['label' => 'operatori.labels.repeat_password']
            ])
            ->add('ruolo', ChoiceType::class, ['choices' =>
                                                   ['operatori.labels.super_admin'     => 'ROLE_SUPER_ADMIN',
                                                    'operatori.labels.amministrazione' => 'ROLE_ADMIN',
                                                    'operatori.labels.gestione'        => 'ROLE_USER'],
                                               'label'   => 'operatori.labels.ruolo'])
            ->add('profileText', TextareaType::class,
                ['label'    => 'operatori.labels.profile_text',
                 'required' => false]);

    }


    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class'         => User::class,
                                'allow_extra_fields' => true]);

    }

}