<?php
// 17/01/17, 14.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MaxCategoriesValidator extends ConstraintValidator {

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var
     */
    private $max_catgories;
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(EntityManager $em, Translator $translator, $blog) {

        $this->em = $em;
        
        $this->max_catgories = $blog['max_active_category'];
        $this->translator    = $translator;
    }

    public function validate($object, Constraint $constraint) {

        if($object->getIsEnabled()) {

            $count = $this->em->getRepository('AppBundle:NewsCategory')->countAllActive($object->getId());

            if($count >= $this->max_catgories) {
                $this->context->buildViolation($this->translator->trans($constraint->message_field))
                    ->setParameter('%string%', $object->translate('it')->getTitolo())
                    ->setParameter('%max%', $this->max_catgories)
                    ->atPath('isEnabled')
                    ->addViolation();
                $this->context->buildViolation($this->translator->trans($constraint->message))
                    ->setParameter('%string%', $object->translate('it')->getTitolo())
                    ->setParameter('%max%', $this->max_catgories)
                    ->addViolation();
            }

        }

    }
}