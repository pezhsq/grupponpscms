<?php
// 20/04/17, 11.51
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class CodiceFiscale
 * @package AppBundle\Validator\Constraints
 * @Annotaion
 */
class CodiceFiscale extends Constraint
{

    public $message = 'La stringa {{ string }} non è un codice fiscale valido';

    public function validatedBy()
    {

        return get_class($this).'Validator';
    }
}