<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 28/05/17
 * Time: 11.40
 */

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_FILEMANAGER')")
 */
class FileManagerController extends Controller
{

    /**
     * @Route("/filemanager", name="filemanager")
     */
    public function indexAction()
    {

        return $this->render(':admin/filemanager:filemanager.html.twig');

    }

    /**
     * @Route("/filemanager/tree", name="filemanager_tree")
     */
    public function getTree()
    {

        $FileManagerHelper = $this->get('app.filemanager_helper');

        $tree = [];

        $item = ['id' => 'assets',
            'text' => 'assets',
            'children' => [],
        ];

        $tree[] = $item;

        $item = ['id' => 'files',
            'text' => 'files',
            'children' => [],
        ];

        $tree[] = $item;

        $item = ['id' => 'media',
            'text' => 'media',
            'children' => [],
        ];

        $tree[] = $item;

        foreach ($tree as &$item) {
            $item['children'] = $FileManagerHelper->getSubFolders($item['id']);
        }

        return new JsonResponse($tree);

    }

    /**
     * @Route("/filemanager/content", name="filemanager_content")
     */
    public function getFiles(Request $request)
    {
        $FileManagerHelper = $this->get('app.filemanager_helper');


        $files = $FileManagerHelper->loadContent($request->request->get('path'), $request->request->get('filters'));

        $return = [];
        $return['result'] = true;
        $return['files'] = $files;

        return new JsonResponse($return);
    }

    /**
     * @Route("/filemanager/crea-directory", name="filemanager_crea_dir")
     */
    public function creaDir(Request $request)
    {
        $translator = $this->get('translator');
        $FileManagerHelper = $this->get('app.filemanager_helper');

        $creata = $FileManagerHelper->creaDir($request->request->get('path'), $request->request->get('dir'));

        $return = [];
        if ($creata === true) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
            $return['errors'] = [$translator->trans($creata)];
        }

        return new JsonResponse($return);
    }

    /**
     * @Route("/filemanager/crea-versione", name="filemanager_crea_versione")
     */
    public function creaVersione(Request $request)
    {

        $translator = $this->get('translator');
        $FileManagerHelper = $this->get('app.filemanager_helper');

        $creata = $FileManagerHelper->creaVersione($request->request->get('path'), $request->request->get('dimensioni'));

        $return = [];
        if ($creata === true) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
            $return['errors'] = [$translator->trans($creata)];
        }

        return new JsonResponse($return);
    }

    /**
     * @Route("/filemanager/delete", name="filemanager_delete")
     */
    public function delete(Request $request)
    {
        $FileManagerHelper = $this->get('app.filemanager_helper');
        $translator = $this->get('translator');

        $return = [];

        $result = $FileManagerHelper->delete($request->request->get('path'));

        if ($result === true) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
            $return['errors'] = [$translator->trans($result)];
        }

        return new JsonResponse($return);
    }

    /**
     * @Route("/filemanager/upload", name="filemanager_upload")
     */
    public function upload(Request $request)
    {

        $FileManagerHelper = $this->get('app.filemanager_helper');
        $translator = $this->get('translator');

        $result = $FileManagerHelper->upload($request->files, $request->request->get('path'));

        if ($result === true) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
            $return['errors'] = [$translator->trans($result)];
        }

        return new JsonResponse($return);
    }

}