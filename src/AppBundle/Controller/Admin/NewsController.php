<?php
// 13/01/17, 13.50
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Commento;
use AppBundle\Entity\News;
use AppBundle\Entity\NewsAttachment;
use AppBundle\Entity\NewsRows;
use AppBundle\Form\NewsForm;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_NEWS')")
 */
class NewsController extends Controller
{

    /**
     * @Route("/news", name="news")
     */
    public function listAction()
    {

        return $this->render('admin/news/list.html.twig');

    }

    /**
     * @Route("/news/new", name="news_new")
     */
    public function newAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(NewsForm::class, null, ['langs' => $langs]);

        $form->handleRequest($request);

        $News = new News();

        $CheckCategories = $em->getRepository('AppBundle:NewsCategory')->countAllActive();

        if (!$CheckCategories) {

            $translator = $this->get('translator');

            $this->addFlash('error', $translator->trans('news.messages.not_enough_cat'));

            return $this->redirectToRoute('news');

        }

        if ($form->isSubmitted() && $form->isValid()) {


            /**
             * @var $News News
             */
            $News = $form->getData();

            $em->persist($News);
            $em->flush();

            $translator = $this->get('translator');

            $rootDir = $this->get('app.web_dir')->get().'/';

            if (!is_dir($rootDir.$News->getUploadDir())) {
                mkdir($rootDir.$News->getUploadDir(), 0755, true);
            }

            foreach ($News->getNewsRows() as $NewsRow) {
                /**
                 * @var $NewsRow NewsRows
                 */

                if ($NewsRow->getSlot1Image()) {
                    $file = $rootDir.'files/news/'.$NewsRow->getSlot1Image();
                    if (file_exists($file)) {
                        rename($file, $rootDir.$News->getUploadDir().$NewsRow->getSlot1Image());
                    }
                }
                if ($NewsRow->getSlot2Image()) {
                    $file = $rootDir.'files/news/'.$NewsRow->getSlot2Image();
                    if (file_exists($file)) {
                        rename($file, $rootDir.$News->getUploadDir().$NewsRow->getSlot2Image());
                    }
                }
                if ($NewsRow->getSlot3Image()) {
                    $file = $rootDir.'files/news/'.$NewsRow->getSlot3Image();
                    if (file_exists($file)) {
                        rename($file, $rootDir.$News->getUploadDir().$NewsRow->getSlot3Image());
                    }

                }

            }

            $elemento = '"'.$News->translate($request->getLocale())->getTitolo().'" ('.$News->getId().') ';

            $this->addFlash('success', 'News '.$elemento.$translator->trans('default.labels.creata'));

            return $this->redirectToRoute('news');

        }

        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;

        return $this->render(
            'admin/news/new.html.twig',
            ['form' => $form->createView(), 'supportData' => $supportData]
        );

    }

    /**
     * @Route("/news/edit/{id}", requirements={"id" = "\d+"}, name="news_edit")
     */
    public function editAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) {
            $News = $em->getRepository('AppBundle:News')->findOneBy(['id' => $request->get('id')]);
            if (!$News) {
                return $this->redirectToRoute('news_new');
            }
        }

        $originalNewsRows = new ArrayCollection();
        // Crea an ArrayCollection delle attuali newsrows nel db
        foreach ($News->getNewsRows() as $newsRow) {
            $originalNewsRows->add($newsRow);
        }

        $originaleNewsHasCat = new ArrayCollection();
        foreach ($News->getNewsCategoriesAssociation() as $association) {
            $originaleNewsHasCat->add($association);
        }

        $form = $this->createForm(NewsForm::class, $News, ['langs' => $langs]);
        $form->handleRequest($request);

        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;
        if ($News->getListImgFileName()) {
            $supportData['listImgUrl'] = '/'.$News->getUploadDir().$News->getListImgFileName();
        }
        if ($News->getHeaderImgFileName()) {
            $supportData['headerImgUrl'] = '/'.$News->getUploadDir().$News->getHeaderImgFileName();
        }


        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($originaleNewsHasCat as $NewsHasCat) {
                if (false === $News->getNewsCategoriesAssociation()->contains($NewsHasCat)) {
                    $em->remove($NewsHasCat);
                }
            }

            foreach ($originalNewsRows as $newsRow) {
                if (false === $News->getNewsRows()->contains($newsRow)) {
                    $em->remove($newsRow);
                }
            }

            $cancellaHeaderPrecedente = $request->get('news_form')['headerImgDelete'];
            if ($cancellaHeaderPrecedente) {
                $this->container->get('vich_uploader.upload_handler')->remove($News, 'headerImg');
                $News->setHeaderImg(null);
                $News->setHeaderImgAlt('');
            }

            $cancellaListPrecedente = $request->get('news_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->container->get('vich_uploader.upload_handler')->remove($News, 'listImg');
                $News->setListImg(null);
                $News->setListImgAlt('');
            }
            $em->persist($News);
            $em->flush();

            $elemento = '"'.$News->translate($request->getLocale())->getTitolo().'" ('.$News->getId().') ';

            $translator = $this->get('translator');

            $this->addFlash('success', 'News '.$elemento.$translator->trans('default.labels.modificata'));

            return $this->redirectToRoute('news');

        }

        return $this->render(
            'admin/news/edit.html.twig',
            ['form' => $form->createView(), 'news' => $News, 'supportData' => $supportData]
        );

    }

    /**
     * @Route("/news/json", name="news_list_json")
     */
    public
    function listJson(
        Request $request
    ) {

        $em = $this->getDoctrine()->getManager();


        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $NewsList = $em->getRepository('AppBundle:News')->findAll();
        } else {
            $NewsList = $em->getRepository('AppBundle:News')->findAllNotDeleted();
        }

        $retData = [];

        $news = [];

        $commentRepository = $em->getRepository('AppBundle:Commento');

        foreach ($NewsList as $News) {

            $categories = [];

            $cats = $News->getCategorieAggiuntive();

            foreach ($cats as $cat) {
                $categories[] = $cat->translate($request->getLocale())->getTitolo();
            }
            /**
             * @var $News News;
             */
            $record = [];
            $record['id'] = $News->getId();
            $record['titolo'] = $News->translate($request->getLocale())->getTitolo();
            $record['isEnabled'] = $News->getIsEnabled();
            $record['category'] = $News->getPrimaryCategory()->translate($request->getLocale())->getTitolo();
            if ($categories) {
                $record['category'] .= ' ('.implode(', ', $categories).')';
            }

            $record['commenti_attivi'] = $commentRepository->getCommentiCountForNews($News);
            $record['commenti'] = $commentRepository->getCommentiCountForNews($News, 0);
            $record['deleted'] = $News->isDeleted();
            $record['createdAt'] = $News->getCreatedAt()->format('d/m/Y H:i:s');
            $record['publishAt'] = $News->getPublishAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $News->getUpdatedAt()->format('d/m/Y H:i:s');

            $news[] = $record;
        }

        $retData['data'] = $news;

        return new JsonResponse($retData);

    }


    /**
     * @Route("/news/commenti/{id}", name="news_commenti_list")
     */
    public
    function commentiAction(
        Request $request
    ) {

        return $this->render('admin/news/list_commenti.html.twig', ['id' => $request->get('id')]);

    }

    /**
     * @Route("/news/commenti/json/{id}", name="news_commenti_list_json")
     */
    public
    function listCommentiJson(
        Request $request,
        News $news
    ) {

        $commenti = $news->getCommenti();

        $return = [];
        $return['data'] = [];

        foreach ($commenti as $Commento) {

            /**
             * @var $Commento Commento;
             */

            $record = [];
            $record['id'] = $Commento->getId();
            $record['nome'] = $Commento->getNome();
            $record['messaggio'] = $Commento->getMessaggio();
            $record['email'] = $Commento->getEmail();
            $record['isEnabled'] = $Commento->getIsEnabled();
            $record['deleted'] = $Commento->isDeleted();
            $record['createdAt'] = $Commento->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Commento->getUpdatedAt()->format('d/m/Y H:i:s');

            $return['data'][] = $record;
        }

        return new JsonResponse($return);

    }


    /**
     * @Route("/news/delete/{id}/{force}", name="news_delete",  requirements={"id" = "\d+"}, defaults={"force" =
     *     false}))
     */
    public
    function deleteAction(
        Request $request,
        News $News
    ) {

        $elemento = '"'.$News->translate('it')->getTitolo().'" ('.$News->getId().')';

        $em = $this->getDoctrine()->getManager();

        if ($News->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }
            }
            // remove the entity
            $em->remove($News);
            $em->flush();

        } elseif (!$News->isDeleted()) {
            $em->remove($News);
            $em->flush();
        }

        $this->addFlash('success', 'News '.$elemento.' eliminata');

        return $this->redirectToRoute('news');

    }

    /**
     * @Route("/news/toggle-commenti-enabled/{id}", name="news_toggle_commenti_enabled")
     */
    public
    function toggleIsEnabledAction(
        Request $request,
        Commento $Commento
    ) {

        $elemento = '"'.$Commento->getNome().'" ('.$Commento->getEmail().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Commento di '.$elemento.' ';

        if ($Commento->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Commento->setIsEnabled(!$Commento->getIsEnabled());

        $em->persist($Commento);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('news_commenti_list', ['id' => $Commento->getNews()->getId()]);


    }


    /**
     * @Route("/news/delete-commento/{id}/{force}", name="news_delete_commento",  requirements={"id" = "\d+"},
     *     defaults={"force" = false}))
     */
    public
    function deleteCommentoAction(
        Request $request,
        Commento $Commento
    ) {

        $elemento = '"'.$Commento->getNome().'" ('.$Commento->getEmail().')';

        $em = $this->getDoctrine()->getManager();

        if ($Commento->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }
            }
            // remove the entity
            $em->remove($Commento);
            $em->flush();

        } elseif (!$Commento->isDeleted()) {
            $em->remove($Commento);
            $em->flush();
        }

        $this->addFlash('success', $elemento.' eliminato');

        return $this->redirectToRoute('news_commenti_list', ['id' => $Commento->getNews()->getId()]);

    }

    /**
     * @Route("/news/restore-commento/{id}", name="news_restore_commento")
     */
    public
    function restoreCommentoAction(
        Request $request,
        Commento $Commento
    ) {

        if ($Commento->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = '"'.$Commento->getNome().' ('.$Commento->getEmail().')';

            $Commento->restore();

            $em->flush();

            $this->addFlash('success', 'Commento di  '.$elemento.' ripristinato');

        }

        return $this->redirectToRoute('news_commenti_list', ['id' => $Commento->getNews()->getId()]);

    }


    /**
     * @Route("/news/restore/{id}", name="news_restore")
     */
    public
    function restoreAction(
        Request $request,
        News $News
    ) {

        if ($News->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = '"'.$News->translate('it')->getTitolo().'" ('.$News->getId().')';

            $News->restore();

            $newsSlugger = $this->get('news_slugger');

            foreach ($News->getTranslations() as $NewsTranslation) {

                /**
                 * @var $NewsTranslation NewsTranslation
                 */
                if ($NewsTranslation->getIsEnabled()) {

                    $newSlug = $newsSlugger->slugify($NewsTranslation, $NewsTranslation->getSlug());

                    if ($NewsTranslation->getSlug() != $newSlug) {
                        $NewsTranslation->setSlug($newSlug);
                    }

                }

            }


            $em->flush();

            $this->addFlash('success', 'Pagina '.$elemento.' ripristinata');

        }

        return $this->redirectToRoute('news_list');

    }


    /**
     * @Route("/news/toggle-enabled/{id}", name="news_toggle_enabled")
     */
    public
    function toggleIsEnabledNewsAction(
        Request $request,
        News $News
    ) {

        $elemento = '"'.$News->translate()->getTitolo().'" ('.$News->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'News '.$elemento.' ';

        if ($News->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $News->setIsEnabled(!$News->getIsEnabled());

        $em->persist($News);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('news');


    }

}
