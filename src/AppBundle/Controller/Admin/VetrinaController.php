<?php
// 09/01/17, 15.36
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Vetrina;
use AppBundle\Entity\VetrinaCategory;
use AppBundle\Form\VetrinaFormType;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_VETRINA')")
 */
class VetrinaController extends Controller
{

    /**
     * @Route("/vetrina", name="vetrina")
     */
    public function listAction()
    {

        return $this->render('admin/vetrina/list.html.twig');


    }

    /**
     * @Route("/vetrina/json", name="vetrina_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $VetrinaList = $em->getRepository('AppBundle:Vetrina')->findAll();
        } else {
            $VetrinaList = $em->getRepository('AppBundle:Vetrina')->findAllNotDeleted();
        }

        $retData = [];

        $vetrine = [];

        foreach ($VetrinaList as $Vetrina) {
            /**
             * @var $Vetrina Vetrina
             */
            $record = [];
            $record['id'] = $Vetrina->getId();
            $record['name'] = $Vetrina->getName();
            $record['deleted'] = $Vetrina->isDeleted();
            $record['isEnabled'] = $Vetrina->getIsEnabled();
            $record['sort'] = $Vetrina->getSort();
            $record['categoria'] = $Vetrina->getCategory()->__toString();
            $record['createdAt'] = $Vetrina->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Vetrina->getUpdatedAt()->format('d/m/Y H:i:s');

            $vetrine[] = $record;
        }

        $retData['data'] = $vetrine;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/vetrina/delete/{id}/{force}", name="vetrina_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Vetrina $Vetrina)
    {

        $elemento = $Vetrina->getName();

        $em = $this->getDoctrine()->getManager();

        if ($Vetrina->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }
            }
            // remove the entity
            $em->remove($Vetrina);
            $em->flush();

        } elseif (!$Vetrina->isDeleted()) {
            $em->remove($Vetrina);
            $em->flush();
        }

        $translator = $this->get('translator');

        $this->addFlash('success', 'Vetrina "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));

        return $this->redirectToRoute('vetrina');

    }

    /**
     * @Route("/vetrina/new", name="vetrina_new")
     * @Route("/vetrina/edit/{id}", name="vetrina_edit")
     */
    public function newEditAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $langs = $this->get('app.languages')->getActiveLanguages();

        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;

        $Vetrina = new Vetrina();

        if ($request->get('id')) {
            $id = $request->get('id');
            $Vetrina = $em->getRepository('AppBundle:Vetrina')->findOneBy(['id' => $id]);

            if (!$Vetrina) {
                return $this->redirectToRoute('vetrina_new');
            }

            if ($Vetrina->getListImgFileName()) {
                $supportData['listImgUrl'] = '/'.$Vetrina->getUploadDir().$Vetrina->getListImgFileName();
            }
        }


        $form = $this->createForm(VetrinaFormType::class, $Vetrina, ['langs' => $langs]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $cancellaListPrecedente = $request->get('vetrina_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($Vetrina, 'listImg');
                $Vetrina->setListImg(null);
                $Vetrina->setListImgAlt('');
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($Vetrina);
            $em->flush();

            $elemento = '"'.$Vetrina->getName().'" ('.$Vetrina->getId().') ';

            $translator = $this->get('translator');

            $this->addFlash('success', 'Vetrina '.$elemento.$translator->trans('default.labels.modificata'));

            return $this->redirectToRoute('vetrina');

        }
        $view = ':admin/vetrina:new.html.twig';

        if ($Vetrina->getId()) {
            $view = ':admin/vetrina:edit.html.twig';
        }

        return $this->render(
            $view,
            ['form' => $form->createView(), 'supportData' => $supportData]
        );

    }

    /**
     * @Route("/vetrina/toggle-enabled/{id}", name="vetrina_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Vetrina $Vetrina)
    {

        $elemento = '"'.$Vetrina->getName().'" ('.$Vetrina->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Vetrina '.$elemento.' ';

        if ($Vetrina->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Vetrina->setIsEnabled(!$Vetrina->getIsEnabled());

        $em->persist($Vetrina);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('vetrina');


    }

    /**
     * @Route("/vetrina/sort", name="vetrina_sort")
     */
    public function sortVetrinaAction()
    {

        $em = $this->getDoctrine()->getManager();

        $trans = $this->get('translator');

        $categorie = [];
        $categorie[0] = $trans->trans('default.labels.select_one');


        $Categories = $em->getRepository('AppBundle:VetrinaCategory')->findAllNotDeleted();

        foreach ($Categories as $VetrinaCategory) {

            /**
             * @var $VetrinaCategory VetrinaCategory
             */
            $categorie[$VetrinaCategory->getId()] = $VetrinaCategory->getNome();
        }

        return $this->render('admin/vetrina/sort.html.twig', ['categorie' => $categorie]);

    }

    /**
     * @Route("/vetrina/getSlides/{id}", name="slides_json")
     */
    public function getSlidesAction(VetrinaCategory $vetrinaCategory)
    {

        $trans = $this->get('translator');

        $return = [];

        if ($vetrinaCategory) {

            $Vetrine = $vetrinaCategory->getVetrine();

            $data = [];
            $data['items'] = [];

            foreach ($Vetrine as $Vetrina) {
                /**
                 * @var $Vetrina Vetrina
                 */
                $label = $Vetrina->getName();
                if (!$Vetrina->getIsEnabled()) {
                    $label .= ' ('.$trans->trans('default.labels.disabilitato').')';
                }
                $record = [];
                $record['id'] = $Vetrina->getId();
                $record['label'] = $label;

                $data['items'][] = $record;


            }
            $return['result'] = true;
            $return['data'] = $data;

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('vetrina.labels.category_not_found')];

        }


        return new JsonResponse($return);

    }

    /**
     * @Route("/vetrina/save-sort", name="save_sort")
     */
    public function saveSortAction(Request $request)
    {

        $trans = $this->get('translator');

        $return = [];
        if ($request->get('sorted')) {

            $em = $this->getDoctrine()->getManager();

            $res = $em->getRepository('AppBundle:Vetrina')->getSortedByIds($request->get('sorted'));

            foreach ($res as $k => $Vetrina) {
                /**
                 * @var $Vetrina Vetrina
                 */

                $Vetrina->setSort($k);
                $em->persist($Vetrina);
            }

            $return['result'] = true;

            $em->flush();

            return new JsonResponse($return);

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);

    }

}