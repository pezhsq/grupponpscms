<?php
// 08/03/17, 9.42
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Settings;
use AppBundle\Form\SettingForm;
use Cocur\Slugify\Slugify;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_SETTINGS')")
 */
class SettaggiController extends Controller
{


    /**
     * @Route("/settaggi", name="settaggi")
     */
    public function settaggiAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $roleHelper = $this->get('app.roles_helper');

        $availableRoles = $roleHelper->getAvailableRoles();

        $settings = $em->getRepository('AppBundle:Settings')->findBy(['requiredRole' => $availableRoles]);

        $gruppi = [];

        foreach ($settings as $setting) {


            if ($setting->getGroup()) {
                $gruppi2[$setting->getGroup()->getId()]['name'] = $setting->getGroup()->getName();

                if (!isset($gruppi[$setting->getGroup()->getId()])) {
                    $gruppi[$setting->getGroup()->getId()] = [];
                    $gruppi[$setting->getGroup()->getId()]['name'] = $setting->getGroup()->getName();
                    $gruppi[$setting->getGroup()->getId()]['settings'] = [];
                }

                $gruppi[$setting->getGroup()->getId()]['settings'][] = $setting;
            }
        }


        if ($request->isMethod('POST')) {

            $errori = [];

            $validator = $this->get('validator');

            $slugger = new Slugify();

            $postData = $request->request->all();

            foreach ($gruppi as $GroupId => $data) {

                $chiaveGruppo = $slugger->slugify($data['name']);

                foreach ($data['settings'] as $Setting) {
                    /**
                     * @var $Setting Settings
                     */
                    if (isset($postData[$chiaveGruppo][$Setting->getChiave()])) {
                        $Setting->setValore($postData[$chiaveGruppo][$Setting->getChiave()]);

                        $errors = $validator->validate($Setting);

                        if (count($errors) > 0) {
                            $errori = array_merge($errori, $errors);
                        } else {
                            $em->persist($Setting);
                        }
                    }
                }
            }

            $em->flush();

            if ($errori) {
                $this->addFlash('error', 'Si sono verificati i seguenti errori:<br />'.implode("<br />", $errori));
            } else {

                $Settings = $em->getRepository('AppBundle:Settings')->findAll();
                $this->get('app.settings_helper')->toYamlFile($Settings);

                $this->addFlash('success', 'I settaggi sono stati salvati');

                $fs = new Filesystem();
                $fs->remove($this->getParameter('kernel.cache_dir'));

            }

        }

        return $this->render('admin/settaggi/settaggi.html.twig', ['gruppi' => $gruppi]);

    }


    /**
     * @Route("/settaggi/form/save/{id}", name="settaggi_save_edit", defaults={"id"=null})
     */
    public function formSave(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $Settings = new Settings();

        if ($id) {
            $Settings = $em->getRepository('AppBundle:Settings')->findOneBy(['id' => $id]);
        }

        $form = $this->createForm(SettingForm::class, $Settings);

        $form->handleRequest($request);

        $return['result'] = false;

        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $Settings = $form->getData();

                $em->persist($Settings);
                $em->flush();

                $return['result'] = true;

                $Settings = $em->getRepository('AppBundle:Settings')->findAll();
                $this->get('app.settings_helper')->toYamlFile($Settings);

            } else {

                $errors = $form->getErrors(true);

                $return['errors'] = [];

                foreach ($errors as $error) {
                    /**
                     * @var $error FormError
                     */
                    $return['errors'][] = $error->getMessage();

                }

            }

        } else {

            $trans = $this->get('translator');
            $return['errors'] = [$trans->trans('default.labels.bad_params')];

        }


        return new JsonResponse($return);


    }

    /**
     * @Route("/settaggi/form", name="settaggi_new")
     * @Route("/settaggi/form/{id}", name="settaggi_edit")
     */
    public function formSettingsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $Settings = null;

        $action = $this->generateUrl('settaggi_save_edit', ['id' => null]);

        if ($request->get('id')) {

            $action = $this->generateUrl('settaggi_save_edit', ['id' => $request->get('id')]);

            $Settings = $em->getRepository('AppBundle:Settings')->findOneBy(['id' => $request->get('id')]);

        }

        $form = $this->createForm(SettingForm::class, $Settings, ['action' => $action]);


        $data = [];
        $data['result'] = true;
        $data['form'] = $this->renderView(
            'admin/settaggi/form.html.twig',
            ['form' => $form->createView(), 'action' => $action]
        );

        return new JsonResponse($data);

    }

    /**
     * @Route("/settaggi/delete/{id}", name="delete_settaggi")
     */
    public function deleteAction(Request $request, Settings $settings)
    {


        $data = [];
        $data['result'] = true;

        if ($settings) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settings);
            $em->flush();

        }


        $Settings = $em->getRepository('AppBundle:Settings')->findAll();
        $this->get('app.settings_helper')->toYamlFile($Settings);

        return new JsonResponse($data);


    }

}