<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 01/12/16
 * Time: 15.16
 */

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\User;
use AppBundle\Form\LoginForm;
use AppBundle\Form\LostPassForm;
use AppBundle\Form\ResetPassForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Uuid;

/**
 * @Route("/admin")
 */
class SecurityController extends Controller
{

    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction()
    {

        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $this->createForm(LoginForm::class, ['_username' => $lastUsername]);

        return $this->render(
            'admin/security/login.html.twig',
            [
                'form'  => $form->createView(),
                'error' => $error,
            ]
        );
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {

        throw new \Exception('qui non ci devo arrivare');

    }

    /**
     * @Route("/lost-admin-pass", name="lost_password")
     */
    public function lostPassAction(Request $request)
    {

        $data  = null;
        $error = false;

        $form = $this->createForm(LostPassForm::class);

        $form->handleRequest($request);

        $router = $this->container->get('router');

        if ($form->isSubmitted() && $form->isValid()) {

            $email = $request->get('lost_pass_form')['email'];

            $em = $this->getDoctrine()->getManager();

            /**
             * @var User;
             */
            $User = $em->getRepository('AppBundle:User')->findOneBy(['email' => $email]);


            if ($User && $User->getIsEnabled()) {

                $link = $router->getContext()->getScheme().'://'.$router->getContext()->getHost();

                if ($router->getContext()->getHttpPort() !== 80) {
                    $link .= ':'.$router->getContext()->getHttpPort();
                }

                $link .= $router->generate('operatori_reset_pass');
                $link .= '?uuid='.$User->getId().'&code='.md5($User->getUpdatedAt()->format('d/m/Y H:i:s'));

                $message = \Swift_Message::newInstance()
                    ->setSubject('Recupero password')
                    ->setFrom($this->getParameter('mailer_from'))
                    ->setTo($User->getEmail())
                    ->setBody(
                        $this->renderView(
                            'Emails/recover_pass.html.twig',
                            ['nome' => $User->getNome(), 'link' => $link]
                        ),
                        'text/html'
                    );

                $this->get('mailer')->send($message);

                $this->addFlash('success', 'operatori.messages.email_reset_inviata');

            } else {
                $error = 'operatori.labels.user_non_trovato';
            }

        }

        return $this->render(
            'admin/security/lost_pass.html.twig',
            [
                'form'  => $form->createView(),
                'error' => $error,
            ]
        );

    }

    /**
     * @Route("/reset-pass", name="operatori_reset_pass")
     */
    public function resetPassAction(Request $request)
    {

        $error = false;

        $form = $this->createForm(
            ResetPassForm::class,
            [
                'uuid' => $request->query->get('uuid'),
                'code' => $request->query->get('code'),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $User = $em->getRepository('AppBundle:User')->findOneBy(['id' => $data['uuid']]);

            if ($User && md5($User->getUpdatedAt()->format('d/m/Y H:i:s')) == $data['code']) {

                $User->setPlainPassword($data['plainPassword']);

                $em->persist($User);
                $em->flush();

                $this->addFlash('success', $this->get('translator')->trans('operatori.messages.password_resettata'));

                return $this->get('security.authentication.guard_handler')
                    ->authenticateUserAndHandleSuccess(
                        $User,
                        $request,
                        $this->get('app.security.login_form_authenticator'),
                        'main'
                    );


            } else {

                $this->addFlash('error', 'operatori.errors.form_manomesso');

                return $this->redirectToRoute('lost_password');

            }


        }

        $uuidConstraint = new Uuid();
        $errors         = $this->get('validator')->validate($request->query->get('uuid'), $uuidConstraint);


        if (!count($errors)) {

            $em = $this->getDoctrine()->getManager();

            $User = $em->getRepository('AppBundle:User')->findOneBy(['id' => $request->query->get('uuid')]);

            if ($User && md5($User->getUpdatedAt()->format('d/m/Y H:i:s')) == $request->query->get('code')) {
                return $this->render(
                    'admin/security/reset_pass.html.twig',
                    [
                        'form'  => $form->createView(),
                        'error' => $error,
                    ]
                );


            } else {

                $this->addFlash('error', 'operatori.errors.url_manomesso');

                return $this->redirectToRoute('lost_password');

            }

        } else {
            $this->addFlash('error', 'operatori.errors.url_manomesso');

            return $this->redirectToRoute('lost_password');
        }


    }

}