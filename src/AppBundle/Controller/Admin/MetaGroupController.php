<?php
// 08/03/17, 8.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\MetaGroup;
use AppBundle\Form\MetaGroupForm;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_GRUPPI_META')")
 */
class MetaGroupController extends Controller
{

    /**
     * @Route("/meta-group", name="gruppi_meta")
     */
    public function listAction()
    {

        return $this->render('admin/gruppi_meta/list.html.twig');

    }

    /**
     * @Route("/meta-group/json", name="gruppi_meta_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $MetaGroupList = $em->getRepository('AppBundle:MetaGroup')->findAll();
        } else {
            $MetaGroupList = $em->getRepository('AppBundle:MetaGroup')->findAllNotDeleted();
        }

        $retData = [];

        $MetaGroups = [];

        foreach ($MetaGroupList as $MetaGroup) {
            /**
             * @var $MetaGroup MetaGroup
             */
            $record              = [];
            $record['id']        = $MetaGroup->getId();
            $record['name']      = $MetaGroup->getName();
            $record['deleted']   = $MetaGroup->isDeleted();
            $record['createdAt'] = $MetaGroup->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $MetaGroup->getUpdatedAt()->format('d/m/Y H:i:s');

            $MetaGroups[] = $record;
        }

        $retData['data'] = $MetaGroups;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/meta-group/new", name="gruppi_meta_new")
     * @Route("/meta-group/edit/{id}", name="gruppi_meta_edit")
     */
    public function newAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $MetaGroup = null;


        if ($request->get('id')) {

            $MetaGroup = $em->getRepository('AppBundle:MetaGroup')->findOneBy(['id' => $request->get('id')]);

        }

        $translator = $this->get('translator');

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(MetaGroupForm::class, $MetaGroup);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $MetaGroup MetaGroup
             */
            $MetaGroup = $form->getData();

            $em->persist($MetaGroup);
            $em->flush();

            $elemento = $MetaGroup->getName().' ('.$MetaGroup->getId().') ';

            $this->addFlash('success', 'Gruppo meta '.$elemento.$translator->trans('default.labels.creato'));

            return $this->redirectToRoute('gruppi_meta');

        }


        return $this->render('admin/gruppi_meta/new.html.twig', ['form' => $form->createView()]);

    }


    /**
     * @Route("/meta-group/delete/{id}/{force}", name="gruppi_meta_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, MetaGroup $MetaGroups)
    {

        $elemento = $MetaGroups->getName();

        $em = $this->getDoctrine()->getManager();

        if ($MetaGroups->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }
            }
            // remove the entity
            $em->remove($MetaGroups);
            $em->flush();

        } elseif (!$MetaGroups->isDeleted()) {
            $em->remove($MetaGroups);
            $em->flush();
        }

        $translator = $this->get('translator');

        $this->addFlash('success', 'Gruppo meta "'.$elemento.'" '.$translator->trans('default.labels.eliminato'));

        return $this->redirectToRoute('gruppi_meta');

    }

    /**
     * @Route("/meta-group/restore/{id}", name="gruppi_meta_restore")
     */
    public function restoreAction(Request $request, MetaGroup $MetaGroups)
    {

        if ($MetaGroups->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $MetaGroups->getName().' ('.$MetaGroups->getId().')';

            $MetaGroups->restore();


            $em->flush();

            $this->addFlash('success', 'Gruppo settaggi '.$elemento.' ripristinato');

        }

        return $this->redirectToRoute('gruppi_meta');

    }

}