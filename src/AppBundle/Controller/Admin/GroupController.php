<?php

namespace AppBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Group;
use AppBundle\Form\GroupForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_SUPER_ADMIN')")
 */
class GroupController extends Controller
{

    /**
     * @Route("/group", name="group")
     */
    public function listAction()
    {

        return $this->render(':admin/groups:list.html.twig');

    }

    /**
     * @Route("/group/json", name="group_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.services.group_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }


    /**
     * @Route("/group/new", name="group_new")
     * @Route("/group/edit/{id}",  name="group_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $translator = $this->get('translator');

        $em = $this->getDoctrine()->getManager();

        $Group = new Group();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Group = $em->getRepository('AppBundle:Group')->findOneBy(['id' => $id]);

            if (!$Group) {
                return $this->redirectToRoute('group_new');
            }

        }

        $form = $this->createForm(
            GroupForm::class,
            $Group
        );

        $form->handleRequest($request);

        $errors = false;

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $GroupGroup
             */

            $Group = $form->getData();

            $new = false;

            if (!$Group->getId()) {
                $new = true;
            }

            $em->persist($Group);
            $em->flush();

            $elemento = $Group->getName();

            $em->persist($Group);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Group "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Group "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }

            $GroupHelper = $this->get('app.services.group_helper')->writeSecurity();
            $fs = new Filesystem();
            $fs->remove($this->getParameter('kernel.cache_dir'));


            return $this->redirectToRoute('group');

        } else {

            $errors = $this->get('app.form_error_helper')->getErrors($form);

        }

        $view = ':admin/groups:new.html.twig';

        if ($Group->getId()) {
            $view = ':admin/groups:edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView(), 'errors' => $errors]);


    }

    /**
     * @Route("/group/toggle-enabled/{id}", name="group_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Group $Group)
    {

        $elemento = '"'.$Group->getName().'" ('.$Group->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Group '.$elemento.' ';

        if ($Group->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Group->setIsEnabled(!$Group->getIsEnabled());

        $em->persist($Group);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('group');


    }

    /**
     * @Route("/group/delete/{id}/{force}", name="group_delete",  requirements={"id" = "\d+"}, defaults={"force" =
     *     false}))
     */
    public function deleteAction(Request $request, Group $Group)
    {

        if ($Group) {

            $elemento = $Group->getName();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Group->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Group);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Group "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Group "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Group->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Group "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($Group);
                $em->flush();


            }

        }

        return $this->redirectToRoute('group');

    }

    /**
     * @Route("/group/restore/{id}", name="group_restore")
     */
    public function restoreAction(Request $request, Group $Group)
    {

        if ($Group->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Group->getName().' ('.$Group->getId().')';

            $Group->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Group "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('group');

    }

    /**
     * @Route("/group/sort", name="group_sort")
     */
    public function sortAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $trans = $this->get('translator');

        $gruppi = [];

        $Groups = $em->getRepository('AppBundle:Group')->findBy([], ['level' => 'ASC']);

        foreach ($Groups as $group) {

            /**
             * @var $group Group
             */
            $gruppi[$group->getId()] = $group->getName();
        }

        return $this->render('admin/groups/sort.html.twig', ['gruppi' => $gruppi]);

    }

    /**
     * @Route("/group/save-sort", name="group_save_sort")
     */
    public function saveSortAction(Request $request)
    {

        $trans = $this->get('translator');

        $return = [];

        if ($request->get('sorted')) {

            $em = $this->getDoctrine()->getManager();

            $res = $em->getRepository('AppBundle:Group')->getSortedByIds($request->get('sorted'));

            foreach ($res as $k => $Group) {
                /**
                 * @var $Group Group
                 */

                $Group->setLevel($k);
                $em->persist($Group);
            }

            $return['result'] = true;

            $em->flush();

            return new JsonResponse($return);

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);

    }

}