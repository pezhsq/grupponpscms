<?php
// 11/05/17, 11.44
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AppBundle\Controller\Admin;

use AppartamentiBundle\Entity\AppartamentoAttachment;
use AppBundle\Entity\Attachment;
use AppBundle\Entity\NewsAttachment;
use AppBundle\Entity\Page;
use AppBundle\Entity\PageAttachment;
use AziendaBundle\Entity\DipendenteAttachment;
use Doctrine\ORM\EntityManager;
use Guzzle\Http\Message\Response;
use IperautoBundle\Entity\MezzoAttachment;
use IperautoBundle\Entity\ModelliNuovoAttachment;
use OfferteBundle\Entity\OffertaAttachment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webtek\EcommerceBundle\Entity\ProductAttachment;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_USER')")
 */
class AttachmentsController extends Controller
{

    private function getAttachment($type, $id)
    {

        $em = $this->getDoctrine();
        switch ($type) {
            case 'appartamenti':
                $attachment = $em->getRepository('AppartamentiBundle:AppartamentoAttachment')->findOneBy(
                    ['id' => $id]
                );
                break;
            case 'dipendenti':
                $attachment = $em->getRepository('AziendaBundle:DipendenteAttachment')->findOneBy(
                    ['id' => $id]
                );
                break;
            case 'offerte':
                $attachment = $em->getRepository('OfferteBundle:OffertaAttachment')->findOneBy(
                    ['id' => $id]
                );
                break;
            case 'pages':
                $attachment = $em->getRepository('AppBundle:PageAttachment')->findOneBy(
                    ['id' => $id]
                );
                break;
            case 'news':
                $attachment = $em->getRepository('AppBundle:NewsAttachment')->findOneBy(
                    ['id' => $id]
                );
                break;
            case 'product':
                $attachment = $em->getRepository('WebtekEcommerceBundle:ProductAttachment')->findOneBy(
                    ['id' => $id]
                );
                break;
        }

        return $attachment;

    }

    /**
     * Route di upload
     *
     * Il/I file caricati vengono riconosciuti in base al $typeOfObject ricevuto dalla request, se l'oggetto di
     * riferimento non ha ancora un id viene generato un token univoco che deve essere opportunamente passato dal form
     * dell'entity. Una volta salvato l'oggetto un listener dovrà spostare i file dalla cartella temporanea e metterli
     * nella giusta posizione.
     *
     * @param Request $request
     * @return JsonResponse
     * @Route("/_attachments/{typeofobject}/upload/{type}", defaults={"type" = false})
     */
    public function upload(Request $request)
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHelper = $this->get('app.image_helper');
        $vichHelper = $this->get('vich_uploader.templating.helper.uploader_helper');
        $files = $request->files->get('files');
        $typeOfObject = $request->get('typeofobject');
        $attachment = null;
        $parentEntity = null;
        $return = [];
        $return['uploaded'] = [];
        foreach ($files as $k => $file) {

            switch ($typeOfObject) {
                case 'appartamenti':
                    $attachment = new AppartamentoAttachment();
                    if (is_numeric($request->get('id'))) {
                        $parentEntity = $em->getRepository('AppartamentiBundle:Appartamento')->findOneBy(
                            ['id' => $request->get('id')]
                        );
                    }
                    break;
                case 'pages':
                    $attachment = new PageAttachment();
                    if (is_numeric($request->get('id'))) {
                        $parentEntity = $em->getRepository('AppBundle:Page')->findOneBy(
                            ['id' => $request->get('id')]
                        );
                    }
                    break;
                case 'dipendenti':
                    $attachment = new DipendenteAttachment();
                    if (is_numeric($request->get('id'))) {
                        $parentEntity = $em->getRepository('AziendaBundle:Dipendente')->findOneBy(
                            ['id' => $request->get('id')]
                        );
                    }
                    break;
                case 'offerte':
                    $attachment = new OffertaAttachment();
                    if (is_numeric($request->get('id'))) {
                        $parentEntity = $em->getRepository('OfferteBundle:Offerta')->findOneBy(
                            ['id' => $request->get('id')]
                        );
                    }
                    break;
                case 'news':
                    $attachment = new NewsAttachment();
                    if (is_numeric($request->get('id'))) {
                        $parentEntity = $em->getRepository('AppBundle:News')->findOneBy(
                            ['id' => $request->get('id')]
                        );
                    }
                    break;
                case 'product':
                    $attachment = new ProductAttachment();
                    if (is_numeric($request->get('id'))) {
                        $parentEntity = $em->getRepository('WebtekEcommerceBundle:Product')->findOneBy(
                            ['id' => $request->get('id')]
                        );
                    }
                    break;
            }
            $attachment->setFile($file);
            if (!is_numeric($request->get('id'))) {
                $attachment->setToken($request->get('id'));
            } else {
                $attachment->setParent($parentEntity);
            }
            $attachment->setSize($file->getSize());
            foreach ($langs as $l => $longLocale) {
                if ($request->get('type') && $request->get('type') == 'legacy') {
                    $alt = $translator->trans('pages.labels.alt_default');
                } else {
                    $alt = $request->get('fileAlt')[$k][$l];
                }
                $attachment->translate($l)->setAlt($alt);
            }
            $attachment->setType($file->guessExtension());
            $em->persist($attachment);
            $attachment->mergeNewTranslations();
            $em->flush();
            // preparo i dati da ritornare
            $uploaded = [];
            $uploaded['filename'] = $attachment->getName();
            $uploaded['alt'] = [];
            $uploaded['id'] = $attachment->getId();
            $uploaded['path'] = $helper->asset($attachment, 'file');
            foreach ($langs as $l => $longLocale) {
                $uploaded['alt'][$l] = $attachment->translate($l)->getAlt();
            }
            if (in_array($attachment->getType(), ['jpeg', 'jpg', 'gif', 'png'])) {
                $uploaded['isImage'] = true;
                $imageHelper->compress($this->get('app.web_dir')->get().$helper->asset($attachment, 'file'));
            } else {
                $uploaded['isImage'] = false;
            }
            $uploaded['type'] = $attachment->getType();
            $return['uploaded'][] = $uploaded;

        }
        $return['result'] = true;

        return new JsonResponse($return, 200, ['Content-Type' => "text/html"]);
    }

    /**
     * @Route("/_attachments/{typeofobject}/delete_attachment/{id}", name="delete_attachment")
     */
    public function deleteAttachment(Request $request)
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        $attachment = $this->getAttachment($request->get('typeofobject'), $request->get('id'));
        $return = [];
        $return['result'] = true;
        $em = $this->getDoctrine()->getManager();
        $em->remove($attachment);
        $em->flush();

        return new JsonResponse($return);

    }

    /**
     * @Route("/_attachments/{typeofobject}/save_alt_attachment/{id}")
     */
    public function saveAltAttachment(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $return = [];
        $return['result'] = true;
        $return['errors'] = [];
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        $typeOfObject = $request->get('typeofobject');
        $attachment = $this->getAttachment($typeOfObject, $request->get('id'));
        $Alt = $request->get('fileAlt')[$attachment->getId()];
        foreach ($langs as $l => $longLocale) {
            if (!$Alt[$l] || ($l == 'it' && $Alt[$l] == $translator->trans('default.labels.alt_default'))) {
                $return['errors'][] = sprintf(
                    $translator->trans('default.errors.empty_alt_for_file'),
                    $attachment->getName(),
                    $longLocale
                );
                $return['result'] = false;
            } else {
                $attachment->translate($l, false)->setAlt($Alt[$l]);
            }
        }
        if ($return['result']) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($attachment);
            $em->flush();
        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/_attachments/{typeofobject}/get-images/{id}", defaults={"type" = false})
     */
    public function getImages(Request $request)
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $typeOfObject = $request->get('typeofobject');
        $id = $request->get('id');
        switch ($typeOfObject) {
            case 'pages':
                $repo = $em->getRepository('AppBundle:PageAttachment');
                break;
            case 'news':
                $repo = $em->getRepository('AppBundle:NewsAttachment');
                break;
            case 'dipendenti':
                $repo = $em->getRepository('AziendaBundle:DipendenteAttachment');
                break;
            case 'offerte':
                $repo = $em->getRepository('OfferteBundle:OffertaAttachment');
                break;
            case 'product':
                $repo = $em->getRepository('WebtekEcommerceBundle:ProductAttachment');
                break;
        }
        $attachments = $repo->findBy(['parent_id' => $id]);
        $data = [];
        foreach ($attachments as $attachment) {
            if (in_array($attachment->getType(), ['jpeg', 'png', 'gif', 'jpg'])) {
                $record = [];
                $record['id'] = $attachment->getId();
                $record['path'] = $helper->asset($attachment, 'file');
                $record['alt'] = $attachment->translate()->getAlt();
                $data[] = $record;

            }
        }
        $return = [];
        $return['result'] = true;
        $return['data'] = $data;

        return new JsonResponse($return);


    }

    /**
     * @Route("/_attachments/{typeofobject}/save-attachments-order", name="save_attachment_order")
     */
    public function saveAttachmentOrder(Request $request)
    {

        $return = [];
        $return['result'] = true;
        $em = $this->getDoctrine()->getManager();
        $typeOfObject = $request->get('typeofobject');
        $id = $request->get('id');
        switch ($typeOfObject) {
            case 'appartamenti':
                $repo = $em->getRepository('AppartamentiBundle:AppartamentoAttachment');
                break;
            case 'news':
                $repo = $em->getRepository('AppBundle:NewsAttachment');
                break;
            case 'pages':
                $repo = $em->getRepository('AppBundle:PageAttachment');
                break;
            case 'dipendenti':
                $repo = $em->getRepository('AziendaBundle:DipendenteAttachment');
                break;
            case 'offerte':
                $repo = $em->getRepository('OfferteBundle:OffertaAttachment');
                break;
            case 'product':
                $repo = $em->getRepository('WebtekEcommerceBundle:ProductAttachment');
                break;
        }
        $res = $repo->getSortedByIds($request->request->get('ids'));
        foreach ($res as $k => $Attachment) {
            /**
             * @var $Attachment Attachment
             */
            $Attachment->setSort($k);
            $em->persist($Attachment);
        }
        $return['result'] = true;
        $em->flush();

        return new JsonResponse($return);

    }


}