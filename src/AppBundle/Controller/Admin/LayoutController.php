<?php
// 01/02/17, 15.03
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_TEMPLATE')")
 */
class LayoutController extends Controller {

    /**
     * @Route("/layout", name="layout")
     */
    public function layoutAction(Request $request) {

        $Trans          = $this->get('translator');
        $StyleGenerator = $this->get('app.assets_manager');

        $fileVariabili = $this->get('kernel')->getRootDir().
            '/'.'Resources'.
            '/'.'views'.
            '/'.'public'.
            '/'.'layouts'.
            '/'.$this->getParameter('generali')['layout'].
            '/'.'styles'.
            '/'.'common'.
            '/'.'_variables.scss';

        $righeFile = file($fileVariabili);

        $re = '/^\$([^:]+)\:(.*);/';

        $data = [];

        foreach($righeFile as $riga) {
            if(preg_match($re, $riga, $m)) {
                $record               = [];
                $record['key']        = $m[1];
                $record['value']      = $m[2];
                $record['utilizzato'] = implode(', ', $StyleGenerator->searchKey('$'.$record['key']));

                $data[] = $record;
            }
        }

        // return
        $return = [];
        $riga   = [];
        foreach($data as $record) {
            if(count($riga) == 2) {
                $return[] = $riga;
                $riga     = [];
            }
            $riga[] = $record;

        }

        if($riga) {
            $return[] = $riga;
        }


        if($request->isMethod('POST')) {

            $file = [];

            foreach($_POST['data'] as $k => $v) {

                $file[] = '$'.$k.':'.$v.';';

            }

            file_put_contents($fileVariabili, implode("\n", $file));

            $result = $StyleGenerator->compile();

            $this->addFlash('success', $Trans->trans('layout.messages.salvataggio_ok'));

            return $this->redirectToRoute('layout');

        }


        return $this->render('admin/layout/edit.html.twig', ['data' => $return]);

    }

}