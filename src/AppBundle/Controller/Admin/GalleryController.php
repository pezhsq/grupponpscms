<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\GalleryItems;
use AppBundle\Form\GalleryItemForm;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use MatthiasMullie\Minify\JS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Gallery;
use AppBundle\Form\GalleryForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_GALLERY')")
 */
class GalleryController extends Controller
{

    /**
     * @Route("/gallery", name="gallery")
     */
    public function listAction()
    {

        return $this->render(':admin/gallery:list.html.twig');

    }

    /**
     * @Route("/gallery/json", name="gallery_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.services.gallery_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/gallery/images/{id}", name="gallery_edit_images")
     */
    public function managePictures(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) {

            $id = $request->get('id');

            $Gallery = $em->getRepository('AppBundle:Gallery')->findOneBy(['id' => $id]);

            if (!$Gallery) {
                return $this->redirectToRoute('gallery');
            }

            return $this->render(
                ':admin/gallery:images.html.twig',
                ['gallery' => $Gallery]
            );

        }

        return $this->redirectToRoute('gallery');

    }

    /**
     * @Route("/gallery/upload", name="gallery_upload")
     */
    public function upload(Request $request)
    {

        $GalleryHelper = $this->get('app.services.gallery_helper');
        $translator = $this->get('translator');

        $result = $GalleryHelper->upload($request->files, $request->request->get('id'));

        if ($result === true) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
            $return['errors'] = [$translator->trans($result)];
        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/gallery/delete-img/{id}", name="gallery_item_delete")
     */
    public function delete(Request $request)
    {

        $translator = $this->get('translator');

        $return['result'] = false;

        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) {

            $id = $request->get('id');

            $GalleryItem = $em->getRepository('AppBundle:GalleryItems')->findOneBy(['id' => $id]);

            if ($GalleryItem) {
                $em->remove($GalleryItem);
                $em->flush();
                $return['result'] = true;
            } else {
                $return['errors'] = [$translator->trans('default.labels.non_trovato')];
            }

        } else {
            $return['errors'] = [$translator->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/gallery/save-alt", name="gallery_item_save_alt")
     */
    public function saveGalleryItem(Request $request)
    {

        $return['result'] = false;

        if ($request->get('gallery_item_form')['id']) {

            $langs = $this->get('app.languages')->getActiveLanguages();
            $em = $this->getDoctrine()->getManager();

            $GalleryItems = $em->getRepository('AppBundle:GalleryItems')->findOneBy(
                ['id' => $request->get('gallery_item_form')['id']]
            );

            if ($GalleryItems) {

                $form = $this->createForm(
                    GalleryItemForm::class,
                    $GalleryItems,
                    ['langs' => $langs]
                );

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                    $GalleryItems = $form->getData();

                    $em->persist($GalleryItems);
                    $em->flush();

                    $return['result'] = true;

                } else {


                    $return['result'] = false;
                    $return['errors'] = $this->get('app.form_error_helper')->getErrors($form, 'array');

                }

            } else {
                $trans = $this->get('translator');
                $return['errors'] = [$trans->trans('default.labels.bad_params')];

            }

        } else {

            $trans = $this->get('translator');
            $return['errors'] = [$trans->trans('default.labels.bad_params')];

        }

        return new JsonResponse($return);
    }

    /**
     * @Route("/gallery/edit-img/{id}", name="gallery_edit_img")
     */
    public function editGalleryItem(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();

        $translator = $this->get('translator');

        $return['result'] = false;

        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) {

            $id = $request->get('id');

            $GalleryItem = $em->getRepository('AppBundle:GalleryItems')->findOneBy(['id' => $id]);

            if ($GalleryItem) {

                $form = $this->createForm(
                    GalleryItemForm::class,
                    $GalleryItem,
                    ['langs' => $langs]
                );

                $form->handleRequest($request);

                $errors = [];


                return $this->render(
                    ':admin/gallery:altForm.html.twig',
                    ['form' => $form->createView()]
                );

            } else {
                $return['errors'] = [$translator->trans('default.labels.non_trovato')];
            }

        } else {
            $return['errors'] = [$translator->trans('default.labels.bad_params')];
        }


    }

    /**
     * @Route("/gallery/images-list/{id}", name="gallery_get_images")
     */
    public function getPictures(Request $request)
    {

        $return = [];
        $return['result'] = false;

        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) {

            $id = $request->get('id');

            $Gallery = $em->getRepository('AppBundle:Gallery')->findOneBy(['id' => $id]);

            if ($Gallery) {

                $return['result'] = true;
                $return['items'] = $this->get('app.services.gallery_helper')->getImages($Gallery);;


            }


        }

        return new JsonResponse($return);

    }


    /**
     * @Route("/gallery/new", name="gallery_new")
     * @Route("/gallery/edit/{id}",  name="gallery_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $Gallery = new Gallery();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Gallery = $em->getRepository('AppBundle:Gallery')->findOneBy(['id' => $id]);

            if (!$Gallery) {
                return $this->redirectToRoute('gallery_new');
            }

        }

        $form = $this->createForm(
            GalleryForm::class,
            $Gallery,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        $errors = [];

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Gallery Gallery
             */

            $Gallery = $form->getData();

            $new = false;

            if (!$Gallery->getId()) {
                $new = true;
            }

            $em->persist($Gallery);
            $em->flush();

            $elemento = $Gallery->translate($request->getLocale())->getTitolo();

            $em->persist($Gallery);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'Gallery "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Gallery "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('gallery');

        } else {

            $errors = $this->get('app.form_error_helper')->getErrors($form);

        }

        $view = ':admin/gallery:new.html.twig';

        if ($Gallery->getId()) {
            $view = ':admin/gallery:edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView(), 'errors' => $errors]);


    }

    /**
     * @Route("/gallery/toggle-enabled/{id}", name="gallery_toggle_enabled")
     */
    public
    function toggleIsEnabledAction(
        Request $request,
        Gallery $Gallery
    ) {

        $elemento = '"'.$Gallery->translate()->getTitolo().'" ('.$Gallery->getId().')';

        $em = $this->getDoctrine()->getManager();

        $flash = 'Gallery '.$elemento.' ';

        if ($Gallery->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $Gallery->setIsEnabled(!$Gallery->getIsEnabled());

        $em->persist($Gallery);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('gallery');


    }

    /**
     * @Route("/gallery/delete/{id}/{force}", name="gallery_delete",  requirements={"id" = "\d+"}, defaults={"force" =
     *     false}))
     */
    public
    function deleteAction(
        Request $request,
        Gallery $Gallery
    ) {

        if ($Gallery) {

            $elemento = $Gallery->translate($request->getLocale())->getTitolo();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Gallery->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Gallery);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Gallery "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Gallery "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Gallery->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'Gallery "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($Gallery);
                $em->flush();


            }

        }

        return $this->redirectToRoute('gallery');

    }

    /**
     * @Route("/gallery/restore/{id}", name="gallery_restore")
     */
    public
    function restoreAction(
        Request $request,
        Gallery $Gallery
    ) {

        if ($Gallery->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Gallery->translate($request->getLocale())->getTitolo().' ('.$Gallery->getId().')';

            $Gallery->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'Gallery "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('gallery');

    }

    /**
     * @Route("/gallery/sort", name="gallery_sort")
     */
    public
    function sortAction(
        Request $request
    ) {

        $em = $this->getDoctrine()->getManager();

        $trans = $this->get('translator');

        $gallerie = [];

        $Galleries = $em->getRepository('AppBundle:Gallery')->findAllNotDeleted();

        foreach ($Galleries as $Gallery) {

            /**
             * @var $Gallery Gallery
             */
            $gallerie[$Gallery->getId()] = $Gallery->translate($request->getLocale())->getTitolo();
        }

        return $this->render('admin/gallery/sort.html.twig', ['gallerie' => $gallerie]);

    }

    /**
     * @Route("/gallery/save-sort", name="gallery_save_sort")
     */
    public function saveSortAction(Request $request)
    {

        $trans = $this->get('translator');

        $return = [];

        if ($request->get('ids')) {

            $em = $this->getDoctrine()->getManager();

            $res = $em->getRepository('AppBundle:GalleryItems')->getSortedByIds($request->get('ids'));

            foreach ($res as $k => $GalleryItem) {
                /**
                 * @var $GalleryItem GalleryItems
                 */

                $GalleryItem->setSort($k);
                $em->persist($GalleryItem);
            }

            $return['result'] = true;

            $em->flush();

            return new JsonResponse($return);

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);

    }

}