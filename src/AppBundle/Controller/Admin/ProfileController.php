<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 12/12/16
 * Time: 15.39
 */

namespace AppBundle\Controller\Admin;


use AppBundle\Form\ProfileEditFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_USER')")
 */
class ProfileController extends Controller
{

    /**
     * @Route("/profile", name="operatori_profile")
     */
    public function profileAction(Request $request)
    {

        $supportData = [];
        $supportData['listImgUrl'] = false;

        $User = $this->getUser();

        if ($User->getListImgFileName()) {
            $supportData['listImgUrl'] = '/'.$User->getUploadDir().$User->getListImgFileName();
        }

        $form = $this->createForm(ProfileEditFormType::class, $User);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var User $User
             */
            $User = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $cancellaListPrecedente = $request->get('profile_edit_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($User, 'listImg');
                $User->setListImg(null);
            }


            $em->persist($User);
            $em->flush();


            $elemento = $User->getNome().' '.$User->getCognome().' ('.$User->getId().')';

            $trans = $this->get('translator');

            $this->addFlash('success', $trans->trans('operatori.messages.profilo_aggiornato'));

            return $this->redirectToRoute('dashboard');

        }

        return $this->render(
            'admin/operatori/profile.html.twig',
            ['form' => $form->createView(), 'supportData' => $supportData]
        );

    }


}