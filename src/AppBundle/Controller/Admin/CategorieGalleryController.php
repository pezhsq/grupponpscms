<?php
// 09/01/17, 16.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\GalleryCategory;
use AppBundle\Entity\NewsCategory;
use AppBundle\Form\CategorieGalleryForm;
use AppBundle\Form\CategorieNewsForm;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormConfigInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_CATNEWS')")
 */
class CategorieGalleryController extends Controller
{

    /**
     * @Route("/gallerycat", name="gallerycat")
     */
    public function listAction()
    {

        return $this->render('admin/gallerycat/list.html.twig');

    }

    /**
     * @Route("/gallerycat/json", name="gallerycat_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.services.gallerycat_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/gallerycat/toggle-enabled/{id}", name="gallerycat_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, GalleryCategory $galleryCategory)
    {

        $em = $this->getDoctrine()->getManager();

        $translator = $this->get('translator');

        $elemento = '"'.$galleryCategory->translate($request->getLocale())->getTitolo().'" ('.$galleryCategory->getId(
            ).')';

        $flash = 'Categoria '.$elemento.' ';

        if ($galleryCategory->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }

        $galleryCategory->setIsEnabled(!$galleryCategory->getIsEnabled());

        $em->persist($galleryCategory);
        $em->flush();

        $this->addFlash('success', $flash);

        return $this->redirectToRoute('gallerycat');


    }


    /**
     * @Route("/gallerycat/new", name="gallerycat_new")
     * @Route("/gallerycat/edit/{id}", name="gallerycat_edit")
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');

        $em = $this->getDoctrine()->getManager();

        $Category = new GalleryCategory();

        $id = null;

        $supportData = [];
        $supportData['listImgUrl'] = false;
        $supportData['headerImgUrl'] = false;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Category = $em->getRepository('AppBundle:GalleryCategory')->findOneBy(['id' => $id]);

            if (!$Category) {
                return $this->redirectToRoute('gallerycat_new');
            }

            if ($Category->getListImgFileName()) {
                $supportData['listImgUrl'] = '/'.$Category->getUploadDir().$Category->getListImgFileName();
            }
            if ($Category->getHeaderImgFileName()) {
                $supportData['headerImgUrl'] = '/'.$Category->getUploadDir().$Category->getHeaderImgFileName();
            }

        }

        $form = $this->createForm(
            CategorieGalleryForm::class,
            $Category,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        $errors = false;

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $Category GalleryCategory
             */

            $Category = $form->getData();

            $new = false;

            if (!$Category->getId()) {
                $new = true;
            }

            $cancellaHeaderPrecedente = $request->get('categorie_gallery_form')['headerImgDelete'];
            if ($cancellaHeaderPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($Category, 'headerImg');
                $Category->setHeaderImg(null);
                $Category->setHeaderImgAlt('');
            }

            $cancellaListPrecedente = $request->get('categorie_gallery_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($Category, 'listImg');
                $Category->setListImg(null);
                $Category->setListImgAlt('');
            }

            $em->persist($Category);
            $em->flush();

            $elemento = $Category->translate($request->getLocale())->getTitolo();


            if (!$new) {
                $this->addFlash(
                    'success',
                    'Categoria "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }


            return $this->redirectToRoute('gallerycat');

        } else {

            $trans = $this->get('translator');

            $errors = $this->get('app.form_error_helper')->getErrors($form);

        }

        $view = ':admin/gallerycat:new.html.twig';

        if ($Category->getId()) {

            $view = ':admin/gallerycat:edit.html.twig';
        }

        return $this->render(
            $view,
            ['form' => $form->createView(), 'supportData' => $supportData, 'errors' => $errors]
        );


    }

    /**
     * @Route("/gallerycat/restore/{id}", name="gallerycat_restore")
     */
    public function restoreAction(Request $request, GalleryCategory $galleryCategory)
    {

        if ($galleryCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = '"'.$galleryCategory->translate($request->getLocale())->getTitolo(
                ).'" ('.$galleryCategory->getId().')';

            $galleryCategory->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria '.$elemento.' '.$translator->trans('default.labels.ripristinata'));

        }

        return $this->redirectToRoute('gallerycat');

    }


    /**
     * @Route("/gallerycat/delete/{id}/{force}", name="gallerycat_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, GalleryCategory $galleryCategory)
    {

        $elemento = '"'.$galleryCategory->translate($request->getLocale())->getTitolo().'" ('.$galleryCategory->getId(
            ).')';

        $translator = $this->get('translator');


        $em = $this->getDoctrine()->getManager();
        if ($galleryCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                foreach ($listeners as $listener) {
                    if ($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }

            }

            // remove the entity
            $em->remove($galleryCategory);

            try {

                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));


            } catch (ForeignKeyConstraintViolationException $e) {

                $this->addFlash(
                    'error',
                    'Categoria "'.$elemento.'" '.$translator->trans('news.errors.non_cancellabile')
                );

            }


        } elseif (!$galleryCategory->isDeleted()) {

            $galleryCategory->setIsEnabled(0);
            $em->flush();

            $em->persist($galleryCategory);

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));

            $em->remove($galleryCategory);
            $em->flush();


        }


        return $this->redirectToRoute('gallerycat');

    }

    /**
     * @Route("/gallerycat/sort", name="gallerycat_sort")
     */
    public function sortAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $trans = $this->get('translator');

        $categorie = [];

        $Categories = $em->getRepository('AppBundle:GalleryCategory')->findAllNotDeleted();

        foreach ($Categories as $GalleryCat) {

            /**
             * @var $GalleryCat GalleryCategory
             */
            $categorie[$GalleryCat->getId()] = $GalleryCat->translate($request->getLocale())->getTitolo();
        }

        return $this->render('admin/gallerycat/sort.html.twig', ['categorie' => $categorie]);

    }

    /**
     * @Route("/gallerycat/save-sort", name="gallerycat_save_sort")
     */
    public function saveSortAction(Request $request)
    {

        $trans = $this->get('translator');

        $return = [];

        if ($request->get('sorted')) {

            $em = $this->getDoctrine()->getManager();

            $res = $em->getRepository('AppBundle:GalleryCategory')->getSortedByIds($request->get('sorted'));

            foreach ($res as $k => $Category) {
                /**
                 * @var $Category NewsCategory
                 */

                $Category->setSort($k);
                $em->persist($Category);
            }

            $return['result'] = true;

            $em->flush();

            return new JsonResponse($return);

        } else {
            $return['result'] = false;
            $return['errors'] = [$trans->trans('default.labels.bad_params')];
        }

        return new JsonResponse($return);

    }

}
