<?php
// 08/03/17, 8.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\SettingsGroup;
use AppBundle\Form\SettingsGroupForm;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_SETTINGS')")
 */
class SettingsGroupController extends Controller {

    /**
     * @Route("/gruppi-settaggi", name="gruppi_settaggi")
     */
    public function listAction() {

        return $this->render('admin/gruppi_settaggi/list.html.twig');

    }

    /**
     * @Route("/gruppi-settaggi/json", name="gruppi_settaggi_list_json")
     */
    public function listJson(Request $request) {

        $em = $this->getDoctrine()->getManager();

        if($this->isGranted('ROLE_RESTORE_DELETED')) {
            $SettingsGroupList = $em->getRepository('AppBundle:SettingsGroup')->findAll();
        } else {
            $SettingsGroupList = $em->getRepository('AppBundle:SettingsGroup')->findAllNotDeleted();
        }

        $retData = [];

        $settingsGroup = [];

        foreach($SettingsGroupList as $SettingsGroup) {
            /**
             * @var $SettingsGroup SettingsGroup
             */
            $record              = [];
            $record['id']        = $SettingsGroup->getId();
            $record['name']      = $SettingsGroup->getName();
            $record['deleted']   = $SettingsGroup->isDeleted();
            $record['createdAt'] = $SettingsGroup->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $SettingsGroup->getUpdatedAt()->format('d/m/Y H:i:s');

            $settingsGroup[] = $record;
        }

        $retData['data'] = $settingsGroup;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/gruppi-settaggi/new", name="gruppi_settaggi_new")
     * @Route("/gruppi-settaggi/edit/{id}", name="gruppi_settaggi_edit")
     */
    public function newAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $SettingsGroup = null;


        if($request->get('id')) {

            $SettingsGroup = $em->getRepository('AppBundle:SettingsGroup')->findOneBy(['id' => $request->get('id')]);

        }

        $translator = $this->get('translator');

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(SettingsGroupForm::class, $SettingsGroup);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $SettingsGroup SettingsGroup
             */
            $SettingsGroup = $form->getData();

            $em->persist($SettingsGroup);
            $em->flush();

            $elemento = $SettingsGroup->getName().' ('.$SettingsGroup->getId().') ';

            $this->addFlash('success', 'Gruppo settaggi '.$elemento.$translator->trans('default.labels.creato'));

            return $this->redirectToRoute('gruppi_settaggi');

        }


        return $this->render('admin/gruppi_settaggi/new.html.twig', ['form' => $form->createView()]);

    }


    /**
     * @Route("/gruppi-settaggi/delete/{id}/{force}", name="gruppi_settaggi_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, SettingsGroup $settingsGroup) {

        $elemento = $settingsGroup->getName();

        $em = $this->getDoctrine()->getManager();

        if($settingsGroup->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach($em->getEventManager()->getListeners() as $eventName => $listeners) {
                foreach($listeners as $listener) {
                    if($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }
            }
            // remove the entity
            $em->remove($settingsGroup);
            $em->flush();

        } elseif(!$settingsGroup->isDeleted()) {
            $em->remove($settingsGroup);
            $em->flush();
        }

        $translator = $this->get('translator');

        $this->addFlash('success', 'Gruppo settaggi "'.$elemento.'" '.$translator->trans('default.labels.eliminato'));

        return $this->redirectToRoute('gruppi_settaggi');

    }

    /**
     * @Route("/gruppi-settaggi/restore/{id}", name="gruppi_settaggi_restore")
     */
    public function restoreAction(Request $request, SettingsGroup $settingsGroup) {

        if($settingsGroup->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $settingsGroup->getName().' ('.$settingsGroup->getId().')';

            $settingsGroup->restore();


            $em->flush();

            $this->addFlash('success', 'Gruppo settaggi '.$elemento.' ripristinato');

        }

        return $this->redirectToRoute('gruppi_settaggi');

    }

}