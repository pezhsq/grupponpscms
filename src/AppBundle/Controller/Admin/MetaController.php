<?php
// 24/05/17, 15.03
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Meta;
use AppBundle\Form\MetaForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_META')")
 */
class MetaController extends Controller
{

    /**
     * @Route("/meta", name="meta")
     */
    public function metaAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $MetaList = $em->getRepository('AppBundle:Meta')->findAll();

        $gruppi = [];

        foreach ($MetaList as $Meta) {


            if ($Meta->getGroup()) {
                $gruppi2[$Meta->getGroup()->getId()]['name'] = $Meta->getGroup()->getName();

                if (!isset($gruppi[$Meta->getGroup()->getId()])) {
                    $gruppi[$Meta->getGroup()->getId()]         = [];
                    $gruppi[$Meta->getGroup()->getId()]['name'] = $Meta->getGroup()->getName();
                    $gruppi[$Meta->getGroup()->getId()]['meta'] = [];
                }

                $gruppi[$Meta->getGroup()->getId()]['meta'][] = $Meta;
            }
        }

        
        return $this->render(':admin/meta:meta.html.twig', ['gruppi' => $gruppi]);

    }


    /**
     * @Route("/meta/form/save/{id}", name="meta_save_edit", defaults={"id"=null})
     */
    public function formSave(Request $request, $id)
    {

        $em    = $this->getDoctrine()->getManager();
        $langs = $this->get('app.languages')->getActiveLanguages();

        $Meta = new Meta();

        if ($id) {
            $Meta = $em->getRepository('AppBundle:Meta')->findOneBy(['id' => $id]);
        }

        $form = $this->createForm(MetaForm::class, $Meta, ['langs' => $langs]);

        $form->handleRequest($request);

        $return['result'] = false;

        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $Meta = $form->getData();

                $em->persist($Meta);
                $em->flush();

                $return['result'] = true;

            } else {

                $errors = $form->getErrors(true);

                $return['errors'] = [];

                foreach ($errors as $error) {
                    /**
                     * @var $error FormError
                     */
                    $return['errors'][] = $error->getMessage();

                }

            }

        } else {

            $trans            = $this->get('translator');
            $return['errors'] = [$trans->trans('default.labels.bad_params')];

        }


        return new JsonResponse($return);


    }

    /**
     * @Route("/meta/form", name="meta_new")
     * @Route("/meta/form/{id}", name="meta_edit")
     */
    public function formMetaAction(Request $request)
    {

        $em    = $this->getDoctrine()->getManager();
        $langs = $this->get('app.languages')->getActiveLanguages();

        $Meta = null;

        $action = $this->generateUrl('meta_save_edit', ['id' => null]);

        if ($request->get('id')) {

            $action = $this->generateUrl('meta_save_edit', ['id' => $request->get('id')]);

            $Meta = $em->getRepository('AppBundle:Meta')->findOneBy(['id' => $request->get('id')]);

        }


        $form = $this->createForm(MetaForm::class, $Meta, ['action' => $action, 'langs' => $langs]);

        $data           = [];
        $data['result'] = true;
        $data['form']   = $this->renderView(
            'admin/meta/form.html.twig',
            ['form' => $form->createView(), 'action' => $action]
        );

        return new JsonResponse($data);

    }

    /**
     * @Route("/meta/delete/{id}", name="delete_meta")
     */
    public function deleteAction(Request $request, Settings $Meta)
    {


        $data           = [];
        $data['result'] = true;

        if ($Meta) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($Meta);
            $em->flush();

        }


        $Meta = $em->getRepository('AppBundle:Settings')->findAll();
        $this->get('app.settings_helper')->toYamlFile($Meta);

        return new JsonResponse($data);


    }

}