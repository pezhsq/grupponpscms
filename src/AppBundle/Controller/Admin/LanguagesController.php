<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 16/12/16
 * Time: 11.26
 */

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Language;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_LANGUAGES')")
 */
class LanguagesController extends Controller {

    /**
     * @Route("/languages", name="languages_list")
     */
    public function listAction() {

        return $this->render('admin/languages/list.html.twig');

    }

    /**
     * @Route("/languages/json", name="languages_list_json")
     */
    public function listJson() {

        $em = $this->getDoctrine()->getManager();

        $Languages = $em->getRepository('AppBundle:Language')->findAll();

        $retData = [];

        $languages = [];

        foreach ($Languages as $Language) {
            /**
             * @var $Language Language;
             */
            $record = [];
            $record['shortCode'] = $Language->getShortCode();
            $record['languageName'] = $Language->getLanguageName();
            $record['locale'] = $Language->getLocale();
            $record['isEnabled'] = $Language->getIsEnabled();
            $record['isEnabledPublic'] = $Language->getIsEnabledPublic();

            $languages[] = $record;
        }

        $retData['data'] = $languages;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/languages/toggle-enabled/{shortCode}", name="languages_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, Language $Language) {

        $elemento = $Language->getLanguageName() . ' (' . $Language->getShortCode() . ')';

        $em = $this->getDoctrine()->getManager();

        $flash = $elemento . ' ';

        $Translator = $this->get('translator');

        if ($Language->getIsEnabled()) {


            if ($em->getRepository('AppBundle:Language')->countActiveLanguages() > 1) {

                $flash .= $Translator->trans('default.labels.disabilitato');

                $Language->setIsEnabled(!$Language->getIsEnabled());

                $em->persist($Language);
                $em->flush();

                $this->addFlash('success', $flash);

            } else {
                $this->addFlash('error', $Translator->trans('languages.messages.cant_disable_unique_language'));
            }

        } else {

            $Language->setIsEnabled(!$Language->getIsEnabled());

            $em->persist($Language);
            $em->flush();

            $flash .= $Translator->trans('default.labels.abilitato');

            $this->addFlash('success', $flash);
        }

        return $this->redirectToRoute('languages_list');


    }

    /**
     * @Route("/languages/toggle-enabled-public/{shortCode}", name="languages_toggle_enabled_public")
     */
    public function toggleIsEnabledPublicAction(Request $request, Language $Language) {

        $elemento = $Language->getLanguageName() . ' (' . $Language->getShortCode() . ')';

        $em = $this->getDoctrine()->getManager();

        $flash = $elemento . ' ';

        $Translator = $this->get('translator');

        if ($Language->getIsEnabledPublic()) {

            if ($em->getRepository('AppBundle:Language')->countActiveLanguagesPublic() > 1) {

                $flash .= $Translator->trans('languages.messages.disabilitato_public');

                $Language->setIsEnabledPublic(!$Language->getIsEnabledPublic());

                $em->persist($Language);
                $em->flush();

                $this->addFlash('success', $flash);

            } else {
                $this->addFlash('error', $Translator->trans('languages.messages.cant_disable_unique_language'));
            }

        } else {

            $Language->setIsEnabledPublic(!$Language->getIsEnabledPublic());
            $Language->setIsEnabled(1);

            $em->persist($Language);
            $em->flush();

            $flash .= $Translator->trans('languages.messages.abilitato_public');

            $this->addFlash('success', $flash);
        }

        return $this->redirectToRoute('languages_list');


    }

}