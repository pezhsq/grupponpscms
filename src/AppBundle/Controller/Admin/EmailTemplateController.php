<?php

namespace AppBundle\Controller\Admin;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\EmailTemplate;
use AppBundle\Form\EmailTemplateForm;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_EMAILTEMPLATE')")
 */
class EmailTemplateController extends Controller
{

    /**
     * @Route("/email-template", name="email_template")
     */
    public function listAction()
    {

        return $this->render(':admin/email_template:list.html.twig');

    }

    /**
     * @Route("/email-template/json", name="email_template_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.services.email_template_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/email-template/new", name="email_template_new")
     * @Route("/email-template/edit/{id}",  name="email_template_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');
        $imageHandler = $this->get('app.base64_image');

        $em = $this->getDoctrine()->getManager();

        $EmailTemplate = new EmailTemplate();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $EmailTemplate = $em->getRepository('AppBundle:EmailTemplate')->findOneBy(['id' => $id]);

            if (!$EmailTemplate) {
                return $this->redirectToRoute('email_template_new');
            }

        }

        $form = $this->createForm(
            EmailTemplateForm::class,
            $EmailTemplate,
            ['langs' => $langs]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $EmailTemplate EmailTemplate
             */

            $EmailTemplate = $form->getData();

            $new = false;

            if (!$EmailTemplate->getId()) {
                $new = true;
            }

            $em->persist($EmailTemplate);
            $em->flush();

            $elemento = $EmailTemplate->translate($request->getLocale())->getSubject();

            $em->persist($EmailTemplate);
            $em->flush();

            if (!$new) {
                $this->addFlash(
                    'success',
                    'EmailTemplate "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash(
                    'success',
                    'EmailTemplate "'.$elemento.'" '.$translator->trans('default.labels.creato')
                );
            }


            return $this->redirectToRoute('email_template');

        }

        $view = ':admin/email_template:new.html.twig';

        if ($EmailTemplate->getId()) {
            $view = ':admin/email_template:edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);


    }

    /**
     * @Route("/email-template/delete/{id}/{force}", name="email_template_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, EmailTemplate $EmailTemplate)
    {

        if ($EmailTemplate) {

            $elemento = $EmailTemplate->translate($request->getLocale())->getSubject();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($EmailTemplate->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($EmailTemplate);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'EmailTemplate "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'EmailTemplate "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$EmailTemplate->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash(
                    'success',
                    'EmailTemplate "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                );

                $em->remove($EmailTemplate);
                $em->flush();


            }

        }

        return $this->redirectToRoute('email_template');

    }

    /**
     * @Route("/email-template/restore/{id}", name="email_template_restore")
     */
    public function restoreAction(Request $request, EmailTemplate $EmailTemplate)
    {

        if ($EmailTemplate->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $EmailTemplate->translate($request->getLocale())->getSubject().' ('.$EmailTemplate->getId().')';

            $EmailTemplate->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash(
                'success',
                'EmailTemplate "'.$elemento.'" '.$translator->trans('default.labels.ripristinata')
            );

        }

        return $this->redirectToRoute('email_template');

    }

}