<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 02/12/16
 * Time: 15.18
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use AppBundle\Form\UserEditFormType;
use AppBundle\Form\UserForm;
use AppBundle\Form\UserFormType;
use AppBundle\Form\UserNewFormType;
use AppBundle\Form\UserRegistrationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_USERS')")
 */
class UserController extends Controller
{


    /**
     * @Route("/operatori", name="operatori_list")
     */
    public function listAction()
    {

        return $this->render('admin/operatori/list.html.twig');

    }

    /**
     * @Route("/operatori/json", name="operatori_list_json")
     */
    public function listJson()
    {

        $em = $this->getDoctrine()->getManager();
        /** @var User $me */
        $me = $this->getUser();
        $Users = $em->getRepository('AppBundle:User')->findAllButUser($me);
        $allowedToSwitch = $this->get('security.authorization_checker')->isGranted('ROLE_ALLOWED_TO_SWITCH');
        $retData = [];
        $operatori = [];
        $Gruppi = $em->getRepository('AppBundle:Group')->findAll();
        $gruppiCache = [];
        $gruppiCache['ROLE_SUPER_ADMIN'] = 'Super Admin';
        foreach ($Gruppi as $Gruppo) {
            /**
             * @var $Gruppo Group
             */
            $gruppiCache[$Gruppo->getRole()] = $Gruppo->getName();

        }
        foreach ($Users as $User) {

            /**
             * @var $User User
             */
            if ($User->getUsername() !== 'system') {

                $record = [];
                $record['id'] = $User->getId();
                if ($User->getListImgFileName()) {
                    $record['avatar'] = '/'.$User->getUploadDir().$User->getListImgFileName();
                } else {
                    $record['avatar'] = '/media/users/user.png';
                }
                $record['nome'] = $User->getNome();
                $record['cognome'] = $User->getCognome();
                $record['username'] = $User->getUsername();
                if (isset($gruppiCache[$User->getRole()])) {
                    $record['gruppo'] = $gruppiCache[$User->getRole()];
                } else {
                    $record['gruppo'] = 'Error!';
                }
                $record['email'] = $User->getEmail();
                $record['isEnabled'] = $User->getIsEnabled();
                $record['createdAt'] = $User->getCreatedAt()->format('d/m/Y H:i:s');
                $record['updatedAt'] = $User->getUpdatedAt()->format('d/m/Y H:i:s');
                $record['allowedToSwitch'] = $allowedToSwitch && $User->getIsEnabled();
                $operatori[] = $record;

            }
        }
        $retData['data'] = $operatori;

        return new JsonResponse($retData);

    }


    /**
     * @Route("/operatori/new", name="operatori_new")
     * @Route("/operatori/edit/{id}",  name="operatori_edit")
     */
    public function newEditAction(Request $request)
    {

        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();
        $User = new User();
        $id = null;
        $supportData = [];
        $supportData['listImgUrl'] = false;
        if ($request->get('id')) {

            $id = $request->get('id');
            $User = $em->getRepository('AppBundle:User')->findOneBy(['id' => $id]);
            if (!$User) {
                return $this->redirectToRoute('operatori_new');
            }
            if ($User->getListImgFileName()) {
                $supportData['listImgUrl'] = '/'.$User->getUploadDir().$User->getListImgFileName();
            }

        }
        $form = $this->createForm(
            UserForm::class,
            $User
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $User User
             */
            $User = $form->getData();
            $new = false;
            if (!$User->getId()) {
                $new = true;
            }
            $cancellaListPrecedente = $request->get('user_form')['listImgDelete'];
            if ($cancellaListPrecedente) {
                $this->get('vich_uploader.upload_handler')->remove($User, 'listImg');
                $User->setListImg(null);
            }
            $em->persist($User);
            $em->flush();
            $elemento = $User->getNome();
            if (!$new) {
                $this->addFlash(
                    'success',
                    'User "'.$elemento.'" '.$translator->trans('default.labels.modificato')
                );
            } else {
                $this->addFlash('success', 'User "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }

            return $this->redirectToRoute('operatori_list');

        }
        $view = ':admin/operatori:new.html.twig';
        if ($User->getId()) {
            $view = ':admin/operatori:edit.html.twig';
        }

        return $this->render(
            $view,
            ['form' => $form->createView(), 'supportData' => $supportData]
        );


    }

    /**
     * @Route("/operatori/delete/{id}", name="operatori_delete")
     */
    public function deleteAction(Request $request, User $User)
    {

        $elemento = $User->getNome().' '.$User->getCognome().' ('.$User->getId().')';
        $imageHandler = $this->get('app.base64_image');
        $imageHandler->saveTo('', $User, $User->getUsername().'.jpg', 1);
        $em = $this->getDoctrine()->getManager();
        $em->remove($User);
        $em->flush();
        $this->addFlash('success', 'Operatore '.$elemento.' eliminato');

        return $this->redirectToRoute('operatori_list');

    }

    /**
     * @Route("/operatori/toggle-enabled/{id}", name="operatori_toggle_enabled")
     */
    public function toggleIsEnabledAction(Request $request, User $User)
    {

        $elemento = $User->getNome().' '.$User->getCognome().' ('.$User->getId().')';
        $em = $this->getDoctrine()->getManager();
        $flash = 'Operatore '.$elemento.' ';
        if ($User->getIsEnabled()) {
            $flash .= 'disabilitato';
        } else {
            $flash .= 'abilitato';
        }
        $User->setIsEnabled(!$User->getIsEnabled());
        $em->persist($User);
        $em->flush();
        $this->addFlash('success', $flash);

        return $this->redirectToRoute('operatori_list');


    }

}