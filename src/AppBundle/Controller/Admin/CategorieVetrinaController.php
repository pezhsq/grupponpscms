<?php
// 09/01/17, 16.35
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\VetrinaCategory;
use AppBundle\Form\CategorieVetrinaForm;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_VETRINA_SUPER')")
 */
class CategorieVetrinaController extends Controller {

    /**
     * @Route("/vetrinacat", name="vetrinacat")
     */
    public function listAction() {

        return $this->render('admin/vetrinacat/list.html.twig');

    }

    /**
     * @Route("/vetrinacat/new", name="vetrinacat_new")
     */
    public function newAction(Request $request) {

        $form = $this->createForm(CategorieVetrinaForm::class);

        $form->handleRequest($request);

        $VetrinaCategory = new VetrinaCategory();

        if($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $VetrinaCategory VetrinaCategory
             */

            $VetrinaCategory = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($VetrinaCategory);
            $em->flush();

            $elemento = $VetrinaCategory->getNome();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.creata'));

            return $this->redirectToRoute('vetrinacat');

        }

        return $this->render('admin/vetrinacat/new.html.twig', ['vetrinaCatForm' => $form->createView()]);

    }

    /**
     * @Route("/vetrinacat/edit/{id}", name="vetrinacat_edit")
     */
    public function editAction(Request $request, VetrinaCategory $vetrinaCategory) {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CategorieVetrinaForm::class, $vetrinaCategory);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($vetrinaCategory);
            $em->flush();

            $elemento = $vetrinaCategory->getNome();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.modificata'));

            return $this->redirectToRoute('vetrinacat');

        }

        return $this->render('admin/vetrinacat/new.html.twig', ['vetrinaCatForm' => $form->createView()]);

    }


    /**
     * @Route("/vetrinacat/json", name="vetrinacat_list_json")
     */
    public function listJson(Request $request) {

        $em = $this->getDoctrine()->getManager();

        if($this->isGranted('ROLE_RESTORE_DELETED')) {
            $CategorieVetrinaList = $em->getRepository('AppBundle:VetrinaCategory')->findAll();
        } else {
            $CategorieVetrinaList = $em->getRepository('AppBundle:VetrinaCategory')->findAllNotDeleted();
        }

        $retData = [];

        $vetrine = [];

        foreach($CategorieVetrinaList as $VetrinaCategory) {
            /**
             * @var $VetrinaCategory VetrinaCategory
             */
            $record               = [];
            $record['id']         = $VetrinaCategory->getId();
            $record['nome']       = $VetrinaCategory->getNome();
            $record['dimensioni'] = $VetrinaCategory->getDimensioni();
            $record['deleted']    = $VetrinaCategory->isDeleted();
            $record['createdAt']  = $VetrinaCategory->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt']  = $VetrinaCategory->getUpdatedAt()->format('d/m/Y H:i:s');

            $vetrine[] = $record;
        }

        $retData['data'] = $vetrine;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/vetrinacat/restore/{id}", name="vetrinacat_restore")
     */
    public function restoreAction(Request $request, VetrinaCategory $vetrinaCategory) {

        if($vetrinaCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $vetrinaCategory->getNome().' ('.$vetrinaCategory->getId().')';

            $vetrinaCategory->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.ripristinata'));

        }

        return $this->redirectToRoute('vetrinacat');

    }


    /**
     * @Route("/vetrinacat/delete/{id}/{force}", name="vetrinacat_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, VetrinaCategory $vetrinaCategory) {

        $elemento = $vetrinaCategory->getNome();

        $translator = $this->get('translator');


        $em = $this->getDoctrine()->getManager();

        if($vetrinaCategory->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

            // initiate an array for the removed listeners
            $originalEventListeners = [];

            // cycle through all registered event listeners
            foreach($em->getEventManager()->getListeners() as $eventName => $listeners) {

                foreach($listeners as $listener) {
                    if($listener instanceof SoftDeletableSubscriber) {

                        // store the event listener, that gets removed
                        $originalEventListeners[$eventName] = $listener;

                        // remove the SoftDeletableSubscriber event listener
                        $em->getEventManager()->removeEventListener($eventName, $listener);
                    }
                }

            }

            // remove the entity
            $em->remove($vetrinaCategory);

            try {

                $em->flush();

                $translator = $this->get('translator');

                $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));


            } catch(ForeignKeyConstraintViolationException $e) {

                $this->addFlash('error', 'Categoria "'.$elemento.'" '.$translator->trans('vetrina.errors.non_cancellabile'));

            }


        } elseif(!$vetrinaCategory->isDeleted()) {

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));

            $em->remove($vetrinaCategory);
            $em->flush();


        }


        return $this->redirectToRoute('vetrinacat');

    }

}