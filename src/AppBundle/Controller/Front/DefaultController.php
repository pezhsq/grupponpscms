<?php
// 26/01/17, 9.47
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Front;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{


    /**
     * @Route("/{_locale}/{slug}", requirements={"_locale" = "de|en|fr|jp|nl|pt|ru|zh|cs|pl|es", "slug"=".+"}, name="page")
     * @Route("/{slug}", defaults={"_locale"="it"}, requirements={"slug"=".+"}, name="page_it")
     */
    public function pageAction(Request $request)
    {

        if ($request->attributes->has('slug')) {

            if ($request->get('slug') == 'home') {

                $route = 'home';

                if ($request->getLocale() == 'it') {
                    $route .= '_it';
                }

                return $this->redirectToRoute($route);
            }

            $pageRetriever = $this->get('app.page_retriever');

            /**
             * var $Page Page
             */
            $Page = $pageRetriever->retrievePage($request->get('slug'), $request->getLocale());

            if ($Page) {


                $Languages = $this->get('app.languages');

                $TemplateLoader = $this->get('app.template_loader');


                $AdditionalData = [];
                $AdditionalData['Entity'] = $Page;
                $AdditionalData['langs'] = $Languages->getActivePublicLanguages();

                $META = [];
                $META['title'] = $Page->translate($request->getLocale())->getMetaTitle();
                $META['description'] = $Page->translate($request->getLocale())->getMetaDescription();
                $META['robots'] = $Page->getRobots();
                $META['alternate'] = [];

                $pagesUrlGenerator = $this->get('app.page_url_generator');

                foreach ($AdditionalData['langs'] as $sigla => $estesa) {
                    if ($Page->translate($sigla)->getIsEnabled()) {
                        $META['alternate'][$sigla] = $pagesUrlGenerator->generaUrl($Page, $sigla, true);
                    }
                }

                $AdditionalData['META'] = $META;

                $twigs = $TemplateLoader->getTwigs(
                    $Page->getTemplate(),
                    $Languages->getActivePublicLanguages(),
                    $AdditionalData
                );


                return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

            }

        }

        throw new NotFoundHttpException("Pagina non trovata");

    }
}
