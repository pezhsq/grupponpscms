<?php
// 03/03/17, 11.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Front;


use AppBundle\Entity\Commento;
use AppBundle\Entity\News;
use AppBundle\Form\CommentoForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class CommentoController extends Controller
{

    /**
     * @Route("/commenta/{id}", defaults={"_locale"="it"}, name="commenta_it")
     * @Route("/{_locale}/commenta/{id}", requirements={"_locale" = "de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="commenta")
     */
    public function indexController(Request $request)
    {

        $Commento = new Commento();

        $form = $this->createForm(CommentoForm::class);

        $trans = $this->get('translator');

        $form->handleRequest($request);

        $return           = [];
        $return['result'] = false;
        $return['errors'] = [$trans->trans('default.labels.undefined_error')];

        if ($form->isSubmitted()) {

            foreach ($request->request->all() as $k => $v) {
                if (preg_match('/.*_wt$/', $k)) {
                    if ($v) {
                        throw new HttpException(400, 'Richiesta non valida');
                    }
                }
            }

            if ($form->isValid()) {

                $return['result'] = true;
                $return['errors'] = [];
                /**
                 * @var $Commento Commento
                 */
                $Commento = $form->getData();

                $Commento->setIsEnabled(false);

                $em = $this->getDoctrine()->getManager();
                $em->persist($Commento);
                $em->flush();

                /**
                 * @var $News News;
                 */
                $News = $Commento->getNews();

                $data['email']     = $Commento->getEmail();
                $data['nome']      = $Commento->getNome();
                $data['messaggio'] = $Commento->getMessaggio();
                $data['News']      = $News->translate($request->getLocale())->getTitolo();
                $data['createdAt'] = $Commento->getCreatedAt()->format('d-m-Y');
                $data['NewsUrl']   = $this->get('app.path_manager')->generateUrl('blog_read_news',
                    [
                        'slug'        => $News->getPrimaryCategory()->translate($request->getLocale())->getSlug(),
                        'slugnotizia' => $News->translate($request->getLocale())->getSlug(),
                    ],
                    $request->getLocale(),
                    true
                );


                $message = \Swift_Message::newInstance()
                    ->setSubject('Nuovo commento')
                    ->setFrom($this->getParameter('emails')['default_from'], 'Webtek CMS')
                    ->setTo($this->getParameter('emails')['destinatario_contatti'])
                    ->setBody(
                        $this->renderView(
                            'public/email-commenti.twig',
                            ['data' => $data]
                        ),
                        'text/html'
                    );

                $this->get('mailer')->send($message);


                return new JsonResponse($return);

            } else {

                $errors = $form->getErrors(true);

                $return['errors'] = [];

                foreach ($errors as $error) {
                    /**
                     * @var $error FormError
                     */
                    $return['errors'][] = $error->getMessage();

                }


                return new JsonResponse($return);

            }

        }

        return new JsonResponse($return);


    }

}