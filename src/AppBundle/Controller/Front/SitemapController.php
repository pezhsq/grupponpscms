<?php
// 17/02/17, 10.58
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Front;

use AppartamentiBundle\Entity\Appartamento;
use AppartamentiBundle\Entity\AppartamentoAttachment;
use AppBundle\Entity\News;
use AppBundle\Entity\Page;
use AppBundle\Service\PagesUrlGenerator;
use AppBundle\Service\PathManager;
use OfferteBundle\Entity\Offerta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SitemapController extends Controller
{

    /**
     * @var $pathManager PathManager
     */
    private $pathManager;
    /**
     * @var $request Request;
     */
    private $request;
    /**
     * @var $pagesUrlGenerator PagesUrlGenerator
     */
    private $pagesUrlGenerator;

    private $imageCache;

    /**
     * @Route("/sitemap.xml")
     */
    public function sitemap(Request $request)
    {

        $this->pathManager = $this->get('app.path_manager');
        $this->request = $request;
        $this->pagesUrlGenerator = $this->get('app.page_url_generator');

        $this->getModulesImages();

        $urls = [];
        $urls = array_merge($urls, $this->getUrlPagine());
        $urls = array_merge($urls, $this->getUrlBlog());
        $urls = array_merge($urls, $this->getUrlAppartamenti());
        $urls = array_merge($urls, $this->getUrlOfferte());
        $urls = array_merge($urls, $this->getUrlCategorie());
        $urls = array_merge($urls, $this->getUrlProdotti());

        $response = new Response($this->renderView('public/sitemap.twig', ['urls' => $urls]));
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');

        return $response;


    }


    private function getUrlProdotti()
    {
        $bundles = $this->getParameter('kernel.bundles');

        $urls = [];
        if (isset($bundles['WebtekEcommerceBundle'])) {

            $em = $this->getDoctrine()->getManager();

            $ProductList = $em->getRepository('WebtekEcommerceBundle:Product')->findBy(['isEnabled' => 1, "deletedAt" => null]);

            $Languages = $this->get('app.languages');

            $langs = $Languages->getActivePublicLanguages();


            foreach ($ProductList as $Product) {
                /**
                 * @var $Product Product
                 */
                foreach ($langs as $sigla => $lang) {

                    $params = ['slug' => $Product->translate($sigla)->getSlug()];

                    $record = [];

                    $record['loc'] = $this->request->getSchemeAndHttpHost() . $this->pathManager->generateUrl(
                            'prodotto_ecommerce',
                            $params,
                            $sigla
                        );

                    $record['lastmod'] = $Product->getUpdatedAt()->format('Y-m-d');

                    $record['changefreq'] = 'monthly';
                    $record['priority'] = '0.8';
                    $record['images'] = [];

                    if ($sigla == 'it') {
                        if ($Product->getListImgFileName()) {
                            $image = [];
                            $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $Product->getUploadDir() . $Product->getListImgFileName();
                            $image['caption'] = $Product->getListImgAlt();
                            $record['images'][] = $image;
                        }


                        $attachments = $Product->getAttachments();

                        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');


                        foreach ($attachments as $attachment) {
                            /**
                             * @var $attachment ProductAttachment
                             */
                            if (in_array($attachment->getType(), ['jpeg', 'jpg', 'png', 'gif'])) {
                                $image = [];
                                $image['loc'] = $this->request->getSchemeAndHttpHost() . $helper->asset(
                                        $attachment,
                                        'file'
                                    );
                                $image['caption'] = $attachment->translate()->getAlt();
                                $record['images'][] = $image;
                            }
                        }
                    }


                    $urls[] = $record;

                }


            }

        }
        return $urls;
    }

    private function getUrlCategorie()
    {
        $bundles = $this->getParameter('kernel.bundles');

        $urls = [];
        if (isset($bundles['WebtekEcommerceBundle'])) {

            $em = $this->getDoctrine()->getManager();

            $CategoryList = $em->getRepository('WebtekEcommerceBundle:Category')->findBy(['isEnabled' => 1]);

            $Languages = $this->get('app.languages');

            $langs = $Languages->getActivePublicLanguages();


            foreach ($CategoryList as $Category) {
                /**
                 * @var $Category Category
                 */
                if ($Category->getMaterializedPath() == "")
                    continue;
                foreach ($langs as $sigla => $lang) {

                    $params = ['slug' => $Category->translate($sigla)->getSlug()];

                    $record = [];

                    $categoryHelper = $this->get("app.webtek_ecommerce.services.category_helper");

                    $record['loc'] = $categoryHelper->generaUrl($Category, $sigla, [], true);;

                    $record['lastmod'] = $Category->getUpdatedAt()->format('Y-m-d');

                    $record['changefreq'] = 'monthly';
                    $record['priority'] = '0.8';
                    $record['images'] = [];

                    if ($sigla == 'it') {
                        if ($Category->getCategoryImgFileName()) {
                            $image = [];
                            $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $Category->getUploadDir() . $Category->getHeaderImgFileName();
                            $image['caption'] = $Category->getCategoryImgFileNameAlt();
                            $record['images'][] = $image;
                        }

//                        if ($Category->getListImgFileName()) {
//                            $image = [];
//                            $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $Category->getUploadDir() . $Category->getListImgFileName();
//                            $image['caption'] = $Category->getListImgAlt();
//                            $record['images'][] = $image;
//                        }
//
//                        $attachments = $Category->getAttachments();

//                        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');


//                        foreach ($attachments as $attachment) {
//                            /**
//                             * @var $attachment AppartamentoAttachment
//                             */
//                            if (in_array($attachment->getType(), ['jpeg', 'jpg', 'png', 'gif'])) {
//                                $image = [];
//                                $image['loc'] = $this->request->getSchemeAndHttpHost() . $helper->asset(
//                                        $attachment,
//                                        'file'
//                                    );
//                                $image['caption'] = $attachment->translate()->getAlt();
//                                $record['images'][] = $image;
//                            }
//                        }
                    }


                    $urls[] = $record;

                }


            }

        }
        return $urls;
    }

    private function getUrlPagine()
    {

        $em = $this->getDoctrine()->getManager();

        $Pages = $em->getRepository('AppBundle:Page')->findAllNotDeleted(true);

        $record = [];
        $record['loc'] = '';
        $record['lastmod'] = '';
        $record['changefreq'] = '';
        $record['priority'] = '';
        $record['images'] = [];

        $imageRecord = [];
        $imageRecord['loc'] = '';
        $imageRecord['caption'] = '';

        $urls = [];

        $Languages = $this->get('app.languages');

        $langs = $Languages->getActivePublicLanguages();

        foreach ($Pages as $Page) {
            /**
             * @var $Page Page
             */
            if ($Page->getTemplate() !== '404') {

                $urlLocalizzati = [];

                foreach ($langs as $sigla => $estesa) {
                    if ($Page->translate($sigla)->getIsEnabled()) {
                        $urlLocalizzati[$sigla] = $this->request->getSchemeAndHttpHost() . $this->pagesUrlGenerator->generaUrl($Page, $sigla);
                    }
                }

                foreach ($urlLocalizzati as $sigla => $loc) {

                    $record['loc'] = $loc;
                    foreach ($urlLocalizzati as $siglaAlternate => $urlAlternate) {
                        if ($sigla != $siglaAlternate) {
                            $record['alternate'][$siglaAlternate] = $urlAlternate;
                        }
                    }
                    $record['lastmod'] = $Page->getUpdatedAt()->format('Y-m-d');
                    $record['changefreq'] = 'monthly';
                    $record['priority'] = '0.8';
                    $record['images'] = [];

                    if ($Page->getHeaderImgFileName()) {
                        $image = [];
                        $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $Page->getUploadDir() . $Page->getHeaderImgFileName();
                        $image['caption'] = $Page->getHeaderImgAlt();
                        $record['images'][] = $image;
                    }

                    if ($Page->getListImgFileName()) {
                        $image = [];
                        $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $Page->getUploadDir() . $Page->getListImgFileName();
                        $image['caption'] = $Page->getListImgAlt();
                        $record['images'][] = $image;
                    }

                    if (isset($this->imageCache['CONTENT'][$Page->getTemplate()])) {
                        $record['images'] = array_merge(
                            $record['images'],
                            $this->imageCache['CONTENT'][$Page->getTemplate()]
                        );
                    }
                    if (isset($this->imageCache['HEADER'][$Page->getTemplate()])) {
                        $record['images'] = array_merge(
                            $record['images'],
                            $this->imageCache['HEADER'][$Page->getTemplate()]
                        );
                    } elseif (isset($this->imageCache['HEADER']['default'])) {
                        $record['images'] = array_merge($record['images'], $this->imageCache['HEADER']['default']);
                    }
                    if (isset($this->imageCache['FOOTER'][$Page->getTemplate()])) {
                        $record['images'] = array_merge(
                            $record['images'],
                            $this->imageCache['FOOTER'][$Page->getTemplate()]
                        );
                    } elseif (isset($this->imageCache['FOOTER']['default'])) {
                        $record['images'] = array_merge($record['images'], $this->imageCache['FOOTER']['default']);
                    }

                    $urls[] = $record;

                }


            }

        }

        return $urls;

    }

    private function getUrlBlog()
    {

        $em = $this->getDoctrine()->getManager();

        $NewsList = $em->getRepository('AppBundle:News')->findNewsButIds([], $this->request->getLocale(), null);

        $Languages = $this->get('app.languages');

        $langs = $Languages->getActivePublicLanguages();

        $urls = [];

        foreach ($NewsList as $News) {
            /**
             * @var $News News
             */

            $urlLocalizzati = [];

            $Category = $News->getPrimaryCategory();

            foreach ($langs as $sigla => $estesa) {
                if ($News->translate($sigla)->getIsEnabled()) {
                    $params = [
                        'slug' => $Category->translate($sigla)->getSlug(),
                        'slugnotizia' => $News->translate($sigla)->getSlug(),
                    ];
                    $urlLocalizzati[$sigla] = $this->pathManager->generate('blog_read_news', $params, $sigla, true);
                }
            }

            foreach ($urlLocalizzati as $sigla => $urlLocalizzato) {

                $record['loc'] = $urlLocalizzato;

                foreach ($urlLocalizzati as $siglaAlternate => $urlAlternate) {
                    if ($siglaAlternate != $sigla) {
                        $record['alternate'][$siglaAlternate] = $urlAlternate;
                    }
                }

                $record['lastmod'] = $News->getUpdatedAt()->format('Y-m-d');
                $record['changefreq'] = 'monthly';
                $record['priority'] = '0.8';
                $record['images'] = [];

                if ($News->getHeaderImgFileName()) {
                    $image = [];
                    $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $News->getUploadDir() . $News->getHeaderImgFileName();
                    $image['caption'] = $News->getHeaderImgAlt();
                    $record['images'][] = $image;
                }

                if ($News->getListImgFileName()) {
                    $image = [];
                    $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $News->getUploadDir() . $News->getListImgFileName();
                    $image['caption'] = $News->getListImgAlt();
                    $record['images'][] = $image;
                }

                $urls[] = $record;

            }


        }

        return $urls;

    }


    private function getModulesImages()
    {

        $em = $this->getDoctrine()->getManager();

        $TemplateList = $em->getRepository('AppBundle:Template')->findBy(
            ['layout' => $this->getParameter('generali')['layout']]
        );

        $cache = [];

        $cache['CONTENT'] = [];
        $cache['HEADER'] = [];
        $cache['FOOTER'] = [];

        foreach ($TemplateList as $Template) {

            $config = $Template->getConfig();
            $template = $Template->getTemplate();
            $type = $Template->getType();

            if (!isset($cache[$type][$template])) {
                $cache[$type][$template] = [];
            }

            if (isset($config['it'])) {
                foreach ($config['it'] as $field => $fieldData) {
                    if (is_array(
                            $fieldData
                        ) && isset($fieldData['src']) && $fieldData['src'] && isset($fieldData['alt']) && $fieldData['alt']) {
                        $image = [];
                        $image['loc'] = $this->request->getSchemeAndHttpHost() . $fieldData['src'];
                        $image['caption'] = $fieldData['alt'];

                        $cache[$type][$template][] = $image;
                    }

                }
            }

        }

        $this->imageCache = $cache;

    }

    private function getUrlAppartamenti()
    {

        $bundles = $this->getParameter('kernel.bundles');

        $urls = [];

        if (isset($bundles['AppartamentiBundle'])) {

            $em = $this->getDoctrine()->getManager();

            $AppartamentiList = $em->getRepository('AppartamentiBundle:Appartamento')->findBy(['isEnabled' => 1]);

            $Languages = $this->get('app.languages');

            $langs = $Languages->getActivePublicLanguages();

            foreach ($AppartamentiList as $Appartamento) {
                /**
                 * @var $Appartamento Appartamento
                 */

                foreach ($langs as $sigla => $lang) {

                    $params = ['slug' => $Appartamento->translate($sigla)->getSlug()];

                    $record = [];

                    $record['loc'] = $this->request->getSchemeAndHttpHost() . $this->pathManager->generateUrl(
                            'appartamento',
                            $params,
                            $sigla
                        );

                    $record['lastmod'] = $Appartamento->getUpdatedAt()->format('Y-m-d');

                    $record['changefreq'] = 'monthly';
                    $record['priority'] = '0.8';
                    $record['images'] = [];

                    if ($sigla == 'it') {
                        if ($Appartamento->getHeaderImgFileName()) {
                            $image = [];
                            $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $Appartamento->getUploadDir() . $Appartamento->getHeaderImgFileName();
                            $image['caption'] = $Appartamento->getHeaderImgAlt();
                            $record['images'][] = $image;
                        }

                        if ($Appartamento->getListImgFileName()) {
                            $image = [];
                            $image['loc'] = $this->request->getSchemeAndHttpHost() . '/' . $Appartamento->getUploadDir() . $Appartamento->getListImgFileName();
                            $image['caption'] = $Appartamento->getListImgAlt();
                            $record['images'][] = $image;
                        }
                        $attachments = $Appartamento->getAttachments();

                        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');

                        foreach ($attachments as $attachment) {
                            /**
                             * @var $attachment AppartamentoAttachment
                             */
                            if (in_array($attachment->getType(), ['jpeg', 'jpg', 'png', 'gif'])) {
                                $image = [];
                                $image['loc'] = $this->request->getSchemeAndHttpHost() . $helper->asset(
                                        $attachment,
                                        'file'
                                    );
                                $image['caption'] = $attachment->translate()->getAlt();
                                $record['images'][] = $image;
                            }
                        }
                    }

                    $urls[] = $record;
                }
            }
        }

        return $urls;
    }

    public function getUrlOfferte()
    {

        $lang_helper = $this->get('app.languages');
        $vich_helper = $this->get('vich_uploader.templating.helper.uploader_helper');
        $em = $this->getDoctrine()->getManager();

        $urlLocalizzati = [];

        $langs = $lang_helper->getActivePublicLanguages();
        $offers = $em->getRepository('OfferteBundle:Offerta')->findBy(['isEnabled' => true]);

        foreach ($offers as $offer) {
            /**
             * @var Offerta $offer
             */
            $images_taken = false;

            foreach ($langs as $key => $lang) {
                $entry = [];
                $slug = $offer->translate($key)->getSlug();
                if ($slug && $slug !== '') {
                    $url = $this->pathManager->generate('offerta', ['slug' => $slug], $key, true, false);
                    $entry['loc'] = $url;
                    $entry['lastmod'] = $offer->getUpdatedAt()->format('Y-m-d');
                    $entry['changefreq'] = 'monthly';
                    $entry['priority'] = '0.8';
                    $entry['images'] = [];

                    if (!$images_taken) {
                        $images_taken = true;

                        if ($offer->getHeaderImgFileName()) {
                            $image = [];
                            $image['loc'] = $this->request->getSchemeAndHttpHost() . $offer->getUploadDir() . $offer->getHeaderImgFileName();
                            $image['caption'] = $offer->getHeaderImgAlt();
                            $entry['images'][] = $image;
                        }

                        if ($offer->getListImgFileName()) {
                            $image = [];
                            $image['loc'] = $this->request->getSchemeAndHttpHost() . $offer->getUploadDir() . $offer->getListImgFileName();
                            $image['caption'] = $offer->getListImgAlt();
                            $entry['images'][] = $image;
                        }

                        foreach ($offer->getAttachments() as $attachment) {
                            $image = [];
                            if (in_array($attachment->getType(), ['jpeg', 'jpg', 'png', 'gif'])) {
                                $image['loc'] = $this->request->getSchemeAndHttpHost() . $vich_helper->asset(
                                        $attachment,
                                        'file'
                                    );
                                $image['caption'] = $attachment->translate()->getAlt();
                                $entry['images'][] = $image;
                            }
                        }
                    }
                }

                if ($entry && $entry !== []) {
                    $urlLocalizzati[] = $entry;
                }

            }
        }

        return $urlLocalizzati;
    }


}