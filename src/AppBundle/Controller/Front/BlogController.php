<?php
// 26/01/17, 10.20
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Front;


use AppBundle\Entity\News;
use AppBundle\Entity\NewsCategory;
use AppBundle\Entity\NewsHasCategory;
use AppBundle\Entity\NewsTranslation;
use AppBundle\Entity\Page;
use Mremi\UrlShortenerBundle\Tests\Entity\Link;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogController extends Controller
{

    /**
     * @Route("/blog/{slug}/feed.xml", defaults={"_locale"="it"}, name="rss_blog_category_it")
     * @Route("/{_locale}/blog/{slug}/feed.xml", requirements={"_locale"="de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="rss_blog_category")
     * @Route("/blog/feed.xml", defaults={"_locale"="it"}, name="rss_blog_it", requirements={"mese":"^[\d]{1,2}\-[\d]{4}$"})
     * @Route("/{_locale}/blog/feed.xml", requirements={"_locale"="de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="rss_blog")
     */
    public function rss(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        /**
         * @var $Page Page
         */
        $Page = $em->getRepository('AppBundle:Page')->findOneBy(['template' => 'blog']);

        if ($Page) {

            $TemplateLoader = $this->get('app.template_loader');

            $Languages = $this->get('app.languages');

            if ($request->get('slug')) {

                $NewsCategoryTranslation = $em->getRepository('AppBundle:NewsCategoryTranslation')->findOneBy(
                    ['slug' => $request->get('slug'), 'locale' => $request->getLocale()]
                );

                if (!$NewsCategoryTranslation) {
                    throw new NotFoundHttpException("Pagina non trovata");
                }

                $NewsCategory = $NewsCategoryTranslation->getTranslatable();

                if (!$NewsCategory->getIsEnabled()) {
                    throw new NotFoundHttpException("Pagina non trovata");
                }

                $NewsList = $em->getRepository('AppBundle:NewsHasCategory')->getNews(
                    $NewsCategory->getId(),
                    $request->getLocale()
                );

            } else {

                // estraggo il resto delle news
                $NewsList = $em->getRepository('AppBundle:News')->findNewsButIds([], $request->getLocale());

            }

            $data = [];
            $data['news'] = [];

            $blogService = $this->get('app.blog_front_end');
            $pathManager = $this->get('app.path_manager');

            $liipManager = $this->get('liip_imagine.controller');
            $liipCache = $this->get('liip_imagine.cache.manager');

            foreach ($NewsList as $News) {

                if ($News instanceof NewsHasCategory) {
                    $News = $News->getNews();
                }

                if ($News->getListImgFileName()) {
                    $liipManager->filterAction(
                        $request,
                        $News->getUploadDir().$News->getListImgFileName(),
                        'news_3'
                    );
                }

                /**
                 * @var $News News
                 */
                $newsCategory = $blogService->getCategory($News, $request->getLocale());
                $params = [
                    'slug' => $newsCategory->getSlug(),
                    'slugnotizia' => $News->translate($request->getLocale())->getSlug(),
                ];

                $record = [];
                $record['title'] = $News->translate($request->getLocale())->getTitolo();
                $record['url'] = $pathManager->generateUrl('blog_read_news', $params);
                $record['author'] = 'Webtek spa';
                $record['postDate'] = $News->getPublishAt()->format('r');
                $record['body'] = $blogService->getTextPreview($News, 300, $request->getLocale());
                $record['thumb'] = false;
                if ($News->getListImgFileName()) {
                    $record['thumb'] = $liipCache->getBrowserPath(
                        $News->getUploadDir().$News->getListImgFileName(),
                        'news_3'
                    );
                    $record['thumb_alt'] = $News->getListImgAlt();
                }

                $data['news'][] = $record;

            }

            $liipManager->filterAction(
                $request,
                'images/loghi/logo_webtek_color.png',
                'rss'
            );

            $data['siteName'] = 'Webtek';
            $data['copyright'] = 'Webtek';
            $data['now'] = new \DateTime();
            $data['now'] = $data['now']->format('r');
            $data['siteUrl'] = 'http://www.webtek.it';
            $data['siteDescription'] = 'Webtek.it - sviluppo software';
            $data['channelImage'] = [];
            $data['channelImage']['url'] = $liipCache->getBrowserPath(
                'images/loghi/logo_webtek_color.png',
                'rss'
            );
            $data['channelImage']['title'] = 'Scopri come far crescere il tuo businer';
            $data['channelImage']['description'] = 'Logo Webtek';

            $data['lang'] = 'it';
            $data['rss'] = '';

            $response = new Response($this->renderView('public/rss.twig', $data));
            $response->headers->set('Content-Type', 'application/rss+xml; charset=utf-8');
            $response->headers->set('Access-Control-Allow-Origin', '*');

            return $response;

        }

    }

    /**
     * @param Request $req
     * @Route("/blog/search/", name = "searchBlog_it", defaults={"_locale":"it"})
     * @Route("/{_locale}/blog/search/", name = "searchBlog", requirements={"_locale"="it|de|en|fr|jp|nl|pt|ru|zh|gcs|pl"})
     */
    public function searchAction(Request $req)
    {

        if (!$req->query->has("query")) {
            return new RedirectResponse($this->get("router")->generate("blog"));
        }

        $query = $req->query->get("query");

        $em = $this->getDoctrine()->getManager();
        $templateLoader = $this->get("app.template_loader");
        $languages = $this->get("app.languages");
        $additionalData = [
            "langs" => $languages->getActivePublicLanguages(),
        ];

        /** @var News[] $news */
        $news = $em->getRepository("AppBundle:News")->search(
            $query,
            $req->getLocale()
        );

        $additionalData["news"] = $news;

        $page = $em->getRepository("AppBundle:Page")->findOneBy(["template" => "blog_search"]);

        if ($page) {

            $twigParams = [];

            $newsHeader = [];

            $next = false;
            $prev = false;

            $META = [
                'title' => $page->translate($req->getLocale())->getMetaTitle(),
                'description' => $page->translate($req->getLocale())->getMetaDescription(),
            ];

            $twigParams['META'] = $META;
            $twigParams['h1'] = $page->translate($req->getLocale())->getTitolo();

            $twigs = $templateLoader->getTwigs(
                'blog_search',
                $languages->getActivePublicLanguages(),
                $additionalData
            );

            $twigParams['twigs'] = $twigs;

            foreach ($twigs['sections']['CONTENT']['widgets'] as $container => $widgets) {
                foreach ($widgets as $widget) {
                    if (isset($widget['config'][$req->getLocale()]) && isset(
                            $widget['config'][$req->getLocale()]['data']
                        ) && isset($widget['config'][$req->getLocale()]['data']['META'])) {
                        $META = array_replace_recursive(
                            $META,
                            $widget['config'][$req->getLocale()]['data']['META']
                        );
                    }
                }
            }

            $baseurl = $req->getScheme().'://'.$req->getHttpHost().$req->getBasePath();
            $META['canonical'] = $baseurl."/blog";

            $META['alternate'] = [];

            $twigParams['META'] = $META;

            return $this->render('public/site.html.twig', $twigParams);

        } else {

            throw new \Exception('Non esiste una pagina con il template blog_search, creala subito!');

        }
    }


    /**
     * @Route("/blog/{slug}/", defaults={"_locale"="it","page"="1"}, name="blog_category_it")
     * @Route("/{_locale}/blog/{slug}", defaults={"page"="1"}, requirements={"_locale"="it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="blog_category")
     * @Route("/blog", defaults={"_locale"="it", "page"="1"}, name="blog_it", requirements={"mese":"^[\d]{1,2}\-[\d]{4}$"})
     * @Route("/{_locale}/blog", defaults={"page"="1"},requirements={"_locale"="it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="blog")
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        /**
         * @var $Page Page
         */
        $Page = $em->getRepository('AppBundle:Page')->findOneBy(['template' => 'blog']);

        if ($Page) {

            $META = [];

            $TemplateLoader = $this->get('app.template_loader');

            $Languages = $this->get('app.languages');

            $AdditionalData = ['langs' => $Languages->getActivePublicLanguages()];

            $TwigParams = [];

            $NewsHeader = [];

            $next = false;
            $prev = false;

            $META = [
                'title' => $Page->translate($request->getLocale())->getMetaTitle(),
                'description' => $Page->translate($request->getLocale())->getMetaDescription(),
            ];

            $twigParams['META'] = $META;
            $twigParams['h1'] = $Page->translate($request->getLocale())->getTitolo();

            /*
                Gestione template associata alla categoria : slug
                Gestito fallback per versioni precedenti
            */
            $template = 'blog';

            $NewsCategory = null;

            if ($request->get('slug')) {
                /**
                 * @var $NewsCategory NewsCategory
                 */
                $NewsCategoryTranslation = $em->getRepository('AppBundle:NewsCategoryTranslation')->findOneBy(
                    ['slug' => $request->get('slug'), 'locale' => $request->getLocale()]
                );

                if ($NewsCategoryTranslation) {

                    $NewsCategory = $NewsCategoryTranslation->getTranslatable();

                    $template = $NewsCategory->getTemplate();
                    $AdditionalData['NewsCategory'] = $NewsCategory;
                }
            }

            $twigs = $TemplateLoader->getTwigs($template, $Languages->getActivePublicLanguages(), $AdditionalData);

            $twigParams['twigs'] = $twigs;

            $MetaManager = $this->get('app.meta_manager');
            $META = $MetaManager->merge($twigs, $META);

            $META['alternate'] = [];

            if ($NewsCategory) {

                if ($NewsCategory->getIsEnabled()) {

                    $pathManager = $this->get('app.path_manager');

                    foreach ($AdditionalData['langs'] as $sigla => $estesa) {
                        $META['alternate'][$sigla] = $pathManager->generateUrl(
                            'blog_category',
                            ['slug' => $NewsCategory->translate($sigla)->getSlug()],
                            $sigla,
                            true
                        );
                    }

                } else {
                    throw new NotFoundHttpException("Pagina non trovata");
                }


            } else {

                $pagesUrlGenerator = $this->get('app.page_url_generator');
                foreach ($AdditionalData['langs'] as $sigla => $estesa) {
                    if ($sigla != $request->getLocale()) {
                        $META['alternate'][$sigla] = $pagesUrlGenerator->generaUrl($Page, $sigla);
                    }
                }
            }

            $twigParams['META'] = $META;


            return $this->render('public/site.html.twig', $twigParams);


        } else {

            throw new \Exception('Non esiste una pagina con il template blog, creala subito!');
        }


    }


    /**
     * @Route("/blog/{slug}/{slugnotizia}", defaults={"_locale"="it"}, name="blog_read_news_it")
     * @Route("/{_locale}/blog/{slug}/{slugnotizia}", requirements={"_locale"="it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="blog_read_news")
     */
    public function readAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $TemplateLoader = $this->get('app.template_loader');

        $Languages = $this->get('app.languages');

        $pathManager = $this->get('app.path_manager');

        $AdditionalData = ['langs' => $Languages->getActivePublicLanguages()];

        /**
         * @var $NewsTranslation NewsTranslation
         */
        $NewsTranslation = $em->getRepository('AppBundle:NewsTranslation')->findOneBy(
            ['slug' => $request->get('slugnotizia'), 'isEnabled' => 1, 'locale' => $request->getLocale()]
        );

        if ($NewsTranslation) {

            /**
             * @var $News News
             */
            $News = $NewsTranslation->getTranslatable();

            $News->translate($request->getLocale())->setVisits(
                $News->translate($request->getLocale())->getVisits() + 1
            );

            if (!$News->translate($request->getLocale())->getShortUrl()) {

                $link = new Link();
                $link->setLongUrl($request->getUri());

                $chainProvider = $this->get('mremi_url_shortener.chain_provider');

                $chainProvider->getProvider('bitly')->shorten($link);

                $News->translate($request->getLocale())->setShortUrl($link->getShortUrl());

            }

            $em->persist($News);
            $em->flush();

            $AdditionalData['Entity'] = $News;

            if ($News && $News->getIsEnabled()) {

                $META['title'] = $News->translate($request->getLocale())->getMetaTitle();
                $META['description'] = $News->translate($request->getLocale())->getMetaDescription();
                $META = array_merge($META, $this->get('app.open_graph')->generateOpenGraphData($News));

                $META['alternate'] = [];

                $Category = $News->getPrimaryCategory();

                foreach ($AdditionalData['langs'] as $sigla => $estesa) {

                    if ($sigla != $request->getLocale()) {
                        if ($News->translate($sigla)->getIsEnabled()) {
                            $params = [
                                'slug' => $Category->translate($sigla)->getSlug(),
                                'slugnotizia' => $News->translate($sigla)->getSlug(),
                            ];
                            $META['alternate'][$sigla] = $pathManager->generate(
                                'blog_read_news',
                                $params,
                                $sigla,
                                true
                            );
                        }
                    }
                }


                $AdditionalData['META'] = $META;

                $twigs = $TemplateLoader->getTwigs(
                    'blog_dettaglio',
                    $Languages->getActivePublicLanguages(),
                    $AdditionalData
                );

                $microData = [];
                $microData['@type'] = 'NewsArticle';
                $microData['mainEntityOfPage'] = [
                    '@type' => 'WebPage',
                    '@id' => $request->getSchemeAndHttpHost().$request->getRequestUri(),
                ];
                $microData['headline'] = $News->translate($request->getLocale())->getTitolo();
                if (isset($META['social']['og']['image'])) {
                    $microData['image'] = [
                        '@type' => 'ImageObject',
                        'url' => $META['social']['og']['image'],
                        'width' => '1200',
                        'height' => '630',
                    ];
                }
                $microData['datePublished'] = $News->getPublishAt()->format('r');
                $microData['dateModified'] = $News->getUpdatedAt()->format('r');
                $microData['publisher'] = [
                    '@type' => 'Organization',
                    'name' => 'Webtek s.p.a.',
                    'logo' => [
                        '@type' => 'ImageObject',
                        'url' => 'https://www.webtek.it/images/loghi/logo_webtek_color.png',
                        'width' => 424,
                        'height' => 98,
                    ],
                ];
                $microData['description'] = $META['description'];

                $microData = json_encode($microData, JSON_PRETTY_PRINT);

                return $this->render(
                    'public/site.html.twig',
                    [
                        'twigs' => $twigs,
                        'META' => $META,
                        'microData' => $microData,
                    ]
                );

            }


        }

        throw new NotFoundHttpException("Pagina non trovata");


    }

    /**
     * @Route("/blog-json/{page}", requirements={"page"="\d"}))
     * @Route("/blog-json/{page}/{slug}", requirements={"page"="\d"})
     */
    public function infiniteLoadingAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $data = [];

        $NewsCategoryAll = $em->getRepository('AppBundle:NewsCategory')->findBy(
            ['isEnabled' => 1],
            ['sort' => 'ASC'],
            4,
            0
        );

        $NewsHeader = [];

        $next = false;
        $prev = false;

        foreach ($NewsCategoryAll as $newsCategory) {

            /**
             * @var $NewsHasCategory NewsHasCategory
             */
            $NewsHasCategories = $em->getRepository('AppBundle:NewsHasCategory')->getNews(
                $newsCategory->getId(),
                $request->getLocale(),
                0,
                1,
                $NewsHeader
            );

            if ($NewsHasCategories) {

                $NewsHasCategory = $NewsHasCategories[0];

                $News = $NewsHasCategory->getNews();

                $NewsHeader[] = $News;

            }

        }

        if (count($NewsHeader) < 4) {

            $res = $em->getRepository('AppBundle:News')->findNewsButIds(
                $NewsHeader,
                $request->getLocale(),
                0,
                4 - count($NewsHeader)
            );


            $NewsHeader = array_merge($NewsHeader, $res);

        }

        if ($request->get('slug')) {
            $NewsCategoryTranslation = $em->getRepository('AppBundle:NewsCategoryTranslation')->findOneBy(
                ['slug' => $request->get('slug'), 'locale' => $request->getLocale()]
            );

            if (!$NewsCategoryTranslation) {
                throw new NotFoundHttpException("Pagina non trovata");
            }

            $NewsCategory = $NewsCategoryTranslation->getTranslatable();

            if (!$NewsCategory->getIsEnabled()) {
                throw new NotFoundHttpException("Pagina non trovata");
            }

            // estraggo il resto delle news
            $NewsList = $em->getRepository('AppBundle:NewsHasCategory')->getNews(
                $NewsCategory->getId(),
                $request->getLocale(),
                ($request->attributes->get('page') - 1) * $this->getParameter('generali')['elementi_pagina_paginatore'],
                $this->getParameter('generali')['elementi_pagina_paginatore']
            );

        } else {

            // estraggo il resto delle news
            $NewsList = $em->getRepository('AppBundle:News')->findNewsButIds(
                $NewsHeader,
                $request->getLocale(),
                ($request->attributes->get('page') - 1) * $this->getParameter('generali')['elementi_pagina_paginatore'],
                $this->getParameter('generali')['elementi_pagina_paginatore']
            );


        }

        $data = [];

        $blogService = $this->get('app.blog_front_end');

        $pathManager = $this->get('app.path_manager');

        $liipManager = $this->get('liip_imagine.controller');
        $liipCache = $this->get('liip_imagine.cache.manager');

        foreach ($NewsList as $News) {

            if ($News instanceof NewsHasCategory) {
                $News = $News->getNews();
            }

            /**
             * @var $News News
             */
            $record = [];
            $record['titolo'] = $News->translate($request->getLocale())->getTitolo();
            $record['text'] = $blogService->getTextPreview($News, 300, $request->getLocale());
            $record['img'] = "/images/defaults/placeholder.png";
            $record['imgAlt'] = $News->translate($request->getLocale())->getTitolo();

            if ($News->getListImgFileName()) {
                $liipManager->filterAction(
                    $request,
                    $News->getUploadDir().$News->getListImgFileName(),
                    'news4'
                );
                $record['img'] = $liipCache->getBrowserPath(
                    $News->getUploadDir().$News->getListImgFileName(),
                    'news4'
                );
                $record['imgAlt'] = $News->getHeaderImgAlt();
            }
            $newsCategory = $blogService->getCategory($News, $request->getLocale());
            $params = [
                'slug' => $newsCategory->getSlug(),
                'slugnotizia' => $News->translate($request->getLocale())->getSlug(),
            ];
            $record['url'] = $pathManager->generateUrl('blog_read_news', $params);
            $record['data'] = $blogService->getDate($News);
            $record['day'] = $News->getPublishAt()->format("d");
            $record['month'] = $this->get("translator")->trans(
                'cal.labels.mese_short_'.$News->getPublishAt()->format('n')
            );
            $record['category'] = $newsCategory->getTitolo();
            $record['category_url'] = $pathManager->generateUrl('blog_category', ['slug' => $newsCategory->getSlug()]);

            $data[] = $record;

        }

        $return = [];
        $return['result'] = true;
        $return['data'] = $data;
        if (isset($page)) {
            $return['page'] = $page;
        }

        return new JsonResponse($return);


    }


}