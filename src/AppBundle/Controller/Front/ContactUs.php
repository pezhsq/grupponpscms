<?php
// 09/02/17, 11.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AppBundle\Controller\Front;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ContactUs extends Controller
{

    /**
     * @Route("/contact-us", defaults={"_locale"="it"}, name="contact-us_it")
     * @Route("/{_locale}/contact-us", requirements={"_locale" = "it|de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, defaults={"_locale"="it"}, name="contact-us")
     */
    public function indexController(Request $request)
    {

        $trans = $this->get('translator');

        $errori = [];

        $data = [];

        if (!$request->request->has('fullname') || !$request->request->get('fullname')) {
            $errori[] = $trans->trans('booking.errors.fullname_empty', [], 'public');
        } else {
            $data['fullname'] = strip_tags($request->request->get('fullname'));
        }
        if (!$request->request->has('passeggeri') || !$request->request->get('passeggeri') || !is_numeric(
                $request->request->get('passeggeri')
            )) {
            $errori[] = $trans->trans('booking.errors.passeggeri_empty', [], 'public');
        } else {
            $data['passeggeri'] = intval($request->request->get('passeggeri'));
        }

        if (!$request->request->has('phone') || !$request->request->get('phone')) {
//            $errori[] = $trans->trans('booking.errors.phone_empty', [], 'public');
        } else {
            $data['phone'] = strip_tags($request->request->get('phone'));
        }
        if (!$request->request->has('email') || !$request->request->get('email') || !filter_var(
                $request->request->get('email'),
                FILTER_VALIDATE_EMAIL
            )) {
            $errori[] = $trans->trans('booking.errors.bad_email', [], 'public');
        } else {
            $data['email'] = $request->request->get('email');
        }
        if (!$request->request->has('data-arrivo') || !$request->request->get('data-arrivo')) {
            $errori[] = $trans->trans('booking.errors.data_arrivo_empty', [], 'public');
        } else {
            $data['data_arrivo'] = strip_tags($request->request->get('data-arrivo'));
        }

        if (!$request->request->has('data-partenza') || !$request->request->get('data-partenza')) {
            $errori[] = $trans->trans('booking.errors.data_partenza_empty', [], 'public');
        } else {
            $data['data_partenza'] = strip_tags($request->request->get('data-partenza'));
        }

        if (!$request->request->has('messaggio')) {
            $errori[] = $trans->trans('booking.errors.messaggio_non_settato');
        } else {
            $data['messaggio'] = strip_tags($request->request->get('messaggio'));
        }

        if (!$request->request->has('privacy')) {
            $errori[] = $trans->trans('booking.errors.privacy_empty', [], 'public');
        }

        foreach ($request->request->all() as $k => $v) {
            if (preg_match('/.*_wt$/', $k)) {
                if ($v) {
                    throw new HttpException(400, 'Richiesta non valida');
                }
            }
        }

        $return = [];
        $return['result'] = true;

        if ($errori) {
            $return['result'] = false;
            $return['errors'] = $errori;
        } else {

            $destinatari = explode(";", $this->getParameter('emails')['destinatario_contatti']);
            $destinatari = array_map('trim', $destinatari);
            $destinatari = array_filter($destinatari);

            $message = \Swift_Message::newInstance()->setSubject('Richiesta informazioni')->setFrom(
                $data['email'],
                $data['fullname']
            )->setTo($destinatari)->setBody(
                $this->renderView(
                    'modules/selettoreDate/SD.1.0.0/selettoreDate.email.twig',
                    ['data' => $data]
                ),
                'text/html'
            );

            $this->get('mailer')->send($message);

        }

        return new JsonResponse($return);

    }

    /**
     * @Route("/contact-us-form", defaults={"_locale"="it"}, name="contact-us-form_it")
     * @Route("/{_locale}/contact-us-form", requirements={"_locale" = "de|en|fr|jp|nl|pt|ru|zh"}, name="contact-us-form")
     * @var $request Request
     * @return JsonResponse
     */
    public function contactForm(Request $request)
    {

        $validator = $this->get("app.form_contatti_helper");

        // Lista di campi obbligatori come passati nella request
        $required_fields = [
            "nome_cognome",
            "email",
            "tipo_richiesta",
            "richiesta",
        ];

        foreach ($request->request->all() as $k => $v) {
            if (preg_match('/.*_wt$/', $k)) {
                if ($v) {
                    throw new HttpException(400, 'Richiesta non valida');
                }
            }
        }

        // Parsa i dati
        $form_valid = $validator->validate($required_fields);

        // Se il form è valido manda la mail, altrimenti crea e restituisce il messaggio d'errore
        if ($form_valid) {
            // Prepara l'array di destinatari del form contatti
            $destinatari = explode(";", $this->getParameter('emails')['destinatario_contatti']);
            $destinatari = array_map('trim', $destinatari);
            $destinatari = array_filter($destinatari);

            // Prepara l'array di mail alle quali rispondere in caso di necessità (solo quella inserita nel form)
            $replyTo = [$request->request->get('email') => $request->request->get('nome_cognome')];

            $toRet = [];
            $toRet["result"] = $this->get("app.services.email_template_helper")->send(
                "EMAIL_CONTATTI",
                $destinatari,
                $validator->getData(),
                $request->getLocale(),
                false,
                $replyTo
            );

        } else {
            $toRet = [
                "result" => false,
                "errors" => $validator->getErrors(),
            ];
        }

        return new JsonResponse($toRet);

    }

}