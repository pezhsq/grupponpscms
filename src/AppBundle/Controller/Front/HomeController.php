<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 02/12/16
 * Time: 8.58
 */

namespace AppBundle\Controller\Front;


use AppBundle\Entity\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{

    /**
     * @Route("/", defaults={"_locale"="it"}, name="home_it")
     * @Route("/{_locale}", requirements={"_locale" = "de|en|fr|jp|nl|pt|ru|zh|cs|pl|es"}, name="home")
     */
    public function indexController(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $HomePage = $em->getRepository('AppBundle:Page')->findOneBy(['template' => 'home']);

        $META = [];
        $META['title'] = $HomePage->translate($request->getLocale())->getMetaTitle();
        $META['description'] = $HomePage->translate($request->getLocale())->getMetaDescription();

        if (!$HomePage) {
            throw new \Exception('Devi creare una pagina con template "home"');
        }

        $TemplateLoader = $this->get('app.template_loader');

        $Languages = $this->get('app.languages');

        $AdditionalData = [];
        $AdditionalData['Entity'] = $HomePage;
        $AdditionalData['langs'] = $Languages->getActivePublicLanguages();

        $pagesUrlGenerator = $this->get('app.page_url_generator');
        foreach ($AdditionalData['langs'] as $sigla => $estesa) {
            if ($HomePage->translate($sigla)->getIsEnabled()) {
                $META['alternate'][$sigla] = $pagesUrlGenerator->generaUrl($HomePage, $sigla, true);
            }
        }

        $AdditionalData['META'] = $META;

        $twigs = $TemplateLoader->getTwigs('home', $Languages->getActivePublicLanguages(), $AdditionalData);

        return $this->render('public/site.html.twig', ['twigs' => $twigs, 'META' => $META]);

    }

}
