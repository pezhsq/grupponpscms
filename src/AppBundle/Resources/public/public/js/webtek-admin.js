$(function () {
    var tplGrid = '<div class="edit-grid" style="width:{{width}}px;height:{{height}}px"><a href="{{href}}" target="_blank" ><i class="glyphicon glyphicon-pencil"></i></div></a>';

    if ($('[data-admin]').length) {

        Mousetrap.bind('w s', function () {
            if ($('.edit-grid').length) {
                $('.edit-grid').remove();
                $('[data-admin]').removeAttr('style');
            } else {

                var compiled = Handlebars.compile(tplGrid);

                $('[data-admin]').each(function () {
                    var $target = $(this);
                    var data = {
                        'href': $target.data('admin'),
                        width: $target.width(),
                        height: $target.height()
                    };
                    if (!data.width || !data.height) {
                        $target = $target.children().first();
                        data.width = $target.width();
                        data.height = $target.height();
                    }
                    $target.css({'position': 'relative'});
                    $(compiled(data)).appendTo($target);


                });
            }
        });


        $('#editModules').on('click', function (e) {


        });
    }
});