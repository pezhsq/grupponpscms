// 17/01/17, 16.23
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

var urlRss = false;

$(function() {

  $rssNotizie = $('#notizieRss');
  $rssOfferte = $('#offerteRss');

  urlRss = $rssNotizie.data('url');

  urlOfferte = $rssOfferte.data('url');

  $.get(urlRss, function(data) {
    var $xml = $(data);
    var compiled = HandleBarHelper.compile('rssNewsRow');

    $xml.find("item").each(function() {
      var $this = $(this),
        item = {
          title: $this.find("title").text(),
          link: $this.find("link").text(),
          description: $this.find("description").text(),
          pubDate: new moment(new Date($this.find("pubDate").text())).format('DD/MM/YYYY h:mm'),
          author: $this.find("author").text()
        };

      $(compiled(item)).appendTo($rssNotizie.find('.timeline'));


    });
  });

  $.get(urlOfferte, function(data) {
    var $xml = $(data);
    var compiled = HandleBarHelper.compile('rssNewsRow');

    $xml.find("item").each(function() {
      var $this = $(this),
        item = {
          title: $this.find("title").text(),
          link: $this.find("link").text(),
          description: $this.find("description").text(),
          pubDate: new moment(new Date($this.find("pubDate").text())).format('DD/MM/YYYY h:mm'),
          author: $this.find("author").text()
        };

      $(compiled(item)).appendTo($rssOfferte.find('.timeline'));


    });
  });

});