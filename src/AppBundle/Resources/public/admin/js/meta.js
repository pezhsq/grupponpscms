var module_name = 'meta';

$(function() {

    $('.btn-edit').on('click', function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/admin/' + module_name + '/form/' + $(this).data('id'),
            dataType: 'json',
            success: function(ret_data) {

                var data = { 'content': ret_data.form, 'titolo': _('Modifica meta') };
                openModalForm(data);
            }
        });
    });

    $('.btn-delete-setting').on('click', function(e) {
        e.preventDefault();

        var id = $(this).data('id');

        var settings = {
            'type': 'danger',
            'titolo': _('Attenzione'),
            'content': _('Vuoi cancellare questo settaggio?'),
            'OK': 'Ok',
            'onOK': function() {
                $.ajax({
                    type: 'POST',
                    url: '/admin/' + module_name + '/delete/' + id,
                    dataType: 'json',
                    success: function(ret_data) {
                        if (ret_data.result) {

                            window.location.assign(window.location.protocol + '//' + window.location.host + window.location.pathname);

                        } else {
                            var opts = {
                                'type': 'danger',
                                'titolo': _('Attenzione'),
                                'content': ret_data.errors.join('<br />'),
                                'OK': 'Ok',
                                'callback': null
                            };
                            HandleBarHelper.alert(opts);
                        }
                    }
                });
            }
        };

        HandleBarHelper.confirm(settings);

    });


    $('.btn-new-meta').on('click', function(e) {
        e.preventDefault();

        url = '/admin/' + module_name + '/form';

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function(ret_data) {
                if (ret_data.result) {

                    var data = { 'content': ret_data.form, 'titolo': _('Creazione nuovo meta') };
                    openModalForm(data);


                } else {
                    var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': ret_data.errors.join('<br />'),
                        'OK': 'Ok',
                        'callback': null
                    };
                    HandleBarHelper.alert(opts);
                }
            }
        });

    });

});

function openModalForm(data) {
    modal = HandleBarHelper.compile('formDialog');

    console.log(modal(data));

    $modal = $(modal(data));

    $modal.modal();

    $modal.on('hidden.bs.modal', function(e) {
        $(this).data('bs.modal', null);
        $modal.remove();
    });

    $modal.on('shown.bs.modal', function(e) {
        $modal.find('.btn-confirm').on('click', function(e) {
            e.preventDefault();
            $modal.find('form').ajaxSubmit({
                'success': function(ret_data) {
                    if (ret_data.result) {
                        $modal.hide();
                        window.location.assign(window.location.protocol + '//' + window.location.host + window.location.pathname);
                    } else {
                        var opts = {
                            'type': 'danger',
                            'titolo': _('Attenzione'),
                            'content': ret_data.errors.join('<br />'),
                            'OK': 'Ok',
                            'callback': null
                        };
                        HandleBarHelper.alert(opts);

                    }
                }
            });


        });
    })

}