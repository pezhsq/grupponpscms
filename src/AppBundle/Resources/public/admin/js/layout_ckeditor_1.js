CKEDITOR.editorConfig = function(config) {
  config.toolbarGroups = [
    { name: 'document', groups: ['mode', 'document', 'doctools'] },
    { name: 'clipboard', groups: ['clipboard', 'undo'] },
    { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
    { name: 'forms', groups: ['forms'] },
    { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
    { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
    { name: 'links', groups: ['links'] },
    { name: 'insert', groups: ['insert'] },
    { name: 'tools', groups: ['tools'] },
    { name: 'styles', groups: ['styles'] },
    { name: 'colors', groups: ['colors'] },
    { name: 'others', groups: ['others'] },
    { name: 'about', groups: ['about'] }
  ];
  config.removeButtons = 'Save,NewPage,Preview,Print,Templates,Find,Replace,SelectAll,Scayt,Form,Radio,Checkbox,TextField,Textarea,Select,Button,HiddenField,ImageButton,Subscript,Superscript,Strike,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Flash,Table,Smiley,SpecialChar,PageBreak,Iframe,ShowBlocks,BGColor,TextColor,About,Source,Cut,Undo,Redo,Copy,Paste,PasteText,PasteFromWord,CopyFormatting,NumberedList,Blockquote,Anchor,Image,HorizontalRule,Styles,Font,FontSize';
  config.autoParagraph = false;
  config.enterMode = CKEDITOR.ENTER_BR;
  config.language = 'it';
  config.extraPlugins = 'sourcedialog';
  config.allowedContent = {
    script: true,
    $1: {
      // This will set the default set of elements
      elements: CKEDITOR.dtd,
      attributes: true,
      styles: true,
      classes: true
    }
  };
};
