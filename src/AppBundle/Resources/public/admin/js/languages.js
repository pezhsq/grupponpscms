$(function () {

    if ($('#datatable').length) {


        var cols = [];


        var col = {
            'title': 'Codice',
            'className': 'dt0',
            searchable: true,
            data: 'shortCode'
        };

        cols.push(col);

        col = {
            'title': 'Nome lingua',
            'className': 'dt1',
            searchable: true,
            data: 'languageName'
        };

        cols.push(col);

        col = {
            'title': 'Locale',
            'className': 'dt2',
            searchable: true,
            data: 'locale'
        };

        cols.push(col);

        col = {
            'className': 'dt-body-center',
            'title': 'Attiva in backend'
        };
        // placeholder attivazione admin
        cols.push(col);

        col = {
            'className': 'dt-body-center',
            'title': 'Attiva sul lato pubblico'
        };
        // placeholder attivazione lato pubblico
        cols.push(col);

        var columnDefs = [];

        columnDef = {
            targets: cols.length - 2,
            searchable: false,
            className: 'dt-body-center',
            orderable: false,
            render: function (data, type, full, meta) {
                var toString = full.languageName;
                var icon = 'check-square-o';
                var title = 'Disabilita: ';
                if (!full.isEnabled) {
                    var icon = 'square-o';
                    var title = 'Abilita: ';
                }
                var ret = '<a href="/admin/languages/toggle-enabled/' + full.shortCode + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                ret += '<i class="fa fa-' + icon + '"></i>';
                ret += '</a>';
                return ret;
            }
        };

        columnDefs.push(columnDef);

        columnDef = {
            targets: cols.length - 1,
            searchable: false,
            className: 'dt-body-center',
            orderable: false,
            render: function (data, type, full, meta) {
                var toString = full.languageName;
                var icon = 'check-square-o';
                var title = 'Disabilita: ';
                if (!full.isEnabledPublic) {
                    var icon = 'square-o';
                    var title = 'Abilita: ';
                }
                var ret = '<a href="/admin/languages/toggle-enabled-public/' + full.shortCode + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                ret += '<i class="fa fa-' + icon + '"></i>';
                ret += '</a>';
                return ret;
            }
        };

        columnDefs.push(columnDef);

        $('#datatable').dataTable({
            ajax: {
                "url": "/admin/languages/json"
            },
            aaSorting: [[2, 'asc']],
            stateSave: true,
            iDisplayLength: 15,
            responsive: true,
            columns: cols,
            columnDefs: columnDefs
        });

    }

});

