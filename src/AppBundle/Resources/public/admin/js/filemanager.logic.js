var moduloFileManager = 'filemanager';
var $directoryTree;
var FMFilters = {};

function setEvents() {

    $('body').on('click', '.item', function (e) {
        e.preventDefault();
        $item = $(this);
        if ($item.data('type') == 'file' && !$item.find('.fa-exclamation-triangle').length) {
            $('.item').removeClass('selected');
            $item.addClass('selected');
            if (typeof(onFileSelected) == 'function') {
                onFileSelected();
            }
        } else if ($item.data('type') == 'file') {

            var msg = _('Questo elemento non è selezionabile, perchè di tipo errato o non rispetta le dimensioni richieste');
            if (typeof FMFilters.dimensioni !== 'undefined') {
                msg += ' (' + FMFilters.dimensioni + ')';
            }
            var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': msg,
                'OK': 'Ok',
                'callback': null
            };
            HandleBarHelper.alert(opts);

        }
    });

}

function loadDirContent(dir) {

    var data = {'path': dir, filters: FMFilters};
    $.ajax({
        type: 'POST',
        url: '/admin/' + moduloFileManager + '/content',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {

                HandleBarHelper.compile('//bundles/app/admin/jstpl/file.jstpl', function (compiled) {

                    $('.files').empty();
                    $(compiled({'files': ret_data.files})).appendTo('.files');
                    $('.thumbcover').height($('.thumbcover:first').width());
                    if (typeof(onFileSelected) == 'function') {
                        onFileSelected();
                    }

                });

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function elimina(path) {
    var data = {'path': path};
    $.ajax({
        type: 'POST',
        url: '/admin/' + moduloFileManager + '/delete',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {

                loadDirContent($('#current-path').val().substring(1));
                $directoryTree.jstree('refresh');
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });
}

function showNewDirDialog(path) {

    HandleBarHelper.compile('//bundles/app/admin/jstpl/new-dir.jstpl', function (compiled) {

        var data = {
            'nomedirDefault': _('nuova-cartella'),
            'path': path,
            lang: {
                nome: _('Nome directory'),
                nome_help: _('Scegli un nome contenente solo lettere minuscole, numeri e _-')
            }
        };

        var opts = {
            'type': 'info',
            'titolo': _('Crea cartella'),
            'content': compiled(data),
            'OK': 'Ok',
            'CANCEL': 'Annulla',
            'onOK': function ($modal) {
                var dirName = $modal.find('#nomedir').val();
                var path = $modal.find('#path').val();
                if (dirName) {
                    $modal.modal('hide');
                    creaDir(dirName, path);
                } else {
                    var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': _('Nome non valido'),
                        'OK': 'Ok',
                        'callback': null
                    };
                    HandleBarHelper.alert(opts);

                }
            },
            'onCANCEL': null
        };
        HandleBarHelper.confirm(opts);


    });


}

function creaDir(dirName, path) {

    var data = {
        'dir': dirName,
        'path': path
    };
    $.ajax({
        type: 'POST',
        url: '/admin/' + moduloFileManager + '/crea-directory',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {
                loadDirContent($('#current-path').val().substring(1));
                $directoryTree.jstree('refresh');
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}


function showNewVersionDialog(path) {

    HandleBarHelper.compile('//bundles/app/admin/jstpl/new-version.jstpl', function (compiled) {

        var data = {
            'dimensioniDefault': '',
            'path': path,
            'lang': {
                'help': {'dimensioni': _('Scegli le dimensioni nel formato larghezzaxaltezza, es. 300x200')},
                'dimensioni': _('Dimensioni')
            }
        };

        var opts = {
            'type': 'info',
            'titolo': _('Crea cartella'),
            'content': compiled(data),
            'OK': 'Ok',
            'CANCEL': 'Annulla',
            'onOK': function ($modal) {
                var dimensioni = $modal.find('#dimensioni').val();
                var path = $modal.find('#path').val();
                var re = /^\d+x\d+$/;
                if (re.test(dimensioni)) {
                    $modal.modal('hide');
                    creaVersione(path, dimensioni);
                } else {
                    var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': _('Dimensioni non valide, indicare ad esempio "300x200"'),
                        'OK': 'Ok',
                        'callback': null
                    };
                    HandleBarHelper.alert(opts);

                }
            },
            'onCANCEL': null
        };
        HandleBarHelper.confirm(opts);

    });
}


function creaVersione(path, dimensioni) {
    var data = {'path': path, 'dimensioni': dimensioni};
    $.ajax({
        type: 'POST',
        url: '/admin/' + moduloFileManager + '/crea-versione',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {
                loadDirContent($('#current-path').val().substring(1));
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });
}

function istanziaDropzone() {
    myDropzone = new Dropzone('.panel-files', {
        url: "/admin/" + moduloFileManager + "/upload",
        createImageThumbnails: false,
        autoProcessQueue: true,
        previewTemplate: '<div style="display:none"></div>'
    });

    myDropzone.on('sending', function (file, xhr, formData) {
            formData.append('path', $('#current-path').val());
        }
    );
    myDropzone.on('success', function (file, ret_data) {
            if (ret_data.result) {

                loadDirContent($('#current-path').val());

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    );

}

function menuContestualeFiles() {
    $.contextMenu({
        selector: '.item',
        autoHide: true,
        items: {
            crea: {
                name: _('Nuova versione'),
                disabled: function (key, opt) {
                    if (!opt.$trigger.data('image')) {
                        return true;
                    }
                },
                callback: function (key, opt) {
                    $item = opt.$trigger;
                    showNewVersionDialog($item.data('path'));
                }
            },
            visualizza: {
                name: _('Visualizza originale'),
                disabled: function (key, opt) {
                    return false;
                },
                callback: function (key, opt) {
                    $item = opt.$trigger;
                    lightbox.start($item.find('a'));
                }
            },
            delete: {
                name: _("Elimina"),
                disabled: function (key, opt) {
                    if (opt.$trigger.data('type') === 'dir-up') {
                        return true;
                    }
                },
                callback: function (key, opt) {
                    $item = opt.$trigger;
                    var tipo = _('cartella');
                    var articolo = _('la')
                    if ($item.data('type') == 'file') {
                        tipo = _('file');
                        articolo = _('il');
                    }

                    var nome = $item.find('.thumbcover').attr('title') || $item.find('.thumbcover a').attr('title');

                    var opts = {
                        'type': 'danger',
                        'titolo': _('Elimina'),
                        'content': _('Vuoi eliminare %s %s "%s"', 'filemanager', articolo, tipo, nome),
                        'OK': 'Ok',
                        'CANCEL': 'Annulla',
                        'onOK': function ($modal) {
                            $modal.modal('hide');
                            elimina($item.data('path'));
                        },
                        'onCANCEL': null
                    };
                    HandleBarHelper.confirm(opts);

                }
            }

        }
    });

}

function menuContestualeDir() {
    $.contextMenu({
        selector: '.dir-content',
        autoHide: true,
        items: {
            new_dir: {
                name: _('Crea directory'),
                disabled: function (key, opt) {
                    if ($('#current-path').val() === '/') {
                        return true;
                    }
                },
                callback: function (key, opt) {
                    showNewDirDialog($('#current-path').val().substring(1));
                }
            }
        }
    });

}

function dirTree() {
    $directoryTree = $(".js-tree.folders");
    $directoryTree.jstree({
        'core': {
            'check_callback': true,
            'data': {
                'url': '/admin/' + moduloFileManager + '/tree',
                'type': 'post',
                'dataType': 'json',
                "error": function (r) {

                },
                'data': function (node) {
                    return {
                        'id': node.id
                    };
                },
            }
        },
        "contextmenu": {
            "items": function ($node) {
                var tree = $directoryTree.jstree(true);
                return {
                    "Create": {
                        "separator_before": false,
                        "separator_after": false,
                        "label": "Crea",
                        "action": function (data) {

                            var inst = $.jstree.reference(data.reference)
                            obj = inst.get_node(data.reference);
                            showNewDirDialog(obj.id);

                        }
                    },
                    "Remove": {
                        "separator_before": false,
                        "separator_after": false,
                        "label": "Elimina",
                        "action": function (data) {

                            var inst = $.jstree.reference(data.reference)
                            obj = inst.get_node(data.reference);

                            var opts = {
                                'type': 'danger',
                                'titolo': _('Elimina'),
                                'content': _('Vuoi eliminare %s %s "%s"', 'filemanager', _('la'), _('cartella'), obj.text),
                                'OK': 'Ok',
                                'CANCEL': 'Annulla',
                                'onOK': function ($modal) {
                                    $modal.modal('hide');
                                    elimina(obj.id);
                                },
                                'onCANCEL': null
                            };
                            HandleBarHelper.confirm(opts);

                        }
                    }
                };
            }
        },
        "force_text": true,
        'plugins': ["contextmenu", "dnd", "search", "wholerow"]
    });

    $directoryTree.on("ready.jstree", function (e, data) {
        //$directoryTree.jstree().open_all();
    });

    $directoryTree.on("activate_node.jstree", function (e, data) {
        $('#current-path').val('/' + data.node.id);
        loadDirContent(data.node.id);
    });

}

function eventiIcone() {

    $(document).on('dblclick', '.item', function (e) {

        switch ($(this).data('type')) {

            case 'dir':
            case 'dir-up':
                $('#current-path').val('/' + $(this).data('path'));
                loadDirContent($(this).data('path'));
                break;

        }

    });

}

function calcolaAltezze() {
    var righColHeight = $('.right_col').height();
    var pageTitleHeight = $('.page-title').outerHeight(true);
    var currentPathLineHeight = $('.curren-path-line').outerHeight(true);
    var paddingXpanel = $('.right_col .x_panel:first').outerHeight(true) - $('.right_col .x_panel:first').height();
    var paddingPanelFiles = $('.panel-files').outerHeight(true) - $('.panel-files').height();
    var clearFixOuterHeight = $('.right_col').find('.clearfix:first').outerHeight(true);
    var filesTitleHeight = $('.panel-files').find('.x_title').outerHeight();
    console.log(righColHeight + ' - ' + pageTitleHeight + ' - ' + currentPathLineHeight + ' - ' + clearFixOuterHeight + ' - ' + filesTitleHeight + ' - ' + paddingXpanel + ' - ' + paddingPanelFiles);
    var h = righColHeight - pageTitleHeight - currentPathLineHeight - clearFixOuterHeight - filesTitleHeight - paddingXpanel - paddingPanelFiles;
    $('.panel-tree').height(h);
    $('.panel-tree').css({'max-height': h});
    $('.dir-content').height(h);
    $('.dir-content').css({'max-height': h, 'overflow': 'auto'});


}

function setFilemanagerFilters(filters) {
    FMFilters = filters;

}