var isAdvancedUpload = function() {
    var div = document.createElement('div');
    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var formData = false;
var $dropZone = false;
var sezioneModulo = false;
$(function() {

    var sezioneModulo = window.location.getPositionalUrl(1);

    $(document).on('click', '.btn-delete-attachment', function(e) {
        e.preventDefault();
        /** In caso di successo, questa sarà la riga da rimuovere dal dom **/
        var $tr = $(this).closest('tr');
        /** Id dell'elemento da cancellare **/
        var id = $tr.data('id');
        /** Viene reperito il nome file direttamente dal DOM **/
        var nomefile = $tr.find('.filename').text();

        var opts = {
            'type': 'danger',
            'titolo': _('Eliminazione allegato'),
            'content': _('Vuoi davvero eliminare l\'allegato "%s"?<br /><strong>L\'operazione sarà irreversibile.</strong>', 'default', nomefile),
            'onOK': function($modal) {
                $modal.modal('hide');
                var opts = {
                    'type': 'danger',
                    'message': _('Cancellazione in corso del file "%s"', 'default', nomefile),
                    'icon': 'fa-spin fa-spinner'
                };
                HandleBarHelper.lockScreen(opts);

                $.ajax({
                    type: "POST",
                    url: '/admin/_attachments/' + sezioneModulo + '/delete_attachment/' + id,
                    dataType: "json",
                    success: function(ret_data) {
                        HandleBarHelper.unlockScreen();
                        if (ret_data.result) {
                            $tr.remove();
                            var params = {
                                'closeText': _('Chiudi'),
                                icon: 'check-mark-2',
                                theme: 'materialish',
                                color: 'lilrobot'
                            };
                            $.notific8(_('File "%s" rimosso', 'default', nomefile), params);
                        } else {
                            var opts = {
                                'type': 'danger',
                                'titolo': _('Attenzione'),
                                'content': ret_data.errors.join('<br />'),
                            };
                            HandleBarHelper.alert(opts);

                        }
                    }
                });
            },
        };
        HandleBarHelper.confirm(opts);
    });

    $(document).on('change', '#filesAllegati .input-alt', function(e) {
        /** Riga contenitore da evidenziare e da cui prelevare i metadati **/
        var $tr = $(this).closest('tr');
        /** Id dell'elemento da cancellare **/
        var id = $tr.data('id');
        if ($tr.find('td:eq(1)').find('.unsaved').length == 0) {
            $('<i class="unsaved fa fa-exclamation-triangle"></i>').prependTo($tr.find('td:eq(1)'));
        }
        var $saveBtn = $tr.find('.btn-save-alt');
        $saveBtn.removeClass('disabled');
        $tr.addClass('warning');

    });

    $(document).on('click', '.btn-save-alt', function(e) {
        /** Riga contenitore **/
        var $tr = $(this).closest('tr');
        /** Id dell'elemento da modificare **/
        var id = $tr.data('id');
        /** Viene reperito il nome file direttamente dal DOM **/
        var nomefile = $tr.find('.filename').text();

        var opts = {
            'type': 'danger',
            'message': _('Salvataggio del testo ALT in corso per il file "%s"', 'default', nomefile),
            'icon': 'fa-spin fa-spinner'
        };
        HandleBarHelper.lockScreen(opts);

        sendAltData(id, sezioneModulo, $tr, nomefile, function() {

            var params = {
                'closeText': _('Chiudi'),
                icon: 'check-mark-2',
                theme: 'materialish',
                color: 'lilrobot'
            };

            $.notific8(_('Testo alternativo per il file "%s" modificato', 'default', nomefile), params);

        });

    });

    $dropzone = $('#dropzone');

    if (isAdvancedUpload) {
        formData = new FormData();
    }

    var droppedFiles = false;
    var allowedExtensions = $dropzone.data('allowed-extensions').split(',');
    var fileMaxSize = $dropzone.data('file-max-size');
    var imageMinSize = $dropzone.data('image-min-size').split('x');


    $('#submitFiles').on('click', function(e) {
        e.preventDefault();
        sendFilesToServer();
    });

    $dropzone.on('droppedFiles', function() {

        console.log('$dropzone.droppedFiles[EVENT]');

        if (isAdvancedUpload) {

            var erroriUpload = [];
            var fileProcessati = 0;

            $.each(droppedFiles, function(i, file) {

                $('#submitFiles').addClass('disabled');

                reader = new FileReader();

                reader.onload = function(e) {

                    fileProcessati += 1;

                    var filePieces = e.target.fileName.split('.');
                    estensione = filePieces.pop().toLowerCase();

                    console.log($.inArray(estensione, allowedExtensions));

                    if ($.inArray(estensione, allowedExtensions) !== -1) {

                        if (e.target.size <= fileMaxSize) {

                            var data = {};

                            if ($.inArray(e.target.type, ["image/gif", "image/jpeg", "image/png"]) != -1) {
                                data.src = e.target.result;
                                data.fileAlt = _('Breve testo descrittivo');

                                var checkImg = new Image();
                                checkImg.src = data.src;

                                checkImg.onload = function() {

                                    if (checkImg.width < imageMinSize[0] || checkImg.height < imageMinSize[1]) {
                                        erroriUpload.push(_('L\'immagine %s è troppo piccola (%s, minimo richiesto %s)', 'default', e.target.fileName, checkImg.width + 'x' + checkImg.height, imageMinSize[0] + 'x' + imageMinSize[1]));
                                        data = false;
                                    }

                                    if (checkImg.width * checkImg.height > 4000000) {
                                        erroriUpload.push(_('L\'immagine %s è troppo grande', 'default', e.target.fileName));
                                        data = false;
                                    }

                                    if (data !== false) {

                                        data.filename = e.target.fileName;
                                        data.index = i;

                                        addFileToUploadList(data, i, file);

                                        $('#submitFiles').css('display', 'block');
                                        $('#submitFiles').removeClass('disabled');

                                    }

                                };

                            } else {
                                data.src = '/admin/images/mime/' + estensione + '.png';
                                data.fileAlt = _('Breve testo descrittivo');
                                addFileToUploadList(data, i, file);

                                $('#submitFiles').css('display', 'block');
                                $('#submitFiles').removeClass('disabled');
                            }


                        } else {
                            erroriUpload.push(_('Il file "%s" supera i limiti consentiti (massimo peso: %s)', 'default', e.target.fileName, humanFileSize(fileMaxSize, true)));

                        }

                    } else {
                        erroriUpload.push(_('Upload del file "%s" non consentito', 'default', e.target.fileName));
                    }


                    if (fileProcessati == droppedFiles.length) {
                        if (erroriUpload.length) {

                            var opts = {
                                'type': 'danger',
                                'titolo': _('Attenzione'),
                                'content': erroriUpload.join('<br />'),
                            };
                            HandleBarHelper.alert(opts);


                        }
                    }

                };


                reader.fileName = droppedFiles[i].name;
                reader.size = droppedFiles[i].size;
                reader.type = droppedFiles[i].type;
                reader.readAsDataURL(droppedFiles[i]);

            });

        }

    });

    $dropzone.find('input').on('change', function(e) {
        droppedFiles = e.target.files;
        sendFilesToServer(function() {
        }, 'inputFileChange');
        //$dropzone.trigger('droppedFiles');
    });

    if (isAdvancedUpload) {

        $dropzone.addClass('has-advanced-upload');

        $dropzone.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
            e.preventDefault();
            e.stopPropagation();
        })
        .on('dragover dragenter', function() {
            $dropzone.addClass('is-dragover');
        })
        .on('dragleave dragend drop', function() {
            $dropzone.removeClass('is-dragover');
        })
        .on('drop', function(e) {
            droppedFiles = e.originalEvent.dataTransfer.files;
            $dropzone.trigger('droppedFiles');
        });

    }

    $('#filesAllegati tbody').sortable({
        stop: function(event, ui) {
            sortedIDs = $('#filesAllegati tbody').sortable("toArray");
            sortedIDs = sortedIDs.join().replace(/a_/g, '');
            sortedIDs = sortedIDs.split(',');

            var data = { 'ids': sortedIDs };
            $.ajax({
                type: 'POST',
                url: '/admin/_attachments/' + sezioneModulo + '/save-attachments-order',
                data: data,
                dataType: 'json',
                success: function(ret_data) {
                    if (ret_data.result) {

                    } else {
                        var opts = {
                            'type': 'danger',
                            'titolo': _('Attenzione'),
                            'content': ret_data.errors.join('<br />'),
                            'OK': 'Ok',
                            'callback': null
                        };
                        HandleBarHelper.alert(opts);
                    }
                }
            });
            console.log(sortedIDs);

        }
    });

});

function addFileToUploadList(data, index, file) {
    if (isAdvancedUpload) {
        formData.append('files[' + index + ']', file);
    }
    var HandleBarCompiled = HandleBarHelper.compile('fileItemPreview');
    data['index'] = index;
    $(HandleBarCompiled(data)).prependTo($('#filesDaCaricare'));
}

function sendFilesToServer(cb, triggeredBy) {

    if (typeof triggeredBy == 'undefined') {
        triggeredBy = 'D&D';
    }

    var opts = {
        'type': 'danger',
        'message': _('Caricamento files in corso...'),
        'icon': 'fa-spin fa-spinner'
    };

    if (isAdvancedUpload && triggeredBy == 'D&D') {

        var errori = [];

        $('.fileData input.input-alt[data-locale="it"]').each(function() {
            var $input = $(this);
            if ($input.val() == _('Breve testo descrittivo')) {
                var $span = $(this).closest('td').find('.filename');
                errori.push(_('Non è stato specificato un testo alternativo valido per il file "%s"', 'default', $span.text()));
            }
        });

        if (errori.length == 0) {

            HandleBarHelper.lockScreen(opts);

            formData.append('id', $('form').data('id'));

            $('.fileData input').each(function() {
                var $input = $(this);
                formData.append($(this).attr('name'), $(this).val());
            });

            $.ajax({
                url: $dropzone.data('endpoint'),
                type: 'post',
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                complete: function() {
                },
                success: function(ret_data) {
                    manageSuccessResponse(ret_data);
                },
                error: function() {
                }
            });

        } else {
            var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': errori.join('<br />'),
            };
            HandleBarHelper.alert(opts);
        }


    } else {

        HandleBarHelper.lockScreen(opts);

        var iframeName = 'uploadiframe' + new Date().getTime();
        var uploadform = 'uploadForm';

        $formIframe = $('<form id="' + uploadform + '" method="post" action="' + $dropzone.data('endpoint') + '/legacy" enctype="multipart/form-data"></form>').appendTo('body');
        $('<input type="hidden" name="id" value="' + $('form:first').data('id') + '" />').appendTo($formIframe);

        $('#file').appendTo($formIframe);

        console.log($formIframe.html());
        $iframe = $('<iframe name="' + iframeName + '" style="display: none;"></iframe>');

        $('body').append($iframe);
        $formIframe.attr('target', iframeName);

        $iframe.one('load', function() {
            var data = JSON.parse($iframe.contents().find('body').text());
            manageSuccessResponse(data);
            $('#file').prependTo('.box__input');
            $formIframe.remove();
            $iframe.remove();
        });

        console.log('form.submit');

        $formIframe.submit();


    }

}

function sendAltData(id, sezioneModulo, $tr, nomefile, callback) {
    $.ajax({
        type: "POST",
        url: '/admin/_attachments/' + sezioneModulo + '/save_alt_attachment/' + id,
        data: $tr.find('input').serialize(),
        dataType: "json",
        success: function(ret_data) {
            HandleBarHelper.unlockScreen();
            if (ret_data.result) {

                $tr.removeClass('warning');
                $tr.find('.unsaved').remove();

                callback();

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function manageSuccessResponse(ret_data) {
    HandleBarHelper.unlockScreen();
    if (ret_data.result) {
        var rigaCompiled = HandleBarHelper.compile('rigaAllegati');
        $tbody = $('#filesAllegati tbody');
        for(var i = 0; i < ret_data.uploaded.length; i++) {
            $(rigaCompiled(ret_data.uploaded[i])).appendTo($tbody);
        }
        $('#filesDaCaricare tbody').empty();
        $('#submitFiles').hide();

        if (typeof(onUploadedFiles) == 'function') {
            onUploadedFiles();
        }
    } else {
        var opts = {
            'type': 'danger',
            'titolo': _('Attenzione'),
            'content': ret_data.errors.join('<br />')
        };
        HandleBarHelper.alert(opts);
    }
}