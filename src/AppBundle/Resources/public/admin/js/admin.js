/**
 * Created by gianiaz on 05/12/16.
 */
var $body = false;

$(function() {

    $body = $('body');

    /** Click sul bottone di cancellazione nelle liste di elementi **/
    $(document).on('click', '.btn-delete', function(e) {

        e.preventDefault();

        var title = $(this).data('title');
        var href = $(this).attr('href');

        var question = _('Vuoi davvero eliminare l\'elemento "%s"?', 'default', title);

        if ($(this).closest('tr').hasClass('danger') || !$body.data('rrd')) {
            question += '<br /><strong>' + _('l\'operazione sarà irreversibile') + '</strong>';
        }

        var opts = {
            'type': 'danger',
            'titolo': _('Conferma cancellazione'),
            'content': question,
            'OK': 'Ok',
            'CANCEL': 'Annulla',
            'onOK': function($modal) {
                $modal.modal('hide');
                window.location.assign(href);
            }
        };
        HandleBarHelper.confirm(opts);
    });


    $('.delFile').on('click', function(e) {
        e.preventDefault();
        var $inputGroup = $(this).closest('.input-group');
        var $formGroup = $inputGroup.closest('.form-group');
        var $markForDelete = $inputGroup.find('[type="checkbox"]');

        var $input = $inputGroup.find('input[type="text"]');
        var file = $input.val();
        if (file) {
            var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': _('Vuoi rimuovere il file "%s"?', 'layoutcontent', file),
                'OK': 'Ok',
                'CANCEL': 'Annulla',
                'onOK': function($modal) {
                    $modal.modal('hide');
                    $markForDelete.prop('checked', true);
                    $input.val('');
                    $formGroup.find('.preview-area').empty();
                },
                'onCANCEL': null
            };
            HandleBarHelper.confirm(opts);
        }

    });

    $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label, input]);
    });

    $(':file').on('fileselect', function(event, numFiles, label, input) {
        var $inputGroup = $(this).closest('.input-group');
        $inputGroup.find('[type="text"]').val(label);
        var $markForDelete = $inputGroup.find('.markForDelete');
        $markForDelete.removeAttr('checked');
        var $formGroup = $markForDelete.closest('.form-group');


        if ($formGroup.find('.preview-area').length) {
            console.log($formGroup);

            if ('FileReader' in window) {
                reader = new FileReader();
                reader.onload = function(e) {
                    $formGroup.find('.preview-area').empty();
                    estensione = reader.fileName.split('.').pop().toLowerCase();
                    if ($.inArray(estensione, ["png", "jpg", "gif", "jpeg"]) != -1) {
                        $('<img class="img-responsive" src="' + e.target.result + '" />').appendTo('.preview-area');
                    }
                };

                var file = input[0]['files'][0];
                reader.fileName = file.name;

                reader.readAsDataURL(file);

            }

        }

    });

    var seoTimeout;

//    $('.seoWidth').on('keyup', function() {
//        var string = $(this).val();
//        var $formGroup = $(this).closest('.form-group');
//        var mobile = 0;
//        var desktop = 0;
//        for(var i = 0, len = string.length; i < len; i++) {
//            var charCode = string.charCodeAt(i);
//            if (typeof(seoWidth[charCode]) != 'undefined') {
//                mobile += seoWidth[charCode]['mobile'];
//                desktop += seoWidth[charCode]['desktop'];
//            } else {
//
//                var params = {
//                    'closeText': _('Chiudi'),
//                    icon: 'check-mark-2',
//                    theme: 'materialish',
//                    color: 'lilrobot'
//                };
//                $.notific8(_('Il carattere [' + string.charAt(i) + ' (' + charCode + ')' + '] non è mappato'), params);
//            }
//        }
//        $formGroup.find('.mobileSeoCounter').val(mobile);
//        $formGroup.find('.desktopSeoCounter').val(desktop);
//
//        if ($formGroup.find('.mobileSeoCounter').data('soglia') < mobile) {
//            $formGroup.find('.mobileSeoCounter').closest('.input-group').addClass('has-error');
//            $formGroup.find('.mobileSeoCounter').closest('.input-group').removeClass('has-success');
//        } else {
//            $formGroup.find('.mobileSeoCounter').closest('.input-group').removeClass('has-error');
//            $formGroup.find('.mobileSeoCounter').closest('.input-group').addClass('has-success');
//        }
//
//        if ($formGroup.find('.desktopSeoCounter').data('soglia') < desktop) {
//            $formGroup.find('.desktopSeoCounter').closest('.input-group').addClass('has-error');
//            $formGroup.find('.desktopSeoCounter').closest('.input-group').removeClass('has-success');
//        } else {
//            $formGroup.find('.desktopSeoCounter').closest('.input-group').removeClass('has-error');
//            $formGroup.find('.desktopSeoCounter').closest('.input-group').addClass('has-success');
//        }
//
//
//    });
//
//    $('.seoWidth').trigger('keyup');

    $('.seoWidth').on('keyup', function() {
        var string = $(this).val();

        var $formGroup = $(this).closest('.form-group');
        var mobile, desktop;

        if ($(this).attr('name').indexOf('metaDescription') != -1) {
            mobile = Math.floor(getTextWidth(string, 'normal 15.8px Arial,sans-serif'));
            desktop = Math.floor(getTextWidth(string, 'normal 14.5px Arial,sans-serif'));
        } else {
            mobile = Math.floor(getTextWidth(string, 'normal 17.2px Arial,sans-serif'));
            desktop = Math.floor(getTextWidth(string, 'normal 19.5px Arial,sans-serif'));
        }
        
        $formGroup.find('.mobileSeoCounter').val(mobile);
        $formGroup.find('.desktopSeoCounter').val(desktop);
        if ($formGroup.find('.mobileSeoCounter').data('soglia') < mobile) {
            $formGroup.find('.mobileSeoCounter').closest('.input-group').addClass('has-error');
            $formGroup.find('.mobileSeoCounter').closest('.input-group').removeClass('has-success');
        } else {
            $formGroup.find('.mobileSeoCounter').closest('.input-group').removeClass('has-error');
            $formGroup.find('.mobileSeoCounter').closest('.input-group').addClass('has-success');
        }

        if ($formGroup.find('.desktopSeoCounter').data('soglia') < desktop) {
            $formGroup.find('.desktopSeoCounter').closest('.input-group').addClass('has-error');
            $formGroup.find('.desktopSeoCounter').closest('.input-group').removeClass('has-success');
        } else {
            $formGroup.find('.desktopSeoCounter').closest('.input-group').removeClass('has-error');
            $formGroup.find('.desktopSeoCounter').closest('.input-group').addClass('has-success');
        }

    });


});