var markUpPulsantieraImgOrTxt = false;
var formPrototype = false;
var markUpRows = false;
var warnings = {};
var $form;
var $tree;
var can_edit_page_rows = false;
$(function() {

    $form = $('form');
    $form.data('warningchecked', false);

    can_edit_page_rows = $form.data('can_edit_page_rows');

    sezione = window.location.getPositionalUrl(2);

    switch (sezione) {
        case 'new':
        case 'edit':
            // markup della pulsantiera per la scelta tra immagine e area testuale.
            markUpPulsantieraImgOrTxt = $('#pulsantieraImgOrTxt').html();
            //
            markUpRows = $('#rowsContainer').html();
            formPrototype = $('#formPrototype').html();
            if (can_edit_page_rows) {
                $('div[data-locale]').each(function() {
                    $(markUpRows).appendTo(this);
                });
            }

            $('#page_form_parent').on('change', function() {
                if ($(this).val() == 0) {
                    $('#page_form_level').closest('.form-group').show();
                } else {
                    $('#page_form_level').closest('.form-group').hide();
                }
            });

            $('#page_form_parent').trigger('change');

            /** Dopo la scelta del file da caricare viene iniettata l'immagine nel container */
            $(document).on('change', '.uploadImgPage', function() {
                var $uploadField = $(this);

                readFile(this, function(e) {
                    // recupero del container
                    var $container = $uploadField.closest('.containerRows');
                    // la riga che contiene il container
                    var $pageRow = $uploadField.closest('.pageRow');
                    // indice del blocco che sto modificando
                    var blockIndex = $container.parent().data('index');


                    var opts = {
                        'id': 'modalImageContainer',
                        'onShow': function($modal) {
                            // src dell'immagine appena caricata;
                            var src = e.target.result;
                            $modal.find('img').attr('src', src);

                            // nome del campo hidden che conserverà il valore dell'alt
                            var altName = 'slot' + (blockIndex + 1) + 'ImageAlt';
                            // il campo input che deve conservare l'alt per l'immagine
                            var $inputAlt = $pageRow.find('[id*="' + altName + '"]');
                            // Cerco di comporre un alt automatico

                            // tab in cui mi trovo
                            var $tabPane = $pageRow.closest('.tab-pane');
                            // campo titolo del tabpane
                            var titolo = $tabPane.find('[name*=titolo]').val();
                            if (titolo) {
                                titolo += ': ';
                            }

                            // cambio il type a text
                            $inputAlt.attr('type', 'text');
                            $inputAlt.addClass('form-control');
                            $inputAlt.val(titolo + _('Immagine ' + ($tabPane.find('.img-responsive').length + 1)));
                            // e lo posiziono sotto l'immagine
                            $inputAlt.appendTo($modal.find('.inputContainer'));

                            $modal.find('.btn-confirm').on('click', function(e) {
                                e.preventDefault();
                                if ($.trim($modal.find('input').val()) == '') {
                                    HandleBarHelper.alert({ 'content': _('Il campo alt non può essere lasciato vuoto') });
                                } else {
                                    // nascondo l'upload field
                                    $uploadField.hide();
                                    // recupero del campo structure per aggiornarne il contenuto
                                    var $structure = $pageRow.find('[id$="structure"]');
                                    // json decode del valore
                                    var structureVal = JSON.parse($structure.val());
                                    // aggiornamento del type
                                    structureVal[blockIndex]['type'] = 'img';
                                    // re-encode del value
                                    structureVal = JSON.stringify(structureVal);
                                    // ri-assegnazione del valore che verrà spedito in post
                                    $structure.val(structureVal);
                                    // il campo upload field viene spostato in una parte del DOM che non subisce modifiche
                                    $uploadField.appendTo($pageRow);
                                    // svuotamento del container per fare spazio all'immagine
                                    $container.empty();
                                    // l'immagine viene messa nel container svuotato precedentemente
                                    $('<img class="img-responsive" src="' + src + '" />').appendTo($container);
                                    // il blocco del form group al campo container
                                    var $formGroup = $inputAlt.closest('.form-group');
                                    // viene tolto l'help che non serve
                                    $formGroup.find('.help').remove();
                                    // viene appeso il gruppo al container
                                    $formGroup.appendTo($container);

                                    $modal.modal('hide');
                                    // aggiornamento dell'altezza della riga in modo che i container siano tutti uguali
                                    aggiornaAltezzaPageRow($pageRow);
                                }
                            });
                        },

                    };

                    HandleBarHelper.modal(opts);

                });

            });

            $('.titolo').on('change', function(e) {
                if (this.defaultValue != '') {
                    var $tabpane = $(this).closest('.tab-pane');
                    console.log($tabpane);
                    var longLocale = $tabpane.data('longlocale');
                    warnings[longLocale] = _('Il cambio del titolo di una pagina determina il cambio dell\'indirizzo per raggiungerla. <br />Il vecchio titolo era <strong>%s</strong> per la lingua %s<br /><strong>Attenzione all\'indicizzazione su Google!</strong>', 'pages', this.defaultValue, longLocale);
                }
            });


            /** Click sul bottone che permette di aggiungere una riga di contenuti **/
            $('.addRow').on('click', function(e) {
                e.preventDefault();
                /** il tab di lingua è stato cliccato il bottone **/
                var $tabPane = $(this).closest('.tab-pane');
                /** la lingua associata **/
                var locale = $tabPane.data('locale');
                /** id del bottone che è stato cliccato **/
                var id = $(this).attr('id');
                /** composizione dell'id contenente il template di griglia **/
                var griglia = id.replace('add', '');
                var htmlBlockId = 'row' + griglia;
                /** markup della griglia **/
                var markUpPageRow = $('#' + htmlBlockId).html();
                /** La nuova riga da aggiungere **/
                var $pageRow = $(markUpPageRow);
                /** Il nuovo index associato, prelevato dal numero di righe già presenti **/
                var indexes = [];
                $('.pageRow').each(function() {
                    indexes.push(parseInt($(this).attr('data-index'), 10));
                });
                var index = Math.max.apply(null, indexes) + 1;
                /** Il form di partenza che poi verrà modificato nel numero di indice utilizzato **/
                var newForm = formPrototype.replace(/__name__/g, index);
                /** Il nuovo form viene appeso alla pageRow **/
                $(newForm).appendTo($pageRow);
                /** id che verrà assegnato alla page row **/
                var idPageRow = 'pageRow_' + locale + '_' + index;
                $pageRow.attr('id', idPageRow);
                $pageRow.attr('data-index', index);
                /** viene aggiunta la pulsantiera per lo spostamento/eliminazione della riga **/

                $('<div class="form-group"></div>' + $('#pulsantiRow').html()).appendTo($pageRow);
                /** La pageRow viene appesa alla tab di lingua, prima della pulsantiera **/
                $pageRow.insertBefore($tabPane.find('.pulsantiera'));
                /** Aggiornamento dello stato delle singole righe **/
                disableInusableButtons();

                var indexBtn;

                /** PageRow Nel dom **/
                var $pageRowDOM = $('#' + idPageRow);


                /** I campi di upload vengono "nascosti" sotto l'icona dell'immagine per ogni blocco disponibile **/
                $pageRowDOM.find('.containerRows').each(function(indexBottone) {
                    var $markUpPulsantieraImgOrTxt = $(markUpPulsantieraImgOrTxt);
                    indexBtn = indexBottone + 1;
                    $('#page_form_pageRows_' + index + '_slot' + indexBtn + 'ImageFile').appendTo($markUpPulsantieraImgOrTxt.find('.btn-img'));
                    $markUpPulsantieraImgOrTxt.appendTo($(this));
                });

                if (can_edit_page_rows) {
                    $($('#pulsantiContainer').html()).insertAfter($pageRowDOM.find('.containerRows'));
                }

                /** I campi upload non necessari vengono rimossi dal DOM **/
                for(var i = indexBtn + 1; i <= 3; i++) {
                    // console.log('Rimuovo ' + 'page_form_pageRows_' + index + '_slot' + i + 'ImageFile');
                    $('#page_form_pageRows_' + index + '_slot' + i + 'ImageFile').remove();
                }

                /** modifica della struttura per rispecchiare il template scelto **/
                var structure = [];

                for(var i = 0, len = griglia.length; i < len; i++) {
                    var column = {
                        size: parseInt(griglia[i]),
                        'type': '-'
                    };
                    if (parseInt(griglia[i]) == 0) {
                        column['type'] = null;
                    }
                    structure.push(column);
                }

                /** Assegnazione del valore della structure */
                structure = JSON.stringify(structure);
                $pageRowDOM.find('#page_form_pageRows_' + index + '_structure').val(structure);
                /** Assegnazione del locale nel form di riga **/
                $pageRowDOM.find('#page_form_pageRows_' + index + '_locale').val(locale);
                /** Assegnazione del campo sort per indicare l'ordine di visualizzazione **/
                $pageRowDOM.find('#page_form_pageRows_' + index + '_sort').val(index);

            });


            $('.btn-main-submit').on('click', function(e) {
                e.preventDefault();

                checkForm(function() {
                    if (checkWarnings()) {
                        $form.submit();
                    }
                });

            });

            $(document).on('click', '.btn-ck', function(e) {
                e.preventDefault();
                /** La pageRow in cui è presente il bottone che è stato cliccato **/
                var $pageRow = $(this).closest('.pageRow');
                /** Il blocco tratteggiato che contiene il bottone appena cliccato **/
                var $container = $(this).closest('.containerRows');
                /** L'indice di posizione rispetto agli altri blocchi per il blocco contenitore **/
                var index = $container.parent().data('index');
                // console.log('Index: ' + index);

                // svuoto il contenitore per poterci mettere la textarea
                $container.empty();

                // Viene prelevata la textarea da spostare
                var $itemToMove = $pageRow.find('[id$="slot' + (index + 1) + '"]');
                // viene aggiunta la classe ck (da ckeditor)
                $itemToMove.addClass('ck').removeClass('hide');

                // viene prelevato il valore del campo in json che contiene la struttura della riga
                var $structure = $pageRow.find('[id$="structure"]');
                // ne viene fatto il parsing
                var structureVal = JSON.parse($structure.val());
                // viene modificato il tipo ck
                structureVal[index]['type'] = 'ck';
                // viene encodato nuovamente
                structureVal = JSON.stringify(structureVal);
                // e riassegnato al campo
                $structure.val(structureVal);

                // l'item viene aggiunto al container
                $itemToMove.appendTo($container);

                aggiornaAltezzaPageRow($pageRow);

                CKEDITOR.replace($itemToMove.get(0), {
                    customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
                });

                CKEDITOR.instances[$itemToMove.attr('id')].on('instanceReady', function(e) {
                    h = $('#' + $itemToMove.attr('id')).closest('.containerRows').height();
                    e.editor.resize('100%', h);
                });


            });


            /** disabilitazione del bottone UP per il primo elemento delle pageRows e del bottone DOWN per l'ultima riga */
            disableInusableButtons();

            /** click sul bottone che permette di spostare alla riga precedente a quella selezionata */
            $(document).on('click', '.btn-row-up', function(e) {
                e.preventDefault();
                var $thisPageRow = $(this).closest('.pageRow');
                var $elementoPrecedente = $thisPageRow.prev();
                $thisPageRow.find('.ck').each(function() {
                    CKEDITOR.instances[this.id].destroy();
                });
                $thisPageRow.insertBefore($elementoPrecedente);
                $thisPageRow.find('.ck').each(function() {
                    CKEDITOR.replace(this, {
                        customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
                    });
                    CKEDITOR.instances[this.id].on('instanceReady', function(e) {
                        h = $('#' + e.editor.name).closest('.containerRows').height();
                        e.editor.resize('100%', h);
                    });
                });
                disableInusableButtons();
            });

            /** click sul bottone che permette di spostare alla riga successiva a quella selezionata */
            $(document).on('click', '.btn-row-down', function(e) {
                e.preventDefault();
                var $thisPageRow = $(this).closest('.pageRow');
                var $elementoPrecedente = $thisPageRow.next();
                $thisPageRow.find('.ck').each(function() {
                    CKEDITOR.instances[this.id].destroy();
                });
                $thisPageRow.insertAfter($elementoPrecedente);
                $thisPageRow.find('.ck').each(function() {
                    CKEDITOR.replace(this, {
                        customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
                    });
                    CKEDITOR.instances[this.id].on('instanceReady', function(e) {
                        h = $('#' + e.editor.name).closest('.containerRows').height();
                        e.editor.resize('100%', h);
                    });
                });
                disableInusableButtons();
            });

            /** Viene creato un editor per ogni textarea che ha la classe .ck **/

            /** Per ogni istanza dell'editor viene aumentata l'altezza in base all'altezza del contenitore */
            if (typeof(CKEDITOR) !== 'undefined') {
                for(var i in CKEDITOR.instances) {
                    CKEDITOR.instances[i].on('instanceReady', function(e) {
                        console.log(i);
                        h = $('#' + i).closest('.containerRows').height();
                        e.editor.resize('100%', h);
                    });
                }
            }

            /** Click sul bottone che permette di eliminare il contenuto del container e ritornare alla situazione iniziale **/
            $(document).on('click', '.btn-reset-container', function(e) {

                $this = $(this);

                e.preventDefault();

                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': _('Vuoi eliminare il contenuto di questo blocco?'),
                    'OK': _('Ok'),
                    'CANCEL': _('Annulla'),
                    'onOK': function($modal) {
                        // questo è il box tratteggiato che potrà contenere un'immagine o un testo
                        var $container = $this.parent().find('.containerRows');
                        // questo è l'indice del container (praticamente un indice da 1 a 3 numerato da sinistra a destra
                        var indexContainer = $container.parent().data('index') + 1;
                        // questo è il contenitore della riga (l'elemento tratteggiato sottile con sfondo colorato
                        var $pageRow = $this.closest('.pageRow');

                        var $textarea = $container.find('textarea');
                        // se al click del cestino il contenuto era una textarea
                        if ($container.find('textarea').lenght) {
                            if (CKEDITOR.instances[$textarea.attr('id')]) {
                                CKEDITOR.instances[$textarea.attr('id')].destroy();
                            }
                        } else {

                            $container.empty();
                        }
                        $textarea.remove();

                        $container.height('auto');

                        // recupero dell'indice partendo da un input che è già presente in riga
                        var re = /\[([\d])\]/;
                        var indexRiga = re.exec($pageRow.find('input:first').attr('name'))[1];
                        var $formPrototype = $('<div>' + $('#formPrototype').html() + '</div>');

                        // viene prelevato il campo input file da aggiungere all'icona dal prototype, cambiando nome e id
                        var $inputImmagine = $formPrototype.find('[name*="slot' + indexContainer + 'ImageFile"]');
                        $inputImmagine.attr('name', $inputImmagine.attr('name').replace('__name__', indexRiga));
                        $inputImmagine.attr('id', $inputImmagine.attr('id').replace('__name__', indexRiga));

                        // box con le 2 icone per scegleire testo o immagine
                        $pulsantieraImgOrTxt = $($('#pulsantieraImgOrTxt').html());
                        $inputImmagine.appendTo($pulsantieraImgOrTxt.find('.btn-img'));
                        $pulsantieraImgOrTxt.appendTo($container);

                        // textarea da posizionare nella pagerow in modo da essere pronta al click dell'icona testuale
                        var $inputTextarea = $formPrototype.find('textarea[name*="slot' + indexContainer + '"]');
                        $inputTextarea.attr('name', $inputTextarea.attr('name').replace('__name__', indexRiga));
                        $inputTextarea.attr('id', $inputTextarea.attr('id').replace('__name__', indexRiga));
                        // siccome potrei aver cancellato diverse volte, la textarea potrebbe già esserci, controllo prima di inserirla.
                        if (!$pageRow.find('#' + $inputTextarea.attr('id')).length) {
                            $inputTextarea.appendTo($pageRow);
                        }

                        // blocco testuale per il testo alternativo in modo da esser pronto in caso di upload
                        var $inputAlt = $formPrototype.find('input[name*="slot' + indexContainer + 'ImageAlt"]');
                        $inputAlt.attr('name', $inputAlt.attr('name').replace('__name__', indexRiga));
                        $inputAlt.attr('id', $inputAlt.attr('id').replace('__name__', indexRiga));
                        if (!$pageRow.find('#' + $inputAlt.attr('id')).length) {
                            $inputAlt.appendTo($pageRow.find('div.hide'));
                        }

                        $modal.modal('hide');

                        aggiornaAltezzaPageRow($pageRow);
                    }
                };
                HandleBarHelper.confirm(opts);

            });

            /** Click sul bottone che elimina la riga **/
            $(document).on('click', '.btn-delete-pagerow', function(e) {
                $this = $(this);

                $pageRow = $(this).closest('.pageRow');

                e.preventDefault();

                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': _('Vuoi eliminare il contenuto di questa riga?', 'pages'),
                    'OK': _('Ok'),
                    'CANCEL': _('Annulla'),
                    'onOK': function($modal) {
                        var $tabPane = $pageRow.closest('.tab-pane');

                        $pageRow.remove();
                        if ($tabPane.find('.pageRow').length == 1) {
                            $tabPane.find('.pulsantiera-row .btn').addClass('disabled');
                        }
                        $modal.modal('hide');
                    }
                };

                HandleBarHelper.confirm(opts);

            });

            /**
             * Dopo l'attivazione di una tab viene istanziato il ck editor per le aree testuali già disponibili,
             * nel caso non ci sia nulla aggiunge una riga vuota.
             */
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                var $tabPane = $($(e.target).data("target"));
                if (!$tabPane.find('.pageRow').length) {
                    addEmptyRow($tabPane);
                } else {
                    $tabPane.find('.ck').each(function() {
                        attivaCkEditor(this);
                    });
                }
            });

            /** Aggiunta di una riga vuota nel caso il tabpane attivo non abbia righe associate **/
            if ($('.tab-pane.active').length && !$('.tab-pane.active').find('.pageRow').length) {
                addEmptyRow($('.tab-pane.active'));
            }

            /** Aggiunge le istanze di ckeditor per la tab attiva al caricamento **/
            if ($('.tab-pane.active').length) {
                var $tabPane = $('.tab-pane.active');
                $tabPane.find('.ck').each(function() {
                    attivaCkEditor(this);
                });
            }
            break;
        case 'sort':
            if ($('#sortContainer').length) {
                console.log('sorting');
                $tree = $('#sortContainer').jstree({
                    "plugins": [
                        "dnd"
                    ],
                    'core': {
                        "check_callback": true,
                        "themes": {
                            'name': 'proton',
                            'responsive': true
                        },
                        'data': {
                            "url": "/admin/pages/json_tree",
                            "dataType": "json" // needed only if you do not supply JSON headers
                        }
                    }
                }).on('loaded.jstree', function() {
                    $tree.jstree('open_all');
                });

            }

            $('#jsonExport').on('click', function(e) {

                e.preventDefault();

                var tree = JSON.stringify($tree.jstree(true).get_json('#'));

                var data = { 'tree': tree };
                $.ajax({
                    type: "POST",
                    url: "/admin/pages/save_json_tree",
                    data: data,
                    dataType: "json",
                    success: function(ret_data) {
                        if (ret_data.result) {
                            var params = {
                                'closeText': _('Chiudi'),
                                icon: 'check-mark-2',
                                theme: 'materialish',
                                color: 'lilrobot'
                            };
                            $.notific8(_('Ordine salvato'), params);
                        } else {
                            if (errors.length) {
                                var alertOptions = {
                                    'titolo': _('Attenzione'),
                                    'content': errors.join('<br />'),
                                    'OK': _('Ok'),
                                    'callback': null
                                };
                                HandleBarHelper.alert(alertOptions);
                            }
                        }
                    }
                });
            });
            break;

        case null:


            var cols = [];


            var col = {
                'title': 'Modifica',
                'className': 'dt0 dt-body-center',
                'searchable': false
            };
            cols.push(col);

            col = {
                'title': 'Titolo',
                'className': 'dt2',
                searchable: true,
                data: 'titolo'
            };

            cols.push(col);

            col = {
                'title': 'Slug',
                'className': 'dt2',
                searchable: true,
                data: 'slug'
            };

            cols.push(col);

            col = {
                'title': 'Genitore',
                'className': 'dt3',
                searchable: true,
                data: 'genitore'
            };

            cols.push(col);

            col = {
                'title': 'Template',
                'className': 'dt4',
                searchable: true,
                data: 'template'
            };

            cols.push(col);

            col = {
                'title': 'Creato',
                'className': 'dt6',
                searchable: true,
                data: 'createdAt'
            };

            cols.push(col);

            col = {
                'title': 'Ultima modifica',
                'className': 'dt7',
                searchable: true,
                data: 'updatedAt'
            };

            cols.push(col);


            col = { 'className': 'dt-body-center' };

            // placeholder cancellazione
            cols.push(col);


            var columnDefs = [];

            var columnDef = {
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'dt-body-center',
                render: function(data, type, full, meta) {
                    var toString = full.titolo;
                    return '<a href="/admin/pages/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                }
            };

            columnDefs.push(columnDef);


            columnDef = {
                targets: cols.length - 1,
                searchable: false,
                className: 'dt-body-center',
                orderable: false,
                render: function(data, type, full, meta) {
                    var toString = full.titolo;
                    console.log(full);
                    if (full.deleted) {
                        var title = _('Recupera:');
                        var ret = '<a href="/admin/pages/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                        ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                        var title = _('Elimina definitivamente: ');
                        ret += '<a href="/admin/pages/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                        if (parseInt(full.cancellabile, 10) == 0) {
                            ret += ' disabled';
                        }
                        ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                    } else {
                        var title = 'Elimina: ';
                        var ret = '<a href="/admin/pages/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                        if (parseInt(full.cancellabile, 10) == 0) {
                            ret += ' disabled';
                        }
                        ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                    }
                    return ret;
                }
            };

            columnDefs.push(columnDef);


            $('#datatable').dataTable({
                ajax: {
                    "url": "/admin/pages/json"
                },
                aaSorting: [[2, 'asc']],
                stateSave: true,
                iDisplayLength: 15,
                responsive: true,
                columns: cols,
                columnDefs: columnDefs,
                createdRow: function(row, data, index) {
                    if (data.deleted) {
                        $(row).addClass("danger");
                    }
                }
            });

            break;

    }


})
;
/** FINE document.ready **/

/**----------**/

/** FUNZIONI **/
/**----------**/
function readFile(input, cb) {
    if (input.files && input.files[0]) {

        if (input.files[0].size > $body.data('max_upload_file_size')) {

            var opts = {
                'type': 'danger',
                'content': _('Non posso permetterti di caricare un file cosi grande (%s)', 'default', humanFileSize(input.files[0].size))
            };
            HandleBarHelper.alert(opts);

        } else {

            var reader = new FileReader();

            reader.onload = function(e) {
                cb(e);
            };

            reader.readAsDataURL(input.files[0]);

        }
    } else {
        var opts = {
            'type': 'danger',
            'content': _('Il tuo browser non supporta le funzionalità')
        };
        HandlerbarHelper.alert(opts);
    }
}

$(window).load(function() {
    /**
     * Dopo il caricamento il contenitore potrebbe variare d'altezza in base al contenuto,
     * con questo codice viene uniformata l'altezza di ogni container al valore più alto
     */
    $('.pageRow').each(function() {
        aggiornaAltezzaPageRow($(this));
    });

});


/**
 * Per ogni tabpane toglie la possibilità di cliccare il bottone Su per la prima riga, e il bottone
 * giu per l'ultima riga.
 * A termine operazioni viene aggiornato il valore del sorting (questa funzione viene richiamata dopo
 * ogni spostamento di riga).
 */
function disableInusableButtons() {
    $('.tab-pane').each(function() {
        var $tabPane = $(this);
        if ($tabPane.find('.pageRow').length > 1) {
            $tabPane.find('.pageRow').find('.pulsantiera-row .btn').removeClass('disabled');

            $tabPane.find('.pageRow:first').find('.pulsantiera-row .btn-row-up').addClass('disabled');
            $tabPane.find('.pageRow:last').find('.pulsantiera-row .btn-row-down').addClass('disabled');
        } else {
            $tabPane.find('.pageRow .pulsantiera-row .btn').attr('disabled');
        }
    });


    refreshSort();
}

/**
 * Cicla le tab, e per ogni riga aggiorna il valore del campo sort in base alla posizione attuale
 * della riga.
 */
function refreshSort() {
    $('.tab-pane').each(function() {
        $(this).find('.pageRow .sortField').each(function(index) {
            $(this).val(index);
        });
    });
}

/**
 * Passato un oggetto jquery $('.pageRow'), calcola l'altezza massima del container
 * per poi decidere il valore da applicare a tutti i container della riga
 * @param $pageRow
 */
function aggiornaAltezzaPageRow($pageRow) {

    $pageRow.find('.containerRows').each(function() {
        $(this).css('height', 'auto');
    });

    var heights = [];

    $pageRow.find('.containerRows').each(function() {
        heights.push($(this).height());
    });

    var h = Math.max.apply(null, heights);

    $pageRow.find('.containerRows').each(function() {
        $(this).height(h);
    });

}

/**
 * Passato un tabpane aggiunge una riga vuota a tutta larghezza con il ckeditor già disponibile
 * @param $tabPane
 */
function addEmptyRow($tabPane) {
    /** markup della griglia **/
    var markUpPageRow = $('#row300').html();
    /** la lingua associata **/
    var locale = $tabPane.data('locale');
    /** La nuova riga da aggiungere **/
    var $pageRow = $(markUpPageRow);
    /** Il nuovo index associato, prelevato dal numero di righe già presenti **/
    var index = $('.pageRow').length;
    /** Il form di partenza che poi verrà modificato nel numero di indice utilizzato **/
    var newForm = formPrototype.replace(/__name__/g, index);
    /** Il nuovo form viene appeso alla pageRow **/
    $(newForm).appendTo($pageRow);
    /** id che verrà assegnato alla page row **/
    var idPageRow = 'pageRow_' + locale + '_' + index;
    $pageRow.attr('id', idPageRow);
    /** La pageRow viene appesa alla tab di lingua, prima della pulsantiera **/
    $pageRow.insertBefore($tabPane.find('.pulsantiera'));
    /** I campi upload non necessari vengono rimossi dal DOM **/

    /** PageRow Nel dom **/
    var $pageRowDOM = $('#' + idPageRow);

    for(var i = 1; i <= 3; i++) {
        // console.log('Rimuovo ' + 'page_form_pageRows_' + index + '_slot' + i + 'ImageFile');
        $('#page_form_pageRows_' + index + '_slot' + i + 'ImageFile').remove();
    }
    if (can_edit_page_rows) {
        $($('#pulsantiContainer').html()).insertAfter($pageRowDOM.find('.containerRows'));
    }

    /** modifica della struttura per rispecchiare il template scelto **/
    var structure = [{ size: 3, type: 'ck' }, { size: 0, type: null }, { size: 0, type: null }];

    $('<div class="form-group"></div>' + $('#pulsantiRow').html()).appendTo($pageRowDOM);

    $pageRowDOM.find('.pulsantiera-row').find('.btn').addClass('disabled');

    /** Assegnazione del valore della structure */
    structure = JSON.stringify(structure);
    $pageRowDOM.find('#page_form_pageRows_' + index + '_structure').val(structure);
    /** Assegnazione del locale nel form di riga **/
    $pageRowDOM.find('#page_form_pageRows_' + index + '_locale').val(locale);
    /** Assegnazione del campo sort per indicare l'ordine di visualizzazione **/
    $pageRowDOM.find('#page_form_pageRows_' + index + '_sort').val(index);

    // Viene prelevata la textarea da spostare
    var $itemToMove = $pageRowDOM.find('[id$="slot' + (index + 1) + '"]');
    $itemToMove.removeClass('hide');

    $container = $pageRowDOM.find('.containerRows');

    // l'item viene aggiunto al container
    $itemToMove.appendTo($container);

    CKEDITOR.replace($itemToMove.get(0), {
        customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
    });

    CKEDITOR.instances[$itemToMove.attr('id')].on('instanceReady', function(e) {
        h = $container.height();
        e.editor.resize('100%', h);
    });

}

/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {

    var $textarea = $(textarea);
    var $container = $textarea.parent();

    CKEDITOR.replace(textarea, {
        customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
    });

    var h = $container.height();

    CKEDITOR.instances[textarea.id].on('instanceReady', function(e) {
        e.editor.resize('100%', h);
    });

}


/**
 * La funzione fa il submit solo dopo aver verificato che non ci siano warning da mostrare
 * Questa funzione deve essere richiamata SOLO quando si è sicuri che il form non contiene errori;
 */
function checkWarnings() {
    var warn = [];

    for(i in warnings) {
        warn.push(warnings[i]);
    }

    if ($form.data('warningchecked')) {
        return true;
    }

    if (!$form.data('warningchecked')) {


        if (warn.length) {

            var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': warn.join('<br />'),
                'OK': _('Sono consapevole, procedi lo stesso'),
                'CANCEL': _('Annulla'),
                'onOK': function($modal) {
                    $modal.modal('hide');
                    $form.submit();
                },
                'onCANCEL': function($modal) {
                    $modal.modal('hide');
                    $form.data('warningchecked', false);

                }
            };
            HandleBarHelper.confirm(opts);

            return false;
        }

    }

    return true;

}


function checkForm(callback) {

    var errors = [];

    /** Verifico quali pageRows sono state lasciate incomplete **/
    $('.a2lix_translationsFields').children('div').each(function() {
        var longlocale = $(this).data('longlocale');
        if ($(this).find('.btn-ck').length) {
            var singolare = ($(this).find('.btn-ck').length == 1);
            var errore = false;
            if (singolare) {
                errore = _('1 blocco nella scheda "%s" non è stato compilato', 'pagine', longlocale);
            } else {
                errore = _('%d blocchi nella scheda "%s" non sono stati compilati', 'pagine', $(this).find('.btn-ck').length, longlocale);
            }
            errors.push(errore);
        }
    });

    /** Controllo se nelle page rows esiste qualche immagine il cui campo alt è stato lasciato vuoto **/
    $('.altTXT:visible').each(function() {
        if ($.trim($(this).val()) == '') {
            var fieldLabel = $(this).closest('.form-group').find('label').text();
            re = /\[([^\]]+)\]/;
            var fieldName = re.exec($(this).attr('name'))[1];
            errors.push(_('Il campo %s (%s) non può essere lasciato vuoto', 'pages', fieldLabel, fieldName));
        }
    });

    var AltVuoti = $('.input-alt').filter(function() {
        return $(this).val() == "";
    }).length;

    if (AltVuoti > 0) {
        errors.push(_('%d testi alternativi mancanti per gli allegati', 'pages', AltVuoti));
    }

    /** Se non ci sono errori vado avanti, altrimenti mi fermo e mostro i warning **/
    if (!errors.length) {

        /** Se ci sono degli allegati non salvati provo a fare il submit automatico via ajax prima di fare
         * il submit vero e proprio del form principale.
         * Il tutto funziona grazie ad una funzioen che accetta una callback, in cui decremento il numero
         * degli allegati non salvati fino ad arrivare a zero .
         */
        var contaAllegatiNonSalvati = $('.unsaved').length;

        if (contaAllegatiNonSalvati) {

            $('.unsaved').each(function() {

                /** Riga contenitore **/
                var $tr = $(this).closest('tr');
                /** Determina quale indirizzo verrà chiamato per la cancellazione **/
                var controller = $tr.data('controller');
                /** Id dell'elemento da modificare **/
                var id = $tr.data('id');
                /** Viene reperito il nome file direttamente dal DOM **/
                var nomefile = $tr.find('.filename').text();

                sendAltData(id, controller, $tr, nomefile, function() {

                    contaAllegatiNonSalvati--;

                    if (!contaAllegatiNonSalvati) {
                        if ($('#submitFiles:visible').length) {
                            sendFilesToServer(function() {
                                callback();
                            });
                        } else {
                            callback();
                        }
                    }
                });

            });

        }

        if ($('#submitFiles:visible').length) {
            sendFilesToServer(function() {
                callback();
            });
        } else {
            callback();
        }

    } else {

        if (errors.length) {
            var alertOptions = {
                'titolo': _('Attenzione'),
                'content': errors.join('<br />'),
                'OK': _('Ok'),
                'callback': null
            };
            HandleBarHelper.alert(alertOptions);
        }

    }

}
