$(function() {
    sezione = window.location.getPositionalUrl(2);

    var $uploadCrop;

    if(window.location.getPositionalUrl(1) == 'profile') {

        initUpload();

        $('#eliminaAvatar').on('click', function(e) {
            e.preventDefault();
            $('.avatarImg').find('img').attr('src', '/media/users/user.png');
            $('#eliminaAvatar').attr('disabled', true);
            $('.upload-trigger').val('');
            $('.delete-control').val(1);
            if($uploadCrop) {
                $('.upload-msg').show();
                $('.profile-photo-wrap').hide();
                //$uploadCrop.croppie('destroy');
            }
        });

        CKEDITOR.replace(document.getElementsByTagName('textarea')[0]);

    }

    if(window.location.getPositionalUrl(1) == 'operatori') {

        switch(sezione) {

            case 'edit':
            case 'new':

                var $uploadCrop;

                initUpload();

                $('#eliminaAvatar').on('click', function(e) {
                    e.preventDefault();
                    $('.avatarImg').find('img').attr('src', '/media/users/user.png');
                    $('#eliminaAvatar').attr('disabled', true);
                    $('.upload-trigger').val('');
                    $('.delete-control').val(1);
                    if($uploadCrop) {
                        $('.upload-msg').show();
                        $('.profile-photo-wrap').hide();
                        //$uploadCrop.croppie('destroy');
                    }
                });

                CKEDITOR.replace(document.getElementsByTagName('textarea')[0]);

                break;

            case null:

                var cols = [];


                var col = {
                    'title': 'Modifica',
                    'className': 'dt0 dt-body-center',
                    'searchable': false
                };
                cols.push(col);

                var col = {
                    'className': 'dt1 dt-body-center',
                    'searchable': false
                };
                cols.push(col);

                col = {
                    'title': 'Nome',
                    'className': 'dt2',
                    searchable: true,
                    data: 'nome'
                };

                cols.push(col);

                col = {
                    'title': 'Cognome',
                    'className': 'dt3',
                    searchable: true,
                    data: 'cognome'
                };

                cols.push(col);

                col = {
                    'title': 'Username',
                    'className': 'dt4',
                    searchable: true,
                    data: 'username'
                };

                cols.push(col);

                col = {
                    'title': 'Email',
                    'className': 'dt5',
                    searchable: true,
                    data: 'email'
                };

                cols.push(col);

                col = {
                    'title': 'Gruppo',
                    'className': 'dt6',
                    searchable: true,
                    data: 'gruppo'
                };

                cols.push(col);

                col = {
                    'title': 'Creato',
                    'className': 'dt7',
                    searchable: true,
                    data: 'createdAt'
                };

                cols.push(col);

                col = {
                    'title': 'Ultima modifica',
                    'className': 'dt8',
                    searchable: true,
                    data: 'updatedAt'
                };

                cols.push(col);


                col = {'className': 'dt-body-center'};

                // placeholder switch user
                cols.push(col);
                // placeholder abilitazione
                cols.push(col);
                // placeholder cancellazione
                cols.push(col);


                var columnDefs = [];

                var columnDef = {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var nome = "'" + full.nome + ' ' + full.cognome + "'";
                        return '<a href="/admin/operatori/edit/' + full.id + '" title="Modifica record: ' + nome + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                    }
                };

                columnDefs.push(columnDef);

                var columnDef = {
                    targets: 1,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var nome = "'" + full.nome + ' ' + full.cognome + "'";
                        return '<img src="' + full.avatar + '" alt="' + nome + '" class="avatarList" />';
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 3,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.nome + ' ' + full.cognome;
                        var icon = 'user';
                        var title = 'Impersona: ';
                        var disabled = '';
                        if(!full.allowedToSwitch) {
                            disabled = ' disabled';
                        }
                        var ret = '<a href="/admin/operatori?_switch_user=' + full.username + '" title="' + title + toString + '" class="btn-xs btn btn-info' + disabled + '" data-tostring="' + toString + '">';
                        ret += '<i class="fa fa-' + icon + '"></i>';
                        ret += '</a>';
                        return ret;
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 2,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.nome + ' ' + full.cognome;
                        var icon = 'check-square-o';
                        var title = 'Disabilita: ';
                        if(!full.isEnabled) {
                            var icon = 'square-o';
                            var title = 'Abilita: ';
                        }
                        var ret = '<a href="/admin/operatori/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                        ret += '<i class="fa fa-' + icon + '"></i>';
                        ret += '</a>';
                        return ret;
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 1,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.nome + ' ' + full.cognome;
                        var title = 'Elimina: ';
                        var ret = '<a href="/admin/operatori/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                        if(parseInt(full.cancellabile, 10) == 0) {
                            ret += ' disabled';
                        }
                        ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        return ret
                    }
                };

                columnDefs.push(columnDef);


                $('#datatable').dataTable({
                    ajax: {
                        "url": "/admin/operatori/json"
                    },
                    aaSorting: [[2, 'asc']],
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs
                });

                break;

        }

    }

});


function initCroppie() {
    return $('#profile-photo').croppie({
        viewport: {
            width: 128,
            height: 128,
            type: 'square'
        },
        enableExif: true
    });
}

function initUpload() {

    function readFile(input) {
        if(input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.profile-photo').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result

                }).then(function() {
                    $('#eliminaAvatar').removeAttr('disabled');

                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = initCroppie();

    $('.upload-trigger').on('change', function() {
        $uploadCrop.estensione = this.files[0].name.split('.').pop().toLowerCase();
        $('.upload-msg').hide();
        $('.profile-photo-wrap').show();
        readFile(this);
    });


    $uploadCrop.on('update', function(e) {
        var format = 'jpeg';
        if($uploadCrop.estensione == 'png') {
            format = 'png';
        }
        $('.delete-control').val(0);

        var retrievedData = $uploadCrop.croppie('get');
        var data = {points: retrievedData.points, dimensioni: [128, 128]};


        $uploadCrop.croppie('result', {
            type: 'base64',
            size: 'viewport',
            format: format
        }).then(function(resp) {
            $('.data-holder').val(JSON.stringify(data));
        });

    });
}
