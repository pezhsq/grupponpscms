// 11/01/17, 14.26
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
var modulo = 'translation';
var translateUrl = 'https://translate.yandex.net/api/v1.5/tr.json/translate';
var $dt;
var encodedStr = function(rawStr) {
    return rawStr.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
        return '&#' + i.charCodeAt(0) + ';';
    });
};

$(function() {

    $(document).on('click', '.edit-translation', function(e) {

        var $modBtn = $(this);

        var modalCompiled = HandleBarHelper.compile('newTranslationKey');

        var data = { titolo: _('Modifica chiave di traduzione') };

        $Modal = $(modalCompiled(data)).appendTo('body');

        $Modal.modal({ 'backdrop': 'static' });

        $Modal.on('shown.bs.modal', function(e) {

            $Modal.find('#translation_id').val($modBtn.data('id'));

            for(var i = 0; i < langs.length; i++) {
                short = langs[i]['short'];
                if (short == 'jp') {
                    short = 'ja';
                }
                $Modal.find('#lang-' + short).val($modBtn.data(langs[i]['short']));
            }
        });


        $Modal.find('.btn-confirm').on('click', function(e) {
            e.preventDefault();
            $Modal.find('form').ajaxSubmit({
                'beforeSubmit': function(arrayData, $form, options) {
                },
                'dataType': 'json',
                'success': function(responseText, statusText, xhr, $form) {
                    $Modal.modal('hide');
                    console.log($dt.ajax);
                    $dt.api().ajax.reload();
                }
            })
        });

        $Modal.on('hidden.bs.modal', function(e) {
            $(this).data('bs.modal', null);
            $Modal.remove();
        });


    });

    $(document).on('click', '.btn-translate-modal', function(e) {
        e.preventDefault();

        $langContainer = $(this).closest('.lang-container');
        var italiano = $('#lang-container-it').find('textarea').val();

        var data = {
            'key': $body.data('yandex'),
            'text': italiano,
            'lang': $(this).data('fromto'),
            'format': 'html'
        };

        $.getJSON(translateUrl, data, function(ret_data) {

            if (ret_data.code == 200) {

                if (ret_data.text.length) {
                    $langContainer.find('textarea').val(ret_data.text[0]);
                } else {
                    var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': _('Traduzione non trovata', 'translation'),
                        'OK': 'Ok',
                        'callback': null
                    };
                    HandleBarHelper.alert(opts);
                }

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': _('Problemi nella connessione al servizio traduzioni', 'translation'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }

        });

    });

    $('.btn-translation-new').on('click', function(e) {
        e.preventDefault();

        var modalCompiled = HandleBarHelper.compile('newTranslationKey');

        var data = { titolo: _('Nuova traduzione'), };

        $Modal = $(modalCompiled(data)).appendTo('body');

        $Modal.modal({ 'backdrop': 'static' });

        $Modal.find('.btn-confirm').on('click', function(e) {
            e.preventDefault();
            $Modal.find('form').ajaxSubmit({
                'beforeSubmit': function(arrayData, $form, options) {

                },
                'dataType': 'json',
                'success': function(responseText, statusText, xhr, $form) {
                    $Modal.modal('hide');
                    $dt.api().ajax.reload();
                }
            })
        });

        $Modal.on('hidden.bs.modal', function(e) {
            $(this).data('bs.modal', null);
            $Modal.remove();
        });


    });

    /** Datatable della pagina di elenco */
    if ($('#datatable').length) {

        $(document).on('click', '.btn-translate', function(e) {
            e.preventDefault();

            $tr = $(this).closest('tr');
            $td = $(this).closest('td');
            var italiano = $tr.find('td:eq(2)').text();

            var data = {
                'key': $body.data('yandex'),
                'text': italiano,
                'lang': $(this).data('fromto'),
                'format': 'html'
            };

            $.getJSON(translateUrl, data, function(ret_data) {

                if (ret_data.code == 200) {

                    if (ret_data.text.length) {
                        $td.find('textarea').val(ret_data.text[0]);
                        $td.find('form').submit();
                    } else {
                        var opts = {
                            'type': 'danger',
                            'titolo': _('Attenzione'),
                            'content': _('Traduzione non trovata', 'translation'),
                            'OK': 'Ok',
                            'callback': null
                        };
                        HandleBarHelper.alert(opts);
                    }

                } else {
                    var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': _('Problemi nella connessione al servizio traduzioni', 'translation'),
                        'OK': 'Ok',
                        'callback': null
                    };
                    HandleBarHelper.alert(opts);
                }

            });

        });

        $(document).on('focus', '#datatable tbody tr td textarea', function(e) {
            $tr = $(this).closest('tr');
            td = $(this).closest('td').get(0);
            if ($(td).find('.btn-translate').length == 0) {
                var idx = $tr.find('td').index(td);
                var shortCode = langs[idx - 2]['short'];
                if (shortCode == 'jp') {
                    shortCode = 'ja';
                }
                if (idx > 2) {
                    $('<button data-fromto="it-' + shortCode + '" class="btn btn-info btn-xs btn-translate"><i class="fa fa-language"></i> Traduci da italiano</button>').appendTo($(this).closest('td'));
                }
            }

            $('textarea').not(this).each(function() {

                submitEditableTextarea(this);

            });

        });

        $(document).on('keydown', '#datatable textarea', function(e) {
            var code = e.keyCode || e.which;
            $tr = $(this).closest('tr');
            td = $(this).closest('td').get(0);
            var idx = $tr.find('td').index(td);
            console.log(code);
            switch (code) {
                case 9:
                    e.preventDefault();
                    if (idx < ($tr.find('td').length - 1)) {
                        idx++;
                    } else {
                        $tr = $tr.next('tr');
                        idx = 1;
                    }
                    $tr.find('td:eq(' + idx + ')').trigger('click');
                    submitEditableTextarea(this);
                    break;
                case 40:
                    e.preventDefault();
                    $tr = $tr.next('tr');
                    $tr.find('td:eq(' + idx + ')').trigger('click');
                    submitEditableTextarea(this);

                    break;
            }

        });


        var cols = [];

        var col = {
            'title': 'Modifica',
            'className': 'dt0 dt-body-center',
            'searchable': false
        };
        cols.push(col);

        col = {
            'title': 'id',
            className: 'td0',
            searchable: true,
            data: 'id'
        };

        cols.push(col);

        var langs = $('#datatable').data('langs');

        for(var i = 0; i < langs.length; i++) {

            col = {
                'title': langs[i]['long'],
                'className': 'lang-' + langs[i]['short'],
                searchable: true,
                data: langs[i]['short']
            };

            cols.push(col);

        }

        var columnDefs = [];

        var columnDef = {
            targets: 0,
            searchable: false,
            orderable: false,
            className: 'dt-body-center',
            render: function(data, type, full, meta) {
                var toString = full.file + '.' + full.section + '.' + full.subSection + '.' + full.key;
                var markup = '<div><a href="#" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit-translation"';
                markup += ' data-id="' + toString + '"';
                for(i in full) {
                    markup += ' data-' + i + '="' + encodedStr(full[i]) + '"';
                }
                markup += '><i class="fa fa-pencil"></i></a></div>';

                return markup;
            }
        };

        columnDefs.push(columnDef);

        columnDefs.push({
                "aTargets": [2, 3, 4, 5, 6, 7, 8, 9, 10],
                "fnCreatedCell": function(td, data, allData, iRow, iCol) {
                    $(td).data('lang', langs[iCol - 2]['short']);
                },
            }
        );

        $dt = $('#datatable').dataTable({
            ajax: "/admin/" + modulo + "/json",
            aaSorting: [[0, 'asc']],
            stateSave: true,
            iDisplayLength: 15,
            responsive: true,
            columns: cols,
            columnDefs: columnDefs,

            "fnCreatedRow": function(nRow, aData, iDataIndex) {
                $(nRow).data('id', aData.file + '.' + aData.section + '.' + aData.subSection + '.' + aData.key);
            },
            fnDrawCallback: function() {
                $('#datatable tbody tr').each(function() {
                    $(this).find('td:gt(0)').editable(function(value, settings) {
                        var data = {
                            'value': value,
                            'id': $(this).closest('tr').data('id'),
                            'lang': $(this).closest('td').data('lang')
                        };
                        $.ajax({
                            type: "POST",
                            url: '/admin/translation/update',
                            data: data,
                            dataType: "json"
                        });
                        return value;
                    }, {
                        type: 'textarea',
                        onblur: 'ignore',
                        indicator: '<img src="/img/ajax-loader.gif">',
                        //tooltip: _('Clicca per modificare'),
                    });
                });
            }
        })
    }

});


function submitEditableTextarea(textarea) {

    var $textarea = $(textarea);
    var $form = $textarea.closest('form');
    var data = {
        'value': $textarea.val(),
        'id': $form.closest('tr').data('id'),
        'lang': $form.closest('td').data('lang')
    };

    $.ajax({
        type: "POST",
        'url': '/admin/translation/update',
        data: data,
        dataType: "json",
        'success': function(ret_data) {
            if (ret_data.result) {
                $form.closest('td').html(data.value);
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}