// 23/01/17, 11.34
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
var modulo = 'layoutcontent';
var index = 0;

$(function () {

    $('.dropdown-menu li').on('click', function (e) {
        e.preventDefault();
        var variable = $(this).find('.var').text();
        var hex = $(this).find('i').css('background-color');
        $dropDown = $(this).closest('.dropdown');
        $input = $dropDown.parent('div').find('input');
        $input.val(variable);
        $dropDown.find('button').css({'background-color': hex}).html(variable);
        $dropDown.data('value', variable);
        ricompilaCss();
    });

    $(document).on('click', '.btn-browse', function (e) {
        e.preventDefault();
        openFileManager($(this));
    });


    $('.btn-remove-array-element').on('click', function (e) {
        e.preventDefault();
        $container = $(this).closest('.raggruppa');
        if ($container.find('.row-field').length > 1) {
            $row = $(this).closest('.row-field');
            $row.remove();
            index = 0;
            $container.find('.row-field').each(function () {
                var $row = $(this);
                $row.find('input').each(function () {
                    var originalInputName = $(this).attr('name');
                    var re = /\[(\d+)\]/;
                    matches = re.exec(originalInputName);
                    name = originalInputName.replace(matches[0], '[' + index + ']');
                    $(this).attr('name', name);
                });
                index++;
            });


        } else {
            var opts = {
                'type': 'danger',
                'titolo': _('Attenzione', 'default'),
                'content': _("Non puoi rimuovere l'ultimo elemento", 'layoutcontent'),
                'OK': 'Ok',
                'callback': null
            };
            HandleBarHelper.alert(opts);
        }

    });

    $('.btn-add-field').on('click', function (e) {
        e.preventDefault();
        $container = $(this).closest('.raggruppa');
        $row = $container.find('.row:first');
        $clone = $row.clone();
        $clone.find('input').each(function () {
            $(this).val('');
            var index = $container.find('.row-field').length;
            $clone.find('input').attr('id', $clone.find('input').attr('id') + index);
            var originalInputName = $(this).attr('name');
            var re = /\[(\d+)\]/;
            matches = re.exec(originalInputName);
            index += parseInt(matches[1], 10);
            name = originalInputName.replace(matches[0], '[' + index + ']');
            $(this).attr('name', name);
        });
        if ($clone.find('.img-responsive').length) {
            var preview = 'http://fpoimg.com/' + $clone.data('dimensioni');
            $clone.find('.img-responsive').attr('src', preview);
        }
        $clone.insertBefore($container.find('.row-btn'));

    });


    codeMirrorOptions = {};
    codeMirrorOptions.theme = 'dracula';
    codeMirrorOptions.lineNumbers = true;
    codeMirrorOptions.mode = 'css';
    codeMirrorOptions.height = 200;
    codeMirrorOptions.readOnly = true;

    if ($('#scss').length) {
        var myCodeMirror = CodeMirror.fromTextArea($('#scss').get(0), codeMirrorOptions);

        codeMirrorOptions.mode = "xml";
        codeMirrorOptions.htmlMode = true;

        var myCodeMirror2 = CodeMirror.fromTextArea($('#twig').get(0), codeMirrorOptions);
    }

    $('.ck').each(function () {
        CKEDITOR.replace(this, {
            customConfig: '/admin/js/layout_ckeditor_' + $body.data('seo') + '.js'
        });
    });

    /** Datatable della pagina di elenco */
    if ($('#datatable').length) {

        var cols = [];

        var col = {
            'title': 'Modifica',
            'className': 'dt0 dt-body-center',
            'searchable': false
        };
        cols.push(col);

        col = {
            'title': 'Codice',
            'className': 'dt2',
            searchable: true,
            data: 'codice'
        };

        cols.push(col);

        col = {
            'title': 'Modulo',
            'className': 'dt2',
            searchable: true,
            data: 'module'
        };

        cols.push(col);

        col = {
            'title': 'Template',
            'className': 'dt2',
            searchable: true,
            data: 'template'
        };

        cols.push(col);

        col = {
            'title': 'Container',
            'className': 'dt2',
            searchable: true,
            data: 'container'
        };

        cols.push(col);

        col = {
            'title': 'Type',
            'className': 'dt2',
            searchable: true,
            data: 'type'
        };

        cols.push(col);

        col = {
            'title': 'Posizione',
            'className': 'dt2',
            searchable: true,
            data: 'position'
        };

        cols.push(col);

        if ($body.data('sa')) {

            col = {
                'title': 'Ruolo richiesto',
                'className': 'dt2',
                searchable: true,
                data: 'requiredRole'
            };

            cols.push(col);

        }

        var columnDefs = [];

        var columnDef = {
            targets: 0,
            searchable: false,
            orderable: false,
            className: 'dt-body-center',
            render: function (data, type, full, meta) {
                var toString = full.module;
                return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
            }
        };

        columnDefs.push(columnDef);

        $('#datatable').dataTable({
            ajax: {
                "url": "/admin/" + modulo + "/json"
            },
            stateSave: true,
            iDisplayLength: 15,
            responsive: true,
            columns: cols,
            columnDefs: columnDefs,
            createdRow: function (row, data, index) {
                if (data.deleted) {
                    $(row).addClass("danger");
                }
            }
        });

    }

    $('.delFileAttachment').on('click', function (e) {
        e.preventDefault();

        var $inputGroup = $(this).closest('.input-group');
        var $formGroup = $inputGroup.closest('.form-group');
        var $rowImage = $formGroup.closest('.row-image');
        var $markForDelete = $inputGroup.find('[type="checkbox"]');

        var $input = $inputGroup.find('input[type="text"]');
        var file = $input.val();
        if (file) {
            var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': _('Vuoi rimuovere il file "%s"?', 'layoutcontent', file),
                'OK': 'Ok',
                'CANCEL': 'Annulla',
                'onOK': function ($modal) {
                    $modal.modal('hide');
                    $input.val('');
                    $rowImage.find('.img-responsive').attr('src', 'http://fpoimg.com/' + $rowImage.data('dimensioni'));
                    $rowImage.find('.img-alt').val('');
                },
                'onCANCEL': null
            };
            HandleBarHelper.confirm(opts);
        }
    });

    $('iframe').on('load', function () {
        $('#preview').css('height', $('#preview').contents().find('html').outerHeight() + 4);
    });


});


function ricompilaCss() {

    var vars = {};

    $('.dropdown').each(function () {
        $divContainer = $(this).parent();
        var $input = $divContainer.find('input');
        var re = /(\$[^\]]+)/;
        var matches = re.exec($input.attr('name'));
        var name = matches[0];
        var record = {};
        vars[name] = $input.val();
    });

    var data = {'vars': vars};
    $.ajax({
        type: 'POST',
        url: '/admin/' + modulo + '/recompile-css/' + $('#id').val(),
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {
                $('iframe').contents().find('#onTheFlyStyle').remove();
                $('<style id="onTheFlyStyle">' + ret_data.css + '</style>').appendTo($('iframe').contents().find('head'));
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function openFileManager($caller) {

    fileManagerCompiled = HandleBarHelper.compile('fileManager');

    var $inputDaRiempire = $caller.closest('.input-group').find('input[type="text"]');
    var $imgRow = $inputDaRiempire.closest('.row-image');
    var $img = $imgRow.find('.img-responsive');


    var opts = {
        'type': 'info',
        'titolo': _('Filemanager'),
        'content': fileManagerCompiled(),
        'large': true,
        'OK': _('Seleziona'),
        'CANCEL': 'Annulla',
        'onOK': function ($modal) {
            var path = $modal.find('.selected').data('path');
            path = '/' + path.replace(/\\/g, '/');
            $inputDaRiempire.val(path);
            $imgRow.find('.img-responsive').attr('src', path);
            $modal.modal('hide');
        },
        'onCANCEL': null,
        'onShow': function ($modal) {
            setFilemanagerFilters({
                'extensions': $imgRow.data('extensions'),
                'dimensioni': $imgRow.data('dimensioni'),
                'tolerance': $imgRow.data('tolerance')
            });
            setEvents();
            istanziaDropzone();
            menuContestualeFiles();
            menuContestualeDir();
            dirTree();
            eventiIcone();
            loadDirContent();
            var currentPathLineHeight = $('.curren-path-line').outerHeight(true);
            var h = $modal.find('.modal-body').height() - currentPathLineHeight - 50;
            $('.panel-tree').height(h);
            $('.panel-files').height(h);

            $modal.find('.btn-confirm').addClass('disabled');
        }
    };
    HandleBarHelper.confirm(opts);

}

function onFileSelected() {
    if ($('#alertDialog').find('.selected').length) {
        $('#alertDialog').find('.btn-confirm').removeClass('disabled');
    } else {
        $('#alertDialog').find('.btn-confirm').addClass('disabled');
    }
}