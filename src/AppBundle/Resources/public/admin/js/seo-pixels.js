var seoWidth = {
    "32": { // spazio
        "desktop": 6,
        "mobile": 5
    },
    "34": { // "
        "desktop": 8,
        "mobile": 6
    },
    "39": { // '
        "desktop": 4,
        "mobile": 3
    },
    "40": { // (
        "desktop": 7,
        "mobile": 6
    },
    "41": { // (
        "desktop": 7,
        "mobile": 6
    },
    "44": { // ,
        "desktop": 6,
        "mobile": 5
    },
    "45": { // -
        "desktop": 7,
        "mobile": 6
    },
    "46": { // .
        "desktop": 6,
        "mobile": 5
    },
    "48": { // 0
        "desktop": 11,
        "mobile": 10
    },
    "49": {
        "desktop": 11,
        "mobile": 10
    },
    "50": {
        "desktop": 11,
        "mobile": 10
    },
    "51": {
        "desktop": 11,
        "mobile": 10
    },
    "52": {
        "desktop": 11,
        "mobile": 10
    },
    "53": {
        "desktop": 11,
        "mobile": 10
    },
    "54": {
        "desktop": 11,
        "mobile": 10
    },
    "55": {
        "desktop": 11,
        "mobile": 10
    },
    "56": {
        "desktop": 11,
        "mobile": 10
    },
    "57": { // 9
        "desktop": 11,
        "mobile": 10
    },
    "58": { // :
        "desktop": 6,
        "mobile": 5
    },
    "59": { // ;
        "desktop": 6,
        "mobile": 5
    },
    "63": { // ?
        "desktop": 1,
        "mobile": 1
    },
    "65": {
        "desktop": 12,
        "mobile": 11
    },
    "66": {
        "desktop": 12,
        "mobile": 11
    },
    "67": {
        "desktop": 13,
        "mobile": 11
    },
    "68": {
        "desktop": 13,
        "mobile": 12
    },
    "69": {
        "desktop": 12,
        "mobile": 10
    },
    "70": {
        "desktop": 11,
        "mobile": 9
    },
    "71": {
        "desktop": 14,
        "mobile": 12
    },
    "72": {
        "desktop": 13,
        "mobile": 12
    },
    "73": {
        "desktop": 5,
        "mobile": 5
    },
    "74": {
        "desktop": 9,
        "mobile": 5
    },
    "75": {
        "desktop": 12,
        "mobile": 10
    },
    "76": {
        "desktop": 10,
        "mobile": 9
    },
    "77": {
        "desktop": 15,
        "mobile": 13
    },
    "78": {
        "desktop": 13,
        "mobile": 12
    },
    "79": {
        "desktop": 14,
        "mobile": 13
    },
    "80": {
        "desktop": 12,
        "mobile": 10
    },
    "81": {
        "desktop": 14,
        "mobile": 13
    },
    "82": {
        "desktop": 13,
        "mobile": 11
    },
    "83": {
        "desktop": 12,
        "mobile": 10
    },
    "84": {
        "desktop": 11,
        "mobile": 9
    },
    "85": {
        "desktop": 13,
        "mobile": 12
    },
    "86": {
        "desktop": 12,
        "mobile": 11
    },
    "87": {
        "desktop": 17,
        "mobile": 15
    },
    "88": {
        "desktop": 12,
        "mobile": 11
    },
    "89": {
        "desktop": 12,
        "mobile": 11
    },
    "90": {
        "desktop": 11,
        "mobile": 10
    },
    "97": { // a minuscola
        "desktop": 10,
        "mobile": 9
    },
    "98": {
        "desktop": 10,
        "mobile": 10
    },
    "99": {
        "desktop": 9,
        "mobile": 9
    },
    "100": {
        "desktop": 10,
        "mobile": 10
    },
    "101": {
        "desktop": 10,
        "mobile": 9
    },
    "102": {
        "desktop": 5,
        "mobile": 6
    },
    "103": {
        "desktop": 10,
        "mobile": 10
    },
    "104": {
        "desktop": 10,
        "mobile": 10
    },
    "105": {
        "desktop": 4,
        "mobile": 3
    },
    "106": {
        "desktop": 4,
        "mobile": 3
    },
    "107": {
        "desktop": 9,
        "mobile": 9
    },
    "108": {
        "desktop": 4,
        "mobile": 4
    },
    "109": {
        "desktop": 15,
        "mobile": 13
    },
    "110": {
        "desktop": 10,
        "mobile": 9
    },
    "111": {
        "desktop": 10,
        "mobile": 9
    },
    "112": {
        "desktop": 10,
        "mobile": 9
    },
    "113": {
        "desktop": 10,
        "mobile": 9
    },
    "114": {
        "desktop": 6,
        "mobile": 5
    },
    "115": {
        "desktop": 9,
        "mobile": 8
    },
    "116": {
        "desktop": 5,
        "mobile": 4
    },
    "117": {
        "desktop": 10,
        "mobile": 9
    },
    "118": {
        "desktop": 9,
        "mobile": 8
    },
    "119": {
        "desktop": 13,
        "mobile": 12
    },
    "120": {
        "desktop": 9,
        "mobile": 8
    },
    "121": {
        "desktop": 9,
        "mobile": 8
    },
    "122": { // z minuscola
        "desktop": 9,
        "mobile": 8
    },
    "124": { // | pipe
        "desktop": 1,
        "mobile": 1
    },
    "160": { // ,
        "desktop": 6,
        "mobile": 5
    },
    "224": { // à
        "desktop": 10,
        "mobile": 9
    },
    "232": { // è
        "desktop": 11,
        "mobile": 9
    },
    "236": { // ì
        "desktop": 5,
        "mobile": 3
    },
    "242": { // ò
        "desktop": 11,
        "mobile": 10
    },
    "249": { // ù
        "desktop": 11,
        "mobile": 10
    }
};