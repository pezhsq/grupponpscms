var modulo = 'vetrina';

$(function() {

        sezione = window.location.getPositionalUrl(2);

        switch (sezione) {
            case 'new':
            case 'edit':
                /** ATTIVAZIONE CK EDITOR **/

                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                    var $tabPane = $($(e.target).data("target"));
                    $tabPane.find('.ck').each(function() {
                        attivaCkEditor(this);
                    });
                });

                /** Aggiunge le istanze di ckeditor per la tab attiva al caricamento **/
                if ($('.tab-pane.active').length) {
                    var $tabPane = $('.tab-pane.active');
                    $tabPane.find('.ck').each(function() {
                        attivaCkEditor(this);
                    });
                }

                $('#vetrina_form_category').data('selected', $('#vetrina_form_category').val());

                $('#vetrina_form_category').on('change', function(e) {

                    var $select = $(this);

                    var newValue = $select.val();

                    var dimensioneNuova = $select.find('[value="' + $select.val() + '"]').data('dimensioni');

                    if ($('#listImgContainer').find('.img-responsive').length) {

                        $select.val($select.data('selected'));

                        var dimensioneVecchia = $select.find('[value="' + $select.val() + '"]').data('dimensioni');

                        var $panel = $('#listImgContainer');

                        if (dimensioneVecchia != dimensioneNuova) {

                            var opts = {
                                'type': 'danger',
                                'titolo': _('Attenzione'),
                                'content': _('Stai cambiando la categoria e di conseguenza le dimensioni dell\'immagine allegata richieste. (da %s a %s)<br />Se procedi l\'immagine attuale verrà scartata e dovrai caricarne una nuova', 'vetrina', dimensioneVecchia, dimensioneNuova),
                                'OK': _('Ok, Procedi'),
                                'CANCEL': 'Annulla',
                                'onOK': function($modal) {

                                    removePanelImage($panel);

                                    $select.data('selected', newValue);
                                    $select.val(newValue);
                                    $modal.modal('hide');
                                    $('#listImgContainer').data('dimensions', dimensioneNuova);

                                },
                                'onCANCEL': null
                            };
                            HandleBarHelper.confirm(opts);

                        } else {
                            $('#listImgContainer').data('dimensions', dimensioneNuova);
                        }

                    } else {
                        $('#listImgContainer').data('dimensions', dimensioneNuova);
                    }

                });

                $('#vetrina_form_category').trigger('change');

                /** Check validità del form **/
                $('.btn-main-submit').on('click', function(e) {
                    return checkForm();
                });

                break;

            case null:
                var cols = [];

                var col = {
                    'title': 'Modifica',
                    'className': 'dt0 dt-body-center',
                    'searchable': false
                };
                cols.push(col);

                col = {
                    'title': 'Nome',
                    'className': 'dt2',
                    searchable: true,
                    data: 'name'
                };

                cols.push(col);

                col = {
                    'title': 'Categoria',
                    'className': 'dt2',
                    searchable: true,
                    data: 'categoria'
                };

                cols.push(col);

                col = {
                    'title': 'Order apparizione',
                    'className': 'dt2',
                    searchable: true,
                    data: 'sort'
                };

                cols.push(col);

                col = {
                    'title': 'Creato',
                    'className': 'dt6',
                    searchable: true,
                    data: 'createdAt'
                };

                cols.push(col);

                col = {
                    'title': 'Ultima modifica',
                    'className': 'dt7',
                    searchable: true,
                    data: 'updatedAt'
                };

                cols.push(col);

                col = { 'className': 'dt-body-center' };

                // placeholder toggle
                cols.push(col);

                // placeholder cancellazione
                cols.push(col);

                var columnDefs = [];

                var columnDef = {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var toString = full.name;
                        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 2,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.name;
                        var icon = 'check-square-o';
                        var title = 'Disabilita: ';
                        if (!full.isEnabled) {
                            var icon = 'square-o';
                            var title = 'Abilita: ';
                        }
                        var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                        ret += '<i class="fa fa-' + icon + '"></i>';
                        ret += '</a>';
                        return ret;
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 1,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.name;
                        console.log(full);
                        if (full.deleted) {
                            var title = _('Recupera:');
                            var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                            var title = _('Elimina definitivamente: ');
                            ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        } else {
                            var title = 'Elimina: ';
                            var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        }
                        return ret;
                    }
                };

                columnDefs.push(columnDef);


                $('#datatable').dataTable({
                    ajax: {
                        "url": "/admin/" + modulo + "/json"
                    },
                    aaSorting: [[3, 'asc']],
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs,
                    createdRow: function(row, data, index) {
                        if (data.deleted) {
                            $(row).addClass("danger");
                        }
                    }
                });

                break;

            case 'sort':
                $('#categoria').on('change', function(e) {
                    e.preventDefault();
                    if ($(this).val() == 0) {
                        $('.sortList').remove();
                        $('.help-block-default').show();
                        $('.help-block-assente').addClass('hide');
                    } else {

                        HandleBarHelper.lockScreen();
                        $.ajax({

                            type: 'POST',
                            url: '/admin/' + modulo + '/getSlides/' + $(this).val(),
                            dataType: 'json',
                            success: function(ret_data) {
                                if (ret_data.result) {
                                    HandleBarHelper.unlockScreen();

                                    $('.help-block-default').hide();
                                    $('.sortList').remove();

                                    if (ret_data.data.items.length > 1) {
                                        compiled = HandleBarHelper.compile('sort');
                                        $(compiled(ret_data.data)).appendTo('.toSort');
                                        $('.sortList ul').sortable();
                                        $('.btn-main-submit').removeClass('disabled');
                                        $('.help-block-assente').addClass('hide');
                                    } else {
                                        $('.help-block-assente').removeClass('hide');
                                    }

                                } else {
                                    var opts = {
                                        'type': 'danger',
                                        'titolo': _('Attenzione'),
                                        'content': ret_data.errors.join('<br />'),
                                        'OK': 'Ok',
                                        'callback': null
                                    };
                                    HandleBarHelper.alert(opts);
                                }
                            }
                        });
                    }
                });

                $('.save-sort').on('click', function(e) {
                    e.preventDefault();
                    var sorted = $('.ui-sortable').sortable("toArray");

                    var data = { 'sorted': sorted };
                    HandleBarHelper.lockScreen();

                    $.ajax({
                        type: 'POST',
                        url: '/admin/' + modulo + '/save-sort',
                        data: data,
                        dataType: 'json',
                        success: function(ret_data) {
                            if (ret_data.result) {
                                HandleBarHelper.unlockScreen();
                                var params = {
                                    'closeText': _('Chiudi'),
                                    icon: 'check-mark-2',
                                    theme: 'materialish',
                                    color: 'lilrobot'
                                };

                                $.notific8(_('Ordinamento per la categoria "%s" salvato', 'default', $('#categoria option:selected').text()), params);
                            } else {
                                var opts = {
                                    'type': 'danger',
                                    'titolo': _('Attenzione'),
                                    'content': ret_data.errors.join('<br />'),
                                    'OK': 'Ok',
                                    'callback': null
                                };
                                HandleBarHelper.alert(opts);
                            }
                        }
                    });

                });

                break;

        }


    }
);

/** FINE document.ready **/


/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {

    var id = textarea.id;

    if (typeof(CKEDITOR.instances[id]) == 'undefined') {

        CKEDITOR.replace(textarea, {
            customConfig: '/bundles/app/admin/js/layout_ckeditor_' + $body.data('seo') + '.js'
        });

    }

}

function checkForm() {

    var errori = [];

    if ($.trim($('#vetrina_form_name').val()) == '') {
        var label = $.trim($('#vetrina_form_name').closest('.form-group').find('label').text().replace(/\s+/g, ' '));
        errori.push(_('Il campo %s è obbligatorio', 'default', label));
    }

    /** Controllo di tutti i campi con attributo required nelle tab **/
    $('.tab-content .tab-pane').each(function() {
        var shortLocale = $(this).data('locale');
        var longLocale = $(this).data('longlocale');

        $(this).find('[required]').each(function() {
            if ($.trim($(this).val()) == '') {
                var label = $.trim($(this).closest('.form-group').find('label').text().replace(/\s+/g, ' '));
                errori.push(_('Il campo %s è obbligatorio (%s)', 'default', label, longLocale));
            }
        });
    });

    /** Testo alternativo per l'eventuale immagine, controllo solo se i campi sono visibili **/
    $('.alt:visible input').each(function() {
        if ($.trim($(this).val()) == '') {
            var label = $.trim($(this).closest('.form-group').find('label').text().replace(/\s+/g, ' '));
            errori.push(_('Il campo %s è obbligatorio', 'default', label));
        }
    });

    if (errori.length) {
        var opts = {
            'type': 'danger',
            'titolo': _('Attenzione'),
            'content': errori.join('<br />'),
            'OK': 'Ok'
        };
        HandleBarHelper.alert(opts);


        return false;

    }

    return true;

}
