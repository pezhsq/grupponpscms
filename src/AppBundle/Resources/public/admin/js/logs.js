// 28/09/17, 15.18
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
var modulo = 'logs';


$(function() {

    sezione = window.location.getPositionalUrl(2);

    switch (sezione) {

        case null:

            /** Datatable della pagina di elenco */
            var cols = [];

            var col = {
                'title': 'Modifica',
                'className': 'dt0 dt-body-center',
                'searchable': false
            };
            cols.push(col);

            col = {
                'title': 'Entity',
                'className': 'dt2',
                searchable: true,
                data: 'entity'
            };

            cols.push(col);

            col = {
                'title': 'Id',
                'className': 'dt2',
                searchable: true,
                data: 'entityId'
            };

            cols.push(col);

            col = {
                'title': 'Action',
                'className': 'dt2',
                searchable: true,
                data: 'action'
            };

            cols.push(col);

            col = {
                'title': 'Autore',
                'className': 'dt2',
                searchable: true,
                data: 'author'
            };

            cols.push(col);

            col = {
                'title': 'Testo',
                'className': 'never',
                searchable: true,
                data: 'message'
            };

            cols.push(col);

            col = {
                'title': 'Creato',
                'className': 'dt6',
                searchable: true,
                data: 'createdAt'
            };

            cols.push(col);

            var columnDefs = [];

            var columnDef = {
                targets: 0,
                searchable: false,
                orderable: false,
                className: 'dt-body-center',
                render: function(data, type, full, meta) {
                    return '<a href="#' + full.id + '" title="Visualizza dettaglio" class="btn btn-xs btn-primary view"><i class="fa fa-eye"></i></a>';
                }
            };

            columnDefs.push(columnDef);

            $('#datatable').dataTable({
                ajax: {
                    "url": "/admin/" + modulo + "/json"
                },
                aaSorting: [[6, 'desc']],
                stateSave: true,
                iDisplayLength: 15,
                responsive: true,
                columns: cols,
                columnDefs: columnDefs,
                createdRow: function(row, data, index) {
                    if (data.deleted) {
                        $(row).addClass("danger");
                    }
                }
            });

            $(document).on('click', '.view', function(e) {
                e.preventDefault();
                var $tr = $(this).closest('tr');
                HandleBarHelper.alert({
                    titolo: $tr.find('td:eq(6)').text() + ' - ' + $tr.find('td:eq(4)').text(),
                    content: $tr.find('td:eq(5)').html()
                })

            });


    }
});

function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}