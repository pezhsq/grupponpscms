var modulo = 'gallery';

$(function() {

    sezione = window.location.getPositionalUrl(2);

    switch (sezione) {

        case 'new':
        case 'edit':

            $(document).on('keyup', '.slug', function(e) {
                this.value = this.value.toLowerCase().replace(/\s/g, '-');
            });

            $(document).on('change', '.slug', function(e) {
                console.log(e);
                this.value = this.value.toLowerCase().replace(/\s/g, '-');
            });


            /** ATTIVAZIONE CK EDITOR **/

            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                var $tabPane = $($(e.target).data("target"));
                $tabPane.find('.ck').each(function() {
                    attivaCkEditor(this);
                });
            });

            /** Aggiunge le istanze di ckeditor per la tab attiva al caricamento **/
            if ($('.tab-pane.active').length) {
                var $tabPane = $('.tab-pane.active');
                $tabPane.find('.ck').each(function() {
                    attivaCkEditor(this);
                });
            }

            break;

        case 'images':

            getImages();


            myDropzone = new Dropzone('.img-container', {
                url: "/admin/" + modulo + "/upload",
                createImageThumbnails: true,
                autoProcessQueue: true,
                previewTemplate: '<div style="display:none"></div>'
            });

            myDropzone.on('sending', function(file, xhr, formData) {
                    formData.append('id', $('.immagini').data('id'));
                }
            );

            myDropzone.on('success', function(file, ret_data) {
                    if (ret_data.result) {

                        getImages();

                    } else {
                        var opts = {
                            'type': 'danger',
                            'titolo': _('Attenzione'),
                            'content': ret_data.errors.join('<br />'),
                            'OK': 'Ok',
                            'callback': null
                        };
                        HandleBarHelper.alert(opts);
                    }
                }
            );

            $(document).on('click', '.btn-delete-img', function(e) {
                e.preventDefault();
                $box = $(this).closest('.image-box');
                name = $box.find('p').text();
                var opts = {
                    'type': 'danger',
                    'titolo': _('Conferma cancellazione', 'gallery'),
                    'content': _('Vuoi cancellare l\'immagine "%s"', 'gallery', name),
                    'OK': 'Ok',
                    'CANCEL': 'Annulla',
                    'onOK': function($Modal) {
                        $Modal.modal('hide');
                        HandleBarHelper.lockScreen({ 'message': _('Attendere prego') });
                        deleteImage($box.data('id'));
                    },
                    'onCANCEL': null
                };
                HandleBarHelper.confirm(opts);

            });

            $(document).on('click', '.btn-edit-img', function(e) {
                e.preventDefault();
                $box = $(this).closest('.image-box');
                editImg($box.data('id'));
            });

            break;

        case null:

            /** Datatable della pagina di elenco */
            if ($('#datatable').length) {

                var cols = [];

                var col = {
                    'title': 'Modifica',
                    'className': 'dt0 dt-body-center',
                    'searchable': false
                };
                cols.push(col);

                col = {
                    'title': 'Titolo',
                    'className': 'dt2',
                    searchable: true,
                    data: 'titolo'
                };

                cols.push(col);

                col = {
                    'title': 'Immagini',
                    'className': 'dt2',
                    searchable: true,
                    data: 'immagini'
                };

                cols.push(col);

                col = {
                    'title': 'Creato',
                    'className': 'dt6',
                    searchable: true,
                    data: 'createdAt'
                };

                cols.push(col);

                col = {
                    'title': 'Ultima modifica',
                    'className': 'dt7',
                    searchable: true,
                    data: 'updatedAt'
                };

                cols.push(col);
                col = { 'className': 'dt-body-center' };

                // placeholder cancellazione
                cols.push(col);

                // placeholder cancellazione
                cols.push(col);

                var columnDefs = [];

                var columnDef = {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 5,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.name;
                        var ret = '<a href="/admin/' + modulo + '/images/' + full.id + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                        ret += '<i class="fa fa-plus"></i>&nbsp;' + _('Gestisci immagini (%d)', 'gallery', full.immagini) + '&nbsp;<i class="fa fa-image"></i>';
                        ret += '</a>';
                        return ret;
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 2,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.name;
                        var icon = 'check-square-o';
                        var title = 'Disabilita: ';
                        if (!full.isEnabled) {
                            var icon = 'square-o';
                            var title = 'Abilita: ';
                        }
                        var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
                        ret += '<i class="fa fa-' + icon + '"></i>';
                        ret += '</a>';
                        return ret;
                    }
                };

                columnDefs.push(columnDef);

                columnDef = {
                    targets: cols.length - 1,
                    searchable: false,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var toString = full.titolo;
                        if (full.deleted) {
                            var title = _('Recupera:');
                            var ret = '<a href="/admin/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                            var title = _('Elimina definitivamente: ');
                            ret += '<a href="/admin/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        } else {
                            var title = 'Elimina: ';
                            var ret = '<a href="/admin/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                            if (parseInt(full.cancellabile, 10) == 0) {
                                ret += ' disabled';
                            }
                            ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                        }
                        return ret;
                    }
                };

                columnDefs.push(columnDef);


                $('#datatable').dataTable({
                    ajax: {
                        "url": "/admin/" + modulo + "/json"
                    },
                    mRender: function(data, type, full) {
                        if (type === 'sort') return data.Sort;
                        return data.Display
                    },
                    stateSave: true,
                    iDisplayLength: 15,
                    responsive: true,
                    columns: cols,
                    columnDefs: columnDefs,
                    createdRow: function(row, data, index) {
                        if (data.deleted) {
                            $(row).addClass("danger");
                        }
                    }
                });

            }

            break;

    }


});
/** FINE document.ready **/


/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {

    var id = textarea.id;

    if (typeof(CKEDITOR.instances[id]) == 'undefined') {

        CKEDITOR.replace(textarea, {
            customConfig: '/bundles/app/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
        });

    }

}


function getImages() {
    HandleBarHelper.lockScreen({ 'message': _('Attendere prego') });
    $.ajax({
        type: 'POST',
        url: '/admin/' + modulo + '/images-list/' + $('.immagini').data('id'),
        dataType: 'json',
        success: function(ret_data) {
            if (ret_data.result) {
                $container = $('.img-container');
                $container.empty();
                markup = '';
                if (ret_data.items.length) {
                    compiled = HandleBarHelper.compile('image');
                    for(var i = 0; i < ret_data.items.length; i++) {
                        markup += compiled(ret_data.items[i]);
                    }
                    $(markup).appendTo($container);
                }

                console.log('attivo sortable');
                $('.img-container').sortable({
                    items: '.sortList',
                    placeholder: "helper col-md-4",
                    helper: 'clone',
                    stop: function(event, ui) {
                        sortedIDs = $('.img-container').sortable("toArray");
                        sortedIDs = sortedIDs.join().replace(/a_/g, '');
                        sortedIDs = sortedIDs.split(',');

                        var data = { 'ids': sortedIDs };
                        $.ajax({
                            type: 'POST',
                            url: '/admin/' + modulo + '/save-sort',
                            data: data,
                            dataType: 'json',
                            success: function(ret_data) {
                                if (ret_data.result) {

                                } else {
                                    var opts = {
                                        'type': 'danger',
                                        'titolo': _('Attenzione'),
                                        'content': ret_data.errors.join('<br />'),
                                        'OK': 'Ok',
                                        'callback': null
                                    };
                                    HandleBarHelper.alert(opts);
                                }
                            }
                        });
                        console.log(sortedIDs);

                    }
                });


                HandleBarHelper.unlockScreen();

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}
function deleteImage(id) {
    $.ajax({
        type: 'POST',
        url: '/admin/' + modulo + '/delete-img/' + id,
        dataType: 'json',
        success: function(ret_data) {
            if (ret_data.result) {
                HandleBarHelper.unlockScreen();
                getImages();
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });
}

function editImg(id) {
    HandleBarHelper.lockScreen({ 'message': _('Attendere prego') });
    $.ajax({
        type: 'POST',
        url: '/admin/' + modulo + '/edit-img/' + id,
        dataType: 'text',
        success: function(ret_data) {

            HandleBarHelper.unlockScreen();

            var settings = {
                size: '',
                markup: '<div style="padding:1em">' + ret_data + '</div>',
                title: _('Modifica dati immagine'),
                onShow: null,
                onOk: function($Modal) {
                    $Modal.find('form').ajaxSubmit({
                        'success': function(ret_data) {
                            if (ret_data.result) {
                                $Modal.modal('hide');
                                getImages();
                            } else {
                                var opts = {
                                    'type': 'danger',
                                    'titolo': _('Attenzione'),
                                    'content': ret_data.errors.join('<br />'),
                                    'OK': 'Ok',
                                    'callback': null
                                };
                                HandleBarHelper.alert(opts);
                            }
                        }
                    });

                },
                onCancel: null,
                cancelLbl: _('Annula'),
                okLbl: _('Ok'),
                modalOptions: {
                    backdrop: 'static',
                    keyboard: false
                },
                parentElement: 'body'
            };

            HandleBarHelper.modalForm(settings);
        }
    });
}