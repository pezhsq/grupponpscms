$(function() {
    $('.panelImg').on('click', '.btn-delete-img', function(e) {
        e.preventDefault();
        var $panel = $(this).closest('.panelImg');
        var formName = $panel.data('formname');
        var opts = {
            'type': 'danger',
            'titolo': _('Cancellazione immagine', 'default'),
            'content': _('Vuoi eliminare l\'immagine?<br /><strong>N.B.<br />L\'operazione sarà irreversibile</strong>'),
            'onOK': function($modal) {
                removePanelImage($panel);
                $modal.modal('hide');
            }
        };
        HandleBarHelper.confirm(opts);
    });

    /**
     * Click sul bottone per la scelta dell'immagine di elenco
     */
    $('.panelImg').on('change', '[type="file"]', function(e) {
        var $panel = $(this).closest('.panelImg');
        var dimensioni = $panel.data('dimensions');
        readImg(this, dimensioni, $panel.attr('id'));
    });
});


/**
 * Riceve in ingresso il file selezionato per l'upload
 */
function readImg(input, dimensioni, idElemento) {

    var d = dimensioni.split('x');

    if (input.files && input.files[0]) {

        if (input.files[0].size > $body.data('max_upload_file_size')) {

            var opts = {
                'type': 'danger',
                'titolo': _('Dimensioni file non consentite'),
                'content': _('Non posso permetterti di caricare un file cosi grande (%s), <strong>il massimo consentito è %s</strong>', 'default', humanFileSize(input.files[0].size, true), humanFileSize(1000000, true))
            };
            HandleBarHelper.alert(opts);

        } else {

            var modalTitle = _('Caricamento immagine', 'default');
            var defaultAlt = 'Immagine';

            var alt = '';

            $('.titolo').each(function() {
                if ($.trim($(this).val()) != '') {
                    alt += $.trim($(this).val());
                    alt += ': ';
                    return false;
                }
            });

            alt += defaultAlt;

            var reader = new FileReader();

            reader.onload = function(e) {

                if (dimensioni != 'nessuna') {

                    var opts = {
                            'id': 'modalCroppie',
                            'data': { 'title': modalTitle },
                            'onShow': function($modal) {

                                //$modal.find('#form_headerImgAlt').closest('.form-group').remove();

                                var w = d[0] / 2;
                                var h = d[1] / 2;

                                $modal.find('input[type="text"]').val(alt);

                                $('.croppie').height(h + h * (20 / 100));

                                var croppieOpts = {
                                    viewport: {
                                        width: w,
                                        height: h,
                                        type: 'square'
                                    },
                                    boundary: {
                                        width: w,
                                        height: h
                                    },
                                    enableExif: true
                                };

                                var $croppie = $modal.find('.croppie').croppie(croppieOpts);

                                $croppie.croppie('bind', {
                                    url: e.target.result
                                });

                                $modal.find('.btn-cancel').on('click', function(e) {
                                    $(input).val('');
                                    $modal.modal('hide');
                                });

                                $modal.find('.btn-confirm').on('click', function(e) {
                                        e.preventDefault();

                                        var retrievedData = $croppie.croppie('get');
                                        var data = { points: retrievedData.points, dimensioni: dimensioni.split('x') }

                                        $croppie.croppie('result', {
                                            type: 'base64',
                                            size: { width: d[0], height: d[1] },
                                            format: 'jpeg'
                                        }).then(function(resp) {

                                            var $imgContainer = $('#' + reader.idElemento);
                                            $imgContainer.find('.crop-data').val(JSON.stringify(data));

                                            var AltImpostato = $modal.find('#alt').val();

                                            $imgContainer.find('.missingImage').addClass('hide');
                                            $imgContainer.find('.delete').val('0');
                                            $imgContainer.find('.btn-delete-img').removeClass('disabled');
                                            $imgContainer.find('.img-responsive').remove();

                                            $('<img class="img-responsive" src="' + resp + '" />').prependTo($imgContainer.find('.x_content'));
                                            $imgContainer.find('.alt').closest('.form-group').parent().removeClass('hide');
                                            $imgContainer.find('.alt').val(AltImpostato);

                                            $croppie.croppie('destroy');
                                            $modal.modal('hide');

                                        });

                                    }
                                );
                            }
                        }
                    ;
                    HandleBarHelper.modal(opts);
                } else {
                    $('#' + reader.idElemento).val(e.target.result);
                    var $imgContainer = $('#' + reader.idElemento);
                    var formName = $imgContainer.data('formname');
                    $imgContainer.find('.missingImage').addClass('hide');
                    $imgContainer.find('.img-responsive').remove();
                    $imgContainer.find('.delete').val('0');
                    $imgContainer.find('.btn-delete-img').removeClass('disabled');
                    $('<img class="img-responsive" src="' + e.target.result + '" />').prependTo($imgContainer);
                    $('#' + formName + reader.idElemento + 'Alt').closest('.form-group').parent().removeClass('hide');
                    $('#' + formName + reader.idElemento + 'Alt').val(alt);
                }

            }
            reader.idElemento = idElemento;
            reader.readAsDataURL(input.files[0]);
        }
    }
    else {
        HandleBarHelper.alert({ 'content': _('Il tuo browser non supporta le funzionalità richieste da questo componente.') });
    }
}


function removePanelImage($panel) {
    $panel.find('img').remove();
    $panel.find('.form-group').parent().addClass('hide');
    $panel.find('.delete').val(1);
    $panel.find('.btn-delete-img').addClass('disabled');
    $panel.find('.missingImage').removeClass('hide');
}

