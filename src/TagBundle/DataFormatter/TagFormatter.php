<?php
// 17/03/17, 15.43
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\DataFormatter;

use AppartamentiBundle\Entity\Appartamento;
use TagBundle\Entity\Tag;
use AppBundle\DataFormatter\DataFormatter;

class TagFormatter extends DataFormatter
{

    var $Servizi;

    public function getData()
    {

        $data['servizi'] = [];
        $data['label']   = 'Non configurato';

        $idCategoria = false;

        if (is_numeric($this->data['it']['categoria_servizi']['val'])) {


            $Categoria = $this->em->getRepository('TagBundle:CategoriaTag')->findOneBy(
                ['id' => $idCategoria]
            );

            if ($Categoria) {

                $idCategoria = $this->data['it']['categoria_servizi']['val'];

                $data['label'] = $Categoria->translate()->getNome();

            }


        } else {

            $CategoriaTranslation = $this->em->getRepository(
                'TagBundle:CategoriaTag'
            )->findOneBy(['nome' => $this->data['it']['categoria_servizi']['val']]);

            if ($CategoriaTranslation) {

                $Categoria = $CategoriaTranslation->getTranslatable();

                $data['label'] = $Categoria->translate()->getNome();

                $idCategoria = $Categoria->getId();
            }

        }

        if ($idCategoria && isset($this->Servizi[$idCategoria])) {
            $data['servizi'] = $this->Servizi[$idCategoria];
        }

        return $data;

    }


    public function extractData()
    {

        if (isset($this->AdditionalData['Entity']) && !$this->Servizi) {

            /**
             * @var $Appartamento Appartamento
             */
            $Appartamento = $this->AdditionalData['Entity'];

            $Servizi = $Appartamento->getServizi();

            $this->Servizi = [];

            foreach ($Servizi as $Servizio) {
                /**
                 * @var $Servizio Tag;
                 */

                $idCategoria = $Servizio->getCategoria()->getId();

                if (!isset($this->Servizi[$idCategoria])) {
                    $this->Servizi[$idCategoria] = [];
                }

                $this->Servizi[$idCategoria][] = $Servizio;

            }

        }
    }


}