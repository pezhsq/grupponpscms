<?php
// 17/03/17, 16.22
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\EntityListener;


use TagBundle\Entity\Tag;
use AppBundle\Service\WebDir;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

class TagListener
{

    private $rootDir;

    /**
     * TagListener constructor.
     */
    public function __construct(WebDir $webDir)
    {

        $this->rootDir = $webDir->get();

    }


    /**
     * @ORM\PostPersist()
     * @param Tag $Tag
     * @param LifecycleEventArgs $args
     */
    public function postPersist(Tag $Tag, LifecycleEventArgs $args)
    {

        if (!is_dir($this->rootDir.'/'.$Tag->getUploadDir())) {

            mkdir($this->rootDir.'/'.$Tag->getUploadDir(), 0755, true);

        }

        if ($Tag->getImageName()) {
            rename(
                $this->rootDir.'/'.$Tag->getUploadDir().'/../'.$Tag->getImageName(),
                $this->rootDir.'/'.$Tag->getUploadDir().'/'.$Tag->getImageName()
            );
        }


    }

}