<?php
// 09/03/17, 14.52
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Repository;


use Doctrine\ORM\EntityRepository;

class TagRepository extends EntityRepository
{

    function findAllNotDeleted()
    {

        return $this->createQueryBuilder('c')
            ->andWhere('c.deletedAt is NULL')
            ->getQuery()
            ->execute();
    }

    function findByLike($term, $excluded = null, $locale = 'it')
    {


        $qb = $this->createQueryBuilder('s')
            ->leftJoin(
                'TagBundle\Entity\TagTranslation',
                'st',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'st.translatable = s.id AND st.locale = :locale'
            )->setParameter('locale', $locale)
            ->where('s.isEnabled = 1')
            ->andWhere('st.nome LIKE :term')
            ->setParameter('term', $term.'%');

        if ($excluded) {
            $qb->andWhere($qb->expr()->notIn('s.id', $excluded));
        }

        $qb->addOrderBy('st.nome', 'ASC');


        return $qb->getQuery()
            ->getResult();

    }

    function findByName($nome, $locale = 'it')
    {

        $qb = $this->createQueryBuilder('s')
            ->leftJoin(
                'TagBundle\Entity\TagTranslation',
                'st',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'st.translatable = s.id AND st.locale = :locale'
            )->setParameter('locale', $locale);

        if (is_array($nome)) {
            $qb->where('st.nome IN (:nomi)')
                ->setParameter('nomi', $nome);
        } else {
            $qb->where('st.nome = :nome')
                ->setParameter('nome', $nome);
        }

        return $qb->getQuery()->getResult();


    }


}