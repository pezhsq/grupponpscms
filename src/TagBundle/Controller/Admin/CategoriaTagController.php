<?php
// 09/03/17, 14.30
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Controller\Admin;


use TagBundle\Entity\CategoriaTag;
use TagBundle\Form\CategoriaTagForm;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_CATEGORIE_TAG')")
 */
class CategoriaTagController extends Controller
{

    /**
     * @Route("/categorie-tag", name="categorie_tag")
     */
    public function listAction()
    {

        return $this->render('@Tag/CategoriaTag/list.html.twig');

    }

    /**
     * @Route("/categorie-tag/json", name="categorie_tag_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $CategoriaList = $em->getRepository('TagBundle:CategoriaTag')->findAll();
        } else {
            $CategoriaList = $em->getRepository('TagBundle:CategoriaTag')->findAllNotDeleted();
        }

        $retData = [];

        $categorie = [];

        foreach ($CategoriaList as $Category) {
            /**
             * @var $Category CategoriaTag
             */
            $record              = [];
            $record['id']        = $Category->getId();
            $record['nome']      = $Category->translate($request->getLocale())->getNome();
            $record['deleted']   = $Category->isDeleted();
            $record['createdAt'] = $Category->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Category->getUpdatedAt()->format('d/m/Y H:i:s');

            $categorie[] = $record;
        }

        $retData['data'] = $categorie;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/categorie-tag/new", name="categorie_tag_new")
     * @Route("/categorie-tag/edit/{id}",  name="categorie_tag_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $CategoriaServizi = new CategoriaTag();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $CategoriaServizi = $em->getRepository('TagBundle:CategoriaTag')->findOneBy(['id' => $id]);

            if (!$CategoriaServizi) {

                return $this->redirectToRoute('categorie_tag_new');
            }

        }

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(CategoriaTagForm::class, $CategoriaServizi, ['langs' => $langs]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $CategoriaServizi CategoriaTag
             */

            $CategoriaServizi = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($CategoriaServizi);
            $em->flush();


            $elemento = $CategoriaServizi->translate($request->getLocale())->getNome();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.creata'));

            return $this->redirectToRoute('categorie_tag');

        }

        $view = '@Tag/CategoriaTag/new.html.twi';

        if ($CategoriaServizi) {
            $view = '@Tag/CategoriaTag/edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);

    }


    /**
     * @Route("/categorie-tag/restore/{id}", name="categorie_tag_restore")
     */
    public function restoreAction(Request $request, CategoriaTag $categoriaServizi)
    {

        if ($categoriaServizi->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $categoriaServizi->translate($request->getLocale())->getNome().' ('.$categoriaServizi->getId(
                ).')';

            $categoriaServizi->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.ripristinata'));

        }

        return $this->redirectToRoute('categorie_tag');

    }


    /**
     * @Route("/categorie-tag/delete/{id}/{force}", name="categorie_tag_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, CategoriaTag $categoriaServizi)
    {

        if ($categoriaServizi) {

            $elemento = $categoriaServizi->translate($request->getLocale())->getNome();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($categoriaServizi->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get(
                    'force'
                ) == 1
            ) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($categoriaServizi);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Categoria "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$categoriaServizi->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));

                $em->remove($categoriaServizi);
                $em->flush();


            }

        }

        return $this->redirectToRoute('categorie_tag');

    }

}
