<?php
// 09/03/17, 17.45
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Controller\Admin;

use TagBundle\Entity\Tag;
use TagBundle\Form\TagForm;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Knp\DoctrineBehaviors\ORM\SoftDeletable\SoftDeletableSubscriber;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_TAG')")
 */
class TagController extends Controller
{

    /**
     * @Route("/tag", name="tag")
     */
    public function listAction()
    {

        return $this->render('TagBundle:Tag:list.html.twig');

    }

    /**
     * @Route("/tag/json", name="tag_list_json")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RESTORE_DELETED')) {
            $TagList = $em->getRepository('TagBundle:Tag')->findAll();
        } else {
            $TagList = $em->getRepository('TagBundle:Tag')->findAllNotDeleted();
        }

        $retData = [];

        $tag = [];

        foreach ($TagList as $Tag) {
            /**
             * @var $Tag Tag
             */
            $record              = [];
            $record['id']        = $Tag->getId();
            $record['nome']      = $Tag->translate($request->getLocale())->getNome();
            $record['categoria'] = $Tag->getCategoria()->translate()->getNome();
            $record['deleted']   = $Tag->isDeleted();
            $record['createdAt'] = $Tag->getCreatedAt()->format('d/m/Y H:i:s');
            $record['updatedAt'] = $Tag->getUpdatedAt()->format('d/m/Y H:i:s');

            $tag[] = $record;
        }

        $retData['data'] = $tag;

        return new JsonResponse($retData);

    }

    /**
     * @Route("/tag/new", name="tag_new")
     * @Route("/tag/edit/{id}",  name="tag_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $Tag = new Tag();

        $id = null;

        if ($request->get('id')) {

            $id = $request->get('id');

            $Tag = $em->getRepository('TagBundle:Tag')->findOneBy(['id' => $id]);

            if (!$Tag) {

                return $this->redirectToRoute('tag_new');
            }

        }

        $langs = $this->get('app.languages')->getActiveLanguages();

        $form = $this->createForm(TagForm::class, $Tag, ['langs' => $langs]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $translator = $this->get('translator');

            $elemento = $Tag->translate($request->getLocale())->getNome();

            if ($Tag->getId()) {
                $this->addFlash('success', 'Tag "'.$elemento.'" '.$translator->trans('default.labels.modificato'));
            } else {
                $this->addFlash('success', 'Tag "'.$elemento.'" '.$translator->trans('default.labels.creato'));
            }

            /**
             * @var $Tag Tag
             */

            $Tag = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($Tag);

            $em->flush();

            return $this->redirectToRoute('tag');

        }

        $view = 'TagBundle:Tag:new.html.twig';

        if ($Tag->getId()) {

            $view = 'TagBundle:Tag:edit.html.twig';
        }

        return $this->render($view, ['form' => $form->createView()]);

    }


    /**
     * @Route("/tag/restore/{id}", name="tag_restore")
     */
    public function restoreAction(Request $request, Tag $Tag)
    {

        if ($Tag->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $Tag->translate($request->getLocale())->getNome().' ('.$Tag->getId().')';

            $Tag->restore();

            $em->flush();

            $translator = $this->get('translator');

            $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.ripristinata'));

        }

        return $this->redirectToRoute('tag');

    }


    /**
     * @Route("/tag/delete/{id}/{force}", name="tag_delete",  requirements={"id" = "\d+"}, defaults={"force" = false}))
     */
    public function deleteAction(Request $request, Tag $Tag)
    {

        if ($Tag) {

            $elemento = $Tag->translate($request->getLocale())->getNome();

            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();

            if ($Tag->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED') && $request->get('force') == 1) {

                // initiate an array for the removed listeners
                $originalEventListeners = [];

                // cycle through all registered event listeners
                foreach ($em->getEventManager()->getListeners() as $eventName => $listeners) {

                    foreach ($listeners as $listener) {
                        if ($listener instanceof SoftDeletableSubscriber) {

                            // store the event listener, that gets removed
                            $originalEventListeners[$eventName] = $listener;

                            // remove the SoftDeletableSubscriber event listener
                            $em->getEventManager()->removeEventListener($eventName, $listener);
                        }
                    }

                }

                // remove the entity
                $em->remove($Tag);

                try {

                    $em->flush();

                    $translator = $this->get('translator');

                    $this->addFlash(
                        'success',
                        'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata')
                    );


                } catch (ForeignKeyConstraintViolationException $e) {

                    $this->addFlash(
                        'error',
                        'Categoria "'.$elemento.'" '.$translator->trans('categoria_vetrina.errors.non_cancellabile')
                    );

                }


            } elseif (!$Tag->isDeleted()) {

                $translator = $this->get('translator');

                $this->addFlash('success', 'Categoria "'.$elemento.'" '.$translator->trans('default.labels.eliminata'));

                $em->remove($Tag);
                $em->flush();


            }

        }

        return $this->redirectToRoute('tag');

    }


    /**
     * @Route("/tag/autocomplete-json", name="tag_autocomplete_list_json")
     */
    public function listAutocompleteJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $TagList = $em->getRepository('TagBundle:Tag')->findByLike($request->get('term'), $request->get('excluded'));

        $retData = [];

        $tag = [];

        foreach ($TagList as $Tag) {

            /**
             * @var $Tag Tag
             */

            $Categoria = $Tag->getCategoria()->translate()->getNome();

            $record          = [];
            $record['id']    = $Tag->getId();
            $record['value'] = $Tag->translate($request->getLocale())->getNome();
            $record['label'] = $Categoria.': '.$Tag->translate($request->getLocale())->getNome();

            $tag[] = $record;
        }

        $retData = $tag;

        return new JsonResponse($retData);

    }

}
