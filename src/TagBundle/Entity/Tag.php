<?php
// 09/03/17, 14.47
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="TagBundle\Repository\TagRepository")
 * @ORM\EntityListeners({"TagBundle\EntityListener\TagListener"})
 * @ORM\Table(name="tag")
 * @Vich\Uploadable()
 */
class Tag
{

    use ORMBehaviours\Timestampable\Timestampable,
        ORMBehaviours\Translatable\Translatable,
        ORMBehaviours\Sortable\Sortable,
        ORMBehaviours\SoftDeletable\SoftDeletable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TagBundle\Entity\CategoriaTag")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $valore;

    /**
     * @Vich\UploadableField(mapping="tag", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    public function __construct()
    {

        $this->appartamenti = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {

        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {

        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {

        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     */
    public function setImageFile($imageFile = null)
    {

        $this->imageFile = $imageFile;

        if ($imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {

        return $this->imageName;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {

        $this->imageName = $imageName;
    }

    /**
     * @return mixed
     */
    public function getValore()
    {

        return $this->valore;
    }

    /**
     * @param mixed $valore
     */
    public function setValore($valore)
    {

        $this->valore = $valore;
    }

    public function __toString()
    {

        return (string)$this->translate()->getNome();
    }

    function getUploadDir()
    {

        return 'files/servizi/'.$this->getId().'/';

    }


}