<?php
// 09/03/17, 14.59
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="categorie_tag_translations")
 */
class CategoriaTagTranslation
{

    use ORMBehaviours\Translatable\Translation;

    public function __call($method, $arguments)
    {

        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $nome;

    /**
     * @return mixed
     */
    public function getNome()
    {

        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {

        $this->nome = $nome;
    }

}