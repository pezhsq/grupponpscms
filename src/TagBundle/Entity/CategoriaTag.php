<?php
// 09/03/17, 14.44
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="TagBundle\Repository\CategoriaTagRepository")
 * @ORM\Table(name="categorie_tag")
 */
class CategoriaTag
{

    use ORMBehaviours\Timestampable\Timestampable,
        ORMBehaviours\Translatable\Translatable,
        ORMBehaviours\SoftDeletable\SoftDeletable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="TagBundle\Entity\Tag", mappedBy="categoria")
     * @ORM\OrderBy({"sort" ="ASC"})
     */
    private $tags;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    public function __construct()
    {

        $this->servizi = new ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getServizi()
    {

        return $this->servizi;
    }

    /**
     * @param mixed $servizi
     */
    public function setServizi($servizi)
    {

        $this->servizi = $servizi;
    }

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {

        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {

        $this->isEnabled = $isEnabled;

    }

    public function __toString()
    {

        $nome = ($this->translate()->getNome()) ? $this->translate()->getNome() : (string)$this->getId();

        return $nome;

    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {

        /**
         * @var $object CategoriaTag
         */
        $object = $context->getObject();

        $translations = $object->getTranslations();

        foreach ($translations as $translation) {
            if (false == $translation->getNome()) {
                $context->buildViolation('newscat.titolo_empty_no_lang')
                    ->atPath('translations['.$translation->getLocale().'].nome')
                    ->addViolation();
                $context->buildViolation('newscat.titolo_empty', ['lang' => $translation->getLocale()])
                    ->atPath('translations_'.$translation->getLocale().'_nome')
                    ->addViolation();
            }
        }

    }


}