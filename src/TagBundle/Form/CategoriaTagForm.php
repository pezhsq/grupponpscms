<?php
// 09/03/17, 15.21
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace TagBundle\Form;


use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use TagBundle\Entity\CategoriaTag;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoriaTagForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'isEnabled',
            ChoiceType::class,
            [
                'label'       => 'categoria_tag.labels.is_public',
                'choices'     => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required'    => false,
            ]
        );

        $fields = [
            'nome' => [
                'label'    => 'categoria_tag.labels.titolo',
                'required' => true,
                'attr'     => ['class' => 'titolo'],
            ],
        ];

        $builder->add(
            'translations',
            TranslationsType::class,
            [
                'locales'          => array_keys($options['langs']),
                'fields'           => $fields,
                'required_locales' => array_keys($options['langs']),
                'label'            => false,
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => CategoriaTag::class,
                'langs'      => ['it' => 'Italiano'],
            ]
        );

    }

}