<?php
// 13/04/17, 10.54
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AnagraficaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviours;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Webtek\EcommerceBundle\Entity\Product;

/**
 * Class Anagrafica
 * @ORM\Entity(repositoryClass="AnagraficaBundle\Repository\AnagraficaRepository")
 * @ORM\Table(name="anagrafica")
 */
class Anagrafica
{

    use ORMBehaviours\Loggable\Loggable, ORMBehaviours\SoftDeletable\SoftDeletable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAzienda;

    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Nazioni")
     * @ORM\JoinColumn(referencedColumnName="countryCode")
     */
    private $nazione;

    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Provincia")
     */
    private $provincia;

    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Comune")
     */
    private $comune;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $comune_text;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $provincia_text;

    /**
     * @ORM\Column(type="string")
     */
    private $codice_fiscale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $partita_iva;

    /**
     * @ORM\Column(type="string")
     */
    private $cap;

    /**
     * @ORM\Column(type="string")
     */
    private $indirizzo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ragione_sociale;

    /**
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    /**
     * @ORM\OneToMany(targetEntity="AnagraficaBundle\Entity\IndirizzoAnagrafica", mappedBy="anagrafica",
     *     cascade={"persist", "remove"})
     * @ORM\OrderBy({"isDefault" = "DESC"})
     */
    private $indirizzi;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isGuest;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dataNascita;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $telefono;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $cellulare;

    /**
     * @ORM\Column(type="boolean" )
     */
    private $isNewsletterSubscribed;


    /**
     * @ORM\ManyToMany(targetEntity="Webtek\EcommerceBundle\Entity\Product", inversedBy="wishlist_holders")
     * @ORM\JoinTable(name="wishlist",
     *   inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *   joinColumns={@ORM\JoinColumn(name="anagrafica_id", referencedColumnName="id")}
     * )
     */
    private $wishlistProducts;

    /**
     * @ORM\OneToMany(targetEntity="Webtek\EcommerceBundle\Entity\Recensione", mappedBy="anagrafica",
     *     cascade={"persist", "remove"})
     */
    private $recensioni;

    /**
     * @return mixed
     */
    public function getRecensioni()
    {

        return $this->recensioni;
    }

    /**
     * @param mixed $recensioni
     */
    public function setRecensioni($recensioni)
    {

        $this->recensioni = $recensioni;
    }

    /**
     * @return mixed
     */
    public function getWishlistProducts()
    {

        return $this->wishlistProducts;
    }

    /**
     * @param mixed $products
     */
    public function setWishlistProducts($products)
    {

        $this->wishlistProducts = $products;
    }

//    public function getProdottiAttivi()
//    {
//
//        $criteria = Criteria::create()->where(Criteria::expr()->eq('isEnabled', 1));
//
//        $return = $this->products->matching($criteria);
//
//        return $return;
//
//    }
    public function addWishlistProduct(Product $product)
    {

        if (!$this->wishlistProducts->contains($product)) {
            $this->wishlistProducts->add($product);
        }

    }

    public function removeWishlistProduct(Product $product)
    {

        if ($this->wishlistProducts->contains($product)) {
            $this->wishlistProducts->removeElement($product);
        }

    }

    /**
     * Anagrafica constructor.
     */
    public function __construct()
    {

        $this->registeredAt = new \DateTime();
        $this->indirizzi = new ArrayCollection();
        $this->isGuest = false;
        $this->isAzienda = false;
        $this->wishlistProducts = new ArrayCollection();
        $this->recensioni = new ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getTipoAnagrafica()
    {

        return $this->tipoAnagrafica;
    }

    /**
     * @param mixed $tipoAnagrafica
     */
    public function setTipoAnagrafica($tipoAnagrafica)
    {

        $this->tipoAnagrafica = $tipoAnagrafica;
    }

    /**
     * @return mixed
     */
    public function getCodiceFiscale()
    {

        return $this->codice_fiscale;
    }

    /**
     * @param mixed $codice_fiscale
     */
    public function setCodiceFiscale($codice_fiscale)
    {

        $this->codice_fiscale = strtoupper($codice_fiscale);
    }

    /**
     * @return mixed
     */
    public function getPartitaIva()
    {

        return $this->partita_iva;
    }

    /**
     * @param mixed $partita_iva
     */
    public function setPartitaIva($partita_iva)
    {

        $this->partita_iva = $partita_iva;
    }

    /**
     * @return mixed
     */
    public function getRagioneSociale()
    {

        return $this->ragione_sociale;
    }

    /**
     * @param mixed $ragione_sociale
     */
    public function setRagioneSociale($ragione_sociale)
    {

        $this->ragione_sociale = $ragione_sociale;
    }

    /**
     * @return mixed
     */
    public function getRegisteredAt()
    {

        return $this->registeredAt;
    }

    /**
     * @return mixed
     */
    public function getIndirizzi()
    {

        return $this->indirizzi;
    }

    /**
     * @param mixed $indirizzi
     */
    public function setIndirizzi($indirizzi)
    {

        $this->indirizzi = $indirizzi;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {

        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {

        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsAzienda()
    {

        return $this->isAzienda;
    }

    /**
     * @param mixed $isAzienda
     */
    public function setIsAzienda($isAzienda)
    {

        $this->isAzienda = $isAzienda;
    }

    /**
     * @return mixed
     */
    public function getNazione()
    {

        return $this->nazione;
    }

    /**
     * @param mixed $nazione
     */
    public function setNazione($nazione)
    {

        $this->nazione = $nazione;
    }

    /**
     * @return mixed
     */
    public function getProvincia()
    {

        return $this->provincia;
    }

    /**
     * @param mixed $provincia
     */
    public function setProvincia($provincia)
    {

        $this->provincia = $provincia;
        $this->provincia_text = '';

    }

    /**
     * @return mixed
     */
    public function getComune()
    {

        return $this->comune;
    }

    /**
     * @param mixed $comune
     */
    public function setComune($comune)
    {

        $this->comune = $comune;
        $this->comune_text = '';
    }

    /**
     * @return mixed
     */
    public function getComuneText()
    {

        return $this->comune_text;
    }

    /**
     * @param mixed $comune_text
     */
    public function setComuneText($comune_text)
    {

        $this->comune_text = $comune_text;
        $this->comune = null;

    }

    /**
     * @return mixed
     */
    public function getProvinciaText()
    {

        return $this->provincia_text;
    }

    /**
     * @param mixed $provincia_text
     */
    public function setProvinciaText($provincia_text)
    {

        $this->provincia_text = $provincia_text;
        $this->provincia = null;
    }

    /**
     * @return mixed
     */
    public function getCap()
    {

        return $this->cap;
    }

    /**
     * @param mixed $cap
     */
    public function setCap($cap)
    {

        $this->cap = $cap;
    }

    /**
     * @return mixed
     */
    public function getIndirizzo()
    {

        return $this->indirizzo;
    }

    /**
     * @param mixed $indirizzo
     */
    public function setIndirizzo($indirizzo)
    {

        $this->indirizzo = $indirizzo;
    }

    /**
     * @return mixed
     */
    public function getisGuest()
    {

        return $this->isGuest;
    }

    /**
     * @param mixed $isGuest
     */
    public function setIsGuest($isGuest)
    {

        $this->isGuest = $isGuest;

    }

    /**
     * @return mixed
     */
    public function getDataNascita()
    {

        return $this->dataNascita;
    }

    /**
     * @param mixed $dataNascita
     */
    public function setDataNascita($dataNascita)
    {

        $this->dataNascita = $dataNascita;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {

        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {

        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getCellulare()
    {

        return $this->cellulare;
    }

    /**
     * @param mixed $cellulare
     */
    public function setCellulare($cellulare)
    {

        $this->cellulare = $cellulare;
    }

    /**
     * @return mixed
     */
    public function getisNewsletterSubscribed()
    {

        return $this->isNewsletterSubscribed;
    }

    /**
     * @param mixed $isNewsletterSubscribed
     */
    public function setIsNewsletterSubscribed($isNewsletterSubscribed)
    {

        $this->isNewsletterSubscribed = $isNewsletterSubscribed;
    }


    public function getProvinciaString()
    {

        if ($this->getProvincia()) {
            return (string)$this->getProvincia();
        } else {
            return (string)$this->getProvinciaText();
        }

    }

    public function getComuneString()
    {

        if ($this->getComune()) {
            return (string)$this->getComune();
        } else {
            return (string)$this->getComuneText();
        }

    }


    public function __toString()
    {

        $string = '';
        if ($this->getIsAzienda()) {
            $string .= $this->getRagioneSociale();
        } else {
            $string .= $this->user->getNome().' '.$this->user->getCognome();
        }
        $string .= "\n";
        $string .= $this->getIndirizzo();
        $string .= "\n";
        $string .= $this->getCap().' '.$this->getComuneString().' ('.$this->getProvinciaString(
            ).') - '.$this->getNazione();
        $string .= "\n";
        $string .= 'C.F.: '.$this->getCodiceFiscale();
        if ($this->getIsAzienda()) {
            $string .= ' - P.I: '.$this->getPartitaIva();
        }

        return (string)$string;
    }


}