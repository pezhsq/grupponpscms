<?php
// 13/04/17, 16.56
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AnagraficaBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class IndirizzoAnagrafica
 * @package AnagraficaBundle\Entity
 * @ORM\Entity(repositoryClass="AnagraficaBundle\Repository\IndirizzoAnagraficaRepository")
 * @ORM\EntityListeners({"AnagraficaBundle\EntityListeners\IndirizzoAnagraficaListener"})
 * @ORM\Table("indirizzi_anagrafica")
 */
class IndirizzoAnagrafica
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AnagraficaBundle\Entity\Anagrafica", inversedBy="indirizzi")
     * @var $anagrafica Anagrafica
     */
    private $anagrafica;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $presso;

    /**
     * @ORM\Column(type="string")
     */
    private $cap;

    /**
     * @ORM\Column(type="string")
     */
    private $indirizzo;
    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Comune")
     */
    private $comune;
    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Provincia")
     */
    private $provincia;
    /**
     * @ORM\Column(type="string")
     */
    private $comune_text;
    /**
     * @ORM\Column(type="string")
     */
    private $provincia_text;
    /**
     * @ORM\ManyToOne(targetEntity="GeoBundle\Entity\Nazioni")
     * @ORM\JoinColumn(referencedColumnName="countryCode")
     */
    private $nazione;
    /**
     * @ORM\Column(type="string")
     */
    private $sort;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isDefault;

    /**
     * @return mixed
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAnagrafica()
    {

        return $this->anagrafica;
    }

    /**
     * @param mixed $anagrafica
     */
    public function setAnagrafica($anagrafica)
    {

        $this->anagrafica = $anagrafica;
    }

    /**
     * @return mixed
     */
    public function getCap()
    {

        return $this->cap;
    }

    /**
     * @param mixed $cap
     */
    public function setCap($cap)
    {

        $this->cap = $cap;
    }

    /**
     * @return mixed
     */
    public function getIndirizzo()
    {

        return $this->indirizzo;
    }

    /**
     * @param mixed $indirizzo
     */
    public function setIndirizzo($indirizzo)
    {

        $this->indirizzo = $indirizzo;
    }

    /**
     * @return mixed
     */
    public function getComune()
    {

        return $this->comune;
    }

    /**
     * @param mixed $comune
     */
    public function setComune($comune)
    {

        $this->comune = $comune;
        $this->comune_text = '';
    }

    /**
     * @return mixed
     */
    public function getProvincia()
    {

        return $this->provincia;
    }

    /**
     * @param mixed $provincia
     */
    public function setProvincia($provincia)
    {

        $this->provincia = $provincia;
        $this->provincia_text = '';
    }

    /**
     * @return mixed
     */
    public function getComuneText()
    {

        return $this->comune_text;
    }

    /**
     * @param mixed $comune_text
     */
    public function setComuneText($comune_text)
    {

        $this->comune_text = $comune_text;
        $this->comune = null;
    }

    /**
     * @return mixed
     */
    public function getProvinciaText()
    {

        return $this->provincia_text;
    }

    /**
     * @param mixed $provincia_text
     */
    public function setProvinciaText($provincia_text)
    {

        $this->provincia_text = $provincia_text;
        $this->provincia = null;
    }

    /**
     * @return mixed
     */
    public function getNazione()
    {

        return $this->nazione;
    }

    /**
     * @param mixed $nazione
     */
    public function setNazione($nazione)
    {

        $this->nazione = $nazione;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {

        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {

        $this->sort = $sort;
    }

    /**
     * @return mixed
     */
    public function getIsDefault()
    {

        return $this->isDefault;
    }

    /**
     * @param mixed $isDefault
     */
    public function setIsDefault($isDefault)
    {

        $this->isDefault = $isDefault;
    }

    /**
     * @return mixed
     */
    public function getPresso()
    {

        return $this->presso;
    }

    /**
     * @param mixed $presso
     */
    public function setPresso($presso)
    {

        $this->presso = $presso;
    }

    public function getProvinciaString()
    {

        if ($this->getProvincia()) {
            return (string)$this->getProvincia();
        } else {
            return (string)$this->getProvinciaText();
        }

    }

    public function getComuneString()
    {

        if ($this->getComune()) {
            return (string)$this->getComune();
        } else {
            return (string)$this->getComuneText();
        }

    }


    public function __toString()
    {

        $string = '';
        if ($this->getPresso()) {
            $string .= $this->anagrafica->getUser()->getNome().' '.$this->anagrafica->getUser()->getCognome();
            $string .= "\n";
            $string .= 'c/o '.$this->getPresso();
        } else {
            if ($this->anagrafica->getIsAzienda()) {
                $string .= $this->anagrafica->getRagioneSociale();
            } else {
                $string .= $this->anagrafica->getUser()->getNome().' '.$this->anagrafica->getUser()->getCognome();
            }
        }
        $string .= "\n";
        $string .= $this->getIndirizzo();
        $string .= "\n";
        $string .= $this->getCap().' '.$this->getComuneString().' ('.$this->getProvinciaString(
            ).') - '.$this->getNazione();
        $string .= "\n";

        return (string)$string;
    }


}