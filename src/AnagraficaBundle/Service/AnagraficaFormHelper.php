<?php
// 19/04/17, 10.54
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AnagraficaBundle\Service;

use AnagraficaBundle\Entity\Anagrafica;
use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use GeoBundle\Service\ComuneHelper;
use GeoBundle\Service\ProvinciaHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Webtek\EcommerceBundle\Entity\Carrello;
use GeoBundle\Entity\Nazioni;
use GeoBundle\Service\NazioniHelper;

class AnagraficaFormHelper
{

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var NazioniHelper
     */
    private $nazioniHelper;
    /**
     * @var Translator
     */
    private $translator;
    /**
     * @var ProvinciaHelper
     */
    private $provinciaHelper;
    /**
     * @var ComuneHelper
     */
    private $comuneHelper;

    private $errors = [];

    /**
     * AnagraficaFormHelper constructor.
     */
    public function __construct(
        EntityManager $em,
        FormFactory $formFactory,
        NazioniHelper $nazioniHelper,
        Translator $translator,
        ProvinciaHelper $provinciaHelper,
        ComuneHelper $comuneHelper
    ) {

        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->nazioniHelper = $nazioniHelper;
        $this->translator = $translator;
        $this->provinciaHelper = $provinciaHelper;
        $this->comuneHelper = $comuneHelper;
    }

    public function createForm($defaultData = [], $locale = 'it', $name = 'form')
    {

        $form = $this->formFactory->createNamedBuilder(
            $name,
            FormType::class,
            $defaultData,
            [
                'validation_groups' => ['Default', $defaultData['validationGroup']],
                'constraints' => [
                    new Callback([$this, 'checkEmail']),
                    new Callback([$this, 'checkPartitaIva']),
                    new Callback([$this, 'checkRagioneSociale']),
                    new Callback([$this, 'checkCodiceFiscale']),
                    new Callback([$this, 'checkProvincia']),
                    new Callback([$this, 'checkComune']),
                ],
                'mapped' => false,
            ]
        )->add(
            'isGuest',
            ChoiceType::class,
            [
                'label' => 'anagrafica.labels.is_guest',
                'choices' => [
                    'default.labels.si' => true,
                    'default.labels.no' => false,
                ],
                'placeholder' => false,
                'required' => false,
            ]
        )->add(
            'nome',
            TextType::class,
            [
                'label' => 'anagrafica.labels.nome',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3]),
                ],
            ]
        )->add(
                'cognome',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'anagrafica.labels.cognome',
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 2]),
                    ],
                ]
            )->add(
                'indirizzo',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 3]),
                    ],
                ]
            )->add(
                'cap',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'anagrafica.labels.cap',
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 2]),
                    ],
                ]
            )->add(
                'telefono',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'anagrafica.labels.telefono',
                ]
            )->add(
                'cellulare',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'anagrafica.labels.cellulare',
                ]
            )->add(
                'nazione',
                EntityType::class,
                [
                    'required' => true,
                    'choices' => $this->nazioniHelper->getNazioniChoices(),
                    'class' => 'GeoBundle\Entity\Nazioni',
                    'preferred_choices' => [$this->em->getRepository("GeoBundle:Nazioni")->find("IT")],
                    'constraints' => [
                        new NotBlank(),
                    ],
                ]
            )->add(
                'provincia',
                ChoiceType::class,
                [
                    'label' => 'anagrafica.labels.provincia',
                    'choices' => $this->getProvinceChoices($defaultData['nazione'], $locale),
                    'placeholder' => false,
                ]
            )->add(
                'comune',
                ChoiceType::class,
                [
                    'label' => 'anagrafica.labels.comune',
                    'choices' => $this->getComuniChoices($defaultData['provincia'], $locale),
                    'placeholder' => false,
                ]
            )->add(
                'provincia_text',
                TextType::class,
                [
                    'label' => 'anagrafica.labels.provincia',
                    'required' => true,
                ]
            )->add(
                'comune_text',
                TextType::class,
                [
                    'label' => 'anagrafica.labels.comune',
                    'required' => true,
                ]
            )->add('ragioneSociale', TextType::class, ['required' => false])->add(
                'codiceFiscale',
                TextType::class,
                [
                    'required' => true,
                ]
            )->add(
                'isAzienda',
                ChoiceType::class,
                [
                    'required' => false,
                    'label' => 'anagrafica.labels.tipologia',
                    'placeholder' => false,
                    'choices' => [
                        'default.labels.privato' => false,
                        'default.labels.azienda' => true,
                    ],
                    "expanded" => true,
                ]
            )->add(
                'email',
                RepeatedType::class,
                [
                    'required' => true,
                    'type' => EmailType::class,
                    'first_options' => ['label' => 'operatori.labels.email'],
                    'second_options' => ['label' => 'operatori.labels.repeat_email'],
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'anagrafica.labels.email_non_vuota',
                            ]
                        ),
                        new Email(
                            [
                                'message' => 'anagrafica.labels.email_non_valida',
                            ]
                        ),
                    ],
                ]
            )->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'required' => $defaultData['validationGroup'] == 'create',
                    'type' => PasswordType::class,
                    'first_options' => ['label' => 'operatori.labels.password'],
                    'second_options' => ['label' => 'operatori.labels.repeat_password'],
                    'constraints' => [
                        new NotBlank(
                            [
                                'groups' => 'create',
                            ]
                        ),
                    ],
                ]
            )->add(
                'partitaIva',
                TextType::class,
                [
                    'required' => false,
                ]
            );
        $form->get('provincia')->resetViewTransformers();
        $form->get('comune')->resetViewTransformers();

        return $form->getForm();


    }

    public function checkCodiceFiscale($data, ExecutionContextInterface $context)
    {

        if (($data['isAzienda'] && (!$this->isValidPartitaIva($data['partitaIva']) && !$this->isValidCF(
                        $data['codiceFiscale']
                    ))) || (!$data['isAzienda'] && !$this->isValidCF($data['codiceFiscale']))) {

            $context->buildViolation($this->translator->trans('default.errors.codice_fiscale_non_valido'))->atPath(
                '[codiceFiscale]'
            )->addViolation();

        }

    }

    public function checkEmail($data, ExecutionContextInterface $context)
    {

    }

    public function checkPartitaIva($data, ExecutionContextInterface $context)
    {

        if ($data['isAzienda'] && (!$data['partitaIva'] || !$this->isValidPartitaIva($data['partitaIva']))) {

            $context->buildViolation($this->translator->trans('default.errors.partita_iva_non_valida'))->atPath(
                '[partitaIva]'
            )->addViolation();

        }

    }

    private function isValidPartitaIva($partitaIva)
    {

        if (strlen($partitaIva) != 11) {
            return false;
        }
        $s = 0;
        for ($i = 0; $i <= 9; $i += 2) {
            $s += ord($partitaIva[$i]) - ord('0');
        }
        for ($i = 1; $i <= 9; $i += 2) {
            $c = 2 * (ord($partitaIva[$i]) - ord('0'));
            if ($c > 9) {
                $c = $c - 9;
            }
            $s += $c;
        }
        if ((10 - $s % 10) % 10 != ord($partitaIva[10]) - ord('0')) {
            return false;
        }

        return true;

    }

    public function checkRagioneSociale($data, ExecutionContextInterface $context)
    {

        if ($data['isAzienda'] && !$data['ragioneSociale']) {

            $context->buildViolation($this->translator->trans('anagrafica.errors.ragione_sociale_non_valida'))->atPath(
                '[ragioneSociale]'
            )->addViolation();

        }

    }

    public function checkProvincia($data, ExecutionContextInterface $context)
    {

        if (!$data['provincia_text'] && !$data['provincia']) {

            $Violation1 = $context->buildViolation($this->translator->trans('anagrafica.errors.provincia_non_valida'));
            $Violation2 = clone $Violation1;
            $Violation1->atPath('[provincia]')->addViolation();
            $Violation2->atPath('[provincia_text]')->addViolation();

        }

    }

    public function checkComune($data, ExecutionContextInterface $context)
    {

        if (!$data['comune_text'] && !$data['comune']) {


            $Violation1 = $context->buildViolation($this->translator->trans('anagrafica.errors.comune_non_valido'));
            $Violation2 = clone $Violation1;
            $Violation1->atPath('[comune_text]')->addViolation();
            $Violation2->atPath('[comune]')->addViolation();


        }

    }

    public function isValidCF($codiceFiscale)
    {

        $valid = true;
        if (!is_string($codiceFiscale) || $codiceFiscale == '') {
            $valid = false;
        }
        if (strlen($codiceFiscale) != 16) {
            $valid = false;
        }
        $cf = strtoupper($codiceFiscale);
        if (!preg_match("/^[A-Z0-9]+$/", $cf)) {
            $valid = false;
        }
        if ($valid) {
            $s = 0;
            for ($i = 1; $i <= 13; $i += 2) {
                $c = $cf[$i];
                if ('0' <= $c && $c <= '9') {
                    $s += ord($c) - ord('0');
                } else {
                    $s += ord($c) - ord('A');
                }
            }
            for ($i = 0; $i <= 14; $i += 2) {
                $c = $cf[$i];
                switch ($c) {
                    case '0':
                        $s += 1;
                        break;
                    case '1':
                        $s += 0;
                        break;
                    case '2':
                        $s += 5;
                        break;
                    case '3':
                        $s += 7;
                        break;
                    case '4':
                        $s += 9;
                        break;
                    case '5':
                        $s += 13;
                        break;
                    case '6':
                        $s += 15;
                        break;
                    case '7':
                        $s += 17;
                        break;
                    case '8':
                        $s += 19;
                        break;
                    case '9':
                        $s += 21;
                        break;
                    case 'A':
                        $s += 1;
                        break;
                    case 'B':
                        $s += 0;
                        break;
                    case 'C':
                        $s += 5;
                        break;
                    case 'D':
                        $s += 7;
                        break;
                    case 'E':
                        $s += 9;
                        break;
                    case 'F':
                        $s += 13;
                        break;
                    case 'G':
                        $s += 15;
                        break;
                    case 'H':
                        $s += 17;
                        break;
                    case 'I':
                        $s += 19;
                        break;
                    case 'J':
                        $s += 21;
                        break;
                    case 'K':
                        $s += 2;
                        break;
                    case 'L':
                        $s += 4;
                        break;
                    case 'M':
                        $s += 18;
                        break;
                    case 'N':
                        $s += 20;
                        break;
                    case 'O':
                        $s += 11;
                        break;
                    case 'P':
                        $s += 3;
                        break;
                    case 'Q':
                        $s += 6;
                        break;
                    case 'R':
                        $s += 8;
                        break;
                    case 'S':
                        $s += 12;
                        break;
                    case 'T':
                        $s += 14;
                        break;
                    case 'U':
                        $s += 16;
                        break;
                    case 'V':
                        $s += 10;
                        break;
                    case 'W':
                        $s += 22;
                        break;
                    case 'X':
                        $s += 25;
                        break;
                    case 'Y':
                        $s += 24;
                        break;
                    case 'Z':
                        $s += 23;
                        break;
                }
            }
            if (chr($s % 26 + ord('A')) != $cf[15]) {
                $valid = false;
            }
        }

        return $valid;

    }

    private function getProvinceChoices(Nazioni $Nazione = null, $locale)
    {

        if ($Nazione) {

            $options = $this->provinciaHelper->getOptions($Nazione->getCountryCode(), $locale, 'symfony');
            if ($options) {
                return $options;
            }
        }

        return ['default.labels.select_one' => 0];

    }

    private function getComuniChoices($Provincia, $locale)
    {

        $options = $this->comuneHelper->getOptions($Provincia, $locale, 'symfony');
        if ($options) {
            return $options;
        }

        return ['anagrafica.labels.select_province' => 0];

    }


    public function handleData($form, Anagrafica $Anagrafica)
    {

        $data = $form->getData();
        $User = $Anagrafica->getUser();
        $userId = 0;
        if ($User) {
            $userId = $User->getId();
        }
        if (!$this->em->getRepository('AppBundle:User')->getByEmailCount(
            $data['email'],
            'ROLE_ANAGRAFICA',
            $userId
        )) {

            if (isset($data['isGuest'])) {
                $Anagrafica->setIsGuest($data['isGuest']);
            } else {
                $Anagrafica->setIsGuest(0);
            }
            $Anagrafica->setNazione($data['nazione']);
            if ($data['provincia']) {
                $Provincia = $this->em->getRepository('GeoBundle:Provincia')->findOneBy(['id' => $data['provincia']]);
                $Anagrafica->setProvincia($Provincia);
            } elseif ($data['provincia_text']) {
                $Anagrafica->setProvinciaText($data['provincia_text']);
            }
            if ($data['comune']) {
                $Comune = $this->em->getRepository('GeoBundle:Comune')->findOneBy(['id' => $data['comune']]);
                $Anagrafica->setComune($Comune);
            } elseif ($data['comune_text']) {
                $Anagrafica->setComuneText($data['comune_text']);
            }
            if (isset($data['isAzienda'])) {
                $Anagrafica->setIsAzienda($data['isAzienda']);
                $Anagrafica->setRagioneSociale($data['ragioneSociale']);
                $Anagrafica->setPartitaIva($data['partitaIva']);
            }
            $Anagrafica->setCodiceFiscale($data['codiceFiscale']);
            $Anagrafica->setIndirizzo($data['indirizzo']);
            $Anagrafica->setCap($data['cap']);
            $Anagrafica->setTelefono($data['telefono']);
            $Anagrafica->setCellulare($data['cellulare']);
            if (!$User) {
                $User = new User();
            }
            if ($Anagrafica->getisGuest()) {
                $User->setPlainPassword(uniqid());
            }
            $User->setNome($data['nome']);
            $User->setCognome($data['cognome']);
            $User->setEmail($data['email']);
            $User->setIsEnabled(1);
            if (!$Anagrafica->getId() || $data['plainPassword']) {
                $User->setPlainPassword($data['plainPassword']);
            }
            $User->setProfileText('');
            $User->setRole('ROLE_ANAGRAFICA');
            $User->setUsername($data['email']);
            $Anagrafica->setUser($User);
            $IndirizzoAnagrafica = false;
            if (!$Anagrafica->getId()) {

                $presso = $User->getNome().' '.$User->getCognome();
                if ($Anagrafica->getIsAzienda()) {
                    $presso .= ' ('.$Anagrafica->getRagioneSociale().')';
                }
                $IndirizzoAnagrafica = new IndirizzoAnagrafica();
                $IndirizzoAnagrafica->setPresso($presso);
                $IndirizzoAnagrafica->setCap($Anagrafica->getCap());
                if ($Anagrafica->getComune()) {
                    $IndirizzoAnagrafica->setComune($Anagrafica->getComune());
                } else {
                    $IndirizzoAnagrafica->setComuneText($Anagrafica->getComuneText());
                }
                if ($Anagrafica->getProvincia()) {
                    $IndirizzoAnagrafica->setProvincia($Anagrafica->getProvincia());
                } else {
                    $IndirizzoAnagrafica->setProvinciaText($Anagrafica->getProvinciaText());
                }
                $IndirizzoAnagrafica->setIndirizzo($Anagrafica->getIndirizzo());
                $IndirizzoAnagrafica->setNazione($Anagrafica->getNazione());
                $IndirizzoAnagrafica->setAnagrafica($Anagrafica);
                $IndirizzoAnagrafica->setSort(0);
                $IndirizzoAnagrafica->setIsDefault(1);
                $indirizzi = new ArrayCollection();
                $indirizzi->add($IndirizzoAnagrafica);
                $Anagrafica->setIndirizzi($indirizzi);
//                $Anagrafica->setOldId(0);
                $Anagrafica->setIsNewsletterSubscribed(0);
            }
            try {
                $this->em->persist($User);
                $this->em->persist($Anagrafica);
                if ($IndirizzoAnagrafica) {
                    $this->em->persist($IndirizzoAnagrafica);
                }
                $this->em->flush();

                return $Anagrafica;

            } catch (\Exception $e) {
                die($e->getMessage());
            }

        } else {

            $this->errors[] = 'Email: Indirizzo email già utilizzato';

            return false;

        }

    }

    public function retrieveDefaultData(Anagrafica $Anagrafica)
    {

        $User = $Anagrafica->getUser();
        $data = [];
        // DEBUG
        if ($User) {
            $data['nome'] = $User->getNome();
            $data['cognome'] = $User->getCognome();
            $data['email'] = $User->getEmail();
        }
        $data['isGuest'] = $Anagrafica->getisGuest();
        $data['cap'] = $Anagrafica->getCap();
        $data['indirizzo'] = $Anagrafica->getIndirizzo();
        $data['isAzienda'] = $Anagrafica->getIsAzienda();
        $data['telefono'] = $Anagrafica->getTelefono();
        $data['cellulare'] = $Anagrafica->getCellulare();
        $data['provincia'] = false;
        if ($Anagrafica->getProvincia()) {
            $data['provincia'] = $Anagrafica->getProvincia()->getId();
        }
        $data['provincia_text'] = $Anagrafica->getProvinciaText();
        $data['comune'] = false;
        if ($Anagrafica->getComune()) {
            $data['comune'] = $Anagrafica->getComune()->getId();
        }
        $data['comune_text'] = $Anagrafica->getComuneText();
        $data['nazione'] = $Anagrafica->getNazione();
        if (!$data['nazione']) {
            $data['nazione'] = $this->em->getRepository("GeoBundle:Nazioni")->find("IT");
        }
        $data['codiceFiscale'] = $Anagrafica->getCodiceFiscale();
        $data['ragioneSociale'] = $Anagrafica->getRagioneSociale();
        $data['partitaIva'] = $Anagrafica->getPartitaIva();

        return $data;
    }

    public function getErrors()
    {

        return $this->errors;
    }


}