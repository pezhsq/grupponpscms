<?php
// 20/04/17, 15.02
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AnagraficaBundle\Service;


use AnagraficaBundle\Entity\Anagrafica;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class AnagraficaHelper
{

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * AnagraficaHelper constructor.
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function getList($deleted = false)
    {

        if ($deleted) {
            $Anagrafiche = $this->entityManager->getRepository('AnagraficaBundle:Anagrafica')->findAll();
        } else {
            $Anagrafiche = $this->entityManager->getRepository('AnagraficaBundle:Anagrafica')->findAllNotDeleted();
        }

        $records = [];

        foreach ($Anagrafiche as $Anagrafica) {


            /**
             * @var $Anagrafica Anagrafica;
             */

            /**
             * @var $User User;
             */
            $User = $Anagrafica->getUser();

            $record               = [];
            $record['id']         = $Anagrafica->getId();
            $record['nominativo'] = $User->getNome().' '.$User->getCognome();
            if ($Anagrafica->getIsAzienda()) {
                $record['nominativo'] .= ' ('.$Anagrafica->getRagioneSociale().')';
            }
            $record['codiceFiscale'] = $Anagrafica->getCodiceFiscale();
            $record['partitaIva']    = $Anagrafica->getPartitaIva() ? $Anagrafica->getPartitaIva() : 'N.A.';
            $record['email']         = $User->getEmail();
            $record['registeredAt']  = $Anagrafica->getRegisteredAt()->format('d/m/Y H:i:s');
            $record['updatedAt']     = $User->getUpdatedAt()->format('d/m/Y H:i:s');
            $record['deleted']       = $Anagrafica->isDeleted();
            
            $records[] = $record;
        }

        return $records;

    }
}