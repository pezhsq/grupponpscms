<?php
// 20/04/17, 17.47
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AnagraficaBundle\Service;


use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use AnagraficaBundle\Form\IndirizzoAnagraficaForm;
use Doctrine\ORM\EntityManager;
use GeoBundle\Service\ComuneHelper;
use GeoBundle\Service\ProvinciaHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use GeoBundle\Entity\Nazioni;
use GeoBundle\Service\NazioniHelper;

class IndirizzoAnagraficaFormHelper
{

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var NazioniHelper
     */
    private $nazioniHelper;
    /**
     * @var Translator
     */
    private $translator;
    /**
     * @var ProvinciaHelper
     */
    private $provinciaHelper;
    /**
     * @var ComuneHelper
     */
    private $comuneHelper;

    public function __construct(
        EntityManager $em,
        FormFactory $formFactory,
        NazioniHelper $nazioniHelper,
        Translator $translator,
        ProvinciaHelper $provinciaHelper,
        ComuneHelper $comuneHelper
    )
    {

        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->nazioniHelper = $nazioniHelper;
        $this->translator = $translator;
        $this->provinciaHelper = $provinciaHelper;
        $this->comuneHelper = $comuneHelper;
    }

    public function createForm($defaultData = [], $locale = 'it')
    {

        $form = $this->formFactory->createBuilder(
            IndirizzoAnagraficaForm::class,
            $defaultData,
            [
                'constraints' => [
                    new Callback([$this, 'checkProvincia']),
                    new Callback([$this, 'checkComune']),
                ],
                'mapped' => false,
            ]
        )->add(
            'anagrafica',
            HiddenType::class,
            ['data' => $defaultData['anagrafica']]
        )->add(
            'presso',
            TextType::class,
            [
                'label' => 'anagrafica.labels.presso',
                'required' => false,
            ]
        )->add(
            'indirizzo',
            TextType::class,
            [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3]),
                ],
            ]
        )->add(
            'cap',
            TextType::class,
            [
                'required' => true,
                'label' => 'anagrafica.labels.cap',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 2]),
                ],
            ]
        )->add(
            'nazione',
            EntityType::class,
            [
                'required' => true,
                'choices' => $this->nazioniHelper->getNazioniChoices(),
                'class' => 'GeoBundle\Entity\Nazioni',
                'preferred_choices' => [$this->em->getRepository("GeoBundle:Nazioni")->find("IT")],
                'constraints' => [
                    new NotBlank(),
                ],

            ]
        )->add(
            'provincia',
            ChoiceType::class,
            [
                'label' => 'anagrafica.labels.provincia',
                'choices' => $this->getProvinceChoices($defaultData['nazione'], $locale),
                'placeholder' => false,
            ]
        )->add(
            'comune',
            ChoiceType::class,
            [
                'label' => 'anagrafica.labels.comune',
                'choices' => $this->getComuniChoices($defaultData['provincia'], $locale),
                'placeholder' => false,
            ]
        )->add(
            'provincia_text',
            TextType::class,
            [
                'label' => 'anagrafica.labels.provincia',
                'required' => true,
            ]
        )->add(
            'comune_text',
            TextType::class,
            [
                'label' => 'anagrafica.labels.comune',
                'required' => true,
            ]
        );

        $form->get('provincia')->resetViewTransformers();
        $form->get('comune')->resetViewTransformers();

        return $form->getForm();

    }

    private function getProvinceChoices(Nazioni $Nazione = null, $locale)
    {

        if ($Nazione) {

            $options = $this->provinciaHelper->getOptions($Nazione->getCountryCode(), $locale, 'symfony');

            if ($options) {
                return $options;
            }
        }

        return ['default.labels.select_one' => 0];

    }

    private function getComuniChoices($Provincia, $locale)
    {

        $options = $this->comuneHelper->getOptions($Provincia, $locale, 'symfony');

        if ($options) {
            return $options;
        }

        return ['anagrafica.labels.select_province' => 0];


    }

    public function handleData($data, IndirizzoAnagrafica $indirizzoAnagrafica)
    {

        $indirizzoAnagrafica->setNazione($data['nazione']);

        if ($data['provincia']) {
            $Provincia = $this->em->getRepository('GeoBundle:Provincia')->findOneBy(['id' => $data['provincia']]);
            $indirizzoAnagrafica->setProvincia($Provincia);
        } elseif ($data['provincia_text']) {
            $indirizzoAnagrafica->setProvinciaText($data['provincia_text']);
        }

        if ($data['comune']) {
            $Comune = $this->em->getRepository('GeoBundle:Comune')->findOneBy(['id' => $data['comune']]);
            $indirizzoAnagrafica->setComune($Comune);
        } elseif ($data['comune_text']) {
            $indirizzoAnagrafica->setComuneText($data['comune_text']);
        }
        $indirizzoAnagrafica->setIndirizzo($data['indirizzo']);
        $indirizzoAnagrafica->setCap($data['cap']);
        $indirizzoAnagrafica->setPresso($data['presso']);
        $indirizzoAnagrafica->setAnagrafica(
            $this->em->getRepository('AnagraficaBundle:Anagrafica')->findOneBy(['id' => $data['anagrafica']])
        );
        $indirizzoAnagrafica->setSort(0);
        if ($indirizzoAnagrafica->getIsDefault() === null) {
            $indirizzoAnagrafica->setIsDefault(0);
        }

        try {

            $this->em->persist($indirizzoAnagrafica);
            $this->em->flush();

            return true;

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    public function checkProvincia($data, ExecutionContextInterface $context)
    {

        if (!$data['provincia_text'] && !$data['provincia']) {

            $Violation1 = $context->buildViolation($this->translator->trans('anagrafica.errors.provincia_non_valida'));
            $Violation2 = clone $Violation1;

            $Violation1->atPath('[provincia]')->addViolation();
            $Violation2->atPath('[provincia_text]')->addViolation();

        }

    }

    public function checkComune($data, ExecutionContextInterface $context)
    {

        if (!$data['comune_text'] && !$data['comune']) {


            $Violation1 = $context->buildViolation($this->translator->trans('anagrafica.errors.comune_non_valido'));
            $Violation2 = clone $Violation1;

            $Violation1->atPath('[comune_text]')->addViolation();
            $Violation2->atPath('[comune]')->addViolation();


        }

    }

}