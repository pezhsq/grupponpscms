<?php
// 13/04/17, 14.58
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
namespace AnagraficaBundle\Form;

use AnagraficaBundle\Entity\Anagrafica;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnagraficaForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('nome')->add('cognome')->add('email')->add('ragioneSociale')->add('codiceFiscale')->add(
                'partitaIva'
            )->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'required' => false,
                    'first_options' => ['label' => 'operatori.labels.password', 'help' => 'operatori.help.password'],
                    'second_options' => ['label' => 'operatori.labels.repeat_password'],
                ]
            );


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'data_class' => Anagrafica::class,
                'error_bubbling' => true,
                'langs' => ['it' => 'Italiano'],
            ]
        );
    }


}