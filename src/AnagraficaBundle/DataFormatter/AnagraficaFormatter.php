<?php
// 30/05/17, 12.09
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AnagraficaBundle\DataFormatter;


use AppBundle\DataFormatter\DataFormatter;

class AnagraficaFormatter extends DataFormatter
{

    private $customer = false;

    public function getData()
    {

        // TODO: Implement getData() method.
    }

    public function extractData()
    {

        /**
         * @var $user User
         */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $authorizationChecker = $this->container->get('security.authorization_checker');

        if ($user != 'anon.' && $authorizationChecker->isGranted('ROLE_ANAGRAFICA')) {
            $this->customer = true;
        } else {
            $this->customer = false;
        }

    }

    public function getStato()
    {




    }

    private function getFormLogin()
    {

    }

    private function getFormRegistrazione()
    {

        return $this->container->get('app.anagrafica.form.anagrafica_form_helper')->createForm([], $this->locale);

    }

    private function getFormOspite()
    {

    }

}