<?php
// 13/04/17, 11.16
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AnagraficaBundle\Controller\Admin;


use AnagraficaBundle\Entity\Anagrafica;
use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use AppBundle\Entity\User;
use MatthiasMullie\Minify\JS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormConfigInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_MANAGE_ANAGRAFICA')")
 */
class AnagraficaController extends Controller
{

    /**
     * @Route("/anagrafica", name="anagrafica")
     *
     */
    public function listAction()
    {

        return $this->render('AnagraficaBundle:Anagrafica:list.html.twig');

    }

    /**
     * @Route("/anagrafica/json", name="anagrafica_list_json")
     * @Security("is_granted('ROLE_MANAGE_ANAGRAFICA')")
     */
    public function listJson(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $return = [];
        $return['result'] = true;
        $return['data'] = $this->get('app.anagrafica.anagrafica_helper')->getList(
            $this->isGranted('ROLE_RESTORE_DELETED')
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/anagrafica/delete/{id}", name="anagrafica_delete",  requirements={"id" = "\d+"})
     * @Security("is_granted('ROLE_MANAGE_ANAGRAFICA')")
     */
    public function deleteAction(Request $request, Anagrafica $Anagrafica)
    {

        /**
         * @var $User User
         */
        $User = $Anagrafica->getUser();

        $elemento = $User->getNome() . ' ' . $User->getCognome();

        if ($Anagrafica->getIsAzienda()) {
            $elemento .= ' (' . $Anagrafica->getRagioneSociale() . ')';
        }

        $em = $this->getDoctrine()->getManager();

        if (!$Anagrafica->isDeleted()) {
            $User->setIsEnabled(false);
            $em->remove($Anagrafica);
            $em->flush();
        }

        $this->addFlash('success', 'Anagrafica ' . $elemento . ' eliminata');

        return $this->redirectToRoute('anagrafica');

    }

    /**
     * @Route("/anagrafica/restore/{id}", name="anagrafica_restore")
     * @Security("is_granted('ROLE_MANAGE_ANAGRAFICA')")
     */
    public function restoreAction(Request $request, Anagrafica $Anagrafica)
    {

        $User = $Anagrafica->getUser();

        if ($Anagrafica->isDeleted() && $this->isGranted('ROLE_RESTORE_DELETED')) {

            $em = $this->getDoctrine()->getManager();

            $elemento = $User->getNome() . ' ' . $User->getCognome();

            if ($Anagrafica->getIsAzienda()) {
                $elemento .= ' (' . $Anagrafica->getRagioneSociale() . ')';
            }

            $User->setIsEnabled(true);
            $Anagrafica->restore();

            $em->flush();

            $this->addFlash('success', 'Anagrafica ' . $elemento . ' ripristinata');

        }

        return $this->redirectToRoute('anagrafica');

    }

    /**
     * @Route("/anagrafica/addresses/{id}" , name="anagrafica_addresses",  requirements={"id": "\d+"})
     */
    public function getAddresses($id)
    {

        if ($id) {

            $em = $this->getDoctrine()->getManager();

            $Anagrafica = $em->getRepository('AnagraficaBundle:Anagrafica')->findOneBy(
                ['id' => $id]
            );

            if ($Anagrafica) {

                foreach ($Anagrafica->getIndirizzi() as $Indirizzo) {
                    if (!(string)$Indirizzo->getNazione()) {
                        $nazioniSymfony = Intl::getRegionBundle()->getCountryNames();
                        $Indirizzo->getNazione()->setNome($nazioniSymfony[$Indirizzo->getNazione()->getCountryCode()]);
                    }
                }

                $return = [];
                $return['result'] = true;
                $return['markup'] = $this->renderView(
                    'AnagraficaBundle:Anagrafica:indirizzi.html.twig',
                    ['Anagrafica' => $Anagrafica]
                );

                return new JsonResponse($return);

            }

        }

        $return = [];
        $return['result'] = false;
        $return['errors'] = ['Richiesta non valida'];


        return new JsonResponse($return);


    }

    /**
     * @Route("/anagrafica/address/get-form", name="anagrafica_address_form")
     */
    public function getForm(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $formIndirizziHelper = $this->get('app.anagrafica.form.indirizzo_form_helper');

        $IndirizzoAnagrafica = null;

        if ($request->request->get('id')) {

            $IndirizzoAnagrafica = $em->getRepository('AnagraficaBundle:IndirizzoAnagrafica')->findOneBy(
                ['id' => $request->request->get('id')]
            );

        }

        if (!$IndirizzoAnagrafica) {

            $IndirizzoAnagrafica = new IndirizzoAnagrafica();

        }

        $addressId = 0;

        if ($IndirizzoAnagrafica->getId()) {

            $addressId = $IndirizzoAnagrafica->getId();

            $defaultData = [
                'anagrafica' => $IndirizzoAnagrafica->getAnagrafica()->getId(),
                'nazione' => $IndirizzoAnagrafica->getNazione(),
                'provincia' => $IndirizzoAnagrafica->getProvincia(),
                'comune' => $IndirizzoAnagrafica->getComune(),
                'provincia_text' => $IndirizzoAnagrafica->getProvinciaText(),
                'comune_text' => $IndirizzoAnagrafica->getComuneText(),
                'cap' => $IndirizzoAnagrafica->getCap(),
                'presso' => $IndirizzoAnagrafica->getPresso(),
                'indirizzo' => $IndirizzoAnagrafica->getIndirizzo(),
            ];

            if ($defaultData['provincia']) {
                $defaultData['provincia'] = $defaultData['provincia']->getId();
            }
            if ($defaultData['comune']) {
                $defaultData['comune'] = $defaultData['comune']->getId();
            }

            $action = $this->generateUrl('anagrafica_address_edit', ['id' => $IndirizzoAnagrafica->getId()]);

        } else {

            $Anagrafica = $em->getRepository('AnagraficaBundle:Anagrafica')->findOneBy(
                ['id' => $request->request->get('anagrafica_id')]
            );

            $defaultData = [
                'anagrafica' => $Anagrafica->getId(),
                'nazione' => $Anagrafica->getNazione(),
                'provincia' => 0,
                'comune' => 0,
            ];


            $action = $this->generateUrl('anagrafica_address_new');
        }

        $formIndirizzo = $formIndirizziHelper->createForm($defaultData, $request->getLocale());


        $return = [];
        $return['result'] = true;
        $return['markup'] = $this->renderView(
            '@Anagrafica/Anagrafica/formAddress.html.twig',
            ['formIndirizzo' => $formIndirizzo->createView(), 'action' => $action, 'addressId' => $addressId]
        );

        return new JsonResponse($return);

    }

    /**
     * @Route("/anagrafica/address/delete/{id}", name="anagrafica_address_delete", requirements={"id": "\d+"})
     */
    public function deleteAddress($id)
    {

        $em = $this->getDoctrine()->getManager();

        $IndirizzoAnagrafica = $em->getRepository('AnagraficaBundle:IndirizzoAnagrafica')->findOneBy(
            ['id' => $id]
        );

        if ($IndirizzoAnagrafica) {

            $em->remove($IndirizzoAnagrafica);
            $em->flush();

        }

        $return = [];
        $return['result'] = true;

        return new JsonResponse($return);


    }

    /**
     * @Route("/anagrafica/address/set-default/{id}", name="anagrafica_address_default", requirements={"id": "\d+"})
     */
    public function setDefaultAddress($id)
    {

        $em = $this->getDoctrine()->getManager();

        $IndirizzoAnagrafica = $em->getRepository('AnagraficaBundle:IndirizzoAnagrafica')->findOneBy(
            ['id' => $id]
        );

        if ($IndirizzoAnagrafica) {

            $IndirizzoAnagrafica->setIsDefault(1);

            $em->persist($IndirizzoAnagrafica);
            $em->flush();

        }

        $return = [];
        $return['result'] = true;

        return new JsonResponse($return);

    }

    /**
     * @Route("/anagrafica/address/", name="anagrafica_address_new")
     * @Route("/anagrafica/addess/edit/{id}", name="anagrafica_address_edit", requirements={"id": "\d+"})
     */
    public function newEditAddressAction(
        Request $request
    )
    {

        $em = $this->getDoctrine()->getManager();

        $anagraficaId = $request->get('indirizzo_anagrafica_form')['anagrafica'];

        if ($anagraficaId) {

            $Anagrafica = $em->getRepository('AnagraficaBundle:Anagrafica')->findOneBy(
                ['id' => $anagraficaId]
            );

            if ($Anagrafica) {

                $formIndirizziHelper = $this->get('app.anagrafica.form.indirizzo_form_helper');

                $IndirizzoAnagrafica = new IndirizzoAnagrafica();

                if ($request->get('id')) {

                    $IndirizzoAnagrafica = $em->getRepository('AnagraficaBundle:IndirizzoAnagrafica')->findOneBy(
                        ['id' => $request->get('id')]
                    );

                    if (!$IndirizzoAnagrafica) {

                        $return = [];
                        $return['result'] = false;
                        $return['error'] = 'L\'indirizzo in modifica non esiste più, è stato cancellato?';

                        return new JsonResponse($return);

                    }

                }

                $defaultData = [
                    'anagrafica' => $Anagrafica->getId(),
                    'nazione' => $Anagrafica->getNazione(),
                    'provincia' => 0,
                    'comune' => 0,
                ];

                $formIndirizzo = $formIndirizziHelper->createForm($defaultData, $request->getLocale());

                $formIndirizzo->handleRequest($request);

                $errors = [];

                if ($formIndirizzo->isSubmitted()) {
                    if ($formIndirizzo->isValid()) {
                        $formIndirizziHelper->handleData($formIndirizzo->getData(), $IndirizzoAnagrafica);

                        $return = [];
                        $return['result'] = true;

                        return new JsonResponse($return);

                    } else {

                        $errors = [];
                        $trans = $this->get('translator');
                        foreach ($formIndirizzo->getErrors(true) as $error) {
                            /**
                             * @var $error FormError
                             */

                            /**
                             * @var $Config FormConfigInterface
                             */
                            $Config = $error->getOrigin()->getConfig();

                            $lbl = $Config->getOptions()['label'];
                            if ($lbl) {
                                $lbl = $trans->trans($lbl);
                            } else {
                                $lbl = ucfirst($Config->getName());
                            }
                            $errors[] = $lbl . ': ' . $error->getMessage();
                        }

                        $errors = array_unique($errors);

                        $return = [];
                        $return['result'] = false;
                        $return['errors'] = $errors;

                        return new JsonResponse($return);

                    }

                } else {

                    $return = [];
                    $return['result'] = false;
                    $return['error'] = 'Richiesta non valida (' . __LINE__ . ')';

                    return new JsonResponse($return);

                }

            } else {

                $return = [];
                $return['result'] = false;
                $return['error'] = 'Richiesta non valida (' . __LINE__ . ')';

                return new JsonResponse($return);

            }

        } else {

            $return = [];
            $return['result'] = false;
            $return['error'] = 'Richiesta non valida (' . __LINE__ . ')';

            return new JsonResponse($return);

        }


    }

    /**
     * @Route("/anagrafica/new", name="anagrafica_new")
     * @Route("/anagrafica/edit/{id}",  name="anagrafica_edit", requirements={"id": "\d+"})
     */
    public function newEditAction(
        Request $request
    )
    {

        $langs = $this->get('app.languages')->getActiveLanguages();
        $translator = $this->get('translator');

        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) {

            $Anagrafica = $em->getRepository('AnagraficaBundle:Anagrafica')->findOneBy(
                ['id' => $request->get('id')]
            );

            if (!$this->get('security.authorization_checker')->isGranted('ROLE_MANAGE_ANAGRAFICA')) {
                if ($this->container->get('security.token_storage')->getToken()->getUser() != $Anagrafica->getUser()) {
                    throw $this->createAccessDeniedException();
                }
            }

            if (!$Anagrafica) {

                return $this->redirectToRoute('anagrafica_new');

            }

        } else {

            $Anagrafica = new Anagrafica();

        }


        $formHelper = $this->get('app.anagrafica.form.anagrafica_form_helper');

        $defaultData = $formHelper->retrieveDefaultData($Anagrafica);

        $defaultData['validationGroup'] = ($Anagrafica->getId()) ? 'update' : 'create';

        /**
         * @var $form FormInterface
         */
        $form = $formHelper->createForm($defaultData, $request->getLocale());

        $form->handleRequest($request);

        $errors = [];

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                if (!$formHelper->handleData($form, $Anagrafica)) {

                    $errors = $formHelper->getErrors();

                    $errors = array_unique($errors);

                } else {

                    return $this->redirectToRoute('anagrafica_edit', ['id' => $Anagrafica->getId()]);

                }

            } else {

                $errors = [];
                $trans = $this->get('translator');
                foreach ($form->getErrors(true) as $error) {
                    /**
                     * @var $error FormError
                     */

                    /**
                     * @var $Config FormConfigInterface
                     */
                    $Config = $error->getOrigin()->getConfig();

                    $lbl = $Config->getOptions()['label'];
                    if ($lbl) {
                        $lbl = $trans->trans($lbl);
                    } else {
                        $lbl = ucfirst($Config->getName());
                    }
                    $errors[] = $lbl . ': ' . $error->getMessage();
                }

                $errors = array_unique($errors);

            }
        }

        $view = 'AnagraficaBundle:Anagrafica:new.html.twig';

        if ($Anagrafica->getId()) {
            $view = 'AnagraficaBundle:Anagrafica:edit.html.twig';
        }

        $data = [
            'form' => $form->createView(),
            'errors' => $errors,
            'action' => $defaultData['validationGroup'],
            'Anagrafica' => $Anagrafica,

        ];

        return $this->render($view, $data);

    }


}