<?php
// 21/04/17, 16.58
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

namespace AnagraficaBundle\EntityListeners;

use AnagraficaBundle\Entity\IndirizzoAnagrafica;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

class IndirizzoAnagraficaListener
{

    /**
     * @ORM\PostUpdate()
     */
    public function postUpdate(IndirizzoAnagrafica $IndirizzoAnagrafica, LifecycleEventArgs $event)
    {

        if ($IndirizzoAnagrafica->getIsDefault()) {
            $em               = $event->getEntityManager();
            $this->repository = $em->getRepository('AnagraficaBundle:IndirizzoAnagrafica')->unsetDefault(
                $IndirizzoAnagrafica->getId(),
                $IndirizzoAnagrafica->getAnagrafica()
            );

        }

    }

}