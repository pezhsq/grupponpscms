$(window).on('load', function() {
  if ($('#' + instance).length) {

    $('#' + instance).find('.thumbnail.text').height($('#' + instance).find('.box-testo').height());

    $('#' + instance).find(".box-vetrina-simple .thumbnail.text").matchHeight({
      target: $('#' + instance).find(".box-vetrina-simple .wrapper-doppio")
    });

    $('#' + instance).find(".box-vetrina-simple .quadrati .col-img").matchHeight({
      target: $('#' + instance).find('.box-vetrina-simple .col-grid')
    });

    $('#' + instance).find(".box-vetrina-simple").on('mouseenter', '.thumbnail', function(event) {
      $(this).find(".overlay img").addClass('animated zoomIn');
    });

    if ($('.col-grande').height() > $('.col-grid').height()) {

      $('#' + instance).find(".box-vetrina-simple .col-grid").matchHeight({
        target: $('#' + instance).find('.col-grande')
      });

    } else {

      $('#' + instance).find(".box-vetrina-simple .col-grande").matchHeight({
        target: $('.col-grid')
      });

    }


    if (!Modernizr.objectfit) {

      $('#' + instance).find(".box-vetrina-simple").each(function(index, el) {

        var $container = $(el).find(".col-grande");
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');
        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }
      });

    }

  }
});