$(window).on('load', function() {

  if ($('#' + instance).length) {


    if ($('.col-big').height() > $('.col-a').height()) {

      $('#' + instance).find(".box-vetrina-simple .col-a").matchHeight({
        target: $('#' + instance).find('.col-big')
      });
      
    } else {

      $('#' + instance).find(".box-vetrina-simple .col-a").matchHeight({
        target: $('.col-big')
      });

    }

    $('#' + instance).find(".box-vetrina-composta .thumbnail.text").matchHeight({
      target: $(".box-vetrina-composta .wrapper-doppio")
    });

    $('#' + instance).find(".box-vetrina-composta .sameHeight").matchHeight();
    $('#' + instance).find(".box-vetrina-composta .quadrati .col-lg-6").matchHeight();

    $('#' + instance).find(".box-vetrina-composta").on('mouseenter', '.thumbnail', function(event) {
      $(this).find(".overlay img").addClass('animated zoomIn');
    });

    if (!Modernizr.objectfit) {

      $('#' + instance).find(".box-vetrina-composta").each(function(index, el) {

        var $container = $(el).find(".box-grande");
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');

        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }

      });

    }
  }
});
