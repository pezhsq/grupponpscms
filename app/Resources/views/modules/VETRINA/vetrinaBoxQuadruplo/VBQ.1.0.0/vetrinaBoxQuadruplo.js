$(function() {
  if ($('#' + instance).length) {
    $('#' + instance).find(".box-quadruplo-box-floating .col-slider .slider-opinioni").slick({
      slidesToShow: 1,
      dots: true,
      infinite: false,
      arrows: false
    });
    $('#' + instance).find(".box-quadruplo-box-floating .col-lg-6").matchHeight({
      target: $('#' + instance).find(".box-quadruplo-box-floating .col-slider")
    });

    if (!Modernizr.objectfit) {

      $('#' + instance).find(".box-quadruplo-box-floating .col-img").each(function(index, el) {

        var $container = $(el);
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');

        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }

      });

    }
  }
});