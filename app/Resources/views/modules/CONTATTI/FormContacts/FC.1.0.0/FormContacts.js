$(function() {

  function validateFormBooking($form) {

    var errori = [];

    if ($.trim($form.find('#nome_cognome').val()) == '') {
      errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Nome e Cognome')));
    }
    if ($.trim($form.find('#tipo_richiesta').val()) == '') {
      errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Oggetto')));
    }
    if ($.trim($form.find('#email').val()) == '') {
      errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Email')));
    }
    if ($.trim($form.find('#richiesta').val()) == '') {
      errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Richiesta')));
    }
    if ($form.find('#privacy:checked').length == 0) {
      errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Privacy')));
    }

    if (errori.length) {

      return errori;

    }

    return true;

  }


  var $form = $('.form_contacts').find('form');

  $form.find('.invio_button').on('click', function(e) {
    e.preventDefault();

    var validForm = validateFormBooking($form);

    if (validForm === true) {

      var btnPrevText = $(this).html();

      var $btn = $(this);

      $(this).html(_('Attendere prego'));
      $(this).attr('disabled', true);
      $(this).addClass('disabled');

      $form.ajaxSubmit({
        'success': function(ret_data) {

          $btn.html(btnPrevText);
          $btn.removeAttr('disabled');
          $btn.removeClass('disabled');

          if (ret_data.result) {
            $form.resetForm();
            var opts = {
              'type': 'success',
              'titolo': _('Richiesta informazioni'),
              'content': _('Abbiamo ricevuto il tuo messaggio, ti risponderemo al più presto'),
              'OK': 'Ok',
              'callback': null
            };
            HandleBarHelper.alert(opts);
          } else {
            var opts = {
              'type': 'danger',
              'titolo': _('Attenzione'),
              'content': ret_data.errors.join('<br />'),
              'OK': 'Ok',
              'callback': null
            };
            HandleBarHelper.alert(opts);
          }
        }
      });
    } else {
      var opts = {
        'type': 'danger',
        'titolo': _('Attenzione'),
        'content': validForm.join('<br />'),
        'OK': 'Ok',
        'callback': null
      };
      HandleBarHelper.alert(opts);
    }

  });

  $(window).on('scroll', function(event) {
    if ($('.form_contacts').length) {
      var topDistance = $(".form_contacts").offset().top - $(window).scrollTop();
      $(".form_contacts .layer").each(function(index, el) {
        var depth = $(this).data('depth');
        var movement = (topDistance * depth)
        var translate3d = 'translate3d(0, ' + movement + 'px, 0)'
        $(this).css("transform", translate3d);
        $(this).css("-moz-transform", translate3d);
        $(this).css("-o-transform", translate3d);
        $(this).css("-webkit-transform", translate3d);
      });
    }
  });

});