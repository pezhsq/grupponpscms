$(function() {

  if ($('#' + instance).length) {

    // $('#' + instance).find(".sezione.contatti .col-lg-6").matchHeight();

    function validateFormContatti($form) {

      var errori = [];

      if ($.trim($form.find('#nome_cognome').val()) == '') {
        errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Nome e Cognome')));
      }
      if ($.trim($form.find('#email').val()) == '') {
        errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Email')));
      }
      if ($.trim($form.find('#phone').val()) == '') {
        errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Telefono')));
      }
      if ($form.find('#privacy:checked').length == 0) {
        errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Privacy')));
      }
      return true;
      if (errori.length) {

        return errori;

      }

      return true;

    }

    $container = $('#' + instance);


    $container.find('.btn-send').on('click', function(e) {
      e.preventDefault();
      $container.find('form').attr({ 'action': $container.find('form').data('endpoint'), 'method': 'post' });

      var validForm = validateFormContatti($container.find('form'));

      if (validForm === true) {
        HandleBarHelper.lockScreen({ 'message': 'Attendere prego' });
        $container.find('form').ajaxSubmit({
          'success': function(ret_data) {
            HandleBarHelper.unlockScreen();
            if (ret_data.result) {
              $container.find('form').resetForm();
              var opts = {
                'type': 'success',
                'titolo': _('Richiesta informazioni'),
                'content': _('Abbiamo ricevuto il tuo messaggio, ti risponderemo al più presto'),
                'OK': 'Ok',
                'callback': null
              };
              HandleBarHelper.alert(opts);
            } else {
              var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': ret_data.errors.join('<br />'),
                'OK': 'Ok',
                'callback': null
              };
              HandleBarHelper.alert(opts);
            }
          }
        });
      } else {
        var opts = {
          'type': 'danger',
          'titolo': _('Attenzione'),
          'content': validForm.join('<br />'),
          'OK': 'Ok',
          'callback': null
        };
        HandleBarHelper.alert(opts);
      }
    });

  }


});
