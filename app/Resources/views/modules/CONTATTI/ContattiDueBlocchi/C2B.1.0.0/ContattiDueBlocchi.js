var mapConfig = [
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }]
  }, {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }]
  }, {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }]
  }, {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }]
  }, {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }]
  }, {
    "featureType": "road.local",
    "elementType": "geometry",
    "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }]
  }, {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }]
  }, {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [{ "color": "#dedede" }, { "lightness": 21 }]
  }, {
    "elementType": "labels.text.stroke",
    "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }]
  }, {
    "elementType": "labels.text.fill",
    "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }]
  }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }]
  }, {
    "featureType": "administrative",
    "elementType": "geometry.fill",
    "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }]
  }, {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }]
  }
];


$(document).ready(function() {
  if ($(".two_columns_contacts").length) {
    // $('.two_columns_contacts .mappa-wrap')
    // .click(function() {
    //   $(this).find("#mappa").addClass('clicked');
    // })
    // .mouseleave(function() {
    //   $(this).find("#mappa").removeClass('clicked');
    // });

    AOS.init({
      disable: "mobile"
    });
  }

  if ($('body').data('gmaps') && $('#mappa').length) {
    loadScript('//www.google.com/jsapi', function() {
      google.load("maps", "3", {
        other_params: 'key=' + $('body').data('gmaps'), callback: function() {
          map = new google.maps.Map(document.getElementById('mappa'), {
            center: { lat: -34.397, lng: 150.644 },
            styles: mapConfig,
            zoom: 8
          });


          var iconBase = '/img/marker.png';


          var markers = [];
          //some array
          var myLatLng = { lat: 45.464103, lng: 9.196453 };

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: iconBase,
            title: 'Hello World!'
          });

          markers.push(marker);

          var myLatLng = { lat: 46.171292, lng: 9.921525 };

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: iconBase,
            title: 'Hello World!'
          });

          markers.push(marker);

          var bounds = new google.maps.LatLngBounds();
          for(var i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
          }

          map.fitBounds(bounds);
        }
      });
    });
  }

});
