$(function() {

	var domElement = $('#' + instance);

	if(domElement.length) {

		var menu = domElement.find("#menu");

		var portfolio_option = menu.find("#Portfolio");
		portfolio_option.click(function(e) {

			e.preventDefault();
			$("#Portfolio").trigger("click");

		});

	}

});