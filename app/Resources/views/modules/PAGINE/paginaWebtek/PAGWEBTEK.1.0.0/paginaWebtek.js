$(function() {
    if ($('#' + instance).length) {

        if($(".navbar-default").length) {
          $(".navbar-default").css('position', 'fixed');
        }

        $(document).ready(function() {
          if($(".box-wrap-webtek").length) {
            $("body").css('overflow-x', 'hidden');
            $(".box-wrap-webtek").addClass('mostra');
          }
        });

        if ($("#"+instance+' .slider-container').length) {
            $("#"+instance+' .slider-container').slick({
              arrows: false,
              dots: false,
              autoplay: true,
              autoplaySpeed: 3000,
              fade: true
            });
        }
    }
});
