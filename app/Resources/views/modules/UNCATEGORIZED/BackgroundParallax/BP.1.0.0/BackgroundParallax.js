$(document).ready(function() {
  if ($('.title_with_background_image').length) {
    $(".title_with_background_image .layer").height($(".title_with_background_image").outerHeight()*2);
    var topDistance = $(".title_with_background_image").offset().top-$(window).scrollTop()-$(".title_with_background_image .layer").outerHeight()-300;
    $(".title_with_background_image .layer").each(function(index, el) {
      var depth = $(this).data('depth');
      var movement = (topDistance * depth)
      var translate3d = 'translate3d(0, ' + movement + 'px, 0)'
      $(this).css("transform",translate3d);
      $(this).css("-moz-transform",translate3d);
      $(this).css("-o-transform",translate3d);
      $(this).css("-webkit-transform",translate3d);
    });
    AOS.init({
      disable:"mobile"
    });
  }
});


$(window).on('scroll', function(event) {
  if ($('.title_with_background_image').length) {
    $(".title_with_background_image .layer").height($(".title_with_background_image").outerHeight()*2);
    var topDistance = $(".title_with_background_image").offset().top-$(window).scrollTop()-$(".title_with_background_image .layer").outerHeight()-300;
    $(".title_with_background_image .layer").each(function(index, el) {
      var depth = $(this).data('depth');
      var movement = (topDistance * depth)
      var translate3d = 'translate3d(0, ' + movement + 'px, 0)'
      $(this).css("transform",translate3d);
      $(this).css("-moz-transform",translate3d);
      $(this).css("-o-transform",translate3d);
      $(this).css("-webkit-transform",translate3d);
    });
  }
});
