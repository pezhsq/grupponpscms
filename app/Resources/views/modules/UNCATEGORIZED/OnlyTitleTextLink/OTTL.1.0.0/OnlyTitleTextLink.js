$(document).ready(function() {
  if($("#" + instance).length){
    var menu_height = 0;
    if($(".navbar").length)
      menu_height = $(".navbar").outerHeight();

    $(window).resize(function(event) {
      if($(".navbar").length)
        menu_height = $(".navbar").outerHeight();
    });

    $(window).on("scroll", function() {
      if($(".navbar").length)
        menu_height = $(".navbar").outerHeight();
    });

    AOS.init({
      disable:"mobile"
    });

    if ($('#' + instance).find('.link_section').find('a').attr('href') == '#') {
      $('.link_section a').on('click', function(e) {
        $('html, body').animate({ scrollTop: $(this).closest('section').height() - menu_height }, 800, 'linear');
      });
    }else if($('#' + instance).length) {
      $('.link_section').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - menu_height }, 800, 'linear');
      });
    }
  }
});
