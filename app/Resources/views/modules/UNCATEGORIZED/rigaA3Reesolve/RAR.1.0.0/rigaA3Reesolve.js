$(function() {

  if ($('#' + instance).length) {

    $('#' + instance).find(".box-triplo-immagine-testi .col-lg-4").matchHeight();

    if (!Modernizr.objectfit) {

      $('#' + instance).find(".box-triplo-immagine-testi .col-img").each(function(index, el) {

        var $container = $(el);
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');

        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }

      });

    }

  }

});