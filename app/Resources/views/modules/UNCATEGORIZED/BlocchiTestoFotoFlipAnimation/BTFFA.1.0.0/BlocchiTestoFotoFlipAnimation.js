// $(window).on("load", function(e) {
//   if($('.nostro_team').length){
//   	CSSPlugin.defaultTransformPerspective = 1000;
//   	//we set the backface
//   	TweenMax.set($(".nostro_team div.cardBack"), {rotationY:-180});
//   	$.each($(".nostro_team div.cardCont"), function(i,element)
//   	{
//   		var frontCard = $(this).children(".cardFront"),
//   	      backCard = $(this).children(".cardBack"),
//   	      tl = new TimelineMax({paused:true});
//
//   		tl
//   			.to(frontCard, 1, {rotationY:180})
//   			.to(backCard, 1, {rotationY:0},0)
//   			.to(element, .5, {z:50},0)
//   			.to(element, .5, {z:0},.5);
//
//   		element.animation = tl;
//   	});
//   	$(".nostro_team div.cardCont").hover(elOver, elOut);
//   	function elOver()
//   	{
//   		if(window.matchMedia('(min-width: 768px)').matches){
//   			var elmt = $(this);
//   			this.animation.play();
//   			elmt.parents(".col_team").css("z-index","2");
//   		}
//   	}
//   	function elOut()
//   	{
//   		if(window.matchMedia('(min-width: 768px)').matches){
//   			var elmt = $(this);
//   	    this.animation.reverse();
//   			setTimeout(function(){
//   				elmt.parents(".col_team").css("z-index","");
//   			},1000);
//   		}
//   	}
//   }
// });

$(document).ready(function(){
  if($('.nostro_team').length){
  	// $('.card_team').matchHeight({
  	// 	byRow:false,
  	// 	target:$(".nostro_team .cardBack img")
  	// });
  	$('.wrapper_card').matchHeight({
  		byRow:false
  	});
  	$.fn.matchHeight._throttle = 100;
  	if(window.matchMedia('(max-width: 767px)').matches){
  		$('.nostro_team .row_team').slick({
  			dots: true,
  			infinite: true,
  			slidesToShow: 2,
  			slidesToScroll: 2,
  			cssEase: 'linear',
  			responsive: [
  				{
  					breakpoint: 480,
  					settings: {
  						slidesToShow: 1,
  						slidesToScroll: 1,
  						infinite: true,
  						dots: false
  					}
  				}
  			]
  		});
  	}
  }
});

$(window).resize(function(){
  if($('.nostro_team').length){
  	if(window.matchMedia('(max-width: 767px)').matches ){
  		if(!$(".nostro_team .row_team").hasClass("slick-initialized")){
  			$('.nostro_team .row_team').slick({
  				dots: true,
  				infinite: true,
  				slidesToShow: 2,
  				slidesToScroll: 2,
  				cssEase: 'linear',
  				responsive: [
  					{
  						breakpoint: 480,
  						settings: {
  							slidesToShow: 1,
  							slidesToScroll: 1,
  							infinite: true,
  							dots: false
  						}
  					}
  				]
  			});
  		}
  	}else{
  		if($(".nostro_team .row_team").hasClass("slick-initialized")){
  			setTimeout(function(){
  				$('.nostro_team .row_team').slick('unslick');
  			},100);
  		}
  	}
  }
});
