$(document).ready(function() {

  $(window).on('scroll', function(event) {
    var topDistance = $(window).scrollTop()-$(".sezione.testo-parallax").height();
    $(".sezione.testo-parallax .layer").each(function(index, el) {
      var depth = $(this).data('depth');
      var movement = -(topDistance * depth);
      var translate3d = 'translate3d(0, ' + movement + 'px, 0)';
      $(this).css("transform",translate3d);
      $(this).css("-moz-transform",translate3d);
      $(this).css("-o-transform",translate3d);
      $(this).css("-webkit-transform",translate3d);
    });
  });

});
