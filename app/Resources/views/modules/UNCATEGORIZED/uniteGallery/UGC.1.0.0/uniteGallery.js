$(document).ready(function() {

    if($(".gallery-carousel-component").length) {
      $("#gallery-carousel").unitegallery({
        gallery_width:"100%",							//gallery width
        gallery_height:"100%",
        theme_enable_fullscreen_button: false,	//show, hide the theme fullscreen button. The position in the theme is constant
        theme_enable_play_button: false,
        gallery_autoplay:true,						//true / false - begin slideshow autoplay on start
        gallery_play_interval: 3000,				//play interval of the slideshow
        slider_transition: "fade",					//fade, slide - the transition of the slide change
        slider_transition_speed:1000,				//transition duration of slide change
        slider_control_zoom:false,					//true, false - enable zooming control
        strip_thumbs_align: "center",	    //left, center, right, top, middle, bottom - the align of the thumbs when they smaller then the strip size.
        slider_enable_progress_indicator: false,
        slider_enable_zoom_panel: false,
        strippanel_handle_offset: 30,
      });
    }

});
