$(document).ready(function() {

  if($(".gallery-tiles-component").length) {
    $("#gallery-tiles").unitegallery({
    gallery_theme: "tiles",
    tiles_type: "nested",
    tiles_nested_optimal_tile_width: 500,
    tiles_space_between_cols: 15,
    tiles_min_columns: 1,
    lightbox_show_textpanel: true,
    lightbox_textpanel_width: 550,
    lightbox_textpanel_enable_title: false,
    lightbox_textpanel_enable_description: true
    });
  }

});
