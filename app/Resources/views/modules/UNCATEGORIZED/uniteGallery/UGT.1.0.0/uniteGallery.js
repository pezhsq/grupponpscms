$(document).ready(function() {

  if($(".gallery-tiles-component").length) {
    $("#gallery-tiles").unitegallery({
    gallery_theme: "tiles",
    tiles_type: "nested",
    tiles_nested_optimal_tile_width: 650,
    tile_enable_textpanel: false,
    tile_textpanel_source: "desc",
    tile_textpanel_always_on: false,
    tile_textpanel_appear_type: "slide",
    tile_textpanel_position:"inside_bottom",
    tile_textpanel_offset:0,
    lightbox_show_textpanel: true,
    lightbox_textpanel_width: 550,
    lightbox_textpanel_enable_title: false,
    lightbox_textpanel_enable_description: true
    });
  }

});
