$(document).ready(function() {
    var menu_height=80;
    if($("#"+instance).length) {
        AOS.init({
            disable: "mobile"
        });
    }
    if ($('#' + instance).find('.pulsante').find('a').attr('href') == '#') {
        $("#"+ instance +' .pulsante .freccia').on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: $(this).parents('section').offset().top + $(this).closest('section').outerHeight() - menu_height }, 800, 'linear');
        });
    }else if($('#' + instance).length) {
        $("#"+ instance +' .pulsante .freccia').on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - menu_height }, 800, 'linear');
        });
    }

});
