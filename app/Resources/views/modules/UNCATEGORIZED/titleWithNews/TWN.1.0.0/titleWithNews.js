$(document).ready(function () {
  if($("#"+instance).length){
    if (!Modernizr.objectfit) {
      $('#' + instance).find(".block-news").each(function(index, el) {
        var $container = $(el).find(".wrapper-img");
        var imgUrl = $container.find(".img-ofi").attr("src");
        $container.find(".img-ofi").css('opacity', '0');
        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }
      });
    }
    $("#" + instance + " .title_with_news .match_height_text").matchHeight();
    $("#" + instance + " .title_with_news .match_height_title").matchHeight();
  }
});
