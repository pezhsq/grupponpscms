$(document).ready(function() {
  if ($('.four_columns_text_slider .slider').length) {
    $('.four_columns_text_slider .slider').slick({
      centerMode: true,
      centerPadding: '0',
      slidesToShow: 3,
      slidesToScroll:1,
      adaptiveHeight: true,
      dots: true,
      autoplay: true,
      autoplaySpeed: 5000,
      infinite:true,
      responsive: [
        {
          breakpoint: 900,
          settings: {
            arrows: false,
            centerMode: true,
            autoplay: true,
            autoplaySpeed: 5000,
            slidesToShow: 1,
            centerPadding: '11%',
          }
        }
      ]
    });
    AOS.init({
      disable: "mobile"
    });
  }
});
