$(document).ready(function() { 
  if ($('.text_button_and_background_parallax').length) {
    $(".text_button_and_background_parallax .layer").height($(".text_button_and_background_parallax").outerHeight()*2);
    var topDistance = $(".text_button_and_background_parallax").offset().top-$(window).scrollTop()-$(".text_button_and_background_parallax .layer").outerHeight()-300;
    $(".text_button_and_background_parallax .layer").each(function(index, el) {
      var depth = $(this).data('depth');
      var movement = (topDistance * depth)
      var translate3d = 'translate3d(0, ' + movement + 'px, 0)'
      $(this).css("transform",translate3d);
      $(this).css("-moz-transform",translate3d);
      $(this).css("-o-transform",translate3d);
      $(this).css("-webkit-transform",translate3d);
    });
  }
});

$(window).on('scroll', function(event) {
  if ($('.text_button_and_background_parallax').length) {
    $(".text_button_and_background_parallax .layer").height($(".text_button_and_background_parallax").outerHeight()*2);
    var topDistance = $(".text_button_and_background_parallax").offset().top-$(window).scrollTop()-$(".text_button_and_background_parallax .layer").outerHeight()-300;
    $(".text_button_and_background_parallax .layer").each(function(index, el) {
      var depth = $(this).data('depth');
      var movement = (topDistance * depth)
      var translate3d = 'translate3d(0, ' + movement + 'px, 0)'
      $(this).css("transform",translate3d);
      $(this).css("-moz-transform",translate3d);
      $(this).css("-o-transform",translate3d);
      $(this).css("-webkit-transform",translate3d);
    });
  }
});
