$(function() {
  if ($('#' + instance).length) {
    var $col_inv = $(".col-inverted");
    var $colImg = $col_inv.find(".col-img");
    var $colText = $col_inv.find(".col-text");

    if (window.matchMedia("(max-width:767px)").matches) {
      $col_inv.find(".row").html("").append($colImg, $colText);
    }

    $(window).resize(function() {
      if (window.matchMedia("(min-width:768px)").matches) {
        $col_inv.find(".row").html("").append($colText, $colImg);
      } else {
        $col_inv.find(".row").html("").append($colImg, $colText);
      }
    });
  }
});
