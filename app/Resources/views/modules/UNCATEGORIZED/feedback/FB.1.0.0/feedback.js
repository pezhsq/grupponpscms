$(document).ready(function() {
  if ($('.clienti_feedback').length) {
    $('.clienti_feedback').slick({
      dots: false,
      arrows: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 7000,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: 'linear',
      responsive: [
        {
          breakpoint: 640,
          settings: {
            dots: false
          }
        }
      ]
    });
  }
});
