/**
 * Created by gabricom on 22/03/2017.
 */
$(function(){
   if($("#"+instance).length){
       objectFitImages();
       var instanza=$("#"+instance);
       instanza.find(".slider").slick({
           "dots":true,
           "autoplay":true
       });

       // $(window).resize();
   }
});

$(window).on('load',function(){
   $(window).resize();
});
if($("#"+instance).length){
    var instanza=$("#"+instance);

    $(window).resize(function(){
        var height=window.innerHeight-$(".header-js .navbar").outerHeight()-$(".footer-js").outerHeight();
        instanza.find(".poweredBy2Blocchi").css("padding-top",$(".header-js .navbar").outerHeight()+"px");
        if(window.innerWidth>960){
            if (height<600) height=600;
            instanza.find(".poweredBy2Blocchi").height(height);
        }
        else {
            instanza.find(".poweredBy2Blocchi").height("");
        }

    })
}