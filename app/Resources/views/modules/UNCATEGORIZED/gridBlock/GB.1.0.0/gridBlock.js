function addCommas(n) {
  var rx = /(\d+)(\d{3})/;
  return String(n).replace(/^\d+/, function(w) {
    while(rx.test(w)) {
      w = w.replace(rx, '$1.$2');
    }
    return w;
  });
}
function startCounter() {
  $(".animation_numbers_homepage .title_numeri").each(function(index, el) {
    var element = $(this);
    $({ someValue: 0 }).animate({
      someValue: element.data('valore')
    }, {
      duration: 3000,
      easing: 'swing',
      step: function() {
        element.html(addCommas(Math.round(this.someValue)));
      }
    });
  });
}

$(document).ready(function() {

  if ($('#numeri').length) {

    $('.animation_numbers_homepage .wrapper_blocchi').matchHeight({
      byRow: false
    });


    var waypoint = new Waypoint({
      element: document.getElementById('numeri'),
      handler: function() {
        startCounter();
      },
      offset: '60%'
    });
    if (window.matchMedia('(max-width: 500px)').matches) {
      $(".progetti_img").insertAfter('.numeri_progetti');
      $(".coffee_img ").insertAfter('.numeri_coffee');
    }

  }
});

$(window).resize(function(event) {
  if ($('#numeri').length) {

    if (window.matchMedia('(max-width: 500px)').matches) {
      $(".progetti_img").insertAfter('.numeri_progetti');
      $(".coffee_img ").insertAfter('.numeri_coffee');
    } else {
      $(".progetti_img").insertBefore('.numeri_progetti');
      $(".coffee_img ").insertBefore('.numeri_coffee');
    }
    
  }
});
