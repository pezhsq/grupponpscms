$(function() {
  if ($('#' + instance).length) {
    $('#' + instance).find(".big-slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      arrows: false,
      dots: true
    });

    $('#' + instance).find(".big-slider").on('beforeChange', function(event, slick, currentSlide, nextSlide) {
      selettore = ".item:eq(" + nextSlide + ")";
      $slide = $('#' + instance).find(selettore);
      var $currentVideo = $('#vid' + currentSlide);
      if ($currentVideo.length) {
        var vid = $currentVideo.get(0);
        vid.pause();
        vid.currentTime = 0;
      }
      var $video = $('#vid' + nextSlide);
      if ($video.length) {
        var vid = $video.get(0);
        vid.play();
      }
    });

    if ($('#' + instance).find('#vid0').length) {
      var $currentVideo = $('#' + instance).find('#vid0');
      var vid = $currentVideo.get(0);
      vid.play();
    }


    $('#' + instance).find(".scroll-down").click(function(event) {
      /* Act on the event */
      $('html, body').animate({
        scrollTop: $('#' + instance).height() - 80
      }, 1000);
    });

    if (!Modernizr.objectfit) {

      $('#' + instance).find(".big-slider .item").each(function(index, el) {

        var $container = $(el);
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');

        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }

      });

    }

  }

});