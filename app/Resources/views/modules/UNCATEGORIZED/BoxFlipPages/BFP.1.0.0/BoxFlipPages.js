$(document).ready(function() {

  if ($(".cosa_realizziamo").length) {

    Page.init();
    $(".bb-custom-side").matchHeight({
      byRow: false
    });

    $(".cosa_realizziamo .bb-bookblock .bb-item:first-child .bb-nav-prev").remove();
    $(".cosa_realizziamo .bb-bookblock .bb-item:last-child .bb-nav-next").remove();

    $.fn.matchHeight._afterUpdate = function(event, groups) {
      if (window.matchMedia('(max-width: 991px)').matches) {
        $('#bb-bookblock').removeClass("bb-vertical").addClass("bb-horizontal");
        $(".bb-bookblock").height($(".text-item").outerHeight() * 2);
      }
      else {
        $('#bb-bookblock').removeClass("bb-horizontal").addClass("bb-vertical");
        $(".bb-bookblock").height($(".text-item").outerHeight());
      }
    };
  }

});


var Page = (function() {
  var config = {
      $bookBlock: $("#bb-bookblock"),
      $navNext: $('.bb-nav-next'),
      $navPrev: $('.bb-nav-prev'),
    },
    init = function() {

      if (window.matchMedia('(max-width: 991px)').matches) {
        config.$bookBlock.bookblock({
          speed: 1200,
          orientation: 'horizontal',
        });
      } else {
        config.$bookBlock.bookblock({
          speed: 1200,
          orientation: 'vertical',
        });
      }
      initEvents();
    },
    initEvents = function() {

      var $slides = config.$bookBlock.children();

      // add navigation events
      config.$navNext.on('click touchstart', function() {
        config.$bookBlock.bookblock('next');
        return false;
      });

      config.$navPrev.on('click touchstart', function() {
        config.$bookBlock.bookblock('prev');
        return false;
      });

      // add swipe events
      if (window.matchMedia('(max-width: 991px)').matches) {

      } else {
        $slides.on({
          'swipeleft': function(event) {
            config.$bookBlock.bookblock('next');
            return false;
          },
          'swiperight': function(event) {
            config.$bookBlock.bookblock('prev');
            return false;
          }
        });
      }
    };
  return { init: init };
})();
