$(function() {
  if ($('#' + instance).length) {

    $('#' + instance).find(".sezione-box-flottante-stats .row-stats .col-lg-3").matchHeight();

    var waypoint = new Waypoint({
      element: document.getElementById('numeri'),
      handler: function() {
        $(".odometer").each(function(index, el) {
          $(el).html("");
          var number = $(el).data("num");
          $(el).html(number);
        });
      },
      offset: '50%'
    });
  }
});