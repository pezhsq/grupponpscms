$(function () {

    if ($('#' + instance).length) {

        var loading = false;
        var page = 1;
        var pages = [];

        if (!Modernizr.objectfit) {
            $('#' + instance).find(".block-news").each(function (index, el) {
                var $container = $(el).find(".wrapper-img");
                var imgUrl = $container.find(".img-ofi").attr("src");
                $container.find(".img-ofi").css('opacity', '0');
                if (imgUrl) {
                    $container
                        .css("background-image", "url(" + imgUrl + ")")
                        .css("background-size", "cover")
                        .css("background-position", "50% 50%");
                }
            });
        }
        $("#" + instance + " .title_with_news .match_height_text").matchHeight();
        $("#" + instance + " .title_with_news .match_height_title").matchHeight();

        if ($('.pagination').length) {

            $('.pagination').closest('.navigation').hide();

            $('.pagination').find('.page').each(function () {
                pages.push(parseInt($(this).text()));
            });

            $(window).bind('scroll', function () {
                if (!loading && pages.length && ($(window).scrollTop() >= $('.block-news:last').offset().top + $('.block-news:last').outerHeight() + $('.pagination').outerHeight() - window.innerHeight)) {
                    console.log('loading!');
                    loading = true;
                    //$('<div class="row loading"><img src="/img/ajax-loader.gif" style="z-index:1000;width:50px;height:50px;" /></div>"').appendTo($('.blog-bottom-left'));
                    if (pages.length) {
                        current = pages.shift();
                        slugs = location.pathname.split('/');
                        slugs.shift();
                        slugs.shift();
                        slugs.pop();
                        var endPoint = '/blog-json';
                        endPoint += '/' + current;

                        if (slugs.length) {
                            endPoint += '/' + slugs;
                        }

                        console.log(endPoint);
                        $.ajax({
                            type: 'POST',
                            url: endPoint,
                            dataType: 'json',
                            success: function (ret_data) {
                                console.log(ret_data);
                                if (ret_data.result) {
                                    var blogPost = HandleBarHelper.compile('blogPost');
                                    console.log(blogPost);
                                    html = '';
                                    for (var i = 0; i < ret_data.data.length; i++) {
                                        html += blogPost(ret_data.data[i]);
                                    }
                                    console.log(html);
                                    //$('.loading').remove();
                                    $(html).insertAfter($('.block-news:last'));

                                    loading = false;


                                } else {
                                    var opts = {
                                        'type': 'danger',
                                        'titolo': _('Attenzione'),
                                        'content': ret_data.errors.join('<br />'),
                                        'OK': 'Ok',
                                        'callback': null
                                    };
                                    HandleBarHelper.alert(opts);
                                }
                            }
                        });

                    }

                }
            });

        }
    }

});
