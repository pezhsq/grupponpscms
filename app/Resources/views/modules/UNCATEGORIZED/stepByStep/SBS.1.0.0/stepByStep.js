var stopAnimation = 0;
var animationStarted = 0;

function starAnimationTree(number) {
  animationStarted = 1;
  if (stopAnimation == 0) {
    $(".tree_animation_homepage .number_slider span").removeClass('selected');
    $(".tree_animation_homepage .number_slider span[data-valore='" + number + "']").addClass('selected');
    for(var x = 1; x <= 5; x++) {
      $(".tree_animation_homepage .text" + x).removeClass('text_fade_in');
      $(".tree_animation_homepage .subtitle" + x).removeClass('text_fade_in');
    }
    if (number == 1) {
      for(var x = 2; x <= 5; x++) {
        if (x != number) {
          $(".tree_animation_homepage #albero" + x).fadeOut('400', function() {
          });
        }
      }
    }
    $(".tree_animation_homepage #albero" + number).fadeIn('400', function() {
    });
    $(".tree_animation_homepage .text" + number).addClass('text_fade_in');
    $(".tree_animation_homepage .subtitle" + number).addClass('text_fade_in');
    if (number > 1) {
      $(".tree_animation_homepage #albero1").fadeOut('400', function() {
      });
      $(".tree_animation_homepage .text1" + number).removeClass('text_fade_in');
      $(".tree_animation_homepage .subtitle1" + number).removeClass('text_fade_in');
    }
    number++;
    if (number > 5) {
      number = 1;
    }
    setTimeout(function() {
      starAnimationTree(number);
    }, 5000);
  }
}

$(document).ready(function() {
  if($(".tree_animation_homepage").length){
    $('.tree_animation_homepage .albero_wrapper').matchHeight({
      byRow: false,
      target: $(".tree_animation_homepage img")
    });
    $('.tree_animation_homepage .wrapper_text').matchHeight({
      byRow: false
    });
    

    $.fn.matchHeight._afterUpdate = function(event, groups) {
      $('.tree_animation_homepage .wrapper_row').height($('.tree_animation_homepage .wrapper_text').height());
    };

    $(".tree_animation_homepage .number_slider span").click(function(event) {
      stopAnimation = 1;
      for(var x = 1; x <= 5; x++) {
        $(".tree_animation_homepage .text" + x).removeClass('text_fade_in');
        $(".tree_animation_homepage .subtitle" + x).removeClass('text_fade_in');
      }
      if ($(this).data('valore') > 1) {
        $(".tree_animation_homepage #albero1").fadeOut('400', function() {
        });
      }
      if ($(this).data('valore') < $(".tree_animation_homepage .number_slider .selected").data('valore')) {
        for(var x = $(this).data('valore') + 1; x <= 5; x++) {
          $(".tree_animation_homepage #albero" + x).fadeOut('400', function() {
          });
        }
      }
      $(".tree_animation_homepage .number_slider span").removeClass('selected');
      $(this).addClass('selected');
      $(".tree_animation_homepage #albero" + $(this).data('valore')).fadeIn('400', function() {
      });
      $(".tree_animation_homepage .text" + $(this).data('valore')).addClass('text_fade_in');
      $(".tree_animation_homepage .subtitle" + $(this).data('valore')).addClass('text_fade_in');
    });

    if ($('#step-by-step').length) {
      var waypointAlbero = new Waypoint({
        element: document.getElementById('step-by-step'),
        handler: function() {
          if (animationStarted == 0)
            starAnimationTree(1);
        },
        offset: '60%'
      });
    }
  }

});
