$(document).ready(function() {
  if($(".four-blocks-with-list").length){
    $.fn.matchHeight._maintainScroll = true;
    $('.four-blocks-with-list .col-md-6').matchHeight();
    AOS.init({
      disable:"mobile"
    });
  }

});
