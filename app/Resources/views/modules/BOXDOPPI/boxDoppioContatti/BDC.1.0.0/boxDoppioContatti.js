$(function() {

  if ($('#' + instance).length) {

    $('#' + instance).find(".sezione.contatti .col-lg-6").matchHeight();

    function validateFormBooking($form) {

      var errori = [];

      if ($.trim($form.find('#fullname').val()) == '') {
        errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Nome e Cognome')));
      }
      if ($.trim($form.find('#email').val()) == '') {
        errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Email')));
      }
      if ($.trim($form.find('#phone').val()) == '') {
        errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Telefono')));
      }
      if ($form.find('#privacy:checked').length == 0) {
        errori.push(_('Il campo <strong>%s</strong> è obbligatorio', 'default', _('Privacy')));
      }

      if (errori.length) {

        return errori;

      }

      return true;

    }

    $container = $('#' + instance);

    $container.find(".btn-plus-passeggeri").click(function(event) {
      event.preventDefault();
      $quantityContainer = $(this).closest('.quantity');
      var n = $quantityContainer.find('p').html();
      n = parseInt(n) + 1;
      $quantityContainer.find('p').html(n);
    });

    $container.find('.btn-minus-passeggeri').click(function(event) {
      event.preventDefault();
      $quantityContainer = $(this).closest('.quantity');
      var n = $quantityContainer.find('p').html();
      if (n != 1) {
        n = parseInt(n) - 1;
      }
      $quantityContainer.find('p').html(n);
    });

    var $datetimePartenza = $container.find('#datetime-partenza-form');
    var $datetimeArrivo = $container.find('#datetime-arrivo-form');

    var DateTimePartenza = new Date($datetimePartenza.val());
    var DateTimeArrivo = new Date($datetimeArrivo.val());


    $datetimePartenza.datetimepicker({
      'locale': $datetimePartenza.closest('.datepicker').data('locale'),
      'format': 'YYYY-M-D',
      'showClose': true
    });
    $datetimePartenza.data('DateTimePicker').date(DateTimePartenza);
    $datetimePartenza.data('DateTimePicker').minDate(DateTimePartenza);

    $datetimeArrivo.datetimepicker({
      'locale': $datetimeArrivo.closest('.datepicker').data('locale'),
      'format': 'YYYY-MM-DD',
      'showClose': true
    });
    $datetimeArrivo.data('DateTimePicker').date(DateTimeArrivo);
    $datetimeArrivo.data('DateTimePicker').minDate(DateTimePartenza);

    $(document).on('click', '.datepicker', function(e) {
      $this = $(this);
      console.log($this.find('input'));
      $this.find('input').data('DateTimePicker').show();
    });

    $(document).on('dp.change', function(e) {

      console.log(e.target.id);

      if (e.oldDate) {

        $input = $(e.target);

        var m = e.date;
        var $container = $input.closest('.datepicker');

        $container.find('.giorno').html(m.format('DD'));
        $container.find('.month-year').html(capitalizeFirstLetter(m.format('MMM')) + '<br />' + m.get('year'));
        $input.data('DateTimePicker').hide();

        if (e.target.id == 'datetime-partenza') {
          if ($datetimePartenza.data('DateTimePicker').date() > $datetimeArrivo.data('DateTimePicker').date()) {
            m.add(1, 'day');
            $datetimeArrivo.data('DateTimePicker').minDate(m);
          }
        }

        if (e.target.id == 'datetime-partenza-form') {

          $dateTimePartenzaForm = $(e.target);
          $dateTimeArrivoForm = $('#datetime-arrivo-form');

          $datetimePartenza.data('DateTimePicker').date($dateTimePartenzaForm.data('DateTimePicker').date());

          if ($dateTimePartenzaForm.data('DateTimePicker').date() > $dateTimeArrivoForm.data('DateTimePicker').date()) {
            m = $dateTimePartenzaForm.data('DateTimePicker').date();
            m.add(1, 'day');
            $dateTimeArrivoForm.data('DateTimePicker').minDate(m);
          }
          $datetimeArrivo.data('DateTimePicker').date($dateTimeArrivoForm.data('DateTimePicker').date());

        }

        if (e.target.id == 'datetime-arrivo-form') {
          $dateTimeArrivoForm = $(e.target);
          $datetimeArrivo.data('DateTimePicker').date($dateTimeArrivoForm.data('DateTimePicker').date());
        }

      }
    });


    $container.find('.btn-send').on('click', function(e) {
      e.preventDefault();
      $container.find('form').attr({ 'action': $container.find('form').data('endpoint'), 'method': 'post' });

      var validForm = validateFormBooking($container.find('form'));

      if (validForm === true) {
        HandleBarHelper.lockScreen({ 'message': 'Attendere prego' });
        $container.find('form').ajaxSubmit({
          'success': function(ret_data) {
            HandleBarHelper.unlockScreen();
            if (ret_data.result) {
              $container.find('form').resetForm();
              var opts = {
                'type': 'success',
                'titolo': _('Richiesta informazioni'),
                'content': _('Abbiamo ricevuto il tuo messaggio, ti risponderemo al più presto'),
                'OK': 'Ok',
                'callback': null
              };
              HandleBarHelper.alert(opts);
            } else {
              var opts = {
                'type': 'danger',
                'titolo': _('Attenzione'),
                'content': ret_data.errors.join('<br />'),
                'OK': 'Ok',
                'callback': null
              };
              HandleBarHelper.alert(opts);
            }
          }
        });
      } else {
        var opts = {
          'type': 'danger',
          'titolo': _('Attenzione'),
          'content': validForm.join('<br />'),
          'OK': 'Ok',
          'callback': null
        };
        HandleBarHelper.alert(opts);
      }
    });
  }


});