/**
 * Created by jd on 12/05/17.
 */
$(function () {
    $('#' + instance).find(".col-bdn").each(function (index, el) {
        $('#' + instance).find(".col-bdn").matchHeight();

        var $container = $(el).find(".col-img");
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');

        if (imgUrl) {
            $container
                .css("background-image", "url(" + imgUrl + ")")
                .css("background-size", "cover")
                .css("background-position", "50% 50%");
        }

    });
    if ($('#' + instance).length) {
      AOS.init({
        disable:"mobile"
      });
    }
});
