$(function() {
  if ($('#' + instance).length) {

    $('#' + instance).find(".box-doppio-stats .col-img").matchHeight({
      target: $(".box-doppio-stats .col-griglia")
    });

    var waypoint = new Waypoint({
      element: $('#' + instance).find('#stats').get(0),
      handler: function() {
        $('#' + instance).find(".odometer").each(function(index, el) {
          $(el).html("");
          var number = $(el).data("num");
          $(el).html(number);
        });
      },
      offset: '50%'
    });

    if (!Modernizr.objectfit) {

      $('#' + instance).find(".box-doppio-stats .col-img").each(function(index, el) {

        var $container = $(el);
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');

        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }

      });

    }

  }

});