$(function() {
  if ($('#' + instance).length) {
    
    $('#' + instance).find(".slider-opinioni").slick({
      slidesToShow: 1,
      dots: true,
      infinite: false,
      arrows: false
    });

    $('#' + instance).find(".box-doppio-opinioni .col-img").matchHeight({
      target: $(".box-doppio-opinioni .col-slider")
    });

    if (!Modernizr.objectfit) {
      $('#' + instance).find(".box-doppio-opinioni .col-img").each(function(index, el) {
        var $container = $(el);
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');
        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }
      });
    }
  }
});