$(function() {
  if ($('#' + instance).length) {
    if ($('#' + instance).find(".box-doppio-offerte").length) {

      $('#' + instance).find(".box-doppio-offerte .col-lg-6").matchHeight();

      if (!Modernizr.objectfit) {

        $('#' + instance).find(".box-doppio-offerte").each(function(index, el) {

          var $container = $(el).find(".col-img");
          var imgUrl = $container.find("> img").prop("src");
          $container.find("> img").css('opacity', '0');

          if (imgUrl) {
            $container
            .css("background-image", "url(" + imgUrl + ")")
            .css("background-size", "cover")
            .css("background-position", "50% 50%");
          }

        });

      }

    }

  }
});