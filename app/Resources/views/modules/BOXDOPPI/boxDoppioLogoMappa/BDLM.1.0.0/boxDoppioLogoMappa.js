$(function() {
    if ($('#' + instance).length) {

      $('#' + instance).find(".box-mappa-contatti .sameHeight").matchHeight();


      var $map = $('#' + instance).find('.mappa');

      var apiKey = $('body').data('gmaps');
      var coordinate = '9.9224325,46.1712462';
      var zoom = 10;
      var icon = false;
      var infoContent = false;

      if ($map.data('center')) {
        coordinate = $map.data('center');
      }
      if ($map.data('zoom')) {
        zoom = $map.data('zoom');
      }
      if ($map.data('cursor')) {
        icon = $map.data('cursor');
      }
      if ($map.data('infowindow')) {
        infoContent = $map.data('infowindow');
      }

      var coords = coordinate.split(',');

      if (apiKey) {
        loadScript('//www.google.com/jsapi', function() {
          google.load("maps", "3", {
            other_params: 'key=' + apiKey, callback: function() {
              console.log(coordinate[0]);
              var center = { 'lat': parseFloat(coords[0]), 'lng': parseFloat(coords[1]) };
              console.log(center);
              var mapOptions = {
                center: center,
                zoom: zoom,
                scrollwheel: false
              };

              if (typeof(mapConfig) !== 'undefined') {
                mapOptions.styles = mapConfig
              }

              var map = new google.maps.Map(document.getElementById('map'), mapOptions);
              var markerOptions = {
                position: center,
                map: map
              };
              if (icon) {
                markerOptions.icon = icon;
              }

              var marker = new google.maps.Marker(markerOptions);
              if (infoContent) {
                var infowindow = new google.maps.InfoWindow({
                  content: infoContent
                });
                infowindow.open(map, marker);
                marker.addListener('click', function() {
                  infowindow.open(map, marker);
                });
              }

            }

          });
        });
      } else {
        console.error('Non è presente l\'api key di google maps');
      }

    }
  }
);