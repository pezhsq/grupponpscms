$(function() {
  if ($('#' + instance).length) {

    $('#' + instance).find(".slider-dx").slick({
      dots: true,
      autoplay: true,
      arrows: false,
      autoplaySpeed: 2000
    });
    $('#' + instance).find(".sezione.box-doppio-gallery .col-xs-12").matchHeight();

  }
})
;