$(function() {
  if ($('#' + instance).length) {

    $('#' + instance).find(".box-doppio-testo-lista .sameHeight").matchHeight();

    if (!Modernizr.objectfit) {

      $('#' + instance).find(".box-doppio-testo-lista .col-img").each(function(index, el) {

        var $container = $(el);
        var imgUrl = $container.find("> img").prop("src");
        $container.find("> img").css('opacity', '0');

        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }

      });

    }
  }
});