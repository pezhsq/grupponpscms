$(function() {

  $('#socialShareFB').on('click', function(e) {
    e.preventDefault();

    $a = $(this);

    FB.ui({
      method: 'share',
      display: 'popup',
      href: $a.attr('href')
    });

  });

  $('#socialShareTW').on('click', function(e) {
    e.preventDefault();
    $a = $(this);
    window.open($a.attr('href'), 'twitter', 'width=' + ($(window).width() * 0.8) + ',height=' + ($(window).height() * 0.8));
  });

  $('#socialShareLI').on('click', function(e) {
    e.preventDefault();
    $a = $(this);
    window.open($a.attr('href'), 'linkedin', 'width=' + ($(window).width() * 0.8) + ',height=' + ($(window).height() * 0.8));
  });

});