$(function() {
  if ($('#' + instance).length) {
    $('#' + instance).find(".gallery-articolo").slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      arrows: true,
      variableWidth: true,
      prevArrow: '<i class="fa fa-angle-left fa-3x" aria-hidden="true"></i>',
      nextArrow: '<i class="fa fa-angle-right fa-3x" aria-hidden="true"></i>'
    });

    $('#' + instance).find('.gallery-articolo').slickLightbox({ 'itemSelector': 'div > a' });
  }
});
