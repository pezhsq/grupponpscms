$(function() {

  if ($('#' + instance).length) {

    var $form = $('#' + instance).find('form');

    var mandatory = ['commento_form_nome', 'commento_form_email', 'commento_form_messaggio'];

    $form.find('[type="submit"]').on('click', function(e) {

        e.preventDefault();

        var errori = [];

        for(var i = 0; i < mandatory.length; i++) {

          if ($.trim($('#' + mandatory[i]).val()) == '') {
            errori.push(_('Il campo "%s" è obbligatorio', '', _(mandatory[i])));
          }
        }

        console.log(errori);

        if (errori.length) {

          HandleBarHelper.alert({
            'titolo': _('Attenzione'),
            'content': errori.join('<br />')
          });

        } else {
          var $btn = $(this);

          $btn.attr('disabled', true);
          $btn.val('Invio commento in corso...');
          $btn.html(_('Invio commento in corso...', 'blog'));

          $form.ajaxSubmit({
            'dataType': 'json',
            'success': function(ret_data) {
              $btn.removeAttr('disabled');
              $btn.val(_('Lascia un commento'));
              $btn.html(_('Lascia un commento'));

              if (ret_data.result) {

                HandleBarHelper.alert({
                  'titolo': _('Attenzione'),
                  'content': _('Il tuo commento è in attesa di approvazione, grazie per il tuo contributo'),
                  'type': 'success'
                });

                $form.resetForm();

              } else {

                HandleBarHelper.alert({
                  'titolo': _('Attenzione'),
                  'content': ret_data.errors.join('<br />')
                });

              }
            }
          });


        }


      }
    );

  }
})
;