$(function() {

  if ($('#' + instance).length) {

    var loading = false;
    var page = 1;
    var pages = [];

    $('#' + instance).find(".sezione.main-blog .left-blog .row_articolo").matchHeight();

    if ($('.pagination').length) {

      $('.pagination').closest('.navigation').hide();

      $('.pagination').find('.page').each(function() {
        pages.push(parseInt($(this).text()));
      });

      $(window).bind('scroll', function() {
        if (!loading && pages.length && ($(window).scrollTop() >= $('.row_articolo:last').offset().top + $('.row_articolo:last').outerHeight() + $('.pagination').outerHeight() - window.innerHeight)) {
          console.log('loading!');
          loading = true;
          //$('<div class="row loading"><img src="/img/ajax-loader.gif" style="z-index:1000;width:50px;height:50px;" /></div>"').appendTo($('.blog-bottom-left'));
          if (pages.length) {
            current = pages.shift();
            slugs = location.pathname.split('/');
            slugs.shift();
            slugs.shift();
            slugs.pop();
            var endPoint = '/blog-json';
            endPoint += '/' + current;

            if (slugs.length) {
              endPoint += '/' + slugs;
            }


            $.ajax({
              type: 'POST',
              url: endPoint,
              dataType: 'json',
              success: function(ret_data) {
                if (ret_data.result) {
                  var blogPost = HandleBarHelper.compile('blogPost');
                  console.log(blogPost);
                  html = '';
                  for(var i = 0; i < ret_data.data.length; i++) {
                    html += blogPost(ret_data.data[i]);
                  }
                  console.log(html);
                  //$('.loading').remove();
                  $(html).insertAfter($('.row_articolo:last'));

                  loading = false;


                } else {
                  var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                  };
                  HandleBarHelper.alert(opts);
                }
              }
            });

          }

        }
      });

    }
  }

});
