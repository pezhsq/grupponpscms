$(function() {
	var domElement = $("#" + instance);
	if($('#' + instance).length) {
		var loading = false;
		var page    = 1;
		var pages   = [];

		if($('.pagination').length) {

			$('.pagination').closest('.navigation').hide();

			$('.pagination').find('.page').each(function() {
				pages.push(parseInt($(this).text()));
			});

			var controller = new ScrollMagic.Controller();

			// build scene
			var scene = new ScrollMagic.Scene({triggerElement: "#loader", triggerHook: "onEnter"})
							.addTo(controller)
							.on("enter", function (e) {
								if (!$("#loader").hasClass("active")) {
									$("#loader").addClass("active");
									// simulate ajax call to add content using the function below
									loadArticoli();
								}
							});

							function loadArticoli () {
				    		// "loading" done -> revert to normal state
								if(pages.length) {
									current = pages.shift();
									slugs   = location.pathname.split('/');
									slugs.shift();
									slugs.shift();
									slugs.pop();
									var endPoint = '/blog-json';
									endPoint += '/' + current;

									var url = new URL(window.location.href);
									var query = url.searchParams.get("query");
									endPoint += "?query=" + query;

									$.ajax({
										type: 'POST',
										url: endPoint,
										dataType: 'json',
										success: function(ret_data) {
				              if (ret_data.result) {
												var blogPost = HandleBarHelper.compile('blogPost');
												html = '';
												for(var i = 0; i < ret_data.data.length; i++) {
													html += blogPost(ret_data.data[i]);
												}
												$(html).insertAfter($('.row_articolo:last'));
											}
											else {
												var opts = {
													'type': 'danger',
													'titolo': _('Attenzione'),
													'content': ret_data.errors.join('<br />'),
													'OK': 'Ok',
													'callback': null
												};
												HandleBarHelper.alert(opts);
											}
										}
									});

								}
				        else {
				          $("#loader").remove();
				        }

				    		scene.update(); // make sure the scene gets the new start position
				    		$("#loader").removeClass("active");
				    	}

		}
		else {
      $("#loader").remove();
    }

	}
});
