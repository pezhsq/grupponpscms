/**
 * Created by gabricom on 24/03/2017.
 */
var loading = false;
var page = 1;
var pages = [];

$(function() {
  if ($('#' + instance).length) {
    objectFitImages();
    if ($('#' + instance + ' .blog-top').length) {
      $('#' + instance + " .blog-top .sameHeight").matchHeight({
        target: $('#' + instance + " .mainHeight")
      });
    }
  }

});

