$(function() {
  var $instance = $('#' + instance);
  if ($instance.length) {

    $instance.find(".slider").on('init', function(event) {
      event.preventDefault();

      AOS.init();

      var maxH = 0;

      $instance.find('.slide').each(function() {
        if ($(this).height() > maxH) {
          maxH = $(this).height();
        }
      });

      $instance.find('.col-img').height(maxH);
      $instance.find('.col-text').height(maxH);

    });

    $instance.find(".slider").slick({
      centerMode: true,
      slidesToShow: 2,
      dots: true,
      infinite: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    if (!Modernizr.objectfit) {

      $instance.find(".slider .slick-slide").each(function(index, el) {

        var $container = $(el).find(".col-img");
        var imgUrl = $container.find("img").prop("src");
        $container.find("> img").css('opacity', '0');

        if (imgUrl) {
          $container
          .css("background-image", "url(" + imgUrl + ")")
          .css("background-size", "cover")
          .css("background-position", "50% 50%");
        }

      });

    }

  }


});