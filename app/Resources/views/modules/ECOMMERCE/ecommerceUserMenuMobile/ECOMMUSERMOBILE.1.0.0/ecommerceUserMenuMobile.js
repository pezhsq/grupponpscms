$(document).ready(function () {
    if ($("#" + instance).length) {
        if ($("#" + instance).find(".alert").length) {
            $("#" + instance + " .pannello-login-mobile").addClass("opened");
        }
        $(".openUserMenuMobile").click(function (e) {
            e.preventDefault();
            $("#menuMobileFull").collapse('hide');

            $(".pannello-mobile:not('.pannello-login-mobile')").removeClass("opened");
            $("#" + instance + " .pannello-login-mobile").toggleClass("opened");
        });
    }
});