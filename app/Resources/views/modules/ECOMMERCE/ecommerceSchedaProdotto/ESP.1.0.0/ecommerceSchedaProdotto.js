$(function() {
    var adattato = 0;
    if ($('#' + instance).length) {
        $(window).resize(function() {
            if (window.matchMedia("(max-width:767px)").matches) {
                if (!adattato) {
                    $(".mobileHelp").prependTo(".colonna-immagine");
                    adattato = 1;
                }
            } else {
                if (adattato) {
                    $(".mobileHelp").prependTo("colonna-info");
                    adattato = 0;
                }
            }


        });


        $(window).resize();

        $("#" + instance + " .slider-img-prodotto").slick({
            "slidesToShow": 4
        });
        var $instance = $('#' + instance);
        var $priceElement = $instance.find('.price');
        var $availabilityElement = $instance.find('.availability');
        var $imgPrincipale = $instance.find('.img-principale');
        var $bottoneAcquisto = $instance.find('.btn-acquista');

        $bottoneAcquisto.on('click', function(e) {
            e.preventDefault();

            mettiInCarrello($instance);

        });


        checkVarianti($instance, $instance.find(".box-varianti .attribute.attributeSelected").data('id'));


        // click di selezione di un attributo
        $instance.find('.attribute').on('click', function(e) {
            e.preventDefault();

            if ($(this).hasClass("notAvailable")) {
                return false;
            }

            var $attributeRow = $(this).closest('.attributeRow');
            var attributo_selezionato = $(this).data('id');
            checkVarianti($instance, attributo_selezionato);

            $attributeRow.find('.attribute').removeClass('attributeSelected');
            $(this).addClass('attributeSelected');


            if (parseInt($priceElement.data('variable'), 10) === 1) {
                getDatiVariante($instance, function(data) {
                    if (data.prezzo != 'N.A.') {
                        $priceElement.html(number_format(data.prezzo, 2, _(','), _('.')) + ' &euro;');
                    } else {
                        $priceElement.html(data.prezzo);
                    }

                    if (data.available) {
                        $availabilityElement.html(_('Disponibile', 'ecommerce'));
                        $bottoneAcquisto.removeClass('disabled');
                        $availabilityElement.removeClass('disabled');
                    } else {
                        $availabilityElement.html(_('Non disponibile', 'ecommerce'));
                        $bottoneAcquisto.addClass('disabled');
                        $availabilityElement.addClass('disabled');
                    }
                    if (data.firstPhoto) {
                        $imgPrincipale.attr('src', data.firstPhoto);
                    }

                    if (typeof data.warning != 'undefined') {

                        var opts = {
                            'type': 'warning',
                            'titolo': _('Attenzione'),
                            'content': data.warning,
                            'OK': 'Ok',
                            'callback': null
                        };
                        HandleBarHelper.alert(opts);
                    }


                });
            }
        });
    }
});

function checkVarianti($instance, attributo_selezionato) {
    var numero_varianti = $instance.find(".box-varianti").length;
    if (parseInt(numero_varianti) === 2 && attributo_selezionato in varianti) {
        var disattivato = 0;
        $instance.find(".box-varianti:eq(1) .attribute").each(function(index, element) {
            if ($(element).data("id") in varianti[attributo_selezionato]) {
                $(element).removeClass("notAvailable");
                if (disattivato) {
                    $(element).addClass("attributeSelected");
                    disattivato = 0;
                }
            } else {
                $(element).addClass("notAvailable");
                if ($(element).hasClass("attributeSelected")) {
                    $(element).removeClass("attributeSelected");
                    disattivato = 1;
                }
            }
        });
        if (disattivato) {
            $(".box-varianti:eq(1) .attribute:not(.notAvailable)").first().addClass("attributeSelected");
        }
    }
}

function getDatiVariante($instance, callback) {

    var attributi = getAttributiSelezionati($instance);

    var data = { 'attributes': attributi };

    console.log(attributi);

    $.ajax({
        type: 'POST',
        url: '/prodotto/api/get-variant-data/' + getProdottoId($instance),
        data: data,
        dataType: 'json',
        success: function(ret_data) {
            if (ret_data.result) {
                callback(ret_data);
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });


}

function getAttributiSelezionati($instance) {

    var attributi = [];
    $instance.find('.attributeSelected').each(function() {
        attributi.push($(this).data('id'));
    });

    return attributi;

}

function getProdottoId($instance) {
    return $instance.find('.scheda-prodotto').data('id');
}

function getQtyInput($instance) {
    return $instance.find('.qty');
}


$('#' + instance + " .wishlist-add").click(function(e) {
    e.preventDefault();
    var q = $(this);
    var product_id = $(this).data("product");
    $.ajax({
        type: 'POST',
        url: '/account/wishlist/addRemove/' + product_id,
        dataType: 'json'
    }).done(function() {
        q.toggleClass("active");
    })
    .fail(function(r) {
        console.log(r);
        HandleBarHelper.modal({
            'id': 'noWishlist',
            'parentElement': '#' + instance,
            modalOptions: {
                backdrop: true,
                keyboard: false
            }
        });
        // alert("Per usare la Wishlist devi registrarti");
    });
});

function mettiInCarrello($instance) {

    var data = {
        attributi: getAttributiSelezionati($instance),
        prodotto: getProdottoId($instance),
        qty: getQtyInput($instance).val()
    };
    HandleBarHelper.lockScreen({ 'message': _('Sto aggiungendo il prodotto al carrello...') });
    $.ajax({
        type: 'POST',
        url: '/carrello/add',
        data: data,
        dataType: 'json',
        success: function(ret_data) {
            HandleBarHelper.unlockScreen();
            if (ret_data.result) {
                if (ret_data.data.notEnough == "1") {
                    HandleBarHelper.modal({
                        'id': 'notEnough',
                        'parentElement': '#' + instance,
                        "data": ret_data.data
                    });
                } else {
                    HandleBarHelper.modal({
                        'id': 'added',
                        'parentElement': '#' + instance,
                        "data": ret_data.data
                    });
                    var qtprec = parseInt($("#badgeCarrello").text());
                    var qt_new = qtprec + parseInt(ret_data.data.added);
                    $("#badgeCarrello").text(qt_new);
                }

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);

            }
        }
    });

}