// 08/11/17, 9.39
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>
$(function() {
    if ($('#' + instance).length) {
        var $instance = $('#' + instance);

        $(window).resize(function() {
            adattamentoGrazie();
        });

        $(window).resize();

    }

});

function adattamentoGrazie($instance) {
    if (window.matchMedia("(max-width:767px)").matches) {
        $(".mob2").attr("colspan", 2);
        $(".mob3").attr("colspan", "3");
    }
}
