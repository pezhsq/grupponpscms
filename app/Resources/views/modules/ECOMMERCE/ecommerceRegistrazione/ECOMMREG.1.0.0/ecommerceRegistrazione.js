$(function() {
    if ($('#' + instance).length) {
        var $instance = $('#' + instance);

        registerEvents($instance);

        $instance.find('#registrationForm_isGuest').val(0);
        $instance.find('#registrationForm_isGuest').trigger('change');
        if ($("input[name=\"registrationForm[isAzienda]\"]:checked").val() == '1') {
            $instance.find('#registrationForm_partitaIva').closest('.form-group').show();
            $instance.find('#registrationForm_ragioneSociale').closest('.form-group').show();

        } else {
            $instance.find('#registrationForm_partitaIva').closest('.form-group').hide();
            $instance.find('#registrationForm_ragioneSociale').closest('.form-group').hide();
        }
    }

});

function registerEvents($instance) {


    $instance.find('#registrationForm_isGuest').on('change', function() {
        if ($(this).val() == 1) {
            $instance.find('#registrationForm_plainPassword_first').closest('.form-group').hide();
            $instance.find('#registrationForm_plainPassword_second').closest('.form-group').hide();
        } else {
            $instance.find('#registrationForm_plainPassword_first').closest('.form-group').show();
            $instance.find('#registrationForm_plainPassword_second').closest('.form-group').show();
        }
    });

    $instance.find('input[name="registrationForm[isAzienda]"]').on('change', function() {
        if ($(this).val() == '1') {
            $instance.find('#registrationForm_partitaIva').closest('.form-group').show();
            $instance.find('#registrationForm_ragioneSociale').closest('.form-group').show();

        } else {
            $instance.find('#registrationForm_partitaIva').closest('.form-group').hide();
            $instance.find('#registrationForm_ragioneSociale').closest('.form-group').hide();
        }
    });

    var formGroups = {
        'selectNazione': $instance.find('#registrationForm_nazione').closest('.form-group'),
        'selectProvincia': $instance.find('#registrationForm_provincia').closest('.form-group'),
        'selectComune': $instance.find('#registrationForm_comune').closest('.form-group'),
        'textProvincia': $instance.find('#registrationForm_provincia_text').closest('.form-group'),
        'textComune': $instance.find('#registrationForm_comune_text').closest('.form-group')
    };

    formGroups.selectNazione.find('select').on('change', function(e) {
        changeNazione($(this).val(), formGroups);
    });

    formGroups.selectProvincia.find('select').on('change', function(e) {
        changeProvincia($(this).val(), formGroups);
    });

    if (formGroups.selectNazione.find('select').val() == 'IT') {
        redrawGeoSelect('select', formGroups);
        formGroups.selectProvincia.find('select').trigger('change');
    } else {
        redrawGeoSelect('text', formGroups);
    }

    $instance.find('.btn-go-indirizzi').on('click', function(e) {
        e.preventDefault();
        $('#datiAnagrafici').removeClass('in');
        $('#indirizzi').addClass('in');
        var $form = $(this).closest('#indirizzi').find('[name="scelta_indirizzo_form"]');
        console.log($form);
        $form.submit();
    });

    $instance.find('.btn-go-anagrafici').on('click', function(e) {
        e.preventDefault();
        $('#indirizzi').removeClass('in');
        $('#datiAnagrafici').addClass('in');

    });

    $instance.find("form[name='registrationForm']").submit(function(e) {
        var campiObbligatori = ['registrationForm_nome', 'registrationForm_cognome', 'registrationForm_indirizzo', 'registrationForm_cap'];
        if ($('#registrationForm_nazione').val() == 'IT') {
            campiObbligatori.push('registrationForm_codiceFiscale');
        }
        campiObbligatori.push('registrationForm_email_first');
        campiObbligatori.push('registrationForm_email_second');
        campiObbligatori.push('registrationForm_plainPassword_first');
        campiObbligatori.push('registrationForm_plainPassword_second');
        var errori = [];
        for(var i = 0; i < campiObbligatori.length; i++) {
            var $field = $instance.find('#' + campiObbligatori[i]);

            if ($.trim($field.val()) == '') {
                errori.push($.trim($field.closest('.form-group').find('label').text().replace('*', '')));
            }
        }


        if (!$instance.find(".accettazione-privacy").is(":checked")) {
            errori.push(_('Informativa Privacy'));
        }

        if (errori.length) {
            console.log(errori);
            var errore = 'I seguenti campi sono obbligatori e non sono stati compilati:' + "<br /><br />";
            errore += errori.join("<br />");
            var settings = {
                'type': 'danger',
                'titolo': _('Errori nella compilazione del form'),
                'content': errore,
                'OK': 'Ok',
                'callback': null
            };

            HandleBarHelper.alert(settings);
            return false;

        }

        return true;

    });

}


function changeNazione(selected, formGroupsDaRivedere) {

    for(var i in formGroupsDaRivedere) {
        formGroupsDaRivedere[i].removeClass('has-error');
        formGroupsDaRivedere[i].find('.error-block').closest('div').remove();
    }

    var data = { 'nazione': selected };

    $.ajax({
        type: 'POST',
        url: '/admin/provincia/options',
        data: data,
        dataType: 'json',
        success: function(ret_data) {
            if (ret_data.result) {

                // se ho ricevuto dati popolo la select, altrimenti mostro i campi testuali
                if (ret_data.options.length) {
                    var compiled = HandleBarHelper.compile('options');
                    var markup = compiled(ret_data);
                    formGroupsDaRivedere.selectProvincia.find('select').html(markup);
                    var compiled = HandleBarHelper.compile('emptyComune');
                    formGroupsDaRivedere.selectComune.find('select').html(compiled());
                    redrawGeoSelect('select', formGroupsDaRivedere);
                    formGroupsDaRivedere.selectProvincia.find('select').trigger('change');
                } else {
                    redrawGeoSelect('text', formGroupsDaRivedere);
                }
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function changeProvincia(selected, formGroupsDaRivedere) {
    var data = { 'provincia': selected };
    $.ajax({
        type: 'POST',
        url: '/admin/comuni/options',
        data: data,
        dataType: 'json',
        success: function(ret_data) {
            if (ret_data.result) {

                if (ret_data.options.length) {
                    var compiled = HandleBarHelper.compile('options');
                    markup = compiled(ret_data);
                    formGroupsDaRivedere.selectComune.find('select').html(markup);
                    redrawGeoSelect('select', formGroupsDaRivedere);
                } else {
                    redrawGeoSelect('text', formGroupsDaRivedere);
                }

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}


function redrawGeoSelect(type, formGroups) {

    if (typeof type == 'undefined') {
        type = 'text';
    }

    if (type == 'text') {

        // nascondo le select
        formGroups.selectProvincia.hide();
        formGroups.selectComune.hide();

        // imposto i select con un unico valore a 0
        formGroups.selectProvincia.find('select').html('<option value="0"></option>');
        formGroups.selectComune.find('select').html('<option value="0"></option>');

        // mostro le versioni testuali
        formGroups.textProvincia.show();
        formGroups.textComune.show();

    } else
        if (type == 'select') {

            // mostro le versioni a tendina
            formGroups.selectProvincia.show();
            formGroups.selectComune.show();


            // nascondo le versioni testuali
            formGroups.textProvincia.hide();
            formGroups.textComune.hide();

            // imposto i campi testuali a valore vuoto
            formGroups.textProvincia.find('input').val('');
            formGroups.textComune.find('input').val('');

        }

}


function redrawGeoSelect(type, formGroups) {

    if (typeof type == 'undefined') {
        type = 'text';
    }

    if (type == 'text') {

        // nascondo le select
        formGroups.selectProvincia.hide();
        formGroups.selectComune.hide();

        // imposto i select con un unico valore a 0
        formGroups.selectProvincia.find('select').html('<option value="0"></option>');
        formGroups.selectComune.find('select').html('<option value="0"></option>');

        // mostro le versioni testuali
        formGroups.textProvincia.show();
        formGroups.textComune.show();

    } else
        if (type == 'select') {

            // mostro le versioni a tendina
            formGroups.selectProvincia.show();
            formGroups.selectComune.show();

            // nascondo le versioni testuali
            formGroups.textProvincia.hide();
            formGroups.textComune.hide();

            // imposto i campi testuali a valore vuoto
            formGroups.textProvincia.find('input').val('');
            formGroups.textComune.find('input').val('');

        }

}

function changeNazione(selected, formGroupsDaRivedere) {

    for(var i in formGroupsDaRivedere) {
        formGroupsDaRivedere[i].removeClass('has-error');
        formGroupsDaRivedere[i].find('.error-block').closest('div').remove();
    }

    var data = { 'nazione': selected };

    $.ajax({
        type: 'POST',
        url: '/admin/provincia/options',
        data: data,
        dataType: 'json',
        success: function(ret_data) {
            if (ret_data.result) {

                // se ho ricevuto dati popolo la select, altrimenti mostro i campi testuali
                if (ret_data.options.length) {
                    var compiled = HandleBarHelper.compile('options');
                    var markup = compiled(ret_data);
                    formGroupsDaRivedere.selectProvincia.find('select').html(markup);
                    var compiled = HandleBarHelper.compile('emptyComune');
                    formGroupsDaRivedere.selectComune.find('select').html(compiled());
                    redrawGeoSelect('select', formGroupsDaRivedere);
                    formGroupsDaRivedere.selectProvincia.find('select').trigger('change');
                } else {
                    redrawGeoSelect('text', formGroupsDaRivedere);
                }
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}
