$(function () {

    if ($('#' + instance).length) {
        $("#" + instance + " .valore").rating(
            {
                "showCaption": false,
                "showClear": false,
                "theme": "krajee-fa"
            }
        );
        $("#" + instance + " form[name='recensione_form']").submit(function (e) {
            if (parseInt($("#" + instance + " .valore").val()) > 0)
                return true;
            alert("devi selezionare un valore");
            return false;
        });
    }
})
;