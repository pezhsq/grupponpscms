var modulo = 'anagrafica';
var formGroups;

$(function () {

    var formGroups = {
        'selectNazione': $('#form_nazione').closest('.form-group'),
        'selectProvincia': $('#form_provincia').closest('.form-group'),
        'selectComune': $('#form_comune').closest('.form-group'),
        'textProvincia': $('#form_provincia_text').closest('.form-group'),
        'textComune': $('#form_comune_text').closest('.form-group'),
    };

    // change del select se è un'azienda o meno
    $('input[name="form[isAzienda]"]').on('change', function (e) {
        $blocks = [$('#form_partitaIva').closest('.form-group'), $('#form_ragioneSociale').closest('.form-group')];
        if ($(this).val() == '0') {
            action = 'hide';
        } else {
            action = 'show';
        }
        for (var i = 0; i < $blocks.length; i++) {
            if (action == 'hide') {
                $blocks[i].hide();
            } else {
                $blocks[i].show();
            }
        }
    });

    // se la tab è disabilitata non fa nulla.
    $(".nav-tabs a[data-toggle=tab]").on("click", function (e) {
        if ($(this).hasClass("disabled")) {
            e.preventDefault();
            return false;
        }
    });

    // cambio del select della nazione, con reperimento delle eventuali province e aggiornamento delle select
    formGroups.selectNazione.find('select').on('change', function (e) {
        changeNazione($(this).val(), formGroups);
    });

    formGroups.selectProvincia.find('select').on('change', function (e) {
        changeProvincia($(this).val(), formGroups);
    });

    $blocks = [$('#form_partitaIva').closest('.form-group'), $('#form_ragioneSociale').closest('.form-group')];
    if ($('input[name="form[isAzienda]"]').val() == '0') {
        action = 'hide';
    } else {
        action = 'show';
    }
    for (var i = 0; i < $blocks.length; i++) {
        if (action == 'hide') {
            $blocks[i].hide();
        } else {
            $blocks[i].show();
        }
    }

    if (formGroups.selectNazione.find('select').val() == 'IT') {
        redrawGeoSelect('select', formGroups);
    } else {
        redrawGeoSelect('text', formGroups);
    }

    // Click sul bottone di aggiunta di un nuovo indirizzo
    $(document).on('click', '.addIndirizzo', function (e) {
        e.preventDefault();
        HandleBarHelper.lockScreen({'message': _('Attendere prego...')})
        getAddressForm(0, function (markup) {
            showAddressForm(markup, _('Inserisci un nuovo indirizzo'));
        });
    });

    // modifica dell'indirizzo
    $(document).on('click', '.edit-address', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        HandleBarHelper.lockScreen({'message': _('Attendere prego...')})
        getAddressForm(id, function (markup) {
            showAddressForm(markup, _('Modifica indirizzo'));
        });
    });

    // modifica dell'indirizzo
    $(document).on('click', '.set-default-address:not(.disabled)', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        HandleBarHelper.lockScreen(_('Attendere prego'));
        $.ajax({
            type: 'POST',
            url: '/account/' + modulo + '/address/set-default/' + id,
            dataType: 'json',
            success: function (ret_data) {
                if (ret_data.result) {
                    loadIndirizzi();
                } else {
                    var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': ret_data.errors.join('<br />'),
                        'OK': 'Ok',
                        'callback': null
                    };
                    HandleBarHelper.alert(opts);
                }
            }
        });

    });

    // click cancellazione indirizzo
    $(document).on('click', '.delete-address:not(.disabled)', function (e) {
        e.preventDefault();

        id = $(this).data('id')

        var opts = {
            'type': 'danger',
            'titolo': _('Eliminazione indirizzo', 'anagrafica'),
            'content': _('Vuoi eliminare l\'indirizzo: <br /><br />%s<br /><strong>ATTENZIONE: L\'operazione sarà irreversibile</strong>', 'anagrafica', $(this).closest('.pannelloIndirizzo').find('.x_content').html()),
            'OK': _('Elmina'),
            'CANCEL': 'Annulla',
            'onOK': function ($Modal) {
                $.ajax({
                    type: 'POST',
                    url: '/account/' + modulo + '/address/delete/' + id,
                    dataType: 'json',
                    success: function (ret_data) {
                        if (ret_data.result) {
                            $Modal.modal('hide');
                            HandleBarHelper.lockScreen(_('Attendere prego'));
                            loadIndirizzi();
                        } else {
                            var opts = {
                                'type': 'danger',
                                'titolo': _('Attenzione'),
                                'content': ret_data.errors.join('<br />'),
                                'OK': 'Ok',
                                'callback': null
                            };
                            HandleBarHelper.alert(opts);
                        }
                    }
                });

            },
            'onCANCEL': null
        };
        HandleBarHelper.confirm(opts);
    });

    /** Datatable della pagina di elenco */
    if ($('#datatable').length) {

        var cols = [];

        var col = {
            'title': _('Modifica', 'anagrafica'),
            'className': 'dt0 dt-body-center',
            'searchable': false
        };
        cols.push(col);

        col = {
            'title': _('Nominativo', 'anagrafica'),
            'className': 'dt1',
            searchable: true,
            data: 'nominativo'
        };

        cols.push(col);

        col = {
            'title': _('Codice Fiscale', 'anagrafica'),
            'className': 'dt2',
            searchable: true,
            data: 'codiceFiscale'
        };

        cols.push(col);

        col = {
            'title': _('Email', 'anagrafica'),
            'className': 'dt3',
            searchable: true,
            data: 'email'
        };

        cols.push(col);

        col = {
            'title': _('Partita IVA', 'anagrafica'),
            'className': 'dt3',
            searchable: true,
            data: 'partitaIva'
        };

        cols.push(col);

        col = {
            'title': _('Data Registrazione'),
            'className': 'dt4',
            searchable: true,
            data: 'registeredAt'
        };

        cols.push(col);

        col = {
            'title': 'Ultima modifica',
            'className': 'dt5',
            searchable: true,
            data: 'updatedAt'
        };

        cols.push(col);

        col = {'className': 'dt6 dt-body-center'};

        // placeholder cancellazione
        cols.push(col);

        var columnDefs = [];

        var columnDef = {
            targets: 0,
            searchable: false,
            orderable: false,
            className: 'dt-body-center',
            render: function (data, type, full, meta) {
                var toString = full.nominativo;
                return '<a href="/account/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
            }
        };

        columnDefs.push(columnDef);


        columnDef = {
            targets: cols.length - 1,
            searchable: false,
            className: 'dt-body-center',
            orderable: false,
            render: function (data, type, full, meta) {
                var toString = full.nominativo;
                if (full.deleted) {
                    var title = _('Recupera:');
                    var ret = '<a href="/account/' + modulo + '/restore/' + full.id + '" class="btn btn-success btn-xs btn-restore';
                    ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-repeat"></i></a>';
                    var title = _('Elimina definitivamente: ');
                    ret += '<a href="/account/' + modulo + '/delete/' + full.id + '/1" class="btn btn-danger btn-xs btn-delete';
                    if (parseInt(full.cancellabile, 10) == 0) {
                        ret += ' disabled';
                    }
                    ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                } else {
                    var title = 'Elimina: ';
                    var ret = '<a href="/account/' + modulo + '/delete/' + full.id + '" class="btn btn-danger btn-xs btn-delete';
                    if (parseInt(full.cancellabile, 10) == 0) {
                        ret += ' disabled';
                    }
                    ret += '" title="' + title + toString + '" data-title="' + toString + '"><i class="fa fa-trash"></i></a>';
                }
                return ret;
            }
        };

        columnDefs.push(columnDef);


        $('#datatable').dataTable({
            ajax: {
                "url": "/account/" + modulo + "/json"
            },
            aaSorting: [[2, 'asc']],
            stateSave: true,
            iDisplayLength: 15,
            responsive: true,
            columns: cols,
            columnDefs: columnDefs,
            createdRow: function (row, data, index) {
                if (data.deleted) {
                    $(row).addClass("danger");
                }
            }
        });

    }

});

/** FINE document.ready **/

function redrawGeoSelect(type, formGroups) {

    if (typeof type == 'undefined') {
        type = 'text';
    }

    if (type == 'text') {

        // nascondo le select
        formGroups.selectProvincia.hide();
        formGroups.selectComune.hide();

        // imposto i select con un unico valore a 0
        formGroups.selectProvincia.find('select').html('<option value="0"></option>');
        formGroups.selectComune.find('select').html('<option value="0"></option>');

        // mostro le versioni testuali
        formGroups.textProvincia.show();
        formGroups.textComune.show();

    } else if (type == 'select') {

        // mostro le versioni a tendina
        formGroups.selectProvincia.show();
        formGroups.selectComune.show();

        // nascondo le versioni testuali
        formGroups.textProvincia.hide();
        formGroups.textComune.hide();

        // imposto i campi testuali a valore vuoto
        formGroups.textProvincia.find('input').val('');
        formGroups.textComune.find('input').val('');

    }

}

function changeNazione(selected, formGroupsDaRivedere) {

    for (var i in formGroupsDaRivedere) {
        formGroupsDaRivedere[i].removeClass('has-error');
        formGroupsDaRivedere[i].find('.error-block').closest('div').remove();
    }

    var data = {'nazione': selected};

    $.ajax({
        type: 'POST',
        url: '/admin/provincia/options',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {

                // se ho ricevuto dati popolo la select, altrimenti mostro i campi testuali
                if (ret_data.options.length) {
                    var compiled = HandleBarHelper.compile('options');
                    var markup = compiled(ret_data);
                    formGroupsDaRivedere.selectProvincia.find('select').html(markup);
                    var compiled = HandleBarHelper.compile('emptyComune');
                    formGroupsDaRivedere.selectComune.find('select').html(compiled());
                    redrawGeoSelect('select', formGroupsDaRivedere);
                    formGroupsDaRivedere.selectProvincia.find('select').trigger('change');
                } else {
                    redrawGeoSelect('text', formGroupsDaRivedere);
                }
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function changeProvincia(selected, formGroupsDaRivedere) {
    var data = {'provincia': selected};
    $.ajax({
        type: 'POST',
        url: '/admin/comuni/options',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {

                if (ret_data.options.length) {
                    var compiled = HandleBarHelper.compile('options');
                    markup = compiled(ret_data);
                    formGroupsDaRivedere.selectComune.find('select').html(markup);
                    redrawGeoSelect('select', formGroupsDaRivedere);
                } else {
                    redrawGeoSelect('text', formGroupsDaRivedere);
                }

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}


function getAddressForm(id, callback) {

    var data = {
        'id': id,
        'anagrafica_id': $('#tab_address').data('anagrafica')
    };

    $.ajax({
        type: 'POST',
        url: '/account/' + modulo + '/address/get-form',
        dataType: 'json',
        data: data,
        success: function (ret_data) {
            if (ret_data.result) {

                callback(ret_data.markup);

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function showAddressForm(markup, title) {

    HandleBarHelper.unlockScreen();

    HandleBarHelper.modalForm({
        'markup': markup,
        'title': title,
        'onOk': function ($Modal) {
            $Modal.find('form').ajaxSubmit({
                'dataType': 'json',
                'success': function (ret_data, status, xhr, $form) {
                    $Modal.modal('hide');
                    HandleBarHelper.lockScreen(_('Attendere prego'));
                    loadIndirizzi();
                }
            });
        },
        'okLbl': _('Salva'),
        'onShow': function ($Modal) {

            var formGroupsIndirizzi = {
                'selectNazione': $Modal.find('#indirizzo_anagrafica_form_nazione').closest('.form-group'),
                'selectProvincia': $Modal.find('#indirizzo_anagrafica_form_provincia').closest('.form-group'),
                'selectComune': $Modal.find('#indirizzo_anagrafica_form_comune').closest('.form-group'),
                'textProvincia': $Modal.find('#indirizzo_anagrafica_form_provincia_text').closest('.form-group'),
                'textComune': $Modal.find('#indirizzo_anagrafica_form_comune_text').closest('.form-group')
            };

            formGroupsIndirizzi.selectNazione.find('select').on('change', function (e) {
                changeNazione($(this).val(), formGroupsIndirizzi);
            });

            formGroupsIndirizzi.selectProvincia.find('select').on('change', function (e) {
                changeProvincia($(this).val(), formGroupsIndirizzi);
            });

            if ($Modal.find('form').data('id') != 0) {
                redrawGeoSelect('select', formGroupsIndirizzi);
            } else {
                formGroupsIndirizzi.selectNazione.find('select').trigger('change');
            }

        }
    });

}

function loadIndirizzi() {
    $.ajax({
        type: 'POST',
        url: '/account/' + modulo + '/addresses/' + $('#tab_address').data('anagrafica'),
        dataType: 'json',
        success: function (ret_data) {
            HandleBarHelper.unlockScreen();
            if (ret_data.result) {
                $('#tab_address').html(ret_data.markup);
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });
}