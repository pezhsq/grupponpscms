$(document).ready(function () {
    if ($("#" + instance).length) {
        $(".openSearchMenuMobile").click(function (e) {
            e.preventDefault();
            $("#menuMobileFull").collapse('hide');
            $(".pannello-mobile:not('.pannello-search-mobile')").removeClass("opened");
            $("#" + instance + " .pannello-search-mobile").toggleClass("opened");
        });
        $(".search-box-btn").click(function () {
            if ($(".search-box-mobile").val().trim() != "")
                window.location.href = window.location.href + "ricerca-prodotti?term=" + $(".search-box-mobile").val().trim();
        });
    }
});