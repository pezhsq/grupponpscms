if ($("#" + instance).length) {
    $(function () {

        $("body").click(function (e) {
            if (!$(e.target).closest(".pannello-ricerca,.icon-search,.search-input").length) {
                $("body").removeClass("search-open");
            }
        });

        $("#" + instance + " ").click(function () {
            $("body").addClass("search-open");

        });
        $("#" + instance + " .search-input").keyup(function (e) {
            e.preventDefault();
            console.log(e.which);
            if (e.which == 13) {
                window.location.href = "/ricerca-prodotti?term=" + $(this).val();
            }
        });
        $("#" + instance + " .search-box-btn").click(function () {
            window.location.href = "/ricerca-prodotti?term=" + $("#" + instance + " .input-search").val();
        });
    });
}
