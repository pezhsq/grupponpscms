var set_qty = 0;
var set_qty_timeout = 0;
$(function() {

        if ($('#' + instance).length) {

            $(window).resize(function() {
                adattamentoCarrello();
            });

            $(window).resize();
            var $instance = $('#' + instance);
            var $btnApplyCoupon = $('#' + instance).find('.btn-coupon');
            var $btnRemoveCoupon = $('#' + instance).find('.btn-coupon-remove');
            var $coupounInput = $('#' + instance).find('.coupon-input');

            $coupounInput.on('keyup', function(event) {
                if (event.keyCode === 13) {
                    $btnApplyCoupon.click();
                }
            });

            $btnRemoveCoupon.on('click', function(e) {
                e.preventDefault();
                HandleBarHelper.lockScreen({ 'message': _('Rimozione coupon sconto...') });
                $.ajax({
                    type: 'POST',
                    url: '/carrello/remove-coupon',
                    dataType: 'json',
                    success: function(ret_data) {
                        if (ret_data.result) {
                            window.location.reload();
                        } else {
                            HandleBarHelper.unlockScreen();
                            var opts = {
                                'type': 'danger',
                                'titolo': _('Attenzione'),
                                'content': ret_data.errors.join('<br />'),
                                'OK': 'Ok',
                                'callback': null
                            };
                            HandleBarHelper.alert(opts);
                        }
                    }
                });

            });

            $btnApplyCoupon.on('click', function(e) {
                e.preventDefault();
                if ($.trim($coupounInput.val()) == '') {
                    var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': _('Il codice coupon non può essere vuoto'),
                        'OK': 'Ok',
                        'callback': null
                    };
                    HandleBarHelper.alert(opts);
                } else {
                    HandleBarHelper.lockScreen({ 'message': _('Applico lo sconto...') });
                    var data = { 'coupon': $.trim($coupounInput.val()) };
                    $.ajax({
                        type: 'POST',
                        url: '/carrello/add-coupon',
                        data: data,
                        dataType: 'json',
                        success: function(ret_data) {
                            if (ret_data.result) {
                                window.location.reload();
                            } else {
                                HandleBarHelper.unlockScreen();
                                var opts = {
                                    'type': 'danger',
                                    'titolo': _('Attenzione'),
                                    'content': ret_data.errors.join('<br />'),
                                    'OK': 'Ok',
                                    'callback': null
                                };
                                HandleBarHelper.alert(opts);
                            }
                        }
                    });


                }
            });

            // initEventListeners($instance);

            $instance.find(".btn-svuota").click(function(e) {
                e.preventDefault();
                HandleBarHelper.confirm({
                    titolo: "Conferma Svuotamento carrello",
                    content: "Sei sicuro di voler svuotare il carrello? ",
                    onOK: function() {
                        HandleBarHelper.lockScreen();
                        $.ajax({
                            type: 'POST',
                            url: '/carrello/svuota',
                            dataType: 'json',
                            success: function(ret_data) {
                                if (ret_data.success == "1") {
                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });

            $instance.find('.tabella-carrello').on('click', ".addone", function(e) {
                e.preventDefault();
                var $Row = $(this).closest('tr');
                var $input = $Row.find('.qty input');
                var quantitaAttuale = parseInt($input.val(), 10);
                var newQty = quantitaAttuale++;
                $input.val(quantitaAttuale);
                setQty($Row.data('id'), $input.val(), $instance);
            });

            $instance.find('.tabella-carrello').on('click', ".minusone", function(e) {
                e.preventDefault();
                var $Row = $(this).closest('tr');
                var $input = $Row.find('.qty input');
                var quantitaAttuale = parseInt($input.val(), 10);
                var newQty = quantitaAttuale - 1;
                quantitaAttuale--;
                $input.val(quantitaAttuale);
                setQty($Row.data('id'), $input.val(), $instance);

                if (newQty <= 0) {
                    $Row.remove();
                    checkVuoto();
                }
            });

            $instance.find('.tabella-carrello').on('keyup', '.qty input', function(e) {
                if (e.keyCode != '13' && e.keyCode != '27' && e.keyCode != '32', e.keyCode != '8') {
                    var $Row = $(this).closest('tr');
                    setQty($Row.data('id'), $(this).val(), $instance);
                }
            });

            $instance.find('.tabella-carrello').on('click', ".remove-btn", function(e) {
                e.preventDefault();
                var $Row = $(this).closest('tr');
                console.log($Row);

                setQty($Row.data('id'), 0, $instance);
                $Row.remove();
                checkVuoto();
            });
        }
    }
);

var adattato = 0;

function checkVuoto() {
    if ($(".riga-prodotto").length == 0) {
        console.log("VUOTO!");
        $('<div class="alert alert-danger">\n' +
            '                    <i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;\n' +
            '                    Il carrello è vuoto\n' +
            '                </div>').insertBefore(".tabella-carrello");
        $(".tabella-carrello,.pulsante-form").remove();


    }
}

function adattamentoCarrello() {
    if (window.matchMedia("(max-width:767px)").matches) {
        if (!adattato) {
            $(".tab-dati-prodotto").attr("colspan", 5);
            $(".labelfoot").attr("colspan", "4");
            $(".valfoot").attr("colspan", "3");
            adattato = 1;
        }


    } else
        if (adattato) {
            $(".tab-dati-prodotto").attr("colspan", 1);
            $(".labelfoot").attr("colspan", "2");
            $(".valfoot").attr("colspan", "2");
            adattato = 0;
        }
}


function setQty(id, quantity, $instance) {
    if (set_qty_timeout != 0)
        clearTimeout(set_qty_timeout);
    set_qty_timeout = setTimeout(function() {
        var data = {
            'recordCarrelloId': id,
            'qty': quantity
        };
        if (set_qty != 0) {
            set_qty.abort();
        }
        set_qty = $.ajax({
            type: 'POST',
            url: '/carrello/set-qty',
            data: data,
            dataType: 'json',
            success: function(ret_data) {
                if (ret_data.result) {
                    if (ret_data.data.notEnough == "1") {
                        console.log(ret_data.data);
                        HandleBarHelper.modal({
                            'id': 'notEnough',
                            'parentElement': '#' + instance,
                            "data": ret_data.data
                        });
                    }
                    refreshCart($instance);
                } else {
                    var opts = {
                        'type': 'danger',
                        'titolo': _('Attenzione'),
                        'content': ret_data.errors.join('<br />'),
                        'OK': 'Ok',
                        'callback': null
                    };
                    HandleBarHelper.alert(opts);
                }
            }
        });
    }, 500);


}

function refreshCart($instance) {

    $.ajax({
        type: 'POST',
        url: '/carrello/json',
        dataType: 'json',
        success: function(ret_data) {

            if (ret_data.result) {
                $(".imponibile-val").text(ret_data.imponibile);
                $(".totale-val").html("<strong>" + ret_data.totale + "</strong>");
                console.log(ret_data);
                $.each(ret_data.recordsCarrello, function(index, element) {
                    $(".riga-prodotto").eq(index).find(".subtotale").text(element.subtotale);
                    $(".riga-prodotto").eq(index).find(".quantita-prodotto").val(element.qty);
                });
                $.each(ret_data.tasse, function(index, element) {
                    $("#tax_" + index).find(".tax-total").text(element.imposta);
                });
                return false;
                var compiled = HandleBarHelper.compile('//bundles/webtekecommerce/public/jstpl/carrello.jstpl', function(compiled) {
                    $table = $('.tabella-carrello');

                    $table.find('tbody').remove();
                    $table.find('tfoot').remove();

                    $(compiled(ret_data)).appendTo($table);

                    // initEventListeners($instance);
                    adattato = 0;
                    adattamentoCarrello();
                });
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}



