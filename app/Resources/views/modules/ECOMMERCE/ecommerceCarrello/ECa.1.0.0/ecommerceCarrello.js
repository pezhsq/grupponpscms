$(function () {
    if ($('#' + instance).length) {

        $(window).resize(function () {
            adattamentoCarrello();
        });

        $(window).resize();
        var $instance = $('#' + instance);

        // initEventListeners($instance);

        $instance.find('.tabella-carrello').on('click', ".addone", function (e) {
            e.preventDefault();
            var $Row = $(this).closest('tr');
            var $input = $Row.find('.qty input');
            var quantitaAttuale = parseInt($input.val(), 10);
            var newQty = quantitaAttuale++;
            $input.val(quantitaAttuale);
            setQty($Row.data('id'), $input.val(), $instance);
        });

        $instance.find('.tabella-carrello').on('click', ".minusone", function (e) {
            e.preventDefault();
            var $Row = $(this).closest('tr');
            var $input = $Row.find('.qty input');
            var quantitaAttuale = parseInt($input.val(), 10);
            var newQty = quantitaAttuale--;
            $input.val(quantitaAttuale);
            setQty($Row.data('id'), $input.val(), $instance);
            if (newQty <= 0) {
                $Row.remove();
            }
        });

        $instance.find('.tabella-carrello').on('keyup', '.qty input', function () {
            var $Row = $(this).closest('tr');
            setQty($Row.data('id'), $(this).val(), $instance);
        });

        $instance.find('.tabella-carrello').on('click', ".remove-btn", function (e) {
            e.preventDefault();
            var $Row = $(this).closest('tr');
            setQty($Row.data('id'), 0, $instance);
        });
    }
});

var adattato = 0;

function adattamentoCarrello() {
    if (window.matchMedia("(max-width:767px)").matches) {
        if (!adattato) {
            console.log("YRAH");
            $(".tab-dati-prodotto").attr("colspan", 5);
            $(".labelfoot").attr("colspan", "4");
            $(".valfoot").attr("colspan", "3");
            adattato = 1;
        }


    } else if (adattato) {
        $(".tab-dati-prodotto").attr("colspan", 1);
        adattato = 0;
    }
}


function setQty(id, quantity, $instance) {

    var data = {
        'recordCarrelloId': id,
        'qty': quantity
    };

    $.ajax({
        type: 'POST',
        url: '/carrello/set-qty',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {
                if (ret_data.data.notEnough == "1") {
                    HandleBarHelper.modal({
                        'id': 'notEnough',
                        'parentElement': '#' + instance,
                        "data": ret_data.data
                    });
                }
                refreshCart($instance);
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function refreshCart($instance) {

    $.ajax({
        type: 'POST',
        url: '/carrello/json',
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {
                var compiled = HandleBarHelper.compile('//bundles/webtekecommerce/public/jstpl/carrello.jstpl', function (compiled) {
                    $table = $('.tabella-carrello');

                    $table.find('tbody').remove();
                    $table.find('tfoot').remove();

                    $(compiled(ret_data)).appendTo($table);

                    // initEventListeners($instance);
                    adattato = 0;
                    adattamentoCarrello();
                });
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}



