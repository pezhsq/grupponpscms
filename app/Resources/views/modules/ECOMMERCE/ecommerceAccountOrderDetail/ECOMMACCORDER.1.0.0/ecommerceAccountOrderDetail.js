$(function () {
    if ($("#" + instance).length) {
        $(window).resize(function () {
            if (window.matchMedia("(max-width:600px)").matches) {
                console.log("TAG");
                $(".subtotale-head,.subtotale").attr("colspan", 3);
            } else {
                $(".subtotale-head,.subtotale").attr("colspan", 1);
            }
        });
        $(window).resize();
    }
});