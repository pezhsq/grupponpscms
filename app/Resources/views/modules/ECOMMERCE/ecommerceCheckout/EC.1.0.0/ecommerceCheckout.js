$(function () {
    if ($('#' + instance).length) {
        var $instance = $('#' + instance);

        registerEvents($instance);

    }

    $(".pannello").matchHeight();

    $("#" + instance + " .openUserMenuCheckout").click(function (e) {
        e.preventDefault();
        if (window.matchMedia("(min-width:767px)").matches) {
            $("html,body").animate({scrollTop: 0 + "px"}, 500);
        } else {
            $("body").addClass("no-overflow");
        }
        $(".pannello-login").addClass("open");
        $(".pannello-login-mobile").addClass("opened");
    });

});
var confirmed = 0;

function registerEvents($instance) {

    $instance.find("form[name='scelta_pagamento_form']").submit(function (e) {
        if (!confirmed) {
            HandleBarHelper.confirm({
                titolo: "Conferma Ordine",
                content: "Sei sicuro di voler procedere con l'ordine? ",
                onOK: function () {
                    console.log("OK");
                    confirmed = 1;
                    $instance.find("form[name='scelta_pagamento_form']").submit();
                    HandleBarHelper.lockScreen();
                }
            });
            return false;
        }
        return true;

    });

    $instance.find("form[name='registrationForm']").submit(function (e) {
        console.log("submit");
        if ($instance.find(".accettazione-privacy").is(":checked")) {
            return true;
        }
        HandleBarHelper.modal({
            'id': 'noPrivacy',
            'parentElement': '#' + instance,
            modalOptions: {
                backdrop: true,
                keyboard: false
            }
        });
        return false;
    });

    $instance.find('.type-selector').on('click', function (e) {
        e.preventDefault();
        var target = $(this).attr('href').replace('#', '');
        $instance.find('.type-panel').removeClass('type-panel-active');
        $instance.find('.' + target).addClass('type-panel-active');
        $instance.find(".pannello-iniziale").hide();

        var type = $(this).data('type');

        switch (type) {
            case 'registrazione':
                $instance.find('#registrationForm_isGuest').val(0);
                $instance.find('#registrationForm_isGuest').trigger('change');
                // $instance.find('input[name="registrationForm[isAzienda]"]').trigger('change');
                break;
            case 'ospite':
                $instance.find('#registrationForm_isGuest').val(1);
                $instance.find('#registrationForm_isGuest').trigger('change');
                $instance.find('input[name="registrationForm[isAzienda]"]').trigger('change');
                break;
            case 'login':
                break;
        }

    });

    $instance.find('#registrationForm_isGuest').on('change', function () {
        if ($(this).val() == 1) {
            $instance.find('#registrationForm_plainPassword_first').closest('.form-group').hide();
            $instance.find('#registrationForm_plainPassword_second').closest('.form-group').hide();
        } else {
            $instance.find('#registrationForm_plainPassword_first').closest('.form-group').show();
            $instance.find('#registrationForm_plainPassword_second').closest('.form-group').show();
        }
    });

    $instance.find('input[name="registrationForm[isAzienda]"]').on('change', function () {
        if ($(this).val() == '1') {
            $instance.find('#registrationForm_partitaIva').closest('.form-group').show();
            $instance.find('#registrationForm_ragioneSociale').closest('.form-group').show();

        } else {
            $instance.find('#registrationForm_partitaIva').closest('.form-group').hide();
            $instance.find('#registrationForm_ragioneSociale').closest('.form-group').hide();
        }
    });

    var formGroups = {
        'selectNazione': $instance.find('#registrationForm_nazione').closest('.form-group'),
        'selectProvincia': $instance.find('#registrationForm_provincia').closest('.form-group'),
        'selectComune': $instance.find('#registrationForm_comune').closest('.form-group'),
        'textProvincia': $instance.find('#registrationForm_provincia_text').closest('.form-group'),
        'textComune': $instance.find('#registrationForm_comune_text').closest('.form-group')
    };

    formGroups.selectNazione.find('select').on('change', function (e) {
        changeNazione($(this).val(), formGroups);
    });

    formGroups.selectProvincia.find('select').on('change', function (e) {
        changeProvincia($(this).val(), formGroups);
    });

    if (formGroups.selectNazione.find('select').val() == 'IT') {
        redrawGeoSelect('select', formGroups);
        formGroups.selectProvincia.find('select').trigger('change');
    } else {
        redrawGeoSelect('text', formGroups);
    }

    $instance.find('.btn-go-indirizzi').on('click', function (e) {
        e.preventDefault();
        $('#datiAnagrafici').removeClass('in');
        $('#indirizzi').addClass('in');
        var $form = $(this).closest('#indirizzi').find('[name="scelta_indirizzo_form"]');
        console.log($form);
        $form.submit();
    });

    $instance.find('.btn-go-anagrafici').on('click', function (e) {
        e.preventDefault();
        $('#indirizzi').removeClass('in');
        $('#datiAnagrafici').addClass('in');

    });

    $instance.find('.addAddress').on('click', function (e) {
        e.preventDefault();
        var $container = $(this).closest('.indirizzoAdd');
        $form = $container.find('.newAddressForm');

        var settings = {
            id: false,
            size: '',
            markup: $form.html(),
            title: _('Inserisci un nuovo indirizzo'),
            data: {},
            onShow: function ($Modal) {
                var formGroupsIndirizzi = {
                    'selectNazione': $Modal.find('#indirizzo_anagrafica_form_nazione').closest('.form-group'),
                    'selectProvincia': $Modal.find('#indirizzo_anagrafica_form_provincia').closest('.form-group'),
                    'selectComune': $Modal.find('#indirizzo_anagrafica_form_comune').closest('.form-group'),
                    'textProvincia': $Modal.find('#indirizzo_anagrafica_form_provincia_text').closest('.form-group'),
                    'textComune': $Modal.find('#indirizzo_anagrafica_form_comune_text').closest('.form-group')
                };

                formGroupsIndirizzi.selectNazione.find('select').on('change', function (e) {
                    changeNazione($(this).val(), formGroupsIndirizzi);
                });

                formGroupsIndirizzi.selectProvincia.find('select').on('change', function (e) {
                    changeProvincia($(this).val(), formGroupsIndirizzi);
                });

                if ($Modal.find('form').data('id') != 0) {
                    redrawGeoSelect('select', formGroupsIndirizzi);
                } else {
                    formGroupsIndirizzi.selectNazione.find('select').trigger('change');
                }

            },
            onOk: function ($modal) {
                HandleBarHelper.lockScreen();
                $modal.find('form').ajaxSubmit({
                    'dataType': 'json', 'success': function (ret_data) {
                        if (ret_data.result) {
                            window.location = window.location.href;
                            window.location.reload();
                        } else {
                            HandleBarHelper.alert({'content': ret_data.errors.join('<br />')});
                            HandleBarHelper.unlockScreen();
                        }
                    }
                });
            },
            onCancel: null,
            cancelLbl: _('Annula'),
            okLbl: _('Ok'),
            modalOptions: {
                backdrop: 'static',
                keyboard: false
            },
            parentElement: 'body'
        };

        HandleBarHelper.modalForm(settings);


    });

    $instance.find('#scelta_pagamento_form').on('submit', function (e) {

        if ($instance.find('#scelta_pagamento_form_condizioni_vendita:checked').length == 0) {
            e.preventDefault();
            var opts = {
                'type': 'danger',
                'titolo': _('Condizioni di vendita'),
                'content': _('Per procedere alla conferma d\'ordine è necessario accettare le condizioni di vendita'),
                'OK': 'Ok',
                'callback': null
            };
            HandleBarHelper.alert(opts);

        }

    });

    $instance.find('.indirizzo').on('click', function (e) {
        e.preventDefault();
        $instance.find('#corrieri').removeClass('in');
        $instance.find('#pagamenti').removeClass('in');
        $instance.find('.conclusione').addClass('hide');
        var $panel = $(this).closest('.panel');
        var $indirizziContainer = $instance.find('#indirizzi');
        $indirizziContainer.find('.btn-continue').removeClass('hide');
        $instance.find('.indirizzo').removeClass('panel-primary').addClass('panel-default');
        $(this).removeClass('panel-default').addClass('panel-primary');
        $indirizziContainer.find('[type=radio]').filter('[value=' + $panel.data('id') + ']').trigger('click');
        $indirizziContainer.find(".btn-go-indirizzi").click();
    });

    $instance.find('.corriere').on('click', function (e) {
        e.preventDefault();
        $instance.find('#pagamenti').removeClass('in');
        $instance.find('.conclusione').addClass('hide');
        var $panel = $(this).closest('.panel');
        var $corrieriContainer = $instance.find('#corrieri');
        $corrieriContainer.find('.btn-continue').removeClass('hide');
        $instance.find('.corriere').removeClass('panel-primary').addClass('panel-default');
        $(this).removeClass('panel-default').addClass('panel-primary');
        $corrieriContainer.find('[value=' + $panel.data('id') + ']').trigger('click');
        $corrieriContainer.find(".btn-continue").click();
    });

    $instance.find('.pagamento').on('click', function (e) {
        e.preventDefault();
        var $panel = $(this).closest('.panel');
        var $pagamentiContainer = $instance.find('#pagamenti');
        $pagamentiContainer.find('.btn-continue').removeClass('hide');
        $instance.find('.pagamento').removeClass('panel-primary').addClass('panel-default');
        $(this).removeClass('panel-default').addClass('panel-primary');
        $pagamentiContainer.find('[value=' + $panel.data('id') + ']').trigger('click');
    });

    $instance.find('.showDetail').on('click', function (e) {
        e.preventDefault();
        $('.detailDiv').slideDown();
    });


    if ($("input[name=\"registrationForm[isAzienda]\"]:checked").val() == '1') {
        $instance.find('#registrationForm_partitaIva').closest('.form-group').show();
        $instance.find('#registrationForm_ragioneSociale').closest('.form-group').show();

    } else {
        $instance.find('#registrationForm_partitaIva').closest('.form-group').hide();
        $instance.find('#registrationForm_ragioneSociale').closest('.form-group').hide();
    }

}


function changeNazione(selected, formGroupsDaRivedere) {

    for (var i in formGroupsDaRivedere) {
        formGroupsDaRivedere[i].removeClass('has-error');
        formGroupsDaRivedere[i].find('.error-block').closest('div').remove();
    }

    var data = {'nazione': selected};

    $.ajax({
        type: 'POST',
        url: '/admin/provincia/options',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {

                // se ho ricevuto dati popolo la select, altrimenti mostro i campi testuali
                if (ret_data.options.length) {
                    var compiled = HandleBarHelper.compile('options');
                    var markup = compiled(ret_data);
                    formGroupsDaRivedere.selectProvincia.find('select').html(markup);
                    var compiled = HandleBarHelper.compile('emptyComune');
                    formGroupsDaRivedere.selectComune.find('select').html(compiled());
                    redrawGeoSelect('select', formGroupsDaRivedere);
                    formGroupsDaRivedere.selectProvincia.find('select').trigger('change');
                } else {
                    redrawGeoSelect('text', formGroupsDaRivedere);
                }
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}

function changeProvincia(selected, formGroupsDaRivedere) {
    var data = {'provincia': selected};
    $.ajax({
        type: 'POST',
        url: '/admin/comuni/options',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {

                if (ret_data.options.length) {
                    var compiled = HandleBarHelper.compile('options');
                    markup = compiled(ret_data);
                    formGroupsDaRivedere.selectComune.find('select').html(markup);
                    redrawGeoSelect('select', formGroupsDaRivedere);
                } else {
                    redrawGeoSelect('text', formGroupsDaRivedere);
                }

            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}


function redrawGeoSelect(type, formGroups) {

    if (typeof type == 'undefined') {
        type = 'text';
    }

    if (type == 'text') {

        // nascondo le select
        formGroups.selectProvincia.hide();
        formGroups.selectComune.hide();

        // imposto i select con un unico valore a 0
        formGroups.selectProvincia.find('select').html('<option value="0"></option>');
        formGroups.selectComune.find('select').html('<option value="0"></option>');

        // mostro le versioni testuali
        formGroups.textProvincia.show();
        formGroups.textComune.show();

    } else if (type == 'select') {

        // mostro le versioni a tendina
        formGroups.selectProvincia.show();
        formGroups.selectComune.show();


        // nascondo le versioni testuali
        formGroups.textProvincia.hide();
        formGroups.textComune.hide();

        // imposto i campi testuali a valore vuoto
        formGroups.textProvincia.find('input').val('');
        formGroups.textComune.find('input').val('');

    }

}


function redrawGeoSelect(type, formGroups) {

    if (typeof type == 'undefined') {
        type = 'text';
    }

    if (type == 'text') {

        // nascondo le select
        formGroups.selectProvincia.hide();
        formGroups.selectComune.hide();

        // imposto i select con un unico valore a 0
        formGroups.selectProvincia.find('select').html('<option value="0"></option>');
        formGroups.selectComune.find('select').html('<option value="0"></option>');

        // mostro le versioni testuali
        formGroups.textProvincia.show();
        formGroups.textComune.show();

    } else if (type == 'select') {

        // mostro le versioni a tendina
        formGroups.selectProvincia.show();
        formGroups.selectComune.show();

        // nascondo le versioni testuali
        formGroups.textProvincia.hide();
        formGroups.textComune.hide();

        // imposto i campi testuali a valore vuoto
        formGroups.textProvincia.find('input').val('');
        formGroups.textComune.find('input').val('');

    }

}

function changeNazione(selected, formGroupsDaRivedere) {

    for (var i in formGroupsDaRivedere) {
        formGroupsDaRivedere[i].removeClass('has-error');
        formGroupsDaRivedere[i].find('.error-block').closest('div').remove();
    }

    var data = {'nazione': selected};

    $.ajax({
        type: 'POST',
        url: '/admin/provincia/options',
        data: data,
        dataType: 'json',
        success: function (ret_data) {
            if (ret_data.result) {

                // se ho ricevuto dati popolo la select, altrimenti mostro i campi testuali
                if (ret_data.options.length) {
                    var compiled = HandleBarHelper.compile('options');
                    var markup = compiled(ret_data);
                    formGroupsDaRivedere.selectProvincia.find('select').html(markup);
                    var compiled = HandleBarHelper.compile('emptyComune');
                    formGroupsDaRivedere.selectComune.find('select').html(compiled());
                    redrawGeoSelect('select', formGroupsDaRivedere);
                    formGroupsDaRivedere.selectProvincia.find('select').trigger('change');
                } else {
                    redrawGeoSelect('text', formGroupsDaRivedere);
                }
            } else {
                var opts = {
                    'type': 'danger',
                    'titolo': _('Attenzione'),
                    'content': ret_data.errors.join('<br />'),
                    'OK': 'Ok',
                    'callback': null
                };
                HandleBarHelper.alert(opts);
            }
        }
    });

}
