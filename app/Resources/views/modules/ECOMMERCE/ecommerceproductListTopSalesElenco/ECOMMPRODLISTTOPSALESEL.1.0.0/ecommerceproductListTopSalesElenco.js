$(document).ready(function () {
    if ($('#' + instance).length) {
        var loading = false;
        var page = 1;
        var pages = [];
        $("#" + instance + " .lista-prodotti").find(".nome-prodotto").matchHeight();

        $('#' + instance + " .lista-prodotti").on('click', '.wishlist-button', function (e) {
            e.preventDefault();
            var q = $(this);
            var product_id = $(this).data("product");
            var eraAttiva = false;
            if (q.hasClass("active")) {
                eraAttiva = true;
            }
            if (!q.hasClass("loading")) {
                q.addClass("loading");
                $.ajax({
                    type: 'POST',
                    url: '/account/wishlist/addRemove/' + product_id,
                    dataType: 'json'
                }).done(function () {
                    q.toggleClass("active");
                    if (eraAttiva) {
                        $("#badgeWishlist").text(parseInt($("#badgeWishlist").text()) - 1);
                    } else {
                        $("#badgeWishlist").text(parseInt($("#badgeWishlist").text()) + 1);
                    }
                    q.removeClass("loading");
                })
                    .fail(function (r) {
                        $(this).removeClass("loading");
                        console.log(r);
                        HandleBarHelper.modal({
                            'id': 'noWishlist',
                            'parentElement': '#' + instance,
                            modalOptions: {
                                backdrop: true,
                                keyboard: false
                            }
                        });
                        // alert("Per usare la Wishlist devi registrarti");
                    });
            }

        });

        // if ($("#" + instance + ' .pagination').length) {
        //
        //     $("#" + instance + ' .pagination').closest('.navigation').hide();
        //
        //     $("#" + instance + ' .pagination').find('.page').each(function () {
        //         pages.push(parseInt($(this).text()));
        //     });
        //     $(window).bind('scroll', function () {
        //         if (!loading && pages.length && ($(window).scrollTop() >= $("#" + instance + " .colonna-prodotto:last").offset().top + $("#" + instance + " .colonna-prodotto:last").outerHeight() + $('.pagination').outerHeight() - window.innerHeight)) {
        //             console.log('loading!');
        //             loading = true;
        //             //$('<div class="row loading"><img src="/img/ajax-loader.gif" style="z-index:1000;width:50px;height:50px;" /></div>"').appendTo($('.blog-bottom-left'));
        //             if (pages.length) {
        //                 current = pages.shift();
        //                 // slugs = location.pathname.split('/');
        //                 // slugs.shift();
        //                 // slugs.shift();
        //                 // slugs.pop();
        //                 var endPoint = '/productscategory-json/' + $("#" + instance + " .category").data('category');
        //                 endPoint += '/' + current;
        //
        //                 // if (slugs.length) {
        //                 //     endPoint += '/' + slugs;
        //                 // }
        //
        //
        //                 $.ajax({
        //                     type: 'POST',
        //                     url: endPoint,
        //                     dataType: 'json',
        //                     success: function (ret_data) {
        //                         if (ret_data.result) {
        //                             var blogPost = HandleBarHelper.compile('product');
        //                             console.log(blogPost);
        //                             html = '';
        //                             for (var i = 0; i < ret_data.data.length; i++) {
        //                                 html += blogPost(ret_data.data[i]);
        //                             }
        //                             console.log(html);
        //                             //$('.loading').remove();
        //                             $(html).insertAfter($('.colonna-prodotto:last'));
        //
        //                             loading = false;
        //                             $("#" + instance + " .lista-prodotti").find(".nome-prodotto").matchHeight();
        //
        //                         } else {
        //                             var opts = {
        //                                 'type': 'danger',
        //                                 'titolo': _('Attenzione'),
        //                                 'content': ret_data.errors.join('<br />'),
        //                                 'OK': 'Ok',
        //                                 'callback': null
        //                             };
        //                             HandleBarHelper.alert(opts);
        //                         }
        //                     }
        //                 });
        //
        //             }
        //
        //         }
        //     });
        // }
    }
});