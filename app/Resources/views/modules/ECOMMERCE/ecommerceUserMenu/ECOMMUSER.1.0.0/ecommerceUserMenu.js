$(function () {
    if ($("#" + instance).length) {
        $("#" + instance).find(".pannello-login-btn").click(function () {
            $(this).find(".pannello-login").show();
        });
        $("body").click(function (e) {
            if (!$(e.target).closest(".pannello-login,.pannello-login-btn").length) {
                $("#" + instance + " .pannello-login").hide();
            }
        });
    }

});