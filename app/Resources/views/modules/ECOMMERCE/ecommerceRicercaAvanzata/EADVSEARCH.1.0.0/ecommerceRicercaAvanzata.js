$(document).ready(function () {
    if ($('#' + instance).length) {
        $("#" + instance + " .ricerca-avanzata").find(".select-style").select2();
        $("#" + instance + " .search").click(function (e) {
            e.preventDefault();
            var url = "/ricerca-prodotti?";
            var category = $("#" + instance + " .category-search").val();
            var brand = $("#" + instance + " .brand-search").val();
            if (category != "" && category != null) {
                url += "category=" + category;
            }
            if (brand != "" && brand != null) {
                url += "&brand=" + brand;
            }
            window.location.href = url;
        });
    }


});