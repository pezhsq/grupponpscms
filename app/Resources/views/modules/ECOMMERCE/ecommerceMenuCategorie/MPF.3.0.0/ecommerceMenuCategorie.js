$(function () {
    var instanza = $("#" + instance);
    if (instanza.length) {
        instanza.find(".nome-cat-primaria").click(function (e) {
            e.preventDefault();
            var id = $(this).attr("href");
            console.log(id);
            $(id).addClass("active");
        });
        $("body").click(function (e) {
            if (!$(e.target).closest(".submenu,.nome-cat-primaria").length) {
                $(".submenu.active").removeClass("active");
            }
        });
    }

});
