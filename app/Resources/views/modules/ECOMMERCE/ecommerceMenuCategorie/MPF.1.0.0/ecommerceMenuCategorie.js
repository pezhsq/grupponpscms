if ($('#' + instance).length) {
    chiusura_menu = function () {
        $(".treeMenuFullScreen").removeClass("first-opening").removeClass("second-opening");
        $(".treeMenuFullScreen").find(".open,.selected").removeClass("open").removeClass("selected");
        $("body").removeClass("fullscreenMenu");
    };
}

$(".mobileToggler").click(function () {
    $(".pannello-mobile").removeClass("opened");
});

$(document).ready(function () {
    if ($('#' + instance).length) {
        var instanza = $("#" + instance);
        $(".prodotti_open").click(function () {
            $(".pannello-mobile").removeClass("opened");
            $("body").addClass("fullscreenMenu");

        });
        $(".treeMenuFullScreen").find(".menu-prodotti li").click(function (e) {
            e.preventDefault();
            var parent = $(this).find(".elemento").attr("href").substring(1);
            $(".treeMenuFullScreen").addClass("first-opening");
            $(".treeMenuFullScreen").removeClass("second-opening");
            $(".treeMenuFullScreen").find(".subsubmenu-prodotti.open").removeClass("open");
            $(".treeMenuFullScreen").find(".submenu-prodotti.open").removeClass("open");
            $(".treeMenuFullScreen").find(".submenu-prodotti[data-parent='" + parent + "']").addClass("open");
            $(".treeMenuFullScreen").find(".menu-prodotti .elemento.selected").removeClass("selected");
            $(this).find(".elemento").addClass("selected");
        });
        $(".treeMenuFullScreen").find(".submenu-prodotti .elemento").click(function (e) {
            e.preventDefault();
            var parent = $(this).attr("href").substring(1);
            $(".treeMenuFullScreen").addClass("second-opening");
            $(".treeMenuFullScreen").find(".subsubmenu-prodotti.open").removeClass("open");
            $(".treeMenuFullScreen").find(".subsubmenu-prodotti[data-parent='" + parent + "']").addClass("open");
            $(".treeMenuFullScreen").find(".submenu-prodotti .elemento.selected").removeClass("selected");
            $(this).addClass("selected");
        });
        $(".treeMenuFullScreen").find(".close-btn").click(function () {
            chiusura_menu();
        });
        $(".treeMenuFullScreen").click(function (e) {
            //TODO NON FUNZIONA SU IE (CONTROLLARE E FIXARE)
            if (!$(".treeMenuFullScreen.second-opening").length && !e.target.closest(".menu-prodotti-column,.submenu-prodotti-column,.back-arrow")) {
                chiusura_menu();
            }

        });

        $(".treeMenuFullScreen .back-arrow").click(function () {
            if ($(this).parents(".primo").length) {
                $(".treeMenuFullScreen").removeClass("first-opening");
                return true;
            }
            if ($(this).parents(".secondo").length) {
                $(".treeMenuFullScreen").removeClass("second-opening");
                return true;
            }
            $("body").removeClass("fullscreenMenu");
        });
    }


});