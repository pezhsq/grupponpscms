$(document).ready(function () {
    if ($('#' + instance).length) {
        $("#" + instance + " .lista-prodotti").find(".nome-prodotto").matchHeight();
    }
    $("#" + instance + " .slider").slick({
        "dots": false,
        "arrow": true,
        "infinite": true,
        "slidesToShow": 4,
        "slidesToScroll": 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

});