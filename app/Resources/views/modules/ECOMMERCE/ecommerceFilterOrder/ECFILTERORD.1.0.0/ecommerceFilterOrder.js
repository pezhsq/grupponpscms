$(document).ready(function () {
    if ($('#' + instance).length) {
        $("#" + instance + " .filter-select").change(function () {
            if ($(this).val() != "") {
                var params = getAllUrlParams();
                var params_new = JSON.parse($(this).val());
                var params_desired = $.extend(params, params_new);
                window.location.href = window.location.href.split('?')[0] + "?" + $.param(params_desired);
            } else {
                window.location.href = window.location.href.split('?')[0];
            }
        });
    }
});