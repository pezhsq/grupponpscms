$(document).ready(function () {
    if ($('#' + instance).length) {
        $("#" + instance + " .search-btn").click(function () {
            if ($("#" + instance + " .search-wishlist").val().trim() != "") {
                var params = getAllUrlParams();
                var params_new = {"term": $("#" + instance + " .search-wishlist").val()};
                var params_desired = $.extend(params, params_new);
                var params_filtered = {};
                $.each(params_desired, function (index, element) {
                    params_filtered[index] = decodeURIComponent(element);
                });
                window.location.href = window.location.href.split('?')[0] + "?" + $.param(params_filtered);
            }
        });
        $("#" + instance + " .filter-select").change(function () {
            if ($(this).val() != "") {
                var params = getAllUrlParams();
                var params_new = JSON.parse($(this).val());
                var params_desired = $.extend(params, params_new);
                var params_filtered = {};
                $.each(params_desired, function (index, element) {
                    params_filtered[index] = decodeURIComponent(element);
                });
                window.location.href = window.location.href.split('?')[0] + "?" + $.param(params_filtered);
            } else {
                window.location.href = window.location.href.split('?')[0];
            }
        });
    }
});