$(document).ready(function () {
    if ($('#' + instance).length) {
        $("#" + instance + " .lista-prodotti").find(".nome-prodotto").matchHeight();
    }
    $("#" + instance + " .wishlist-button").click(function (e) {
        e.preventDefault();
        console.log("CHIAMATA");
        var q = $(this);
        var product_id = $(this).data("product");
        $.ajax({
            type: 'POST',
            url: '/account/wishlist/remove/' + product_id,
            dataType: 'json'
        }).done(function () {
            q.parents(".colonna-prodotto").remove();
        })
            .fail(function (r) {
                alert("Per usare la Wishlist devi registrarti");
            });
    });


});