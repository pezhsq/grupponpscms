$(document).ready(function () {
    if ($('#' + instance).length) {
        $("#" + instance + " .lista-prodotti").find(".nome-prodotto").matchHeight();
    }
    $("#" + instance + " .wishlist-button").click(function (e) {
        e.preventDefault();
        var q = $(this);
        var product_id = $(this).data("product");
        if (!q.hasClass("loading")) {
            q.addClass("loading");
            $.ajax({
                type: 'POST',
                url: '/account/wishlist/remove/' + product_id,
                dataType: 'json'
            }).done(function () {
                q.removeClass("loading");
                q.parents(".colonna-prodotto").remove();
                $("#badgeWishlist").text(parseInt($("#badgeWishlist").text()) - 1);
            })
                .fail(function (r) {
                    alert("Per usare la Wishlist devi registrarti");
                });
        }

    });


});