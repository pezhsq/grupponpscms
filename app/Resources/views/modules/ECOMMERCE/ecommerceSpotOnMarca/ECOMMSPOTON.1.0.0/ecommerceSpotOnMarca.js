$(document).ready(function () {
    if ($('#' + instance).length) {
        $("#" + instance + " .spotMarca").find(".nome-prodotto").matchHeight();
    }

    $('#' + instance + " .wishlist-button").click(function (e) {
        e.preventDefault();
        var q = $(this);
        var product_id = $(this).data("product");
        $.ajax({
            type: 'POST',
            url: '/account/wishlist/addRemove/' + product_id,
            dataType: 'json'
        }).done(function () {
            q.toggleClass("active");
        })
            .fail(function (r) {
                console.log(r);
                HandleBarHelper.modal({
                    'id': 'noWishlist',
                    'parentElement': '#' + instance,
                    modalOptions: {
                        backdrop: true,
                        keyboard: false
                    }
                });
                // alert("Per usare la Wishlist devi registrarti");
            });
    });
});