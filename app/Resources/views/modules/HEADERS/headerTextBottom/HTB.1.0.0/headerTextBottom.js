$(function() {
  var menu_height = 0;
  if($(".navbar").length)
    menu_height = $(".navbar").outerHeight();

  $(window).resize(function(event) {
    if($(".navbar").length)
      menu_height = $(".navbar").outerHeight();
  });

  $(window).on("scroll", function() {
    if($(".navbar").length)
      menu_height = $(".navbar").outerHeight();
  });

  if ($('#' + instance).find('#scroll_button').find('a').attr('href') == '#') {
    $('#scroll_button a').on('click', function(e) {
      $('html, body').animate({ scrollTop: $(this).closest('section').height() - menu_height }, 800, 'linear');
    });
  }else if($('#' + instance).length) {
    $('#scroll_button a').on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - menu_height }, 800, 'linear');
    });
  }

  if ($('#' + instance).length) {
    AOS.init({
      disable:"mobile"
    });
  }

});
