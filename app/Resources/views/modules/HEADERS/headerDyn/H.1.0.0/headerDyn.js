$(function() {
  if ($('#' + instance).length) {
    setInterval(function() {
      $('#' + instance).find(".text_with_background_image .arrow").toggleClass('animate');
    }, 1200);

    $('#' + instance).find(".text_with_background_image .arrow").click(function(event) {
      /* Act on the event */
      $('html, body').animate({
        scrollTop: $('#' + instance).height() - 80
      }, 1000);
    });
  }
});
