$(function () {
    if ($("#" + instance).length) {
        var instanza = $("#" + instance);

        instanza.find(".slider").slick({
            "arrows": false,
            "dots": true,
            "slidesToShow": 1,
            "infinite": false
        });
        objectFitImages();
    }
});