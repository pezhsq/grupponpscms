$(function() {

  if ($('#scroll_button').find('a').attr('href') == '#') {
    $('#scroll_button a').on('click', function(e) {
      $('html, body').animate({ scrollTop: $(this).closest('section').height() - $('.navbar-default').height() }, 500, 'linear');
    });
  }else {
    $('#scroll_button a').on('click', function(e) {
      e.preventDefault();
      console.log("PROVA");
      $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - $('.navbar-default').height() }, 500, 'linear');
    });
  }

});
