$(function() {
  if ($('#' + instance).length) {
    $('#' + instance).find(".text_with_background_image #scroll_button a[href='#']").click(function(event) {
      /* Act on the event */
      $('html, body').animate({
        scrollTop: $('#' + instance).height() - 80
      }, 1000);
    });
  }
});