$(function() {
    if($('#' + instance).length) {

      if ($('.scroll-btn').attr('href') == '#') {
        $('.scroll-btn').on('click', function(e) {
          $('html, body').animate({ scrollTop: $(this).closest('section').height() - $('.navbar-menu .navbar-fixed-top .navbar-collapse').height() }, 1000, 'linear');
        });
      }else {
        $('.scroll-btn').on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - $('.navbar-menu .navbar-fixed-top .navbar-collapse').height() }, 1000, 'linear');
        });
      }

    }
});
