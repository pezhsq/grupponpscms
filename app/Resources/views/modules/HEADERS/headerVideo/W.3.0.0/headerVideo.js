$(document).ready(function() {
  if ($('#' + instance).length) {
    $('#' + instance).find(".fa-angle-down").on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({
        scrollTop: $('#' + $(this).data('to')).offset().top - 60
      }, 1000);
    });
  }
});