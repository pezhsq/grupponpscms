/**
 * Created by gabricom on 22/03/2017.
 */
$(function () {
    if ($("#" + instance).length) {
        if (($(window).scrollTop()) > 80) {
            $(".dropdownTreeMenu .navbar").addClass('scrolled');
        }
        if (($(window).scrollTop()) <= 80) {
            $(".dropdownTreeMenu .navbar").removeClass('scrolled');
        }
        $(window).on("scroll", function () {
            if (($(window).scrollTop()) > 80) {
                $(".dropdownTreeMenu .navbar").addClass('scrolled');
            }
            if (($(window).scrollTop()) <= 80) {
                $(".dropdownTreeMenu .navbar").removeClass('scrolled');
            }
        })
        // $("#" + instance + " .overlay-submenu").each(function (index, element) {
        //     var height = 100 / $(element).find("li").length;
        //     $(element).find("li").css("height", height + "%");
        // });
        $('.dropdownTreeMenu #toggle').click(function () {
            $(this).toggleClass('active');
            $(".gradient").fadeOut();
            $(".selettoreLingue").toggleClass("hidden-xs");
            $(".dropdownTreeMenu #overlay").toggleClass('open');
            $(".pulsante-ecommerce").toggleClass('hidden-xs');
            if ($(".dropdownTreeMenu #overlay").hasClass('open')) {
                $("body").css("overflow", "hidden");
            } else {
                $("body").css("overflow", "");
                $(".gradient").fadeIn();
            }
        });
        // $("#" + instance + " .submenu-toggler").click(function () {
        //     $("#" + $(this).data("subtree")).addClass("opened");
        // });
        // $("#" + instance + " .submenu-back").click(function () {
        //     $(this).parents(".overlay-submenu").removeClass("opened");
        // });
    }
});