$(function() {

  var $logo_noscroll = $(".logo_noscroll");
  var $logo_scroll = $(".logo_scroll");
  var $navbar = $('.navbar');
  var $overlay = $('#overlay');

  $(window).on("scroll", function() {
    if (($(window).scrollTop()) > 80 && !$navbar.hasClass('open')) {
      $logo_noscroll.addClass('hidden');
      $logo_scroll.removeClass('hidden');
      $navbar.addClass('resize_menu');
    }
    if (($(window).scrollTop()) <= 80 && !$overlay.hasClass('open') && !$navbar.hasClass('blog-header')) {
      $logo_noscroll.removeClass('hidden');
      $logo_scroll.addClass('hidden');
      $navbar.removeClass('resize_menu');
    }
  });

  $('#toggle').click(function() {
    $(this).toggleClass('active');
    $overlay.toggleClass('open');
    if ($overlay.hasClass('open')) {
      $("body").css("overflow","hidden");
      $navbar.addClass('resize_menu');
      $navbar.addClass('no_boxshadow');
      $logo_noscroll.addClass('hidden');
      $logo_scroll.removeClass('hidden');
      $(".albero_menu").fadeIn('slow', function() {
      });
    } else {
      $(".albero_menu").hide();
      $("body").css("overflow","");
      $navbar.addClass('resize_menu');
      $navbar.removeClass('no_boxshadow');
      if ($(window).scrollTop() <= 80) {
        $navbar.removeClass('resize_menu');
        $logo_noscroll.removeClass('hidden');
        $logo_scroll.addClass('hidden');
      }
    }
  });

  if ($navbar.hasClass('resize_menu') && $(window).scrollTop() <= 80) {
    $logo_noscroll.removeClass('hidden');
    $logo_scroll.addClass('hidden');
    $navbar.removeClass('resize_menu');
  } else {
    $logo_noscroll.addClass('hidden');
    $logo_scroll.removeClass('hidden');
    $navbar.addClass('resize_menu');
  }
});
