$(document).ready(function() {
  var $navbar = $('.navbar');

  if (($(window).scrollTop()) > 80) {
    $navbar.addClass('resize_menu');
  }
  if (($(window).scrollTop()) <= 80) {
    $navbar.removeClass('resize_menu');
  }

  $('.modulo_menu_centered #toggle').click(function() {
    $(this).toggleClass('active');
    $(".modulo_menu_centered #overlay").toggleClass('open');
    if ($(".modulo_menu_centered #overlay").hasClass('open')) {
      $("body").css("overflow","hidden");
    } else {
      $("body").css("overflow","");
    }
  });

  $(window).on("scroll", function() {
    if (($(window).scrollTop()) > 80) {
      $navbar.addClass('resize_menu');
    }
    if (($(window).scrollTop()) <= 80) {
      $navbar.removeClass('resize_menu');
    }
  });
});
