$(function() {
  if ($('#' + instance).length) {
    if ($(window).scrollTop() === 0) {
      $(".navbar-fixed-top").css('background-color', 'rgba(51, 51, 51, 0.55)').css('border-color', '#fff');
    }

    $(window).on("scroll", function() {
      if (($(window).scrollTop()) > 60) {
        $(".navbar-fixed-top").css('background-color', '#333').css('border-color', 'transparent');
        $(".navbar").addClass('resize_menu');
      }
      if (($(window).scrollTop()) <= 60) {
        $(".navbar-fixed-top").css('background-color', 'rgba(51, 51, 51, 0.55)').css('border-color', '#fff');
      }
    });
  }
});

