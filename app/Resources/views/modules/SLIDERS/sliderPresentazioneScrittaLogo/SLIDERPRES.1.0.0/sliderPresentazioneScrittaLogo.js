/**
 * Created by gabricom on 22/03/2017.
 */
$(function(){
    var menu_height=80;
   if($("#"+instance).length){
       var instanza=$("#"+instance);
       instanza.find(".slider").slick({
           "dots":true
       });
       $(window).resize();
       AOS.init({
           disable:"mobile"
       });
       if ($('#' + instance).find('.pulsante').find('a').attr('href') == '#') {
           $("#"+ instance +' .pulsante .freccia').on('click', function(e) {
               e.preventDefault();
               $('html, body').animate({ scrollTop: $(this).closest('section').height() - menu_height }, 800, 'linear');
           });
       }else if($('#' + instance).length) {
           $("#"+ instance +' .pulsante .freccia').on('click', function(e) {
               e.preventDefault();
               $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - menu_height }, 800, 'linear');
           });
       }
   }
});

if($("#"+instance).length){
    var instanza=$("#"+instance);

    $(window).resize(function(){
        instanza.find(".slider-wrap").height(function(){
            return window.innerHeight-instanza.find(".testo-wrap").outerHeight();
        });
    })
}