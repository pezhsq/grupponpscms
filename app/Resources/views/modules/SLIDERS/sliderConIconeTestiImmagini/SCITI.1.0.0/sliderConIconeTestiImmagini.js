$(document).ready(function() {
  if ($("#" + instance).length) {
    var url = document.location.toString();
    if (url.match('#') && url.split('#')[1] != ""){
      $("#" + url.split('#')[1].replace("vino-", "")).addClass('in active');
      $("a[href='#" + url.split('#')[1].replace("vino-", "") + "']").parents(".lista_slider").addClass('active');
    }
    else{
      $(".tab-pane:first").addClass('in active');
      $(".lista_slider:first").addClass('active');
    }

    var array_sezioni = new Array();

    $("#" + instance + " .matcheight_plugin").matchHeight();

    $("#" + instance + " .slider_con_icone_testi_immagini .lista_slider").each(function(index, el) {
      array_sezioni.push($(this).find("a").attr("href"));
    });

    var waypointServizi = new Waypoint({
      element: $(".slider_con_icone_testi_immagini"),
      handler: function() {
        if (url.match('#') && url.split('#')[1] != ""){
          $("#" + url.split('#')[1].replace("vino-", "")).find(".col_sfondo_servizi, .col_testo_servizi").addClass('show_block');
        }
        else{
          $(".col_sfondo_servizi:first, .col_testo_servizi:first").addClass('show_block');
        }
      },
      offset: '60%'
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      $('html, body').animate({ scrollTop: $(".slider_con_icone_testi_immagini").offset().top - 110 }, 500, 'linear');
      var tab_opened = $(e.target).attr("href");
      var tab_closed = $(e.relatedTarget).attr("href");
      setTimeout(function() {
        $(tab_closed).find(".col_testo_servizi, .col_sfondo_servizi").removeClass('show_block');
        $(tab_opened).find(".col_testo_servizi, .col_sfondo_servizi").addClass('show_block');
      }, 50);
    });


    $("#" + instance + ' .slider_con_icone_testi_immagini').on('swipeleft', function(e) {
      for(var x=0;x<array_sezioni.length;x++){
        if($(array_sezioni[x]).hasClass('active')){
          if(x < array_sezioni.length)
            $('.nav-tabs a[href="'+array_sezioni[x+1]+'"]').tab('show');
        }
      }
    });

    $("#" + instance + ' .slider_con_icone_testi_immagini').on('swiperight', function(e) {
      for(var x=0;x<array_sezioni.length;x++){
        if($(array_sezioni[x]).hasClass('active')){
          if(x > 0)
            $('.nav-tabs a[href="'+array_sezioni[x-1]+'"]').tab('show');
        }
      }
    });
  }
});


$(window).on("load", function (e) {
  if ($('#' + instance).length) {
    var url = document.location.toString();
    if (url.match('#') && url.split('#')[1] != ""){
      $('html, body').animate({ scrollTop: $(".slider_con_icone_testi_immagini").offset().top - 111 }, 800, 'linear');
    }
  }
});
