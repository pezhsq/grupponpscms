$(document).ready(function() {
  if ($(".tabs_with_text_and_images").length) {
    var array_sezioni = new Array()

    $(".tabs_with_text_and_images .lista_slider").each(function(index, el) {
      array_sezioni.push($(this).find("a").attr("href"));
    });

    console.log(array_sezioni);

    var waypointServizi = new Waypoint({
      element: $(".tabs_with_text_and_images"),
      handler: function() {
        $("#grafica .col_sfondo_servizi, #grafica .col_testo_servizi").addClass('show_block');
      },
      offset: '60%'
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      $('html, body').animate({ scrollTop: $(".tabs_with_text_and_images").offset().top - 80 }, 500, 'linear');
      var tab_opened = $(e.target).attr("href");
      var tab_closed = $(e.relatedTarget).attr("href");
      setTimeout(function() {
        $(tab_closed).find(".col_testo_servizi, .col_sfondo_servizi").removeClass('show_block');
        $(tab_opened).find(".col_testo_servizi, .col_sfondo_servizi").addClass('show_block');
      }, 50);
    });


    $('.tabs_with_text_and_images').on('swipeleft', function(e) {
      for(var x=0;x<array_sezioni.length;x++){
        if($(array_sezioni[x]).hasClass('active')){
          if(x < array_sezioni.length)
            $('.nav-tabs a[href="'+array_sezioni[x+1]+'"]').tab('show')
        }
      }
    });

    $('.tabs_with_text_and_images').on('swiperight', function(e) {
      for(var x=0;x<array_sezioni.length;x++){
        if($(array_sezioni[x]).hasClass('active')){
          if(x > 0)
            $('.nav-tabs a[href="'+array_sezioni[x-1]+'"]').tab('show')
        }
      }
    });

  }
});
