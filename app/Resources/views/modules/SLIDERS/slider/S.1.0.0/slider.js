// 25/01/17, 10.01
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

$(function() {
  if ($('#servizi').length) {
    var waypointServizi = new Waypoint({
      element: document.getElementById('servizi'),
      handler: function() {
        $("#grafica .col_sfondo_servizi, #grafica .col_testo_servizi").addClass('show_block');
      },
      offset: '60%'
    });
  }
});