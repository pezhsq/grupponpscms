$(document).ready(function() {

  if ($(".tabs_with_text_and_images").length) {

    var tab_name = window.location.href.split('#')[1];
    $(".tabs_with_text_and_images a[href='#"+ tab_name +"']").tab('show');

    var array_sezioni = new Array()

    $(".tabs_with_text_and_images .lista_slider").each(function(index, el) {
      array_sezioni.push($(this).find("a").attr("href"));
    });

    var waypointServizi = new Waypoint({
      element: $(".tabs_with_text_and_images"),
      handler: function() {
        $(".tabs_with_text_and_images .col_sfondo_servizi, .tabs_with_text_and_images .col_testo_servizi").addClass('show_block');
      },
      offset: '60%'
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      $('html, body').animate({ scrollTop: $(".tabs_with_text_and_images").offset().top - 120 }, 500, 'linear');
      var tab_opened = $(e.target).attr("href");
      var tab_closed = $(e.relatedTarget).attr("href");
      setTimeout(function() {
        $(tab_closed).find(".col_testo_servizi, .col_sfondo_servizi").removeClass('show_block');
        $(tab_opened).find(".col_testo_servizi, .col_sfondo_servizi").addClass('show_block');
      }, 50);
    });


    $('.tabs_with_text_and_images').on('swipeleft', function(e) {
      for(var x=0;x<array_sezioni.length;x++){
        if($(array_sezioni[x]).hasClass('active')){
          if(x < array_sezioni.length)
            $('.nav-tabs a[href="'+array_sezioni[x+1]+'"]').tab('show')
        }
      }
    });

    $('.tabs_with_text_and_images').on('swiperight', function(e) {
      for(var x=0;x<array_sezioni.length;x++){
        if($(array_sezioni[x]).hasClass('active')){
          if(x > 0)
            $('.nav-tabs a[href="'+array_sezioni[x-1]+'"]').tab('show')
        }
      }
    });

  }
});
