$(function() {
  if ($('#' + instance).length) {

    $('#' + instance).find(".slider-main").slick({
      autoplay: true,
      dots: false,
      fade: true,
      cssEase: 'linear',
      infinite: true,
      nextArrow: '<img class="next" src="/images/chalet/Freccia-slider-dx.png">',
      prevArrow: '<img class="prev" src="/images/chalet/Freccia-slider-sx.png">'
    });
  }
});