// 25/01/17, 10.01
// @author : Giovanni Battista Lenoci <gianiaz@gmail.com>

$(function() {
    if ($('#' + instance).length) {
        if ($("#"+instance+' .simple-slider').length) {
            // $("#"+instance+" .slide-container").on('init', function () {
            //     $(this).addClass('init');
            //     $(window).resize();
            // });
            $("#"+instance+' .slide-container').slick({
                dots: false,
                infinite: true,
                speed: 500,
                autoplaySpeed: 4000,
                autoplay: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                variableWidth:true,
                arrows:true,
                "prevArrow":$("#"+instance+" .slick-precedente"),
                "nextArrow":$("#"+instance+" .slick-successivo"),
                centerMode:true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
            // $(window).resize();
        }
    }
});
