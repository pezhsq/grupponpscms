$(document).ready(function() {
  if ($('#' + instance).length) {
    $('.slider_icone').slick({
      dots: false,
      infinite: true,
      speed: 500,
      autoplaySpeed:4000,
      autoplay:true,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
  }
});
