$(document).ready(function () {
    if ($('#' + instance).length) {
        $("#" + instance + " .slider-home").slick({
            "lazyLoad": 'ondemand',
            "dots": true,
            "arrows": false,
            "autoplay": true
        });
        $(window).resize();
        $("#" + instance + " .scroll_button").click(function () {
            console.log($(this).parents(".big-slider-home").offset().top);
            console.log($(this).parents(".big-slider-home").outerHeight());
            var spazio = $(this).parents(".big-slider-home").offset().top + $(this).parents(".big-slider-home").outerHeight();
            console.log(spazio);
            $("html,body").animate({scrollTop: spazio + "px"}, 1000);
        });
    }


});

var adattato = 0;
if ($("#" + instance).length) {
    $(window).resize(function () {
        if (window.matchMedia("(max-width:992px)").matches) {
            if (!adattato) {
                $("#" + instance + " .img-slider").each(function (index, element) {
                    $(element).attr("src", $(element).data("cropped"));
                });
                adattato = 1;
            }

        }
        else {
            if (adattato) {
                $("#" + instance + " .img-slider").each(function (index, element) {
                    $(element).attr("src", $(element).data("original"));
                    adattato = 0;
                });
            }
        }
    });

}