if ($(".sezione-slider-offerte").length) {

    function adattamento() {
        var width = $(".sezione-slider-offerte .slider-offerte").find(".slick-center img").outerWidth();
        $(".sezione-slider-offerte .slick-arrow.prev").css({
            "top": "50%",
            "left": "50%",
            "transform": "translateX(-" + width / 2 + "px) translateX(-75px) translateY(-50%)",
            "-webkit-transform": "translateX(-" + width / 2 + "px) translateX(-75px) translateY(-50%)",
            "-moz-transform": "translateX(-" + width / 2 + "px) translateX(-75px) translateY(-50%)",
            "-ms-transform": "translateX(-" + width / 2 + "px) translateX(-75px) translateY(-50%)",
            "-o-transform": "translateX(-" + width / 2 + "px) translateX(-75px) translateY(-50%)"
        });
        $(".sezione-slider-offerte .slick-arrow.next").css({
            "top": "50%",
            "right": "50%",
            "transform": "translateX(" + width / 2 + "px) translateX(75px) translateY(-50%)",
            "-webkit-transform": "translateX(" + width / 2 + "px) translateX(75px) translateY(-50%)",
            "-moz-transform": "translateX(" + width / 2 + "px) translateX(75px) translateY(-50%)",
            "-ms-transform": "translateX(" + width / 2 + "px) translateX(75px) translateY(-50%)",
            "-o-transform": "translateX(" + width / 2 + "px) translateX(75px) translateY(-50%)",
        });
    }

    $(window).on('load', function () {
        adattamento();
    });

    $(document).ready(function () {

        $(window).on('resize', function () {
            adattamento();
        });

        $(".sezione-slider-offerte .slider-offerte").slick({
            centerMode: true,
            slidesToShow: 1,
            variableWidth: true,
            autoplay: false,
            infinite: true,
            dots: false,
            prevArrow: "<img class='prev' src='" + slider_sx + "'>",
            nextArrow: "<img class='next' src='" + slider_dx + "'>",
            responsive: [
                {
                    breakpoint: 699,
                    settings: {
                        variableWidth: false,
                        centerMode: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        adattamento();
    });

}