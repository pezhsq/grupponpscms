$(function() {
    if ($('#' + instance).length) {

        if($(".navbar-default").length) {
          $(".navbar-default").css('position', 'fixed');
        }

        if ($("#"+instance+' .slider-pagina-webtek').length) {
            $("#"+instance+' .slider-pagina-webtek .slider-container').slick({
              arrows: false,
              dots: false,
              autoplay: true,
              autoplaySpeed: 3000,
              fade: true
            });
        }
    }
});
