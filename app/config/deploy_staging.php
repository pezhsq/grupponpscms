<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{

    public function configure()
    {

        return $this->getConfigBuilder(
        )// SSH connection string to connect to the remote server (format: user@host-or-IP:port-number)
        ->remoteComposerBinaryPath(
            '/opt/plesk/php/7.1/bin/php /var/www/vhosts/webtek.it/composer.phar'
        )->remotePhpBinaryPath('/opt/plesk/php/7.1/bin/php')->controllersToRemove([])->composerInstallFlags(
            '--prefer-dist --no-interaction --quiet'
        )->sharedFilesAndDirs(
            ['web/files', 'web/media/', 'web/assets']
        )/*** QUESTA E' LA PARTE DA VARIARE PER OGNI PROGETTO ***/
        // nome del server a cui connettersi, vedi http://wiki.webtek.it/index.php?title=Deploy_web_application#Connessione_SSH
        //ERRORE DI SINTASSI INSERITO VOLUTAMENTE, DEVI MODIFICARE QUESTO FILE SE VUOI FARE IL DEPLOY
        ->server('webteklab')// path assoluto dove fare il deploy
        ->deployDir('/var/www/vhosts/webteklab.it/PROGETTO.webteklab.it')// url SSH del repository git
        ->repositoryUrl('git@bitbucket.org:webtekteam/PROGETTO.git')// nome del branch di cui fare il deploy
        ->repositoryBranch('iperauto');
    }

    public function beforeStartingDeploy()
    {
        $this->runRemote(' if [ -f ./current/bin/console ]; then ./current/bin/console app:db:backup; fi');
    }

    public function beforeFinishingDeploy()
    {

        $this->runRemote('cp app/config/config_dev.yml.dist app/config/config_dev.yml');
        $this->runRemote('{{ console_bin }} app:assets:compile');
        $this->runRemote('{{ console_bin }} app:postdeploy');
        $this->runRemote('/opt/plesk/node/7/bin/npm --prefix web install web');
        $this->runRemote('echo 1 > clearopcache');
    }
};
