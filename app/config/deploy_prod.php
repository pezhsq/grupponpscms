<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{

    public function configure()
    {

        return $this->getConfigBuilder(
        )// SSH connection string to connect to the remote server (format: user@host-or-IP:port-number)
        ->remoteComposerBinaryPath(
            '/usr/bin/composer'
        )->remotePhpBinaryPath('/usr/bin/php -d memory_limit=512M')->controllersToRemove([])->composerInstallFlags(
            '--prefer-dist --no-interaction --quiet'
        )->sharedFilesAndDirs(
            ['web/files', 'web/media/', 'web/assets', 'var/logs', 'web/logo-firma', 'web/Logo-firma']
        )/*** QUESTA E' LA PARTE DA VARIARE PER OGNI PROGETTO ***/
        // nome del server a cui connettersi, vedi http://wiki.webtek.it/index.php?title=Deploy_web_application#Connessione_SSH
        ERRORE DI SINTASSI INSERITO VOLUTAMENTE, DEVI MODIFICARE QUESTO FILE SE VUOI FARE IL DEPLOY
        ->server('NOMESERVER')// path assoluto dove fare il deploy
    ->deployDir('DEPLOY_DIR')// url SSH del repository git
    ->repositoryUrl('GIT_REPO')// nome del branch di cui fare il deploy
    ->repositoryBranch('BRANCH');
    }

    public function beforeStartingDeploy()
    {
        $this->runRemote(' if [ -f ./current/bin/console ]; then ./current/bin/console app:db:backup; fi');
    }

    public function beforeFinishingDeploy()
    {

        $this->runRemote('cp app/config/config_dev.yml.dist app/config/config_dev.yml');
        $this->runRemote('{{ console_bin }} app:assets:compile');
        $this->runRemote('/usr/bin/npm --prefix web install web');
        $this->runRemote('echo 1 > clearopcache');
        $this->runRemote('rm web/robots.txt && touch web/robots.txt');
    }
};
