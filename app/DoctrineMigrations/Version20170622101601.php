<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170622101601 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE modelli_nuovo_rows (id INT AUTO_INCREMENT NOT NULL, modello_id INT DEFAULT NULL, slot1 LONGTEXT DEFAULT NULL, slot1Image LONGTEXT DEFAULT NULL, slot2Image LONGTEXT DEFAULT NULL, slot3Image LONGTEXT DEFAULT NULL, slot1ImageAlt VARCHAR(255) DEFAULT NULL, slot2ImageAlt VARCHAR(255) DEFAULT NULL, slot3ImageAlt VARCHAR(255) DEFAULT NULL, slot2 LONGTEXT DEFAULT NULL, slot3 LONGTEXT DEFAULT NULL, structure LONGTEXT NOT NULL, locale VARCHAR(2) NOT NULL, sort INT NOT NULL, createdAt DATETIME DEFAULT NULL, updatedAt DATETIME DEFAULT NULL, INDEX IDX_AD4EBFE810D7CADA (modello_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE modelli_nuovo_attachments (id INT NOT NULL, modello_id INT DEFAULT NULL, INDEX IDX_28176C410D7CADA (modello_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE modelli_nuovo (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, isEnabled TINYINT(1) NOT NULL, headerImgFileName VARCHAR(255) DEFAULT NULL, headerImgAlt VARCHAR(255) DEFAULT NULL, sort INT NOT NULL, deletedAt DATETIME DEFAULT NULL, createdAt DATETIME DEFAULT NULL, updatedAt DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE modelli_nuovo_translations (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, isEnabled TINYINT(1) NOT NULL, titolo VARCHAR(128) DEFAULT NULL, sottotitolo VARCHAR(255) DEFAULT NULL, metaTitle VARCHAR(70) DEFAULT NULL, metaDescription VARCHAR(180) DEFAULT NULL, slug VARCHAR(30) DEFAULT NULL, shortUrl VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_354BEE432C2AC5D3 (translatable_id), UNIQUE INDEX modelli_nuovo_translations_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE modelli_nuovo_rows ADD CONSTRAINT FK_AD4EBFE810D7CADA FOREIGN KEY (modello_id) REFERENCES modelli_nuovo (id)');
        $this->addSql('ALTER TABLE modelli_nuovo_attachments ADD CONSTRAINT FK_28176C410D7CADA FOREIGN KEY (modello_id) REFERENCES modelli_nuovo (id)');
        $this->addSql('ALTER TABLE modelli_nuovo_attachments ADD CONSTRAINT FK_28176C4BF396750 FOREIGN KEY (id) REFERENCES attachments (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE modelli_nuovo_translations ADD CONSTRAINT FK_354BEE432C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES modelli_nuovo (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE modelli_nuovo_rows DROP FOREIGN KEY FK_AD4EBFE810D7CADA');
        $this->addSql('ALTER TABLE modelli_nuovo_attachments DROP FOREIGN KEY FK_28176C410D7CADA');
        $this->addSql('ALTER TABLE modelli_nuovo_translations DROP FOREIGN KEY FK_354BEE432C2AC5D3');
        $this->addSql('DROP TABLE modelli_nuovo_rows');
        $this->addSql('DROP TABLE modelli_nuovo_attachments');
        $this->addSql('DROP TABLE modelli_nuovo');
        $this->addSql('DROP TABLE modelli_nuovo_translations');
    }
}
