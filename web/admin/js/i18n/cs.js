var Locale = {
    "pages": [],
    "default": {
        "Ecommerce": "Ecommerce",
        "Anagrafiche clienti": "Zákazník master data",
        "Categorie di Tag": "Kategorie Značky",
        "Tags": "Tagy",
        "Marchi": "Značky",
        "Pubblico \/ Abilitato \/ Visibile": "Veřejné \/ Povoleno \/ Viditelné",
        "Gruppi Attributi": "Atribut Skupiny",
        "Attributi ": "Atributy ",
        "Gruppi Caratteristiche": "Skupiny Funkcí",
        "Caratteristiche": "Funkce",
        "Prodotti": "Produkty",
        "Famiglie di prodotti": "Rodiny produktů",
        "Vettori": "Vettori",
        "Listini prezzi": "Ceníky",
        "Market Places": "Místa Na Trhu",
        "Meta di default": "Meta default",
        "Gruppi di meta": "Gruppi di meta",
        "pagina": "stránka",
        "Stati ordine": "Státy, aby",
        "non trovato": "nebyl nalezen",
        "Gestione ordini": "Order management",
        "Modelli email": "E-mailové šablony",
        "File manager": "Správce souborů",
        "Iperauto": "Iperauto",
        "Modelli Auto": "Modely Automobilů",
        "Gestione dipendenti": "Řízení zaměstnanců",
        "Categorie Gallery": "Kategorie Galerie",
        "Gallery": "Galerie",
        "Categorie Offerte": "",
        "Offerte": "",
        "Gestione Offerte": "",
        "Gestione Gruppi ": "",
        "Azienda": "",
        "Sedi": "",
        "Privato": "",
        "Registrati": "",
        "Logs": "",
        "Magazzino": "",
        "Coupons": ""
    },
    "anagrafica": {
        "Gestione anagrafiche clienti": "Řízení zákaznických master dat",
        "E' un'azienda": "To je společnost,",
        "Dati anagrafici": "Údaje o obyvatelstvu",
        "Dati accesso": "Přístup k datům",
        "Indirizzi": "Adresy",
        "Seleziona una provincia": "Vyberte provincii,",
        "Provincia": "Provincie",
        "Comune": "Společné",
        "Il campo cognome è troppo corto, dovrebbe essere almeno di {limit} caratteri": "Poslední název pole je příliš krátká, měla by být alespoň {limit} znaků",
        "Nome": "Jméno",
        "Cognome": "Příjmení",
        "C.A.P.": "PSČ",
        "Indirizzo predefinito": "Výchozí adresa",
        "Aggiungi": "Přidat",
        "Rendi predefinito": "Nastavit jako výchozí",
        "Elimina questo indirizzo": "Odstranit tuto adresu",
        "Modifica": "Edit",
        "Indirizzo aggiuntivo ": "Další adresa ",
        "C\/O": "C\/O",
        "Utente ospite": "Hosta",
        "Telefono": "",
        "Cellulare": "",
        "Tipologia": ""
    },
    "tag": {
        "Valore significativo": "Významnou hodnotu",
        "Icona": ""
    },
    "help": [],
    "brands": {
        "Nome": "Jméno",
        "Sottotitolo": "Titulky",
        "Testo": "Text",
        "Immagine elenchi": "Immagine elenchi",
        "Gestione Marchi": "Ochranná Známka Řízení"
    },
    "group_attribute": {
        "Tipo": "Typ",
        "Titolo": "Název",
        "Gruppi di Attributi": "Atribut skupiny",
        "Checkbox \/ Radio": "Checkbox \/ Radio",
        "Colore": "Barva",
        "Select": "Select",
        "Radio box": "Radio box"
    },
    "attribute": {
        "Attributi": "Atributy",
        "Gruppo di appartenenza": "Členství ve skupině",
        "Etichetta descrittiva": "Popisný štítek"
    },
    "group_feature": {
        "Chiave": "Klíč",
        "Etichetta": "Štítek",
        "Gruppi Specifiche": "Skupiny Zvláštní"
    },
    "feature": {
        "Gruppo specifiche": "Skupina-specifické",
        "Gestione caratteristiche": "Funkce pro správu",
        "Salva disposizione": "S výjimkou, jak je stanoveno v"
    },
    "news": [],
    "product_type": {
        "Famiglie di prodotto": "Výrobků",
        "Nome": "Jméno",
        "Gruppi assegnati": "Přiřazené skupiny"
    },
    "delivery": {
        "Dati anagrafici": "Údaje o obyvatelstvu",
        "Ragione Sociale": "Důvod, Proč Sociální",
        "Telefono": "Telefon",
        "Indirizzo": "Adresa",
        "Cap": "Cap",
        "Comune": "Comune",
        "Provincia": "Provincia",
        "Nazione": "Country",
        "Vettori": "Vettori",
        "Nome": "Jméno",
        "Descrizione": "Popis",
        "Soglia minima": "Minimální práh",
        "Soglia Massima": "Maximální Prahovou Hodnotou",
        "Costo": "Náklady",
        "Valido per tutte le nazioni": "Platí pro všechny národy",
        "Aggiungi nuovo scaglione di prezzi": "Aggiungi nuovo scaglione di prezzi",
        "Fasce di prezzo": "Cena se pohybuje",
        "Elimina riga": "Odstranit řádek"
    },
    "product": {
        "Gestione Prodotti": "Řízení Produktů",
        "Dati descrittivi prodotto": "Popisná data produktu",
        "Dati spedizione": "Dodací údaje",
        "Dati posizionamento": "Údaje umístění",
        "Codice": "Kód",
        "Barcode": "Čárový kód",
        "Dimensioni prodotto": "Rozměry výrobku",
        "Prezzi, Varianti e Giacenze": "Ceny, Varianty a Zásob",
        "Peso": "Hmotnost",
        "Altezza": "Výška",
        "Larghezza": "Šířka",
        "Corrieri": "Kurýři",
        "Spedibile con tutti i corrieri?": "Povoleno odesílání se všechny kurýry?",
        "Seleziona i corrieri che potranno spedire questo prodotto": "Vyberte přepravce, které může loď tento produkt",
        "Profondità": "Hloubka",
        "Prezzo netto per \"%listino%\" ": "Čistá cena za \"%listino%\" ",
        "Prezzo ivato per \"%listino%\"": "Cena pro ivato %listino%",
        "Varianti prodotto": "Varianty produktu",
        "Attributi": "Atributy",
        "Nessun attributo selezionato, comincia selezionando gli attributi per creare una variante": "Nessun attributo selezionato, comincia selezionando gli attributi per creare una variante",
        "crea variante": "vytvořit varianta",
        "giacenza": "Skladem",
        "minima": "minimální",
        "descrizione": "popis",
        "Attributi selezionati": "Vybrané atributy",
        "Varianti presenti": "Varianty dárek",
        "Nessuna variante presente": "Žádná varianta dárek",
        "Foto": "Fotografie",
        "impatto prezzo": "cenový dopad",
        "Comportamento offline": "Offline chování",
        "Destinazione redirect": "cíl přesměrování",
        "Dati strutturali": "Strukturální údaje",
        "Traduzioni": "Překlady",
        "301 verso prodotto": "301 k produktu",
        "301 verso categoria": "301 do kategorie",
        "302 verso categoria": "302 směrem kategorii",
        "Errore 404": "Chyba 404",
        "302 verso prodotto": "302 k produktu",
        "Caratteristiche prodotto": "Vlastnosti produktu",
        "Caratteristica": "Funkce",
        "Valori predefiniti": "Výchozí hodnoty",
        "Valore testuale": "Textové hodnoty",
        "Aggiungi caratteristica": "Přidat funkce",
        "Nessuna caratteristica presente": "Bez této funkce",
        "Seleziona la tipologia di caratteristica": "Vyberte typ funkce",
        "Dati prezzi": "Data ceny",
        "Immagine per elenchi": "Obraz pro seznamy",
        "Nome prodotto": "Název produktu",
        "Codice EAN": "Codice EAN",
        "Codice produttore": "Kód výrobce",
        "Prezzi": "Ceny",
        "Quantità minima": "Minimální množství",
        "Multipli": "Více",
        "Quantità": "Množství",
        "Categoria": "Kategorie",
        "Tags": "Tagy",
        "Prodotti correlati": "Související produkty",
        "Prezzo netto": "Cena netto",
        "Prezzo ivato": "Cena ivato",
        "Market places": "Místa Na Trhu",
        "Prezzo dipende da variante": "Cena závisí na varianta",
        "attuale": "aktuální",
        "Default": "Default",
        "Magazzino": "Sklad",
        "La combinazione scelta non è a magazzino": "Kombinace vybral není na skladě",
        "Quantità richiesta": "Množství požadované",
        "Prezzo civetta": "",
        "Quantità a magazzino non sufficiente": ""
    },
    "listino": {
        "Default": "Default",
        "Gestione listini": "Ceníky řízení",
        "Nome listino": "Jméno seznam"
    },
    "products": {
        "Impatto prezzo": "Cenový dopad",
        "Disponibile": "Dostupné",
        "Tipo prodotto": "Typ produktu",
        "Sempre disponibile": "Vždy k dispozici"
    },
    "market_place": {
        "Market place": "Trhu",
        "Nome": "Jméno"
    },
    "settings": {
        "Tipo di dato": "Typ dat"
    },
    "categorie_prodotti": {
        "Nodo amazon": "Uzel amazon",
        "Nome categoria": "Jméno kategorie",
        "Sottotitolo": "Titulky"
    },
    "meta": {
        "Chiave univoca": "Unikátní klíč",
        "Etichetta": "Štítek",
        "Help in linea": "On-line Pomoci",
        "Valore": "Hodnota"
    },
    "gruppi_meta": {
        "Gruppi meta tags": "Gruppi meta tags"
    },
    "meta_group": {
        "Nome": "Jméno"
    },
    "ecommerce": {
        "Il prodotto richiesto non è stato trovato": "Požadovaný produkt nebyl nalezen",
        "Imponibile": "Zdanitelné",
        "Dati Anagrafici": "Údaje O Obyvatelstvu",
        "continua &raquo;": "continue &raquo;",
        "esci": "exit",
        "Indirizzo di consegna": "Dodací adresu",
        "Metodo di consegna": "Způsob dodání",
        "Metodo di pagamento": "Způsob platby",
        "Contenuto del carrello": "Obsah košíku",
        "numero di prodotti": "počet produktů",
        "Mostra dettaglio": "Zobrazit detail",
        "Totale": "Celkem",
        "Descrizione": "Popis",
        "Q.tà": "Qty",
        "Subtotale": "Mezisoučet",
        "Se vuoi puoi effettuare ora il pagamento usando questo link: ": "Pokud chcete, můžete si udělat čas platby pomocí tohoto odkazu: ",
        "Paga ora": "Zaplatit teď",
        "Modifica i tuoi dati": "",
        "La mia wishlist": "",
        "I miei Ordini": "",
        "rimuovi wishlist": "",
        "Prodotto attualmente non disponibile": "Výrobek je v současné době nejsou k dispozici",
        "Torna all'elenco": "",
        "Accetto Termini e condizioni. Cliccando su CONCLUDI ORDINE stipuli un contratto legalmente vincolante per acquistare i prodotti": "",
        "Indica qui le note relative al tuo ordine...": "",
        "Inserisci qui le note relative al tuo ordine": "",
        "Registrazione": "",
        "Hai un codice sconto? Inseriscilo qui:": ""
    },
    "metodi_pagamento_ecommerce": {
        "Debug": "Ladění",
        "Codice": "Kód",
        "Dati per la connessione al gateway": "Údaje pro připojení ke gateway",
        "Client ID": "ID klienta"
    },
    "contact": {
        "Abbiamo ricevuto il tuo messaggio, ti risponderemo al più presto.": "Obdrželi jsme vaši zprávu, budeme odpovědět, jakmile je to možné"
    },
    "orders_status": {
        "Stati ordine": "Státy, aby",
        "Codice": "Kód",
        "Titolo": "Název"
    },
    "order": {
        "Ordini ecommerce": "Ecommerce order",
        "Dettaglio ordine": "Cílem detail",
        "Pagato": "Zaplatil",
        "Stato ordine": "Stav objednávky",
        "Somma pagata": "Zaplacená částka",
        "Rif. transazione": "Rif. transazione",
        "Metodo di pagamento": "Způsob platby",
        "Statistiche": "Statistiky",
        "Statistiche per anno": "Statistiky za rok",
        "Statistiche per mese": "Statistiky za měsíc",
        "Tracking Url": ""
    },
    "email_template": {
        "Gestione Emails": "Správa E-Mailů",
        "Variabili": "Proměnné",
        "Codice": "Kód",
        "Oggetto": "Předmět",
        "Testo": "Text"
    },
    "filemanager": {
        "Filemanager": "Filemanager",
        "Elenco files": "Seznam souborů",
        "Albero directory": "Adresářový strom",
        "La directory non può essere cancellata": "Adresář nelze smazat"
    },
    "layoutcontent": {
        "Aggiungi un nuovo elemento": "Přidat novou položku"
    },
    "modelli_nuovo": {
        "Codice": "Kód",
        "Prezzo": "Cena",
        "Design": "Design",
        "Caratteristiche": "Funkce",
        "Prestazioni": "Výkon"
    },
    "dipendenti": {
        "Nome": "Jméno",
        "Cognome": "Příjmení",
        "Email": "E-mail",
        "Telefono": "Telefon",
        "Profilo facebook": "Profil Facebook",
        "Profilo twitter": "Twitter profil",
        "Profilo linkedIn": "Profil na LinkedIn",
        "Profilo google plus": "Google plus profilu",
        "Titolo": "Název",
        "Mansione": "Práce",
        "Descrizione": "Popis",
        "Gestione Dipendenti": "Řízení Zaměstnanců"
    },
    "gallerycat": {
        "Template": "Šablony",
        "Titolo": "Název",
        "Sottotitolo": "Titulky",
        "Testo": "Text",
        "Categorie Gallery": "Kategorie Galerie"
    },
    "gallery": {
        "Titolo": "Název",
        "Sottotitolo": "Titulky",
        "Testo": "Text",
        "Titolo in lingua vuoto": "Jazyk název prázdné",
        "Gestione Gallery": "Vedení Galerie",
        "Immagini": "Obrázky",
        "Puoi caricare nuove immagini effettuando il drag & drop direttamente dal tuo computer.<br\/><br\/> \n\nIn alternativa puoi cliccare in qualsiasi parte bianca del riquadro di sinistra.<br\/><br\/> \n\nSe il riquandro contiene già delle immagini queste avranno 2 bottoni associati, uno per la modifica della descrizione e del testo alternativo della foto, l'altro per la cancellazione.<br\/><br\/> \n\nPer ogni immagine comparirà nell'angolo in alto a destra il seguente simbolo: <i class=\"fa fa-arrows-alt\"><\/i>, cliccando con il tasto sinistro del mouse su questo simbolo e trascinando la foto sarà possibile modificare l'ordine di apparizione delle foto all'interno della gallery.<br\/><br\/> \n\nAlcune foto potrebbero riportare il seguente simbolo <i class=\"fa fa-exclamation-triangle\"><\/i>, questo simbolo sta a indicare che per la foto corrispondente non è stato specificato un testo alternativo che serve ai motori di ricerca per indicizzare la foto. Cliccando sul bottone di edit sarà possibile aggiungere i dati mancnati. \n<br\/><br\/> ": "Można wgrać nowe obrazy poprzez drag &amp; drop bezpośrednio z komputera.<br \/>\n\nAlternatywnie możesz kliknąć w dowolnym białej części panelu po lewej stronie. <br \/>\n\nJeśli przebudowa zawiera już zdjęcia te będą 2 przyciski związane, jednej, aby zmienić opis i tekst alternatywny, zdjęcia, inny do usunięcia. <br \/>\n\nDla każdego zdjęcia pojawi się w prawym górnym rogu, następujący symbol: <i class=",
        "Elenco immagini": "Seznam obrázků",
        "Aiuto": "Pomoc",
        "Testo alternativo": "Alternativní text",
        "Descrizione": "Popis"
    },
    "appartamenti": {
        "Prezzo": "",
        "Superficie": ""
    },
    "offerte": {
        "Offerte": ""
    },
    "groups": {
        "Gestione gruppi": "",
        "Nome gruppo": ""
    },
    "group": {
        "Ruoli abilitati": ""
    },
    "orders": {
        "Dati Fatturazione": "",
        "Dati di consegna": "",
        "Dati di contatto": ""
    },
    "coupon": {
        "Codice": "",
        "Tipo": "",
        "Valore": "",
        "Illimitato": "",
        "Ordine minimo": "",
        "Attivo per tutti i prodotti": "",
        "Descrizione": "",
        "Gestione Coupons": "",
        "Valore in EURO": "",
        "Percentuale": "",
        "Numero di coupon disponibili": ""
    }
};