var Locale = {
    "pages": [],
    "default": {
        "Ecommerce": "Ecommerce",
        "Anagrafiche clienti": "Dane osobowe klientów",
        "Categorie di Tag": "Tagi Kategorii",
        "Tags": "Tagi",
        "Marchi": "Znaki",
        "Pubblico \/ Abilitato \/ Visibile": "Publiczny \/ Włączony \/ Widoczny",
        "Gruppi Attributi": "Grupy Atrybutów",
        "Attributi ": "Atrybuty ",
        "Gruppi Caratteristiche": "Grupy, Cechy",
        "Caratteristiche": "Cechy",
        "Prodotti": "Produkty",
        "Famiglie di prodotti": "Rodzin produktów",
        "Vettori": "Vettori",
        "Listini prezzi": "Cenniki",
        "Market Places": "Market Place",
        "Meta di default": "Meta domyślnie",
        "Gruppi di meta": "Gruppi di meta",
        "pagina": "strona",
        "Stati ordine": "Były zamówienia",
        "non trovato": "nie znaleziono",
        "Gestione ordini": "Zarządzanie zamówieniami",
        "Modelli email": "Szablony e-mail",
        "File manager": "Menedżer plików",
        "Iperauto": "Iperauto",
        "Modelli Auto": "Modele Samochodów",
        "Gestione dipendenti": "Zarządzanie pracownikami",
        "Categorie Gallery": "Kategorii Galeria",
        "Gallery": "Galeria",
        "Categorie Offerte": "",
        "Offerte": "",
        "Gestione Offerte": "",
        "Gestione Gruppi ": "",
        "Azienda": "",
        "Sedi": "",
        "Privato": "",
        "Registrati": "",
        "Logs": "",
        "Magazzino": "",
        "Coupons": ""
    },
    "anagrafica": {
        "Gestione anagrafiche clienti": "Zarządzanie danymi osobowymi klientów",
        "E' un'azienda": "Firma",
        "Dati anagrafici": "Kieszonkowy",
        "Dati accesso": "Dane dostępu",
        "Indirizzi": "Adresy",
        "Seleziona una provincia": "Wybierz prowincji",
        "Provincia": "Prowincja",
        "Comune": "Ogólny",
        "Il campo cognome è troppo corto, dovrebbe essere almeno di {limit} caratteri": "Pole \"nazwisko\" jest za krótka, powinna być nie mniejsza niż {limit} znaków",
        "Nome": "Nazwa",
        "Cognome": "Nazwisko",
        "C.A.P.": "Kod POCZTOWY",
        "Indirizzo predefinito": "Domyślny adres",
        "Aggiungi": "Dodaj",
        "Rendi predefinito": "Zrobić domyślnie",
        "Elimina questo indirizzo": "Usuń ten adres",
        "Modifica": "Zmiana",
        "Indirizzo aggiuntivo ": "Adres ",
        "C\/O": "C\/O",
        "Utente ospite": "Użytkownik gość",
        "Telefono": "",
        "Cellulare": "",
        "Tipologia": ""
    },
    "tag": {
        "Valore significativo": "Istotne znaczenie",
        "Icona": ""
    },
    "help": [],
    "brands": {
        "Nome": "Nazwa",
        "Sottotitolo": "Podtytuł",
        "Testo": "Tekst",
        "Immagine elenchi": "Immagine elenchi",
        "Gestione Marchi": "Zarządzanie Znakami Towarowymi"
    },
    "group_attribute": {
        "Tipo": "Typ",
        "Titolo": "Nazwa",
        "Gruppi di Attributi": "Grupy Atrybutów",
        "Checkbox \/ Radio": "Pole Wyboru \/ Radio",
        "Colore": "Kolor",
        "Select": "Select",
        "Radio box": "Radio box"
    },
    "attribute": {
        "Attributi": "Atrybuty",
        "Gruppo di appartenenza": "Grupy członkostwa",
        "Etichetta descrittiva": "Barwny opis"
    },
    "group_feature": {
        "Chiave": "Klucz",
        "Etichetta": "Tagi",
        "Gruppi Specifiche": "Grupy, Dane Techniczne"
    },
    "feature": {
        "Gruppo specifiche": "Grupa specyfikacji",
        "Gestione caratteristiche": "Zarządzania cechy",
        "Salva disposizione": "Zapisz"
    },
    "news": [],
    "product_type": {
        "Famiglie di prodotto": "Rodzina produktów",
        "Nome": "Nazwa",
        "Gruppi assegnati": "Grupy przypisane"
    },
    "delivery": {
        "Dati anagrafici": "Podstawowe dane",
        "Ragione Sociale": "Nazwa",
        "Telefono": "Telefon",
        "Indirizzo": "Adres",
        "Cap": "Cap",
        "Comune": "Comune",
        "Provincia": "Provincia",
        "Nazione": "Country",
        "Vettori": "Vettori",
        "Nome": "Nazwa",
        "Descrizione": "Opis",
        "Soglia minima": "Minimalny próg",
        "Soglia Massima": "Próg Maksymalnej",
        "Costo": "Cena",
        "Valido per tutte le nazioni": "Jest ważny dla wszystkich narodów",
        "Aggiungi nuovo scaglione di prezzi": "Aggiungi nuovo scaglione di prezzi",
        "Fasce di prezzo": "Przedziałach cenowych",
        "Elimina riga": "Usuń wiersz"
    },
    "product": {
        "Gestione Prodotti": "Zarządzanie Produktami",
        "Dati descrittivi prodotto": "Dane opis produktu",
        "Dati spedizione": "Dane wysyłka",
        "Dati posizionamento": "Dane pozycjonowania",
        "Codice": "Kod",
        "Barcode": "Kod kreskowy",
        "Dimensioni prodotto": "Rozmiar produktu",
        "Prezzi, Varianti e Giacenze": "Ceny, Opcje i Zapasów",
        "Peso": "Waga",
        "Altezza": "Wysokość",
        "Larghezza": "Szerokość",
        "Corrieri": "Kurierzy",
        "Spedibile con tutti i corrieri?": "Obsługa kurierów?",
        "Seleziona i corrieri che potranno spedire questo prodotto": "Wybór kurierów, którzy mogą dostarczyć ten produkt",
        "Profondità": "Głębokość",
        "Prezzo netto per \"%listino%\" ": "Cena netto dla \"%listino%\" ",
        "Prezzo ivato per \"%listino%\"": "Cena ivato, aby %listino%",
        "Varianti prodotto": "Wariantów produktu",
        "Attributi": "Atrybuty",
        "Nessun attributo selezionato, comincia selezionando gli attributi per creare una variante": "Nessun attributo selezionato, comincia selezionando gli attributi per creare una variante",
        "crea variante": "tworzy opcja",
        "giacenza": "Magazynie",
        "minima": "minimalna",
        "descrizione": "opis",
        "Attributi selezionati": "Wybrane atrybuty",
        "Varianti presenti": "Możliwości są",
        "Nessuna variante presente": "Opcja nie istnieje",
        "Foto": "Zdjęcia",
        "impatto prezzo": "wpływ cena",
        "Comportamento offline": "Postępowanie w trybie offline",
        "Destinazione redirect": "przeznaczenia redirect",
        "Dati strutturali": "Strukturalne dane",
        "Traduzioni": "Tłumaczenie",
        "301 verso prodotto": "301 do produktu",
        "301 verso categoria": "301 do kategorii",
        "302 verso categoria": "302 do kategorii",
        "Errore 404": "Błąd 404",
        "302 verso prodotto": "302 do produktu",
        "Caratteristiche prodotto": "Cechy produktu",
        "Caratteristica": "Charakterystyka",
        "Valori predefiniti": "Wartości domyślne",
        "Valore testuale": "Wartość tekstowa",
        "Aggiungi caratteristica": "Dodaj charakterystyka",
        "Nessuna caratteristica presente": "Ani jedna cecha jest obecna",
        "Seleziona la tipologia di caratteristica": "Wybierz typ charakterystyka",
        "Dati prezzi": "Dane ceny",
        "Immagine per elenchi": "Zdjęcia do listy",
        "Nome prodotto": "Nazwa produktu",
        "Codice EAN": "Codice EAN",
        "Codice produttore": "Kod producenta",
        "Prezzi": "Ceny",
        "Quantità minima": "Minimalna ilość",
        "Multipli": "Wieloskładnikowe",
        "Quantità": "Ilość",
        "Categoria": "Kategoria",
        "Tags": "Tagi",
        "Prodotti correlati": "Produkty powiązane",
        "Prezzo netto": "Cena netto",
        "Prezzo ivato": "Cena ivato",
        "Market places": "Market place",
        "Prezzo dipende da variante": "Cena zależy od opcji",
        "attuale": "bieżąca",
        "Default": "Domyślnie",
        "Magazzino": "Magazyn",
        "La combinazione scelta non è a magazzino": "Połączenie wyboru nie będzie w magazynie",
        "Quantità richiesta": "Ilość zapytanie",
        "Prezzo civetta": "",
        "Quantità a magazzino non sufficiente": ""
    },
    "listino": {
        "Default": "Domyślnie",
        "Gestione listini": "Zarządzanie cenniki",
        "Nome listino": "Nazwa cennik"
    },
    "products": {
        "Impatto prezzo": "Wpływ cena",
        "Disponibile": "Istniejący",
        "Tipo prodotto": "Typ produktu",
        "Sempre disponibile": "Zawsze w magazynie"
    },
    "market_place": {
        "Market place": "Market place",
        "Nome": "Nazwa"
    },
    "settings": {
        "Tipo di dato": "Typ danych"
    },
    "categorie_prodotti": {
        "Nodo amazon": "Węzeł amazon",
        "Nome categoria": "Nazwa kategoria",
        "Sottotitolo": "Podtytuł"
    },
    "meta": {
        "Chiave univoca": "Unikalny klucz",
        "Etichetta": "Tagi",
        "Help in linea": "Online pomoc",
        "Valore": "Wartość"
    },
    "gruppi_meta": {
        "Gruppi meta tags": "Gruppi meta tags"
    },
    "meta_group": {
        "Nome": "Nazwa"
    },
    "ecommerce": {
        "Il prodotto richiesto non è stato trovato": "Ten towar nie znaleziono",
        "Imponibile": "Opodatkowania",
        "Dati Anagrafici": "Podstawowe Dane",
        "continua &raquo;": "continue &raquo;",
        "esci": "wyjść",
        "Indirizzo di consegna": "Adres dostawy",
        "Metodo di consegna": "Sposób dostawy",
        "Metodo di pagamento": "Sposób płatności",
        "Contenuto del carrello": "Zawartość koszyka",
        "numero di prodotti": "ilość produktów",
        "Mostra dettaglio": "Wystawy szybka",
        "Totale": "Całkowita",
        "Descrizione": "Opis",
        "Q.tà": "Qty",
        "Subtotale": "Suma",
        "Se vuoi puoi effettuare ora il pagamento usando questo link: ": "Jeśli chcesz, możesz spędzić czas płatności, korzystając z tego linku: ",
        "Paga ora": "Płacisz teraz",
        "Modifica i tuoi dati": "",
        "La mia wishlist": "",
        "I miei Ordini": "",
        "rimuovi wishlist": "",
        "Prodotto attualmente non disponibile": "Produkt nie jest obecnie dostępny",
        "Torna all'elenco": "",
        "Accetto Termini e condizioni. Cliccando su CONCLUDI ORDINE stipuli un contratto legalmente vincolante per acquistare i prodotti": "",
        "Indica qui le note relative al tuo ordine...": "",
        "Inserisci qui le note relative al tuo ordine": "",
        "Registrazione": "",
        "Hai un codice sconto? Inseriscilo qui:": ""
    },
    "metodi_pagamento_ecommerce": {
        "Debug": "Debugowanie",
        "Codice": "Kod",
        "Dati per la connessione al gateway": "Dane do podłączenia do bramy",
        "Client ID": "ID klienta"
    },
    "contact": {
        "Abbiamo ricevuto il tuo messaggio, ti risponderemo al più presto.": "Otrzymaliśmy wiadomość, odpowiemy w najbliższym czasie"
    },
    "orders_status": {
        "Stati ordine": "Były zamówienia",
        "Codice": "Kod",
        "Titolo": "Nazwa"
    },
    "order": {
        "Ordini ecommerce": "Zamówienia e-commerce",
        "Dettaglio ordine": "Szybkie zamówienie",
        "Pagato": "Zapłacił",
        "Stato ordine": "Był zamówienie",
        "Somma pagata": "Kwota wypłacona",
        "Rif. transazione": "Rif. transazione",
        "Metodo di pagamento": "Sposób płatności",
        "Statistiche": "Statystyki",
        "Statistiche per anno": "Statystyki za rok",
        "Statistiche per mese": "Statystyki za miesiąc",
        "Tracking Url": ""
    },
    "email_template": {
        "Gestione Emails": "Zarządzanie Email",
        "Variabili": "Zmienne",
        "Codice": "Kod",
        "Oggetto": "Obiekt",
        "Testo": "Tekst"
    },
    "filemanager": {
        "Filemanager": "Filemanager",
        "Elenco files": "Lista plików",
        "Albero directory": "Drzewo katalogów",
        "La directory non può essere cancellata": "Katalog nie może być usunięte"
    },
    "layoutcontent": {
        "Aggiungi un nuovo elemento": "Dodaj nowy element"
    },
    "modelli_nuovo": {
        "Codice": "Kod",
        "Prezzo": "Cena",
        "Design": "Design",
        "Caratteristiche": "Cechy",
        "Prestazioni": "Wydajność"
    },
    "dipendenti": {
        "Nome": "Nazwa",
        "Cognome": "Nazwisko",
        "Email": "Email",
        "Telefono": "Telefon",
        "Profilo facebook": "Profil Facebook",
        "Profilo twitter": "Profil na twitterze",
        "Profilo linkedIn": "Profil linkedIn",
        "Profilo google plus": "Profil google plus",
        "Titolo": "Nazwa",
        "Mansione": "Stanowisko",
        "Descrizione": "Opis",
        "Gestione Dipendenti": "Zarządzanie Pracownikami"
    },
    "gallerycat": {
        "Template": "Szablon",
        "Titolo": "Nazwa",
        "Sottotitolo": "Podtytuł",
        "Testo": "Tekst",
        "Categorie Gallery": "Kategorii Galeria"
    },
    "gallery": {
        "Titolo": "Nazwa",
        "Sottotitolo": "Podtytuł",
        "Testo": "Tekst",
        "Titolo in lingua vuoto": "Nazwa w języku pusty",
        "Gestione Gallery": "Zarządzanie Gallery",
        "Immagini": "Obrazy",
        "Puoi caricare nuove immagini effettuando il drag & drop direttamente dal tuo computer.<br\/><br\/> \n\nIn alternativa puoi cliccare in qualsiasi parte bianca del riquadro di sinistra.<br\/><br\/> \n\nSe il riquandro contiene già delle immagini queste avranno 2 bottoni associati, uno per la modifica della descrizione e del testo alternativo della foto, l'altro per la cancellazione.<br\/><br\/> \n\nPer ogni immagine comparirà nell'angolo in alto a destra il seguente simbolo: <i class=\"fa fa-arrows-alt\"><\/i>, cliccando con il tasto sinistro del mouse su questo simbolo e trascinando la foto sarà possibile modificare l'ordine di apparizione delle foto all'interno della gallery.<br\/><br\/> \n\nAlcune foto potrebbero riportare il seguente simbolo <i class=\"fa fa-exclamation-triangle\"><\/i>, questo simbolo sta a indicare che per la foto corrispondente non è stato specificato un testo alternativo che serve ai motori di ricerca per indicizzare la foto. Cliccando sul bottone di edit sarà possibile aggiungere i dati mancnati. \n<br\/><br\/> ": "U kunt nieuwe afbeeldingen uploaden via drag &amp; drop rechtstreeks vanaf uw computer.<br \/>\n\nU kunt ook klikken op een wit gedeelte van het linkerdeelvenster. <br \/>\n\nAls de riquandro al bestanden bevat, zijn deze 2 toetsen is gekoppeld, te bewerken de omschrijving en de alternatieve tekst van de foto ' s, de andere voor de annulering. <br \/>\n\nVoor elke afbeelding die wordt weergegeven in de rechterbovenhoek met het volgende symbool: <i class=",
        "Elenco immagini": "Lista obrazów",
        "Aiuto": "Pomoc",
        "Testo alternativo": "Tekst alternatywny",
        "Descrizione": "Opis"
    },
    "appartamenti": {
        "Prezzo": "",
        "Superficie": ""
    },
    "offerte": {
        "Offerte": ""
    },
    "groups": {
        "Gestione gruppi": "",
        "Nome gruppo": ""
    },
    "group": {
        "Ruoli abilitati": ""
    },
    "orders": {
        "Dati Fatturazione": "",
        "Dati di consegna": "",
        "Dati di contatto": ""
    },
    "coupon": {
        "Codice": "",
        "Tipo": "",
        "Valore": "",
        "Illimitato": "",
        "Ordine minimo": "",
        "Attivo per tutti i prodotti": "",
        "Descrizione": "",
        "Gestione Coupons": "",
        "Valore in EURO": "",
        "Percentuale": "",
        "Numero di coupon disponibili": ""
    }
};