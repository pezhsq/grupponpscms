var Locale = {
    "languages": [],
    "default": {
        "Abilitato": "有効な",
        "eliminata": "eliminata",
        "Allegati": "添付",
        "Attenzione": "注意",
        "Bentornato": "おかえり",
        "Categorie News": "カテゴリのニュース",
        "Dati strutturali": "構造データ",
        "MetaTag Title": "MetaTag Title",
        "MetaTag Description": "MetaTag Description",
        "Slug": "Slug",
        "Gestione Articoli Blog": "経営のブログ記事",
        "Leggi di più": "もっと読む",
        "Modifica ordine": "用したい続きから再生する",
        "Impostazioni SITO": "Impostazioni SITO",
        "Blocchi di contenuto": "Blocchi di contenuto",
        "Gestione Template": "Gestione Template",
        "Gestione Layout": "レイアウト管理",
        "Categorie": "",
        "Inserisci i campi mancanti": "",
        "Effettua una selezione": "カテゴリーを選択",
        "Parametri errati": "誤ったパラメータ",
        "scritto da ": "書 ",
        "Impostazioni Generali": "一般設定",
        "Tasse": "税",
        "Metodi Pagamento": "お支払い方法",
        "Categorie servizi": "カテゴリーサービス",
        "Sfoglia": "閲覧",
        "Tag": "サービス",
        "Appartamenti": "アパート",
        "Immagine per l'header": "Immagine per l'header",
        "Immagini per elenchi": "画像リスト",
        "Metodi Spedizione": "配送方法",
        "Ecommerce": "Ecommerce",
        "Anagrafiche clienti": "お客様のマスターデータ",
        "Categorie di Tag": "棚分類タグ",
        "Tags": "タグ",
        "Marchi": "ブランド",
        "Pubblico \/ Abilitato \/ Visibile": "公開\/有効化\/可視",
        "Gruppi Attributi": "属団体",
        "Attributi ": "属性 ",
        "Gruppi Caratteristiche": "グループ機能",
        "Caratteristiche": "特徴",
        "Prodotti": "製品",
        "Famiglie di prodotti": "家族の商品",
        "Vettori": "Vettori",
        "Listini prezzi": "価格リスト",
        "Market Places": "市場",
        "Meta di default": "メタデフォルト",
        "Gruppi di meta": "Gruppi di meta",
        "pagina": "ページ",
        "Stati ordine": "国順",
        "non trovato": "見つかりませんで",
        "Gestione ordini": "受注管理",
        "Modelli email": "メールテンプレート",
        "File manager": "ファイルマネージャー",
        "Iperauto": "Iperauto",
        "Modelli Auto": "カーモデル",
        "Gestione dipendenti": "管理社員",
        "Categorie Gallery": "カテゴリギャラリー",
        "Gallery": "ギャラリー",
        "Categorie Offerte": "",
        "Offerte": "",
        "Gestione Offerte": "",
        "Gestione Gruppi ": "",
        "Azienda": "",
        "Sedi": "",
        "Privato": "",
        "Registrati": "",
        "Logs": "",
        "Magazzino": "",
        "Coupons": ""
    },
    "translation": {
        "Traduci da Italiano": "Traduci da Italiano"
    },
    "pages": {
        "Aggiungi riga": "追加のライン",
        "Breve testo descrittivo": "短い説明テキスト",
        "Livello": "レベル",
        "Menu TOP": "Menu TOP",
        "Menu TOP (2)": "Menu TOP (2)",
        "Menu Medio": "Menu Medio",
        "Menu Footer": "Menu Footer",
        "Menu Footer (2)": "Menu Footer (2)",
        "Pagine attive pubbliche": "活動ページを公開",
        "News attive": "ニュースで活躍",
        "Salva disposizione": "Save disposition"
    },
    "operatori": {
        "Amministrazione": "管理"
    },
    "newscat": {
        "Categorie News": "カテゴリのニュース",
        "Nome categoria": "カテゴリ名",
        "Sottotitolo": "字幕",
        "Testo": "テキスト",
        "Categoria Attiva": "カテゴリーで活躍"
    },
    "news_cat": [],
    "vetrina": {
        "Categoria": "カテゴリ",
        "Categoria non trovata": "カテゴリ見つかりませんで"
    },
    "news": {
        "Abilitata": "有効な",
        "Attiva sul lato pubblico": "活躍するパブリック-サイド",
        "Dati strutturali": "構造データ",
        "Gestione News": "経営ニュース",
        "Titolo": "タイトル",
        "Sottotitolo": "字幕",
        "Immagine Header": "ヘッダー画像",
        "Elimina riga": "行の削除",
        "Testo alt": "のaltテキスト",
        "Immagine elenchi": "画像リスト",
        "Testo alt immagini": "Altテキスト画像",
        "Data pubblicazione": "出版日",
        "Categoria principale": "メインカテゴリ",
        "Categorie Aggiuntive": "追加カテゴリ",
        "Aggiungi riga": "追加のライン",
        "Gestione commenti": "管理コメント",
        "Torna ai commenti": "のコメント",
        "Commenti abilitati": "コメントを有効に"
    },
    "news_categories": [],
    "dashboard": {
        "Notizie da Webtek": "ニュースからWebtek",
        "Offerte Webtek": "Offerte Webtek",
        "Contatta": "連絡先"
    },
    "settings": {
        "Chiave Yandex per traduzioni": "Chiave Yandex per traduzioni",
        "Categorie attivabili blog": "Categorie attivabili blog",
        "Url RSS notizie dashboard": "Url RSS notizie dashboard",
        "Url RSS offerte": "Url RSS offerte",
        "Token Sentry": "Token Sentry",
        "Codice univoco sentry": "Codice univoco sentry",
        "Impostazioni ADMIN": "の管理者の設定",
        "Dimensioni immagini header (NEWS)": "Dimensioni immagini header",
        "Dimensioni immagini elenchi (NEWS)": "Dimensioni immagini elenchi (NEWS)",
        "Layout selezionato": "Layout selezionato",
        "Aiuto": "助",
        "Etichetta": "ラベル",
        "Chiave univoca": "ユニークキー",
        "Valore": "値",
        "Ruolo richiesto": "役割",
        "Tipo di dato": "データタイプ"
    },
    "layoutcontent": {
        "Gestione Blocchi": "Gestione Blocchi",
        "Dati dipendenti dalla lingua di navigazione": "Dati dipendenti dalla lingua di navigazione",
        "Dati indipendenti dalla lingua di navigazione": "Dati indipendenti dalla lingua di navigazione",
        "Aspetto grafico": "グラフィック登場",
        "Anteprima": "プレビュー",
        "SCSS": "SCSS",
        "Twig": "Twig",
        "Ruolo richiesto": "Ruolo richiesto",
        "Impostazioni di sicurezza": "Impostazioni di sicurezza",
        "Aggiungi un nuovo elemento": "新規追加項目"
    },
    "template": {
        "Elenco dei moduli disponibili": "のリストで利用できるモジュール",
        "Ricerca moduli": "研究モジュール",
        "Moduli": "Moduli",
        "Gestione Template": "テンプレート管理",
        "Headers": "Headers",
        "Footers": "Footers",
        "Template di contenuto": "Template di contenuto",
        "Template configurabili": "Template configurabili",
        "Seleziona un template per poter visualizzare l'elenco dei moduli già configurati e per eventualmente rimuoverli o aggiungerne di nuovi.": "Seleziona un template per poter visualizzare l'elenco dei moduli già configurati e per eventualmente rimuoverli o aggiungerne di nuovi.",
        "Moduli template selezionato": "Moduli template selezionato",
        "Moduli caricati nel container ": "Moduli caricati nel container "
    },
    "layout": {
        "variabile attualmente non utilizzata": "variabile attualmente non utilizzata",
        "Utilizzata in: ": "Utilizzata in: ",
        "Gestione colori layout": "Gestione colori layout"
    },
    "categorie_prodotti": {
        "Gestione Categorie": "",
        "Elenco Categorie": "",
        "Dettaglio categoria seleziionata": "",
        "Descrizione categoria": "",
        "Nuova Categoria": "",
        "Categoria": "",
        "Sottocategoria": "",
        "Nuova sottocategoria": "",
        "Nodo amazon": "ノードのアマゾン",
        "Nome categoria": "カテゴリ名",
        "Sottotitolo": "字幕"
    },
    "cal": {
        "Gennaio": "月",
        "Febbraio": "月",
        "Marzo": "月",
        "Aprile": "月",
        "Maggio": "月",
        "Giugno": "月",
        "Luglio": "月",
        "Agosto": "月",
        "Settembre": "月",
        "Ottobre": "月",
        "Novembre": "月",
        "Dicembre": "月",
        "Lunedì": "月曜日",
        "Martedì": "火曜日",
        "Mercoledì": "水曜日",
        "Giovedì": "木曜日",
        "Venerdì": "金曜日",
        "Sabato": "土曜日",
        "Domenica": "日曜日",
        "Gen": "",
        "Feb": "",
        "Mar": "",
        "Apr": "",
        "Mag": "",
        "Giu": "",
        "Lug": "",
        "Ago": "",
        "Set": "",
        "Ott": "",
        "Nov": "",
        "Dic": ""
    },
    "vetrinacat": [],
    "form_contatti": {
        "Indirizzo email fornito vuoto oppure in un formato non valido": "Eメールアドレスが空である、または無効な形式です。"
    },
    "setup_ecommerce": {
        "Impostazioni Ecommerce": "",
        "Ragione Sociale": "",
        "P.IVA": "",
        "Indirizzo": "",
        "CAP": "",
        "Città": "",
        "Provincia": "",
        "Nazione": "",
        "Telefono": "",
        "Fax": "",
        "Sede legale": "",
        "Sede operativa": "",
        "Privacy Policy": "",
        "Condizioni di vendita": "",
        "Impostazioni Generali": "一般設定"
    },
    "gruppi_settaggi": {
        "Gruppi di settaggi": "団体の設定"
    },
    "settings_group": {
        "Nome": "名称"
    },
    "tax_ecommerce": {
        "Tassa Attiva": "料金活動",
        "Codice": "コード",
        "Aliquota": "率",
        "Descrizione": "説明",
        "Tassa": "料金"
    },
    "metodi_pagamento_ecommerce": {
        "Metodi Pagamento": "お支払い方法",
        "Metodo Attivo": "活動方法",
        "Nome Controller ": "コントローラ名 ",
        "Nome": "名称",
        "Descrizione": "説明",
        "Debug": "デバッグ",
        "Codice": "コード",
        "Dati per la connessione al gateway": "データへの接続ゲートウェイ",
        "Client ID": "クライアントID"
    },
    "categoria_tag": {
        "Gestione categorie tag": "カテゴリ管理サービス",
        "Categoria attiva": "カテゴリーで活躍",
        "Nome categoria": "カテゴリ名"
    },
    "appartamenti": {
        "Gestione appartamenti": "経営のアパート",
        "Appartamento pubblicato": "掲載",
        "Comincia a digitare le prime lettere di un servizio disponibile per estrarre un elenco di servizi che soddisfino la ricerca": "文字列を入力の最初の数文字のサービスのエキスのリストを満足いただけるコンサルティングの研究",
        "Tag da aggiungere": "サービス追加",
        "Tag": "サービス",
        "Prezzo": "",
        "Superficie": ""
    },
    "metodi_spedizione_ecommerce": {
        "Attivo": "活動",
        "Nome": "",
        "Tipo Pagamento": "",
        "Descrizione": "",
        "Valido Per": "",
        "Per Order": "",
        "Per quantità": "",
        "Tutte le nazioni": "",
        "Valido Per le nazioni specificate": "",
        "Metodi Spedizione": ""
    },
    "ecommerce": {
        "Gestione Prezzi": "",
        "Prezzo": "",
        "Dati Fatturazione": "",
        "Nome": "",
        "Indirizzo": "",
        "Cap": "",
        "Città": "",
        "Provincia": "",
        "Nazione": "",
        "Telefono": "",
        "Il prodotto richiesto non è stato trovato": "のに必要な製品は見つかりませんでした",
        "Imponibile": "課税所得の",
        "Dati Anagrafici": "人口データ",
        "continua &raquo;": "continue &raquo;",
        "esci": "口",
        "Indirizzo di consegna": "配送先",
        "Metodo di consegna": "配送方法",
        "Metodo di pagamento": "お支払い方法",
        "Contenuto del carrello": "の内容をカート",
        "numero di prodotti": "商品数",
        "Mostra dettaglio": "ショーの詳細",
        "Totale": "合計",
        "Descrizione": "説明",
        "Q.tà": "Qty",
        "Subtotale": "小計",
        "Se vuoi puoi effettuare ora il pagamento usando questo link: ": "したい場合はできる時間でのお支払いこのリンク: ",
        "Paga ora": "今すぐ支払い",
        "Modifica i tuoi dati": "",
        "La mia wishlist": "",
        "I miei Ordini": "",
        "rimuovi wishlist": "",
        "Prodotto attualmente non disponibile": "この製品は現在設定はありません",
        "Torna all'elenco": "",
        "Accetto Termini e condizioni. Cliccando su CONCLUDI ORDINE stipuli un contratto legalmente vincolante per acquistare i prodotti": "",
        "Indica qui le note relative al tuo ordine...": "",
        "Inserisci qui le note relative al tuo ordine": "",
        "Registrazione": "",
        "Hai un codice sconto? Inseriscilo qui:": ""
    },
    "anagrafica": {
        "Gestione anagrafiche clienti": "管理お客様のマスターデータ",
        "E' un'azienda": "この会社",
        "Dati anagrafici": "人口データ",
        "Dati accesso": "データアクセス",
        "Indirizzi": "アドレス",
        "Seleziona una provincia": "選択州",
        "Provincia": "地",
        "Comune": "共通",
        "Il campo cognome è troppo corto, dovrebbe essere almeno di {limit} caratteri": "最後のフィールド名フィールドでは短すぎるべき少なくとも{制限}文字",
        "Nome": "名称",
        "Cognome": "姓",
        "C.A.P.": "郵便番号",
        "Indirizzo predefinito": "デフォルトのアドレス",
        "Aggiungi": "追加",
        "Rendi predefinito": "くデフォルト",
        "Elimina questo indirizzo": "このアドレス削除",
        "Modifica": "編集",
        "Indirizzo aggiuntivo ": "追加のアドレス ",
        "C\/O": "C\/O",
        "Utente ospite": "ゲストユーザー",
        "Telefono": "",
        "Cellulare": "",
        "Tipologia": ""
    },
    "tag": {
        "Valore significativo": "大きな価値",
        "Icona": ""
    },
    "help": [],
    "brands": {
        "Nome": "名称",
        "Sottotitolo": "字幕",
        "Testo": "テキスト",
        "Immagine elenchi": "Immagine elenchi",
        "Gestione Marchi": "商標管理"
    },
    "group_attribute": {
        "Tipo": "タイプ",
        "Titolo": "タイトル",
        "Gruppi di Attributi": "属団体",
        "Checkbox \/ Radio": "チェックボックス\/電波",
        "Colore": "色",
        "Select": "Select",
        "Radio box": "ラジオボックス"
    },
    "attribute": {
        "Attributi": "属性",
        "Gruppo di appartenenza": "グループメンバーシップ",
        "Etichetta descrittiva": "ラベルの記述"
    },
    "group_feature": {
        "Chiave": "キー",
        "Etichetta": "ラベル",
        "Gruppi Specifiche": "的として"
    },
    "feature": {
        "Gruppo specifiche": "グループに特",
        "Gestione caratteristiche": "管理機能",
        "Salva disposizione": "場合を除いておいて"
    },
    "product_type": {
        "Famiglie di prodotto": "製品ファミリー",
        "Nome": "名称",
        "Gruppi assegnati": "属団体"
    },
    "delivery": {
        "Dati anagrafici": "人口データ",
        "Ragione Sociale": "その理由社会",
        "Telefono": "電話",
        "Indirizzo": "住所",
        "Cap": "Cap",
        "Comune": "Comune",
        "Provincia": "Provincia",
        "Nazione": "Country",
        "Vettori": "Vettori",
        "Nome": "名称",
        "Descrizione": "説明",
        "Soglia minima": "最小限のしきい値",
        "Soglia Massima": "最大値",
        "Costo": "コスト",
        "Valido per tutte le nazioni": "有効なすべての国",
        "Aggiungi nuovo scaglione di prezzi": "Aggiungi nuovo scaglione di prezzi",
        "Fasce di prezzo": "価格帯",
        "Elimina riga": "行の削除"
    },
    "product": {
        "Gestione Prodotti": "管理製品",
        "Dati descrittivi prodotto": "記述データを活用した商品",
        "Dati spedizione": "出荷データ",
        "Dati posizionamento": "データ配置",
        "Codice": "コード",
        "Barcode": "バーコード",
        "Dimensioni prodotto": "製品寸法",
        "Prezzi, Varianti e Giacenze": "価格変異体の在庫",
        "Peso": "重量",
        "Altezza": "高さ",
        "Larghezza": "幅",
        "Corrieri": "宅配便",
        "Spedibile con tutti i corrieri?": "可派遣すべての宅配便か？",
        "Seleziona i corrieri che potranno spedire questo prodotto": "を選択し、荷主できる船舶この製品",
        "Profondità": "深さ",
        "Prezzo netto per \"%listino%\" ": "当期につき価格: \"%listino%\" ",
        "Prezzo ivato per \"%listino%\"": "価格のためのivato %listino%",
        "Varianti prodotto": "製品構成",
        "Attributi": "属性",
        "Nessun attributo selezionato, comincia selezionando gli attributi per creare una variante": "Nessun attributo selezionato, comincia selezionando gli attributi per creare una variante",
        "crea variante": "を変",
        "giacenza": "株式",
        "minima": "最小",
        "descrizione": "説明",
        "Attributi selezionati": "選択した属性",
        "Varianti presenti": "異明",
        "Nessuna variante presente": "なvariant現在",
        "Foto": "写真",
        "impatto prezzo": "価格の影響",
        "Comportamento offline": "オフラインの挙動",
        "Destinazione redirect": "先にリダイレクト",
        "Dati strutturali": "構造データ",
        "Traduzioni": "翻訳",
        "301 verso prodotto": "301製品",
        "301 verso categoria": "301カテゴリ",
        "302 verso categoria": "302に向けてカテゴリ",
        "Errore 404": "Error404",
        "302 verso prodotto": "302に向けての製品",
        "Caratteristiche prodotto": "製品の特徴",
        "Caratteristica": "特徴",
        "Valori predefiniti": "デフォルト値",
        "Valore testuale": "テキスト値",
        "Aggiungi caratteristica": "追加機能",
        "Nessuna caratteristica presente": "な特徴",
        "Seleziona la tipologia di caratteristica": "データのダウンロードはこの特徴",
        "Dati prezzi": "データ価格",
        "Immagine per elenchi": "イメージリスト",
        "Nome prodotto": "製品名",
        "Codice EAN": "Codice EAN",
        "Codice produttore": "メーカーコード",
        "Prezzi": "価格",
        "Quantità minima": "最小数量",
        "Multipli": "複数の",
        "Quantità": "金額",
        "Categoria": "カテゴリ",
        "Tags": "タグ",
        "Prodotti correlati": "関連製品",
        "Prezzo netto": "当期純物価",
        "Prezzo ivato": "価格ivato",
        "Market places": "市場",
        "Prezzo dipende da variante": "価格に依存して変異体",
        "attuale": "現在の",
        "Default": "デフォルト",
        "Magazzino": "倉庫",
        "La combinazione scelta non è a magazzino": "の組み合わせに選ばれていない株式",
        "Quantità richiesta": "必要量",
        "Prezzo civetta": "",
        "Quantità a magazzino non sufficiente": ""
    },
    "listino": {
        "Default": "デフォルト",
        "Gestione listini": "価格表管理",
        "Nome listino": "名前一覧"
    },
    "products": {
        "Impatto prezzo": "価格の影響",
        "Disponibile": "ご用意",
        "Tipo prodotto": "製品のタイプ",
        "Sempre disponibile": "常に使用可能"
    },
    "market_place": {
        "Market place": "市場",
        "Nome": "名称"
    },
    "meta": {
        "Chiave univoca": "ユニークキー",
        "Etichetta": "ラベル",
        "Help in linea": "オンライン-ヘルプ",
        "Valore": "値"
    },
    "gruppi_meta": {
        "Gruppi meta tags": "Gruppi meta tags"
    },
    "meta_group": {
        "Nome": "名称"
    },
    "contact": {
        "Abbiamo ricevuto il tuo messaggio, ti risponderemo al più presto.": "届いたメッセージで、以降の回答となりますのでご了承お早めに"
    },
    "orders_status": {
        "Stati ordine": "国順",
        "Codice": "コード",
        "Titolo": "タイトル"
    },
    "order": {
        "Ordini ecommerce": "Ec順",
        "Dettaglio ordine": "注文詳細",
        "Pagato": "の支払額",
        "Stato ordine": "状況の確認",
        "Somma pagata": "子どもが生まれたときの給付",
        "Rif. transazione": "Rif. transazione",
        "Metodo di pagamento": "お支払い方法",
        "Statistiche": "統計",
        "Statistiche per anno": "統計年",
        "Statistiche per mese": "統計月",
        "Tracking Url": ""
    },
    "email_template": {
        "Gestione Emails": "メールの管理",
        "Variabili": "変数",
        "Codice": "コード",
        "Oggetto": "オブジェクト",
        "Testo": "テキスト"
    },
    "filemanager": {
        "Filemanager": "Filemanager",
        "Elenco files": "ファイルリスト",
        "Albero directory": "ディレクトリツリー",
        "La directory non può essere cancellata": "ディレクトリの削除はできません"
    },
    "modelli_nuovo": {
        "Codice": "コード",
        "Prezzo": "価格",
        "Design": "Design",
        "Caratteristiche": "特徴",
        "Prestazioni": "性能"
    },
    "dipendenti": {
        "Nome": "名称",
        "Cognome": "姓",
        "Email": "メール",
        "Telefono": "電話",
        "Profilo facebook": "プFacebook",
        "Profilo twitter": "Twitterプロフィール",
        "Profilo linkedIn": "LinkedInプ",
        "Profilo google plus": "Googleプラス概要",
        "Titolo": "タイトル",
        "Mansione": "ジョブ",
        "Descrizione": "説明",
        "Gestione Dipendenti": "管理社員"
    },
    "gallerycat": {
        "Template": "テンプレート",
        "Titolo": "タイトル",
        "Sottotitolo": "字幕",
        "Testo": "テキスト",
        "Categorie Gallery": "カテゴリギャラリー"
    },
    "gallery": {
        "Titolo": "タイトル",
        "Sottotitolo": "字幕",
        "Testo": "テキスト",
        "Titolo in lingua vuoto": "言語のタイトルの空白",
        "Gestione Gallery": "経営ギャラリー",
        "Immagini": "画像",
        "Puoi caricare nuove immagini effettuando il drag & drop direttamente dal tuo computer.<br\/><br\/> \n\nIn alternativa puoi cliccare in qualsiasi parte bianca del riquadro di sinistra.<br\/><br\/> \n\nSe il riquandro contiene già delle immagini queste avranno 2 bottoni associati, uno per la modifica della descrizione e del testo alternativo della foto, l'altro per la cancellazione.<br\/><br\/> \n\nPer ogni immagine comparirà nell'angolo in alto a destra il seguente simbolo: <i class=\"fa fa-arrows-alt\"><\/i>, cliccando con il tasto sinistro del mouse su questo simbolo e trascinando la foto sarà possibile modificare l'ordine di apparizione delle foto all'interno della gallery.<br\/><br\/> \n\nAlcune foto potrebbero riportare il seguente simbolo <i class=\"fa fa-exclamation-triangle\"><\/i>, questo simbolo sta a indicare che per la foto corrispondente non è stato specificato un testo alternativo che serve ai motori di ricerca per indicizzare la foto. Cliccando sul bottone di edit sarà possibile aggiungere i dati mancnati. \n<br\/><br\/> ": "Você pode fazer o upload de novas imagens por meio de arrastar e soltar diretamente do seu computador.<br \/>\n\nAlternativamente, você pode clicar em qualquer parte branca do painel esquerdo. <br \/>\n\nSe o riquandro já contém imagens, estas terão 2 botões associado a ele, um para editar a descrição e o texto alternativo das fotos, outro para cancelamento. <br \/>\n\nPara cada imagem será exibida no canto superior direito com o seguinte símbolo: <i class=",
        "Elenco immagini": "リスト画像",
        "Aiuto": "助",
        "Testo alternativo": "代替テキスト",
        "Descrizione": "説明"
    },
    "offerte": {
        "Offerte": ""
    },
    "groups": {
        "Gestione gruppi": "",
        "Nome gruppo": ""
    },
    "group": {
        "Ruoli abilitati": ""
    },
    "orders": {
        "Dati Fatturazione": "",
        "Dati di consegna": "",
        "Dati di contatto": ""
    },
    "coupon": {
        "Codice": "",
        "Tipo": "",
        "Valore": "",
        "Illimitato": "",
        "Ordine minimo": "",
        "Attivo per tutti i prodotti": "",
        "Descrizione": "",
        "Gestione Coupons": "",
        "Valore in EURO": "",
        "Percentuale": "",
        "Numero di coupon disponibili": ""
    }
};