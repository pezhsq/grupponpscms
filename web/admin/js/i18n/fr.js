var Locale = {
    "languages": [],
    "default": {
        "Abilitato": "Activé",
        "Allegati": "Les pièces jointes",
        "Chiudi": "Chiudi",
        "eliminata": "eliminata",
        "Amministrazione": "L'Administration",
        "Testo alternativo": "Le texte de remplacement",
        "Attenzione": "Attention",
        "creata": "creata",
        "Bentornato": "Bienvenue",
        "Categorie News": "Catégories Actualités",
        "Dati strutturali": "Données structurelles",
        "MetaTag Title": "MetaTag Title",
        "MetaTag Description": "MetaTag Description",
        "Slug": "Slug",
        "Gestione Articoli Blog": "Gestion Des Articles De Blog",
        "Leggi di più": "Lire plus",
        "Modifica ordine": "La modification de la commande",
        "Impostazioni SITO": "Impostazioni SITO",
        "Blocchi di contenuto": "Blocchi di contenuto",
        "Gestione Template": "Gestione Template",
        "Gestione Layout": "Structure De La Gestion Des",
        "Categorie": "",
        "Inserisci i campi mancanti": "",
        "Effettua una selezione": "Faire une sélection",
        "Parametri errati": "Des paramètres incorrects",
        "scritto da ": "écrit par ",
        "Impostazioni Generali": "Paramètres Généraux",
        "Gruppi Settaggi CMS": "",
        "Tasse": "Les Taxes",
        "Metodi Pagamento": "Les Méthodes De Paiement",
        "Categorie servizi": "Catégories de services",
        "Sfoglia": "Parcourir",
        "Tag": "Services",
        "Appartamenti": "Appartements",
        "Immagine per l'header": "Immagine per l'header",
        "Immagini per elenchi": "Des Images pour les listes",
        "Metodi Spedizione": "Méthodes D'Expédition",
        "Ecommerce": "Ecommerce",
        "Anagrafiche clienti": "Données de base des clients",
        "Categorie di Tag": "Les Catégories Les Tags",
        "Tags": "Tags",
        "Marchi": "Marques",
        "Pubblico \/ Abilitato \/ Visibile": "Public \/ Activé \/ Visible",
        "Gruppi Attributi": "Les Groupes D'Attributs",
        "Attributi ": "Attributs ",
        "Gruppi Caratteristiche": "Les Groupes Caractéristiques",
        "Caratteristiche": "Caractéristiques",
        "Prodotti": "Produits",
        "Famiglie di prodotti": "Les familles de produits",
        "Vettori": "Vettori",
        "Listini prezzi": "Les listes de prix",
        "Market Places": "Les Places De Marché",
        "Meta di default": "Meta par défaut",
        "Gruppi di meta": "Gruppi di meta",
        "pagina": "page",
        "Stati ordine": "Ordre des états",
        "non trovato": "pas trouvé",
        "Gestione ordini": "Gestion des commandes",
        "Modelli email": "Modèles d'e-mail",
        "File manager": "Gestionnaire de fichiers",
        "Iperauto": "Iperauto",
        "Modelli Auto": "Les Modèles De Voiture",
        "Gestione dipendenti": "Les employés de la direction",
        "Categorie Gallery": "Les Catégories De La Galerie",
        "Gallery": "Galerie",
        "Categorie Offerte": "",
        "Offerte": "",
        "Gestione Offerte": "",
        "Gestione Gruppi ": "",
        "Azienda": "",
        "Sedi": "",
        "Privato": "",
        "Registrati": "",
        "Logs": "",
        "Magazzino": "",
        "Coupons": ""
    },
    "translation": {
        "Traduci da Italiano": "Traduci da Italiano"
    },
    "pages": {
        "Aggiungi riga": "Ajouter la ligne",
        "Livello": "Niveau",
        "Menu TOP": "Menu TOP",
        "Menu TOP (2)": "Menu TOP (2)",
        "Menu Medio": "Menu Medio",
        "Menu Footer": "Menu Footer",
        "Menu Footer (2)": "Menu Footer (2)",
        "Pagine attive pubbliche": "Pages actives public",
        "News attive": "Des nouvelles active",
        "Salva disposizione": "Save disposition"
    },
    "operatori": {
        "Amministrazione": "L'Administration"
    },
    "newscat": {
        "Categorie News": "Catégories Actualités",
        "Nome categoria": "Nom de la catégorie",
        "Sottotitolo": "Sous-titres",
        "Testo": "Texte",
        "Categoria Attiva": "Catégorie Active"
    },
    "news_cat": [],
    "vetrina": {
        "Categoria": "Catégorie",
        "Categoria non trovata": "Catégorie introuvable"
    },
    "news": {
        "Abilitata": "Activé",
        "Attiva sul lato pubblico": "Actif sur le côté public",
        "Dati strutturali": "Données structurelles",
        "Gestione News": "Gestion Des News",
        "Titolo": "Titre",
        "Sottotitolo": "Sous-titres",
        "Immagine Header": "Image D'En-Tête",
        "Elimina riga": "Supprimer la ligne",
        "Testo alt": "Le texte alt",
        "Immagine elenchi": "Listes d'images",
        "Testo alt immagini": "Le texte Alt des images",
        "Data pubblicazione": "Date de Publication",
        "Categoria principale": "Catégorie principale",
        "Categorie Aggiuntive": "D'Autres Catégories",
        "Aggiungi riga": "Ajouter la ligne",
        "Gestione commenti": "Commentaires de la direction",
        "Torna ai commenti": "En arrière pour les commentaires",
        "Commenti abilitati": "Commentaires activé"
    },
    "news_categories": [],
    "dashboard": {
        "Notizie da Webtek": "Des nouvelles de Webtek",
        "Offerte Webtek": "Offerte Webtek",
        "Contatta": "Contact"
    },
    "settings": {
        "Chiave Yandex per traduzioni": "Chiave Yandex per traduzioni",
        "Categorie attivabili blog": "Categorie attivabili blog",
        "Url RSS notizie dashboard": "Url RSS notizie dashboard",
        "Url RSS offerte": "Url RSS offerte",
        "Token Sentry": "Token Sentry",
        "Codice univoco sentry": "Codice univoco sentry",
        "Impostazioni ADMIN": "Les paramètres ADMINISTRATEUR",
        "Dimensioni immagini header (NEWS)": "Dimensioni immagini header",
        "Dimensioni immagini elenchi (NEWS)": "Dimensioni immagini elenchi (NEWS)",
        "Layout selezionato": "Layout selezionato",
        "Aiuto": "Aider",
        "Etichetta": "Étiquette",
        "Chiave univoca": "Clé Unique",
        "Valore": "Valeur",
        "Ruolo richiesto": "Rôle nécessaire",
        "Tipo di dato": "Type de données"
    },
    "layoutcontent": {
        "Gestione Blocchi": "Gestione Blocchi",
        "Dati dipendenti dalla lingua di navigazione": "Dati dipendenti dalla lingua di navigazione",
        "Dati indipendenti dalla lingua di navigazione": "Dati indipendenti dalla lingua di navigazione",
        "Aspetto grafico": "Aspect graphique",
        "Anteprima": "Aperçu",
        "SCSS": "SCSS",
        "Twig": "Twig",
        "Ruolo richiesto": "Ruolo richiesto",
        "Impostazioni di sicurezza": "Impostazioni di sicurezza",
        "Aggiungi un nuovo elemento": "Ajouter un nouvel élément"
    },
    "template": {
        "Elenco dei moduli disponibili": "Une liste des modules disponibles",
        "Ricerca moduli": "Modules de recherche",
        "Moduli": "Moduli",
        "Gestione Template": "Modèle De Gestion De La",
        "Headers": "Headers",
        "Footers": "Footers",
        "Template di contenuto": "Template di contenuto",
        "Template configurabili": "Template configurabili",
        "Seleziona un template per poter visualizzare l'elenco dei moduli già configurati e per eventualmente rimuoverli o aggiungerne di nuovi.": "Seleziona un template per poter visualizzare l'elenco dei moduli già configurati e per eventualmente rimuoverli o aggiungerne di nuovi.",
        "Moduli template selezionato": "Moduli template selezionato",
        "Moduli caricati nel container ": "Moduli caricati nel container "
    },
    "layout": {
        "variabile attualmente non utilizzata": "variabile attualmente non utilizzata",
        "Utilizzata in: ": "Utilizzata in: ",
        "Gestione colori layout": "Gestione colori layout"
    },
    "categorie_prodotti": {
        "Gestione Categorie": "",
        "Elenco Categorie": "",
        "Dettaglio categoria seleziionata": "",
        "Descrizione categoria": "",
        "Nuova Categoria": "",
        "Categoria": "",
        "Sottocategoria": "",
        "Nuova sottocategoria": "",
        "Nodo amazon": "Nœud amazon",
        "Nome categoria": "Nom de la catégorie",
        "Sottotitolo": "Sous-titres"
    },
    "cal": {
        "Gennaio": "Janvier",
        "Febbraio": "Février",
        "Marzo": "Mars",
        "Aprile": "Avril",
        "Maggio": "Peut",
        "Giugno": "Juin",
        "Luglio": "Juillet",
        "Agosto": "Août",
        "Settembre": "Septembre",
        "Ottobre": "Octobre",
        "Novembre": "Novembre",
        "Dicembre": "Décembre",
        "Lunedì": "Lundi",
        "Martedì": "Mardi",
        "Mercoledì": "Mercredi",
        "Giovedì": "Jeudi",
        "Venerdì": "Vendredi",
        "Sabato": "Samedi",
        "Domenica": "Dimanche",
        "Gen": "",
        "Feb": "",
        "Mar": "",
        "Apr": "",
        "Mag": "",
        "Giu": "",
        "Lug": "",
        "Ago": "",
        "Set": "",
        "Ott": "",
        "Nov": "",
        "Dic": ""
    },
    "vetrinacat": [],
    "form_contatti": {
        "Indirizzo email fornito vuoto oppure in un formato non valido": "Adresse E-mail est vide ou dans un format non valide"
    },
    "setup_ecommerce": {
        "Impostazioni Ecommerce": "",
        "Ragione Sociale": "",
        "P.IVA": "",
        "Indirizzo": "",
        "CAP": "",
        "Città": "",
        "Provincia": "",
        "Nazione": "",
        "Telefono": "",
        "Fax": "",
        "Sede legale": "",
        "Sede operativa": "",
        "Privacy Policy": "",
        "Condizioni di vendita": "",
        "Impostazioni Generali": "Paramètres Généraux"
    },
    "gruppi_settaggi": {
        "Gruppi di settaggi": "Les groupes de paramètres"
    },
    "settings_group": {
        "Nome": "Nom"
    },
    "tax_ecommerce": {
        "Tassa Attiva": "Frais D'Actif",
        "Codice": "Code",
        "Aliquota": "Taux de",
        "Descrizione": "Description",
        "Tassa": "Frais"
    },
    "metodi_pagamento_ecommerce": {
        "Metodi Pagamento": "Les Méthodes De Paiement",
        "Metodo Attivo": "Méthode Active",
        "Nome Controller ": "Nom Du Contrôleur De ",
        "Nome": "Nom",
        "Descrizione": "Description",
        "Debug": "Debug",
        "Codice": "Code",
        "Dati per la connessione al gateway": "Les données pour la connexion à la passerelle",
        "Client ID": "ID de Client"
    },
    "categoria_tag": {
        "Gestione categorie tag": "Les catégories de services de gestion de",
        "Categoria attiva": "Catégorie active",
        "Nome categoria": "Nom de la catégorie"
    },
    "appartamenti": {
        "Gestione appartamenti": "La gestion des appartements",
        "Appartamento pubblicato": "Publié",
        "Comincia a digitare le prime lettere di un servizio disponibile per estrarre un elenco di servizi che soddisfino la ricerca": "Commencez à taper les premières lettres d'un service disponible pour extraire une liste de services répondant à la recherche",
        "Tag da aggiungere": "Services à ajouter",
        "Tag": "Services",
        "Prezzo": "",
        "Superficie": ""
    },
    "metodi_spedizione_ecommerce": {
        "Attivo": "Active",
        "Nome": "",
        "Tipo Pagamento": "",
        "Descrizione": "",
        "Valido Per": "",
        "Per Order": "",
        "Per quantità": "",
        "Tutte le nazioni": "",
        "Valido Per le nazioni specificate": "",
        "Metodi Spedizione": ""
    },
    "ecommerce": {
        "Gestione Prezzi": "",
        "Prezzo": "",
        "Dati Fatturazione": "",
        "Nome": "",
        "Indirizzo": "",
        "Cap": "",
        "Città": "",
        "Provincia": "",
        "Nazione": "",
        "Telefono": "",
        "Il prodotto richiesto non è stato trovato": "Le produit n'a pas été trouvé",
        "Imponibile": "Imposable",
        "Dati Anagrafici": "Les Données De Population",
        "continua &raquo;": "continuer &raquo;",
        "esci": "sortie",
        "Indirizzo di consegna": "L'adresse de livraison",
        "Metodo di consegna": "Méthode de livraison",
        "Metodo di pagamento": "Méthode de paiement",
        "Contenuto del carrello": "Le contenu du panier",
        "numero di prodotti": "nombre de produits",
        "Mostra dettaglio": "Montrer les détails",
        "Totale": "Total",
        "Descrizione": "Description",
        "Q.tà": "Qty",
        "Subtotale": "Sous-total",
        "Se vuoi puoi effettuare ora il pagamento usando questo link: ": "Si vous le désirez, vous pouvez prendre le temps de paiement à l'aide de ce lien: ",
        "Paga ora": "Payer maintenant",
        "Modifica i tuoi dati": "",
        "La mia wishlist": "",
        "I miei Ordini": "",
        "rimuovi wishlist": "",
        "Prodotto attualmente non disponibile": "Le produit n'est pas disponible actuellement",
        "Torna all'elenco": "",
        "Accetto Termini e condizioni. Cliccando su CONCLUDI ORDINE stipuli un contratto legalmente vincolante per acquistare i prodotti": "",
        "Indica qui le note relative al tuo ordine...": "",
        "Inserisci qui le note relative al tuo ordine": "",
        "Registrazione": "",
        "Hai un codice sconto? Inseriscilo qui:": ""
    },
    "anagrafica": {
        "Gestione anagrafiche clienti": "La gestion des données de base clients",
        "E' un'azienda": "C'est une société",
        "Dati anagrafici": "Les données de Population",
        "Dati accesso": "L'accès aux données",
        "Indirizzi": "Adresses",
        "Seleziona una provincia": "Sélectionnez une province",
        "Provincia": "La province",
        "Comune": "Commune",
        "Il campo cognome è troppo corto, dovrebbe essere almeno di {limit} caratteri": "Le nom de champ est trop court, devrait être au moins de {limit} caractères",
        "Nome": "Nom",
        "Cognome": "Nom de famille",
        "C.A.P.": "Code POSTAL",
        "Indirizzo predefinito": "L'adresse par défaut",
        "Aggiungi": "Ajouter",
        "Rendi predefinito": "Par défaut",
        "Elimina questo indirizzo": "Supprimer cette adresse",
        "Modifica": "Modifier",
        "Indirizzo aggiuntivo ": "Adresse supplémentaire ",
        "C\/O": "C\/O",
        "Utente ospite": "Utilisateur invité",
        "Telefono": "",
        "Cellulare": "",
        "Tipologia": ""
    },
    "tag": {
        "Valore significativo": "Une valeur importante",
        "Icona": ""
    },
    "help": [],
    "brands": {
        "Nome": "Nom",
        "Sottotitolo": "Sous-titres",
        "Testo": "Texte",
        "Immagine elenchi": "Immagine elenchi",
        "Gestione Marchi": "Gestion Des Marques"
    },
    "group_attribute": {
        "Tipo": "Type",
        "Titolo": "Titre",
        "Gruppi di Attributi": "Les groupes d'attributs",
        "Checkbox \/ Radio": "Case À Cocher \/ Radio",
        "Colore": "Couleur",
        "Select": "Select",
        "Radio box": "Boîte Radio"
    },
    "attribute": {
        "Attributi": "Attributs",
        "Gruppo di appartenenza": "L'appartenance à un groupe",
        "Etichetta descrittiva": "Étiquette Descriptive"
    },
    "group_feature": {
        "Chiave": "Clé",
        "Etichetta": "Étiquette",
        "Gruppi Specifiche": "Des Groupes Spécifiques"
    },
    "feature": {
        "Gruppo specifiche": "Groupe spécifique",
        "Gestione caratteristiche": "Fonctions de gestion de l'",
        "Salva disposizione": "Sauf tel que prévu dans"
    },
    "product_type": {
        "Famiglie di prodotto": "Familles de produits",
        "Nome": "Nom",
        "Gruppi assegnati": "Affectés à des groupes"
    },
    "delivery": {
        "Dati anagrafici": "Les données de Population",
        "Ragione Sociale": "Raison Sociale",
        "Telefono": "Téléphone",
        "Indirizzo": "Adresse",
        "Cap": "Cap",
        "Comune": "Comune",
        "Provincia": "Provincia",
        "Nazione": "Country",
        "Vettori": "Vettori",
        "Nome": "Nom",
        "Descrizione": "Description",
        "Soglia minima": "Seuil Minimum",
        "Soglia Massima": "Seuil Maximum",
        "Costo": "Coût",
        "Valido per tutte le nazioni": "Valable pour toutes les nations",
        "Aggiungi nuovo scaglione di prezzi": "Aggiungi nuovo scaglione di prezzi",
        "Fasce di prezzo": "Les gammes de prix",
        "Elimina riga": "Supprimer la ligne"
    },
    "product": {
        "Gestione Prodotti": "Produits De Gestion De",
        "Dati descrittivi prodotto": "Descriptif produit de données",
        "Dati spedizione": "Données d'expédition",
        "Dati posizionamento": "Le placement des données",
        "Codice": "Code",
        "Barcode": "Code-barres",
        "Dimensioni prodotto": "Dimensions du produit",
        "Prezzi, Varianti e Giacenze": "Les prix, les Variantes et les Stocks",
        "Peso": "Poids",
        "Altezza": "Hauteur",
        "Larghezza": "Largeur",
        "Corrieri": "Coursiers",
        "Spedibile con tutti i corrieri?": "Permis expédition avec toutes les passeurs?",
        "Seleziona i corrieri che potranno spedire questo prodotto": "Sélectionnez les expéditeurs qui peut expédier ce produit",
        "Profondità": "Profondeur",
        "Prezzo netto per \"%listino%\" ": "Le prix net \"%listino%\" ",
        "Prezzo ivato per \"%listino%\"": "Prix d'ivato à %listino%",
        "Varianti prodotto": "Les variantes de produit",
        "Attributi": "Attributs",
        "Nessun attributo selezionato, comincia selezionando gli attributi per creare una variante": "Nessun attributo selezionato, comincia selezionando gli attributi per creare una variante",
        "crea variante": "créer variante",
        "giacenza": "Stock",
        "minima": "minimum",
        "descrizione": "description",
        "Attributi selezionati": "Les attributs sélectionnés",
        "Varianti presenti": "Variantes présentes",
        "Nessuna variante presente": "Aucune variante n'présent",
        "Foto": "Photos",
        "impatto prezzo": "prix de l'impact",
        "Comportamento offline": "Comportements hors ligne",
        "Destinazione redirect": "destination de redirection",
        "Dati strutturali": "Données structurelles",
        "Traduzioni": "Traductions",
        "301 verso prodotto": "301 produits",
        "301 verso categoria": "301 catégorie",
        "302 verso categoria": "302 vers la catégorie",
        "Errore 404": "Erreur 404",
        "302 verso prodotto": "302 vers le produit",
        "Caratteristiche prodotto": "Caractéristiques du produit",
        "Caratteristica": "Fonctionnalité",
        "Valori predefiniti": "Les valeurs par défaut",
        "Valore testuale": "Valeur de texte",
        "Aggiungi caratteristica": "Ajouter caractéristique",
        "Nessuna caratteristica presente": "Pas de cette fonctionnalité",
        "Seleziona la tipologia di caratteristica": "Sélectionnez le type de fonction",
        "Dati prezzi": "Le prix de la Data",
        "Immagine per elenchi": "Image pour les listes",
        "Nome prodotto": "Nom du produit",
        "Codice EAN": "Codice EAN",
        "Codice produttore": "Fabricant code",
        "Prezzi": "Prix",
        "Quantità minima": "Quantité minimale",
        "Multipli": "Plusieurs",
        "Quantità": "Montant",
        "Categoria": "Catégorie",
        "Tags": "Tags",
        "Prodotti correlati": "Produits connexes",
        "Prezzo netto": "Prix Net",
        "Prezzo ivato": "Prix d'ivato",
        "Market places": "Les Places De Marché",
        "Prezzo dipende da variante": "Le prix dépend de la variante",
        "attuale": "actuel",
        "Default": "Par défaut",
        "Magazzino": "Entrepôt",
        "La combinazione scelta non è a magazzino": "La combinaison choisie n'est pas en stock",
        "Quantità richiesta": "Quantité requise",
        "Prezzo civetta": "",
        "Quantità a magazzino non sufficiente": ""
    },
    "listino": {
        "Default": "Par défaut",
        "Gestione listini": "Gestion des listes de prix",
        "Nome listino": "Nom de la liste"
    },
    "products": {
        "Impatto prezzo": "Prix de l'impact",
        "Disponibile": "Disponible",
        "Tipo prodotto": "Type de produit",
        "Sempre disponibile": "Toujours disponible"
    },
    "market_place": {
        "Market place": "Place du marché",
        "Nome": "Nom"
    },
    "meta": {
        "Chiave univoca": "Clé Unique",
        "Etichetta": "Étiquette",
        "Help in linea": "L'Aide en ligne",
        "Valore": "Valeur"
    },
    "gruppi_meta": {
        "Gruppi meta tags": "Gruppi meta tags"
    },
    "meta_group": {
        "Nome": "Nom"
    },
    "contact": {
        "Abbiamo ricevuto il tuo messaggio, ti risponderemo al più presto.": "Nous avons reçu votre message, nous vous répondrons dès que possible"
    },
    "orders_status": {
        "Stati ordine": "Ordre des états",
        "Codice": "Code",
        "Titolo": "Titre"
    },
    "order": {
        "Ordini ecommerce": "Le commerce électronique de commande",
        "Dettaglio ordine": "Afin de détail",
        "Pagato": "Payé",
        "Stato ordine": "Statut de la commande",
        "Somma pagata": "Le montant payé",
        "Rif. transazione": "Rif. transazione",
        "Metodo di pagamento": "Méthode de paiement",
        "Statistiche": "Statistiques",
        "Statistiche per anno": "Les statistiques pour l'année",
        "Statistiche per mese": "Statistiques pour le mois",
        "Tracking Url": ""
    },
    "email_template": {
        "Gestione Emails": "La Gestion Des E-Mails",
        "Variabili": "Les Variables",
        "Codice": "Code",
        "Oggetto": "Objet",
        "Testo": "Texte"
    },
    "filemanager": {
        "Filemanager": "Filemanager",
        "Elenco files": "Liste des fichiers",
        "Albero directory": "Arborescence de répertoires",
        "La directory non può essere cancellata": "Le répertoire ne peut pas être supprimé"
    },
    "modelli_nuovo": {
        "Codice": "Code",
        "Prezzo": "Prix",
        "Design": "Design",
        "Caratteristiche": "Caractéristiques",
        "Prestazioni": "Performance"
    },
    "dipendenti": {
        "Nome": "Nom",
        "Cognome": "Prénom",
        "Email": "E-mail",
        "Telefono": "Téléphone",
        "Profilo facebook": "Profil Facebook",
        "Profilo twitter": "Profil Twitter",
        "Profilo linkedIn": "Le profil LinkedIn",
        "Profilo google plus": "Profil Google plus",
        "Titolo": "Titre",
        "Mansione": "Emploi",
        "Descrizione": "Description",
        "Gestione Dipendenti": "Les Employés De La Direction"
    },
    "gallerycat": {
        "Template": "Modèle",
        "Titolo": "Titre",
        "Sottotitolo": "Sous-titres",
        "Testo": "Texte",
        "Categorie Gallery": "Les Catégories De La Galerie"
    },
    "gallery": {
        "Titolo": "Titre",
        "Sottotitolo": "Sous-titres",
        "Testo": "Texte",
        "Titolo in lingua vuoto": "Langue titre vide",
        "Gestione Gallery": "La Gestion De La Galerie",
        "Immagini": "Images",
        "Puoi caricare nuove immagini effettuando il drag & drop direttamente dal tuo computer.<br\/><br\/> \n\nIn alternativa puoi cliccare in qualsiasi parte bianca del riquadro di sinistra.<br\/><br\/> \n\nSe il riquandro contiene già delle immagini queste avranno 2 bottoni associati, uno per la modifica della descrizione e del testo alternativo della foto, l'altro per la cancellazione.<br\/><br\/> \n\nPer ogni immagine comparirà nell'angolo in alto a destra il seguente simbolo: <i class=\"fa fa-arrows-alt\"><\/i>, cliccando con il tasto sinistro del mouse su questo simbolo e trascinando la foto sarà possibile modificare l'ordine di apparizione delle foto all'interno della gallery.<br\/><br\/> \n\nAlcune foto potrebbero riportare il seguente simbolo <i class=\"fa fa-exclamation-triangle\"><\/i>, questo simbolo sta a indicare che per la foto corrispondente non è stato specificato un testo alternativo che serve ai motori di ricerca per indicizzare la foto. Cliccando sul bottone di edit sarà possibile aggiungere i dati mancnati. \n<br\/><br\/> ": "Vous pouvez télécharger de nouvelles images par glisser-déposer directement à partir de votre ordinateur. \n\nSinon, vous pouvez cliquez sur une partie blanche de la le volet de gauche. \n\nSi le riquandro contient déjà des images, il faudra les 2 boutons sont associés, l'un pour modifier la description et le texte de remplacement de photos, l'autre pour l'annulation. \n\nLes photos peuvent être réorganisés avec le drag & drop pour choisir l'ordre d'affichage sur le côté du public.",
        "Elenco immagini": "Liste des images",
        "Aiuto": "Aider",
        "Testo alternativo": "Le texte de remplacement",
        "Descrizione": "Description"
    },
    "offerte": {
        "Offerte": ""
    },
    "groups": {
        "Gestione gruppi": "La gestion des groupes",
        "Nome gruppo": ""
    },
    "group": {
        "Ruoli abilitati": ""
    },
    "orders": {
        "Dati Fatturazione": "",
        "Dati di consegna": "",
        "Dati di contatto": ""
    },
    "coupon": {
        "Codice": "",
        "Tipo": "",
        "Valore": "",
        "Illimitato": "",
        "Ordine minimo": "",
        "Attivo per tutti i prodotti": "",
        "Descrizione": "",
        "Gestione Coupons": "",
        "Valore in EURO": "",
        "Percentuale": "",
        "Numero di coupon disponibili": ""
    }
};