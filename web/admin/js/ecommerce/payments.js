var modulo = 'metodi-pagamento-ecommerce';

$(function() {

    $(".ck").each(function(index, el) {
      attivaCkEditor(el);
    });



  /** Datatable della pagina di elenco */
  if ($('#datatable').length) {

    var cols = [];

    var col = {
      'title': 'Modifica',
      'className': 'dt0 dt-body-center',
      'searchable': false
    };
    cols.push(col);

    col = {
      'title': 'Nome Controller',
      'className': 'dt2',
      searchable: true,
      data: 'nomeController'
    };

    cols.push(col);


    col = {
      'title': 'Nome',
      'className': 'dt6',
      searchable: true,
      data: 'nome'
    };

    cols.push(col);

    col = {
      'title': 'Descrizione',
      'className': 'dt7',
      searchable: true,
      data: 'descrizione'
    };

    cols.push(col);
    col = { 'className': 'dt-body-center' };

    // placeholder cancellazione
    cols.push(col);


    var columnDefs = [];

    var columnDef = {
      targets: 0,
      searchable: false,
      orderable: false,
      className: 'dt-body-center',
      render: function(data, type, full, meta) {
        var toString = full.titolo;
        return '<a href="/admin/' + modulo + '/edit/' + full.id + '" title="Modifica record: ' + toString + '" class="btn btn-xs btn-primary edit"><i class="fa fa-pencil"></i></a>';
      }
    };

    columnDefs.push(columnDef);

    columnDef = {
      targets: cols.length - 1,
      searchable: false,
      className: 'dt-body-center',
      orderable: false,
      render: function(data, type, full, meta) {
        var toString = full.codice;
        var icon = 'check-square-o';
        var title = 'Disabilita: ';
        if (!full.isEnabled) {
          var icon = 'square-o';
          var title = 'Abilita: ';
        }
        var ret = '<a href="/admin/' + modulo + '/toggle-enabled/' + full.id + '" title="' + title + toString + '" class="btn-xs btn btn-info" data-tostring="' + toString + '">';
        ret += '<i class="fa fa-' + icon + '"></i>';
        ret += '</a>';
        return ret;
      }
    };

    columnDefs.push(columnDef);

    $('#datatable').dataTable({
      ajax: {
        "url": "/admin/" + modulo + "/json"
      },
      aaSorting: [[2, 'asc']],
      stateSave: true,
      iDisplayLength: 15,
      responsive: true,
      columns: cols,
      columnDefs: columnDefs,
      createdRow: function(row, data, index) {
        if (data.deleted) {
          $(row).addClass("danger");
        }
      }
    });

  }

});

/** FINE document.ready **/


/**
 * Data una textarea istanzia il CKeditor e ne aggiorna l'altezza in base al contenitore in cui è inserita
 * @param textarea
 */
function attivaCkEditor(textarea) {

  var id = textarea.id;

  if (typeof(CKEDITOR.instances[id]) == 'undefined') {

    CKEDITOR.replace(textarea, {
      customConfig: '/admin/js/pages_ckeditor_' + $body.data('seo') + '.js'
    });

  }

}
