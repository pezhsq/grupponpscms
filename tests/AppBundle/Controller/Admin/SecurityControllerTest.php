<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 14/12/16
 * Time: 16.15
 */

namespace Tests\AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase {

    private $loggedClient = false;
    private $User;

    public function setUp() {

        $this->loggedClient = static::createClient([], [
            'PHP_AUTH_USER' => 'gianiaz',
            'PHP_AUTH_PW' => 'webtek1216;',
        ]);

        $this->loggedClient->followRedirects();

        $this->getFirstOfJson();

    }

    private function getFirstOfJson() {

        $crawler = $this->loggedClient->request('GET', '/admin/operatori/json');

        $this->assertTrue($this->loggedClient->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $this->assertTrue(
            $this->loggedClient->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'mi aspettavo il "Content-Type:application/json"'
        );


        $pageContent = $this->loggedClient->getResponse()->getContent();

        $jsonList = json_decode($pageContent, true);

        $this->User = $jsonList['data'][0];

    }


    /**
     * Test di login corretto.
     *
     * Il test è valido se l'utente si logga e vede il messaggio della dashboard.
     */
    public function testLogin() {

        $client = static::createClient();

        $client->followRedirects();

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($client->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $buttonCrawlerNode = $crawler->selectButton('default.labels.log_in');

        $form = $buttonCrawlerNode->form();

        $data = ['login_form[_username]' => 'gianiaz',
            'login_form[_password]' => 'webtek1216;'];

        $client->submit($form, $data);

        $crawler = $client->getCrawler();

        $content = $client->getResponse()->getContent();

        $this->assertContains('default.labels.scrivania', $content, 'Il login non è andato a buon fine per l\'utente "gianiaz"');

    }

    /**
     * Test di login non corretto.
     *
     * Il test è valido se l'utente vedrà un messaggio che gli indica di aver inserito credenziali errate.
     */
    public function testFailedLogin() {

        $client = static::createClient();

        $client->followRedirects();

        $crawler = $client->request('GET', '/login');

        $this->assertTrue($client->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $buttonCrawlerNode = $crawler->selectButton('default.labels.log_in');

        $form = $buttonCrawlerNode->form();

        $data = ['login_form[_username]' => 'webtek1',
            'login_form[_password]' => 'ciaopepp'];

        $client->submit($form, $data);

        $crawler = $client->getCrawler();

        $content = $client->getResponse()->getContent();

        $this->assertContains('Username could not be found.', $content, 'Il login non è andato a buon fine per l\'utente "gianiaz"');

        $data = ['login_form[_username]' => 'gianiaz',
            'login_form[_password]' => 'passworderrata'];

        $client->submit($form, $data);

        $crawler = $client->getCrawler();

        $content = $client->getResponse()->getContent();

        $this->assertContains('Invalid credentials.', $content, 'Il login non è andato a buon fine per l\'utente "gianiaz"');

    }

    /**
     * Carica la pagina del form per la richiesta di invio email per resettare la pass
     *
     * Il test è valido se l'app ritorna il messaggio di email inviata
     */
    public function testLostPass() {

        $client = static::createClient();

        $client->followRedirects();

        $crawler = $client->request('GET', '/lost-admin-pass');

        $this->assertTrue($client->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $content = $client->getResponse()->getContent();

        $buttonCrawlerNode = $crawler->filter('.submit');

        $form = $buttonCrawlerNode->form();

        $data = ['lost_pass_form[email]' => 'giovanni.lenoci.webtek@gmail.com'];

        $client->submit($form, $data);

        $crawler = $client->getCrawler();

        $content = $client->getResponse()->getContent();

        $search = 'operatori.messages.email_reset_inviata';

        $this->assertContains($search, $content, 'Qualcosa è andato storto nella richiesta pass');
    }

    /**
     * Carica il form di richiesta pass, ma poi inserisce un indirizzo non valido o comunque non presente in database
     * Il test è valido se l'app ritorna un errore perchè non trova l'utente
     */
    public function testFailLostPass() {

        $client = static::createClient();

        $client->followRedirects();

        $crawler = $client->request('GET', '/lost-admin-pass');

        $this->assertTrue($client->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $content = $client->getResponse()->getContent();

        $buttonCrawlerNode = $crawler->filter('.submit');

        $form = $buttonCrawlerNode->form();

        $data = ['lost_pass_form[email]' => 'gianiazgmail.com'];

        $client->submit($form, $data);

        $crawler = $client->getCrawler();

        $content = $client->getResponse()->getContent();

        $search = 'operatori.labels.user_non_trovato';

        $this->assertContains($search, $content, 'Qualcosa è andato storto nella richiesta pass');
    }

    /**
     * Test per il reset della pass, viene caricato l'url con i parametri corretti e reimpostata la pass
     * Il test è valido se la pass viene cambiata correttamente.
     */
    public function testResetPass() {

        $client = static::createClient();

        $client->followRedirects();

        $url = '/reset-pass?uuid=' . $this->User['id'] . '&code=' . md5($this->User['updatedAt']);

        $crawler = $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $buttonCrawlerNode = $crawler->filter('.submit');

        $form = $buttonCrawlerNode->form();

        $data = ['reset_pass_form[plainPassword][first]' => 'webtek',
            'reset_pass_form[plainPassword][second]' => 'webtek'];

        $client->submit($form, $data);

        $crawler = $client->getCrawler();

        $search = 'operatori.messages.password_resettata';

        $content = $client->getResponse()->getContent();

        $this->assertContains($search, $content, 'Qualcosa è andato storto nella richiesta pass');

    }

    /**
     * Carica un url di reset pass con i parametri errati e
     * verifica che venga mostrato il messaggio di errore
     */
    public function testFailedLoadingResetPassForm() {

        $client = static::createClient();

        $client->followRedirects();

        $url = '/reset-pass?uuid=abc&code=fail';

        $crawler = $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $content = $client->getResponse()->getContent();

        $this->assertContains('operatori.errors.url_manomesso', $content, 'Avrei dovuto trovare un errore per codici errati');

    }

    /**
     * Carica l'url di reset pass corretto, ma poi compila con dati errati
     */
    public function testFailedLoadingResetPass() {

        $client = static::createClient();

        $client->followRedirects();

        $url = '/reset-pass?uuid=' . $this->User['id'] . '&code=' . md5($this->User['updatedAt']);

        $crawler = $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $buttonCrawlerNode = $crawler->filter('.submit');

        $form = $buttonCrawlerNode->form();

        $data = ['reset_pass_form[plainPassword][first]' => 'webtek',
            'reset_pass_form[plainPassword][second]' => 'webtek1'];

        $client->submit($form, $data);

        $crawler = $client->getCrawler();

        $content = $client->getResponse()->getContent();

        $search = 'operatori.pass_mismatch';

        $this->assertContains($search, $content, 'Avrei dovuto trovare un errore per password non corrisponenti tra loro');

    }

}