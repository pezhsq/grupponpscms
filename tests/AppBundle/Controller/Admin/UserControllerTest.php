<?php

namespace Tests\AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use AppBundle\Repository\UserRepository;

class UserControllerTest extends WebTestCase
{

    private $loggedClient   = false;
    private $unloggedClient = false;
    private $User;

    public function setUp()
    {

        $this->loggedClient = static::createClient(
            [],
            [
                'PHP_AUTH_USER' => 'gianiaz',
                'PHP_AUTH_PW'   => 'webtek1216;',
            ]
        );

        $this->unloggedClient = static::createClient();

        $this->loggedClient->followRedirects();

        $this->getFirstOfJson();

    }

    public function testAccessDenied()
    {

        $urls = [
            '/admin/operatori/json',
            '/admin/operatori',
            '/admin/operatori/new',
            '/admin/operatori/edit/asdf',
            '/admin/operatori/delete/rtrew',
            '/admin/operatori/toggle-enabled/sda',
        ];

        foreach ($urls as $url) {

            $crawler = $this->unloggedClient->request('GET', $url);

            $this->assertFalse(
                $this->unloggedClient->getResponse()->isSuccessful(),
                'L\'utente non loggato non deve poter accedere a '.$url
            );

        }


    }

    /**
     * Testa il form di creazione nuovo utente (con dati errati)
     */
    public function testNewFail()
    {

        $crawler = $this->loggedClient->request('GET', '/admin/operatori/new');

        $this->assertTrue($this->loggedClient->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $this->assertTrue(
            $this->loggedClient->getResponse()->headers->contains(
                'Content-Type',
                'text/html; charset=UTF-8'
            ),
            'mi aspettavo il "Content-Type:text/html; charset=UTF-8"'
        );


        $buttonCrawlerNode = $crawler->selectButton('default.labels.salva');

        $form = $buttonCrawlerNode->form();

        $data = [
            'user_new_form[nome]'                  => 'Mario',
            'user_new_form[cognome]'               => 'Rossi',
            'user_new_form[email]'                 => 'mario.rossigmail.com',
            'user_new_form[username]'              => 'mrossi',
            'user_new_form[ruolo]'                 => 'ROLE_USER',
            'user_new_form[plainPassword][first]'  => 'webtek',
            'user_new_form[plainPassword][second]' => 'webtek',
            'user_new_form[isEnabled]'             => true,
            'user_new_form[profileText]'           => 'Power ho! hoist to be crushed.',
        ];

        $this->loggedClient->submit($form, $data);


        $crawler = $this->loggedClient->getCrawler();

        $input     = $crawler->filter('#user_new_form_email');
        $container = $input->parents()->first();
        $container = $container->parents()->first();

        $this->assertContains(
            'has-error',
            $container->attr('class'),
            'Il campo email dovrebbe essere segnalato come errato'
        );


    }

    /**
     * Testa il form di creazione nuovo utente (con dati corretti)
     */
    public function testNew()
    {

        $crawler = $this->loggedClient->request('GET', '/admin/operatori/new');

        $this->assertTrue($this->loggedClient->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $this->assertTrue(
            $this->loggedClient->getResponse()->headers->contains(
                'Content-Type',
                'text/html; charset=UTF-8'
            ),
            'mi aspettavo il "Content-Type:text/html; charset=UTF-8"'
        );


        $buttonCrawlerNode = $crawler->selectButton('default.labels.salva');

        $form = $buttonCrawlerNode->form();

        $data = [
            'user_new_form[nome]'                  => 'Mario',
            'user_new_form[cognome]'               => 'Rossi',
            'user_new_form[email]'                 => 'mario.rossi@gmail.com',
            'user_new_form[username]'              => 'mrossi',
            'user_new_form[ruolo]'                 => 'ROLE_USER',
            'user_new_form[plainPassword][first]'  => 'webtek',
            'user_new_form[plainPassword][second]' => 'webtek',
            'user_new_form[isEnabled]'             => true,
            'user_new_form[profileText]'           => 'Power ho! hoist to be crushed.',
        ];

        $this->loggedClient->submit($form, $data);

        //$crawler = $this->loggedClient->followRedirect(true);

        $elemento = $data['user_new_form[nome]'].' '.$data['user_new_form[cognome]'];

        $content = $this->loggedClient->getResponse()->getContent();

        $this->assertContains(
            'Operatore '.$elemento,
            $content
        );

        $this->assertContains(
            'creato',
            $content
        );


    }

    /**
     * Testa il metodo per l'abilitazione e la disabilitazione dell'utente
     */
    public function testToggleIsEnabled()
    {


        $crawler = $this->loggedClient->request('GET', '/admin/operatori/toggle-enabled/'.$this->User['id']);

        if ($this->User['isEnabled']) {
            $stato = ' disabilitato';
        } else {
            $stato = ' abilitato';
        }

        $this->assertTrue($this->loggedClient->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $content = $this->loggedClient->getResponse()->getContent();

        $elemento = $this->User['nome'].' '.$this->User['cognome'].' ('.$this->User['id'].')';

        $this->assertContains(
            'Operatore '.$elemento.$stato,
            $content
        );

    }

    public function testDelete()
    {

        $crawler = $this->loggedClient->request('GET', '/admin/operatori/delete/'.$this->User['id']);

        $this->assertTrue($this->loggedClient->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $content = $this->loggedClient->getResponse()->getContent();

        $elemento = $this->User['nome'].' '.$this->User['cognome'].' ('.$this->User['id'].')';

        $this->assertContains(
            'Operatore '.$elemento.' eliminato',
            $content
        );


    }

    public function testEdit()
    {


        $crawler = $this->loggedClient->request('GET', '/admin/operatori/edit/'.$this->User['id']);

        $content = $this->loggedClient->getResponse()->getContent();

        $this->assertContains(
            'Modifica operatore',
            $content
        );

        $buttonCrawlerNode = $crawler->selectButton('default.labels.salva');

        $form = $buttonCrawlerNode->form();

        $this->loggedClient->submit($form);

        //$crawler = $this->loggedClient->followRedirect(true);

        $elemento = $this->User['nome'].' '.$this->User['cognome'].' ('.$this->User['id'].') ';

        $content = $this->loggedClient->getResponse()->getContent();

        $this->assertContains(
            'Operatore '.$elemento.'modificato',
            $content,
            'Fallita edit dell\'operatore'
        );

    }

    private function getFirstOfJson()
    {

        $crawler = $this->loggedClient->request('GET', '/admin/operatori/json');

        $this->assertTrue($this->loggedClient->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $this->assertTrue(
            $this->loggedClient->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'mi aspettavo il "Content-Type:application/json"'
        );


        $pageContent = $this->loggedClient->getResponse()->getContent();

        $jsonList = json_decode($pageContent, true);

        $this->User = $jsonList['data'][0];

    }

    public function testListJson()
    {


        $crawler = $this->loggedClient->request('GET', '/admin/operatori/json');

        $this->assertTrue($this->loggedClient->getResponse()->isSuccessful(), 'Non ho ricevuto risposta corretta');

        $this->assertTrue(
            $this->loggedClient->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'mi aspettavo il "Content-Type:application/json"'
        );


        $pageContent = $this->loggedClient->getResponse()->getContent();

        $this->assertJson($pageContent, 'Non è un json valido');

    }

}
