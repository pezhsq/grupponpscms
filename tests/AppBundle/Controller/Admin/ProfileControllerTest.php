<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 14/12/16
 * Time: 15.02
 */

namespace Tests\AppBundle\Controller\Admin;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfileControllerTest extends WebTestCase {

    private $loggedClient = false;
    private $unloggedClient = false;
    private $User;
    private $container;

    public function setUp() {

        $this->loggedClient = static::createClient([], [
            'PHP_AUTH_USER' => 'gianiaz',
            'PHP_AUTH_PW' => 'webtek1216;',
        ]);

        $this->unloggedClient = static::createClient();

        $this->loggedClient->followRedirects();

        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    public function testProfileAction() {

        $crawler = $this->loggedClient->request('GET', '/admin/profile');

        $content = $this->loggedClient->getResponse()->getContent();

        $this->assertContains('default.labels.modifica_profilo', $content, 'Non ho trovato la stringa "Modifica profilo"');

        $buttonCrawlerNode = $crawler->selectButton('default.labels.salva');

        $form = $buttonCrawlerNode->form();

        $this->loggedClient->submit($form);

        $content = $this->loggedClient->getResponse()->getContent();

        $this->assertContains('operatori.messages.profilo_aggiornato', $content, 'Non ho trovato la stringa "Il tuo profilo è stato aggiornato"');

    }


    public function testFailProfileAction() {

        $crawler = $this->loggedClient->request('GET', '/admin/profile');

        $content = $this->loggedClient->getResponse()->getContent();

        $this->assertContains('default.labels.modifica_profilo', $content, 'Non ho trovato la stringa "Modifica profilo"');

        $buttonCrawlerNode = $crawler->selectButton('default.labels.salva');

        $data = ['profile_edit_form[nome]' => '',
            'profile_edit_form[email]' => 'mariorossigmail.com',
            'profile_edit_form[plainPassword][first]' => 'webte1k',
            'profile_edit_form[plainPassword][second]' => 'webtek'];

        $form = $buttonCrawlerNode->form();

        $this->loggedClient->submit($form, $data);

        $crawler = $this->loggedClient->getCrawler();

        $input = $crawler->filter('#profile_edit_form_email');
        $container = $input->parents()->first();
        $container = $container->parents()->first();


        $this->assertContains('has-error', $container->attr('class'), 'Il campo email dovrebbe essere segnalato come errato');

        $input = $crawler->filter('#profile_edit_form_plainPassword_first');
        $container = $input->parents()->first();
        $container = $container->parents()->first();

        $this->assertContains('has-error', $container->attr('class'), 'Il campo password dovrebbe essere segnalato come errato');

        $input = $crawler->filter('#profile_edit_form_nome');
        $container = $input->parents()->first();
        $container = $container->parents()->first();

        $this->assertContains('has-error', $container->attr('class'), 'Il campo nome dovrebbe essere segnalato come errato');

    }

}