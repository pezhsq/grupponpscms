<?php
/**
 * Created by PhpStorm.
 * User: gianiaz
 * Date: 16/12/16
 * Time: 12.40
 */

namespace Tests\AppBundle\Controller\Admin;

use Tests\AppBundle\Base\BaseController;

class LanguagesControllerTest extends BaseController
{

    public function setUpCrudController()
    {

        $this->addUrl('list', '/admin/languages');
        $this->addUrl('listJson', '/admin/languages/json');
        /*
        $this->addUrl('delete', '/admin/tag/delete/{id}');
        $this->addUrl('delete', '/admin/tag/delete/{id}/force');
        $this->addUrl('restore', '/admin/tag/restore/{id}');
        $this->addUrl('new', '/admin/tag/new');
        $this->addUrl('edit', '/admin/tag/edit/{id}');
        $this->addUrl('autocomplete', '/admin/tag/autocomplete-json');
*/
    }

    public function setUp()
    {

        parent::setUp();

        $this->getJsonData();
    }

    public function getJsonData()
    {

        $crawler = $this->loggedSuperAdmin->request('GET', $this->urls['listJson']);

        $pageContent = $this->loggedSuperAdmin->getResponse()->getContent();

        $this->assertJson($pageContent, 'Non è un json valido');

        $json = json_decode($pageContent, true);

        foreach ($json['data'] as $element) {

            $this->lingue[$element['shortCode']] = [
                'name'            => $element['languageName'],
                'isEnabled'       => $element['isEnabled'],
                'isEnabledPublic' => $element['isEnabledPublic'],
            ];

        }


    }

    public function testIsEnabled()
    {

        $crawler = $this->loggedSuperAdmin->request('GET', '/admin/languages/toggle-enabled/it');

        $content = $this->loggedSuperAdmin->getResponse()->getContent();

        // non posso disabilitare l'unica lingua attiva (l'italiano)
        $this->assertContains(
            'languages.messages.cant_disable_unique_language',
            $content,
            'Non dovrebbe essere possibile disabilitare l\'unica lingua disponibile'
        );

        $crawler = $this->loggedSuperAdmin->request('GET', '/admin/languages/toggle-enabled/en');

        $content = $this->loggedSuperAdmin->getResponse()->getContent();

        // abilito l'inglese che di default è disabilitato
        $this->assertContains('default.labels.abilitato', $content, 'Dovrebbe abilitare la lingua ora attiva');

        $crawler = $this->loggedSuperAdmin->request('GET', '/admin/languages/toggle-enabled/en');

        $content = $this->loggedSuperAdmin->getResponse()->getContent();

        // disabilito l'inglese attivato precedentemente
        $this->assertContains(
            'default.labels.disabilitato',
            $content,
            'Dovrebbe disabilitare la lingua appena attivata'
        );


    }

    /**
     * Test della funzionalità di disabilitazione/abilitazione della lingua lato pubblico
     *
     */
    public function testIsEnabledPublic()
    {

        $crawler = $this->loggedSuperAdmin->request('GET', '/admin/languages/toggle-enabled-public/it');


        $content = $this->loggedSuperAdmin->getResponse()->getContent();

        // non posso disabilitare l'unica lingua attiva (l'italiano)
        $this->assertContains(
            'languages.messages.cant_disable_unique_language',
            $content,
            'Non dovrebbe essere possibile disabilitare l\'unica lingua disponibile'
        );


        $crawler = $this->loggedSuperAdmin->request('GET', '/admin/languages/toggle-enabled-public/en');

        $content = $this->loggedSuperAdmin->getResponse()->getContent();

        // abilito l'inglese che di default è disabilitato
        $this->assertContains(
            'languages.messages.abilitato_public',
            $content,
            'Dovrebbe abilitare la lingua ora attiva'
        );

        $crawler = $this->loggedSuperAdmin->request('GET', '/admin/languages/toggle-enabled-public/en');

        $content = $this->loggedSuperAdmin->getResponse()->getContent();

        // disabilito l'inglese attivato precedentemente
        $this->assertContains(
            'languages.messages.disabilitato_public',
            $content,
            'Dovrebbe disabilitare la lingua appena attivata'
        );

    }

}